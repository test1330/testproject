package com.aie.arc.account.api.common.annotation;

import com.aie.arc.account.api.common.annotation.constants.AnnotationKeyConstants;
import com.aie.arc.account.api.common.annotation.validator.ValidNameARCValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


/**
 * The annotated element must not be {@code null} and must contain at least one
 * non-whitespace character. Accepts {@code CharSequence}.
 *
 * @author Hardy Ferentschik
 * @since 2.0
 *
 * @see Character#isWhitespace(char)
 */
@Documented
@Constraint(validatedBy = {ValidNameARCValidator.class})
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Repeatable(ValidNameARC.List.class)
public @interface ValidNameARC {

    String message() default AnnotationKeyConstants.FIRST_NAME_INVALID;

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };


    /**
     * Defines several {@code @NotBlank} constraints on the same element.
     *
     * @see javax.validation.constraints.NotBlank
     */
    @Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
    @Retention(RUNTIME)
    @Documented
    @interface List {
        ValidNameARC[] value();
    }


}
