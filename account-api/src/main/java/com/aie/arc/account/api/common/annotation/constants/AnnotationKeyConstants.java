package com.aie.arc.account.api.common.annotation.constants;



public class AnnotationKeyConstants {
    private  AnnotationKeyConstants()
    {

    }
    public static final String NOT_BLANK_ARC = "NotBlank";
    public static final String SELECT_EXPIRATION_DATE = "SelectExpirationDate";
    public static final String CARD_LENGTH_INCORRECT = "CardNumberLengthIncorrect";
    public static final String FIRST_NAME_INVALID = "FirstNameInvalid";
    public static final String INVALID_CARD_NUMBER = "InvalidCardNumber";
    public static final String INVALID_CARD_HOLDER_NAME = "IncorrectCardHolderName";
    public static final String INVALID_CARD_INFORMATION = "InvalidCardInformation";
    public static final String INVALID_CARD_FIELD_LENGTH = "InvalidCardFieldLength";
    public static final String INVALID_ADDRESS_FIELD_LENGTH = "InvalidAddressFieldLength";
    public static final String INVALID_NAME_FIELD_LENGTH = "InvalidNameFieldLength";
    public static final String INVALID_CITY_FIELD_LENGTH = "InvalidCityFieldLength";
    public static final String INVALID_STATE_FIELD_LENGTH = "InvalidStateFieldLength";
    public static final String INVALID_POSTAL_CODE_FIELD_LENGTH = "InvalidPostalCodeFieldLength";


}
