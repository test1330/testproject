package com.aie.arc.account.api.common.annotation.validator;

import com.aie.arc.account.api.common.annotation.ValidNameARC;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Check that a character sequence is not {@code null} nor empty after removing any leading or trailing whitespace.
 *
 * @author Guillaume Smet
 */
public class ValidNameARCValidator implements ConstraintValidator<ValidNameARC, CharSequence> {

    /**
     * Checks that the character sequence is not {@code null} nor empty after removing any leading or trailing
     * whitespace.
     *
     * @param charSequence the character sequence to validate
     * @param constraintValidatorContext context in which the constraint is evaluated
     * @return returns {@code true} if the string is not {@code null} and the length of the trimmed
     * {@code charSequence} is strictly superior to 0, {@code false} otherwise
     */
    @Override
    public boolean isValid(CharSequence charSequence, ConstraintValidatorContext constraintValidatorContext) {
        if (charSequence == null || !charSequence.toString().matches("^[a-zA-Z ]*$")) {
            return false;
        }
        return charSequence.toString().trim().length() > 0;
    }

}
