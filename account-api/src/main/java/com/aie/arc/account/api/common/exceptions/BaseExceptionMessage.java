package com.aie.arc.account.api.common.exceptions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.lightbend.lagom.javadsl.api.deser.ExceptionMessage;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@JsonIgnoreProperties({"name", "detail"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseExceptionMessage extends ExceptionMessage {

    private static final long serialVersionUID = 1L;

    private Integer status;

    @Setter(AccessLevel.NONE)
    @Singular("error")
    private BaseError errors = null;

    public BaseExceptionMessage(String errorCode, String exceptionName, String errorMessage) {
        super(exceptionName, errorMessage);

        this.addError(BaseError.builder()
               // .id(UUID.randomUUID())
                //.time(Instant.now().toEpochMilli())
                .errorCode(errorCode)
                .internalMessage(errorMessage)
                .build()
        );
    }

    public BaseExceptionMessage(BaseError error) {
        this(error.getDeveloperMessage(), error);
    }

    public BaseExceptionMessage(String exceptionName, BaseError error) {
        super(exceptionName, error.getInternalMessage());

        this.addError(error);
    }

    public void addError(BaseError error) {
        this.errors = error;
    }
}
