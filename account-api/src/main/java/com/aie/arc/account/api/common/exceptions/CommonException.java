package com.aie.arc.account.api.common.exceptions;

import com.lightbend.lagom.javadsl.api.transport.TransportErrorCode;

public class CommonException extends BaseTransportException{

    public CommonException(String[] message) {
        super(TransportErrorCode.BadRequest, BaseError.builder()
                .message(message)
                .build()
            );
    }
}
