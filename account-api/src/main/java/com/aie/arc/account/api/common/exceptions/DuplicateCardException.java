package com.aie.arc.account.api.common.exceptions;

import com.lightbend.lagom.javadsl.api.transport.TransportErrorCode;

public class DuplicateCardException extends BaseTransportException {

    private String[] strings = {"Duplicate Card"};

    public DuplicateCardException(String[] message) {
        super(TransportErrorCode.BadRequest, BaseError.builder()
                .message(message)
                .build()
        );
        strings = message;
    }
}
