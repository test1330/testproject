package com.aie.arc.account.api.common.exceptions;


/**
 * This exception indicates that cryptography operation error occurred.
 */
public class EncryptorException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new EncryptorException.
	 */
	public EncryptorException() {
		super();
	}

	/**
	 * Constructs a new EncryptorException with the given 
	 * explanation.
	 *
	 * @param pStr String that describes exception
	 */
	public EncryptorException(String pStr) {
		super(pStr);
	}

	/**
	 * Constructs a new EncryptorException.
	 * @param pSourceException the initial exception which was the root
	 * cause of the problem
	 */
	public EncryptorException(Throwable pSourceException) {
		super(pSourceException);
	}

	/**
	 * Constructs a new EncryptorException with the given 
	 * explanation.
	 * @param pStr String that describes exception
	 *
	 * @param pSourceException the initial exception which was the root
	 * cause of the problem
	 */
	public EncryptorException(String pStr, Throwable pSourceException) {
		super(pStr, pSourceException);
	}

}
