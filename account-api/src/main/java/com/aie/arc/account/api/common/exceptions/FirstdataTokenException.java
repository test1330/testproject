package com.aie.arc.account.api.common.exceptions;

import com.lightbend.lagom.javadsl.api.transport.TransportErrorCode;

public class FirstdataTokenException extends BaseTransportException {

    private String[] strings = {"Unable to get the Subscription Id from FirstData Service"};

    public FirstdataTokenException(String[] message) {
        super(TransportErrorCode.BadRequest, BaseError.builder()
                .message(message)
                .build()
        );
        strings = message;
    }
}
