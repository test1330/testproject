package com.aie.arc.account.api.common.exceptions;

import com.lightbend.lagom.javadsl.api.transport.TransportErrorCode;

public class InvalidParameter extends BaseTransportException {

    static String[] DEFAULT_MESSAGE = {"Parameter Required"};

    public InvalidParameter() {
        super(TransportErrorCode.BadRequest, BaseError.builder()
                .message(DEFAULT_MESSAGE)
                .build()
        );
    }

    public InvalidParameter(String message) {
        super(TransportErrorCode.BadRequest, BaseError.builder()
                .message(new String[] {message})
                .build()
        );
    }
    public InvalidParameter(String mainExceptionMessage, String message) {
        this(mainExceptionMessage, new String[]{message});
    }
    public InvalidParameter(String mainExceptionMessage, String[] message) {
        super(TransportErrorCode.BadRequest,mainExceptionMessage, BaseError.builder()
                .message(message)
                .build()
        );
        DEFAULT_MESSAGE = message;
    }

    public InvalidParameter(String[] message) {
        super(TransportErrorCode.BadRequest, BaseError.builder()
                .message(message)
                .build()
        );
        DEFAULT_MESSAGE = message;
    }

    public InvalidParameter(Throwable throwable) {
        super(TransportErrorCode.BadRequest, BaseError.builder()
                .message(DEFAULT_MESSAGE)
                .developerMessage(throwable.getMessage())
                .build()
        );
    }

    public InvalidParameter(BaseError error) {
        super(TransportErrorCode.BadRequest, error);
    }

    public String[] getDefaultMessage() {
        return DEFAULT_MESSAGE;
    }
}
