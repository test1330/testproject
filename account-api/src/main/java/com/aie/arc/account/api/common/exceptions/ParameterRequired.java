package com.aie.arc.account.api.common.exceptions;

import com.lightbend.lagom.javadsl.api.transport.TransportErrorCode;

public class ParameterRequired extends BaseTransportException {

        static String[] DEFAULT_MESSAGE = {"Parameter Required"};

        public ParameterRequired() {
            super(TransportErrorCode.BadRequest, BaseError.builder()
                    .message(DEFAULT_MESSAGE)
                    .build()
            );
        }

        public ParameterRequired(String[] message) {
            super(TransportErrorCode.BadRequest, BaseError.builder()
                    .message(message)
                    .build()
            );
        }

        public ParameterRequired(Throwable throwable) {
            super(TransportErrorCode.BadRequest, BaseError.builder()
                    .message(DEFAULT_MESSAGE)
                    .developerMessage(throwable.getMessage())
                    .build()
            );
        }

        public ParameterRequired(BaseError error) {
            super(TransportErrorCode.BadRequest, error);
        }

        public String[] getDefaultMessage() {
            return DEFAULT_MESSAGE;
        }

    }
