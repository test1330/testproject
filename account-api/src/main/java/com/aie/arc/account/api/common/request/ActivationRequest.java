package com.aie.arc.account.api.common.request;

import javax.validation.constraints.NotNull;


public class ActivationRequest extends BaseRequest {
    @NotNull
    private Boolean value;

    public ActivationRequest() {
    }

    public ActivationRequest(Boolean value) {
        this.value = value;
    }

    public Boolean getValue() {
        return value;
    }

    public void setValue(Boolean value) {
        this.value = value;
    }
}