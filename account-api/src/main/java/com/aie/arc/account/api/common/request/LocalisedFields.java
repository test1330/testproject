package com.aie.arc.account.api.common.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.validation.constraints.NotBlank;

//@Value
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class LocalisedFields {
    @NotBlank
    private String name;
    @NotBlank
    private String description;
}
