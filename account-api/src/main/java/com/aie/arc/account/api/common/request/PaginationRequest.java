package com.aie.arc.account.api.common.request;

import lombok.Getter;

import javax.validation.constraints.Pattern;

@Getter
public class PaginationRequest {
    @Pattern(regexp="(^$|[0-9]{1,4})" , message = "Invalid pageNo")
    private String pageNo = "0";
    @Pattern(regexp="(^$|[0-9]{1,4})" , message = "Invalid pageSize")
    private String pageSize="10";
}
