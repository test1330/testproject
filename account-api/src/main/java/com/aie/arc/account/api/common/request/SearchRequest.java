package com.aie.arc.account.api.common.request;

import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Map;

@Getter
public class SearchRequest extends BaseRequest {
    Map<@NotBlank String, @NotBlank String> filters = new HashMap<>();
    @Valid
    PaginationRequest pagination;
    @Valid
    SortingRequest sorting;
}
