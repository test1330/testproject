package com.aie.arc.account.api.common.request;

import lombok.Getter;

@Getter
public class SortingRequest {
    private String ascending;
    private String field;
}
