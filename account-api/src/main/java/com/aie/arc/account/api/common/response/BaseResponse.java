package com.aie.arc.account.api.common.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Value;

import java.util.UUID;

@Value
@JsonIgnoreProperties({"name", "detail"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponse {

    private static final long serialVersionUID = 1L;

    private String id;
    private Long time;
    private Integer statusCode;
    private String message;

    public BaseResponse(String id, Long time, Integer statusCode, String message) {
        this.id = id;
        this.time = time;
        this.statusCode = statusCode;
        this.message = message;
    }

    public BaseResponse(Integer statusCode, String message) {
        this.id = null;
        this.time = null;
        this.statusCode = statusCode;
        this.message = message;
    }

    public BaseResponse(String id, Integer statusCode, String message) {
        this.id = id;
        this.time = System.currentTimeMillis();
        this.statusCode = statusCode;
        this.message = message;
    }

    public static BaseResponse getResponseByStatusAndMessage(String id,Integer status, String message) {
        return new BaseResponse(id,status, message);
    }
    public static BaseResponse getResponseByStatusAndMessage(Integer status, String message) {
        return new BaseResponse(status, message);
    }
}
