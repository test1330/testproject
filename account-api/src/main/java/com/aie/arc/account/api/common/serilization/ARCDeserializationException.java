/*
 * Copyright (C) 2016-2019 Lightbend Inc. <https://www.lightbend.com>
 */

package com.aie.arc.account.api.common.serilization;

import com.aie.arc.account.api.common.exceptions.BaseError;
import com.aie.arc.account.api.common.exceptions.BaseTransportException;
import com.lightbend.lagom.javadsl.api.transport.TransportErrorCode;

/** Exception thrown when there is a problem deserializing a message. */
public class ARCDeserializationException extends BaseTransportException {

  private static final long serialVersionUID = 1L;

  public static final TransportErrorCode ERROR_CODE = TransportErrorCode.UnsupportedData;

  public ARCDeserializationException(Throwable cause) {
    super(ERROR_CODE, BaseError.builder()
            .message(new String[] {cause.getMessage()})
            .build());
  }

  public ARCDeserializationException(String message) {
    super(ERROR_CODE, BaseError.builder()
            .message(new String[] {message})
            .build());
  }
}
