package com.aie.arc.account.api.common.serilization;

import akka.Done;
import akka.util.ByteString;
import com.lightbend.lagom.javadsl.api.deser.SerializationException;
import com.lightbend.lagom.javadsl.api.deser.StrictMessageSerializer;
import com.lightbend.lagom.javadsl.api.transport.MessageProtocol;
import org.pcollections.PSequence;
import org.pcollections.TreePVector;

import java.util.List;
import java.util.Optional;

public class ARCDoneMessageSerializer<MessageEntity>
        implements StrictMessageSerializer<MessageEntity> {

    private final NegotiatedSerializer<MessageEntity, ByteString> serializer = new DoneSerializer();
    private final NegotiatedDeserializer<MessageEntity, ByteString> deserializer = new DoneDeserializer();

    @Override
    public PSequence<MessageProtocol> acceptResponseProtocols() {
        return TreePVector.singleton(
                new MessageProtocol(Optional.of("application/json"), Optional.empty(), Optional.empty()));
    }

    @Override
    public NegotiatedSerializer<MessageEntity, ByteString> serializerForRequest() {
        return serializer;
    }

    @Override
    public NegotiatedDeserializer<MessageEntity, ByteString> deserializer(
            MessageProtocol messageProtocol) throws SerializationException {
        return deserializer;
    }

    @Override
    public NegotiatedSerializer<MessageEntity, ByteString> serializerForResponse(
            List<MessageProtocol> acceptedMessageProtocols) {
        return serializer;
    }

    private class DoneSerializer implements NegotiatedSerializer<MessageEntity, ByteString> {

        private final ByteString doneJson = ByteString.fromString("{ \"done\" : true }");

        @Override
        public MessageProtocol protocol() {
            return new MessageProtocol(
                    Optional.of("application/json"), Optional.of("utf-8"), Optional.empty());
        }

        @Override
        public ByteString serialize(MessageEntity obj) {
            return doneJson;
        }
    }

    private class DoneDeserializer implements NegotiatedDeserializer<MessageEntity, ByteString> {
        @SuppressWarnings("unchecked")
        @Override
        public MessageEntity deserialize(ByteString bytes) {
            return (MessageEntity) Done.getInstance();
        }
    }
}
