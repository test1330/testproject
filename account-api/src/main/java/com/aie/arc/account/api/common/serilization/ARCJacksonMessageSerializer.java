package com.aie.arc.account.api.common.serilization;

import akka.util.ByteString;
import akka.util.ByteString$;
import akka.util.ByteStringBuilder;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.lightbend.lagom.javadsl.api.deser.SerializationException;
import com.lightbend.lagom.javadsl.api.deser.StrictMessageSerializer;
import com.lightbend.lagom.javadsl.api.transport.MessageProtocol;
import lombok.extern.slf4j.Slf4j;
import org.pcollections.PSequence;
import org.pcollections.TreePVector;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

@Slf4j
public class ARCJacksonMessageSerializer<MessageEntity> implements StrictMessageSerializer<MessageEntity> {

    private final NegotiatedSerializer<MessageEntity, ByteString> serializer;
    private final NegotiatedDeserializer<MessageEntity, ByteString> deserializer;

    public ARCJacksonMessageSerializer(Type type, ObjectMapper objectMapper) {
        JavaType javaType = objectMapper.constructType(type);
        serializer = new ARCJacksonSerializer(objectMapper.writerFor(javaType));
        deserializer = new ARCJacksonDeserializer(objectMapper.readerFor(javaType), type);
    }

    @Override
    public PSequence<MessageProtocol> acceptResponseProtocols() {
        log.debug("acceptResponseProtocols method");
        return TreePVector.singleton(
                new MessageProtocol(Optional.of("application/json"), Optional.empty(), Optional.empty()));
    }

    @Override
    public NegotiatedSerializer<MessageEntity, ByteString> serializerForRequest() {
        return serializer;
    }

    @Override
    public NegotiatedDeserializer<MessageEntity, ByteString> deserializer(
            MessageProtocol messageProtocol) throws SerializationException {
        return deserializer;
    }

    @Override
    public NegotiatedSerializer<MessageEntity, ByteString> serializerForResponse(
            List<MessageProtocol> acceptedMessageProtocols) {
        return serializer;
    }

    private class ARCJacksonSerializer implements NegotiatedSerializer<MessageEntity, ByteString> {
        private final ObjectWriter writer;

        public ARCJacksonSerializer(ObjectWriter writer) {
            this.writer = writer;
        }

        @Override
        public MessageProtocol protocol() {
            return new MessageProtocol(
                    Optional.of("application/json"), Optional.of("utf-8"), Optional.empty());
        }

        @Override
        public ByteString serialize(MessageEntity messageEntity) {
            ByteStringBuilder builder = ByteString$.MODULE$.newBuilder();
            try {
                writer.writeValue(builder.asOutputStream(), messageEntity);
            } catch (Exception exp) {
                log.error("Exception occured at : ",exp);
                throw new SerializationException(exp);
            }
            return builder.result();
        }
    }

    private class ARCJacksonDeserializer implements NegotiatedDeserializer<MessageEntity, ByteString> {
        private final ObjectReader reader;
        private final Type type;

        public ARCJacksonDeserializer(ObjectReader reader, Type type) {
            this.reader = reader;
            this.type = type;
        }

        @Override
        public MessageEntity deserialize(ByteString bytes) {
            try {
                if (bytes.isEmpty() && this.type == Optional.class) {
                    bytes = ByteString.fromString("null");
                }
                return reader.readValue(bytes.iterator().asInputStream());
            } catch (Exception exp) {
                log.error("Exception message while deserializing : ", exp);
                throw new ARCDeserializationException("common: Error708: Deserialization Exception encountered. Possibly invalid request payload.");
            }
        }
    }
}
