/*
 * Copyright (C) 2016-2019 Lightbend Inc. <https://www.lightbend.com>
 */

package com.aie.arc.account.api.common.serilization;

import akka.Done;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lightbend.lagom.javadsl.api.deser.SerializerFactory;
import com.lightbend.lagom.javadsl.api.deser.StrictMessageSerializer;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.lang.reflect.Type;

/** A Jackson Serializer Factory */
@Singleton
@Slf4j
public class ARCJacksonSerializerFactory implements SerializerFactory {

  private final ObjectMapper objectMapper;

  /** For testing purposes */
  public ARCJacksonSerializerFactory(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  @Override
  public <MessageEntity> StrictMessageSerializer<MessageEntity> messageSerializerFor(Type type) {
    if (type == Done.class) return new ARCDoneMessageSerializer<>();
    else return new ARCJacksonMessageSerializer<>(type, objectMapper);
  }




}
