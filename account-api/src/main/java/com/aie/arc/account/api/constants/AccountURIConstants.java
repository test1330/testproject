package com.aie.arc.account.api.constants;

public class AccountURIConstants {
    private  AccountURIConstants()
    {

    }

    //Account
    public static final String GET_ACCOUNT_URI = "/api/v1/accounts/:accountId";
    public static final String GET_ACCOUNT_ADDRESS_URI = "/api/v1/accounts/:accountId/addresses/:addressId";
    public static final String GET_ACCOUNT_ADDRESS_LIST_URI = "/api/v1/accounts/:accountId/addresses";

    public static final String VALIDATE_ACCOUNT_ADDRESS_URI = "/api/v1/accounts/:accountId/validate/addresses";
    public static final String ADD_ACCOUNT_ADDRESS_URI = "/api/v1/accounts/:accountId/addresses";
    public static final String UPDATE_ACCOUNT_ADDRESS_URI = "/api/v1/accounts/:accountId/addresses/:addressId";
    public static final String SET_DEFAULT_BILLING_ADDRESS_URI = "/api/v1/accounts/:accountId/billing-addresses/:addressId";
    public static final String SET_DEFAULT_SHIPPING_ADDRESS_URI = "/api/v1/accounts/:accountId/shipping-addresses/:addressId";
    public static final String ACTIVE_ADDRESS_URI = "/api/v1/accounts/:accountId/addresses/:addressId/active";

    public static final String SUBSCRIBE_URI = "/api/v1/accounts/:accountId/subscribe";
    public static final String ACCOUNT_ADDRESS_CREATE_MESSAGE = "AccountAddress created";
    public static final String ACCOUNT_ADDRESS_UPDATE_MESSAGE = "AccountAddress updated";
    public static final String DEFAULT_BILLING_ADDRESS_SET_MESSAGE = "Default Billing address added";
    public static final String DEFAULT_SHIPPING_ADDRESS_SET_MESSAGE = "Default Shipping address added";

    public static final String SUBSCRIBE_MESSAGE = "Subscribed";
    public static final String UNSUBSCRIBE_MESSAGE = "Unsubscribed";

    //Add zipCode to Account
    public static final String SET_ZIP_CODE_URI = "/api/v1/accounts/:accountId/zip-codes";
    public static final String ZIP_CODE_ADDED_MESSAGE = "ZipCode set.";

    //Add card to Account
    public static final String ADD_CARD_URI = "/api/v1/accounts/:accountId/cards";
    public static final String UPDATE_CARD_URI = "/api/v1/accounts/:accountId/cards/:cardId";
    public static final String GET_CARD_URI = "/api/v1/accounts/:accountId/cards/:cardId";
    public static final String GET_ACCOUNT_CARDS_URI = "/api/v1/account/:accountId/cards";
    public static final String GET_CARD_LIST_URI = "/api/v1/accounts/:accountId/cards/search";
    public static final String ACTIVE_CARD_URI = "/api/v1/accounts/:accountId/cards/:cardId/active";
    public static final String DEFAULT_PAYMENT_URI = "/api/v1/accounts/:accountId/cards/:cardId/default-cards";
    public static final String GET_DEFAULT_PAYMENT_URI = "/api/v1/accounts/:accountId/cards/default-cards";

    public static final String CARD_CREATED_MESSAGE = "Card created";
    public static final String CARD_UPDATED_MESSAGE = "Card updated";
    public static final String CARD_ACTIVATE_MESSAGE = "Card activated";
    public static final String CARD_DEACTIVATE_MESSAGE = "Card deactivated";
    public static final String DEFAULT_PAYMENT_MESSAGE = "Default Payment Set";

    public static final String TEST_EMAIL_SENT = "Test Email Sent";

    // For testing purpose only.
    public static final String POST_TEST_ACC_URI = "/api/v1/accounts/testAcc";


    // Address
    public static final String ADDRESS_CREATED_MESSAGE = "Address created";
    public static final String ADDRESS_UPDATED_MESSAGE = "Address updated";
    public static final String ADDRESS_ACTIVATE_MESSAGE = "Address activated";
    public static final String ADDRESS_DEACTIVATE_MESSAGE = "Address deactivated";

    public static final String EMAIL_ADDRESS_VALIDATION_URI = "/api/v1/email/validator?email";
    public static final String ADDRESS_VALIDATION_URI = "/api/v1/address/validator";
    public static final String IP_INFO_VALIDATION_URI = "/api/v1/ipinfo/validator?ipAddress";
    public static final String GET_PROFILE_ZIP_CODE_URI = "/api/v1/accounts/:accountId/profile-zip-code";
    public static final String POST_SEND_EMAIL = "/api/v1/sendEmail";


    public static final String CREATE_CUSTOMER_SOAP_SERVICE_TEST_URI = "/api/v1/soap/test/create-customer";
    public static final String SUBSCRIPTION_SOAP_SERVICE_TEST_URI = "/api/v1/soap/test/subscription";

    //BusinessUserAccount
    public static final String UPDATE_BUSINESS_USER_URI = "/api/v1/business-users/:businessUserId";
    public static final String CREATE_BUSINESS_USER_ADDRESS_URI = "/api/v1/business-users/:businessUserId/addresses";
    public static final String UPDATE_BUSINESS_USER_ADDRESS_URI = "/api/v1/business-users/:businessUserId/addresses/:addressId";
    public static final String GET_BUSINESS_USER_ADDRESS_URI = "/api/v1/business-users/:businessUserId/addresses/:addressId";
    public static final String GET_BUSINESS_USER_ADDRESS_LIST = "/api/v1/business-users/:businessUserId/addresses/search";
    public static final String ACTIVE_BUSINESS_USER_URI = "/api/v1/business-users/:businessUserId/active";
    public static final String GET_BUSINESS_USER_URI = "/api/v1/business-users/:businessUserId";
    public static final String GET_BUSINESS_USER_DETAIL_URI = "/api/v1/business-users/details/:businessUserId";
    public static final String BUSINESS_USER_UPDATED_MESSAGE = "BusinessUser updated";
    public static final String GET_BUSINESS_USER_LIST_URL = "/api/v1/business-users/search";
    public static final String BUSINESS_USER_ACTIVATE_MESSAGE="Business User activated";
    public static final String BUSINESS_USER_DEACTIVATE_MESSAGE="Business User deactivated";

    //CustomerGroup
    public static final String CREATE_CUSTOMER_GROUP_URI = "/api/v1/customer-groups";
    public static final String UPDATE_CUSTOMER_GROUP_URI = "/api/v1/customer-groups/:customerGroupId";
    public static final String GET_CUSTOMER_GROUP_URI = "/api/v1/customer-groups/:customerGroupId";
    public static final String GET_CUSTOMER_GROUP_LIST_URI = "/api/v1/customer-groups/search";
    public static final String ACTIVE_CUSTOMER_GROUP_URI = "/api/v1/customer-groups/:customerGroupId/active";
    public static final String CUSTOMER_GROUP_CREATED_MESSAGE = "Customer Group created";
    public static final String CUSTOMER_GROUP_UPDATED_MESSAGE = "Customer Group updated";
    public static final String CUSTOMER_GROUP_ACTIVATE_MESSAGE = "Customer Group activated";
    public static final String CUSTOMER_GROUP_DEACTIVATE_MESSAGE = "Customer Group deactivated";

    //Store
    public static final String ADD_STORE ="/api/v1/accounts/:accountId/stores";
    public static final String UPDATE_STORE ="/api/v1/accounts/:accountId/stores";
    public static final String UPDATE_STORE_MESSAGE ="Store updated to account";
    public static final String ADD_STORE_MESSAGE ="Store added to  account";


}