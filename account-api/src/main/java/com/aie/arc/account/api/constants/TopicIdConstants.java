package com.aie.arc.account.api.constants;

public class TopicIdConstants {
    private TopicIdConstants()
    {

    }

    /*account*/
    public static final String ACCOUNT_SYNC_TOPIC_ID = "arc_AccountSync";

    public static final String ACCOUNT_TOPIC_ID = "acc-profile";
    public static final String CARD_TOPIC_ID = "acc-profile-payment";
    public static final String ADDRESS_TOPIC_ID = "acc-profile-address";

    public static final String BUSINESS_USER_TOPIC_ID = "business-user-userPublishedEvent";
    public static final String CUSTOMER_GROUP_TOPIC_ID = "customerGroup-customerGroupPublishedEvent";

}
