package com.aie.arc.account.api.email;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EmailRequest {

    private List<Personalizations> personalizations = new ArrayList<Personalizations>();

    private From from;

    @JsonProperty(value = "reply_to")
    private ReplyTo replyTo;

    @JsonProperty(value = "template_id")
    private String templateId;

}
