package com.aie.arc.account.api.email;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Personalizations {

    private List<To> to = new ArrayList<To>();

    @JsonProperty(value = "dynamic_template_data")
    private Map<String, String> content = new HashMap<String, String>();

    private String subject;
}
