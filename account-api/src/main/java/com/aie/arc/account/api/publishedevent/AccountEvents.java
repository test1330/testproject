package com.aie.arc.account.api.publishedevent;

import com.aie.arc.account.api.request.dto.AccountProfile;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

import java.time.LocalDateTime;
import java.util.List;


@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", defaultImpl = Void.class)
@JsonSubTypes({
        @JsonSubTypes.Type(AccountEvents.AccountEvent.class),
})
public interface AccountEvents {

    String getAccountId();


    @JsonTypeName(value = "account-event")
    @Value
    @JsonDeserialize
    class AccountEvent implements AccountEvents {

        public final String accountId;
        public final String emailId;
        public final String firstName;
        public final String lastName;
        public final String myZip;
        /*private AccountProfile profile;*/
        public final String bmRoleId;
        public final List<String> flashSaleLimitedIds;
        public final List<String> addressIds;
        public final String defaultBillingAddressId;
        public final String defaultShippingAddressId;
        public final List<String> cardIds;
        public final String defaultCardId;
        public final String eventType;

        @Override
        public String getAccountId() {
            return this.accountId;
        }

        @JsonCreator
        public AccountEvent(String accountId,String emailId,String firstName,String lastName,String myZip,String bmRoleId,
                           List<String> flashSaleLimitedIds,List<String> addressIds, String defaultBillingAddressId,
                            String defaultShippingAddressId,List<String> cardIds, String defaultCardId,String eventType){
            this.accountId = accountId;
            this.emailId = emailId;
            this.firstName = firstName;
            this.lastName = lastName;
            this.myZip = myZip;
            this.bmRoleId = bmRoleId;
            this.flashSaleLimitedIds = flashSaleLimitedIds;
            this.addressIds = addressIds;
            this.defaultBillingAddressId = defaultBillingAddressId;
            this.defaultShippingAddressId = defaultShippingAddressId;
            this.cardIds = cardIds;
            this.defaultCardId =  defaultCardId;
            this.eventType = eventType;
        }
    }
/*
    @JsonTypeName(value = "account-updated")
    @Value
    class AccountUpdated implements AccountEvents {
        private String accountId;
        private Integer accountNumber;
        private AccountProfile profile;
        String bmRoleId;
        List<String> flashSaleLimitedIds;
        private List<String> addressIds;
        private String defaultBillingAddressId;
        private String defaultShippingAddressId;
        private List<String> cardIds;
        private boolean subscribed;
        private boolean active;
        private LocalDateTime timeStamp;

        @Override
        public String getAccountId() {
            return this.accountId;
        }
    }

    @JsonTypeName(value = "profile-updated")
    @Value
    class ProfileUpdated implements AccountEvents {
        private String accountId;
        private AccountProfile profile;
        private boolean active;
        private LocalDateTime timeStamp;


        @Override
        public String getAccountId() {
            return this.accountId;
        }
    }
    @JsonTypeName(value = "user-profile")
    @Value
    class UserProfile implements AccountEvents {
        private String accountId;
        private String myZip;
        private String listPiceId;
        private String bmRoleId;
        private List<String> flashSaleData;


        @Override
        public String getAccountId() {
            return this.accountId;
        }
    }

    @JsonTypeName(value = "account-activated")
    @Value
    class AccountActivated implements AccountEvents {
        private String accountId;
        private boolean active;
        private LocalDateTime timeStamp;

        @Override
        public String getAccountId() {
            return this.accountId;
        }
    }

    @JsonTypeName(value = "account-deactivated")
    @Value
    class AccountDeactivated implements AccountEvents {
        private String accountId;
        private boolean active;
        private LocalDateTime timeStamp;

        @Override
        public String getAccountId() {
            return this.accountId;
        }
    }

    @JsonTypeName(value = "account-newAddressLinkedToAccount")
    @Value
    class NewAddressLinkedToAccount implements AccountEvents {
        private String accountId;
        private String addressId;
        boolean shippingAddressId;
        boolean billingAddressId;
        private LocalDateTime timeStamp;

        @Override
        public String getAccountId() {
            return this.accountId;
        }
    }

    @JsonTypeName(value = "account-updateAddressLinkedToAccount")
    @Value
    class UpdateAddressLinkedToAccount implements AccountEvents {
        private String accountId;
        private String addressId;
        boolean shippingAddressId;
        boolean billingAddressId;
        private LocalDateTime timeStamp;

        @Override
        public String getAccountId() {
            return this.accountId;
        }
    }

    @JsonTypeName(value = "account-defaultBillingAddressUpdatedToLinkedToAccount")
    @Value
    class DefaultBillingAddressUpdatedToLinkedToAccount implements AccountEvents {
        private String accountId;
        String billingAddressId;
        private LocalDateTime timeStamp;

        @Override
        public String getAccountId() {
            return this.accountId;
        }
    }

    @JsonTypeName(value = "account-defaultShippingAddressUpdatedToLinkedToAccount")
    @Value
    class DefaultShippingAddressUpdatedToLinkedToAccount implements AccountEvents {
        private String accountId;
        String shippingAddressId;
        private LocalDateTime timeStamp;

        @Override
        public String getAccountId() {
            return this.accountId;
        }
    }


    @JsonTypeName(value = "account-addressDeactivatedToAccount")
    @Value
    class AddressDeactivatedToAccount implements AccountEvents {
        private String accountId;
        String addressId;
        private LocalDateTime timeStamp;

        @Override
        public String getAccountId() {
            return this.accountId;
        }
    }

    @JsonTypeName(value = "account-newCardLinkedToAccount")
    @Value
    class NewCardLinkedToAccount implements AccountEvents {
        private String accountId;
        String cardId;
        private LocalDateTime timeStamp;

        @Override
        public String getAccountId() {
            return this.accountId;
        }
    }

    @JsonTypeName(value = "account-updateCardLinkedToAccount")
    @Value
    class UpdateCardLinkedToAccount implements AccountEvents {
        private String accountId;
        String cardId;
        private LocalDateTime timeStamp;

        @Override
        public String getAccountId() {
            return this.accountId;
        }
    }

    @JsonTypeName(value = "account-updateCardAsDefaultCardToAccount")
    @Value
    class UpdateCardAsDefaultCardToAccount implements AccountEvents {
        private String accountId;
        String cardId;
        private LocalDateTime timeStamp;

        @Override
        public String getAccountId() {
            return this.accountId;
        }
    }

    @JsonTypeName(value = "account-cardDeactivatedToAccount")
    @Value
    class CardDeactivatedToAccount implements AccountEvents {
        private String accountId;
        String cardId;
        private LocalDateTime timeStamp;

        @Override
        public String getAccountId() {
            return this.accountId;
        }
    }

    @JsonTypeName(value = "account-zipCodeAdded")
    @Value
    class ZipCodeAdded implements AccountEvents {
        String accountId;
        String myZip;
        LocalDateTime timeStamp;

        @Override
        public String getAccountId() {
            return this.accountId;
        }
    }*/
}