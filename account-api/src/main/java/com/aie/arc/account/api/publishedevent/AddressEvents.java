package com.aie.arc.account.api.publishedevent;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.time.LocalDateTime;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", defaultImpl = Void.class)
@JsonSubTypes({
        @JsonSubTypes.Type(AddressEvents.AddressCreated.class),
        @JsonSubTypes.Type(AddressEvents.AddressUpdated.class),
        @JsonSubTypes.Type(AddressEvents.AddressDeactivated.class)
})
public interface AddressEvents extends Jsonable {

    String getAddressId();

    @JsonTypeName(value = "address-create")
    @Value
    @JsonDeserialize
    class AddressCreated implements AddressEvents {
        public final String accountId;
        public final String addressId;
        public final String firstName;
        public final String lastName;
        public final String address1;
        public final String address2;
        public final String city;
        public final String state;
        public final String postalCode;
        public final String phone;
        public final boolean defaultShipping;
        public final boolean defaultBilling;
        public final boolean addressFromCnC;
        public final String createdTmst;
        public final String modifiedTmst;
        public final String addressMode;

        @JsonCreator
        public AddressCreated(String accountId, String addressId, String firstName, String lastName, String address1,
                              String address2, String city, String state, String postalCode, String phone,
                              boolean defaultShipping, boolean defaultBilling, boolean addressFromCNC, String createdAt,
                              String modifiedAt, String addressMode) {
            this.accountId = accountId;
            this.addressId = addressId;
            this.firstName = firstName;
            this.lastName = lastName;
            this.address1 = address1;
            this.address2 = address2;
            this.city = city;
            this.state = state;
            this.postalCode = postalCode;
            this.phone = phone;
            this.defaultShipping = defaultShipping;
            this.defaultBilling = defaultBilling;
            this.addressFromCnC = addressFromCNC;
            this.createdTmst = createdAt;
            this.modifiedTmst = modifiedAt;
            this.addressMode = addressMode;
        }

        @Override
        public String getAddressId() {
            return addressId;
        }
    }

    @JsonTypeName(value = "address-updated")
    @Value
    @JsonDeserialize
    class AddressUpdated implements AddressEvents {
        public final String accountId;
        public final String addressId;
        public final String firstName;
        public final String lastName;
        public final String address1;
        public final String address2;
        public final String city;
        public final String state;
        public final String postalCode;
        public final String phone;
        public final boolean defaultShipping;
        public final boolean defaultBilling;
        public final boolean addressFromCNC;
        public final String createdAt;
        public final String modifiedAt;
        public final String addressMode;

        @JsonCreator
        public AddressUpdated(String accountId, String addressId, String firstName, String lastName, String address1,
                              String address2, String city, String state, String postalCode, String phone,
                              boolean defaultShipping, boolean defaultBilling, boolean addressFromCNC, String createdAt,
                              String modifiedAt, String addressMode) {
            this.accountId = accountId;
            this.addressId = addressId;
            this.firstName = firstName;
            this.lastName = lastName;
            this.address1 = address1;
            this.address2 = address2;
            this.city = city;
            this.state = state;
            this.postalCode = postalCode;
            this.phone = phone;
            this.defaultShipping = defaultShipping;
            this.defaultBilling = defaultBilling;
            this.addressFromCNC = addressFromCNC;
            this.createdAt = createdAt;
            this.modifiedAt = modifiedAt;
            this.addressMode = addressMode;
        }

        @Override
        public String getAddressId() {
            return addressId;
        }
    }

    @JsonTypeName(value = "address-deactivated")
    @Value
    @JsonDeserialize
    class AddressDeactivated implements AddressEvents {
        public final  String addressId;
        public final  String addressMode;
        public final  String timeStamp;

        @JsonCreator
        public AddressDeactivated(String addressId,String addressMode, String timeStamp) {
            this.addressId = addressId;
            this.addressMode = addressMode;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getAddressId() {
            return this.addressId;
        }
    }


}