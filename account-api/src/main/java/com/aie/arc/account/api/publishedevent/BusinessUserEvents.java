package com.aie.arc.account.api.publishedevent;

import com.aie.arc.account.api.request.dto.BusinessProfile;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

import java.util.List;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "eventType", defaultImpl = Void.class)
@JsonSubTypes({
        @JsonSubTypes.Type(BusinessUserEvents.BusinessUserCreated.class),
        @JsonSubTypes.Type(BusinessUserEvents.BusinessUserUpdated.class),
//        @JsonSubTypes.Type(BusinessUserEvents.BusinessUserProfileUpdated.class),
        @JsonSubTypes.Type(BusinessUserEvents.BusinessUserActivated.class),
        @JsonSubTypes.Type(BusinessUserEvents.BusinessUserDeactivated.class),
})
public interface BusinessUserEvents {

    String getAccountId();

    @JsonTypeName(value = "business-user-created")
    @Value
    @JsonDeserialize
    class BusinessUserCreated implements BusinessUserEvents {
        public final String businessUserId;
        public final BusinessProfile profile;
        public final String designation;
        public final List<String> roleIds;
        public final boolean active;
        public final String timeStamp;
        @JsonCreator
        public BusinessUserCreated(String businessUserId, BusinessProfile profile, String designation, List<String> roleIds, boolean active, String timeStamp) {
            this.businessUserId = businessUserId;
            this.profile = profile;
            this.designation = designation;
            this.roleIds = roleIds;
            this.active = active;
            this.timeStamp = timeStamp;
        }
        @Override
        public String getAccountId() {
            return this.businessUserId;
        }
    }

    @JsonTypeName(value = "business-user-updated")
    @Value
    @JsonDeserialize
    class BusinessUserUpdated implements BusinessUserEvents {
        public final String businessUserId;
        public final BusinessProfile profile;
        public final String designation;
        public final List<String> roleIds;
        public final boolean active;
        public final String timeStamp;
        @JsonCreator
        public BusinessUserUpdated(String businessUserId, BusinessProfile profile, String designation, List<String> roleIds, boolean active, String timeStamp) {
            this.businessUserId = businessUserId;
            this.profile = profile;
            this.designation = designation;
            this.roleIds = roleIds;
            this.active = active;
            this.timeStamp = timeStamp;
        }
        @Override
        public String getAccountId() {
            return this.businessUserId;
        }
    }
/*    @JsonTypeName(value = "business-user-profile-updated")
    @Value
    @JsonDeserialize
    class BusinessUserProfileUpdated implements BusinessUserEvents {
        public final String businessUserId;
        public final BusinessProfile profile;
        public final boolean active;
        public final String timeStamp;

        @JsonCreator
        public BusinessUserProfileUpdated(String businessUserId, BusinessProfile profile, boolean active, String timeStamp) {
            this.businessUserId = businessUserId;
            this.profile = profile;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getAccountId() {
            return this.businessUserId;
        }
    }*/

    @JsonTypeName(value = "business-user-activated")
    @Value
    @JsonDeserialize
    class BusinessUserActivated implements BusinessUserEvents {
        public final String businessUserId;
        public final BusinessProfile profile;
        public final String designation;
        public final List<String> roleIds;
        public final boolean active;
        public final String timeStamp;
        @JsonCreator
        public BusinessUserActivated(String businessUserId, BusinessProfile profile, String designation, List<String> roleIds, boolean active, String timeStamp) {
            this.businessUserId = businessUserId;
            this.profile = profile;
            this.designation = designation;
            this.roleIds = roleIds;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getAccountId() {
            return this.businessUserId;
        }
    }

    @JsonTypeName(value = "business-user-deactivated")
    @Value
    @JsonDeserialize
    class BusinessUserDeactivated implements BusinessUserEvents {
        public final String businessUserId;
        public final BusinessProfile profile;
        public final String designation;
        public final List<String> roleIds;
        public final boolean active;
        public final String timeStamp;
        @JsonCreator
        public BusinessUserDeactivated(String businessUserId, BusinessProfile profile, String designation, List<String> roleIds, boolean active, String timeStamp) {
            this.businessUserId = businessUserId;
            this.profile = profile;
            this.designation = designation;
            this.roleIds = roleIds;
            this.active = active;
            this.timeStamp = timeStamp;
        }
        @Override
        public String getAccountId() {
            return this.businessUserId;
        }
    }
}
