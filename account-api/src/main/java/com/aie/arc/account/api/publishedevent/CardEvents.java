package com.aie.arc.account.api.publishedevent;

import com.aie.arc.rbac.api.request.dto.CreatedAt;
import com.aie.arc.rbac.api.request.dto.LastModifiedAt;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.time.LocalDateTime;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", defaultImpl = Void.class)
@JsonSubTypes({
        @JsonSubTypes.Type(CardEvents.CardCreated.class),
        @JsonSubTypes.Type(CardEvents.CardUpdated.class),
        @JsonSubTypes.Type(CardEvents.CardDeactivated.class)
})
public interface CardEvents extends Jsonable {

    String getCardId();

    @JsonTypeName(value = "card-added")
    @Value
    @JsonDeserialize
    class CardCreated implements CardEvents {
        public final String accountId;
        public final String cardId;
        public final String firstName;
        public final String lastName;
        public final String cardType;
        public final String cardNumber;
        public final String expirationMonth;
        public final String expirationYear;
        public final boolean cardFromCNC;
        public final String cardMode;
        public final boolean defaultCreditCard;
        public final String createdAt;
        public final String lastModifiedAt;
        public final String subscriptionId;

        @JsonCreator
        public CardCreated(String accountId, String cardId, String firstName, String lastName, String cardType,
                           String cardNumber, String expirationMonth, String expirationYear, boolean cardFromCNC,
                           String cardMode, boolean defaultCreditCard, String createdAt, String lastModifiedAt,
                           String subscriptionId) {
            this.accountId = accountId;
            this.cardId = cardId;
            this.firstName = firstName;
            this.lastName = lastName;
            this.cardType = cardType;
            this.cardNumber = cardNumber;
            this.expirationMonth = expirationMonth;
            this.expirationYear = expirationYear;
            this.cardFromCNC = cardFromCNC;
            this.cardMode = cardMode;
            this.defaultCreditCard = defaultCreditCard;
            this.createdAt = createdAt;
            this.lastModifiedAt = lastModifiedAt;
            this.subscriptionId = subscriptionId;
        }

        @Override
        public String getCardId() {
            return cardId;
        }
    }

    @JsonTypeName(value = "card-updated")
    @Value
    @JsonDeserialize
    class CardUpdated implements CardEvents {
        public final String accountId;
        public final String cardId;
        public final String firstName;
        public final String lastName;
        public final String cardType;
        public final String cardNumber;
        public final String expirationMonth;
        public final String expirationYear;
        public final boolean cardFromCNC;
        public final String cardMode;
        public final boolean defaultCreditCard;
        public final String lastModifiedAt;

        @JsonCreator
        public CardUpdated(String accountId, String cardId, String firstName, String lastName, String cardType,
                           String cardNumber, String expirationMonth, String expirationYear, boolean cardFromCNC,
                           String cardMode, boolean defaultCreditCard, String lastModifiedAt) {
            this.accountId = accountId;
            this.cardId = cardId;
            this.firstName = firstName;
            this.lastName = lastName;
            this.cardType = cardType;
            this.cardNumber = cardNumber;
            this.expirationMonth = expirationMonth;
            this.expirationYear = expirationYear;
            this.cardFromCNC = cardFromCNC;
            this.cardMode = cardMode;
            this.defaultCreditCard = defaultCreditCard;
            this.lastModifiedAt = lastModifiedAt;
        }

        @Override
        public String getCardId() {
            return cardId;
        }
    }

    @JsonTypeName(value = "card-deactivated")
    @Value
    @JsonDeserialize
    class CardDeactivated implements CardEvents {
        public final String cardId;
        public final String cardMode;
        public final String timeStamp;

        @JsonCreator
        public CardDeactivated(String cardId,String cardMode, String timeStamp) {
            this.cardId = cardId;
            this.cardMode = cardMode;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getCardId() {
            return cardId;
        }
    }
}
