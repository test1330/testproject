package com.aie.arc.account.api.publishedevent;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Value;

import java.time.LocalDateTime;
import java.util.List;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", defaultImpl = Void.class)
@JsonSubTypes({
        @JsonSubTypes.Type(CustomerGroupEvents.CustomerGroupCreated.class),
        @JsonSubTypes.Type(CustomerGroupEvents.CustomerGroupUpdated.class),
        @JsonSubTypes.Type(CustomerGroupEvents.CustomerGroupActivated.class),
        @JsonSubTypes.Type(CustomerGroupEvents.CustomerGroupDeactivated.class)
})
public interface CustomerGroupEvents {

    String getCustomerGroupId();

    @JsonTypeName(value = "customerGroup-created")
    @Value
    class CustomerGroupCreated implements CustomerGroupEvents {
        private String customerGroupId;
        private List<String> customerIds;
        private int recency;
        private int frequency;
        private double monetary;
        private boolean active;
        private LocalDateTime timeStamp;

        @Override
        public String getCustomerGroupId() {
            return this.customerGroupId;
        }
    }

    @JsonTypeName(value = "customerGroup-updated")
    @Value
    class CustomerGroupUpdated implements CustomerGroupEvents {
        private String customerGroupId;
        private List<String> customerIds;
        private int recency;
        private int frequency;
        private double monetary;
        private boolean isActive;
        private LocalDateTime timeStamp;

        @Override
        public String getCustomerGroupId() {
            return this.customerGroupId;
        }
    }

    @JsonTypeName(value = "customerGroup-activated")
    @Value
    class CustomerGroupActivated implements CustomerGroupEvents {
        private String customerGroupId;
        private boolean active;
        private LocalDateTime timeStamp;

        @Override
        public String getCustomerGroupId() {
            return this.customerGroupId;
        }
    }

    @JsonTypeName(value = "customerGroup-deactivated")
    @Value
    class CustomerGroupDeactivated implements CustomerGroupEvents {
        private String customerGroupId;
        private boolean active;
        private LocalDateTime timeStamp;


        @Override
        public String getCustomerGroupId() {
            return this.customerGroupId;
        }
    }

}
