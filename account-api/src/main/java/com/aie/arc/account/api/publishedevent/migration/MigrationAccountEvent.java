package com.aie.arc.account.api.publishedevent.migration;

import com.aie.arc.account.api.publishedevent.migration.vo.Profile;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Value;


@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", defaultImpl = Void.class)
@JsonSubTypes({
        @JsonSubTypes.Type(MigrationAccountEvent.AccountAddressCreated.class),
        @JsonSubTypes.Type(MigrationAccountEvent.AccountAddressUpdated.class),
        @JsonSubTypes.Type(MigrationAccountEvent.AccountAddressUpdated.class),
        @JsonSubTypes.Type(MigrationAccountEvent.DefaultBillingAddressUpdatedToAccount.class),
        @JsonSubTypes.Type(MigrationAccountEvent.DefaultShippingAddressUpdatedToAccount.class),
        @JsonSubTypes.Type(MigrationAccountEvent.AccountCardCreated.class),
        @JsonSubTypes.Type(MigrationAccountEvent.AccountCardUpdated.class),
        @JsonSubTypes.Type(MigrationAccountEvent.AccountCardDeleted.class)

})
@JsonIgnoreProperties(value = { "accountId"})
public interface MigrationAccountEvent {

    String getAccountId();

    @JsonTypeName(value = "account-address-created")
    @Value
    class AccountAddressCreated implements MigrationAccountEvent {

        @JsonUnwrapped
        Profile profile;

        @Override
        public String getAccountId() {
            return this.profile.getUserId();
        }
    }

    @JsonTypeName(value = "account-address-updated")
    @Value
    class AccountAddressUpdated implements MigrationAccountEvent {

        @JsonUnwrapped
        Profile profile;

        @Override
        public String getAccountId() {
            return this.profile.getUserId();
        }
    }

    @JsonTypeName(value = "account-address-deleted")
    @Value
    class AccountAddressDeleted implements MigrationAccountEvent {

        @JsonUnwrapped
        Profile profile;

        @Override
        public String getAccountId() {
            return this.profile.getUserId();
        }
    }

    @JsonTypeName(value = "account-default-billing-address-updated")
    @Value
    class DefaultBillingAddressUpdatedToAccount implements MigrationAccountEvent {

        @JsonUnwrapped
        Profile profile;

        @Override
        public String getAccountId() {
            return this.profile.getUserId();
        }
    }

    @JsonTypeName(value = "account-default-shipping-address-updated")
    @Value
    class DefaultShippingAddressUpdatedToAccount implements MigrationAccountEvent {

        @JsonUnwrapped
        Profile profile;

        @Override
        public String getAccountId() {
            return this.profile.getUserId();
        }
    }

    @JsonTypeName(value = "account-card-created")
    @Value
    class AccountCardCreated implements MigrationAccountEvent {

        @JsonUnwrapped
        Profile profile;

        @Override
        public String getAccountId() {
            return this.profile.getUserId();
        }
    }

    @JsonTypeName(value = "account-card-updated")
    @Value
    class AccountCardUpdated implements MigrationAccountEvent {

        @JsonUnwrapped
        Profile profile;

        @Override
        public String getAccountId() {
            return this.profile.getUserId();
        }
    }

    @JsonTypeName(value = "account-default-card-selected")
    @Value
    class DefaultPaymentSeated implements MigrationAccountEvent {

        @JsonUnwrapped
        Profile profile;

        @Override
        public String getAccountId() {
            return this.profile.getUserId();
        }
    }

    @JsonTypeName(value = "account-card-deleted")
    @Value
    class AccountCardDeleted implements MigrationAccountEvent {

        @JsonUnwrapped
        Profile profile;

        @Override
        public String getAccountId() {
            return this.profile.getUserId();
        }
    }

}