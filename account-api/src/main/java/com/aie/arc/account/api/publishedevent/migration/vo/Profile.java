package com.aie.arc.account.api.publishedevent.migration.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Profile {

    String userId;
    String action;
    String login;
    String password;
    String passwordSalt;
    String firstName;
    String lastName;
    String email;
    String dateOfBirth;
    String myZip;
    String storeId;
    Boolean subscribed;
    String groupId;
    String shoppingFor;
    String aboutUs;
    String accountType;
    String defaultShippingAddress;
    String defaultBillingAddress;
    String defaultCreditCard;
    String priceList;
    String salePriceList;
    String lastPasswordUpdate;
    List<ProfileAddress> addresses;
    List<ProfileCard> cards;

}

