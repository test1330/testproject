package com.aie.arc.account.api.publishedevent.migration.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfileAddress {

    String addressId;
    String action;
    String firstName;
    String lastName;
    String address1;
    String address2;
    String city;
    String state;
    String postalCode;
    String country;
    String phoneNumber;
    String phoneNumberAlt;
    String fipsState;
    String fipsCounty;
    boolean avsValid;
    boolean skipAVSValidation;
}
