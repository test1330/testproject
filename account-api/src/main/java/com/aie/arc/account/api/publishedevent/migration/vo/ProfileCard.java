package com.aie.arc.account.api.publishedevent.migration.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfileCard {

    String paymentId;
    String action;
    String creditCardNumber;
    String creditCardType;
    String expirationMonth;
    String expirationYear;
    String firstName;
    String lastName;
    String subscriptionId;
    String cardNickName;

}
