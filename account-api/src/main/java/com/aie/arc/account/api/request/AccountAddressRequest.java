package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.annotation.*;
import com.aie.arc.account.api.common.request.BaseRequest;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class AccountAddressRequest extends BaseRequest {
    @ValidNameARC
    @NameFieldLength
    private String firstName;
    @ValidNameARC
    @NameFieldLength
    private String lastName;
    @NotBlankARC
    @AddressFieldLength
    private String address1;
    /*@AddressFieldLength*/
    private String address2;
    @NotBlankARC
    @CityFieldLength
    private String city;
    @NotBlankARC
   /* @StateFieldLength*/
    private String state;
    @NotBlankARC
    private String country;
    @NotBlankARC
    @PostalCodeFieldLength
    private String postalCode;
    @NotBlank
    @Pattern(regexp="(^$|\\([0-9]{3}\\)\\s?[0-9]{3}-[0-9]{4})" , message = "please enter valid mobile number in following pattern (xxx) xxx-xxxx")
    private String phone;
    private boolean defaultShipping;
    private boolean defaultBilling;
    private String fipscounty;
    private String fipsstate;
    private String phonenumberAlt;
    private boolean avsvalid;
    private boolean skipavsvalidation;
}
