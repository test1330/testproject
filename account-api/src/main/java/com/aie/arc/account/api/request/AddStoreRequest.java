package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import lombok.Getter;

@Getter
public class AddStoreRequest extends BaseRequest {
    private String storeId;
}
