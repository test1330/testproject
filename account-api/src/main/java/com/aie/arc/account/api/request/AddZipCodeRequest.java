package com.aie.arc.account.api.request;


import com.aie.arc.account.api.common.annotation.NotBlankARC;
import com.aie.arc.account.api.common.request.BaseRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.validation.constraints.Pattern;

@Getter
public final class AddZipCodeRequest extends BaseRequest {

    @NotBlankARC
    @Pattern(regexp="\\d{5}", message = "Error346: Please enter 5 digits ZipCode.")
    private String myZip;

}