package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import lombok.Getter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
public class BusinessAccountAddressRequest extends BaseRequest {
    private String flatNumber;
    private String apartmentNumber;
    private String building;
    private String apartment;
    private String streetName;
    private String streetNumber;
    private String additionalStreetInfo;
    @NotBlank
    private String zipCodeId;
    @NotBlank
    private String cityId;
    private String region;
    @NotBlank
    private String stateId;
    @NotBlank
    private String countryId;
    private String company;
    private String department;
    private String pOBox;
    @Pattern(regexp="(^$|[0-9]{10})" , message = "10 digits you need to enter for mobile")
    private String phone;
    @NotBlank
    @Pattern(regexp="(^$|[0-9]{10})" , message = "10 digits you need to enter for mobile")
    private String mobile;
    @NotBlank
    @Email
    private String email;
    private String fax;
    private String additionalAddressInfo;
    @NotNull
    private Boolean isShippingAddress;
    @NotNull
    private Boolean isBillingAddress;
    @NotNull
    private Boolean active;
}
