package com.aie.arc.account.api.request;

import com.aie.arc.account.api.request.brandsmart.AdditionalFormats;
import com.aie.arc.account.api.util.ExternalServiceUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;

@Data
@JsonDeserialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CMPWSApiServiceRequest {
    String orderNumber;
    String mop;
    String accountNumber;
    String expirationDate;
    String amount;
    String currencyCode;
    String transactionType;
    String accountNumberDesc;
    String actionCode;
    AdditionalFormats additionalFormats;

    @Override
    public String toString() {
        return "CMPWSApiServiceRequest{" +
                "orderNumber='" + orderNumber + '\'' +
                ", mop='" + mop + '\'' +
                ", accountNumber='" + ExternalServiceUtil.maskCreditCard(accountNumber) + '\'' +
                ", expirationDate='" + expirationDate + '\'' +
                ", amount='" + amount + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", accountNumberDesc='" + accountNumberDesc + '\'' +
                ", actionCode='" + actionCode + '\'' +
                ", additionalFormats=" + additionalFormats +
                '}';
    }
}
