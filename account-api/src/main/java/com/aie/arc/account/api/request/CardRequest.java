package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.annotation.*;
import com.aie.arc.account.api.common.request.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * The state for the {@link CardRequest} entity.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class CardRequest extends BaseRequest {
    @ValidNameARC
    @CardFieldLength
    private String firstName;
    @ValidNameARC
    @CardFieldLength
    private String lastName;
    private boolean defaultCreditCard;
    @NotBlankARC
    @Pattern(regexp="\\d+" , message = "InvalidCardFormat")
    @ToString.Exclude
    private String cardNumber;
    @NotBlankARC
    private String cardType;
    // @NotBlankCardExpirationDateARC
    private String expirationMonth;
    // @NotBlankCardExpirationDateARC
    private String expirationYear;
    @NotNull
    private Boolean active;

}