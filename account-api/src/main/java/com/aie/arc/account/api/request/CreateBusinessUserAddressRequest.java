package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.annotation.NotBlankARC;
import com.aie.arc.account.api.common.annotation.ValidNameARC;
import com.aie.arc.account.api.common.request.BaseRequest;
import lombok.Getter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
public class CreateBusinessUserAddressRequest extends BaseRequest {
    @ValidNameARC
    private String firstName;
    @ValidNameARC
    private String lastName;
    @NotBlankARC
    private String address1;
    private String address2;
    @NotBlankARC
    private String city;
    @NotBlankARC
    private String state;
    @NotBlankARC
    private String country;
    @NotBlankARC
    private String postalCode;
    @NotBlank
    @Pattern(regexp="(^$|\\([0-9]{3}\\)\\s?[0-9]{3}-[0-9]{4})" , message = "please enter valid mobile number in following pattern (xxx) xxx-xxxx")
    private String phone;
    private Boolean isResidenceAddress;
    private Boolean isOfficeAddress;
}
