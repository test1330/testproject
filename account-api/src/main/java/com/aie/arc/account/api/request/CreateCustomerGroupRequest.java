package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import com.aie.arc.account.api.common.request.LocalisedFields;
import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Getter
public class CreateCustomerGroupRequest extends BaseRequest {

    @NotNull
    private Map<@NotBlank String, @Valid LocalisedFields> localization;
    @NotNull
    private List<@NotBlank String> accountIds;
    @NotNull
    @Min(1)
    private Integer recency;
    @NotNull
    @Min(1)
    private Integer frequency;
    @NotNull
    @Digits(integer=6, fraction=2)
    private Double monetary;
    @NotNull
    private Boolean active;
}
