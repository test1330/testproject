package com.aie.arc.account.api.request;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

@JsonDeserialize
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class CreateCustomerRequest {
    String atgProfileId;
    String atgProfileUserType;
    String firstName;
    String lastName;
    String email;
}