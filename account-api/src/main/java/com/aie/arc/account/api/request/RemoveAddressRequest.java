package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
public class RemoveAddressRequest extends BaseRequest {
    @NotNull
    private List<String> addressIds;
}

