package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class SubscribeRequest extends BaseRequest {
    @NotNull
    private Boolean value;
}
