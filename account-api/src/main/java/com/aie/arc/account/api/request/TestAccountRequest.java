package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.annotation.CardFieldLength;
import com.aie.arc.account.api.common.annotation.NotBlankARC;
import com.aie.arc.account.api.common.annotation.ValidNameARC;
import com.aie.arc.account.api.common.request.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * The state for the {@link TestAccountRequest} entity.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestAccountRequest extends BaseRequest {
    private String userId;
    private String firstName;
    private String lastName;
    private String email;
    private String bmRoleId;
    private String accountType;
}