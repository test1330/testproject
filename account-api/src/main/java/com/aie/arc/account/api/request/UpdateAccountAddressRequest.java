package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.annotation.AddressFieldLength;
import com.aie.arc.account.api.common.annotation.NotBlankARC;
import com.aie.arc.account.api.common.annotation.ValidNameARC;
import com.aie.arc.account.api.common.request.BaseRequest;
import lombok.*;

import javax.validation.constraints.*;

@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UpdateAccountAddressRequest extends BaseRequest {
    @ValidNameARC
    @AddressFieldLength
    private String firstName;
    @ValidNameARC
    @AddressFieldLength
    private String lastName;
    @NotBlankARC
    private String address1;
    private String address2;
    @NotBlankARC
    private String city;
    @NotBlankARC
    private String state;
    @NotBlankARC
    private String country;
    @NotBlankARC
    private String postalCode;
    @Pattern(regexp="(^$|\\([0-9]{3}\\)\\s?[0-9]{3}-[0-9]{4})" , message = "please enter valid mobile number in following pattern (xxx) xxx-xxxx")
    private String phone;
    private boolean defaultShipping;
    private boolean defaultBilling;
    private String fipscounty;
    private String fipsstate;
    private String phonenumberAlt;
    private boolean avsvalid;
    private boolean skipavsvalidation;

}
