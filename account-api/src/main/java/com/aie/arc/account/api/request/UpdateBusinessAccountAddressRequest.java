package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import lombok.Getter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
public class UpdateBusinessAccountAddressRequest extends BaseRequest {
    private String flatNumber;
    private String apartmentNumber;
    private String building;
    private String apartment;
    private String streetName;
    private String streetNumber;
    private String additionalStreetInfo;
    private String zipCodeId;
    private String cityId;
    private String region;
    private String stateId;
    private String countryId;
    private String company;
    private String department;
    private String pOBox;
    private String phone;
    @Size(min = 10, max = 10)
    private String mobile;
    @Email
    private String email;
    private String fax;
    private String additionalAddressInfo;
    @NotNull
    private Boolean isShippingAddress;
    @NotNull
    private Boolean isBillingAddress;
    private Boolean active;
}
