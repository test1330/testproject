package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.annotation.NotBlankARC;
import com.aie.arc.account.api.common.annotation.ValidNameARC;
import com.aie.arc.account.api.common.request.BaseRequest;
import lombok.Getter;
import lombok.Value;

import javax.validation.constraints.*;

@Value
public class UpdateBusinessUserAddressRequest extends BaseRequest {
    @ValidNameARC
    private String firstName;
    @ValidNameARC
    private String lastName;
    @NotBlankARC
    private String address1;
    private String address2;
    @NotBlankARC
    private String city;
    @NotBlankARC
    private String state;
    @NotBlankARC
    private String country;
    @NotBlankARC
    private String postalCode;
    @Pattern(regexp = "(^$|\\([0-9]{3}\\)\\s?[0-9]{3}-[0-9]{4})", message = "please enter valid mobile number in following pattern (xxx) xxx-xxxx")
    private String phone;
    //In backend level we will map this to isShipping
    private Boolean isResidenceAddress;
    //In backend level we will map this to isBilling
    private Boolean isOfficeAddress;
}
