package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import com.aie.arc.account.api.request.dto.BusinessProfile;
import lombok.Getter;
import lombok.NonNull;

import javax.validation.Valid;

/**
 * Request for creating updating profile related information.
 */
@Getter
public class UpdateBusinessUserProfileRequest extends BaseRequest {
    @Valid
    @NonNull
    private BusinessProfile profile;
}
