package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import com.aie.arc.account.api.request.dto.BusinessProfile;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.validation.Valid;
import java.util.List;

/**
 * Request for creating updating profile related information.
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateBusinessUserRequest extends BaseRequest {
    @Valid
    @NonNull
    private BusinessProfile profile;
    private String designation;
    List<String> roleIds;
}
