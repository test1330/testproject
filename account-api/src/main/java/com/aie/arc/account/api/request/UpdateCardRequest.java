package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.annotation.AddressFieldLength;
import com.aie.arc.account.api.common.annotation.ValidNameARC;
import com.aie.arc.account.api.common.request.BaseRequest;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class UpdateCardRequest extends BaseRequest {

    @ValidNameARC
    @AddressFieldLength
    private String firstName;
    @ValidNameARC
    @AddressFieldLength
    private String lastName;
    private String expirationMonth;
    private String expirationYear;
    private boolean defaultCreditCard;

}
