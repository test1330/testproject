package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import com.aie.arc.account.api.common.request.LocalisedFields;
import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

@Getter
public class UpdateCustomerGroupRequest extends BaseRequest {
    private Map<@NotBlank String, @Valid LocalisedFields> localization;
    private List<@NotBlank String> accountIds;
    private Integer recency;
    private Integer frequency;
    private Double monetary;
    private Boolean active;
}
