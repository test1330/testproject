package com.aie.arc.account.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import com.aie.arc.account.api.request.dto.AccountProfile;
import lombok.Getter;
import lombok.NonNull;

import javax.validation.Valid;

@Getter
public class UpdateProfileRequest extends BaseRequest {
    @Valid
    @NonNull
    private AccountProfile profile;
}
