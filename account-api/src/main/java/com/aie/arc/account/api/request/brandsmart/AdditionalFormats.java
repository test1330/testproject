package com.aie.arc.account.api.request.brandsmart;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;

@Data
@JsonDeserialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdditionalFormats {
    String tokenType;
    String csv;

    @Override
    public String toString() {
        return "AdditionalFormats{" +
                "tokenType='" + tokenType + '\'' +
                ", fraudParameter=XXXX" +
                '}';
    }
}
