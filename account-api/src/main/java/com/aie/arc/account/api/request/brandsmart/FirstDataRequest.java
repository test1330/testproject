package com.aie.arc.account.api.request.brandsmart;

import com.aie.arc.account.api.util.ExternalServiceUtil;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FirstDataRequest {
    String orderNumber;
    String mop;
    String accountNumber;
    String expirationDate;
    String amount;
    String currencyCode;
    String transactionType;
    String accountNumberDesc;
    String actionCode;
    String paymentType;
    AdditionalFormats additionalFormats;

    @Override
    public String toString() {
        return "CMPWSApiServiceRequest{" +
                "orderNumber='" + orderNumber + '\'' +
                ", mop='" + mop + '\'' +
                ", accountNumber='" + ExternalServiceUtil.maskCreditCard(accountNumber) + '\'' +
                ", expirationDate='" + expirationDate + '\'' +
                ", amount='" + amount + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", accountNumberDesc='" + accountNumberDesc + '\'' +
                ", actionCode='" + actionCode + '\'' +
                ", additionalFormats=" + additionalFormats +
                '}';
    }
}
