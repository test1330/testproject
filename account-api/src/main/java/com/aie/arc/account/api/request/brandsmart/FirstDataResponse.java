package com.aie.arc.account.api.request.brandsmart;

import com.aie.arc.account.api.util.ExternalServiceUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@JsonDeserialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FirstDataResponse {
    String responseReasonCode;
    String authorizationCode;
    String avsResponseCode;
    String csvResponseCode;
    String accountNumber;
    String responseDate;
    String orderNumber;
    String amount;


    @Override
    public String toString() {
        return "CMPWSApiServiceResponse{" +
                "responseReasonCode='" + responseReasonCode + '\'' +
                ", authorizationCode='" + authorizationCode + '\'' +
                ", avsResponseCode='" + avsResponseCode + '\'' +
                ", csvResponseCode=XXXX" +
                ", accountNumber='" + ExternalServiceUtil.maskCreditCard(accountNumber) + '\'' +
                ", responseDate='" + responseDate + '\'' +
                ", orderNumber='" + orderNumber + '\'' +
                ", amount='" + amount + '\'' +
                '}';
    }
}
