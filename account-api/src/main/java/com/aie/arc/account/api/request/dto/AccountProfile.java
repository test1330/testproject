package com.aie.arc.account.api.request.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;


@Value
@JsonDeserialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountProfile {
    private String firstName;
    private String middleName;
    private String lastName;
    @JsonFormat(pattern = "MM/dd/yyyy")
    private String dateOfBirth;
    private String gender;
    @Email
    private String email;
    @Size(min = 10, max = 10)
    private String phone;
    private SocialIdentity socialIdentity;
    private String storeId;
    private String shoppingFor;
    private String aboutUs;

    public AccountProfile(String firstName, String middleName, String lastName, String dateOfBirth, String gender,
                          String email, String phone, SocialIdentity socialIdentity,String storeId,String shoppingFor,
                          String aboutUs){
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.socialIdentity = socialIdentity;
        this.storeId =  storeId;
        this.shoppingFor = shoppingFor;
        this.aboutUs = aboutUs;
    }
}
