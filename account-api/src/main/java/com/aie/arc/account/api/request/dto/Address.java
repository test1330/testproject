package com.aie.arc.account.api.request.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;
import java.time.LocalDateTime;

/**
 * The state for the {@link Address} entity.
 */
@SuppressWarnings("serial")
@Value
@JsonDeserialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public final class Address {
    private String addressId;
    private String accountId;
    private String firstName;
    private String lastName;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String country;
    private String postalCode;
    private String phone;
    private boolean defaultShipping;
    private boolean defaultBilling;
    private boolean active;
    private LocalDateTime timeStamp;
}