package com.aie.arc.account.api.request.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

/**
 * A value object class for storing a Business Account's profile related information.
 */
@SuppressWarnings("serial")
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BusinessProfile {
    private String firstName;
    private String userName;
    private String middleName;
    private String lastName;
    @Email
    private String email;
    @Size(min = 10, max = 10)
    private String phone;
}
