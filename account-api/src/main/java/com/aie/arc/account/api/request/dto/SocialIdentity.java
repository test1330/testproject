package com.aie.arc.account.api.request.dto;

import lombok.Value;

/**
 *
 */
@Value
public class SocialIdentity {
    private String provider;
    private String userId;
    private String authType;

    public SocialIdentity(String provider, String userId, String authType) {
        this.provider = provider;
        this.userId = userId;
        this.authType = authType;
    }
}
