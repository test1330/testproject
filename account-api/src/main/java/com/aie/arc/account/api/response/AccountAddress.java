package com.aie.arc.account.api.response;

import lombok.AccessLevel;
import lombok.Value;
import lombok.experimental.FieldDefaults;

@Value
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountAddress {
    String addressId;
    String accountId;
    String firstName;
    String lastName;
    String address1;
    String address2;
    String city;
    String state;
    String postalCode;
    String phone;
    boolean defaultShipping;
    boolean defaultBilling;
    boolean active;
}
