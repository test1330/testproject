package com.aie.arc.account.api.response;

import lombok.Value;

import java.util.List;

@Value
public class AccountDetail {
    private String accountId;
    private String bmcisProfileId;
    private Integer accountNumber;
    private ProfileDetails accountProfile;
    private List<String> listOfCardIds;
    private CardDetail defaultCardDetail;
    private List<String> addressIds;
    private AddressDetail defaultBillingAddress;
    private AddressDetail defaultShippingAddress;
    private String storeId;
    private String myZip;
    private boolean active;

}
