package com.aie.arc.account.api.response;

import lombok.Value;

import java.util.List;
@Value
public class AccountDetails {
    private List<AccountDetail> accountDetail;
    private Integer totalSize;
}
