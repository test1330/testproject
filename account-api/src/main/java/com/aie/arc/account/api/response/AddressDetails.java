package com.aie.arc.account.api.response;

import lombok.Value;

import java.util.List;

@Value
public class AddressDetails {
    private List<AddressDetail> addressDetail;
    private Integer totalSize;
}
