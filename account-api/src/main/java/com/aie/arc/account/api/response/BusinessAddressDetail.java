package com.aie.arc.account.api.response;

import lombok.Value;

@Value
public class BusinessAddressDetail {
    private String addressId;
    private String firstName;
    private String lastName;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String country;
    private String postalCode;
    private String phone;
    private boolean active;
}
