package com.aie.arc.account.api.response;

import lombok.Value;

import java.util.List;

@Value
public class BusinessAddressDetails {
    private List<BusinessAddressDetail> addressDetails;
    private Integer totalSize;
}
