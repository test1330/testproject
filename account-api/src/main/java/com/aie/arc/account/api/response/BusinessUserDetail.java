package com.aie.arc.account.api.response;

import com.aie.arc.rbac.api.response.RoleDetail;
import lombok.Value;

import java.util.List;

@Value
public class BusinessUserDetail {
    private String businessUserId;
    private String organizationId;
    private BusinessUserProfileDetails profile;
    private String createdAt;
    private String modifiedAt;
    private String designation;
    private List<RoleDetail> roles;
    private boolean active;
}
