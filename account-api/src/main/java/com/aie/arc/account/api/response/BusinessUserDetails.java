package com.aie.arc.account.api.response;

import lombok.Value;

import java.util.List;

@Value
public class BusinessUserDetails {
    private List<BusinessUserDetail> businessUserDetails;
    private Integer totalSize;
}
