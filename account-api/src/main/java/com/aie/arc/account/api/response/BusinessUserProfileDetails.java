package com.aie.arc.account.api.response;

import lombok.Value;

@Value
public class BusinessUserProfileDetails {
    private String firstName;
    private String middleName;
    private String lastName;
    private String userName;
    private String email;
    private String phone;
}
