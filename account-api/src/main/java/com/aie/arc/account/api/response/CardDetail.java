package com.aie.arc.account.api.response;

import lombok.Value;

@Value
public class CardDetail {
    private String cardId;
    private String firstName;
    private String lastName;
    private String cardNumber;
    private String cardType;
    private String expirationMonth;
    private String expirationYear;
    private boolean defaultCreditCard;
    private boolean active;

}
