package com.aie.arc.account.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

@Value
@JsonDeserialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateCustomerResponse {
    private String responseStatus;
    private int errorId;
    private String message;
    private int customerId;
}
