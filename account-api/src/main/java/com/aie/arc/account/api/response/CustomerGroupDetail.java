package com.aie.arc.account.api.response;

import com.aie.arc.account.api.common.request.LocalisedFields;
import lombok.Value;

import java.util.List;
import java.util.Map;

@Value
public class CustomerGroupDetail {
    private String customerGroupId;
    private Map<String, LocalisedFields> localizedFieldsVOMap;
    private List<String> customerIds;
    private Integer recency;
    private Integer frequency;
    private Double monetary;
    private boolean active;
}
