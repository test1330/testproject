package com.aie.arc.account.api.response;

import lombok.Value;

import java.util.List;

@Value
public class CustomerGroupDetails {
    private List<CustomerGroupDetail> customerGroupDetails;
    private Integer totalSize;
}
