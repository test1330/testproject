package com.aie.arc.account.api.response;

import com.aie.arc.rbac.api.response.GetRoleDetail;
import lombok.Value;

import java.util.List;

@Value
public class GetBusinessUserDetail {
    private String businessUserId;
    private String organizationId;
    private BusinessUserProfileDetails profile;
    private String designation;
    private List<GetRoleDetail> roles;
}
