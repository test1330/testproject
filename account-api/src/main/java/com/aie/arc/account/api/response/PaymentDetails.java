package com.aie.arc.account.api.response;

import lombok.AccessLevel;
import lombok.Value;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Value
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PaymentDetails {
    String creditCardDefaultId;
    List<CardDetail> creditCards;
}
