package com.aie.arc.account.api.response;

import com.aie.arc.account.api.request.dto.SocialIdentity;
import lombok.Value;


@Value
public class ProfileDetails {
    private String firstName;
    private String middleName;
    private String lastName;
    private String dateOfBirth;
    private String gender;
    private String email;
    private String phone;
    private SocialIdentity socialIdentity;
    private boolean emailVerified;
    private String createdAt;
    private String modifiedAt;
    private boolean subscribed;
    private String storeId;
    private String shoppingFor;
    private String aboutUs;


    public ProfileDetails(String firstName, String middleName, String lastName, String dateOfBirth, String gender,
                          String email, String phone, SocialIdentity socialIdentity, boolean emailVerified,
                          String createdAt, String modifiedAt,boolean subscribed,String storeId,String shoppingFor,
                          String aboutUs) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.socialIdentity = socialIdentity;
        this.emailVerified = emailVerified;
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
        this.subscribed = subscribed;
        this.storeId = storeId;
        this.shoppingFor = shoppingFor;
        this.aboutUs = aboutUs;
    }

}
