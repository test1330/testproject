package com.aie.arc.account.api.response;

import lombok.*;

import java.util.List;

@Value
public class SuggestedAddresses {
    private boolean hasAddressChangedOrImproved;
    List<SuggestedAddress> suggestedAddresses;
}
