package com.aie.arc.account.api.service;

import akka.NotUsed;
import com.aie.arc.account.api.common.request.ActivationRequest;
import com.aie.arc.account.api.common.request.SearchRequest;
import com.aie.arc.account.api.common.response.BaseResponse;
import com.aie.arc.account.api.common.serilization.ARCJacksonSerializerFactory;
import com.aie.arc.account.api.email.EmailRequest;
import com.aie.arc.account.api.publishedevent.AccountEvents;
import com.aie.arc.account.api.publishedevent.AddressEvents;
import com.aie.arc.account.api.publishedevent.BusinessUserEvents;
import com.aie.arc.account.api.publishedevent.CardEvents;
import com.aie.arc.account.api.publishedevent.migration.MigrationAccountEvent;
import com.aie.arc.account.api.request.*;
import com.aie.arc.account.api.request.brandsmart.FirstDataRequest;
import com.aie.arc.account.api.request.brandsmart.FirstDataResponse;
import com.aie.arc.account.api.response.*;
import com.aie.arc.externalservice.api.request.AvalaraRequest;
import com.aie.arc.externalservice.api.response.AvalaraResponse;
import com.aie.arc.externalservice.api.response.IpInfoServiceResponse;
import com.aie.arc.rbac.api.publishedevent.PermissionEvents;
import com.aie.arc.rbac.api.publishedevent.RoleEvents;
import com.aie.arc.rbac.api.request.*;
import com.aie.arc.rbac.api.response.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.broker.Topic;
import com.lightbend.lagom.javadsl.api.broker.kafka.KafkaProperties;
import com.lightbend.lagom.javadsl.api.transport.Method;

import java.util.List;

import static com.aie.arc.account.api.constants.AccountURIConstants.*;
import static com.aie.arc.account.api.constants.TopicIdConstants.*;
import static com.aie.arc.rbac.api.constants.RBACURIConstants.*;
import static com.aie.arc.rbac.api.constants.TopicIdConstants.PERMISSION_TOPIC_ID;
import static com.aie.arc.rbac.api.constants.TopicIdConstants.ROLE_TOPIC_ID;
import static com.lightbend.lagom.javadsl.api.Service.restCall;


/**
 * The AccountService service.
 * Manages the lifecycle of BusinessUser and Customer, as well properties on them.
 */
public interface AccountService extends Service {
    // Services for Customer Account.
    ServiceCall<NotUsed, AccountDetail> getAccount(String accountId);
    ServiceCall<NotUsed, String> getProfileZipCode(String accountId);
    ServiceCall<AddZipCodeRequest, BaseResponse> addZipCodeToAccount(String accountId);

    //ServiceCall<SubscribeRequest, BaseResponse> subscribe(String id);

    //services for Address.
    ServiceCall<AccountAddressRequest, SuggestedAddresses> validateAccountAddress(String accountId);
    ServiceCall<AccountAddressRequest, BaseResponse> addAccountAddress(String accountId);
    ServiceCall<UpdateAccountAddressRequest, BaseResponse> updateAccountAddress(String accountId, String addressId);
    ServiceCall<NotUsed, BaseResponse> setDefaultBillingAddress(String accountId, String addressId);
    ServiceCall<NotUsed, BaseResponse> setDefaultShippingAddress(String accountId, String addressId);
    ServiceCall<ActivationRequest, BaseResponse> activateAddress(String accountId, String id);
    ServiceCall<NotUsed, List<AccountAddress>> getAddresses(String accountId);
    ServiceCall<NotUsed, AddressDetail> getAccountAddress(String accountId, String addressId);

    //services for Card.
    ServiceCall<CardRequest, BaseResponse> addCard(String accountId);
    ServiceCall<UpdateCardRequest, BaseResponse> updateCard(String accountId,String cardId);
    ServiceCall<NotUsed, CardDetail> getCard(String accountId, String cardId);
    ServiceCall<NotUsed, PaymentDetails> getAccountCards(String accountId);
    ServiceCall<NotUsed, BaseResponse> setDefaultPayment(String accountId, String cardId);
    ServiceCall<ActivationRequest, BaseResponse> activateCard(String accountId,String card);
    ServiceCall<NotUsed, CardDetail> getDefaultPayment(String accountId);


    //Below Service used for validation purpose.
    ServiceCall<AvalaraRequest, AvalaraResponse> validateAddress();
    ServiceCall<NotUsed, IpInfoServiceResponse> validateIPAddress(String ipAddress);
    ServiceCall<EmailRequest, BaseResponse> sendTestEmail();
    // Testing purpose only
    ServiceCall<TestAccountRequest, BaseResponse> testCtAccount(String accountId);


    //Services for BusinessUserAccount

    ServiceCall<UpdateBusinessUserRequest, BaseResponse> updateBusinessUser(String businessUserId);
    ServiceCall<NotUsed, BusinessUserDetail> getBusinessUser(String businessUserId);
    ServiceCall<NotUsed, GetBusinessUserDetail> getBusinessUserDetails(String businessUserId);
    ServiceCall<SearchRequest, BusinessUserDetails> getBusinessUsers();
    ServiceCall<ActivationRequest, BaseResponse> activateBusinessUser(String id);



    // Commented Unused API's for BM project
    //services for CustomerGroup.
    /*ServiceCall<CreateCustomerGroupRequest, BaseResponse> createCustomerGroup();
    ServiceCall<UpdateCustomerGroupRequest, BaseResponse> updateCustomerGroup(String customerGroupId);
    ServiceCall<NotUsed, CustomerGroupDetail> getCustomerGroup(String customerGroupId);
    ServiceCall<SearchRequest, CustomerGroupDetails> getCustomerGroups();
    ServiceCall<ActivationRequest, BaseResponse> activateCustomerGroup(String id);*/

    //store
    ServiceCall<AddStoreRequest, BaseResponse> addStore(String accountId);
    ServiceCall<AddStoreRequest, BaseResponse> updateStore(String accountId);

    /*Customer group*/
    ServiceCall<RoleGroupRequest, BaseResponse> createRoleGroup();
    ServiceCall<RoleGroupRequest, BaseResponse> updateRoleGroup(String id);
    ServiceCall<NotUsed, RoleGroupDetail> getRoleGroup(String id);
    ServiceCall<SearchRequest, RoleGroupDetails> getRoleGroups();
    ServiceCall<ActivationRequest, BaseResponse> activateRoleGroup(String id);

    /*Role*/
    ServiceCall<RoleRequest, BaseResponse> createRole();
    ServiceCall<RoleRequest, BaseResponse> updateRole(String id);
    ServiceCall<NotUsed, RoleDetail> getRole(String id);
    ServiceCall<SearchRequest, RoleDetails> getRoles();
    ServiceCall<ActivationRequest, BaseResponse> activateRole(String id);

    // Commented Unused API's for BM project
    /*Microservice*/
   /* ServiceCall<MicroServiceRequest, BaseResponse> createMicroService();
    ServiceCall<MicroServiceRequest, BaseResponse> updateMicroService(String microServiceId);
    ServiceCall<NotUsed, MicroServiceDetail> getMicroService(String microServiceId);
    ServiceCall<SearchRequest, MicroServiceDetails> getMicroServices();
    ServiceCall<ActivationRequest, BaseResponse> activateMicroService(String id);

    *//*Api*//*
    ServiceCall<NotUsed, ApiDetail> getApi(String microServiceApiId, String apiId);
    ServiceCall<ApiRequest, BaseResponse> addApiToMicroService(String microServiceId);
    ServiceCall<ApiRequest, BaseResponse> updateApiToMicroService(String microServiceId, String microServiceApiId);
    ServiceCall<ActivationRequest, BaseResponse> activateApi(String id);
    ServiceCall<ActivationRequest, BaseResponse> activateApiFromMicroService(String microServiceId,String apiId);
    ServiceCall<SearchRequest, ApiDetails> getApisInMicroService(String microServiceId);*/

    /*Workspace*/
    ServiceCall<WorkspaceRequest, BaseResponse> createWorkspace();
    ServiceCall<UpdateWorkspaceRequest, BaseResponse> updateWorkspace(String workspaceId);
    ServiceCall<NotUsed, WorkspaceDetail> getWorkspace(String workspaceId);
    ServiceCall<SearchRequest, WorkspaceDetails> getAllWorkspace();
    ServiceCall<ActivationRequest,BaseResponse> deactivateWorkspace(String workspaceId);


    /*Permission*/
    ServiceCall<PermissionRequest, BaseResponse> createPermission();
    ServiceCall<PermissionRequest, BaseResponse> updatePermission(String permissionId);
    ServiceCall<NotUsed, PermissionDetailWithTag> getPermission(String permissionId);
    ServiceCall<ActivationRequest, BaseResponse> activatePermission(String id);
    ServiceCall<SearchRequest, PermissionDetails> getPermissions();

    ServiceCall<AssignRole, BaseResponse> assignRoleToUser(String userId);
    ServiceCall<AssignBusinessRole, BaseResponse> assignRoleToBusinessUser();
    ServiceCall<NotUsed, APITagDetail> getBusinessUserApis(String businessUserId);

    /*AccessRight*/
    ServiceCall<AccessRightRequest, BaseResponse> createAccessRight();
    ServiceCall<UpdateAccessRightRequest, BaseResponse> updateAccessRight(String accessId);
    ServiceCall<NotUsed, AccessRightDetail> getAccessRight(String accessId);
    ServiceCall<SearchRequest, AccessRightDetails> getAllAccessRights();
    ServiceCall<ActivationRequest,BaseResponse> deactivateAccessRight(String accessId);

    // Topic Events API
    Topic<AccountEvents> accountPublishedEvent();
    Topic<MigrationAccountEvent> accountMigratedPublishedEvent();
    Topic<AddressEvents> addressPublishedEvent();
    Topic<CardEvents> cardPublishedEvent();

    /*RBAC Events Api*/
    Topic<BusinessUserEvents> businessUserPublishedEvent();
    Topic<RoleEvents> rolePublishedEvent();
    Topic<PermissionEvents> permissionPublishedEvent();

    /*SOAP test services*/
    ServiceCall<CreateCustomerRequest, CreateCustomerResponse> validateCreateCustomerSOAPService();
    ServiceCall<CMPWSApiServiceRequest, CMPWSApiServiceResponse> validateSubscriptionSOAPService();


    @Override
    default Descriptor descriptor() {
        return Service.named("account").withCalls(

                //Account APIs
                restCall(Method.GET, GET_ACCOUNT_URI, this::getAccount),
                restCall(Method.GET, GET_PROFILE_ZIP_CODE_URI, this::getProfileZipCode),
                restCall(Method.PUT, SET_ZIP_CODE_URI, this::addZipCodeToAccount),
                //restCall(Method.PUT, SUBSCRIBE_URI, this::subscribe),


                //Address APIs.
                restCall(Method.POST, ADD_ACCOUNT_ADDRESS_URI, this::addAccountAddress),
                restCall(Method.POST, VALIDATE_ACCOUNT_ADDRESS_URI, this::validateAccountAddress),
                restCall(Method.PUT, UPDATE_ACCOUNT_ADDRESS_URI, this::updateAccountAddress),
                restCall(Method.PUT, SET_DEFAULT_BILLING_ADDRESS_URI, this::setDefaultBillingAddress),
                restCall(Method.PUT, SET_DEFAULT_SHIPPING_ADDRESS_URI, this::setDefaultShippingAddress),
                restCall(Method.GET, GET_ACCOUNT_ADDRESS_LIST_URI, this::getAddresses),
                restCall(Method.GET, GET_ACCOUNT_ADDRESS_URI, this::getAccountAddress),
                restCall(Method.PUT, ACTIVE_ADDRESS_URI, this::activateAddress),

                //Card APIs.
                restCall(Method.POST, ADD_CARD_URI, this::addCard),
                restCall(Method.PUT, UPDATE_CARD_URI, this::updateCard),
                restCall(Method.GET, GET_CARD_URI, this::getCard),
                restCall(Method.GET, GET_ACCOUNT_CARDS_URI, this::getAccountCards),
                restCall(Method.PUT, ACTIVE_CARD_URI, this::activateCard),
                restCall(Method.PUT, DEFAULT_PAYMENT_URI, this::setDefaultPayment),
                restCall(Method.GET, GET_DEFAULT_PAYMENT_URI, this::getDefaultPayment),

                //Email validation service
                restCall(Method.POST, ADDRESS_VALIDATION_URI, this::validateAddress),
                restCall(Method.GET, IP_INFO_VALIDATION_URI, this::validateIPAddress),
                restCall(Method.POST, CREATE_CUSTOMER_SOAP_SERVICE_TEST_URI, this::validateCreateCustomerSOAPService),
                restCall(Method.POST, SUBSCRIPTION_SOAP_SERVICE_TEST_URI, this::validateSubscriptionSOAPService),
                restCall(Method.POST, POST_SEND_EMAIL, this::sendTestEmail),
                restCall(Method.POST, POST_TEST_ACC_URI, this::testCtAccount),

                //BusinessUserAccount API.
                restCall(Method.PUT, UPDATE_BUSINESS_USER_URI, this::updateBusinessUser),
                restCall(Method.GET, GET_BUSINESS_USER_URI, this::getBusinessUser),
                restCall(Method.GET, GET_BUSINESS_USER_DETAIL_URI, this::getBusinessUserDetails),
                restCall(Method.POST, GET_BUSINESS_USER_LIST_URL, this::getBusinessUsers),
                restCall(Method.PUT, ACTIVE_BUSINESS_USER_URI, this::activateBusinessUser),

                // Commented Unused API's for BM project
                //customer group.
                /*restCall(Method.POST, CREATE_CUSTOMER_GROUP_URI, this::createCustomerGroup),
                restCall(Method.PUT, UPDATE_CUSTOMER_GROUP_URI, this::updateCustomerGroup),
                restCall(Method.GET, GET_CUSTOMER_GROUP_URI, this::getCustomerGroup),
                restCall(Method.POST, GET_CUSTOMER_GROUP_LIST_URI, this::getCustomerGroups),
                restCall(Method.PUT, ACTIVE_CUSTOMER_GROUP_URI, this::activateCustomerGroup),*/

                //Store
                restCall(Method.POST, ADD_STORE, this::addStore),
                restCall(Method.PUT, UPDATE_STORE, this::updateStore),

                //Role Group.
                restCall(Method.POST, CREATE_ROLE_URI, this::createRole),
                restCall(Method.PUT, UPDATE_ROLE_URI, this::updateRole),
                restCall(Method.GET, GET_ROLE_URI, this::getRole),
                restCall(Method.POST, GET_ROLE_LIST_URI, this::getRoles),
                restCall(Method.PUT, ACTIVE_ROLE_URI, this::activateRole),

                //Role API.
                restCall(Method.POST, CREATE_ROLE_GROUP_URI, this::createRoleGroup),
                restCall(Method.PUT, UPDATE_ROLE_GROUP_URI, this::updateRoleGroup),
                restCall(Method.GET, GET_ROLE_GROUP_URI, this::getRoleGroup),
                restCall(Method.POST, GET_ROLE_GROUP_LIST_URI, this::getRoleGroups),
                restCall(Method.PUT, ACTIVE_ROLE_GROUP_URI, this::activateRoleGroup),

                // Commented Unused API's for BM project
                //MicroService Api
                /*restCall(Method.POST, CREATE_MICRO_SERVICE_URI, this::createMicroService),
                restCall(Method.PUT, UPDATE_MICRO_SERVICE_URI, this::updateMicroService),
                restCall(Method.GET, GET_MICRO_SERVICE_URI, this::getMicroService),
                restCall(Method.POST, GET_MICRO_SERVICE_LIST_URI, this::getMicroServices),
                restCall(Method.PUT, ACTIVE_MICRO_SERVICE_URI, this::activateMicroService),
                restCall(Method.POST, ADD_API_TO_MICRO_SERVICE_URI, this::addApiToMicroService),
                restCall(Method.PUT, UPDATE_API_IN_MICRO_SERVICE_URI, this::updateApiToMicroService),
                restCall(Method.PUT, ACTIVE_API_URI, this::activateApiFromMicroService),
                restCall(Method.POST, GET_API_LIST_URI, this::getApisInMicroService),
                restCall(Method.GET, GET_API_URI, this::getApi),*/

                //Workspace
                restCall(Method.POST, CREATE_WORKSPACE_URI, this::createWorkspace),
                restCall(Method.PUT, UPDATE_WORKSPACE_URI, this::updateWorkspace),
                restCall(Method.GET, GET_WORKSPACE_URI, this::getWorkspace),
                restCall(Method.POST, GET_ALL_WORKSPACE_URI, this::getAllWorkspace),
                restCall(Method.PUT, DEACTIVATE_WORKSPACE_URI, this::deactivateWorkspace),

                //User Api
                restCall(Method.GET, GET_USER_API_URI, this::getBusinessUserApis),
                //Permission
                restCall(Method.POST, CREATE_PERMISSION_URI, this::createPermission),
                restCall(Method.PUT, UPDATE_PERMISSION_URI, this::updatePermission),
                restCall(Method.GET, GET_PERMISSION_URI, this::getPermission),
                restCall(Method.POST, GET_PERMISSION_LIST_URI, this::getPermissions),
                restCall(Method.PUT, ACTIVE_PERMISSION_URI, this::activatePermission),
                restCall(Method.POST, ASSIGN_ROLE_TO_USER_URI, this::assignRoleToUser),
                restCall(Method.POST, ASSIGN_ROLE_TO_BUSINESS_USER_URI, this::assignRoleToBusinessUser),


                //AccessRight
                restCall(Method.POST, CREATE_ACCESS_RIGHT_URI, this::createAccessRight),
                restCall(Method.PUT, UPDATE_ACCESS_RIGHT_URI, this::updateAccessRight),
                restCall(Method.GET, GET_ACCESS_RIGHT_URI, this::getAccessRight),
                restCall(Method.POST, GET_ACCESS_RIGHT_LIST_URI, this::getAllAccessRights),
                restCall(Method.PUT, ACTIVE_ACCESS_RIGHT_URI, this::deactivateAccessRight)


        )
               .withSerializerFactory(new ARCJacksonSerializerFactory(new ObjectMapper()))

                .withTopics(

                /*account*/
                Service.topic(ACCOUNT_TOPIC_ID, this::accountPublishedEvent)
                        .withProperty(KafkaProperties.partitionKeyStrategy(), AccountEvents::getAccountId),
                Service.topic(ADDRESS_TOPIC_ID, this::addressPublishedEvent)
                        .withProperty(KafkaProperties.partitionKeyStrategy(), AddressEvents::getAddressId),
                Service.topic(CARD_TOPIC_ID, this::cardPublishedEvent)
                        .withProperty(KafkaProperties.partitionKeyStrategy(), CardEvents::getCardId),

                Service.topic(ACCOUNT_SYNC_TOPIC_ID, this::accountMigratedPublishedEvent)
                        .withProperty(KafkaProperties.partitionKeyStrategy(), MigrationAccountEvent::getAccountId),
                Service.topic(BUSINESS_USER_TOPIC_ID, this::businessUserPublishedEvent)
                                .withProperty(KafkaProperties.partitionKeyStrategy(), BusinessUserEvents::getAccountId),
                Service.topic(ROLE_TOPIC_ID, this::rolePublishedEvent)
                        .withProperty(KafkaProperties.partitionKeyStrategy(), RoleEvents::getRoleId),
                Service.topic(PERMISSION_TOPIC_ID, this::permissionPublishedEvent)
                        .withProperty(KafkaProperties.partitionKeyStrategy(), PermissionEvents::getPermissionId)
                ).withAutoAcl(true);
    }
}
