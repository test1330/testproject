package com.aie.arc.account.api.service;

import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.broker.Topic;

public interface KafkaService extends Service {

    Topic<String> usersTopic();
    Topic<String> profileSyncForMigrationTopic();
    Topic<String> businessUsersTopic();
    Topic<String> accountMigrationTopic();
    Topic<String> checkoutPaymentTopic();
    Topic<String> checkoutAddressTopic();

    String USERS_TOPIC_NAME = "arc_ProfileSync";
    String BUSINESS_USERS_TOPIC_NAME = "bu_arc_ProfileSync";
    String PROFILE_SYNC_MIGRATION_TOPIC_NAME = "ProfileSyncForMigration";
    String ACCOUNT_MIGRATION_TOPIC_NAME = "atg_accountMigration";
    String CHECKOUT_PAYMENT_TOPIC = "cnc-profile-payment";
    String CHECKOUT_ADDRESS_TOPIC = "cnc-profile-address";

    @Override
    default Descriptor descriptor() {
        return Service.named("kafkaService").withTopics(
                Service.topic(USERS_TOPIC_NAME, this::usersTopic),
                Service.topic(BUSINESS_USERS_TOPIC_NAME, this::businessUsersTopic),
                Service.topic(PROFILE_SYNC_MIGRATION_TOPIC_NAME, this::profileSyncForMigrationTopic),
                Service.topic(ACCOUNT_MIGRATION_TOPIC_NAME, this::accountMigrationTopic),
                Service.topic(CHECKOUT_PAYMENT_TOPIC, this::checkoutPaymentTopic),
                Service.topic(CHECKOUT_ADDRESS_TOPIC, this::checkoutAddressTopic)
        ).withAutoAcl(true);
    }
}
