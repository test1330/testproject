package com.aie.arc.account.api.subscribe.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CncCard {
    private String cardId;
    private String accountId;
    private String firstName;
    private String lastName;
    private boolean defaultCreditCard;
    private String cardNumber;
    private String cardType;
    private String subscriptionId;
    private String expirationMonth;
    private String expirationYear;
    private String cardNickName;
    private boolean active;

}

