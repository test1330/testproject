package com.aie.arc.account.api.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

@Slf4j
public class ExternalServiceUtil {
    ExternalServiceUtil() {}
    public static final String MASK_LETTER = "XXXXXXXXXXXX";
    public static final int NUM_4 = 4;
    public static String maskCreditCard(String cardNumber) {
        if (StringUtils.isNotBlank(cardNumber) && cardNumber.length() > NUM_4) {
            return MASK_LETTER + cardNumber.substring(cardNumber.length() - NUM_4);
        }
        return cardNumber;
    }
}
