package com.aie.arc.rbac.api.constants;

public class    RBACURIConstants {
    //Role
    public static final String CREATE_ROLE_URI = "/api/v1/roles";
    public static final String UPDATE_ROLE_URI = "/api/v1/roles/:roleId";
    public static final String GET_ROLE_URI = "/api/v1/roles/:roleId";
    public static final String GET_ROLE_LIST_URI = "/api/v1/roles/search";
    public static final String ACTIVE_ROLE_URI = "/api/v1/roles/:roleId/active";

    public static final String ROLE_CREATED_MESSAGE = "Role created";
    public static final String ROLE_UPDATED_MESSAGE = "Role updated";
    public static final String ROLE_ACTIVATE_MESSAGE = "Role Activated";
    public static final String ROLE_DEACTIVATE_MESSAGE = "Role Deactivated";

    //Role Group
    public static final String CREATE_ROLE_GROUP_URI = "/api/v1/role-groups";
    public static final String UPDATE_ROLE_GROUP_URI = "/api/v1/role-groups/:roleGroupId";
    public static final String GET_ROLE_GROUP_URI = "/api/v1/role-groups/:roleGroupId";
    public static final String GET_ROLE_GROUP_LIST_URI = "/api/v1/role-groups/search";
    public static final String ACTIVE_ROLE_GROUP_URI = "/api/v1/role-groups/:roleGroupId/active";

    public static final String ROLE_GROUP_CREATED_MESSAGE = "Role Group created";
    public static final String ROLE_GROUP_UPDATED_MESSAGE = "Role Group updated";
    public static final String ROLE_GROUP_ACTIVATE_MESSAGE = "Role Group Activated";
    public static final String ROLE_GROUP_DEACTIVATE_MESSAGE = "Role Group Deactivated";

    //MicroService
    public static final String CREATE_MICRO_SERVICE_URI = "/api/v1/micro-services";
    public static final String UPDATE_MICRO_SERVICE_URI = "/api/v1/micro-services/:microServiceId";
    public static final String GET_MICRO_SERVICE_URI = "/api/v1/micro-services/:microServiceId";
    public static final String GET_MICRO_SERVICE_LIST_URI = "/api/v1/micro-services/search";
    public static final String ACTIVE_MICRO_SERVICE_URI = "/api/v1/micro-services/:microServiceId/active";
    public static final String MICRO_SERVICE_CREATED_MESSAGE = "MicroService created";
    public static final String MICRO_SERVICE_UPDATED_MESSAGE = "MicroService updated";
    public static final String MICRO_SERVICE_ACTIVATE_MESSAGE = "MicroService Activated";
    public static final String MICRO_SERVICE_DEACTIVATE_MESSAGE = "MicroService Deactivated";

    public static final String GET_API_URI = "/api/v1/micro-services/:microServiceId/apis/:apiId";
    public static final String ACTIVE_API_URI = "/api/v1/micro-services/:microServiceId/apis/:apiId/active";
    public static final String ADD_API_TO_MICRO_SERVICE_URI = "/api/v1/micro-services/:microServiceId/apis";
    public static final String UPDATE_API_IN_MICRO_SERVICE_URI = "/api/v1/micro-services/:microServiceId/apis/:apiId";
    public static final String GET_API_LIST_URI = "/api/v1/micro-services/:microServiceId/apis/search";

    public static final String ADD_API_CREATED_MESSAGE = "Api added to Given MicroService";
    public static final String ADD_API_UPDATED_MESSAGE = "Api updated in Given MicroService";
    public static final String API_ACTIVATE_MESSAGE = "Api Activated from Given MicroService";
    public static final String API_DEACTIVATE_MESSAGE = "Api Deactivated from Given MicroService";

    //Permission
    public static final String CREATE_PERMISSION_URI = "/api/v1/permissions";
    public static final String UPDATE_PERMISSION_URI = "/api/v1/permissions/:permissionId";
    public static final String ACTIVE_PERMISSION_URI = "/api/v1/permissions/:permissionId/active";
    public static final String GET_PERMISSION_URI = "/api/v1/permissions/:permissionId";
    public static final String GET_PERMISSION_LIST_URI = "/api/v1/permissions/search";
    public static final String PERMISSION_CREATE_MESSAGE = "Permission Created";
    public static final String PERMISSION_UPDATE_MESSAGE = "Permission Updated";
    public static final String PERMISSION_ACTIVATED_MESSAGE = "Permission Activated";
    public static final String PERMISSION_DEACTIVATED_MESSAGE = "Permission Deactivated";
    //API tags
    public static final String GET_USER_API_URI = "/api/v1/business-users/apitags/:businessuserid";

    // Workspace
    public static final String CREATE_WORKSPACE_URI = "/api/v1/workspace";
    public static final String UPDATE_WORKSPACE_URI = "/api/v1/workspace/:workspaceId";
    public static final String GET_WORKSPACE_URI = "/api/v1/workspace/:workspaceId";
    public static final String GET_ALL_WORKSPACE_URI = "/api/v1/workspace/search";
    public static final String DEACTIVATE_WORKSPACE_URI = "/api/v1/workspace/:workspaceId/active";
    public static final String ACTIVATE_WORKSPACE_URI = "/api/v1/workspace/:workspaceId/active";
    public static final String WORKSPACE_CREATE_MESSAGE = "Workspace Created";
    public static final String WORKSPACE_UPDATE_MESSAGE = "Workspace Updated";
    public static final String WORKSPACE_DEACTIVATED_MESSAGE = "Workspace Deactivated";
    public static final String WORKSPACE_ACTIVATED_MESSAGE = "Workspace Activated";

    //User
    public static final String ASSIGN_ROLE_TO_USER_URI = "/api/v1/user/:userId";
    public static final String ROLE_ASSIGN_MESSAGE = "Role Assign To Given User";
    public static final String ASSIGN_ROLE_TO_BUSINESS_USER_URI = "/api/v1/business-users/assign-roles";
    public static final String ROLE_ASSIGN_TO_BUSINESS_MESSAGE = "Role Assign To Business User";

    //Access Right
    public static final String CREATE_ACCESS_RIGHT_URI = "/api/v1/access-rights";
    public static final String UPDATE_ACCESS_RIGHT_URI = "/api/v1/access-rights/:accessId";
    public static final String GET_ACCESS_RIGHT_URI = "/api/v1/access-rights/:accessId";
    public static final String GET_ACCESS_RIGHT_LIST_URI = "/api/v1/access-rights/search";
    public static final String ACTIVE_ACCESS_RIGHT_URI = "/api/v1/access-rights/:accessId/active";
    public static final String ACCESS_RIGHT_CREATED_MESSAGE = "Access Right Created";
    public static final String ACCESS_RIGHT_UPDATED_MESSAGE = "Access Right Updated";
    public static final String ACCESS_RIGHT_DEACTIVATED_MESSAGE = "Access Right Deactivated";
    public static final String ACCESS_RIGHT_ACTIVATED_MESSAGE = "Access Right Activated";

}