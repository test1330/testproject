package com.aie.arc.rbac.api.constants;

public class TopicIdConstants {
    /*rbac*/
    public final static String ROLE_GROUP_TOPIC_ID = "rolegroup-roleGroupPublishedEvent";
    public final static String ROLE_TOPIC_ID = "role-rolePublishedEvent";
    public final static String MICRO_SERVICE_TOPIC_ID = "microService-microservicePublishedEvent";
    public final static String API_TOPIC_ID = "api-apiPublishedEvent";
    public final static String PERMISSION_TOPIC_ID = "permission-permissionPublishedEvent";

}
