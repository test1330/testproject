package com.aie.arc.rbac.api.publishedevent;

import com.aie.arc.rbac.api.request.dto.LocalizedFieldsRequest;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

import java.time.LocalDateTime;
import java.util.Map;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", defaultImpl = Void.class)
@JsonSubTypes({
        @JsonSubTypes.Type(ApiEvents.ApiCreated.class),
        @JsonSubTypes.Type(ApiEvents.ApiUpdated.class),
        @JsonSubTypes.Type(ApiEvents.ApiActivated.class),
        @JsonSubTypes.Type(ApiEvents.ApiDeactivated.class)
})
public interface ApiEvents {

    String getApiId();

    @JsonTypeName(value = "api-created")
    @Value
    @JsonDeserialize
    class ApiCreated implements ApiEvents {
        public final String id;
        public final Map<String, LocalizedFieldsRequest> localizedFieldsMap;
        public final boolean active;
        public final LocalDateTime timeStamp;
        @JsonCreator
        public ApiCreated(String id, Map<String, LocalizedFieldsRequest> localizedFieldsMap, boolean active, LocalDateTime timeStamp) {
            this.id = id;
            this.localizedFieldsMap = localizedFieldsMap;
            this.active = active;
            this.timeStamp = timeStamp;
        }
        @Override
        public String getApiId() {
            return this.id;
        }
    }

    @JsonTypeName(value = "api-updated")
    @Value
    @JsonDeserialize
    class ApiUpdated implements ApiEvents {
        public final String id;
        public final Map<String, LocalizedFieldsRequest> localizedFieldsMap;
        public final boolean active;
        public final LocalDateTime timeStamp;
        @JsonCreator
        public ApiUpdated(String id, Map<String, LocalizedFieldsRequest> localizedFieldsMap, boolean active, LocalDateTime timeStamp) {
            this.id = id;
            this.localizedFieldsMap = localizedFieldsMap;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getApiId() {
            return this.id;
        }
    }

    @JsonTypeName(value = "api-activated")
    @Value
    @JsonDeserialize
    class ApiActivated implements ApiEvents {
        public final String id;
        public final boolean active;
        public final LocalDateTime timeStamp;
        @JsonCreator
        public ApiActivated(String id, boolean active, LocalDateTime timeStamp) {
            this.id = id;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getApiId() {
            return this.id;
        }
    }

    @JsonTypeName(value = "api-deactivated")
    @Value
    @JsonDeserialize
    class ApiDeactivated implements ApiEvents {
        public final String id;
        public final boolean active;
        public final LocalDateTime timeStamp;
        @JsonCreator
        public ApiDeactivated(String id, boolean active, LocalDateTime timeStamp) {
            this.id = id;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getApiId() {
            return this.id;
        }
    }
}