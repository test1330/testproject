package com.aie.arc.rbac.api.publishedevent;

import com.aie.arc.rbac.api.request.dto.LocalizedFieldsRequest;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", defaultImpl = Void.class)
@JsonSubTypes({
        @JsonSubTypes.Type(MicroServiceEvents.MicroServiceCreated.class),
        @JsonSubTypes.Type(MicroServiceEvents.MicroServiceUpdated.class),
        @JsonSubTypes.Type(MicroServiceEvents.MicroServiceActivated.class),
        @JsonSubTypes.Type(MicroServiceEvents.MicroServiceDeactivated.class)
})
public interface MicroServiceEvents {


    String getMicroServiceId();

    @JsonTypeName(value = "micro-service-created")
    @Value
    @JsonDeserialize
    class MicroServiceCreated implements MicroServiceEvents {
        public final String microServiceId;
        public final Map<String, LocalizedFieldsRequest> localizedFieldsMap;
        public final List<String> apiIds;
        public final boolean active;
        public final LocalDateTime timestamp;
        @JsonCreator
        public MicroServiceCreated(String microServiceId, Map<String, LocalizedFieldsRequest> localizedFieldsMap, List<String> apiIds, boolean active, LocalDateTime timestamp) {
            this.microServiceId = microServiceId;
            this.localizedFieldsMap = localizedFieldsMap;
            this.apiIds = apiIds;
            this.active = active;
            this.timestamp = timestamp;
        }

        @Override
        public String getMicroServiceId() {
            return this.microServiceId;
        }
    }

    @JsonTypeName(value = "micro-service-updated")
    @Value
    @JsonDeserialize
    class MicroServiceUpdated implements MicroServiceEvents {
        public final String microServiceId;
        public final Map<String, LocalizedFieldsRequest> localizedFieldsMap;
        public final List<String> apiIds;
        public final boolean active;
        public final LocalDateTime timestamp;
        @JsonCreator
        public MicroServiceUpdated(String microServiceId, Map<String, LocalizedFieldsRequest> localizedFieldsMap, List<String> apiIds, boolean active, LocalDateTime timestamp) {
            this.microServiceId = microServiceId;
            this.localizedFieldsMap = localizedFieldsMap;
            this.apiIds = apiIds;
            this.active = active;
            this.timestamp = timestamp;
        }

        @Override
        public String getMicroServiceId() {
            return this.microServiceId;
        }
    }

    @JsonTypeName(value = "micro-service-activated")
    @Value
    @JsonDeserialize
    class MicroServiceActivated implements MicroServiceEvents {
        public final String microServiceId;
        public final boolean active;
        public final LocalDateTime timeStamp;
        @JsonCreator
        public MicroServiceActivated(String microServiceId, boolean active, LocalDateTime timeStamp) {
            this.microServiceId = microServiceId;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getMicroServiceId() {
            return this.microServiceId;
        }
    }

    @JsonTypeName(value = "micro-service-deactivated")
    @Value
    @JsonDeserialize
    class MicroServiceDeactivated implements MicroServiceEvents {
        public final String microServiceId;
        public final boolean active;
        public final LocalDateTime timeStamp;
        @JsonCreator
        public MicroServiceDeactivated(String microServiceId, boolean active, LocalDateTime timeStamp) {
            this.microServiceId = microServiceId;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getMicroServiceId() {
            return this.microServiceId;
        }
    }
}
