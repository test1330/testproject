package com.aie.arc.rbac.api.publishedevent;

import com.aie.arc.rbac.api.request.dto.LocalizedFieldsRequest;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

import java.util.List;
import java.util.Map;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "eventType", defaultImpl = Void.class)
@JsonSubTypes({
        @JsonSubTypes.Type(PermissionEvents.PermissionCreated.class),
        @JsonSubTypes.Type(PermissionEvents.PermissionUpdated.class),
        @JsonSubTypes.Type(PermissionEvents.PermissionActivated.class),
        @JsonSubTypes.Type(PermissionEvents.PermissionDeactivated.class)
})
public interface PermissionEvents {

    String getPermissionId();

    @JsonTypeName(value = "permission-created")
    @Value
    @JsonDeserialize
    class PermissionCreated implements PermissionEvents {
        public final String permissionId;
        public final Map<String, LocalizedFieldsRequest> localizedFieldsMap;
        public final List<String> apiTag;
        public final boolean active;
        public final String timeStamp;
        @JsonCreator
        public PermissionCreated(String permissionId, Map<String, LocalizedFieldsRequest> localizedFieldsMap, List<String> apiTag, boolean active, String timeStamp) {
            this.permissionId = permissionId;
            this.localizedFieldsMap = localizedFieldsMap;
            this.apiTag = apiTag;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getPermissionId() {
            return this.permissionId;
        }
    }

    @JsonTypeName(value = "permission-updated")
    @Value
    @JsonDeserialize
    class PermissionUpdated implements PermissionEvents {
        public final String permissionId;
        public final Map<String, LocalizedFieldsRequest> localizedFieldsMap;
        public final List<String> apiTag;
        public final boolean active;
        public final String timeStamp;
        @JsonCreator
        public PermissionUpdated(String permissionId, Map<String, LocalizedFieldsRequest> localizedFieldsMap, List<String> apiTag, boolean active, String timeStamp) {
            this.permissionId = permissionId;
            this.localizedFieldsMap = localizedFieldsMap;
            this.apiTag = apiTag;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getPermissionId() {
            return this.permissionId;
        }
    }

    @JsonTypeName(value = "permission-activated")
    @Value
    @JsonDeserialize
    class PermissionActivated implements PermissionEvents {
        public final String permissionId;
        public final Map<String, LocalizedFieldsRequest> localizedFieldsMap;
        public final List<String> apiTag;
        public final boolean active;
        public final String timeStamp;
        @JsonCreator
        public PermissionActivated(String permissionId, Map<String, LocalizedFieldsRequest> localizedFieldsMap, List<String> apiTag, boolean active, String timeStamp) {
            this.permissionId = permissionId;
            this.localizedFieldsMap = localizedFieldsMap;
            this.apiTag = apiTag;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getPermissionId() {
            return this.permissionId;
        }
    }

    @JsonTypeName(value = "permission-deactivated")
    @Value
    @JsonDeserialize
    class PermissionDeactivated implements PermissionEvents {
        public final String permissionId;
        public final Map<String, LocalizedFieldsRequest> localizedFieldsMap;
        public final List<String> apiTag;
        public final boolean active;
        public final String timeStamp;
        @JsonCreator
        public PermissionDeactivated(String permissionId, Map<String, LocalizedFieldsRequest> localizedFieldsMap, List<String> apiTag, boolean active, String timeStamp) {
            this.permissionId = permissionId;
            this.localizedFieldsMap = localizedFieldsMap;
            this.apiTag = apiTag;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getPermissionId() {
            return this.permissionId;
        }
    }

}
