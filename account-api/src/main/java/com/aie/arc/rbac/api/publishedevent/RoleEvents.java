package com.aie.arc.rbac.api.publishedevent;

import com.aie.arc.rbac.api.request.dto.LocalizedFieldsRequest;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

import java.util.List;
import java.util.Map;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "eventType", defaultImpl = Void.class)
@JsonSubTypes({
        @JsonSubTypes.Type(RoleEvents.RoleCreated.class),
        @JsonSubTypes.Type(RoleEvents.RoleUpdated.class),
        @JsonSubTypes.Type(RoleEvents.RoleActivated.class),
        @JsonSubTypes.Type(RoleEvents.RoleDeactivated.class)
})
public interface RoleEvents {

    String getRoleId();

    @JsonTypeName(value = "role-created")
    @Value
    @JsonDeserialize
    class RoleCreated implements RoleEvents {
        public final String roleId;
        public final String roleGroupId;
        public final List<String> permissionIds;
        public final Map<String, LocalizedFieldsRequest> localizedFieldsMap;
        public final boolean active;
        public final String timeStamp;
        @JsonCreator
        public RoleCreated(String roleId, String roleGroupId, List<String> permissionIds, Map<String, LocalizedFieldsRequest> localizedFieldsMap, boolean active, String timeStamp) {
            this.roleId = roleId;
            this.roleGroupId = roleGroupId;
            this.permissionIds = permissionIds;
            this.localizedFieldsMap = localizedFieldsMap;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getRoleId() {
            return this.roleId;
        }
    }

    @JsonTypeName(value = "role-updated")
    @Value
    @JsonDeserialize
    class RoleUpdated implements RoleEvents {
        public final String roleId;
        public final String roleGroupId;
        public final List<String> permissionIds;
        public final Map<String, LocalizedFieldsRequest> localizedFieldsMap;
        public final boolean active;
        public final String timeStamp;
        @JsonCreator
        public RoleUpdated(String roleId, String roleGroupId, List<String> permissionIds, Map<String, LocalizedFieldsRequest> localizedFieldsMap, boolean active, String timeStamp) {
            this.roleId = roleId;
            this.roleGroupId = roleGroupId;
            this.permissionIds = permissionIds;
            this.localizedFieldsMap = localizedFieldsMap;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getRoleId() {
            return this.roleId;
        }
    }

    @JsonTypeName(value = "role-activated")
    @Value
    @JsonDeserialize
    class RoleActivated implements RoleEvents {
        public final String roleId;
        public final String roleGroupId;
        public final List<String> permissionIds;
        public final Map<String, LocalizedFieldsRequest> localizedFieldsMap;
        public final boolean active;
        public final String timeStamp;
        @JsonCreator
        public RoleActivated(String roleId, String roleGroupId, List<String> permissionIds, Map<String, LocalizedFieldsRequest> localizedFieldsMap, boolean active, String timeStamp) {
            this.roleId = roleId;
            this.roleGroupId = roleGroupId;
            this.permissionIds = permissionIds;
            this.localizedFieldsMap = localizedFieldsMap;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getRoleId() {
            return this.roleId;
        }
    }

    @JsonTypeName(value = "role-deactivated")
    @Value
    @JsonDeserialize
    class RoleDeactivated implements RoleEvents {
        public final String roleId;
        public final String roleGroupId;
        public final List<String> permissionIds;
        public final Map<String, LocalizedFieldsRequest> localizedFieldsMap;
        public final boolean active;
        public final String timeStamp;
        @JsonCreator
        public RoleDeactivated(String roleId, String roleGroupId, List<String> permissionIds, Map<String, LocalizedFieldsRequest> localizedFieldsMap, boolean active, String timeStamp) {
            this.roleId = roleId;
            this.roleGroupId = roleGroupId;
            this.permissionIds = permissionIds;
            this.localizedFieldsMap = localizedFieldsMap;
            this.active = active;
            this.timeStamp = timeStamp;
        }
        @Override
        public String getRoleId() {
            return this.roleId;
        }
    }

}
