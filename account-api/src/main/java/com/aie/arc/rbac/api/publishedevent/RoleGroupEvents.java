package com.aie.arc.rbac.api.publishedevent;

import com.aie.arc.rbac.api.request.dto.LocalizedFieldsRequest;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

import java.time.LocalDateTime;
import java.util.Map;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", defaultImpl = Void.class)
@JsonSubTypes({
        @JsonSubTypes.Type(RoleGroupEvents.RoleGroupCreated.class),
        @JsonSubTypes.Type(RoleGroupEvents.RoleGroupUpdated.class),
        @JsonSubTypes.Type(RoleGroupEvents.RoleGroupActivated.class),
        @JsonSubTypes.Type(RoleGroupEvents.RoleGroupDeactivated.class)


})
public interface RoleGroupEvents {

    String getRoleGroupId();

    @JsonTypeName(value = "role-group-created")
    @Value
    @JsonDeserialize
    class RoleGroupCreated implements RoleGroupEvents {
        public final String roleGroupId;
        public final Map<String, LocalizedFieldsRequest> localizedFieldsMap;
        public final boolean active;
        public final LocalDateTime timeStamp;
        @JsonCreator
        public RoleGroupCreated(String roleGroupId, Map<String, LocalizedFieldsRequest> localizedFieldsMap, boolean active, LocalDateTime timeStamp) {
            this.roleGroupId = roleGroupId;
            this.localizedFieldsMap = localizedFieldsMap;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getRoleGroupId() {
            return this.roleGroupId;
        }
    }

    @JsonTypeName(value = "role-group-updated")
    @Value
    @JsonDeserialize
    class RoleGroupUpdated implements RoleGroupEvents {
        public final String roleGroupId;
        public final Map<String, LocalizedFieldsRequest> localizedFieldsMap;
        public final boolean active;
        public final LocalDateTime timeStamp;
        @JsonCreator
        public RoleGroupUpdated(String roleGroupId, Map<String, LocalizedFieldsRequest> localizedFieldsMap, boolean active, LocalDateTime timeStamp) {
            this.roleGroupId = roleGroupId;
            this.localizedFieldsMap = localizedFieldsMap;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getRoleGroupId() {
            return this.roleGroupId;
        }
    }

    @JsonTypeName(value = "role-group-activate")
    @Value
    @JsonDeserialize
    class RoleGroupActivated implements RoleGroupEvents {
        public final String roleGroupId;
        public final boolean active;
        public final LocalDateTime timeStamp;
        @JsonCreator
        public RoleGroupActivated(String roleGroupId, boolean active, LocalDateTime timeStamp) {
            this.roleGroupId = roleGroupId;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getRoleGroupId() {
            return this.roleGroupId;
        }
    }

    @JsonTypeName(value = "role-group-deactivate")
    @Value
    @JsonDeserialize
    class RoleGroupDeactivated implements RoleGroupEvents {
        public final String roleGroupId;
        public final boolean active;
        public final LocalDateTime timeStamp;
        @JsonCreator
        public RoleGroupDeactivated(String roleGroupId, boolean active, LocalDateTime timeStamp) {
            this.roleGroupId = roleGroupId;
            this.active = active;
            this.timeStamp = timeStamp;
        }

        @Override
        public String getRoleGroupId() {
            return this.roleGroupId;
        }
    }


}
