package com.aie.arc.rbac.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AddApiToRoleRequest extends BaseRequest {
    @NotNull
    @NotEmpty
    private Map<@NotBlank String, @NotNull List<@NotBlank String>> apiRoles;
    @NotNull
    private Boolean active;
}