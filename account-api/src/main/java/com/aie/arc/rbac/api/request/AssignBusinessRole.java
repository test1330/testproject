package com.aie.arc.rbac.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AssignBusinessRole extends BaseRequest {
    @NotNull
    List<@NotBlank String> roleIds;
    @NotNull
     String userId;

}
