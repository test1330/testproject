package com.aie.arc.rbac.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
@Getter
public class AssignRole extends BaseRequest {
    @NotNull
    List<@NotBlank String> roleIds;
    @NotNull
    private Boolean active;
}
