package com.aie.arc.rbac.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import com.aie.arc.account.api.common.request.LocalisedFields;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Map;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class LocaleRequest extends BaseRequest {
    @NotBlank
    private String code;
    @NotBlank
    private Map<String, @Valid LocalisedFields> localization;
}
