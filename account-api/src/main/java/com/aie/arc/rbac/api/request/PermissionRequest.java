package com.aie.arc.rbac.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import com.aie.arc.account.api.common.request.LocalisedFields;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;


@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PermissionRequest extends BaseRequest {
    @NotNull
    @NotEmpty
    private Map<@NotBlank String, @Valid LocalisedFields> localization;
    @NotNull
    @NotEmpty
    private String accessRightId;
    @NotNull
    @NotEmpty
    private String workspaceId;
    @NotNull
    @NotEmpty
    private List<String> apiTags;
    @NotNull
    private Boolean active;
}
