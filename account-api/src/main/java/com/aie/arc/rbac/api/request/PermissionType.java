package com.aie.arc.rbac.api.request;

public enum  PermissionType {
    Workspace, Workflow
}
