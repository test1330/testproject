package com.aie.arc.rbac.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
public class RemoveRoleRequest extends BaseRequest {
    @NotNull
    @NotEmpty
    private List<@NotBlank String> roleIds;
}
