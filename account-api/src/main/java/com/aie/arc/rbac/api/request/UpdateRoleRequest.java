package com.aie.arc.rbac.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
public class UpdateRoleRequest extends BaseRequest {
    @NotNull
    private List<String> roleIds;
    @NotNull
    private Boolean active;
}
