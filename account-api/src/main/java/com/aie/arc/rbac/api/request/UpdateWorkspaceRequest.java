package com.aie.arc.rbac.api.request;

import com.aie.arc.account.api.common.request.BaseRequest;
import com.aie.arc.account.api.common.request.LocalisedFields;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateWorkspaceRequest extends BaseRequest {

    @NotNull
    @NotEmpty
    private Map<@NotBlank String, @Valid LocalisedFields> localization;
    private String module;
    private PermissionType permissionType;
    private Boolean active;
}
