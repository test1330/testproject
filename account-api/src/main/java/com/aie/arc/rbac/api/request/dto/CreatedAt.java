package com.aie.arc.rbac.api.request.dto;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class CreatedAt {

    private LocalDateTime createdAt;

    public CreatedAt(){
        createdAt = null;
    }

    public CreatedAt(LocalDateTime createdAt){
        this.createdAt = createdAt;
    }

}
