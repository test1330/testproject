package com.aie.arc.rbac.api.request.dto;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class LastModifiedAt {
    private LocalDateTime lastModifiedAt;

    public LastModifiedAt(){
        lastModifiedAt = null;
    }

    public LastModifiedAt(LocalDateTime lastModifiedAt){
        this.lastModifiedAt = lastModifiedAt;
    }

}
