package com.aie.arc.rbac.api.request.dto;

import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Value
public class LocalizedFieldsRequest {
    @NotBlank
    @Size(max = 60)
    private String name;
    @NotBlank
    @Size(max = 200)
    private String description;
}
