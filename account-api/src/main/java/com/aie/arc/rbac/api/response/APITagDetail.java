package com.aie.arc.rbac.api.response;

import lombok.Value;

import java.util.Set;

@Value
public class APITagDetail {
        Set<String> apiTags;
}
