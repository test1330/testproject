package com.aie.arc.rbac.api.response;

import com.aie.arc.account.api.common.request.LocalisedFields;
import lombok.Value;

import java.util.Map;

@Value
public class AccessRightDetail {

    String accessId;
    Map<String, LocalisedFields> localisedFieldsMap;
    Boolean active;
}
