package com.aie.arc.rbac.api.response;

import lombok.Value;

import java.util.List;

@Value
public class AccessRightDetails {

    List<AccessRightDetail> accessRightDetails;
    Integer totalSize;
}

