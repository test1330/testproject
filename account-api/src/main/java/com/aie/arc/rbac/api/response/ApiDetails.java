package com.aie.arc.rbac.api.response;

import lombok.Value;

import java.util.List;
@Value
public class ApiDetails {
    private List<ApiDetail> apiDetails;
    private Integer totalSize;
}
