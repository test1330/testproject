package com.aie.arc.rbac.api.response;

import com.aie.arc.rbac.api.request.PermissionType;
import lombok.Value;

@Value
public class GetPermissionDetail {

     String permissionId;
     String module;
     PermissionType permissionType;
     Boolean active;
}
