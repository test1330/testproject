package com.aie.arc.rbac.api.response;

import com.aie.arc.account.api.common.request.LocalisedFields;
import lombok.Value;

import java.util.List;
import java.util.Map;

@Value
public class GetRoleDetail {
    private String roleId;
    Map<String, LocalisedFields> localisedFieldsMap;
    List<GetPermissionDetail> permissions;
    private String roleGroupId;
    private Boolean active;


    public GetRoleDetail(String roleId, Map<String, LocalisedFields> localisedFieldsMap, List<GetPermissionDetail> permissions, String roleGroupId, Boolean active)
    {

        this.roleId = roleId;
        this.localisedFieldsMap = localisedFieldsMap;
        this.permissions = permissions;
        this.roleGroupId = roleGroupId;
        this.active = active;
    }

}
