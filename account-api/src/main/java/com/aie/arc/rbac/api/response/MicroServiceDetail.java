package com.aie.arc.rbac.api.response;

import com.aie.arc.account.api.common.request.LocalisedFields;
import lombok.Value;

import java.util.List;
import java.util.Map;

@Value
public class MicroServiceDetail {
   private String microServiceId;
   Map<String, LocalisedFields> localisedFieldsMap;
   private List<String> apiIds;
   private Boolean active;
}
