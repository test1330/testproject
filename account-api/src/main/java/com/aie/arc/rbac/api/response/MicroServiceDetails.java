package com.aie.arc.rbac.api.response;

import lombok.Value;

import java.util.List;
@Value
public class MicroServiceDetails {
    private List<MicroServiceDetail> microServiceDetails;
    private Integer totalSize;
}
