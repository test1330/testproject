package com.aie.arc.rbac.api.response;

import com.aie.arc.account.api.common.request.LocalisedFields;
import lombok.Value;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

@Value
public class PermissionDetail {

     String permissionId;
     Map<@NotBlank String, @Valid LocalisedFields> localization;
     AccessRightDetail accessRight;
     WorkspaceDetail workspace;
     String workspaceId;
     Boolean active;
}
