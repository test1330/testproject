package com.aie.arc.rbac.api.response;

import lombok.Value;

import java.util.List;

@Value
public class PermissionDetails {
    private List<PermissionDetailWithTag> permissionDetails;
    private Integer totalSize;
}
