package com.aie.arc.rbac.api.response;

import com.aie.arc.account.api.common.request.LocalisedFields;
import lombok.Value;

import java.util.List;
import java.util.Map;

@Value
public class RoleDetail {
    private String roleId;
    Map<String, LocalisedFields> localisedFieldsMap;
    List<String> permissionIds;
    List<PermissionDetail> permissions;
    private String roleGroupId;
    private Boolean active;


    public RoleDetail(String roleId, Map<String, LocalisedFields> localisedFieldsMap, List<String> permissionIds, List<PermissionDetail> permissions, String roleGroupId, Boolean active)
    {

        this.roleId = roleId;
        this.localisedFieldsMap = localisedFieldsMap;
        this.permissionIds = permissionIds;
        this.permissions = null;
        this.roleGroupId = roleGroupId;
        this.active = active;
    }

    public RoleDetail(String roleId, Map<String, LocalisedFields> localisedFieldsMap, List<PermissionDetail> permissions, String roleGroupId, Boolean active)
    {

        this.roleId = roleId;
        this.localisedFieldsMap = localisedFieldsMap;
        this.permissionIds = null;
        this.permissions = permissions;
        this.roleGroupId = roleGroupId;
        this.active = active;
    }
}
