package com.aie.arc.rbac.api.response;

import lombok.Value;

import java.util.List;
@Value
public class RoleDetails {
    private List<RoleDetail> roleDetails;
    private Integer totalSize;
}
