package com.aie.arc.rbac.api.response;

import com.aie.arc.account.api.common.request.LocalisedFields;
import lombok.Value;

import java.util.Map;

@Value
public class RoleGroupDetail {
    private String roleGroupId;
    Map<String, LocalisedFields> localisedFieldsMap;
    private Boolean active;
}
