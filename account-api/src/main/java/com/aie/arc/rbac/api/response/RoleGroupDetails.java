package com.aie.arc.rbac.api.response;

import lombok.Value;

import java.util.List;
@Value
public class RoleGroupDetails {
    private List<RoleGroupDetail> roleGroupDetails;
    private Integer totalSize;
}
