package com.aie.arc.rbac.api.response;

import com.aie.arc.account.api.common.request.LocalisedFields;
import com.aie.arc.rbac.api.request.PermissionType;
import lombok.Value;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Value
public class WorkspaceDetail {

     String workspaceId;
     Map<@NotBlank String, @Valid LocalisedFields> localization;
     String module;
     PermissionType permissionType;
     Boolean active;







}
