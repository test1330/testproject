package com.aie.arc.rbac.api.response;

import lombok.Value;

import java.util.List;

@Value
public class WorkspaceDetails {
    private List<WorkspaceDetail> workspaceDetails;
    private Integer totalSize;

}
