package com.aie.arc.account.impl.command;

import akka.Done;
import akka.actor.typed.ActorRef;
import com.aie.arc.account.impl.entity.entities.Account;
import com.aie.arc.account.impl.entity.entities.vo.Profile;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Data;
import lombok.Value;

import java.util.Optional;

public interface AccountCommand extends Jsonable {

    @Value
    @JsonDeserialize
    final class RegisterAccount implements AccountCommand {
        public final  Account account;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public RegisterAccount(Account account, ActorRef<Done> replyTo) {
            this.account = account;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateAccount implements AccountCommand {
       public final  Account account;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateAccount(Account account, ActorRef<Done> replyTo) {
            this.account = account;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateProfile implements AccountCommand {
        public final String accountId;
        public final Profile profile;
        public final ActorRef<Done> replyTo;
        @JsonCreator
        public UpdateProfile(String accountId, Profile profile, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.profile = profile;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class AddZipCode implements AccountCommand {
        public final String accountId;
        public final String myZip;
        public final ActorRef<Done> replyTo;
        @JsonCreator
        public AddZipCode(String accountId, String myZip, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.myZip = myZip;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class AddAddressToAccount implements AccountCommand {
        public final String accountId;
        public final String addressId;
        public final String addressZipCode;
        public final boolean defaultShipping;
        public final boolean defaultBilling;
        public final boolean migratedEvent;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public AddAddressToAccount(String accountId, String addressId, String addressZipCode, boolean defaultShipping, boolean defaultBilling, boolean migratedEvent, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.addressId = addressId;
            this.addressZipCode = addressZipCode;
            this.defaultShipping = defaultShipping;
            this.defaultBilling = defaultBilling;
            this.migratedEvent = migratedEvent;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateAddressToAccount implements AccountCommand {
        public final String accountId;
        public final String addressId;
        public final String addressZipCode;
        public final boolean defaultShipping;
        public final boolean defaultBilling;
        public final boolean migratedEvent;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateAddressToAccount(String accountId, String addressId, String addressZipCode, boolean defaultShipping, boolean defaultBilling, boolean migratedEvent, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.addressId = addressId;
            this.addressZipCode = addressZipCode;
            this.defaultShipping = defaultShipping;
            this.defaultBilling = defaultBilling;
            this.migratedEvent = migratedEvent;
            this.replyTo = replyTo;
        }
    }
    @Value
    @JsonDeserialize
    final class DeactivateAddressToAccount implements AccountCommand {
        public final String accountId;
        public final String addressId;
        public final boolean migratedEvent;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public DeactivateAddressToAccount(String accountId, String addressId, boolean migratedEvent, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.addressId = addressId;
            this.migratedEvent = migratedEvent;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class AddCardToAccount implements AccountCommand {
        public final String accountId;
        public final String cardId;
        public final boolean defaultCreditCard;
        public final boolean migratedEvent;
        public final ActorRef<Done> replyTo;


        @JsonCreator
        public AddCardToAccount(String accountId, String cardId, boolean defaultCreditCard, boolean migratedEvent, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.cardId = cardId;
            this.defaultCreditCard = defaultCreditCard;
            this.migratedEvent = migratedEvent;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateCardToAccount implements AccountCommand {
        public final String accountId;
        public final String cardId;
        public final boolean defaultCreditCard;
        public final boolean migratedEvent;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateCardToAccount(String accountId, String cardId, boolean defaultCreditCard, boolean migratedEvent, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.cardId = cardId;
            this.defaultCreditCard = defaultCreditCard;
            this.migratedEvent = migratedEvent;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class DeactivateAccountCard implements AccountCommand {
        public final String accountId;
        public final String cardId;
        public final boolean value;
        public final boolean migratedEvent;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public DeactivateAccountCard(String accountId, String cardId, boolean value, boolean migratedEvent, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.cardId = cardId;
            this.value = value;
            this.migratedEvent = migratedEvent;
            this.replyTo = replyTo;
        }
    }


    @Value
    @JsonDeserialize
    final class UpdateDefaultShippingAddress implements AccountCommand {
        public final String accountId;
        public final String shippingAddressId;
        public final String billingAddressId;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateDefaultShippingAddress(String accountId, String shippingAddressId, String billingAddressId, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.shippingAddressId = shippingAddressId;
            this.billingAddressId = billingAddressId;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateDefaultBillingAddress implements AccountCommand {
        public final String accountId;
        public final String billingAddressId;
        public final String shippingAddressId;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateDefaultBillingAddress(String accountId, String billingAddressId, String shippingAddressId, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.billingAddressId = billingAddressId;
            this.shippingAddressId = shippingAddressId;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class AddStoreToAccount implements AccountCommand {
        public final String accountId;
        public final String storeId;
        public final ActorRef<Done> replyTo;
        @JsonCreator
        public AddStoreToAccount(String accountId, String storeId, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.storeId = storeId;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateStoreToAccount implements AccountCommand {
        public final String accountId;
        public final String storeId;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateStoreToAccount(String accountId, String storeId, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.storeId = storeId;
            this.replyTo = replyTo;
        }
    }

    @Value
    @Data
    @JsonDeserialize
    final class ActivateAccount implements AccountCommand {
        public final String accountId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public ActivateAccount(String accountId, boolean value, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class DeactivateAccount implements AccountCommand {
        public final String accountId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public DeactivateAccount(String accountId, boolean value, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class Subscribe implements AccountCommand {
        public final String accountId;
        public final  boolean value;
        public final ActorRef<Done> replyTo;
        @JsonCreator
        public Subscribe(String accountId, boolean value, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class Unsubscribe implements AccountCommand {
        public final String accountId;
        public final boolean value;
        public final ActorRef<Done> replyTo;
        @JsonCreator
        public Unsubscribe(String accountId, boolean value, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class SetDefaultPayment implements AccountCommand {
        public final String accountId;
        public final String cardId;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public SetDefaultPayment(String accountId, String cardId, ActorRef<Done> replyTo) {
            this.accountId = accountId;
            this.cardId = cardId;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class GetAccount implements AccountCommand {
        public final ActorRef<Optional<Account>> replyTo;

        @JsonCreator
        public GetAccount(ActorRef<Optional<Account>> replyTo) {
            this.replyTo = replyTo;
        }
    }
}
