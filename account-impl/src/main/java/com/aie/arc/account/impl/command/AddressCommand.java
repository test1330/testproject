package com.aie.arc.account.impl.command;

import akka.Done;
import akka.actor.typed.ActorRef;
import com.aie.arc.account.impl.entity.entities.Address;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.util.Optional;

public interface AddressCommand  extends Jsonable {
    @Value
    @JsonDeserialize
    final class CreateAddress implements AddressCommand {
        public final  Address address;
        public final ActorRef<Done> replyTo;
        @JsonCreator
        public CreateAddress(Address address, ActorRef<Done> replyTo) {
            this.address = address;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateAddress implements AddressCommand {
        public final Address address;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateAddress(Address address, ActorRef<Done> replyTo) {
            this.address = address;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class ActivateAddress implements AddressCommand {
        public final String addressId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public ActivateAddress(String addressId, boolean value, ActorRef<Done> replyTo) {
            this.addressId = addressId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class DeactivateAddress implements AddressCommand {
        public final String addressId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public DeactivateAddress(String addressId, boolean value, ActorRef<Done> replyTo) {
            this.addressId = addressId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }


    @Value
    @JsonDeserialize
    final class GetAddress implements AddressCommand {
        public final ActorRef<Optional<Address>> replyTo;

        @JsonCreator
        public GetAddress(ActorRef<Optional<Address>> replyTo) {
            this.replyTo = replyTo;
        }
    }
}
