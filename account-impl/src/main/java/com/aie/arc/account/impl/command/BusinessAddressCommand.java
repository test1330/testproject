package com.aie.arc.account.impl.command;

import akka.Done;
import akka.actor.typed.ActorRef;
import com.aie.arc.account.impl.entity.entities.BusinessAddress;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.util.Optional;

public interface BusinessAddressCommand extends Jsonable {
    @Value
    @JsonDeserialize
    final class CreateAddress implements BusinessAddressCommand {
        public final  BusinessAddress address;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public CreateAddress(BusinessAddress address, ActorRef<Done> replyTo) {
            this.address = address;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateAddress implements BusinessAddressCommand {
        public final  BusinessAddress address;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateAddress(BusinessAddress address, ActorRef<Done> replyTo) {
            this.address = address;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class ActivateAddress implements BusinessAddressCommand {
        public final  String addressId;
        public final  boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public ActivateAddress(String addressId, boolean value, ActorRef<Done> replyTo) {
            this.addressId = addressId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class DeactivateAddress implements BusinessAddressCommand {
        public final  String addressId;
        public final  boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public DeactivateAddress(String addressId, boolean value, ActorRef<Done> replyTo) {
            this.addressId = addressId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class GetAddress implements BusinessAddressCommand {
        public final ActorRef<Optional<BusinessAddress>> replyTo;

        @JsonCreator
        public GetAddress(ActorRef<Optional<BusinessAddress>> replyTo) {
            this.replyTo = replyTo;
        }
    }
}
