package com.aie.arc.account.impl.command;

import akka.Done;
import akka.actor.typed.ActorRef;
import com.aie.arc.account.impl.entity.entities.BusinessUser;
import com.aie.arc.account.impl.entity.entities.vo.BusinessProfile;
import com.aie.arc.account.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.account.impl.entity.entities.vo.LastModifiedAt;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.util.List;
import java.util.Optional;

public interface BusinessUserCommand extends Jsonable {
    @Value
    @JsonDeserialize
    final class CreateBusinessUser implements BusinessUserCommand {
        BusinessUser businessUser;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public CreateBusinessUser(BusinessUser businessUser, ActorRef<Done> replyTo) {
            this.businessUser = businessUser;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateBusinessUser implements BusinessUserCommand {
        public final String businessUserId;
        public final BusinessUser businessUser;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateBusinessUser(String businessUserId, BusinessUser businessUser, ActorRef<Done> replyTo) {
            this.businessUserId = businessUserId;
            this.businessUser = businessUser;
            this.replyTo = replyTo;
        }
    }
    

    @Value
    @JsonDeserialize
    final class ActivateBusinessUser implements BusinessUserCommand {
        public final String businessUserId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public ActivateBusinessUser(String businessUserId, boolean value, ActorRef<Done> replyTo) {
            this.businessUserId = businessUserId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class DeactivateBusinessUser implements BusinessUserCommand {
        public final String businessUserId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public DeactivateBusinessUser(String businessUserId, boolean value, ActorRef<Done> replyTo) {
            this.businessUserId = businessUserId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class AssignRoleToBusinessUser implements BusinessUserCommand {
        public final String userId;
        public final List<String> roleIds;
        public final CreatedAt createdAt;
        public final LastModifiedAt lastModifiedAt;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public AssignRoleToBusinessUser(String userId, List<String> roleIds, CreatedAt createdAt, LastModifiedAt lastModifiedAt, ActorRef<Done> replyTo) {
            this.userId = userId;
            this.roleIds = roleIds;
            this.createdAt = createdAt;
            this.lastModifiedAt = lastModifiedAt;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class GetBusinessUser implements BusinessUserCommand, PersistentEntity.ReplyType<Optional<BusinessUser>> {
        public final ActorRef<Optional<BusinessUser>> replyTo;

        @JsonCreator
        public GetBusinessUser(ActorRef<Optional<BusinessUser>> replyTo) {
            this.replyTo = replyTo;
        }
    }
}
