package com.aie.arc.account.impl.command;

import akka.Done;
import akka.actor.typed.ActorRef;
import com.aie.arc.account.impl.entity.entities.Card;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.util.Optional;

public interface CardCommand extends Jsonable {

    @Value
    @JsonDeserialize
    final class AddCard implements CardCommand {
        public final Card card;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public AddCard(Card card, ActorRef<Done> replyTo) {
            this.card = card;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateCard implements CardCommand {
        public final Card card;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateCard(Card card, ActorRef<Done> replyTo) {
            this.card = card;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class ActivateCard implements CardCommand {
        public final String cardId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public ActivateCard(String cardId, boolean value, ActorRef<Done> replyTo) {
            this.cardId = cardId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class DeactivateCard implements CardCommand {
        public final String cardId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public DeactivateCard(String cardId, boolean value, ActorRef<Done> replyTo) {
            this.cardId = cardId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class GetCard implements CardCommand {
        public final ActorRef<Optional<Card>> replyTo;

        @JsonCreator
        public GetCard(ActorRef<Optional<Card>> replyTo) {
            this.replyTo = replyTo;
        }
    }
}
