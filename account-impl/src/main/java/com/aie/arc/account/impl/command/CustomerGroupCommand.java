package com.aie.arc.account.impl.command;

import akka.Done;
import akka.actor.typed.ActorRef;
import com.aie.arc.account.impl.entity.entities.CustomerGroup;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;
import lombok.Value;

import java.util.Optional;

public interface CustomerGroupCommand {

    @Value
    final class CreateCustomerGroup implements CustomerGroupCommand {
        CustomerGroup customerGroup;
        public final ActorRef<Done> replyTo;
    }

    @Value
    final class UpdateCustomerGroup implements CustomerGroupCommand {
        CustomerGroup customerGroup;
        public final ActorRef<Done> replyTo;
    }
    @Value
    final class ActivateCustomerGroup implements CustomerGroupCommand {
        String customerGroupId;
        boolean value;
        public final ActorRef<Done> replyTo;
    }

    @Value
    final class DeactivateCustomerGroup implements CustomerGroupCommand {
        String customerGroupId;
        boolean value;
        public final ActorRef<Done> replyTo;
    }

    @Value
    final class GetCustomerGroup implements CustomerGroupCommand {
        public final ActorRef<Optional<CustomerGroup>> replyTo;
    }
}

