package com.aie.arc.account.impl.command;

import akka.Done;
import com.aie.arc.account.impl.entity.entities.ErrorProcessor;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;
import lombok.Value;

import java.time.LocalDateTime;
import java.util.Optional;

public interface ErrorProcessorCommand {
    enum GetErrorProcessor implements ErrorProcessorCommand, PersistentEntity.ReplyType<Optional<ErrorProcessor>> {
        INSTANCE
    }

    //#Add
    @Value
    final class CreateErrorProcessor implements ErrorProcessorCommand, PersistentEntity.ReplyType<Done> {
        private String eventId;
        private String eventName;
        private String eventData;
        private String eventErrorMessage;
        private LocalDateTime localDateTime;
    }
}