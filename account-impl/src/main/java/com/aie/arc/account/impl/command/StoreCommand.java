package com.aie.arc.account.impl.command;

import akka.Done;
import akka.actor.typed.ActorRef;
import com.aie.arc.account.impl.entity.entities.Store;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;
import lombok.Value;

import java.util.Optional;

public interface StoreCommand {

    @Value
    final class GetStore implements StoreCommand {
        public final ActorRef<Optional<Store>> replyTo;
    }

    //#Add Store
    @Value
    final class CreateStore implements StoreCommand {
        Store storeVO;
        public final ActorRef<Done> replyTo;
    }

    @Value
    final class UpdateStore implements StoreCommand {
        Store storeVO;
        public final ActorRef<Done> replyTo;
    }

    @Value
    final class ActivateStore implements StoreCommand {
        String customerGroupId;
        boolean value;
    }

    @Value
    final class DeactivateStore implements StoreCommand {
        String customerGroupId;
        boolean value;
        public final ActorRef<Done> replyTo;
    }
}