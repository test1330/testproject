package com.aie.arc.account.impl.email;

import com.aie.arc.account.api.email.*;
import com.aie.arc.account.impl.util.MessageConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.sendgrid.Request;
import com.typesafe.config.Config;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
@Slf4j
public class EmailService {

    @Inject
    private Config config;

    @Inject
    private ObjectMapper objectMapper;

    private String emailHostApi = null;

    private From fromEmail = null;

    private ReplyTo replyTo = null;

    private String sendGridkey = null;

    private String authorization = null;

    private static final int INDEX = 0;

    private String perfTestEmail = null;


    public void sendMail(EmailRequest emailRequest) {
        log.debug("Email sending start");

        if (perfTestEmail == null) {
            perfTestEmail = config.getConfig(MessageConstants.CONFIG_EMAIL_SERVICE).getString("performance_test_email");
        }

        List<Personalizations> listOfPersonalization = emailRequest.getPersonalizations();

        String toEmail = null;
        String subject = null;

        if(listOfPersonalization != null && listOfPersonalization.size() > INDEX){
            List<To> listOfTo = listOfPersonalization.get(INDEX).getTo();
            subject = listOfPersonalization.get(INDEX).getSubject();
            if(listOfTo != null && listOfTo.size() > INDEX){
                To to = listOfTo.get(INDEX);
                toEmail = to.getEmail();
                if(toEmail != null && toEmail.contains(perfTestEmail)){
                    log.debug("Skipping the Email sending for the performance test email :{}",toEmail);
                    return ;
                }
            }
        }

        if (fromEmail == null) {
            fromEmail = new From();
            fromEmail.setEmail(config.getConfig(MessageConstants.CONFIG_EMAIL_SERVICE).getString("fromAddressEmail"));
            fromEmail.setName(config.getConfig(MessageConstants.CONFIG_EMAIL_SERVICE).getString("fromAddressName"));
        }
        if (replyTo == null) {
            replyTo = new ReplyTo();
            replyTo.setEmail(config.getConfig(MessageConstants.CONFIG_EMAIL_SERVICE).getString("replyToEmail"));
            replyTo.setName(config.getConfig(MessageConstants.CONFIG_EMAIL_SERVICE).getString("replyToName"));
        }
        emailRequest.setFrom(fromEmail);

        emailRequest.setReplyTo(replyTo);
        try {
            String requestBody = objectMapper.writeValueAsString(emailRequest);

            if (authorization == null) {
                sendGridkey = config.getConfig(MessageConstants.CONFIG_EMAIL_SERVICE).getString("SENDGRID_API_KEY");
                if(StringUtils.isBlank(sendGridkey)) {
                    log.error("  *************   SendGrid Api Key is Empty   *************");
                }
                authorization = new StringBuilder("Bearer ").append(sendGridkey).toString();
            }

            if (emailHostApi == null) {
                emailHostApi = config.getConfig(MessageConstants.CONFIG_EMAIL_SERVICE).getString("email_host_api");
                if(StringUtils.isBlank(emailHostApi)) {
                    log.error("  *************   SendGrid Host Api is Empty   *************");
                }
                log.debug(" Email Host Api : {}",emailHostApi);
            }

            log.debug(" Email body Json :{}",requestBody);

            HttpResponse<String> response = Unirest.post(emailHostApi)
                    .header("authorization", authorization)
                    .header("content-type", "application/json")
                    .body(requestBody)
                    .asString();

            if(response != null) {
                log.debug("Email sent Status : {}, Body : {}", response.getStatus(), response.getBody());
                if(response.getStatus() != HttpStatus.SC_ACCEPTED){
                    log.error("Unsuccessful Email Response status code : {},  Body : {}", response.getStatus(), response.getBody());
                    log.error(" Unsuccessful email sent user Email id  : {} and TemplateId : {} and Subject :{}",
                            toEmail, emailRequest.getTemplateId(),subject);
                }
            }

        } catch (UnirestException | JsonProcessingException exp) {
            log.error("Exception occured at email sending :", exp);
        }
        log.debug("Email sending end");
    }
}