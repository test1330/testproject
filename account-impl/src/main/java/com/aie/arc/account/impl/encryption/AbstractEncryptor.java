package com.aie.arc.account.impl.encryption;

import com.aie.arc.account.api.common.exceptions.EncryptorException;

import java.io.Serializable;


/**
 * A simple abstract class for performing encryption and decryption.
 */
public abstract class AbstractEncryptor implements Encryptor, Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Was initialized.
	 */
	private transient boolean mInitialized = false;

	/**
	 * Monitor.
	 */
	private final transient Object mMonitor = new Object();

	/**
	 * Default constructor.
	 */
	public AbstractEncryptor() {
	}

	/**
	 * Sets the <code>pValue</code> property. This must get called before any
	 * encryption operations take place.
	 *
	 * @param pValue The <code>byte[]</code> to set as the key property.
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	public final void acceptEncryptKey(byte[] pValue) throws EncryptorException {
		synchronized ( mMonitor ) {
			if ( !mInitialized ) {
				doAcceptEncryptKey(pValue);
			}
		}
	}

	/**
	 * Sets the <code>key</code> property. This must get called before any
	 * encryption operations take place.
	 *
	 * @param pValue The <code>byte[]</code> to set as the key property.
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	public final void acceptDecryptKey(byte[] pValue) throws EncryptorException {
		synchronized ( mMonitor ) {
			if ( !mInitialized ) {
				doAcceptDecryptKey(pValue);
			}
		}
	}

	/**
	 * Call this initialization method before every cryptography operation.
	 *
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	protected final void init() throws EncryptorException {
		if ( !mInitialized ) {
			synchronized ( mMonitor ) {
				if ( !mInitialized ) {
					doInit();
					mInitialized = true;
				}
			}
		}
	}

	/**
	 * Encrypts <code>pValue</code> string.
	 *
	 * @param pValue String to encrypt.
	 * @return Encrypted string.
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	public final String encrypt(String pValue) throws EncryptorException {
		init();
		return postEncrypt(encodeToString(doEncrypt(preEncrypt(pValue.getBytes()))));
	}

	/**
	 * Decrypts encrypted <code>pValue</code> string.
	 *
	 * @param pValue encrypted String
	 * @return decrypted String
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	public final String decrypt(String pValue) throws EncryptorException {
		init();
		return postDecrypt(new String(doDecrypt(decodeToByteArray(preDecrypt(pValue)))));
	}

	/**
	 * Encrypts <code>pValue</code> byte[].
	 *
	 * @param pValue byte[] array to encrypt.
	 * @return encrypted byte[] array
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	public final byte[] encrypt(byte[] pValue) throws EncryptorException {
		init();
		return postEncrypt(doEncrypt(preEncrypt(pValue)));
	}

	/**
	 * Decrypts encrypted <code>pValue</code> byte[].
	 *
	 * @param pValue encrypted array of byte[]
	 * @return decrypted array of byte[]
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	public final byte[] decrypt(byte[] pValue) throws EncryptorException {
		init();
		return postDecrypt(doDecrypt(preDecrypt(pValue)));
	}

	/**
	 * Process <code>pValue</code> before encrypt.
	 *
	 * @param pValue String to encrypt
	 * @return String after processing
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	protected String preEncrypt(String pValue) throws EncryptorException {
		return pValue;
	}

	/**
	 * Process <code>pValue</code> before decrypt.
	 *
	 * @param pValue String to decrypt
	 * @return String after processing
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	protected String preDecrypt(String pValue) throws EncryptorException {
		return pValue;
	}

	/**
	 * Process <code>pValue</code> after encrypt.
	 *
	 * @param pValue String after encryption
	 * @return String after processing
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	protected String postEncrypt(String pValue) throws EncryptorException {
		return pValue;
	}

	/**
	 * Process <code>pValue</code> after decrypt.
	 *
	 * @param pValue String after decryption
	 * @return String after processing
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	protected String postDecrypt(String pValue) throws EncryptorException {
		return pValue;
	}

	/**
	 * Process <code>pValue</code> before encrypt.
	 *
	 * @param pValue byte[] to encrypt
	 * @return byte[] after processing
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	protected byte[] preEncrypt(byte[] pValue) throws EncryptorException {
		return pValue;
	}

	/**
	 * Process <code>pValue</code> before decrypt.
	 *
	 * @param pValue byte[] to decrypt
	 * @return byte[] after processing
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	protected byte[] preDecrypt(byte[] pValue) throws EncryptorException {
		return pValue;
	}

	/**
	 * Process <code>pValue</code> after encrypt.
	 *
	 * @param pValue byte[] after encryption
	 * @return byte[] after processing
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	protected byte[] postEncrypt(byte[] pValue) throws EncryptorException {
		return pValue;
	}

	/**
	 * Process <code>pValue</code> after decrypt.
	 *
	 * @param pValue byte[] after decryption
	 * @return byte[] after processing
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	protected byte[] postDecrypt(byte[] pValue) throws EncryptorException {
		return pValue;
	}

	/**
	 * Override this method if encoding of the raw encrypted data is necessary.
	 *
	 * Once encrypted, string data may no longer a string because the encrypted
	 * data is binary and may contain null characters, thus it may need to be
	 * encoded using a encoder such as Base64, UUEncode (ASCII only) or
	 * UCEncode(ASCII independent).
	 *
	 * @param pValue array of byte[] to encrypt
	 * @return resulted string
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	protected String encodeToString(byte[] pValue) throws EncryptorException {
		return new String(pValue);
	}

	/**
	 * Override this method to decode the data back into raw encrypted data if
	 * you have used a character encoder.
	 *
	 * @param pValue string to decode
	 * @return resulted byte[]
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	protected byte[] decodeToByteArray(String pValue) throws EncryptorException {
		return pValue.getBytes();
	}

	/**
	 * Implement this to accept a byte array as a key.
	 *
	 * @param pValue byte array
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	protected abstract void doAcceptEncryptKey(byte[] pValue)
			throws EncryptorException;

	/**
	 * Implement this to accept a byte array as a key.
	 *
	 * @param pValue byte array
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	protected abstract void doAcceptDecryptKey(byte[] pValue)
			throws EncryptorException;

	/**
	 * Implement this and do your initialization.
	 *
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	protected abstract void doInit() throws EncryptorException;

	/**
	 * Implement this with the actual encryption operation.
	 *
	 * @param pValue array of bytes
	 * @return encrypted array of bytes
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	protected abstract byte[] doEncrypt(byte[] pValue) throws EncryptorException;

	/**
	 * Implement this with the actual decryption operation.
	 *
	 * @param pValue array of bytes
	 * @return decrypted array of bytes
	 * @throws EncryptorException exception indicates that cryptography operation error occurred.
	 */
	protected abstract byte[] doDecrypt(byte[] pValue) throws EncryptorException;
}
