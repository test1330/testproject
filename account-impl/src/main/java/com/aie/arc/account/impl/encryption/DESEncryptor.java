package com.aie.arc.account.impl.encryption;

import com.aie.arc.account.api.common.exceptions.EncryptorException;
import sun.misc.CharacterDecoder;
import sun.misc.CharacterEncoder;
import sun.misc.UCDecoder;
import sun.misc.UCEncoder;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;



/**
 * A simple class for performing encryption/decryption operations using the
 * javax.crypto package.
 *
 */
public class DESEncryptor extends AbstractEncryptor {

	private static final long serialVersionUID = 1L;

/**
   * Crypto algorithm name.
   */
  protected static final String ALGORITHM = "DES";

  /**
   * 24 byte key string.
   */
  protected static final String KEY = "J9c&W;!n";

  /**
   * Encoder.
   */
  private static transient CharacterEncoder mEncoder = new UCEncoder();

  /**
   * Decoder.
   */
  private static transient CharacterDecoder mDecoder = new UCDecoder();

  /**
   * Key string.
   */
  private byte[] mKeyString = null;

  /**
   * Key specification.
   */
  transient DESKeySpec mKeySpec = null;

  /**
   * Secret key.
   */
  transient SecretKey mKey = null;

  /**
   * Encrypt cypher.
   */
  private transient javax.crypto.Cipher mEncryptCypher = null;

  /**
   * Decrypt cypher.
   */
  private transient javax.crypto.Cipher mDecryptCypher = null;

  //-------------------------------------
  // Constructors.
  //-------------------------------------

  /**
   * Default constructor.
   */
  public DESEncryptor() {
    super();
  }

  //-------------------------------------
  // Methods.
  //-------------------------------------

  /**
   * This is two way encryption, so encrypt/decrypt keys are the same.
   * @param pValue - key
   * @throws EncryptorException if encryption error occurs
   */
  protected final void doAcceptEncryptKey(byte[] pValue)
    throws EncryptorException {
    acceptKey(pValue);
  }

  /**
   * This is two way encryption, so encrypt/decrypt keys are the same.
   * @param pValue - key
   * @throws EncryptorException if encryption error occurs
   */
  protected final void doAcceptDecryptKey(byte[] pValue)
    throws EncryptorException {
    acceptKey(pValue);
  }

  /**
   * Initialize the KeySpec.
   *
   * @param pValue new key
   * @throws EncryptorException This exception indicates that a severe error
   * occured while performing a cryptograpy operation.
   */
  private void acceptKey(byte[] pValue) {
    mKeyString = pValue;
  }

  /**
   * Initialize DESEncrytor.
   *
   * @throws EncryptorException This exception indicates that a severe error
   * occured while performing a cryptograpy operation.
   */
  protected final void doInit() throws EncryptorException {
    try {
      if (mKeyString == null) {
        mKeyString = KEY.getBytes();
      }

      mKeySpec = new DESKeySpec(mKeyString);
      mKey = new SecretKeySpec(mKeySpec.getKey(), ALGORITHM);
      mEncryptCypher = javax.crypto.Cipher.getInstance(ALGORITHM);
      mEncryptCypher.init(javax.crypto.Cipher.ENCRYPT_MODE, mKey);
      mDecryptCypher = javax.crypto.Cipher.getInstance(ALGORITHM);
      mDecryptCypher.init(javax.crypto.Cipher.DECRYPT_MODE, mKey);
    } catch (NoSuchAlgorithmException nsae) {
      throw new EncryptorException(nsae);
    } catch (NoSuchPaddingException nspe) {
      throw new EncryptorException(nspe);
    } catch (InvalidKeyException nske) {
      throw new EncryptorException(nske);
    }
  }

  /**
   * Performs encryption of array of bytes.
   *
   * @param pValue array of bytes to encrypt
   * @return encrypted array of bytes
   * @throws EncryptorException This exception indicates that a severe error
   * occured while performing a cryptograpy operation.
   */
  protected final byte[] doEncrypt(byte[] pValue) throws EncryptorException {
    try {
      return mEncryptCypher.doFinal(pValue);
    } catch (IllegalBlockSizeException ibse) {
      throw new EncryptorException(ibse);
    } catch (BadPaddingException bpe) {
      throw new EncryptorException(bpe);
    }
  }

  /**
   * Performs decription of array of bytes.
   *
   * @param pValue decrypt array of bytes
   * @return decrypted array of bytes
   * @throws EncryptorException This exception indicates that a severe error
   * occured while performing a cryptograpy operation.
   */
  protected final byte[] doDecrypt(byte[] pValue) throws EncryptorException {
    try {
      return mDecryptCypher.doFinal(pValue);
    } catch (IllegalBlockSizeException ibse) {
      throw new EncryptorException(ibse);
    } catch (BadPaddingException bpe) {
      throw new EncryptorException(bpe);
    }
  }

  /**
   * Once encrypted, string data may no longer be a string because the
   * encrypted data is binary and may contain null characters, thus it may
   * need to be encoded using a encoder such as Base64, UUEncode (ASCII only)
   * or UCEncode(ASCII independent).
   * @param pValue Value to encode
   * @throws EncryptorException This exception indicates that an error
   * occured while performing a cryptograpy operation.
   * @return Encoded data
   */
  @Override
  protected String encodeToString(byte[] pValue) throws EncryptorException {
    return mEncoder.encode(pValue);
  }

  /**
   * Decode to byte array.
   *
   * @param pValue - value to decode
   * @return byte array
   * @throws EncryptorException if encryption error occurs
   */
  @Override
  protected byte[] decodeToByteArray(String pValue) throws EncryptorException {
    try {
      return mDecoder.decodeBuffer(pValue);
    } catch (IOException ioe) {
      throw new EncryptorException("Failed to decode byte array: ", ioe);
    }
  }

} // end of class Encryptor
