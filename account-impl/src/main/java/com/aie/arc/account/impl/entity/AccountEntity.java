package com.aie.arc.account.impl.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.*;
import com.aie.arc.account.api.common.exceptions.ParameterRequired;
import com.aie.arc.account.impl.command.AccountCommand;
import com.aie.arc.account.impl.entity.entities.Account;
import com.aie.arc.account.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.account.impl.event.AccountEvent;
import com.aie.arc.account.api.common.exceptions.ParameterRequired;
import com.aie.arc.account.impl.state.AccountState;
import com.aie.arc.account.impl.status.Status;
import com.aie.arc.account.impl.util.MessageConstants;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.Set;

@Slf4j
public class AccountEntity extends EventSourcedBehaviorWithEnforcedReplies<AccountCommand, AccountEvent, AccountState> {

    final private EntityContext<AccountCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<AccountCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(AccountCommand.class, "AccountEntity");

    AccountEntity(EntityContext<AccountCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }


    @Override
    public CommandHandlerWithReply<AccountCommand, AccountEvent, AccountState> commandHandler() {
        CommandHandlerWithReplyBuilder<AccountCommand, AccountEvent, AccountState> builder = newCommandHandlerWithReplyBuilder();

        builder.forAnyState()
                .onCommand(AccountCommand.GetAccount.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.account))

                .onCommand(AccountCommand.RegisterAccount.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.AccountCreated(cmd.getAccount()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.UpdateProfile.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.ProfileUpdated(cmd.getAccountId(), cmd.getProfile()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.UpdateAccount.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.AccountUpdated(cmd.getAccount()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.AddAddressToAccount.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.AddressAddedToAccount(cmd.getAccountId(), cmd.getAddressId(), cmd.getAddressZipCode(), cmd.isDefaultShipping(), cmd.isDefaultBilling(), cmd.isMigratedEvent()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.UpdateAddressToAccount.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.AddressUpdatedToAccount(cmd.getAccountId(), cmd.getAddressId(), cmd.getAddressZipCode(), cmd.isDefaultShipping(), cmd.isDefaultBilling(), cmd.isMigratedEvent()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.AddCardToAccount.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.CardAddedToAccount(cmd.getAccountId(), cmd.getCardId(), cmd.isDefaultCreditCard(), cmd.isMigratedEvent()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.AddZipCode.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.ZipCodeAdded(cmd.getAccountId(), cmd.getMyZip()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.UpdateCardToAccount.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.CardUpdatedToAccount(cmd.getAccountId(), cmd.getCardId(), cmd.isDefaultCreditCard(), cmd.isMigratedEvent()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.DeactivateAccountCard.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.AccountCardDeactivated(cmd.getAccountId(), cmd.getCardId(), cmd.isMigratedEvent()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.UpdateDefaultShippingAddress.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.DefaultShippingAddressUpdatedToAccount(cmd.getAccountId(), cmd.getShippingAddressId(), cmd.getBillingAddressId()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.UpdateDefaultBillingAddress.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.DefaultBillingAddressUpdatedToAccount(cmd.getAccountId(), cmd.getBillingAddressId(), cmd.getShippingAddressId()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.DeactivateAddressToAccount.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.AddressDeactivatedToAccount(cmd.getAccountId(), cmd.getAddressId(), cmd.isMigratedEvent()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.AddStoreToAccount.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.StoreAddedToAccount(cmd.getAccountId(), cmd.getStoreId()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.UpdateStoreToAccount.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.StoreAddedToAccount(cmd.getAccountId(), cmd.getStoreId()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.DeactivateAccount.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.AccountDeactivated(cmd.getAccountId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.ActivateAccount.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.AccountActivated(cmd.getAccountId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.Subscribe.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.Subscribed(cmd.getAccountId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.Unsubscribe.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.Unsubscribed(cmd.getAccountId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccountCommand.SetDefaultPayment.class, (state, cmd) -> Effect()
                        .persist(new AccountEvent.DefaultPaymentSet(cmd.getAccountId(), cmd.getCardId()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
        ;
        return builder.build();
    }

    @Override
    public EventHandler<AccountState, AccountEvent> eventHandler() {
        EventHandlerBuilder<AccountState, AccountEvent> builder = newEventHandlerBuilder();

        builder.forAnyState()
                .onEvent(AccountEvent.AccountCreated.class, (state, evt) -> AccountState.create(evt.getAccount()))
                .onEvent(AccountEvent.ProfileUpdated.class, (state, evt) -> updateProfile(evt, state))
                .onEvent(AccountEvent.AccountUpdated.class, (state, evt) -> updateAccount(evt, state))
                .onEvent(AccountEvent.AddressAddedToAccount.class, (state, evt) -> addAddressToAccount(evt, state))
                .onEvent(AccountEvent.AddressUpdatedToAccount.class, (state, evt) -> updateAddressToAccount(evt, state))
                .onEvent(AccountEvent.CardAddedToAccount.class, (state, evt) -> addCardToAccount(evt, state))
                .onEvent(AccountEvent.ZipCodeAdded.class, (state, evt) -> state.addZipCode(evt.getMyZip()))
                .onEvent(AccountEvent.CardUpdatedToAccount.class, (state, evt) -> updateCardToAccount(evt, state))
                .onEvent(AccountEvent.AccountCardDeactivated.class, (state, evt) -> deactivateAccountCard(evt, state))
                .onEvent(AccountEvent.DefaultShippingAddressUpdatedToAccount.class, (state, evt) -> setDefaultShippingAddress(evt, state))
                .onEvent(AccountEvent.DefaultBillingAddressUpdatedToAccount.class, (state, evt) -> setDefaultBillingAddress(evt, state))
                .onEvent(AccountEvent.AddressDeactivatedToAccount.class, (state, evt) -> deactivateAddressToAccount(evt, state))
                .onEvent(AccountEvent.StoreAddedToAccount.class, (state, evt) -> state.updateDetails(evt.getStoreId()))
                .onEvent(AccountEvent.StoreUpdatedToAccount.class, (state, evt) -> updateStore(evt, state))
                .onEvent(AccountEvent.AccountDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(AccountEvent.AccountActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(AccountEvent.Subscribed.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(AccountEvent.Unsubscribed.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(AccountEvent.DefaultPaymentSet.class, (state, evt) -> state.updateDefaultPaymentDetails(evt.getCardId()))
        ;
        return builder.build();
    }


    private AccountState updateAccount(AccountEvent.AccountUpdated evt, AccountState state) {
        if (state.account.isPresent()) {
            Account account = state.account.get();
            Account updatedAccount = account.updateIfNotEmptyAccount(account.getAccountId(), evt.getAccount().getBmcisProfileId(),
                    account.getAccountNumber(), evt.getAccount().getProfile(), account.getBmRoleId(),
                    account.getFlashSaleLimitedIds(), account.getAddressIds(), account.getDefaultBillingAddressId(),
                    account.getDefaultShippingAddressId(), account.getCardIds(), account.getDefaultCardId(),
                    evt.getAccount().getStoreId(), evt.getAccount().getMyZip(), evt.getAccount().isSubscribed(),
                    evt.getAccount().isActive(), account.getCreatedAt(), evt.getAccount().getLastModifiedAt(),
                    evt.getAccount().getStatus(), null, account.getOnlineStoreId());
            return state.updateDetails(updatedAccount);
        } else {
            throw new ParameterRequired(new String[]{MessageConstants.ACCOUNT_NOT_FOUND});
        }
    }

    ;


    private AccountState updateProfile(AccountEvent.ProfileUpdated evt, AccountState state) {
        if (state.account.isPresent()) {
            Account account = state.account.get();
            Account updatedAccount = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                    account.getAccountNumber(), evt.getProfile(), account.getBmRoleId(), account.getFlashSaleLimitedIds(),
                    account.getAddressIds(), account.getDefaultBillingAddressId(),
                    account.getDefaultShippingAddressId(), account.getCardIds(), account.getDefaultCardId(),
                    account.getStoreId(), account.getMyZip(), account.isSubscribed(), account.isActive(),
                    account.getCreatedAt(), account.getLastModifiedAt(), account.getStatus(), null,
                    account.getOnlineStoreId());
            return state.updateDetails(updatedAccount);
        } else {
            throw new ParameterRequired(new String[]{MessageConstants.ACCOUNT_NOT_FOUND});
        }
    }

    private AccountState addAddressToAccount(AccountEvent.AddressAddedToAccount evt, AccountState state) {
        if (state.account.isPresent()) {
            Account account = state.account.get();
            account.getAddressIds().add(evt.getAddressId());
            if (evt.isDefaultShipping() && evt.isDefaultBilling()) {
                account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                        account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                        account.getFlashSaleLimitedIds(),account.getAddressIds(), evt.getAddressId(), evt.getAddressId(),
                        account.getCardIds(), account.getDefaultCardId(), account.getStoreId(), evt.getAddressZipCode(),
                        account.isSubscribed(), account.isActive(), account.getCreatedAt(), account.getLastModifiedAt(),
                        account.getStatus(), null, account.getOnlineStoreId());
            } else if (evt.isDefaultShipping() && !evt.isDefaultBilling()) {
                account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                        account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                        account.getFlashSaleLimitedIds(),account.getAddressIds(), evt.getAddressId(),
                        account.getDefaultBillingAddressId(), account.getCardIds(),
                        account.getDefaultCardId(), account.getStoreId(), evt.getAddressZipCode(),
                        account.isSubscribed(), account.isActive(), account.getCreatedAt(), account.getLastModifiedAt(),
                        account.getStatus(), null, account.getOnlineStoreId());
            } else if (!evt.isDefaultShipping() && evt.isDefaultBilling()) {
                account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                        account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                        account.getFlashSaleLimitedIds(),account.getAddressIds(), account.getDefaultShippingAddressId(),
                        evt.getAddressId(), account.getCardIds(), account.getDefaultCardId(), account.getStoreId(),
                        account.getMyZip(), account.isSubscribed(), account.isActive(), account.getCreatedAt(),
                        account.getLastModifiedAt(), account.getStatus(),null, account.getOnlineStoreId());
            }
            return state.updateDetails(account);
        } else {
            throw new ParameterRequired(new String[]{MessageConstants.ACCOUNT_NOT_FOUND});
        }
    }

    private AccountState updateAddressToAccount(AccountEvent.AddressUpdatedToAccount evt, AccountState state) {
        if (state.account.isPresent()) {
            Account account = state.account.get();
            if (evt.isDefaultShipping() && evt.isDefaultBilling()) {
                log.trace("Both true. AddressId : {}, AccountDefaultShippingId : {}, AccountDefaultBilling : {}",
                        evt.getAddressId(), account.getDefaultShippingAddressId(), account.getDefaultBillingAddressId());
                account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                        account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                        account.getFlashSaleLimitedIds(),account.getAddressIds(), evt.getAddressId(), evt.getAddressId(),
                        account.getCardIds(), account.getDefaultCardId(), account.getStoreId(), evt.getAddressZipCode(),
                        account.isSubscribed(), account.isActive(),
                        account.getCreatedAt(), account.getLastModifiedAt(), account.getStatus(),
                        null, account.getOnlineStoreId());
            }
            if (evt.isDefaultShipping() && !evt.isDefaultBilling()) {
                log.trace("DefaultBilling false. AddressId : {}, AccountDefaultShippingId : {}, AccountDefaultBilling : {}",
                        evt.getAddressId(), account.getDefaultShippingAddressId(), account.getDefaultBillingAddressId());
                account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                        account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                        account.getFlashSaleLimitedIds(),account.getAddressIds(), evt.getAddressId(),
                        StringUtils.equalsIgnoreCase(account.getDefaultBillingAddressId(), evt.getAddressId()) ?
                                null : account.getDefaultBillingAddressId(),
                        account.getCardIds(),
                        account.getDefaultCardId(), account.getStoreId(), evt.getAddressZipCode(), account.isSubscribed(),
                        account.isActive(), account.getCreatedAt(), account.getLastModifiedAt(), account.getStatus(),
                        null, account.getOnlineStoreId());
            }
            if (!evt.isDefaultShipping() && evt.isDefaultBilling()) {
                log.trace("DefaultShipping false. AddressId : {}, AccountDefaultShippingId : {}, AccountDefaultBilling : {}",
                        evt.getAddressId(), account.getDefaultShippingAddressId(), account.getDefaultBillingAddressId());
                boolean zipCodeUpdate = StringUtils.equalsIgnoreCase(account.getDefaultShippingAddressId(),
                        evt.getAddressId());
                log.trace("zipCodeUpdate flag : {}", zipCodeUpdate);
                log.trace("zipCode value check using ? operator : {}", zipCodeUpdate ? null : account.getMyZip());
                account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                        account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                        account.getFlashSaleLimitedIds(),account.getAddressIds(), zipCodeUpdate ?
                                null : account.getDefaultShippingAddressId(),
                        evt.getAddressId(), account.getCardIds(),
                        account.getDefaultCardId(), account.getStoreId(), zipCodeUpdate ? null : account.getMyZip(),
                        account.isSubscribed(), account.isActive(), account.getCreatedAt(), account.getLastModifiedAt(),
                        account.getStatus(), null, account.getOnlineStoreId());
            }
            if (!evt.isDefaultShipping() && !evt.isDefaultBilling()) {
                log.trace("Both false. AddressId : {}, AccountDefaultShippingId : {}, AccountDefaultBilling : {}",
                        evt.getAddressId(), account.getDefaultShippingAddressId(), account.getDefaultBillingAddressId());
                boolean zipCodeUpdate = StringUtils.equalsIgnoreCase(account.getDefaultShippingAddressId(),
                        evt.getAddressId());
                log.trace("zipCodeUpdate flag : {}", zipCodeUpdate);
                log.trace("zipCode value check using ? operator : {}", zipCodeUpdate ? null : account.getMyZip());
                account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                        account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                        account.getFlashSaleLimitedIds(),account.getAddressIds(), zipCodeUpdate ?
                                null : account.getDefaultShippingAddressId(),
                        StringUtils.equalsIgnoreCase(account.getDefaultBillingAddressId(), evt.getAddressId()) ?
                                null : account.getDefaultBillingAddressId(), account.getCardIds(),
                        account.getDefaultCardId(), account.getStoreId(), zipCodeUpdate ?
                                null : account.getMyZip(), account.isSubscribed(),
                        account.isActive(), account.getCreatedAt(), account.getLastModifiedAt(), account.getStatus(),
                        null, account.getOnlineStoreId());
            }
            return state.updateDetails(account);
        } else {
            throw new ParameterRequired(new String[]{MessageConstants.ACCOUNT_NOT_FOUND});
        }
    }

    /**
     * Handler method for setEventHandler method. It adds card to list of cards owned by Account.
     *
     * @return new Account state after applying changes to existing state.
     */
    private AccountState addCardToAccount(AccountEvent.CardAddedToAccount evt, AccountState state) {
        if (state.account.isPresent()) {
            Account account = state.account.get();
            account.getCardIds().add(evt.getCardId());
            if (account.getCardIds().size() == 1 || evt.isDefaultCreditCard()) {
                account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                        account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                        account.getFlashSaleLimitedIds(),account.getAddressIds(), account.getDefaultShippingAddressId(),
                        account.getDefaultBillingAddressId(),
                        account.getCardIds(), evt.getCardId(), account.getStoreId(), account.getMyZip(),
                        account.isSubscribed(), account.isActive(), account.getCreatedAt(),
                        account.getLastModifiedAt(), account.getStatus(),null, account.getOnlineStoreId());
            }
            return state.updateDetails(account);
        } else {
            throw new ParameterRequired(new String[]{MessageConstants.ACCOUNT_NOT_FOUND});
        }
    }

    private AccountState deactivateAccountCard(AccountEvent.AccountCardDeactivated evt, AccountState state) {
        if (state.account.isPresent()) {
            Account account = state.account.get();
            boolean isDefaultCreditCard = StringUtils.equalsIgnoreCase(evt.getCardId(), account.getDefaultCardId());
            account.getCardIds().remove(evt.getCardId());
            if (CollectionUtils.isEmpty(account.getCardIds())) {
                account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                        account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                        account.getFlashSaleLimitedIds(),account.getAddressIds(), account.getDefaultShippingAddressId(),
                        account.getDefaultBillingAddressId(), account.getCardIds(), null,
                        account.getStoreId(), account.getMyZip(), account.isSubscribed(),
                        account.isActive(), account.getCreatedAt(), account.getLastModifiedAt(), account.getStatus(),
                        null, account.getOnlineStoreId());
            } else if (account.getCardIds().size() == 1) {
                account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                        account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                        account.getFlashSaleLimitedIds(),account.getAddressIds(), account.getDefaultShippingAddressId(),
                        account.getDefaultBillingAddressId(), account.getCardIds(), account.getCardIds().isEmpty() ?
                                null : account.getCardIds().get(0),
                        account.getStoreId(), account.getMyZip(), account.isSubscribed(),
                        account.isActive(), account.getCreatedAt(), account.getLastModifiedAt(), account.getStatus(),
                        null, account.getOnlineStoreId());
            } else if (isDefaultCreditCard) {
                account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                        account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                        account.getFlashSaleLimitedIds(), account.getAddressIds(), account.getDefaultShippingAddressId(),
                        account.getDefaultBillingAddressId(), account.getCardIds(), account.getCardIds().isEmpty() ?
                                null : account.getCardIds().get(0),
                        account.getStoreId(), account.getMyZip(), account.isSubscribed(),
                        account.isActive(), account.getCreatedAt(), account.getLastModifiedAt(), account.getStatus(),
                        null, account.getOnlineStoreId());
            }
            return state.updateDetails(account);
        } else {
            throw new ParameterRequired(new String[]{MessageConstants.ACCOUNT_NOT_FOUND});
        }
    }


    /**
     * Handler method for setEventHandler method. It updates card to list of cards owned by Account.
     *
     * @return new Account state after applying changes to existing state.
     */
    private AccountState updateCardToAccount(AccountEvent.CardUpdatedToAccount evt, AccountState state) {
        if (state.account.isPresent()) {
            Account account = state.account.get();
            boolean isDefaultCardUpdated = StringUtils.equalsIgnoreCase(account.getDefaultCardId(),
                    evt.getCardId());
            String defaultCardId = account.getDefaultCardId();
            if (evt.isDefaultCreditCard()) {
                defaultCardId = evt.getCardId();
            } else if (isDefaultCardUpdated && !evt.isDefaultCreditCard()) {
                defaultCardId = null;
            }
            account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                    account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                    account.getFlashSaleLimitedIds(), account.getAddressIds(), account.getDefaultShippingAddressId(),
                    account.getDefaultBillingAddressId(),
                    account.getCardIds(), defaultCardId, account.getStoreId(),
                    account.getMyZip(), account.isSubscribed(), account.isActive(), account.getCreatedAt(),
                    account.getLastModifiedAt(), account.getStatus(), null, account.getOnlineStoreId());
            return state.updateDetails(account);
        } else {
            throw new ParameterRequired(new String[]{MessageConstants.ACCOUNT_NOT_FOUND});
        }
    }

    private AccountState updateStore(AccountEvent.StoreUpdatedToAccount evt, AccountState state) {
        if (state.account.isPresent()) {
            Account account = state.account.get();
            account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                    account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                    account.getFlashSaleLimitedIds(), account.getAddressIds(), account.getDefaultShippingAddressId(),
                    account.getDefaultBillingAddressId(), account.getCardIds(),
                    account.getDefaultCardId(), evt.getStoreId(), account.getMyZip(), account.isSubscribed(),
                    account.isActive(), account.getCreatedAt(), new LastModifiedAt(LocalDateTime.now()),
                    account.getStatus(), null, account.getOnlineStoreId());
            return state.updateDetails(account);
        } else {
            throw new ParameterRequired(new String[]{MessageConstants.ACCOUNT_NOT_FOUND});
        }
    }

    private AccountState setDefaultShippingAddress(AccountEvent.DefaultShippingAddressUpdatedToAccount evt, AccountState state) {
        if (state.account.isPresent()) {
            Account account = state.account.get();
            account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                    account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                    account.getFlashSaleLimitedIds(), account.getAddressIds(), evt.getShippingAddressId(),
                    account.getDefaultBillingAddressId(), account.getCardIds(),
                    account.getDefaultCardId(), account.getStoreId(), account.getMyZip(), account.isSubscribed(),
                    account.isActive(), account.getCreatedAt(), new LastModifiedAt(LocalDateTime.now()),
                    account.getStatus(), null, account.getOnlineStoreId());
            return state.updateDetails(account);
        } else {
            throw new ParameterRequired(new String[]{MessageConstants.ACCOUNT_NOT_FOUND});
        }
    }

    private AccountState setDefaultBillingAddress(AccountEvent.DefaultBillingAddressUpdatedToAccount evt, AccountState state) {
        if (state.account.isPresent()) {
            Account account = state.account.get();
            account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                    account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                    account.getFlashSaleLimitedIds(), account.getAddressIds(), account.getDefaultShippingAddressId(),
                    evt.getBillingAddressId(), account.getCardIds(),
                    account.getDefaultCardId(), account.getStoreId(), account.getMyZip(), account.isSubscribed(),
                    account.isActive(), account.getCreatedAt(), new LastModifiedAt(LocalDateTime.now()),
                    account.getStatus(), null, account.getOnlineStoreId());
            return state.updateDetails(account);
        } else {
            throw new ParameterRequired(new String[]{MessageConstants.ACCOUNT_NOT_FOUND});
        }
    }

    private AccountState deactivateAddressToAccount(AccountEvent.AddressDeactivatedToAccount evt, AccountState state) {
        if (state.account.isPresent()) {
            Account account = state.account.get();
            boolean isDefaultShipping = StringUtils.equalsIgnoreCase(
                    account.getDefaultShippingAddressId(), evt.getAddressId());
            boolean isDefaultBilling = StringUtils.equalsIgnoreCase(
                    account.getDefaultBillingAddressId(), evt.getAddressId());
            account.getAddressIds().remove(evt.getAddressId());
            if (CollectionUtils.isEmpty(account.getAddressIds())) {
                account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                        account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                        account.getFlashSaleLimitedIds(), account.getAddressIds(), null,
                        null, account.getCardIds(),
                        account.getDefaultCardId(), account.getStoreId(), account.getMyZip(), account.isSubscribed(),
                        account.isActive(), account.getCreatedAt(), new LastModifiedAt(LocalDateTime.now()),
                        account.getStatus(), null, account.getOnlineStoreId());
            } else {
                account = account.updateIfNotEmptyAccount(account.getAccountId(), account.getBmcisProfileId(),
                        account.getAccountNumber(), account.getProfile(), account.getBmRoleId(),
                        account.getFlashSaleLimitedIds(), account.getAddressIds(),
                        isDefaultShipping ? account.getAddressIds().get(0) : account.getDefaultShippingAddressId(),
                        isDefaultBilling ? null : account.getDefaultBillingAddressId(),
                        account.getCardIds(),
                        account.getDefaultCardId(), account.getStoreId(), account.getMyZip(), account.isSubscribed(),
                        account.isActive(), account.getCreatedAt(), new LastModifiedAt(LocalDateTime.now()),
                        account.getStatus(), null, account.getOnlineStoreId());
            }
            return state.updateDetails(account);
        } else {
            throw new ParameterRequired(new String[]{MessageConstants.ACCOUNT_NOT_FOUND});
        }
    }

    @Override
    public AccountState emptyState() {
        return AccountState.empty();
    }

    @Override
    public Set<String> tagsFor(AccountEvent accountEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, AccountEvent.TAG).apply(accountEvent);
    }

    public static Behavior<AccountCommand> create(EntityContext<AccountCommand> entityContext) {
        return new AccountEntity(entityContext);
    }
}
