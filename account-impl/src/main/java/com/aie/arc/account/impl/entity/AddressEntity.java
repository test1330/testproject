package com.aie.arc.account.impl.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.account.impl.command.AddressCommand;
import com.aie.arc.account.impl.event.AddressEvent;
import com.aie.arc.account.impl.state.AddressState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Slf4j
public class AddressEntity extends EventSourcedBehaviorWithEnforcedReplies<AddressCommand, AddressEvent, AddressState> {


    final private EntityContext<AddressCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<AddressCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(AddressCommand.class, "AddressEntity");

    AddressEntity(EntityContext<AddressCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }


    @Override
    public CommandHandlerWithReply<AddressCommand, AddressEvent, AddressState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()
                .onCommand(AddressCommand.GetAddress.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.address))

                .onCommand(AddressCommand.CreateAddress.class, (state, cmd) -> Effect()
                        .persist(new AddressEvent.AddressCreated(cmd.getAddress()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AddressCommand.UpdateAddress.class, (state, cmd) -> Effect()
                        .persist(new AddressEvent.AddressUpdated(cmd.getAddress()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AddressCommand.DeactivateAddress.class, (state, cmd) -> Effect()
                        .persist(new AddressEvent.AddressDeactivated(cmd.getAddressId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AddressCommand.ActivateAddress.class, (state, cmd) -> Effect()
                        .persist(new AddressEvent.AddressActivated(cmd.getAddressId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<AddressState, AddressEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(AddressEvent.AddressCreated.class, (state, evt) -> AddressState.create(evt.getAddress()))
                .onEvent(AddressEvent.AddressUpdated.class, (state, evt) -> state.updateDetails(evt.getAddress()))
                .onEvent(AddressEvent.AddressDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(AddressEvent.AddressActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public AddressState emptyState() {
        return AddressState.empty();
    }

    @Override
    public Set<String> tagsFor(AddressEvent addressEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, AddressEvent.TAG).apply(addressEvent);
    }

    public static Behavior<AddressCommand> create(EntityContext<AddressCommand> entityContext) {
        return new AddressEntity(entityContext);
    }
}
