
package com.aie.arc.account.impl.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.account.impl.command.BusinessAddressCommand;
import com.aie.arc.account.impl.event.BusinessAddressEvent;
import com.aie.arc.account.impl.state.BusinessAddressState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Slf4j
public class BusinessAddressEntity extends EventSourcedBehaviorWithEnforcedReplies<BusinessAddressCommand, BusinessAddressEvent,
        BusinessAddressState> {


    final private EntityContext<BusinessAddressCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<BusinessAddressCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(BusinessAddressCommand.class, "BusinessAddressEntity");

    BusinessAddressEntity(EntityContext<BusinessAddressCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }


    @Override
    public CommandHandlerWithReply<BusinessAddressCommand, BusinessAddressEvent, BusinessAddressState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(BusinessAddressCommand.GetAddress.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.address))

                .onCommand(BusinessAddressCommand.CreateAddress.class, (state, cmd) -> Effect()
                        .persist(new BusinessAddressEvent.AddressCreated(cmd.getAddress()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(BusinessAddressCommand.UpdateAddress.class, (state, cmd) -> Effect()
                        .persist(new BusinessAddressEvent.AddressUpdated(cmd.getAddress()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(BusinessAddressCommand.DeactivateAddress.class, (state, cmd) -> Effect()
                        .persist(new BusinessAddressEvent.AddressDeactivated(cmd.getAddressId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(BusinessAddressCommand.ActivateAddress.class, (state, cmd) -> Effect()
                        .persist(new BusinessAddressEvent.AddressActivated(cmd.getAddressId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<BusinessAddressState, BusinessAddressEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(BusinessAddressEvent.AddressCreated.class, (state, evt) -> BusinessAddressState.create(evt.getAddress()))
                .onEvent(BusinessAddressEvent.AddressUpdated.class, (state, evt) -> state.updateDetails(evt.getAddress()))
                .onEvent(BusinessAddressEvent.AddressDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(BusinessAddressEvent.AddressActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public BusinessAddressState emptyState() {
        return BusinessAddressState.empty();
    }

    @Override
    public Set<String> tagsFor(BusinessAddressEvent businessAddressEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, BusinessAddressEvent.TAG).apply(businessAddressEvent);
    }

    public static Behavior<BusinessAddressCommand> create(EntityContext<BusinessAddressCommand> entityContext) {
        return new BusinessAddressEntity(entityContext);
    }


}
