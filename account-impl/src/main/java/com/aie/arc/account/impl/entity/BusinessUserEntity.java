package com.aie.arc.account.impl.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.account.impl.command.BusinessUserCommand;
import com.aie.arc.account.impl.entity.entities.BusinessUser;
import com.aie.arc.account.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.account.impl.event.BusinessUserEvent;
import com.aie.arc.account.impl.state.BusinessUserState;
import com.aie.arc.account.impl.status.Status;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BusinessUserEntity extends EventSourcedBehaviorWithEnforcedReplies<BusinessUserCommand, BusinessUserEvent, BusinessUserState> {

    final private EntityContext<BusinessUserCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<BusinessUserCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(BusinessUserCommand.class, "BusinessUserEntity");

    BusinessUserEntity(EntityContext<BusinessUserCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public BusinessUserState emptyState() {
        return BusinessUserState.empty();
    }

    @Override
    public CommandHandlerWithReply<BusinessUserCommand, BusinessUserEvent, BusinessUserState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(BusinessUserCommand.GetBusinessUser.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.businessUser))

                .onCommand(BusinessUserCommand.CreateBusinessUser.class, (state, cmd) -> Effect()
                        .persist(new BusinessUserEvent.BusinessUserCreated(cmd.getBusinessUser()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(BusinessUserCommand.UpdateBusinessUser.class, (state, cmd) -> Effect()
                        .persist(new BusinessUserEvent.BusinessUserUpdated(cmd.getBusinessUserId(), cmd.getBusinessUser()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(BusinessUserCommand.DeactivateBusinessUser.class, (state, cmd) -> Effect()
                        .persist(new BusinessUserEvent.BusinessUserDeactivated(cmd.getBusinessUserId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(BusinessUserCommand.ActivateBusinessUser.class, (state, cmd) -> Effect()
                        .persist(new BusinessUserEvent.BusinessUserActivated(cmd.getBusinessUserId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(BusinessUserCommand.AssignRoleToBusinessUser.class, (state, cmd) -> Effect()
                        .persist(new BusinessUserEvent.RoleAssignedToBusinessUser(cmd.getUserId(), cmd.getRoleIds(), cmd.getCreatedAt(), cmd.getLastModifiedAt()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .build();
    }

    @Override
    public EventHandler<BusinessUserState, BusinessUserEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(BusinessUserEvent.BusinessUserCreated.class, (state, evt) -> BusinessUserState.create(evt.getBusinessUser()))
                .onEvent(BusinessUserEvent.BusinessUserUpdated.class, (state, evt) -> state.updateDetails(evt.getBusinessUser()))
                .onEvent(BusinessUserEvent.BusinessUserDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(BusinessUserEvent.BusinessUserActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(BusinessUserEvent.RoleAssignedToBusinessUser.class, this::roleAssignToUser)
                .build();
    }


    private BusinessUserState roleAssignToUser(BusinessUserState state, BusinessUserEvent.RoleAssignedToBusinessUser evt) {
        BusinessUser businessUser = state.businessUser.get();
        if (businessUser.getRoleIds() == null) {
            List<String> ids = new ArrayList<>();
            ids.addAll(evt.getRoleIds());
            businessUser = businessUser.updateIfNotEmptyBusinessUserAccount(businessUser.getBusinessUserId(), businessUser.getOrganizationId(), businessUser.getProfile(), businessUser.getDesignation(), ids, businessUser.isActive(), businessUser.getCreatedAt(), new LastModifiedAt(LocalDateTime.now()), Status.CREATED);
        } else {
            businessUser.getRoleIds().addAll(evt.getRoleIds());
        }
        return state.updateDetails(businessUser);
    }

    @Override
    public Set<String> tagsFor(BusinessUserEvent businessUserEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, BusinessUserEvent.TAG).apply(businessUserEvent);
    }

    public static Behavior<BusinessUserCommand> create(EntityContext<BusinessUserCommand> entityContext) {
        return new BusinessUserEntity(entityContext);
    }

}

