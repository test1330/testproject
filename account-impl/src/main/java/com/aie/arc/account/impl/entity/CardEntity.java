package com.aie.arc.account.impl.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.account.impl.command.CardCommand;
import com.aie.arc.account.impl.event.CardEvent;
import com.aie.arc.account.impl.state.CardState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Slf4j
public class CardEntity extends EventSourcedBehaviorWithEnforcedReplies<CardCommand, CardEvent, CardState> {


    final private EntityContext<CardCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<CardCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(CardCommand.class, "CardEntity");

    CardEntity(EntityContext<CardCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }


    @Override
    public CommandHandlerWithReply<CardCommand, CardEvent, CardState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(CardCommand.GetCard.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.card))

                .onCommand(CardCommand.AddCard.class, (state, cmd) -> Effect()
                        .persist(new CardEvent.CardCreated(cmd.getCard()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(CardCommand.UpdateCard.class, (state, cmd) -> Effect()
                        .persist(new CardEvent.CardUpdated(cmd.getCard()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(CardCommand.DeactivateCard.class, (state, cmd) -> Effect()
                        .persist(new CardEvent.CardDeactivated(cmd.getCardId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(CardCommand.ActivateCard.class, (state, cmd) -> Effect()
                        .persist(new CardEvent.CardActivated(cmd.getCardId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<CardState, CardEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(CardEvent.CardCreated.class, (state, evt) -> CardState.create(evt.getCard()))
                .onEvent(CardEvent.CardUpdated.class, (state, evt) -> state.updateDetails(evt.getCard()))
                .onEvent(CardEvent.CardDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(CardEvent.CardActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public CardState emptyState() {
        return CardState.empty();
    }

    @Override
    public Set<String> tagsFor(CardEvent cardEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, CardEvent.TAG).apply(cardEvent);
    }

    public static Behavior<CardCommand> create(EntityContext<CardCommand> entityContext) {
        return new CardEntity(entityContext);
    }
}
