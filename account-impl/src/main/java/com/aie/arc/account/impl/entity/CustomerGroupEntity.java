package com.aie.arc.account.impl.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.account.impl.command.CustomerGroupCommand;
import com.aie.arc.account.impl.event.CustomerGroupEvent;
import com.aie.arc.account.impl.state.CustomerGroupState;
import com.lightbend.lagom.javadsl.api.transport.NotFound;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;
import java.util.function.Function;

public class CustomerGroupEntity extends EventSourcedBehaviorWithEnforcedReplies<CustomerGroupCommand, CustomerGroupEvent, CustomerGroupState> {

    final private EntityContext<CustomerGroupCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<CustomerGroupCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(CustomerGroupCommand.class, "CustomerGroupEntity");

    CustomerGroupEntity(EntityContext<CustomerGroupCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }


    @Override
    public CustomerGroupState emptyState() {
        return CustomerGroupState.empty();
    }

    @Override
    public CommandHandlerWithReply<CustomerGroupCommand, CustomerGroupEvent, CustomerGroupState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(CustomerGroupCommand.GetCustomerGroup.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.customerGroup))

                .onCommand(CustomerGroupCommand.CreateCustomerGroup.class, (state, cmd) -> Effect()
                        .persist(new CustomerGroupEvent.CustomerGroupCreated(cmd.getCustomerGroup()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(CustomerGroupCommand.UpdateCustomerGroup.class, (state, cmd) -> Effect()
                        .persist(new CustomerGroupEvent.CustomerGroupUpdated(cmd.getCustomerGroup()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(CustomerGroupCommand.DeactivateCustomerGroup.class, (state, cmd) -> Effect()
                        .persist(new CustomerGroupEvent.CustomerGroupDeactivated(cmd.getCustomerGroupId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(CustomerGroupCommand.ActivateCustomerGroup.class, (state, cmd) -> Effect()
                        .persist(new CustomerGroupEvent.CustomerGroupActivated(cmd.getCustomerGroupId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }


    @Override
    public EventHandler<CustomerGroupState, CustomerGroupEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(CustomerGroupEvent.CustomerGroupCreated.class, (state, evt) -> CustomerGroupState.create(evt.getCustomerGroup()))
                .onEvent(CustomerGroupEvent.CustomerGroupUpdated.class, (state, evt) -> state.updateDetails(evt.getCustomerGroup()))
                .onEvent(CustomerGroupEvent.CustomerGroupDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(CustomerGroupEvent.CustomerGroupActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public Set<String> tagsFor(CustomerGroupEvent customerGroupEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, CustomerGroupEvent.TAG).apply(customerGroupEvent);
    }

    public static Behavior<CustomerGroupCommand> create(EntityContext<CustomerGroupCommand> entityContext) {
        return new CustomerGroupEntity(entityContext);
    }
}
