package com.aie.arc.account.impl.entity;

import akka.Done;
import com.aie.arc.account.impl.command.ErrorProcessorCommand;
import com.aie.arc.account.impl.entity.entities.ErrorProcessor;
import com.aie.arc.account.impl.event.ErrorProcessorEvent;
import com.aie.arc.account.impl.state.ErrorProcessorState;
import com.aie.arc.account.impl.status.Status;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;

import java.util.Optional;

public class ErrorProcessorEntity extends PersistentEntity<ErrorProcessorCommand, ErrorProcessorEvent, ErrorProcessorState> {

    @Override
    public Behavior initialBehavior(Optional<ErrorProcessorState> snapshotState) {
        Status status = snapshotState.map(ErrorProcessorState::getStatus).orElse(Status.NOT_CREATED);
        switch (status) {
            case NOT_CREATED:
                return empty();
            case CREATED:
                if(snapshotState.isPresent())
                    return created(snapshotState.get());
                else
                    throw new IllegalStateException("Unknown status or snapshortState is empty : " + status);
            default:
                throw new IllegalStateException("Unknown status: " + status);
        }
    }

    @SuppressWarnings("unchecked")
    private Behavior empty() {
        BehaviorBuilder builder = newBehaviorBuilder(ErrorProcessorState.empty());
        builder.setReadOnlyCommandHandler(ErrorProcessorCommand.GetErrorProcessor.class, this::getErrorProcessor);
        builder.setCommandHandler(ErrorProcessorCommand.CreateErrorProcessor.class, (cmd, ctx) ->
                ctx.thenPersist(new ErrorProcessorEvent.ErrorProcessorCreated(new ErrorProcessor(cmd.getEventId(), cmd.getEventName(), cmd.getEventData(), cmd.getEventErrorMessage(), cmd.getLocalDateTime())), evt -> ctx.reply(Done.getInstance())));

        builder.setEventHandlerChangingBehavior(ErrorProcessorEvent.ErrorProcessorCreated.class, evt -> created(ErrorProcessorState.create(evt.getErrorProcessorVO())));

        return builder.build();
    }


    @SuppressWarnings("unchecked")
    private Behavior created(ErrorProcessorState currentState) {
        BehaviorBuilder builder = newBehaviorBuilder(currentState);
        builder.setReadOnlyCommandHandler(ErrorProcessorCommand.GetErrorProcessor.class, this::getErrorProcessor);

        return builder.build();

    }

    private void getErrorProcessor(ErrorProcessorCommand.GetErrorProcessor get, ReadOnlyCommandContext<Optional<ErrorProcessor>> ctx) {
        ctx.reply(state().errorProcessorVO);
    }
}