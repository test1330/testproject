package com.aie.arc.account.impl.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.account.impl.command.StoreCommand;
import com.aie.arc.account.impl.event.StoreEvent;
import com.aie.arc.account.impl.state.StoreState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;

public class StoreEntity extends EventSourcedBehaviorWithEnforcedReplies<StoreCommand, StoreEvent, StoreState> {

    final private EntityContext<StoreCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<StoreCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(StoreCommand.class, "StoreEntity");

    StoreEntity(EntityContext<StoreCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }


    @Override
    public CommandHandlerWithReply<StoreCommand, StoreEvent, StoreState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(StoreCommand.GetStore.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.storeVO))

                .onCommand(StoreCommand.CreateStore.class, (state, cmd) -> Effect()
                        .persist(new StoreEvent.StoreCreated(cmd.getStoreVO()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(StoreCommand.UpdateStore.class, (state, cmd) -> Effect()
                        .persist(new StoreEvent.StoreUpdated(cmd.getStoreVO()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .build();
    }

    @Override
    public EventHandler<StoreState, StoreEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(StoreEvent.StoreCreated.class, (state, evt) -> StoreState.create(evt.getStoreVO()))
                .onEvent(StoreEvent.StoreUpdated.class, (state, evt) -> state.updateDetails(evt.getStoreVO()))
                .build();
    }

    @Override
    public StoreState emptyState() {
        return StoreState.empty();
    }

    @Override
    public Set<String> tagsFor(StoreEvent storeEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, StoreEvent.TAG).apply(storeEvent);
    }

    public static Behavior<StoreCommand> create(EntityContext<StoreCommand> entityContext) {
        return new StoreEntity(entityContext);
    }
}