package com.aie.arc.account.impl.entity.entities;

import com.aie.arc.account.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.account.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.account.impl.entity.entities.vo.Profile;
import com.aie.arc.account.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

@Value
@JsonDeserialize
public class Account implements Jsonable {
    private String accountId;
    private String bmcisProfileId;
    private Integer accountNumber;
    private Profile profile;
    String bmRoleId;
    List<String> flashSaleLimitedIds;
    private List<String> addressIds;
    private String defaultShippingAddressId;
    private String defaultBillingAddressId;
    private List<String> cardIds;
    private String defaultCardId;
    private String storeId;
    private String myZip;
    private List<String> customerGroupIds;
    private boolean active;
    private boolean subscribed;
    private CreatedAt createdAt;
    private LastModifiedAt lastModifiedAt;
    private Status status;
    private String onlineStoreId;

    @JsonCreator
    public Account(String accountId, String bmcsProfileId, Integer accountNumber, Profile profile, String bmRoleId,
                   List<String> flashSaleLimitedIds, List<String> addressIds, String defaultShippingAddressId,
                   String defaultBillingAddressId, List<String> cardIds, String defaultCardId, String storeId,
                   String myZip, boolean active, boolean subscribe, CreatedAt createdAt, LastModifiedAt lastModifiedAt,
                   Status status,List<String> customerGroupIds, String onlineStoreId) {
        this.accountId = accountId;
        this.bmcisProfileId = bmcsProfileId;
        this.accountNumber = accountNumber;
        this.profile = profile;
        this.bmRoleId = bmRoleId;
        this.flashSaleLimitedIds = flashSaleLimitedIds;
        this.addressIds = addressIds;
        this.defaultShippingAddressId = defaultShippingAddressId;
        this.defaultBillingAddressId = defaultBillingAddressId;
        this.cardIds = cardIds;
        this.defaultCardId = defaultCardId;
        this.storeId = storeId;
        this.myZip=myZip;
        this.active = active;
        this.subscribed = subscribe;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
        this.status = status;
        this.customerGroupIds = customerGroupIds;
        this.onlineStoreId = onlineStoreId;
    }

    public Account addZipCode(String myZip) {
        return updateIfNotEmptyAccount(
                this.getAccountId(),
                this.getBmcisProfileId(),
                this.getAccountNumber(),
                this.getProfile(),
                this.getBmRoleId(),
                this.getFlashSaleLimitedIds(),
                this.getAddressIds(),
                this.getDefaultShippingAddressId(),
                this.getDefaultBillingAddressId(),
                this.getCardIds(),
                this.getDefaultCardId(),
                this.getStoreId(),
                myZip,
                this.isActive(),
                this.isSubscribed(),
                this.createdAt,
                new LastModifiedAt(LocalDateTime.now()),
                this.getStatus(),
                this.customerGroupIds,
                this.onlineStoreId
        );
    }

    public Account withDetails(Account details) {
        return updateIfNotEmptyAccount(
                details.getAccountId(),
                details.getBmcisProfileId(),
                details.getAccountNumber(),
                details.getProfile(),
                details.getBmRoleId(),
                details.getFlashSaleLimitedIds(),
                details.getAddressIds(),
                details.getDefaultShippingAddressId(),
                details.getDefaultBillingAddressId(),
                details.getCardIds(),
                details.getDefaultCardId(),
                details.getStoreId(),
                details.getMyZip(),
                details.isActive(),
                details.isSubscribed(),
                details.createdAt,
                details.getLastModifiedAt(),
                details.getStatus(),
                details.getCustomerGroupIds(),
                details.getOnlineStoreId()
        );
    }

    public Account withDetails(String storeId) {
        return updateIfNotEmptyAccount(
                this.getAccountId(),
                this.getBmcisProfileId(),
                this.getAccountNumber(),
                this.getProfile(),
                this.getBmRoleId(),
                this.getFlashSaleLimitedIds(),
                this.getAddressIds(),
                this.getDefaultShippingAddressId(),
                this.getDefaultBillingAddressId(),
                this.getCardIds(),
                this.getMyZip(),
                this.getDefaultCardId(),
                storeId,
                this.isActive(),
                this.isSubscribed(),
                this.getCreatedAt(),
                new LastModifiedAt(LocalDateTime.now()),
                this.status,
                this.customerGroupIds,
                this.onlineStoreId
        );
    }

    public Account updateDefaultPaymentDetails(String cardId) {
        return updateIfNotEmptyAccount(
                this.getAccountId(),
                this.getBmcisProfileId(),
                this.getAccountNumber(),
                this.getProfile(),
                this.getBmRoleId(),
                this.getFlashSaleLimitedIds(),
                this.getAddressIds(),
                this.getDefaultShippingAddressId(),
                this.getDefaultBillingAddressId(),
                this.getCardIds(),
                cardId,
                this.getStoreId(),
                this.getMyZip(),
                this.isActive(),
                this.isSubscribed(),
                this.getCreatedAt(),
                new LastModifiedAt(LocalDateTime.now()),
                this.status,
                this.customerGroupIds,
                this.onlineStoreId
        );
    }

    public Account withDetails(boolean value) {
        return updateIfNotEmptyAccount(
                this.getAccountId(),
                this.getBmcisProfileId(),
                this.getAccountNumber(),
                this.getProfile(),
                this.getBmRoleId(),
                this.getFlashSaleLimitedIds(),
                this.getAddressIds(),
                this.getDefaultShippingAddressId(),
                this.getDefaultBillingAddressId(),
                this.getCardIds(),
                this.getDefaultCardId(),
                this.getStoreId(),
                this.getMyZip(),
                value,
                this.isSubscribed(),
                this.getCreatedAt(),
                new LastModifiedAt(LocalDateTime.now()),
                this.status,
                this.customerGroupIds,
                this.onlineStoreId
        );
    }

    public Account updateIfNotEmptyAccount(String customerAccountId, String bmcsProfileId, Integer accountNumber,
                                           Profile profile,String bmRoleId, List<String> flashSaleLimitedIds,
                                           List<String> billingAddressIds, String defaultShippingAddressId,
                                           String defaultBillingAddressId, List<String> cardIds, String defaultCardId,
                                           String storeId, String myZip, boolean subscribe, boolean active,
                                           CreatedAt createdAt, LastModifiedAt lastModifiedAt, Status status,
                                           List<String> customerGroupIds,String onlineStoreId) {
        return new Account(
                updateIfFoundInRequest(customerAccountId, getAccountId()),
                bmcsProfileId,
                accountNumber != null ? accountNumber : getAccountNumber(),
                profile != null ? profile : getProfile(),
                bmRoleId,
                flashSaleLimitedIds,
                updateIfFoundInRequest(billingAddressIds, getAddressIds()),
                defaultShippingAddressId,
                defaultBillingAddressId,
                cardIds,
                defaultCardId,
                updateIfFoundInRequest(storeId, getStoreId()),
                myZip,
                active,
                subscribe,
                createdAt,
                lastModifiedAt,
                status,
                customerGroupIds,
                onlineStoreId
        );
    }

    //utils
    private String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

    //utils
    private <T> List<T> updateIfFoundInRequest(List<T> valueInRequest, List<T> valueInCurrentState) {
        if (valueInRequest == null) return valueInCurrentState;
        return !valueInRequest.isEmpty() ? valueInRequest : valueInCurrentState;
    }

}