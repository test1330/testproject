package com.aie.arc.account.impl.entity.entities;

import com.aie.arc.account.api.request.AccountAddressRequest;
import com.aie.arc.account.api.request.CreateBusinessUserAddressRequest;
import com.aie.arc.account.api.request.UpdateAccountAddressRequest;
import com.aie.arc.account.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.account.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.account.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * The stateId for the {@link Address} entity.
 */
@Value
@JsonDeserialize
public final class Address implements Jsonable {
    private String addressId;
    private String accountId;
    private String firstName;
    private String lastName;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String country;
    private String postalCode;
    private String phone;
    private boolean defaultShipping;
    private boolean defaultBilling;
    private boolean active;
    private CreatedAt createdAt;
    private LastModifiedAt lastModifiedAt;
    private Status status;
    private String fipscounty;
    private String fipsstate;
    private String phonenumberAlt;
    private boolean avsvalid;
    private boolean skipavsvalidation;

    @JsonCreator
    public Address(String addressId, String accountId, String firstName, String lastName, String address1, String address2, String city, String state, String country, String postalCode, String phone, boolean defaultShipping, boolean defaultBilling, boolean active, CreatedAt createdAt, LastModifiedAt lastModifiedAt, Status status, String fipscounty, String fipsstate, String phonenumberAlt, boolean avsvalid, boolean skipavsvalidation) {
        this.addressId = addressId;
        this.accountId = accountId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.state = state;
        this.country = country;
        this.postalCode = postalCode;
        this.phone = phone;
        this.defaultShipping = defaultShipping;
        this.defaultBilling = defaultBilling;
        this.active = active;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
        this.status = status;
        this.fipscounty = fipscounty;
        this.fipsstate = fipsstate;
        this.phonenumberAlt = phonenumberAlt;
        this.avsvalid = avsvalid;
        this.skipavsvalidation = skipavsvalidation;
    }

    public static Address createAddressByCustomerRequest(String addressId, String accountId,
                                                         AccountAddressRequest address) {
        return new Address(
                addressId,
                accountId,
                address.getFirstName(),
                address.getLastName(),
                address.getAddress1(),
                address.getAddress2(),
                address.getCity(),
                address.getState(),
                address.getCountry(),
                address.getPostalCode(),
                address.getPhone(),
                address.isDefaultShipping(),
                address.isDefaultBilling(),
                true,
                new CreatedAt(LocalDateTime.now()),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED,
                address.getFipscounty(),
                address.getFipsstate(),
                address.getPhonenumberAlt(),
                address.isAvsvalid(),
                address.isSkipavsvalidation()
        );
    }

    public static Address updateAddressByCustomerRequest(Address address, UpdateAccountAddressRequest addressReq) {
        return new Address(
                address.getAddressId(),
                address.getAccountId(),
                updateIfDataInRequest(addressReq.getFirstName(), address.getFirstName()),
                updateIfDataInRequest(addressReq.getLastName(), address.getLastName()),
                updateIfDataInRequest(addressReq.getAddress1(), address.getAddress1()),
                addressReq.getAddress2(),
                updateIfDataInRequest(addressReq.getCity(), address.getCity()),
                updateIfDataInRequest(addressReq.getState(), address.getState()),
                updateIfDataInRequest(addressReq.getCountry(), address.getCountry()),
                updateIfDataInRequest(addressReq.getPostalCode(), address.getPostalCode()),
                updateIfDataInRequest(addressReq.getPhone(), address.getPhone()),
                address.isDefaultShipping(),
                address.isDefaultBilling(),
                address.isActive(),
                address.getCreatedAt(),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED,
                updateIfDataInRequest(addressReq.getFipscounty(), address.getFipscounty()),
                updateIfDataInRequest(addressReq.getFipsstate(), address.getFipsstate()),
                updateIfDataInRequest(addressReq.getPhonenumberAlt(), address.getPhonenumberAlt()),
                address.isAvsvalid(),
                address.isSkipavsvalidation()
        );
    }


    public Address withDetails(Address address) {
        return updateIfNotEmptyAddress(
                addressId,
                accountId,
                address.getFirstName(),
                address.getLastName(),
                address.getAddress1(),
                address.getAddress2(),
                address.getCity(),
                address.getState(),
                address.getCountry(),
                address.getPostalCode(),
                address.getPhone(),
                address.isDefaultShipping(),
                address.isDefaultBilling(),
                address.isActive(),
                address.getCreatedAt(),
                address.getLastModifiedAt(),
                Status.CREATED,
                address.getFipscounty(),
                address.getFipsstate(),
                address.getPhonenumberAlt(),
                address.isAvsvalid(),
                address.isSkipavsvalidation()
        );
    }


    public Address withDetails(boolean active) {
        return updateIfNotEmptyAddress(
                this.getAddressId(),
                this.getAccountId(),
                this.getFirstName(),
                this.getLastName(),
                this.getAddress1(),
                this.getAddress2(),
                this.getCity(),
                this.getState(),
                this.getCountry(),
                this.getPostalCode(),
                this.getPhone(),
                this.isDefaultShipping(),
                this.isDefaultBilling(),
                active,
                this.getCreatedAt(),
                this.getLastModifiedAt(),
                Status.CREATED,
                this.getFipscounty(),
                this.getFipsstate(),
                this.getPhonenumberAlt(),
                this.isAvsvalid(),
                this.isSkipavsvalidation()
        );
    }

    private Address updateIfNotEmptyAddress(String addressId, String accountId, String firstName, String lastName, String address1,
                                            String address2, String city, String state, String country, String postalCode, String phone,
                                            boolean defaultShipping, boolean defaultBilling, boolean active,
                                            CreatedAt createdAt, LastModifiedAt lastModifiedAt, Status status, String fipscounty,
                                            String fipsstate, String phonenumberAlt, boolean avsvalid, boolean skipavsvalidation) {

        return new Address(
                updateIfFoundInRequest(addressId, getAddressId()),
                updateIfFoundInRequest(accountId, getAccountId()),
                updateIfFoundInRequest(firstName, getFirstName()),
                updateIfFoundInRequest(lastName, getLastName()),
                updateIfFoundInRequest(address1, getAddress1()),
                address2,
                updateIfFoundInRequest(city, getCity()),
                updateIfFoundInRequest(state, getState()),
                updateIfFoundInRequest(country, getCountry()),
                updateIfFoundInRequest(postalCode, getPostalCode()),
                updateIfFoundInRequest(phone, getPhone()),
                defaultShipping,
                defaultBilling,
                active,
                createdAt,
                lastModifiedAt,
                status,
                updateIfDataInRequest(fipscounty, getFipscounty()),
                updateIfDataInRequest(fipsstate, getFipsstate()),
                updateIfDataInRequest(phonenumberAlt, getPhonenumberAlt()),
                avsvalid,
                skipavsvalidation
        );

    }

    //utils
    private String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

    //utils
    private static String updateIfDataInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

}