package com.aie.arc.account.impl.entity.entities;

import com.aie.arc.account.api.request.*;
import com.aie.arc.account.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.account.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.account.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * The stateId for the {@link Address} entity.
 */
@Value
@JsonDeserialize
public final class BusinessAddress implements Jsonable {
    private String addressId;
    private String firstName;
    private String lastName;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String country;
    private String postalCode;
    private String phone;
    private boolean isResidenceAddress;
    private boolean isOfficeAddress;
    private boolean active;
    private com.aie.arc.account.impl.entity.entities.vo.CreatedAt createdAt;
    private com.aie.arc.account.impl.entity.entities.vo.LastModifiedAt lastModifiedAt;
    private Status status;

    @JsonCreator
    public BusinessAddress(String addressId, String firstName, String lastName, String address1, String address2, String city, String state, String country, String postalCode, String phone, boolean isResidenceAddress, boolean isOfficeAddress, boolean active, CreatedAt createdAt, LastModifiedAt lastModifiedAt, Status status) {
        this.addressId = addressId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.state = state;
        this.country = country;
        this.postalCode = postalCode;
        this.phone = phone;
        this.isResidenceAddress = isResidenceAddress;
        this.isOfficeAddress = isOfficeAddress;
        this.active = active;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
        this.status = status;
    }

    public static BusinessAddress createAddressByBusinessUserRequest(String addressId, CreateBusinessUserAddressRequest address) {
        return new BusinessAddress(
                addressId,
                address.getFirstName(),
                address.getLastName(),
                address.getAddress1(),
                address.getAddress2(),
                address.getCity(),
                address.getState(),
                address.getCountry(),
                address.getPostalCode(),
                address.getPhone(),
                address.getIsResidenceAddress(),
                address.getIsOfficeAddress(),
                true,
                new CreatedAt(LocalDateTime.now()),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED
        );
    }

    public static BusinessAddress updateAddressByBusinessUserRequest(BusinessAddress address, UpdateBusinessUserAddressRequest addressReq) {
        return new BusinessAddress(
                address.getAddressId(),
                updateIfDataInRequest(addressReq.getFirstName(), address.getFirstName()),
                updateIfDataInRequest(addressReq.getLastName(), address.getLastName()),
                updateIfDataInRequest(addressReq.getAddress1(), address.getAddress1()),
                updateIfDataInRequest(addressReq.getAddress2(), address.getAddress2()),
                updateIfDataInRequest(addressReq.getCity(), address.getCity()),
                updateIfDataInRequest(addressReq.getState(), address.getState()),
                updateIfDataInRequest(addressReq.getCountry(), address.getCountry()),
                updateIfDataInRequest(addressReq.getPostalCode(), address.getPostalCode()),
                updateIfDataInRequest(addressReq.getPhone(), address.getPhone()),
                address.isResidenceAddress(),
                address.isOfficeAddress(),
                address.isActive(),
                address.getCreatedAt(),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED
        );
    }

    public BusinessAddress withDetails(BusinessAddress address) {
        return updateIfNotEmptyAddress(
                addressId,
                address.getFirstName(),
                address.getLastName(),
                address.getAddress1(),
                address.getAddress2(),
                address.getCity(),
                address.getState(),
                address.getCountry(),
                address.getPostalCode(),
                address.getPhone(),
                address.isResidenceAddress(),
                address.isOfficeAddress(),
                address.isActive(),
                address.getCreatedAt(),
                address.getLastModifiedAt(),
                Status.CREATED
        );
    }

    public BusinessAddress withDetails(boolean value) {
        return updateIfNotEmptyAddress(
                this.getAddressId(),
                this.getFirstName(),
                this.getLastName(),
                this.getAddress1(),
                this.getAddress2(),
                this.getCity(),
                this.getState(),
                this.getCountry(),
                this.getPostalCode(),
                this.getPhone(),
                this.isOfficeAddress(),
                this.isResidenceAddress(),
                active,
                this.getCreatedAt(),
                this.getLastModifiedAt(),
                Status.CREATED
        );
    }

    public BusinessAddress updateIfNotEmptyAddress(String addressId,String firstName, String lastName,
                                                   String address1,String address2, String city, String state, String country,
                                                   String postalCode, String phone,boolean defaultShipping, boolean defaultBilling,
                                                   boolean active,com.aie.arc.account.impl.entity.entities.vo.CreatedAt createdAt,
                                                   com.aie.arc.account.impl.entity.entities.vo.LastModifiedAt lastModifiedAt,
                                                   Status status) {
        return new BusinessAddress(
                updateIfFoundInRequest(addressId, getAddressId()),
                updateIfFoundInRequest(firstName, getFirstName()),
                updateIfFoundInRequest(lastName, getLastName()),
                updateIfFoundInRequest(address1, getAddress1()),
                updateIfFoundInRequest(address2, getAddress2()),
                updateIfFoundInRequest(city, getCity()),
                updateIfFoundInRequest(state, getState()),
                updateIfFoundInRequest(country, getCountry()),
                updateIfFoundInRequest(postalCode, getPostalCode()),
                updateIfFoundInRequest(phone, getPhone()),
                defaultShipping,
                defaultBilling,
                active,
                createdAt,
                lastModifiedAt,
                status);
    }

    //utils
    private String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

    //utils
    private static String updateIfDataInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }
}