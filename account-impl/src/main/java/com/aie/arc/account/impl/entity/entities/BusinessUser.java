package com.aie.arc.account.impl.entity.entities;

import com.aie.arc.account.api.request.UpdateBusinessUserRequest;
import com.aie.arc.account.impl.entity.entities.vo.BusinessProfile;
import com.aie.arc.account.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.account.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.account.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.aie.arc.account.impl.util.MessageConstants.DATE_TIME_FORMAT;

@Value
@JsonDeserialize
public class BusinessUser implements Jsonable {
    private String businessUserId;
    private String organizationId;
    private BusinessProfile profile;
    private String designation;
    private List<String> roleIds;
    private boolean active;
    private CreatedAt createdAt;
    private LastModifiedAt lastModifiedAt;
    private Status status;

    public BusinessUser() {
        this.businessUserId = null;
        this.organizationId = null;
        this.profile = null;
        this.designation = null;
        this.roleIds = null;
        this.active = false;
        this.createdAt = null;
        this.lastModifiedAt = null;
        this.status = null;
    }
    @JsonCreator
    public BusinessUser(String businessUserId, String organizationId, BusinessProfile profile, String designation, List<String> roleIds, boolean active, CreatedAt createdAt, LastModifiedAt lastModifiedAt, Status status) {
        this.businessUserId = businessUserId;
        this.organizationId = organizationId;
        this.profile = profile;
        this.designation = designation;
        this.roleIds = roleIds;
        this.active = active;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
        this.status = status;
    }

      public static BusinessUser updateBusinessUser(BusinessUser businessUser, UpdateBusinessUserRequest request) {
       return new BusinessUser(
               businessUser.getBusinessUserId(),
               businessUser.getOrganizationId(),
               businessProfileRequestToBusinessProfile(request.getProfile(),businessUser),
               updateIfFoundInRequest(request.getDesignation(), businessUser.getDesignation()),
               updateIfFoundInRequest(request.getRoleIds(), businessUser.getRoleIds()),
               businessUser.isActive(),
               businessUser.getCreatedAt(),
               new LastModifiedAt(LocalDateTime.now()),
               Status.CREATED
       );

    }

    public BusinessUser withDetails(BusinessUser details) {
        return updateIfNotEmptyBusinessUserAccount(
                details.getBusinessUserId(),
                details.getOrganizationId(),
                details.getProfile(),
                details.getDesignation(),
                details.getRoleIds(),
                details.isActive(),
                details.getCreatedAt(),
                details.getLastModifiedAt(),
                details.status
        );
    }

    public BusinessUser withDetails(BusinessProfile profile) {
        return updateIfNotEmptyBusinessUserAccount(
                this.getBusinessUserId(),
                this.getOrganizationId(),
                profile,
                this.getDesignation(),
                this.getRoleIds(),
                this.isActive(),
                this.getCreatedAt(),
                this.getLastModifiedAt(),
                this.status
        );
    }

    public BusinessUser withDetails(boolean value) {
        return updateIfNotEmptyBusinessUserAccount(
                this.getBusinessUserId(),
                this.getOrganizationId(),
                this.getProfile(),
                this.getDesignation(),
                this.getRoleIds(),
                value,
                this.getCreatedAt(),
                this.getLastModifiedAt(),
                this.status
        );
    }

    public BusinessUser updateIfNotEmptyBusinessUserAccount(String businessUserAccountId, String organizationId, BusinessProfile profile, String designation, List<String> roleIds, boolean active, CreatedAt createdAt, LastModifiedAt lastModifiedAt, Status status) {
        return new BusinessUser(
                updateIfFoundInRequest(businessUserAccountId, getBusinessUserId()),
                updateIfFoundInRequest(organizationId,getOrganizationId()),
                profile != null ? profile : getProfile(),
                updateIfFoundInRequest(designation, getDesignation()),
                updateIfFoundInRequest(roleIds, getRoleIds()),
                active,
                createdAt,
                lastModifiedAt,
                Status.CREATED
        );
    }

    //utils
    private static String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

    //utils
    private static <T> List<T> updateIfFoundInRequest(List<T> valueInRequest, List<T> valueInCurrentState) {
        if (valueInRequest == null ) return valueInCurrentState;
        return !valueInRequest.isEmpty() ? valueInRequest : valueInCurrentState;
    }

    public static BusinessProfile businessProfileRequestToBusinessProfile(com.aie.arc.account.api.request.dto.BusinessProfile profile, BusinessUser user)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
        return new BusinessProfile(
                updateIfFoundInRequest(profile.getFirstName(),user.getProfile().getFirstName()),
                updateIfFoundInRequest(profile.getUserName(),user.getProfile().getUserName()),
                updateIfFoundInRequest(profile.getMiddleName(),user.getProfile().getMiddleName()),
                updateIfFoundInRequest(profile.getLastName(),user.getProfile().getLastName()),
                user.getProfile().getEmail(),
                user.getProfile().isEmailVerified(),
                updateIfFoundInRequest(profile.getPhone(),user.getProfile().getPhone()),
                user.getProfile().getCreatedAt(),
                formatter.format(LocalDateTime.now()));
    }

}
