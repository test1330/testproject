package com.aie.arc.account.impl.entity.entities;

import com.aie.arc.account.api.request.CardRequest;
import com.aie.arc.account.api.request.UpdateCardRequest;
import com.aie.arc.account.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.account.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.account.impl.status.Status;
import com.aie.arc.account.impl.util.MessageConstants;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.time.LocalDateTime;

@Value
@JsonDeserialize
public class Card implements Jsonable {
    private String cardId;
    private String accountId;
    private String firstName;
    private String lastName;
    private boolean defaultCreditCard;
    private String cardNumber;
    private String cardType;
    private String subscriptionId;
    private String expirationMonth;
    private String expirationYear;
    private String cardNickName;
    private boolean active;
    private CreatedAt createdAt;
    private LastModifiedAt lastModifiedAt;
    private Status status;

    @JsonCreator
    public Card(String cardId, String accountId, String firstName, String lastName, boolean defaultCreditCard,
                String cardNumber, String cardType, String subscriptionId,
                String expirationMonth, String expirationYear,String cardNickName, boolean active, CreatedAt createdAt,
                LastModifiedAt lastModifiedAt, Status status) {
        this.cardId = cardId;
        this.accountId = accountId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.defaultCreditCard = defaultCreditCard;
        this.cardNumber = cardNumber;
        this.cardType = cardType;
        this.subscriptionId = subscriptionId;
        this.expirationMonth = expirationMonth;
        this.expirationYear = expirationYear;
        this.cardNickName = cardNickName;
        this.active = active;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
        this.status = status;
    }

    public static Card createCardByRequest(String cardId, String accountId, String encryptedCardNumber,
                                           String subscriptionId, String cardNickName, CardRequest cardRequest) {
        return new Card(
                cardId,
                accountId,
                cardRequest.getFirstName(),
                cardRequest.getLastName(),
                cardRequest.isDefaultCreditCard(),
                encryptedCardNumber,
                cardRequest.getCardType(),
                subscriptionId,
                StringUtils.stripStart(cardRequest.getExpirationMonth(), MessageConstants.ZERO),
                cardRequest.getExpirationYear(),
                cardNickName,
                cardRequest.getActive(),
                new CreatedAt(LocalDateTime.now()),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED
        );
    }

    public static Card updateCardByRequest(Card card, UpdateCardRequest cardRequest) {
        return new Card(
                card.getCardId(),
                card.getAccountId(),
                StringUtils.isNotBlank(cardRequest.getFirstName())? cardRequest.getFirstName() : card.getFirstName(),
                StringUtils.isNotBlank(cardRequest.getLastName())? cardRequest.getLastName() : card.getLastName(),
                cardRequest.isDefaultCreditCard(),
                card.getCardNumber(),
                card.getCardType(),
                card.getSubscriptionId(),
                StringUtils.isNotBlank(cardRequest.getExpirationMonth())?
                StringUtils.stripStart(cardRequest.getExpirationMonth(), MessageConstants.ZERO) : card.getExpirationMonth(),
                StringUtils.isNotBlank(cardRequest.getExpirationYear())? cardRequest.getExpirationYear() : card.getExpirationYear(),
                card.getCardNickName(),
                card.isActive(),
                card.getCreatedAt(),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED
        );
    }

    public Card withDetails(Card card) {
        return updateIfNotEmptyCard(
                card.getCardId(),
                card.getAccountId(),
                card.getFirstName(),
                card.getLastName(),
                card.isDefaultCreditCard(),
                card.getCardNumber(),
                card.getCardType(),
                card.getSubscriptionId(),
                card.getCardNickName(),
                card.getExpirationMonth(),
                card.getExpirationYear(),
                card.active,
                card.getCreatedAt(),
                card.getLastModifiedAt(),
                card.getStatus()
        );
    }

    public Card withDetails(boolean value) {
        return updateIfNotEmptyCard(
                this.getCardId(),
                this.getAccountId(),
                this.getFirstName(),
                this.getLastName(),
                this.isDefaultCreditCard(),
                this.getCardNumber(),
                this.getCardType(),
                this.getSubscriptionId(),
                this.getCardNickName(),
                this.getExpirationMonth(),
                this.getExpirationYear(),
                value,
                this.getCreatedAt(),
                this.getLastModifiedAt(),
                this.getStatus()
        );
    }

    public Card updateIfNotEmptyCard(String cardId, String accountId, String firstName, String lastName,
                                     boolean defaultCreditCard, String cardNumber, String cardType,
                                     String subscriptionId, String cardNickName,
                                     String expirationMonth, String expirationYear , boolean active, CreatedAt createdAt,
                                     LastModifiedAt lastModifiedAt, Status status) {
        return new Card(
                updateIfNotFoundInRequest(cardId, getCardId()),
                updateIfFoundInRequest(accountId, getAccountId()),
                updateIfNotFoundInRequest(firstName, getFirstName()),
                updateIfNotFoundInRequest(lastName, getLastName()),
                defaultCreditCard,
                updateIfFoundInRequest(cardNumber, getCardNumber()),
                updateIfFoundInRequest(cardType, getCardType()),
                updateIfFoundInRequest(subscriptionId, getSubscriptionId()),
                updateIfFoundInRequest(expirationMonth, getExpirationMonth()),
                updateIfFoundInRequest(expirationYear, getExpirationYear()),
                updateIfFoundInRequest(cardNickName, getCardNickName()),
                active,
                createdAt,
                lastModifiedAt,
                Status.CREATED
        );
    }

    //utils
    private String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

    private static String updateIfNotFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

}
