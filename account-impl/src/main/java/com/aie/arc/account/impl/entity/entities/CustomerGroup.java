package com.aie.arc.account.impl.entity.entities;

import com.aie.arc.account.api.common.request.LocalisedFields;
import com.aie.arc.account.api.request.CreateCustomerGroupRequest;
import com.aie.arc.account.api.request.UpdateCustomerGroupRequest;
import com.aie.arc.account.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.account.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.account.impl.entity.entities.vo.LocalizedFields;
import com.aie.arc.account.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Value
@JsonDeserialize
public class CustomerGroup implements Jsonable {
    private String customerGroupId;
    private Map<String, LocalizedFields> localization;
    private List<String> customerIds;
    private Integer recency;
    private Integer frequency;
    private Double monetary;
    private boolean active;
    private CreatedAt createdAt;
    private LastModifiedAt lastModifiedAt;
    private Status status;


    @JsonCreator
    public CustomerGroup(String customerGroupId, Map<String, LocalizedFields> localization, List<String> customerIds, Integer recency, Integer frequency, Double monetary, boolean active, CreatedAt createdAt, LastModifiedAt lastModifiedAt, Status status) {
        this.customerGroupId = customerGroupId;
        this.localization = localization;
        this.customerIds = customerIds;
        this.recency = recency;
        this.frequency = frequency;
        this.monetary = monetary;
        this.active = active;
        this.createdAt=createdAt;
        this.lastModifiedAt=lastModifiedAt;
        this.status = status;
    }

    public static CustomerGroup createCustomerGroupByRequest(String customerGroupId, Map<String, LocalizedFields> localizedFieldsVOMap, List<String> customerIds, CreateCustomerGroupRequest customerGroupRequest) {
        return new CustomerGroup(customerGroupId, localizedFieldsVOMap, customerIds, customerGroupRequest.getRecency(), customerGroupRequest.getFrequency(), customerGroupRequest.getMonetary(),customerGroupRequest.getActive(), new CreatedAt(LocalDateTime.now()),
                new LastModifiedAt(LocalDateTime.now()), Status.CREATED);
    }

    public static CustomerGroup updateCustomerGroupByRequest(CustomerGroup customerGroup, List<String> customerIds, UpdateCustomerGroupRequest customerGroupRequest) {
        Map<String, LocalizedFields> currentLocalization = customerGroup.getLocalization();
        Map<String, LocalisedFields> requestedLocalization = customerGroupRequest.getLocalization();
        requestedLocalization.forEach((code, value) -> currentLocalization.put(code, new LocalizedFields(value.getName(), value.getDescription())));
        return new CustomerGroup(
                customerGroup.getCustomerGroupId(),
                currentLocalization,
                customerIds,
                customerGroupRequest.getRecency() != null ? customerGroupRequest.getRecency() : customerGroup.getRecency(),
                customerGroupRequest.getFrequency() != null ? customerGroupRequest.getFrequency() : customerGroup.getRecency(),
                customerGroupRequest.getMonetary() != null ? customerGroupRequest.getMonetary() : customerGroup.getMonetary(),
                customerGroupRequest.getActive(),
                customerGroup.getCreatedAt(),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED);
    }

    public CustomerGroup withDetails(CustomerGroup customerGroup) {
        return updateIfNotEmptyCustomerGroup(
                customerGroup.getCustomerGroupId(),
                customerGroup.localization,
                customerGroup.getCustomerIds(),
                customerGroup.getRecency(),
                customerGroup.getFrequency(),
                customerGroup.getMonetary(),
                customerGroup.isActive(),
                customerGroup.getCreatedAt(),
                customerGroup.getLastModifiedAt(),
                customerGroup.getStatus()
        );
    }
    public CustomerGroup withDetails(boolean value) {
        return updateIfNotEmptyCustomerGroup(
                this.getCustomerGroupId(),
                this.localization,
                this.getCustomerIds(),
                this.getRecency(),
                this.getFrequency(),
                this.getMonetary(),
                value,
                this.getCreatedAt(),
                this.getLastModifiedAt(),
                this.getStatus()
        );
    }

    public CustomerGroup updateIfNotEmptyCustomerGroup(String id, Map<String, LocalizedFields> localizedFieldsVOMap, List<String> customerIds, int recency, int frequency, double monetary,boolean active,CreatedAt createdAt,LastModifiedAt lastModifiedAt, Status status) {
        return new CustomerGroup(
                updateIfFoundInRequest(id, getCustomerGroupId()),
                localizedFieldsVOMap.isEmpty() ? getLocalization() : localizedFieldsVOMap,
                updateIfFoundInRequest(customerIds, getCustomerIds()),
                recency != 0 ? recency : getRecency(),
                frequency != 0 ? frequency : getFrequency(),
                monetary != 0 ? monetary : getMonetary(),
                active,
                createdAt,
                lastModifiedAt,
                Status.CREATED
        );
    }

    //utils
    private String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

    //utils
    private <T> List<T> updateIfFoundInRequest(List<T> valueInRequest, List<T> valueInCurrentState) {
        if (valueInRequest == null) return valueInCurrentState;
        return !valueInRequest.isEmpty() ? valueInRequest : valueInCurrentState;
    }
}
