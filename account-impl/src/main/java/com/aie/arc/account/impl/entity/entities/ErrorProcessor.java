package com.aie.arc.account.impl.entity.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.io.Serializable;
import java.time.LocalDateTime;

@Value
@JsonDeserialize
public class ErrorProcessor implements Jsonable {
    private String eventId;
    private String eventName;
    private String eventData;
    private String eventErrorMessage;
    private LocalDateTime localDateTime;

    @JsonCreator
    public ErrorProcessor(String eventId, String eventName, String eventData, String eventErrorMessage, LocalDateTime localDateTime) {
        this.eventId = eventId;
        this.eventName = eventName;
        this.eventData = eventData;
        this.eventErrorMessage = eventErrorMessage;
        this.localDateTime = localDateTime;
    }
}
