package com.aie.arc.account.impl.entity.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.util.List;

@Value
@JsonDeserialize
public class ListWrapper implements Jsonable {
    private List<String> list;
    private Integer totalSize;

    @JsonCreator
    public ListWrapper(List<String> list, Integer totalSize) {
        this.list = list;
        this.totalSize = totalSize;
    }
}
