package com.aie.arc.account.impl.entity.entities;

import com.aie.arc.account.impl.entity.entities.vo.LocalizedFields;
import com.aie.arc.account.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Value
@JsonDeserialize
public class Store  implements Jsonable {
    private String storeId;
    private String code;
    private Map<String, LocalizedFields> localization;
    LocalDateTime timestamp;
    private boolean active;
    private Status status;

    @JsonCreator
    public Store(String storeId, String code, Map<String, LocalizedFields> localization, LocalDateTime timestamp, boolean active, Status status) {
        this.storeId = storeId;
        this.code = code;
        this.localization = localization;
        this.timestamp = timestamp;
        this.active = active;
        this.status = status;
    }

    public Store withDetails(Store details) {
        return updateIfNotEmptyShippingStore(details.getStoreId(), details.getCode(), details.getLocalization(),details.getTimestamp(),details.active,details.getStatus());
    }

    private Store updateIfNotEmptyShippingStore(String storeId, String code, Map<String, LocalizedFields> localizedFieldsVOMap,LocalDateTime timestamp, boolean active,Status status) {
        return new Store(
                updateIfFoundInRequest(storeId, getStoreId()),
                updateIfFoundInRequest(code, getCode()),
                localizedFieldsVOMap != null ? localizedFieldsVOMap : getLocalization(),
                timestamp != null ? timestamp : getTimestamp(),
              /*  createdAt,
                lastModifiedAt,*/
                true,
                Status.CREATED

        );
    }

    //utils
    private String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

    //utils
    private <T> List<T> updateIfFoundInRequest(List<T> valueInRequest, List<T> valueInCurrentState) {
        if (valueInRequest == null) return valueInCurrentState;
        return !valueInRequest.isEmpty() ? valueInRequest : valueInCurrentState;
    }
}