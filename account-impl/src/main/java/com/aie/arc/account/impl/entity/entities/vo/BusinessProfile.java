package com.aie.arc.account.impl.entity.entities.vo;

import com.aie.arc.account.api.request.UpdateBusinessUserProfileRequest;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@Value
@JsonDeserialize
public class BusinessProfile {
    private String firstName;
    private String userName;
    private String middleName;
    private String lastName;
    private String email;
    private boolean emailVerified;
    private String phone;
    private String createdAt;
    private String lastModifiedAt;

    public BusinessProfile() {
        this.userName = null;
        this.firstName = null;
        this.middleName = null;
        this.lastName = null;
        this.email = null;
        this.emailVerified = false;
        this.phone = null;
        this.createdAt = null;
        this.lastModifiedAt = null;
    }
    @JsonCreator
    public BusinessProfile(String firstName, String userName, String middleName, String lastName, String email, Boolean emailVerified, String phone, String createdAt, String lastModifiedAt) {
        this.firstName = firstName;
        this.userName = userName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.email = email;
        this.emailVerified = emailVerified;
        this.phone = phone;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
    }

    public static BusinessProfile updateBusinessProfile(UpdateBusinessUserProfileRequest request, BusinessProfile profile, String lastModifiedAt) {
        return new BusinessProfile(
                updateIfFoundInRequest(request.getProfile().getFirstName(), profile.getFirstName()),
                updateIfFoundInRequest(request.getProfile().getUserName(), profile.getUserName()),
                updateIfFoundInRequest(request.getProfile().getMiddleName(), profile.getMiddleName()),
                updateIfFoundInRequest(request.getProfile().getLastName(), profile.getLastName()),
                updateIfFoundInRequest(request.getProfile().getEmail(), profile.getEmail()),
                profile.isEmailVerified(),
                updateIfFoundInRequest(request.getProfile().getPhone(), profile.getPhone()),
                profile.getCreatedAt(),
                updateIfFoundInRequest(lastModifiedAt, profile.getLastModifiedAt())
        );
    }

    //utils
    private  static String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

    //utils
    private <T> List<T> updateIfFoundInRequest(List<T> valueInRequest, List<T> valueInCurrentState) {
        if (valueInRequest == null) return valueInCurrentState;
        return !valueInRequest.isEmpty() ? valueInRequest : valueInCurrentState;
    }
}
