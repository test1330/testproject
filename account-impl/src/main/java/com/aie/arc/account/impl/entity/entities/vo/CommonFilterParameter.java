package com.aie.arc.account.impl.entity.entities.vo;

import lombok.Value;

/**
 * The state for the {@link CommonFilterParameter} entity.
 */
@Value
public final class CommonFilterParameter {
    private int pageNo;
    private int pageSize;
    private boolean active;
    private boolean ascending;
    private boolean all;
    private String name;

    public static CommonFilterParameter getInstance(int pageNo, int pageSize, boolean active, boolean ascending, boolean all, String name) {
        return new CommonFilterParameter(pageNo, pageSize, active, ascending, all, name);
    }
}
