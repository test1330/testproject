package com.aie.arc.account.impl.entity.entities.vo;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class CreatedAt  {

    private LocalDateTime localDateTime;

    public CreatedAt() {
        localDateTime = null;
    }

    public CreatedAt(LocalDateTime localDateTime1){
        this.localDateTime = localDateTime1;
    }

}
