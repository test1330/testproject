package com.aie.arc.account.impl.entity.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Value;

@Value
@JsonIgnoreProperties(ignoreUnknown = true)
public class GuestUserAddress {
    private String firstName;
    private String lastName;
    private String address1;
    private String address2;
    private String city;
    private String phoneNumber;
    private String postalCode;
    private String state;
    private boolean defaultShipping;
    private boolean defaultBilling;
    private String country;
    private String fipscounty;
    private String fipsstate;
    private String phonenumberAlt;
    private boolean avsvalid;
    private boolean skipavsvalidation;


    public GuestUserAddress() {
        this.firstName = null;
        this.lastName = null;
        this.address1 = null;
        this.address2 = null;
        this.city = null;
        this.phoneNumber = null;
        this.postalCode = null;
        this.state = null;
        this.defaultShipping = false;
        this.defaultBilling = false;
        this.country = null;
        this.fipscounty = null;
        this.fipsstate = null;
        this.phonenumberAlt = null;
        this.avsvalid = false;
        this.skipavsvalidation = false;
    }

    public GuestUserAddress(String firstName, String lastName, String address1, String address2, String city,
                            String phoneNumber, String postalCode,
                            String state, boolean defaultShipping, boolean defaultBilling, String country,String fipscounty,
                            String fipsstate,String phonenumberAlt,boolean avsvalid,boolean skipavsvalidation) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.phoneNumber =  phoneNumber;
        this.postalCode = postalCode;
        this.state = state;
        this.defaultShipping = defaultShipping;
        this.defaultBilling = defaultBilling;
        this.country = country;
        this.fipscounty = fipscounty;
        this.fipsstate = fipsstate;
        this.phonenumberAlt = phonenumberAlt;
        this.avsvalid = avsvalid;
        this.skipavsvalidation = skipavsvalidation;
    }
}
