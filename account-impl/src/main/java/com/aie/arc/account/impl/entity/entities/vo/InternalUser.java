package com.aie.arc.account.impl.entity.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Value;

import java.util.List;

@Value
@JsonIgnoreProperties(ignoreUnknown = true)
public class InternalUser {
    private String businessUserId;
    private String organizationId;
    private InternalUserProfile profile;
    private String designation;
    private List<String> roleIds;
    private boolean active;
    private String createdAt;
    private String lastModifiedAt;

    public InternalUser() {
        this.businessUserId = null;
        this.organizationId = null;
        this.profile = null;
        this.designation = null;
        this.roleIds = null;
        this.active = false;
        this.createdAt = null;
        this.lastModifiedAt = null;

    }
    public InternalUser(String businessUserId, String organizationId, InternalUserProfile profile, String designation, List<String> roleIds, boolean active, String createdAt, String lastModifiedAt) {
        this.businessUserId = businessUserId;
        this.organizationId = organizationId;
        this.profile = profile;
        this.designation = designation;
        this.roleIds = roleIds;
        this.active = active;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
    }
}
