package com.aie.arc.account.impl.entity.entities.vo;

import lombok.Value;

@Value
public class InternalUserProfile {
    private String firstName;
    private String middleName;
    private String lastName;
    private String userName;
    private String email;
    private boolean emailVerified;
    private String phone;

    public InternalUserProfile() {
        this.userName = null;
        this.firstName = null;
        this.middleName = null;
        this.lastName = null;
        this.email = null;
        this.emailVerified = false;
        this.phone = null;
    }

    public InternalUserProfile(String firstName, String middleName, String lastName, String userName, String email, Boolean emailVerified, String phone) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.userName = userName;
        this.email = email;
        this.emailVerified = emailVerified;
        this.phone = phone;
    }


}
