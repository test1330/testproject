package com.aie.arc.account.impl.entity.entities.vo;

import lombok.Value;

import java.time.LocalDateTime;

@Value

public class LastModifiedAt  {
    private LocalDateTime localDateTime;

    public LastModifiedAt() {
        localDateTime = null;
    }

    public LastModifiedAt(LocalDateTime localDateTime){
        this.localDateTime = localDateTime;
    }

}
