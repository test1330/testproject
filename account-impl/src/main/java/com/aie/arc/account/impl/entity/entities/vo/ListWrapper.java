package com.aie.arc.account.impl.entity.entities.vo;

import lombok.Value;

import java.util.List;

@Value
public class ListWrapper {
    private List<String> list;
    private Integer totalSize;
}
