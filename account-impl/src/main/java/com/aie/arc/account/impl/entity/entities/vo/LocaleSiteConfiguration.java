package com.aie.arc.account.impl.entity.entities.vo;

import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
public class LocaleSiteConfiguration {
    @NotBlank
    String localeCode;
    @NotBlank
    String siteURL;
}
