package com.aie.arc.account.impl.entity.entities.vo;

import lombok.Value;

@Value
public class LocalizedFields {
    private String name;
    private String description;
}
