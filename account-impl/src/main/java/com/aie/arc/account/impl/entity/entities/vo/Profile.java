package com.aie.arc.account.impl.entity.entities.vo;

import com.aie.arc.account.api.request.UpdateProfileRequest;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@Value
@JsonDeserialize
public class Profile implements Jsonable {
    private String firstName;
    private String middleName;
    private String lastName;
    private String dateOfBirth;
    private String gender;
    private String email;
    private String phone;
    private SocialIdentity socialIdentity;
    private boolean emailVerified;
    private String createdAt;
    private String lastModifiedAt;
    private String storeId;
    private String shoppingFor;
    private String aboutUs;

    public Profile() {
        this.firstName = null;
        this.middleName = null;
        this.lastName = null;
        this.dateOfBirth = null;
        this.gender = null;
        this.email = null;
        this.phone = null;
        this.socialIdentity = null;
        this.emailVerified = false;
        this.createdAt = null;
        this.lastModifiedAt = null;
        this.storeId = null;
        this.shoppingFor = null;
        this.aboutUs = null;

    }

    @JsonCreator
    public Profile(String firstName, String middleName, String lastName, String dateOfBirth,
                   String gender, String email, String phone, SocialIdentity socialIdentity, Boolean
                           emailVerified, String createdAt, String lastModifiedAt,String storeId,String shoppingFor,String aboutUs) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.socialIdentity = socialIdentity;
        this.emailVerified = emailVerified;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
        this.storeId = storeId;
        this.shoppingFor = shoppingFor;
        this.aboutUs = aboutUs;
    }

    public static Profile updateProfile(UpdateProfileRequest request, Profile profile, com.aie.arc.account.api.request.dto.SocialIdentity socialIdentity, String lastModifiedAt) {
               return new Profile(
                updateIfFoundInRequest(request.getProfile().getFirstName(), profile.getFirstName()),
                updateIfFoundInRequest(request.getProfile().getMiddleName(), profile.getMiddleName()),
                updateIfFoundInRequest(request.getProfile().getLastName(), profile.getLastName()),
                //request.getProfile().getDateOfBirth() != null ? localDate : profile.getDateOfBirth(),
                updateIfFoundInRequest(request.getProfile().getGender(), profile.getGender()),
                updateIfFoundInRequest(request.getProfile().getEmail(), profile.getEmail()),
                updateIfFoundInRequest(request.getProfile().getPhone(), profile.getPhone()),
                updateIfFoundInRequest(request.getProfile().getDateOfBirth(), profile.getDateOfBirth()),
                socialIdentity != null ? new SocialIdentity(socialIdentity.getProvider(), socialIdentity.getUserId(), socialIdentity.getAuthType()) : profile.getSocialIdentity(),
                profile.isEmailVerified(),
                profile.getCreatedAt(),
                lastModifiedAt,
                profile.getStoreId(),
                profile.getShoppingFor(),
                profile.getAboutUs()
        );
    }

    //utils
    private static String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }
}
