package com.aie.arc.account.impl.entity.entities.vo;

import lombok.Value;

@Value
public class Shipping {

    Boolean preOrderEnabled;
    Boolean bopisEnabled;
    Boolean bossEnabled;

}
