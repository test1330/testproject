package com.aie.arc.account.impl.entity.entities.vo;

import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 *
 */
@Value
public class SocialIdentity  {
    private String provider;
    private String userId;
    private String authType;


    public SocialIdentity() {
        this.provider = null;
        this.userId = null;
        this.authType = null;
    }

    public SocialIdentity(String provider, String userId, String authType) {
        this.provider = provider;
        this.userId = userId;
        this.authType = authType;
    }


    public SocialIdentity updateSocialIdentity(String provider, String userId, String authType) {
        return new SocialIdentity(
                updateIfFoundInRequest(provider, getProvider()),
                updateIfFoundInRequest(userId, getUserId()),
                updateIfFoundInRequest(authType, getAuthType()));
    }

    //utils
    private static String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

    //utils
    private <T> List<T> updateIfFoundInRequest(List<T> valueInRequest, List<T> valueInCurrentState) {
        if (valueInRequest == null) return valueInCurrentState;
        return !valueInRequest.isEmpty() ? valueInRequest : valueInCurrentState;
    }
}
