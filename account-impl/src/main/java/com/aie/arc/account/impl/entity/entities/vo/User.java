package com.aie.arc.account.impl.entity.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Value;

import java.util.ArrayList;
import java.util.List;

@Value
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
    private String userId;
    private String firstName;
    private String lastName;
    private String email;
    private String timeStamp;
    private String mobileNumber;
    private boolean emailVerified;
    private String accountType;
    private boolean active;
    private boolean subscribed;
    private String dateOfBirth;
    private String storeId;
    private String shoppingFor;
    private String aboutUs;
    private String myZip;
    private String onlineStoreId;
    private List<GuestUserAddress> guestUserAddresses;
    private String bmRoleId;
    private boolean isMigrated;



    public User() {
        this.userId = null;
        this.firstName = null;
        this.lastName = null;
        this.email = null;
        this.mobileNumber = null;
        this.emailVerified = false;
        this.timeStamp = null;
        this.accountType = null;
        this.active = false;
        this.subscribed = false;
        this.dateOfBirth = null;
        this.storeId = null;
        this.shoppingFor = null;
        this.aboutUs = null;
        this.myZip = null;
        this.guestUserAddresses =null;
        this.onlineStoreId = null;
        this.bmRoleId = null;
        this.isMigrated = false;
    }

    public User(String userId, String firstName, String lastName, String email, String timeStamp, String mobileNumber, String accountType,
                boolean emailVerified, boolean active, boolean subscribed, String dateOfBirth,
                String storeId, String shoppingFor, String aboutUs, String myZip, List<GuestUserAddress> guestUserAddresses,
                String onlineStoreId,String bmRoleId, boolean isMigrated) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.timeStamp = timeStamp;
        this.mobileNumber =  mobileNumber;
        this.emailVerified = emailVerified;
        this.accountType = accountType;
        this.active = active;
        this.subscribed = subscribed;
        this.dateOfBirth = dateOfBirth;
        this.storeId = storeId;
        this.shoppingFor = shoppingFor;
        this.aboutUs = aboutUs;
        this.myZip = myZip;
        this.guestUserAddresses = guestUserAddresses;
        this.onlineStoreId = onlineStoreId;
        this.bmRoleId = bmRoleId;
        this.isMigrated = isMigrated;
    }

}
