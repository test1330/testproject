package com.aie.arc.account.impl.entity.entities.vo.common;

import lombok.Value;

import java.util.Map;

/**
 * The state for the {@link FilterParameter} entity.
 */
@Value
public final class FilterParameter {
    private Integer pageNo;
    private Integer pageSize;
    private String ascending;
    private String ascendingField;
    private Map<String, String> filters;


    public static FilterParameter getNewInstance(Integer pageNo, Integer pageSize, String ascending, String ascendingField, Map<String, String> filters) {
        return new FilterParameter(pageNo, pageSize, ascending, ascendingField, filters);
    }
}