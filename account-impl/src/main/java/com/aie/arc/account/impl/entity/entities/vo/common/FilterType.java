package com.aie.arc.account.impl.entity.entities.vo.common;

import lombok.Getter;

@Getter
public enum FilterType {
    STRING, INTEGER, BOOLEAN, DATE
}
