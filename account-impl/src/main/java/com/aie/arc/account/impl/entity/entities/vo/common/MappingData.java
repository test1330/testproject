package com.aie.arc.account.impl.entity.entities.vo.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MappingData {
    private String columnName;
    private FilterType type;
}
