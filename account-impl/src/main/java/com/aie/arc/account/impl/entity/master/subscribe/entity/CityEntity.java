package com.aie.arc.account.impl.entity.master.subscribe.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.tool.master.subscribe.command.CityCommand;
import com.aie.arc.tool.master.subscribe.event.CityEvent;
import com.aie.arc.tool.master.subscribe.state.CityState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;

public class CityEntity extends EventSourcedBehaviorWithEnforcedReplies<CityCommand, CityEvent, CityState> {

    final private EntityContext<CityCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<CityCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(CityCommand.class, "CityEntity");

    CityEntity(EntityContext<CityCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public CityState emptyState() {
        return CityState.empty();
    }

    @Override
    public CommandHandlerWithReply<CityCommand, CityEvent, CityState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(CityCommand.GetCity.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.city))

                .onCommand(CityCommand.CreateCity.class, (state, cmd) -> Effect()
                        .persist(new CityEvent.CityCreated(cmd.getCity()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(CityCommand.UpdateCity.class, (state, cmd) -> Effect()
                        .persist(new CityEvent.CityUpdated(cmd.getCity()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(CityCommand.DeactivateCity.class, (state, cmd) -> Effect()
                        .persist(new CityEvent.CityDeactivated(cmd.getCityId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(CityCommand.ActivateCity.class, (state, cmd) -> Effect()
                        .persist(new CityEvent.CityActivated(cmd.getCityId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<CityState, CityEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(CityEvent.CityCreated.class, (state, evt) -> CityState.create(evt.getCity()))
                .onEvent(CityEvent.CityUpdated.class, (state, evt) -> state.updateDetails())
                .onEvent(CityEvent.CityActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(CityEvent.CityDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public Set<String> tagsFor(CityEvent cityEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, CityEvent.TAG).apply(cityEvent);
    }

    public static Behavior<CityCommand> create(EntityContext<CityCommand> entityContext) {
        return new CityEntity(entityContext);
    }
}