package com.aie.arc.account.impl.entity.master.subscribe.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.tool.master.subscribe.command.CountryCommand;
import com.aie.arc.tool.master.subscribe.event.CountryEvent;
import com.aie.arc.tool.master.subscribe.state.CountryState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;

public class CountryEntity extends EventSourcedBehaviorWithEnforcedReplies<CountryCommand, CountryEvent, CountryState> {

    final private EntityContext<CountryCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<CountryCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(CountryCommand.class, "CountryEntity");

    CountryEntity(EntityContext<CountryCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public CountryState emptyState() {
        return CountryState.empty();
    }

    @Override
    public CommandHandlerWithReply<CountryCommand, CountryEvent, CountryState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(CountryCommand.GetCountry.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.country))

                .onCommand(CountryCommand.CreateCountry.class, (state, cmd) -> Effect()
                        .persist(new CountryEvent.CountryCreated(cmd.getCountry()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(CountryCommand.UpdateCountry.class, (state, cmd) -> Effect()
                        .persist(new CountryEvent.CountryUpdated(cmd.getCountry()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(CountryCommand.DeactivateCountry.class, (state, cmd) -> Effect()
                        .persist(new CountryEvent.CountryDeactivated(cmd.getCountryId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(CountryCommand.ActivateCountry.class, (state, cmd) -> Effect()
                        .persist(new CountryEvent.CountryActivated(cmd.getCountryId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<CountryState, CountryEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(CountryEvent.CountryCreated.class, (state, evt) -> CountryState.create(evt.getCountry()))
                .onEvent(CountryEvent.CountryUpdated.class, (state, evt) -> state.updateDetails())
                .onEvent(CountryEvent.CountryActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(CountryEvent.CountryDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public Set<String> tagsFor(CountryEvent countryEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, CountryEvent.TAG).apply(countryEvent);
    }

    public static Behavior<CountryCommand> create(EntityContext<CountryCommand> entityContext) {
        return new CountryEntity(entityContext);
    }
}