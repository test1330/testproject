package com.aie.arc.account.impl.entity.master.subscribe.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.tool.master.subscribe.command.CurrencyCommand;
import com.aie.arc.tool.master.subscribe.event.CurrencyEvent;
import com.aie.arc.tool.master.subscribe.state.CurrencyState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;

public class CurrencyEntity extends EventSourcedBehaviorWithEnforcedReplies<CurrencyCommand, CurrencyEvent, CurrencyState> {

    final private EntityContext<CurrencyCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<CurrencyCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(CurrencyCommand.class, "CurrencyEntity");

    CurrencyEntity(EntityContext<CurrencyCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public CurrencyState emptyState() {
        return CurrencyState.empty();
    }

    @Override
    public CommandHandlerWithReply<CurrencyCommand, CurrencyEvent, CurrencyState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(CurrencyCommand.GetCurrency.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.currency))

                .onCommand(CurrencyCommand.CreateCurrency.class, (state, cmd) -> Effect()
                        .persist(new CurrencyEvent.CurrencyCreated(cmd.getCurrency()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(CurrencyCommand.UpdateCurrency.class, (state, cmd) -> Effect()
                        .persist(new CurrencyEvent.CurrencyUpdated(cmd.getCurrency()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(CurrencyCommand.DeactivateCurrency.class, (state, cmd) -> Effect()
                        .persist(new CurrencyEvent.CurrencyDeactivated(cmd.getCurrencyId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(CurrencyCommand.ActivateCurrency.class, (state, cmd) -> Effect()
                        .persist(new CurrencyEvent.CurrencyActivated(cmd.getCurrencyId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<CurrencyState, CurrencyEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(CurrencyEvent.CurrencyCreated.class, (state, evt) -> CurrencyState.create(evt.getCurrency()))
                .onEvent(CurrencyEvent.CurrencyUpdated.class, (state, evt) -> state.updateDetails())
                .onEvent(CurrencyEvent.CurrencyActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(CurrencyEvent.CurrencyDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public Set<String> tagsFor(CurrencyEvent currencyEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, CurrencyEvent.TAG).apply(currencyEvent);
    }

    public static Behavior<CurrencyCommand> create(EntityContext<CurrencyCommand> entityContext) {
        return new CurrencyEntity(entityContext);
    }
}