package com.aie.arc.account.impl.entity.master.subscribe.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.tool.master.subscribe.command.LanguageCommand;
import com.aie.arc.tool.master.subscribe.event.LanguageEvent;
import com.aie.arc.tool.master.subscribe.state.LanguageState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;

public class LanguageEntity extends EventSourcedBehaviorWithEnforcedReplies<LanguageCommand, LanguageEvent, LanguageState> {

    final private EntityContext<LanguageCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<LanguageCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(LanguageCommand.class, "LanguageEntity");

    LanguageEntity(EntityContext<LanguageCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public LanguageState emptyState() {
        return LanguageState.empty();
    }

    @Override
    public CommandHandlerWithReply<LanguageCommand, LanguageEvent, LanguageState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(LanguageCommand.GetLanguage.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.language))

                .onCommand(LanguageCommand.CreateLanguage.class, (state, cmd) -> Effect()
                        .persist(new LanguageEvent.LanguageCreated(cmd.getLanguage()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(LanguageCommand.UpdateLanguage.class, (state, cmd) -> Effect()
                        .persist(new LanguageEvent.LanguageUpdated(cmd.getLanguage()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(LanguageCommand.DeactivateLanguage.class, (state, cmd) -> Effect()
                        .persist(new LanguageEvent.LanguageDeactivated(cmd.getLanguageId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(LanguageCommand.ActivateLanguage.class, (state, cmd) -> Effect()
                        .persist(new LanguageEvent.LanguageActivated(cmd.getLanguageId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<LanguageState, LanguageEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(LanguageEvent.LanguageCreated.class, (state, evt) -> LanguageState.create(evt.getLanguage()))
                .onEvent(LanguageEvent.LanguageUpdated.class, (state, evt) -> state.updateDetails())
                .onEvent(LanguageEvent.LanguageActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(LanguageEvent.LanguageDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public Set<String> tagsFor(LanguageEvent languageEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, LanguageEvent.TAG).apply(languageEvent);
    }

    public static Behavior<LanguageCommand> create(EntityContext<LanguageCommand> entityContext) {
        return new LanguageEntity(entityContext);
    }
}



