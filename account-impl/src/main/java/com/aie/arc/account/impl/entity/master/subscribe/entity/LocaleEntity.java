package com.aie.arc.account.impl.entity.master.subscribe.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.tool.master.subscribe.command.LocaleCommand;
import com.aie.arc.tool.master.subscribe.event.LocaleEvent;
import com.aie.arc.tool.master.subscribe.state.LocaleState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;

public class LocaleEntity extends EventSourcedBehaviorWithEnforcedReplies<LocaleCommand, LocaleEvent, LocaleState> {

    final private EntityContext<LocaleCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<LocaleCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(LocaleCommand.class, "LocaleEntity");

    LocaleEntity(EntityContext<LocaleCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public LocaleState emptyState() {
        return LocaleState.empty();
    }

    @Override
    public CommandHandlerWithReply<LocaleCommand, LocaleEvent, LocaleState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(LocaleCommand.GetLocale.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.locale))

                .onCommand(LocaleCommand.CreateLocale.class, (state, cmd) -> Effect()
                        .persist(new LocaleEvent.LocaleCreated(cmd.getLocale()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(LocaleCommand.UpdateLocale.class, (state, cmd) -> Effect()
                        .persist(new LocaleEvent.LocaleUpdated(cmd.getLocale()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(LocaleCommand.DeactivateLocale.class, (state, cmd) -> Effect()
                        .persist(new LocaleEvent.LocaleDeactivated(cmd.getLocaleId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(LocaleCommand.ActivateLocale.class, (state, cmd) -> Effect()
                        .persist(new LocaleEvent.LocaleActivated(cmd.getLocaleId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<LocaleState, LocaleEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(LocaleEvent.LocaleCreated.class, (state, evt) -> LocaleState.create(evt.getLocale()))
                .onEvent(LocaleEvent.LocaleUpdated.class, (state, evt) -> state.updateDetails())
                .onEvent(LocaleEvent.LocaleActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(LocaleEvent.LocaleDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public Set<String> tagsFor(LocaleEvent event) {
        return AkkaTaggerAdapter.fromLagom(entityContext, LocaleEvent.TAG).apply(event);
    }

    public static Behavior<LocaleCommand> create(EntityContext<LocaleCommand> entityContext) {
        return new LocaleEntity(entityContext);
    }
}








