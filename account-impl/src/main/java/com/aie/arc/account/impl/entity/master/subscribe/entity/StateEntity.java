package com.aie.arc.account.impl.entity.master.subscribe.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.tool.master.subscribe.command.StateCommand;
import com.aie.arc.tool.master.subscribe.event.StateEvent;
import com.aie.arc.tool.master.subscribe.state.StateState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;

public class StateEntity extends EventSourcedBehaviorWithEnforcedReplies<StateCommand, StateEvent, StateState> {

    final private EntityContext<StateCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<StateCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(StateCommand.class, "StateEntity");

    StateEntity(EntityContext<StateCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public StateState emptyState() {
        return StateState.empty();
    }

    @Override
    public CommandHandlerWithReply<StateCommand, StateEvent, StateState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(StateCommand.GetState.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.state))

                .onCommand(StateCommand.CreateState.class, (state, cmd) -> Effect()
                        .persist(new StateEvent.StateCreated(cmd.getState()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(StateCommand.UpdateState.class, (state, cmd) -> Effect()
                        .persist(new StateEvent.StateUpdated(cmd.getState()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(StateCommand.DeactivateState.class, (state, cmd) -> Effect()
                        .persist(new StateEvent.StateDeactivated(cmd.getStateId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(StateCommand.ActivateState.class, (state, cmd) -> Effect()
                        .persist(new StateEvent.StateActivated(cmd.getStateId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<StateState, StateEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(StateEvent.StateCreated.class, (state, evt) -> StateState.create(evt.getState()))
                .onEvent(StateEvent.StateUpdated.class, (state, evt) -> state.updateDetails())
                .onEvent(StateEvent.StateActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(StateEvent.StateDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public Set<String> tagsFor(StateEvent event) {
        return AkkaTaggerAdapter.fromLagom(entityContext, StateEvent.TAG).apply(event);
    }

    public static Behavior<StateCommand> create(EntityContext<StateCommand> entityContext) {
        return new StateEntity(entityContext);
    }
}

