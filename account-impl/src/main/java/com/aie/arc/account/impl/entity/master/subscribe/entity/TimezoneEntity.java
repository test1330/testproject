package com.aie.arc.account.impl.entity.master.subscribe.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.tool.master.subscribe.command.TimezoneCommand;
import com.aie.arc.tool.master.subscribe.event.TimezoneEvent;
import com.aie.arc.tool.master.subscribe.state.TimezoneState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;

public class TimezoneEntity extends EventSourcedBehaviorWithEnforcedReplies<TimezoneCommand, TimezoneEvent, TimezoneState> {

    final private EntityContext<TimezoneCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<TimezoneCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(TimezoneCommand.class, "TimezoneEntity");

    TimezoneEntity(EntityContext<TimezoneCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public TimezoneState emptyState() {
        return TimezoneState.empty();
    }

    @Override
    public CommandHandlerWithReply<TimezoneCommand, TimezoneEvent, TimezoneState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(TimezoneCommand.GetTimezone.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.timezone))

                .onCommand(TimezoneCommand.CreateTimezone.class, (state, cmd) -> Effect()
                        .persist(new TimezoneEvent.TimezoneCreated(cmd.getTimezone()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(TimezoneCommand.UpdateTimezone.class, (state, cmd) -> Effect()
                        .persist(new TimezoneEvent.TimezoneUpdated(cmd.getTimezone()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(TimezoneCommand.DeactivateTimezone.class, (state, cmd) -> Effect()
                        .persist(new TimezoneEvent.TimezoneDeactivated(cmd.getTimezoneId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(TimezoneCommand.ActivateTimezone.class, (state, cmd) -> Effect()
                        .persist(new TimezoneEvent.TimezoneActivated(cmd.getTimezoneId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<TimezoneState, TimezoneEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(TimezoneEvent.TimezoneCreated.class, (state, evt) -> TimezoneState.create(evt.getTimezone()))
                .onEvent(TimezoneEvent.TimezoneUpdated.class, (state, evt) -> state.updateDetails())
                .onEvent(TimezoneEvent.TimezoneActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(TimezoneEvent.TimezoneDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public Set<String> tagsFor(TimezoneEvent event) {
        return AkkaTaggerAdapter.fromLagom(entityContext, TimezoneEvent.TAG).apply(event);
    }

    public static Behavior<TimezoneCommand> create(EntityContext<TimezoneCommand> entityContext) {
        return new TimezoneEntity(entityContext);
    }
}