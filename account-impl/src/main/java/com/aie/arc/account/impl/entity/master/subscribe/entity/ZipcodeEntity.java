package com.aie.arc.account.impl.entity.master.subscribe.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.tool.master.subscribe.command.ZipcodeCommand;
import com.aie.arc.tool.master.subscribe.event.ZipcodeEvent;
import com.aie.arc.tool.master.subscribe.state.ZipcodeState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;

public class ZipcodeEntity extends EventSourcedBehaviorWithEnforcedReplies<ZipcodeCommand, ZipcodeEvent, ZipcodeState> {

    final private EntityContext<ZipcodeCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<ZipcodeCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(ZipcodeCommand.class, "ZipcodeEntity");

    ZipcodeEntity(EntityContext<ZipcodeCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public ZipcodeState emptyState() {
        return ZipcodeState.empty();
    }

    @Override
    public CommandHandlerWithReply<ZipcodeCommand, ZipcodeEvent, ZipcodeState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(ZipcodeCommand.GetZipcode.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.zipcode))

                .onCommand(ZipcodeCommand.CreateZipcode.class, (state, cmd) -> Effect()
                        .persist(new ZipcodeEvent.ZipcodeCreated(cmd.getZipcode()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(ZipcodeCommand.UpdateZipcode.class, (state, cmd) -> Effect()
                        .persist(new ZipcodeEvent.ZipcodeUpdated(cmd.getZipcode()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(ZipcodeCommand.DeactivateZipcode.class, (state, cmd) -> Effect()
                        .persist(new ZipcodeEvent.ZipcodeDeactivated(cmd.getZipCodeId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(ZipcodeCommand.ActivateZipcode.class, (state, cmd) -> Effect()
                        .persist(new ZipcodeEvent.ZipcodeActivated(cmd.getZipCodeId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<ZipcodeState, ZipcodeEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(ZipcodeEvent.ZipcodeCreated.class, (state, evt) -> ZipcodeState.create(evt.getZipCode()))
                .onEvent(ZipcodeEvent.ZipcodeUpdated.class, (state, evt) -> state.updateDetails())
                .onEvent(ZipcodeEvent.ZipcodeActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(ZipcodeEvent.ZipcodeDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public Set<String> tagsFor(ZipcodeEvent event) {
        return AkkaTaggerAdapter.fromLagom(entityContext, ZipcodeEvent.TAG).apply(event);
    }

    public static Behavior<ZipcodeCommand> create(EntityContext<ZipcodeCommand> entityContext) {
        return new ZipcodeEntity(entityContext);
    }
}