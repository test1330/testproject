package com.aie.arc.account.impl.entity.organization.subscribe.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.tool.organization.subscribe.command.OfflineStoreCommand;
import com.aie.arc.tool.organization.subscribe.event.OfflineStoreEvent;
import com.aie.arc.tool.organization.subscribe.state.OfflineStoreState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;

public class OfflineStoreEntity extends EventSourcedBehaviorWithEnforcedReplies<OfflineStoreCommand, OfflineStoreEvent, OfflineStoreState> {


    final private EntityContext<OfflineStoreCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<OfflineStoreCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(OfflineStoreCommand.class, "OfflineStoreEntity");

    OfflineStoreEntity(EntityContext<OfflineStoreCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public OfflineStoreState emptyState() {
        return OfflineStoreState.empty();
    }

    @Override
    public CommandHandlerWithReply<OfflineStoreCommand, OfflineStoreEvent, OfflineStoreState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()
                .onCommand(OfflineStoreCommand.GetOfflineStore.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.offlineStore))

                .onCommand(OfflineStoreCommand.CreateOfflineStore.class, (state, cmd) -> Effect()
                        .persist(new OfflineStoreEvent.OfflineStoreCreated(cmd.getOfflineStoreVO()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(OfflineStoreCommand.UpdateOfflineStore.class, (state, cmd) -> Effect()
                        .persist(new OfflineStoreEvent.OfflineStoreCreated(cmd.getOfflineStoreVO()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(OfflineStoreCommand.ActivateOfflineStore.class, (state, cmd) -> Effect()
                        .persist(new OfflineStoreEvent.OfflineStoreActivated(cmd.getOfflineStoreId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(OfflineStoreCommand.DeactivateOfflineStore.class, (state, cmd) -> Effect()
                        .persist(new OfflineStoreEvent.OfflineStoreDeactivated(cmd.getOfflineStoreId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .build();
    }

    @Override
    public EventHandler<OfflineStoreState, OfflineStoreEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(OfflineStoreEvent.OfflineStoreCreated.class, (state, evt) -> OfflineStoreState.create(evt.getOfflineStoreVO()))
                .onEvent(OfflineStoreEvent.OfflineStoreUpdated.class, (state, evt) -> state.updateDetails())
                .onEvent(OfflineStoreEvent.OfflineStoreActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(OfflineStoreEvent.OfflineStoreDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public Set<String> tagsFor(OfflineStoreEvent offlineStoreEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, OfflineStoreEvent.TAG).apply(offlineStoreEvent);
    }

    public static Behavior<OfflineStoreCommand> create(EntityContext<OfflineStoreCommand> entityContext) {
        return new OfflineStoreEntity(entityContext);
    }

}