package com.aie.arc.account.impl.entity.organization.subscribe.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.tool.organization.subscribe.command.OnlineStoreCommand;
import com.aie.arc.tool.organization.subscribe.event.OnlineStoreEvent;
import com.aie.arc.tool.organization.subscribe.state.OnlineStoreState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;

public class OnlineStoreEntity extends EventSourcedBehaviorWithEnforcedReplies<OnlineStoreCommand, OnlineStoreEvent, OnlineStoreState> {


    final private EntityContext<OnlineStoreCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<OnlineStoreCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(OnlineStoreCommand.class, "OnlineStoreEntity");

    OnlineStoreEntity(EntityContext<OnlineStoreCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }


    @Override
    public CommandHandlerWithReply<OnlineStoreCommand, OnlineStoreEvent, OnlineStoreState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()
                .onCommand(OnlineStoreCommand.GetOnlineStore.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.onlineStore))

                .onCommand(OnlineStoreCommand.CreateOnlineStore.class, (state, cmd) -> Effect()
                        .persist(new OnlineStoreEvent.OnlineStoreCreated(cmd.getOnlineStore()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(OnlineStoreCommand.UpdateOnlineStore.class, (state, cmd) -> Effect()
                        .persist(new OnlineStoreEvent.OnlineStoreUpdated(cmd.getOnlineStore()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(OnlineStoreCommand.ActivateOnlineStore.class, (state, cmd) -> Effect()
                        .persist(new OnlineStoreEvent.OnlineStoreActivated(cmd.getOnlineStoreId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(OnlineStoreCommand.DeactivateOnlineStore.class, (state, cmd) -> Effect()
                        .persist(new OnlineStoreEvent.OnlineStoreDeactivated(cmd.getOnlineStoreId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(OnlineStoreCommand.UpdateDefaultPaymentMethodAndAddToList.class, (state, cmd) -> Effect()
                        .persist(new OnlineStoreEvent.DefaultPaymentMethodAndListUpdated(cmd.getOnlineStoreId(), cmd.getPaymentMethodId()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(OnlineStoreCommand.UpdateDefaultShippingMethodAndAddToList.class, (state, cmd) -> Effect()
                        .persist(new OnlineStoreEvent.DefaultShippingMethodAndListUpdated(cmd.getOnlineStoreId(), cmd.getShippingMethodId()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(OnlineStoreCommand.UpdateDefaultShippingTypeAndAddToList.class, (state, cmd) -> Effect()
                        .persist(new OnlineStoreEvent.DefaultShippingTypeAndListUpdated(cmd.getOnlineStoreId(), cmd.getShippingTypeId()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .build();
    }

    @Override
    public EventHandler<OnlineStoreState, OnlineStoreEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(OnlineStoreEvent.OnlineStoreCreated.class, (state, evt) -> OnlineStoreState.create(evt.getOnlineStore()))
                .onEvent(OnlineStoreEvent.OnlineStoreUpdated.class, (state, evt) -> state.updateDetails())
                .onEvent(OnlineStoreEvent.OnlineStoreActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(OnlineStoreEvent.OnlineStoreDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(OnlineStoreEvent.DefaultPaymentMethodAndListUpdated.class, (state, evt) -> state.updatePaymentMethodDetails(evt.getPaymentMethodId()))
                .onEvent(OnlineStoreEvent.DefaultShippingMethodAndListUpdated.class, (state, evt) -> state.updateShippingMethodDetails(evt.getShippingMethodId()))
                .onEvent(OnlineStoreEvent.DefaultShippingTypeAndListUpdated.class, (state, evt) -> state.updateShippingTypeDetails(evt.getShippingTypeId()))
                .build();
    }

    @Override
    public OnlineStoreState emptyState() {
        return OnlineStoreState.empty();
    }

    @Override
    public Set<String> tagsFor(OnlineStoreEvent onlineStoreEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, OnlineStoreEvent.TAG).apply(onlineStoreEvent);
    }

    public static Behavior<OnlineStoreCommand> create(EntityContext<OnlineStoreCommand> entityContext) {
        return new OnlineStoreEntity(entityContext);
    }


}