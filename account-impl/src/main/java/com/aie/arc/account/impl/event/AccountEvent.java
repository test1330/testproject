package com.aie.arc.account.impl.event;

import com.aie.arc.account.impl.entity.entities.Account;
import com.aie.arc.account.impl.entity.entities.vo.Profile;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

public interface AccountEvent extends Jsonable, AggregateEvent<AccountEvent> {

    int NUM_SHARDS = 4;
    AggregateEventShards<AccountEvent> TAG = AggregateEventTag.sharded(AccountEvent.class, NUM_SHARDS);

    String getAccountId();

    default boolean isPublic() {
        return true;
    }

    @Value
    @JsonDeserialize
    class AccountCreated implements AccountEvent {
        public final Account account;

        @JsonCreator
        public AccountCreated(Account account) {
            this.account = account;
        }

        @Override
        public String getAccountId() {
            return account.getAccountId();
        }
    }

    @Value
    @JsonDeserialize
    class AccountUpdated implements AccountEvent {
        public final Account account;

        @JsonCreator
        public AccountUpdated(Account account) {
            this.account = account;
        }

        @Override
        public String getAccountId() {
            return account.getAccountId();
        }

    }

    @Value
    @JsonDeserialize
    class AccountActivated implements AccountEvent {
        public final String accountId;
        public final boolean value;

        @JsonCreator
        public AccountActivated(String accountId, boolean value) {
            this.accountId = accountId;
            this.value = value;
        }

        @Override
        public String getAccountId() {
            return accountId;
        }

    }

    @Value
    @JsonDeserialize
    class AccountDeactivated implements AccountEvent {
        public final String accountId;
        public final boolean value;

        @JsonCreator
        public AccountDeactivated(String accountId, boolean value) {
            this.accountId = accountId;
            this.value = value;
        }

        @Override
        public String getAccountId() {
            return accountId;
        }

    }


    @Value
    @JsonDeserialize
    class ProfileUpdated implements AccountEvent {
        public final String accountId;
        public final Profile profile;

        @JsonCreator
        public ProfileUpdated(String accountId, Profile profile) {
            this.accountId = accountId;
            this.profile = profile;
        }

        @Override
        public String getAccountId() {
            return accountId;
        }

    }

    @Value
    @JsonDeserialize
    class AddressAddedToAccount implements AccountEvent {
        public final String accountId;
        public final String addressId;
        public final String addressZipCode;
        public final boolean defaultShipping;
        public final boolean defaultBilling;
        public final boolean migratedEvent;

        @JsonCreator
        public AddressAddedToAccount(String accountId, String addressId, String addressZipCode, boolean defaultShipping,
                                     boolean defaultBilling, boolean migratedEvent) {
            this.accountId = accountId;
            this.addressId = addressId;
            this.addressZipCode = addressZipCode;
            this.defaultShipping = defaultShipping;
            this.defaultBilling = defaultBilling;
            this.migratedEvent = migratedEvent;
        }

        @Override
        public String getAccountId() {
            return accountId;
        }

    }

    @Value
    @JsonDeserialize
    class AddressUpdatedToAccount implements AccountEvent {
        public final String accountId;
        public final String addressId;
        public final String addressZipCode;
        public final boolean defaultShipping;
        public final boolean defaultBilling;
        public final boolean migratedEvent;

        @JsonCreator
        public AddressUpdatedToAccount(String accountId, String addressId, String addressZipCode, boolean defaultShipping,
                                       boolean defaultBilling, boolean migratedEvent) {
            this.accountId = accountId;
            this.addressId = addressId;
            this.addressZipCode = addressZipCode;
            this.defaultShipping = defaultShipping;
            this.defaultBilling = defaultBilling;
            this.migratedEvent = migratedEvent;
        }

        @Override
        public String getAccountId() {
            return accountId;
        }

    }

    @Value
    @JsonDeserialize
    class CardAddedToAccount implements AccountEvent {
        public final String accountId;
        public final String cardId;
        public final boolean defaultCreditCard;
        public final boolean migratedEvent;

        @JsonCreator
        public CardAddedToAccount(String accountId, String cardId, boolean defaultCreditCard, boolean migratedEvent) {
            this.accountId = accountId;
            this.cardId = cardId;
            this.defaultCreditCard = defaultCreditCard;
            this.migratedEvent = migratedEvent;
        }
    }

    @Value
    @JsonDeserialize
    class ZipCodeAdded implements AccountEvent {
        public final String accountId;
        public final String myZip;

        @JsonCreator
        public ZipCodeAdded(String accountId, String myZip) {
            this.accountId = accountId;
            this.myZip = myZip;
        }
    }

    @Value
    @JsonDeserialize
    class CardUpdatedToAccount implements AccountEvent {
        public final String accountId;
        public final String cardId;
        public final boolean defaultCreditCard;
        public final boolean migratedEvent;

        @JsonCreator
        public CardUpdatedToAccount(String accountId, String cardId, boolean defaultCreditCard, boolean migratedEvent) {
            this.accountId = accountId;
            this.cardId = cardId;
            this.defaultCreditCard = defaultCreditCard;
            this.migratedEvent = migratedEvent;
        }
    }

    @Value
    @JsonDeserialize
    class AccountCardDeactivated implements AccountEvent {
        public final String accountId;
        public final String cardId;
        public final boolean migratedEvent;

        @JsonCreator
        public AccountCardDeactivated(String accountId, String cardId,boolean migratedEvent) {
            this.accountId = accountId;
            this.cardId = cardId;
            this.migratedEvent = migratedEvent;
        }
    }

    @Value
    @JsonDeserialize
    class StoreAddedToAccount implements AccountEvent {
        public final String accountId;
        public final String storeId;

        @JsonCreator
        public StoreAddedToAccount(String accountId, String storeId) {
            this.accountId = accountId;
            this.storeId = storeId;
        }
    }

    @Value
    @JsonDeserialize
    class StoreUpdatedToAccount implements AccountEvent {
        public final String accountId;
        public final String storeId;

        @JsonCreator
        public StoreUpdatedToAccount(String accountId, String storeId) {
            this.accountId = accountId;
            this.storeId = storeId;
        }
    }

    @Value
    @JsonDeserialize
    class AddressDeactivatedToAccount implements AccountEvent {
        public final String accountId;
        public final String addressId;
        public final boolean migratedEvent;

        @JsonCreator
        public AddressDeactivatedToAccount(String accountId, String addressId, boolean migratedEvent) {
            this.accountId = accountId;
            this.addressId = addressId;
            this.migratedEvent = migratedEvent;
        }
    }

    @Value
    @JsonDeserialize
    class DefaultBillingAddressUpdatedToAccount implements AccountEvent {
        public final String accountId;
        public final String billingAddressId;
        public final String shippingAddressId;

        @JsonCreator
        public DefaultBillingAddressUpdatedToAccount(String accountId, String billingAddressId,
                                                     String shippingAddressId) {
            this.accountId = accountId;
            this.billingAddressId = billingAddressId;
            this.shippingAddressId = shippingAddressId;
        }
    }

    @Value
    @JsonDeserialize
    class DefaultShippingAddressUpdatedToAccount implements AccountEvent {
        public final String accountId;
        public final String shippingAddressId;
        public final String billingAddressId;

        @JsonCreator
        public DefaultShippingAddressUpdatedToAccount(String accountId, String shippingAddressId,
                                                      String billingAddressId) {
            this.accountId = accountId;
            this.shippingAddressId = shippingAddressId;
            this.billingAddressId = billingAddressId;
        }
    }

    @Value
    @JsonDeserialize
    class Subscribed implements AccountEvent {
        public final String accountId;
        public final boolean value;

        @JsonCreator
        public Subscribed(String accountId, boolean value) {
            this.accountId = accountId;
            this.value = value;
        }

        @Override
        public String getAccountId() {
            return accountId;
        }

    }

    @Value
    @JsonDeserialize
    class Unsubscribed implements AccountEvent {
        public final String accountId;
        public final boolean value;

        @JsonCreator
        public Unsubscribed(String accountId, boolean value) {
            this.accountId = accountId;
            this.value = value;
        }

        @Override
        public String getAccountId() {
            return accountId;
        }
    }

    @Value
    @JsonDeserialize
    class DefaultPaymentSet implements AccountEvent {
        public final String accountId;
        public final String cardId;

        @JsonCreator
        public DefaultPaymentSet(String accountId, String cardId) {
            this.accountId = accountId;
            this.cardId = cardId;
        }

        @Override
        public String getAccountId() {
            return accountId;
        }
    }

    @Override
    default AggregateEventTagger<AccountEvent> aggregateTag() {
        return TAG;
    }
}
