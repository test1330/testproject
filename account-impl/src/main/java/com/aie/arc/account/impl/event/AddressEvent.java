package com.aie.arc.account.impl.event;

import com.aie.arc.account.impl.entity.entities.Address;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

public interface AddressEvent extends Jsonable, AggregateEvent<AddressEvent> {
    int NUM_SHARDS = 4;
    AggregateEventShards<AddressEvent> TAG = AggregateEventTag.sharded(AddressEvent.class, NUM_SHARDS);

    String getAddressId();

    default boolean isPublic() {
        return true;
    }

    @Override
    default AggregateEventTagger<AddressEvent> aggregateTag() {
        return TAG;
    }


    @Value
    @JsonDeserialize
    class AddressCreated implements AddressEvent {
        public final Address address;

        @JsonCreator
        public AddressCreated(Address address) {
            this.address = address;
        }

        @Override
        public String getAddressId() {
            return address.getAddressId();
        }
    }

    @Value
    @JsonDeserialize
    class AddressUpdated implements AddressEvent {
        public final Address address;

        @JsonCreator
        public AddressUpdated(Address address) {
            this.address = address;
        }

        @Override
        public String getAddressId() {
            return address.getAddressId();
        }
    }

    @Value
    @JsonDeserialize
    class AddressActivated implements AddressEvent {
        public final String addressId;
        public final boolean value;

        @JsonCreator
        public AddressActivated(String addressId, boolean active) {
            this.addressId = addressId;
            this.value = active;
        }

        @Override
        public String getAddressId() {
            return addressId;
        }
    }
    @Value
    @JsonDeserialize
    class AddressDeactivated implements AddressEvent {
        public final String addressId;
        public final boolean value;

        @JsonCreator
        public AddressDeactivated(String addressId, boolean active) {
            this.addressId = addressId;
            this.value = active;
        }

        @Override
        public String getAddressId() {
            return addressId;
        }
    }

}
