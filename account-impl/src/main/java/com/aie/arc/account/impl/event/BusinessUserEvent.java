package com.aie.arc.account.impl.event;

import com.aie.arc.account.impl.entity.entities.BusinessUser;
import com.aie.arc.account.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.account.impl.entity.entities.vo.LastModifiedAt;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.util.List;

public interface BusinessUserEvent extends Jsonable, AggregateEvent<BusinessUserEvent> {
    int NUM_SHARDS = 4;
    AggregateEventShards<BusinessUserEvent> TAG = AggregateEventTag.sharded(BusinessUserEvent.class, NUM_SHARDS);

    String getBusinessUserId();

    default boolean isPublic() {
        return true;
    }
    @Override
    default AggregateEventTagger<BusinessUserEvent> aggregateTag() {
        return TAG;
    }

    @Value
    @JsonDeserialize
    class BusinessUserCreated implements BusinessUserEvent {
        public final BusinessUser businessUser;

        @JsonCreator
        public BusinessUserCreated(BusinessUser businessUser) {
            this.businessUser = businessUser;
        }

        @Override
        public String getBusinessUserId() {
            return businessUser.getBusinessUserId();
        }
    }

    @Value
    @JsonDeserialize
    class BusinessUserUpdated implements BusinessUserEvent {
        public final String businessUserId;
        public final BusinessUser businessUser;

        @JsonCreator
        public BusinessUserUpdated(String businessUserId, BusinessUser businessUser) {
            this.businessUserId = businessUserId;
            this.businessUser = businessUser;
        }

        @Override
        public String getBusinessUserId() {
            return businessUser.getBusinessUserId();
        }
    }


    @Value
    @JsonDeserialize
    class AddressAddedToBusinessUser implements BusinessUserEvent {
        public final String businessUserId;
        public final String shippingAddressId;
        public final String billingAddressId;

        @JsonCreator
        public AddressAddedToBusinessUser(String businessUserId, String shippingAddressId, String billingAddressId) {
            this.businessUserId = businessUserId;
            this.shippingAddressId = shippingAddressId;
            this.billingAddressId = billingAddressId;
        }
    }

    @Value
    @JsonDeserialize
    class AddressUpdatedToBusinessUser implements BusinessUserEvent {
        public final String businessUserId;
        public final String shippingAddressId;
        public final String billingAddressId;

        @JsonCreator
        public AddressUpdatedToBusinessUser(String businessUserId, String shippingAddressId, String billingAddressId) {
            this.businessUserId = businessUserId;
            this.shippingAddressId = shippingAddressId;
            this.billingAddressId = billingAddressId;
        }
    }

    @Value
    @JsonDeserialize
    class BusinessUserActivated implements BusinessUserEvent {
        public final String businessUserId;
        public final boolean value;

        @JsonCreator
        public BusinessUserActivated(String businessUserId, boolean value) {
            this.businessUserId = businessUserId;
            this.value = value;
        }

        @Override
        public String getBusinessUserId() {
            return businessUserId;
        }
    }
    @Value
    @JsonDeserialize
    class BusinessUserDeactivated implements BusinessUserEvent {
        public final String businessUserId;
        public final boolean value;

        @JsonCreator
        public BusinessUserDeactivated(String businessUserId, boolean value) {
            this.businessUserId = businessUserId;
            this.value = value;
        }

        @Override
        public String getBusinessUserId() {
            return businessUserId;
        }
    }
    @Value
    @JsonDeserialize
    class RoleAssignedToBusinessUser implements BusinessUserEvent {
        public final String businessUserId;
        public final List<String> roleIds;
        public final CreatedAt createdAt;
        public final LastModifiedAt lastModifiedAt;
        @JsonCreator
        public RoleAssignedToBusinessUser(String businessUserId, List<String> roleIds, CreatedAt createdAt, LastModifiedAt lastModifiedAt) {
            this.businessUserId = businessUserId;
            this.roleIds = roleIds;
            this.createdAt = createdAt;
            this.lastModifiedAt = lastModifiedAt;
        }

        @Override
        public String getBusinessUserId() {
            return businessUserId;
        }
    }

}
