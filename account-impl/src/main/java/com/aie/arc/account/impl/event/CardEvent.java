package com.aie.arc.account.impl.event;

import com.aie.arc.account.impl.entity.entities.Card;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

public interface CardEvent extends Jsonable, AggregateEvent<CardEvent> {
    int NUM_SHARDS = 4;
    AggregateEventShards<CardEvent> TAG = AggregateEventTag.sharded(CardEvent.class, NUM_SHARDS);

    String getCardId();

    default boolean isPublic() {
        return true;
    }
    @Override
    default AggregateEventTagger<CardEvent> aggregateTag() {
        return TAG;
    }

    @Value
    @JsonDeserialize
    class CardCreated implements CardEvent {
        public final Card card;

        @JsonCreator
        public CardCreated(Card card) {
            this.card = card;
        }

        @Override
        public String getCardId() {
            return card.getCardId();
        }
    }

    @Value
    @JsonDeserialize
    class CardUpdated implements CardEvent {
        public final Card card;

        @JsonCreator
        public CardUpdated(Card card) {
            this.card = card;
        }

        @Override
        public String getCardId() {
            return card.getCardId();
        }
    }

    @Value
    @JsonDeserialize
    class CardActivated implements CardEvent {
        public final String cardId;
        public final boolean value;

        @JsonCreator
        public CardActivated(String cardId, boolean active) {
            this.cardId = cardId;
            this.value = active;
        }

        @Override
        public String getCardId() {
            return cardId;
        }
    }

    @Value
    @JsonDeserialize
    class CardDeactivated implements CardEvent {
       public final String cardId;
       public final boolean value;

        @JsonCreator
        public CardDeactivated(String cardId, boolean value) {
            this.cardId = cardId;
            this.value = value;
        }

        @Override
        public String getCardId() {
            return cardId;
        }
    }
}
