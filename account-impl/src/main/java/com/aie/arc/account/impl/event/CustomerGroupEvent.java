package com.aie.arc.account.impl.event;

import com.aie.arc.account.impl.entity.entities.CustomerGroup;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

public interface CustomerGroupEvent extends Jsonable, AggregateEvent<CustomerGroupEvent> {
    int NUM_SHARDS = 4;
    AggregateEventShards<CustomerGroupEvent> TAG = AggregateEventTag.sharded(CustomerGroupEvent.class, NUM_SHARDS);

    String getCustomerGroupId();

    default boolean isPublic() {
        return true;
    }

    @Value
    class CustomerGroupCreated implements CustomerGroupEvent {
        CustomerGroup customerGroup;

        @JsonCreator
        public CustomerGroupCreated(CustomerGroup customerGroup) {
            this.customerGroup = customerGroup;
        }

        @Override
        public String getCustomerGroupId() {
            return customerGroup.getCustomerGroupId();
        }
    }

    @Value
    class CustomerGroupUpdated implements CustomerGroupEvent {
        CustomerGroup customerGroup;

        @JsonCreator
        public CustomerGroupUpdated(CustomerGroup customerGroup) {
            this.customerGroup = customerGroup;
        }

        @Override
        public String getCustomerGroupId() {
            return customerGroup.getCustomerGroupId();
        }
    }

    @Value
    class CustomerGroupActivated implements CustomerGroupEvent {
        String customerGroupId;
        boolean value;

        @JsonCreator
        public CustomerGroupActivated(String customerGroupId, boolean value) {
            this.customerGroupId = customerGroupId;
            this.value = value;
        }

        @Override
        public String getCustomerGroupId() {
            return customerGroupId;
        }
    }
    @Value
    class CustomerGroupDeactivated implements CustomerGroupEvent {
        String customerGroupId;
        boolean value;

        @JsonCreator
        public CustomerGroupDeactivated(String customerGroupId, boolean value) {
            this.customerGroupId = customerGroupId;
            this.value = value;
        }

        @Override
        public String getCustomerGroupId() {
            return customerGroupId;
        }
    }

    @Override
    default AggregateEventTagger<CustomerGroupEvent> aggregateTag() {
        return TAG;
    }
}
