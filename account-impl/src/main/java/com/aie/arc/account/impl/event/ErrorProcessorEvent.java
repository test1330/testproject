package com.aie.arc.account.impl.event;

import com.aie.arc.account.impl.entity.entities.ErrorProcessor;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

public interface ErrorProcessorEvent extends Jsonable, AggregateEvent<ErrorProcessorEvent> {

    int NUM_SHARDS = 4;
    AggregateEventShards<ErrorProcessorEvent> TAG = AggregateEventTag.sharded(ErrorProcessorEvent.class, NUM_SHARDS);

    String getErrorProcessorId();

    default boolean isPublic() {
        return true;
    }

    @Value
    class ErrorProcessorCreated implements ErrorProcessorEvent {
        ErrorProcessor errorProcessorVO;

        @Override
        public String getErrorProcessorId() {
            return errorProcessorVO.getEventId();
        }
    }

    @Override
    default AggregateEventTagger<ErrorProcessorEvent> aggregateTag() {
        return TAG;
    }
}