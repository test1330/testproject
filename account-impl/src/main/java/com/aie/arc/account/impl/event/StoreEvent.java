package com.aie.arc.account.impl.event;

import com.aie.arc.account.impl.entity.entities.Store;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

public interface StoreEvent extends Jsonable, AggregateEvent<StoreEvent> {

    int NUM_SHARDS = 4;
    AggregateEventShards<StoreEvent> TAG = AggregateEventTag.sharded(StoreEvent.class, NUM_SHARDS);

    String getStoreId();

    default boolean isPublic() {
        return true;
    }

    @Value
    class StoreCreated implements StoreEvent {
        Store storeVO;

        @JsonCreator
        public StoreCreated(Store storeVO) {
            this.storeVO = storeVO;
        }

        @Override
        public String getStoreId() {
            return storeVO.getStoreId();
        }
    }

    @Value
    class StoreUpdated implements StoreEvent {
        Store storeVO;

        @JsonCreator
        public StoreUpdated(Store storeVO) {
            this.storeVO = storeVO;
        }

        @Override
        public String getStoreId() {
            return storeVO.getStoreId();
        }
    }

    @Value
    class StoreActivated implements StoreEvent {
        String id;
        String storeId;

        @JsonCreator
        public StoreActivated(String id, String storeId) {
            this.id = id;
            this.storeId = storeId;
        }

        @Override
        public String getStoreId() {
            return storeId;
        }
    }

    @Override
    default AggregateEventTagger<StoreEvent> aggregateTag() {
        return TAG;
    }
}