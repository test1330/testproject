package com.aie.arc.account.impl.mapper;

import com.aie.arc.account.api.common.exceptions.EncryptorException;
import com.aie.arc.account.api.common.request.LocalisedFields;
import com.aie.arc.account.api.request.dto.AccountProfile;
import com.aie.arc.account.api.request.dto.SocialIdentity;
import com.aie.arc.account.api.response.*;
import com.aie.arc.account.impl.encryption.DESEncryptor;
import com.aie.arc.account.impl.entity.entities.*;
import com.aie.arc.account.impl.entity.entities.vo.BusinessProfile;
import com.aie.arc.account.impl.entity.entities.vo.LocalizedFields;
import com.aie.arc.account.impl.entity.entities.vo.Profile;
import com.aie.arc.account.impl.service.AccountServiceValidation;
import com.aie.arc.rbac.api.response.GetRoleDetail;
import com.aie.arc.rbac.api.response.RoleDetail;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class Mapper {

    private static final DESEncryptor DES_DECRYPTOR = new DESEncryptor();
    private static final String MASK_CHARACTER_X = "X";
    private Mapper()
    {

    }
    //Address
    public static AccountAddress mapToAddress(Address address, Account account) {
        return new AccountAddress(
                address.getAddressId(),
                address.getAccountId(),
                address.getFirstName(),
                address.getLastName(),
                address.getAddress1(),
                address.getAddress2(),
                address.getCity(),
                address.getState(),
                address.getPostalCode(),
                address.getPhone(),
                StringUtils.equalsIgnoreCase(account.getDefaultShippingAddressId(), address.getAddressId()),
                StringUtils.equalsIgnoreCase(account.getDefaultBillingAddressId(), address.getAddressId()),
                address.isActive()
        );
    }
    public static AddressDetail toAddressDetails(Address address) {
        return new AddressDetail(
                address.getAddressId(),
                address.getAccountId(),
                address.getFirstName(),
                address.getLastName(),
                address.getAddress1(),
                address.getAddress2(),
                address.getCity(),
                address.getState(),
                address.getCountry(),
                address.getPostalCode(),
                address.getPhone(),
                address.isActive()
        );
    }

    public static AccountDetail toAccountDetails(AccountServiceValidation accountServiceValidation, Account account) {

        return new AccountDetail(
                account.getAccountId(),
                account.getBmcisProfileId(),
                account.getAccountNumber(),
                new ProfileDetails(
                        account.getProfile().getFirstName(),
                        account.getProfile().getMiddleName(),
                        account.getProfile().getLastName(),
                        account.getProfile().getDateOfBirth(),
                        account.getProfile().getGender(),
                        account.getProfile().getEmail(),
                        account.getProfile().getPhone(),
                        mapToSocialIdentity(account.getProfile().getSocialIdentity()),
                        account.getProfile().isEmailVerified(),
                        account.getProfile().getCreatedAt(),
                        account.getProfile().getLastModifiedAt(),
                        account.isSubscribed(),
                        account.getProfile().getStoreId(),
                        account.getProfile().getShoppingFor(),
                        account.getProfile().getAboutUs()
                ),
                account.getCardIds(),
                account.getDefaultCardId() != null ? toCardDetails( accountServiceValidation.validateAndGetCardById(
                        account.getDefaultCardId()).toCompletableFuture().join().get(), account.getDefaultCardId()) : null,
                account.getAddressIds(),
                account.getDefaultBillingAddressId() != null ? toAddressDetails(
                        accountServiceValidation.validateAndGetAddressById(account.getDefaultBillingAddressId()).toCompletableFuture().join().get()) : null,
                account.getDefaultShippingAddressId() != null ? toAddressDetails(
                        accountServiceValidation.validateAndGetAddressById(account.getDefaultShippingAddressId()).toCompletableFuture().join().get()) : null,
                account.getProfile().getStoreId(),
                account.getMyZip(),
                account.isActive()
        );
    }

    public static PaymentDetails toPaymentDetails(String defaultCardId, List<CardDetail> cards) {
        return new PaymentDetails(defaultCardId, cards);
    }

    //Card
    public static CardDetail toCardDetails(Card card, String defaultCardId) {
        try {
            return new CardDetail(
                    card.getCardId(),
                    card.getFirstName(),
                    card.getLastName(),
                    maskExceptLast4Digits(DES_DECRYPTOR.decrypt(card.getCardNumber())),
                    card.getCardType(),
                    card.getExpirationMonth(),
                    card.getExpirationYear(),
                    StringUtils.equalsAnyIgnoreCase(card.getCardId(), defaultCardId),
                    card.isActive()
            );
        } catch (EncryptorException e) {
            log.error("Unable to decrypt card number for cardId : {}", card.getCardId());
        }
        return new CardDetail(card.getCardId(), card.getFirstName(), card.getLastName(), card.getCardNumber(),
                card.getCardType(), card.getExpirationMonth(), card.getExpirationYear(), card.isDefaultCreditCard(),
                card.isActive());
    }

    /**
     * This method masks 'cardNumberToBeMasked',
     * 1. All characters except last 4 if 'cardNumberToBeMasked' length is greater than 4
     * 2. Returns X if 'cardNumberToBeMasked' length is less than or equal to 4
     *
     * @param cardNumberToBeMasked - String to masked
     * @return returns masked string
     */
    public static String maskExceptLast4Digits(String cardNumberToBeMasked) {
        String maskedString = MASK_CHARACTER_X;
        if (StringUtils.isNotBlank(cardNumberToBeMasked) && cardNumberToBeMasked.trim().length() > 4) {
            int maskLength = cardNumberToBeMasked.length() - 4;
            maskedString = StringUtils.overlay(cardNumberToBeMasked,
                    StringUtils.repeat(MASK_CHARACTER_X, maskLength), 0, maskLength);
        }
        return maskedString;
    }

    private static SocialIdentity mapToSocialIdentity(
            com.aie.arc.account.impl.entity.entities.vo.SocialIdentity socialIdentity) {
        return new SocialIdentity(
                socialIdentity.getProvider(),
                socialIdentity.getUserId(),
                socialIdentity.getAuthType()
        );

    }

    public static AccountProfile mapToProfileAPI(Account account) {
        Profile profile = account.getProfile();
        return new AccountProfile(
                profile.getFirstName(),
                profile.getMiddleName(),
                profile.getLastName(),
                profile.getDateOfBirth() != null ? profile.getDateOfBirth() : null,
                profile.getGender(),
                profile.getEmail(),
                profile.getPhone(),
                mapToSocialIdentity(profile.getSocialIdentity()),
                profile.getStoreId(),
                profile.getShoppingFor(),
                profile.getAboutUs());

    }

    //Card
    public static CustomerGroupDetail toCustomerGroupDetails(CustomerGroup customerGroup) {
        return new CustomerGroupDetail(
                customerGroup.getCustomerGroupId(),
                toLocalisedFieldMap(customerGroup.getLocalization()),
                customerGroup.getCustomerIds(),
                customerGroup.getRecency(),
                customerGroup.getFrequency(),
                customerGroup.getMonetary(),
                customerGroup.isActive()
        );
    }

    private static Map<String, LocalisedFields> toLocalisedFieldMap(Map<String, LocalizedFields> localizedFieldsVOMap) {
        Map<String, LocalisedFields> localisedFieldsMap = new HashMap<>();
        localizedFieldsVOMap.forEach(((code, localised) -> {
            localisedFieldsMap.put(code, new LocalisedFields(localised.getName(), localised.getDescription()));
        }));
        return localisedFieldsMap;
    }

    //BusinessUser
    public static GetBusinessUserDetail toGetBusinessUserDetails(BusinessUser businessUser,List<GetRoleDetail> roles) {
        return new GetBusinessUserDetail(
                businessUser.getBusinessUserId(),
                businessUser.getOrganizationId(),
                new BusinessUserProfileDetails(
                        businessUser.getProfile().getFirstName(),
                        businessUser.getProfile().getMiddleName(),
                        businessUser.getProfile().getLastName(),
                        businessUser.getProfile().getUserName(),
                        businessUser.getProfile().getEmail(),
                        businessUser.getProfile().getPhone()
                ),
                businessUser.getDesignation(),
                roles
        );
    }

    public static BusinessUserDetail toBusinessUserDetails(BusinessUser businessUser,List<RoleDetail> roles) {
        return new BusinessUserDetail(
                businessUser.getBusinessUserId(),
                businessUser.getOrganizationId(),
                new BusinessUserProfileDetails(
                        businessUser.getProfile().getFirstName(),
                        businessUser.getProfile().getMiddleName(),
                        businessUser.getProfile().getLastName(),
                        businessUser.getProfile().getUserName(),
                        businessUser.getProfile().getEmail(),
                        businessUser.getProfile().getPhone()
                ),
                businessUser.getProfile().getCreatedAt(),
                businessUser.getProfile().getLastModifiedAt(),
                businessUser.getDesignation(),
                roles,
                businessUser.isActive()
        );
    }

    public static com.aie.arc.account.api.request.dto.BusinessProfile mapToBusinessProfileAPI(BusinessUser businessUser) {
        BusinessProfile profile = businessUser.getProfile();
        return new com.aie.arc.account.api.request.dto.BusinessProfile(
                profile.getFirstName(),
                profile.getUserName(),
                profile.getMiddleName(),
                profile.getLastName(),
                profile.getEmail(),
                profile.getPhone()
        );
    }

    public static BusinessAddressDetail toBusinessAddressDetails(BusinessAddress address) {
        return new BusinessAddressDetail(
                address.getAddressId(),
                address.getFirstName(),
                address.getLastName(),
                address.getAddress1(),
                address.getAddress2(),
                address.getCity(),
                address.getState(),
                address.getCountry(),
                address.getPostalCode(),
                address.getPhone(),
                address.isActive()
        );
    }

}

