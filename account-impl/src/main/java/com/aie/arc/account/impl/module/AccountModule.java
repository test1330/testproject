package com.aie.arc.account.impl.module;

import com.aie.arc.account.api.service.AccountService;
import com.aie.arc.account.api.service.KafkaService;
import com.aie.arc.account.impl.service.AccountServiceImpl;
import com.aie.arc.account.impl.util.common.BMCISFailedRecordsScheduler;
import com.aie.arc.externalservice.api.AvalaraService;
import com.aie.arc.externalservice.api.FreshAddressService;
import com.aie.arc.externalservice.api.IpInfoService;

import com.aie.arc.organization.api.service.OrganizationService;
import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.akka.discovery.AkkaDiscoveryServiceLocator;
import com.lightbend.lagom.javadsl.api.ServiceLocator;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;
import com.typesafe.config.Config;
import lombok.extern.slf4j.Slf4j;
import play.Environment;
/*
 *Binding service api to service impl
 *
 * */
@Slf4j
public class AccountModule extends AbstractModule implements ServiceGuiceSupport {

    private final Environment environment;
    private final Config config;

    public AccountModule(Environment environment, Config config) {
        this.environment = environment;
        this.config = config;
    }

    @Override
    protected void configure() {
        bindService(AccountService.class, AccountServiceImpl.class);
        bindClient(KafkaService.class);
        bindClient(OrganizationService.class);
        bind(BMCISFailedRecordsScheduler.class).asEagerSingleton();
        bindClient(FreshAddressService.class);
        bindClient(AvalaraService.class);
        bindClient(IpInfoService.class);
        log.info(" ********** Environment Mode : {} ************ ", environment.mode().toString());
        if (environment.isProd()) {
            bind(ServiceLocator.class).to(AkkaDiscoveryServiceLocator.class);
        }
    }
}

