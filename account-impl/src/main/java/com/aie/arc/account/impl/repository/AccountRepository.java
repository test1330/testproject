package com.aie.arc.account.impl.repository;

import com.aie.arc.account.impl.entity.entities.ListWrapper;
import com.aie.arc.account.impl.entity.entities.vo.common.FilterParameter;
import com.aie.arc.account.impl.entity.entities.vo.common.MappingData;
import com.aie.arc.account.impl.repository.readside.AccountReadSide;
import com.aie.arc.account.impl.util.common.FilterConfig;
import com.aie.arc.account.impl.util.common.RepositoryUtil;
import com.lightbend.lagom.javadsl.persistence.ReadSide;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcSession;
import lombok.extern.slf4j.Slf4j;
import org.pcollections.PSequence;
import org.pcollections.TreePVector;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.CompletionStage;

@Singleton
@Slf4j
public class AccountRepository {

    private final JdbcSession session;

    @Inject
    public AccountRepository(JdbcSession session, ReadSide readSide) {
        this.session = session;
        readSide.register(AccountReadSide.class);
    }

    public CompletionStage<ListWrapper> getAccountIds(FilterParameter filter) {
        return session.withConnection(
                connection -> {
                    // Creating dynamic query
                    Map<String, MappingData> assetTypeFieldMap = FilterConfig.getAccountTypeMap();
                    StringBuilder dynamicQuery = new StringBuilder();
                    filter.getFilters().forEach((key, value) -> {
                        MappingData mappingData = assetTypeFieldMap.get(key);
                        switch (mappingData.getType()) {
                            case BOOLEAN:
                                RepositoryUtil.appendBooleanParameter(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case STRING:
                                RepositoryUtil.appendStringParameter(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case INTEGER:
                                RepositoryUtil.addIntegerFieldInDynamicQuery(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            default:
                                break;
                        }
                    });
                    StringBuilder selectQuery = new StringBuilder("SELECT DISTINCT (account_id), *  FROM account ");
                    int count = RepositoryUtil.getCount(connection, dynamicQuery, "account", "account_id");
                    RepositoryUtil.addSortColumnInDynamicQuery(dynamicQuery, assetTypeFieldMap.get(filter.getAscendingField()).getColumnName(),  filter.getAscending());
                    selectQuery.append(dynamicQuery);
                    int offset = filter.getPageNo() * filter.getPageSize();
                    // return empty if offset is greater than count
                    if (offset > count) {
                        return new ListWrapper(TreePVector.empty(), count);
                    }
                    selectQuery.append(" LIMIT " + filter.getPageSize() + " OFFSET " + offset);
                    PreparedStatement statement = connection.prepareStatement(selectQuery.toString());
                    try (ResultSet resultSet = statement.executeQuery()) {
                        PSequence<String> accountIds = TreePVector.empty();
                        while (resultSet.next()) {
                            accountIds = accountIds.plus(resultSet.getString("account_id"));
                        }
                        return new ListWrapper(accountIds, count);
                    }
                }
        );
    }

    public void processFailedAccountRecords(String accountId) {
        log.debug("adding record in bmcis_failed_records for accountId : {} ", accountId);
        session.withConnection(
                connection -> {
                    try (PreparedStatement statement = connection.prepareStatement(
                            "INSERT INTO bmcis_failed_records (account_id, status, modified_at) VALUES (?, ? ,?)")) {
                        int index = 1;
                        statement.setString(index++, accountId);
                        statement.setString(index++, "FAILED");
                        statement.setString(index++, LocalDateTime.now().toString());
                        statement.executeUpdate();
                        log.debug("processFailedAccountRecords read side query successfully executed for accountId: {}", accountId);
                    } catch (SQLException e) {
                        log.error("processFailedAccountRecords query failed due to : {}", e.getMessage());
                    }
                    return "";
                }
        );
    }

    public void updateFailedAccountRecords(String accountId) {
        log.debug("updating record in bmcis_failed_records for accountId : {} ", accountId);
        session.withConnection(
                connection -> {
                    try (PreparedStatement statement = connection.prepareStatement(
                            "UPDATE bmcis_failed_records SET status = 'SENT' WHERE account_id = ? ")) {
                        int index = 1;
                        statement.setString(index++, accountId);
                        statement.executeUpdate();
                        log.debug("updateFailedAccountRecords read side query successfully executed for accountId: {}", accountId);
                    } catch (SQLException e) {
                        log.error("updateFailedAccountRecords query failed due to : {}", e.getMessage());
                    }
                    return "";
                }
        );
    }

    public CompletionStage<PSequence<String>> getFailedAccountIds() {
        return session.withConnection(connection -> {
            ResultSet resultSet = connection.prepareStatement(
                    "SELECT account_id FROM bmcis_failed_records WHERE status = 'FAILED'").executeQuery();
            PSequence<String> accountIds = TreePVector.empty();
            while (resultSet.next()) {
                accountIds = accountIds.plus(resultSet.getString("account_id"));
            }
            return accountIds;
        });
    }

}