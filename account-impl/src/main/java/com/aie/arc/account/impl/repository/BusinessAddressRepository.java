package com.aie.arc.account.impl.repository;

import com.aie.arc.account.impl.entity.entities.ListWrapper;
import com.aie.arc.account.impl.entity.entities.vo.common.FilterParameter;
import com.aie.arc.account.impl.entity.entities.vo.common.MappingData;
import com.aie.arc.account.impl.repository.readside.AddressReadSide;
import com.aie.arc.account.impl.repository.readside.BusinessAddressReadSide;
import com.aie.arc.account.impl.util.common.FilterConfig;
import com.aie.arc.account.impl.util.common.RepositoryUtil;
import com.lightbend.lagom.javadsl.persistence.ReadSide;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcSession;
import lombok.extern.slf4j.Slf4j;
import org.pcollections.PSequence;
import org.pcollections.TreePVector;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.concurrent.CompletionStage;

import static com.aie.arc.account.impl.util.MessageConstants.ADDRESS;
import static com.aie.arc.account.impl.util.MessageConstants.ADDRESS_ID;
@Slf4j
@Singleton
public class BusinessAddressRepository {

    private final JdbcSession session;

    @Inject
    public BusinessAddressRepository(JdbcSession session, ReadSide readSide) {
        this.session = session;
        //readSide.register(BusinessAddressReadSide.class);
    }

    /*Get List Of Customer account*/
    public CompletionStage<ListWrapper> getAddressIds(FilterParameter filter, List<String> ids) {
        return session.withConnection(
                connection -> {
                    // Creating dynamic query
                    Map<String, MappingData> assetTypeFieldMap = FilterConfig.getAddressTypeMap();
                    StringBuilder dynamicQuery = new StringBuilder();
                    filter.getFilters().forEach((key, value) -> {
                        MappingData mappingData = assetTypeFieldMap.get(key);
                        switch (mappingData.getType()) {
                            case BOOLEAN:
                                RepositoryUtil.appendBooleanParameter(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case STRING:
                                RepositoryUtil.appendStringParameter(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case INTEGER:
                                RepositoryUtil.addIntegerFieldInDynamicQuery(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            default:
                                break;
                        }
                    });
                    StringJoiner stringJoiner = new StringJoiner(",");
                    ids.forEach(id -> stringJoiner.add("'" + id + "'"));
                    RepositoryUtil.appendInParameter(dynamicQuery,ADDRESS_ID , stringJoiner.toString());
                    StringBuilder selectQuery = new StringBuilder("SELECT DISTINCT (address_id),*  FROM address ");
                    int count = RepositoryUtil.getCount(connection, dynamicQuery, ADDRESS, ADDRESS_ID);
                    RepositoryUtil.addSortColumnInDynamicQuery(dynamicQuery, assetTypeFieldMap.get(filter.getAscendingField()).getColumnName(), filter.getAscending());
                    selectQuery.append(dynamicQuery);
                    int offset = filter.getPageNo() * filter.getPageSize();
                    // return empty if offset is greater than count
                    if (offset > count) {
                        return new ListWrapper(TreePVector.empty(), count);
                    }
                    selectQuery.append(" LIMIT " + filter.getPageSize() + " OFFSET " + offset);
                    PreparedStatement statement = connection.prepareStatement(selectQuery.toString());
                    log.info("selectQuery {}" , selectQuery);
                    try (ResultSet resultSet = statement.executeQuery()) {
                        PSequence<String> addressIds = TreePVector.empty();
                        while (resultSet.next()) {
                            addressIds = addressIds.plus(resultSet.getString(ADDRESS_ID));
                        }
                        return new ListWrapper(addressIds, count);
                    }
                }
        );
    }

}
