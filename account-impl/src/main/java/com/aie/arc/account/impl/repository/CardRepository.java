package com.aie.arc.account.impl.repository;

import com.aie.arc.account.api.common.exceptions.ParameterRequired;
import com.aie.arc.account.impl.repository.readside.CardReadSide;
import com.lightbend.lagom.javadsl.persistence.ReadSide;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcSession;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static com.aie.arc.account.api.constants.AccountURIConstants.CARD_UPDATED_MESSAGE;
import static com.aie.arc.account.api.constants.AccountURIConstants.UPDATE_CARD_URI;
import static com.aie.arc.account.impl.util.MessageConstants.SUCCESS_STATUS;

@Slf4j
@Singleton
public class CardRepository {
    private final JdbcSession session;

    @Inject
    public CardRepository(JdbcSession session, ReadSide readSide) {
        this.session = session;
        readSide.register(CardReadSide.class);
    }

    public CompletionStage<Boolean> cardExist(String cardNickName, String accountId) {

        return session.withConnection(connection -> {
            PreparedStatement statement;
            try {
                statement = connection.prepareStatement(
                        new StringBuilder("SELECT COUNT ( * ) as count FROM card  WHERE card_nick_name = '" + cardNickName
                                + "' AND account_id='" + accountId + "' AND active = 'true'").toString());
                ResultSet countRes = statement.executeQuery();
                countRes.next();
                if (countRes.getInt("count") != 0)
                    return true;
                else
                    return false;
            } catch (SQLException e) {
                throw new ParameterRequired(new String[]{"SQL Exception encountered!"});
            }
        });

    }

}
