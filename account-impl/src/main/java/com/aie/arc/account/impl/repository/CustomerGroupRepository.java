package com.aie.arc.account.impl.repository;

import com.aie.arc.account.impl.entity.entities.ListWrapper;
import com.aie.arc.account.impl.entity.entities.vo.common.FilterParameter;
import com.aie.arc.account.impl.entity.entities.vo.common.MappingData;
import com.aie.arc.account.impl.repository.readside.CustomerGroupReadSide;
import com.aie.arc.account.impl.util.common.FilterConfig;
import com.aie.arc.account.impl.util.common.RepositoryUtil;
import com.lightbend.lagom.javadsl.persistence.ReadSide;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcSession;
import org.pcollections.PSequence;
import org.pcollections.TreePVector;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;
import java.util.concurrent.CompletionStage;

@Singleton
public class CustomerGroupRepository {

    private final JdbcSession session;

    @Inject
    public CustomerGroupRepository(JdbcSession session, ReadSide readSide) {
        this.session = session;
        //readSide.register(CustomerGroupReadSide.class);
    }
    public CompletionStage<ListWrapper> getCustomerGroupIds(FilterParameter filter) {
        return session.withConnection(
                connection -> {
                    // Creating dynamic query
                    Map<String, MappingData> assetTypeFieldMap = FilterConfig.getCustomerGroupTypeMap();
                    StringBuilder dynamicQuery = new StringBuilder();
                    filter.getFilters().forEach((key, value) -> {
                        MappingData mappingData = assetTypeFieldMap.get(key);
                        switch (mappingData.getType()) {
                            case BOOLEAN:
                                RepositoryUtil.appendBooleanParameter(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case STRING:
                                RepositoryUtil.appendStringParameter(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case INTEGER:
                                RepositoryUtil.addIntegerFieldInDynamicQuery(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case DATE:
                                break;
                        }
                    });
                    StringBuilder selectQuery = new StringBuilder("SELECT DISTINCT(customer_group_id),* FROM customer_group ");
                    int count = RepositoryUtil.getCount(connection, dynamicQuery, "customer_group", "customer_group_id");
                    RepositoryUtil.addSortColumnInDynamicQuery(dynamicQuery, assetTypeFieldMap.get(filter.getAscendingField()).getColumnName(),  filter.getAscending());
                    selectQuery.append(dynamicQuery);
                    int offset = filter.getPageNo() * filter.getPageSize();
                    // return empty if offset is greater than count
                    if (offset > count) {
                        return new ListWrapper(TreePVector.empty(), count);
                    }
                    selectQuery.append(" LIMIT " + filter.getPageSize() + " OFFSET " + offset);
                    PreparedStatement statement = connection.prepareStatement(selectQuery.toString());
                    System.out.println("selectQuery = " + selectQuery);
                    try (ResultSet resultSet = statement.executeQuery()) {
                        PSequence<String> customerGroupIds = TreePVector.empty();
                        while (resultSet.next()) {
                            customerGroupIds = customerGroupIds.plus(resultSet.getString("customer_group_id"));
                        }
                        return new ListWrapper(customerGroupIds, count);
                    }
                }
        );
    }

}
