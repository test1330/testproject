package com.aie.arc.account.impl.repository.readside;

import com.aie.arc.account.impl.entity.entities.Account;
import com.aie.arc.account.impl.entity.entities.vo.Profile;
import com.aie.arc.account.impl.event.AccountEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.Offset;
import com.lightbend.lagom.javadsl.persistence.ReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcReadSide;
import lombok.extern.slf4j.Slf4j;
import org.pcollections.PSequence;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Slf4j
public class AccountReadSide extends ReadSideProcessor<AccountEvent> {
    private final JdbcReadSide jdbcReadSide;

    @Inject
    public AccountReadSide(JdbcReadSide jdbcReadSide) {
        this.jdbcReadSide = jdbcReadSide;
    }

    @Override
    public ReadSideHandler<AccountEvent> buildHandler() {
        return jdbcReadSide.<AccountEvent>builder("accountEventOffset")
                .setEventHandler(AccountEvent.AccountCreated.class, this::processCreateAccount)
                .setEventHandler(AccountEvent.AccountUpdated.class, this::processUpdateAccount)
                .setEventHandler(AccountEvent.ZipCodeAdded.class, this::processAddZipCodeInAccount)
                .build();
    }

    private void processCreateAccount(Connection connection, AccountEvent.AccountCreated event, Offset offset) throws SQLException {
        int index = 1;
        Account account = event.getAccount();
        try {
            if(isRecordPresentForId(event.getAccountId(), connection) != 0) {
                log.info("Record already present for accountId {} , performing update!", account.getAccountId());
                processUpdateAccount(connection, new AccountEvent.AccountUpdated(account), offset);
            } else {
                log.info("read_side_create_account id : {}, OFFSET : {}", account.getAccountId(), offset);
                PreparedStatement statement = connection.prepareStatement(
                        "INSERT INTO account (account_id,store_id, account_number,first_name,middle_name,last_name,email," +
                                "phone,created_at,modified_at, subscribed, active,date_of_birth,shopping_for,about_us,my_zip," +
                                "default_shipping_id,default_billing_id,default_card_id) " +
                                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                Profile profile = account.getProfile();
                statement.setString(index++, account.getAccountId());
                statement.setString(index++, account.getStoreId());
                statement.setInt(index++, account.getAccountNumber());
                statement.setString(index++, profile.getFirstName().length() > 40 ? profile.getFirstName().substring(0, 39) : profile.getFirstName());
                statement.setString(index++, profile.getMiddleName());
                statement.setString(index++, profile.getLastName().length() > 40 ? profile.getLastName().substring(0, 39) : profile.getLastName());
                statement.setString(index++, profile.getEmail());
                statement.setString(index++, profile.getPhone());
                statement.setString(index++, profile.getCreatedAt());
                statement.setString(index++, profile.getLastModifiedAt());
                statement.setBoolean(index++, account.isSubscribed());
                statement.setBoolean(index++, account.isActive());
                statement.setString(index++, profile.getDateOfBirth());
                statement.setString(index++, profile.getShoppingFor());
                statement.setString(index++, profile.getAboutUs());
                statement.setString(index++, account.getMyZip() != null ? account.getMyZip().length() > 10 ? account.getMyZip().
                        substring(0, 9) : account.getMyZip() : account.getMyZip());
                statement.setString(index++, account.getDefaultShippingAddressId());
                statement.setString(index++, account.getDefaultBillingAddressId());
                statement.setString(index, account.getDefaultCardId());
                statement.execute();
                log.info("insert_account_read_side_query_successfully executed for accountId: {}", account.getAccountId());
            }
        } catch (SQLException e) {
            log.error("insert_account_query_is_failed for account : {} due to :", account, e);
        }
    }

    private void processUpdateAccount(Connection connection, AccountEvent.AccountUpdated event, Offset offset) throws SQLException {
        int index = 1;
        Account account = event.getAccount();
        log.info("read_side_update_account id : {}, OFFSET : {}", account.getAccountId(), offset);
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE account SET first_name=?,middle_name=?,last_name=?,email=?,phone=?,modified_at=?, " +
                        "subscribed=?,store_id=?,shopping_for=?,about_us=?,date_of_birth=?,my_zip=?, " +
                        "default_shipping_id=?,default_billing_id=?,default_card_id=? WHERE account_id=?")) {
            Profile profile = account.getProfile();
            statement.setString(index++, profile.getFirstName().length() > 40 ? profile.getFirstName().substring(0, 39) : profile.getFirstName());
            statement.setString(index++, profile.getMiddleName());
            statement.setString(index++, profile.getLastName().length() > 40 ? profile.getLastName().substring(0, 39) : profile.getLastName());
            statement.setString(index++, profile.getEmail());
            statement.setString(index++, profile.getPhone());
            statement.setString(index++, profile.getLastModifiedAt());
            statement.setBoolean(index++, account.isSubscribed());
            statement.setString(index++, profile.getStoreId());
            statement.setString(index++, profile.getShoppingFor());
            statement.setString(index++, profile.getAboutUs());
            statement.setString(index++, profile.getDateOfBirth());
            statement.setString(index++, account.getMyZip() != null ? account.getMyZip().length() > 10 ? account.getMyZip().
                    substring(0, 9) : account.getMyZip() : account.getMyZip());
            statement.setString(index++, account.getDefaultShippingAddressId());
            statement.setString(index++, account.getDefaultBillingAddressId());
            statement.setString(index++, account.getDefaultCardId());
            statement.setString(index, account.getAccountId());
            statement.executeUpdate();
            log.info("update_account_read_side_query_successfully executed for accountId: {}", account.getAccountId());
        } catch (SQLException e) {
            log.error("update_account_query_is_failed for account : {} due to :", account, e);
        }
    }

    private void processAddZipCodeInAccount(Connection connection, AccountEvent.ZipCodeAdded event, Offset offset) {
        log.info("read_side_add_zipcode_to_account id : {}, OFFSET : {}", event.getAccountId(), offset);
        int index = 1;
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE account SET my_zip = ? WHERE account_id = ?")) {
            statement.setString(index++, event.getMyZip() != null ? event.getMyZip().length() > 10 ? event.getMyZip().
                    substring(0, 9) : event.getMyZip() : event.getMyZip());
            statement.setString(index++, event.getAccountId());
            statement.executeUpdate();
            log.debug("updateZipCodeInAccount read side query successfully executed for accountId: {}", event.getAccountId());
        } catch (SQLException e) {
            log.error("updateZipCodeInAccount query failed due to :", e.getMessage());
        }
    }

    public int isRecordPresentForId(String id, Connection connection) throws SQLException {
        ResultSet resultSet = connection.prepareStatement(
                                "select count(*) as COUNT from account where account_id = '" + id + "'")
                                .executeQuery();
        resultSet.next();
        return resultSet.getInt(1);
    }

    @Override
    public PSequence<AggregateEventTag<AccountEvent>> aggregateTags() {
        return AccountEvent.TAG.allTags();
    }
}