package com.aie.arc.account.impl.repository.readside;

import com.aie.arc.account.impl.entity.entities.Address;
import com.aie.arc.account.impl.event.AddressEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.Offset;
import com.lightbend.lagom.javadsl.persistence.ReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcReadSide;
import lombok.extern.slf4j.Slf4j;
import org.pcollections.PSequence;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Slf4j
public class AddressReadSide extends ReadSideProcessor<AddressEvent> {

    private final JdbcReadSide jdbcReadSide;

    @Inject
    public AddressReadSide(JdbcReadSide jdbcReadSide) {
        this.jdbcReadSide = jdbcReadSide;
    }

    @Override
    public ReadSideHandler<AddressEvent> buildHandler() {
        return jdbcReadSide.<AddressEvent>builder("addressEventOffset")
                .setEventHandler(AddressEvent.AddressCreated.class, this::processCreateAddress)
                .setEventHandler(AddressEvent.AddressUpdated.class, this::processUpdateAddress)
                .setEventHandler(AddressEvent.AddressActivated.class, this::processActivateAddress)
                .setEventHandler(AddressEvent.AddressDeactivated.class, this::processDeactivateAddress)
                .build();
    }

    private void processCreateAddress(Connection connection, AddressEvent.AddressCreated event, Offset offset) {
        Address address = event.getAddress();
        int index = 1;
        try {
            if(isRecordPresentForId(event.getAddressId(), connection) != 0) {
                log.info("Record already present for addressId {} , performing update!", address.getAddressId());
                processUpdateAddress(connection, new AddressEvent.AddressUpdated(address), offset);
            } else {
                log.info("read_side_create_address account_id : {}, address_id : {}, OFFSET : {}", address.getAccountId(), address.getAddressId(), offset);
                PreparedStatement statement = connection.prepareStatement(
                        "INSERT INTO address(address_id,account_id,address_1,address_2,first_name,last_name,city,state," +
                                "country,postal_code,phone,is_shipping,is_billing,fips_country,fips_state,alt_phone_number,avs_valid" +
                                ",skip_avs_validation,active) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                statement.setString(index++, address.getAddressId());
                statement.setString(index++, address.getAccountId());
                statement.setString(index++, address.getAddress1());
                statement.setString(index++, address.getAddress2());
                statement.setString(index++, address.getFirstName().length() > 100 ? address.getFirstName().substring(0, 99) : address.getFirstName());
                statement.setString(index++, address.getLastName().length() > 100 ? address.getLastName().substring(0, 99) : address.getLastName());
                statement.setString(index++, address.getCity());
                statement.setString(index++, address.getState());
                statement.setString(index++, address.getCountry());
                statement.setString(index++, address.getPostalCode().length() > 10 ? address.getPostalCode().substring(0, 9) : address.getPostalCode());
                statement.setString(index++, address.getPhone());
                statement.setBoolean(index++, address.isDefaultShipping());
                statement.setBoolean(index++, address.isDefaultBilling());
                statement.setString(index++, address.getFipscounty());
                statement.setString(index++, address.getFipsstate());
                statement.setString(index++, address.getPhonenumberAlt());
                statement.setBoolean(index++, address.isAvsvalid());
                statement.setBoolean(index++, address.isSkipavsvalidation());
                statement.setBoolean(index, address.isActive());
                statement.execute();
                log.info("insert_address_read_side_query_successfully executed for addressId: {}", address.getAddressId());
            }
        } catch (SQLException e) {
            log.error("insert_address_query_failed for address : {} due to :", address.getAddressId(), e);
        }
    }

    private void processUpdateAddress(Connection connection, AddressEvent.AddressUpdated event, Offset offset) throws SQLException {
        int index = 1;
        Address address = event.getAddress();
        log.info("read_side_update_address account_id : {}, address_id : {}, OFFSET : {}", address.getAccountId(), address.getAddressId(), offset);
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE address SET address_1=?,address_2=?,first_name=?,last_name=?,city=?,state=?," +
                        "country=?,postal_code=?,phone=?,is_shipping=?,is_billing=?,fips_country=?,fips_state=?," +
                        "alt_phone_number=?,avs_valid=?,skip_avs_validation=? WHERE address_id=?")) {
            statement.setString(index++, address.getAddress1());
            statement.setString(index++, address.getAddress2());
            statement.setString(index++, address.getFirstName().length() > 100 ? address.getFirstName().substring(0, 99) : address.getFirstName());
            statement.setString(index++, address.getLastName().length() > 100 ? address.getLastName().substring(0, 99) : address.getLastName());
            statement.setString(index++, address.getCity());
            statement.setString(index++, address.getState());
            statement.setString(index++, address.getCountry());
            statement.setString(index++, address.getPostalCode().length() > 10 ? address.getPostalCode().substring(0, 9) : address.getPostalCode());
            statement.setString(index++, address.getPhone());
            statement.setBoolean(index++, address.isDefaultShipping());
            statement.setBoolean(index++, address.isDefaultBilling());
            statement.setString(index++, address.getFipscounty());
            statement.setString(index++, address.getFipsstate());
            statement.setString(index++, address.getPhonenumberAlt());
            statement.setBoolean(index++, address.isAvsvalid());
            statement.setBoolean(index++, address.isSkipavsvalidation());
            statement.setString(index, address.getAddressId());
            statement.executeUpdate();
            log.info("update_address_read_side_query_successfully executed for addressId: {}", address.getAddressId());
        } catch (SQLException e) {
            log.debug("In update Exception");
            log.debug("Exception is - " + e.getCause().getMessage());
            log.error("update_ERROR____address_query_is_failed for address : {} due to :", address.getAddressId(), e.getCause().getMessage());
            log.debug("update DEBUG____ {} ",  e.getCause().getMessage());
            log.error("update_ERROR3____address_query_is_failed for address : {} due to :", address.getAddressId(), e);
        }
    }

    private void processActivateAddress(Connection connection, AddressEvent.AddressActivated event, Offset offset) throws SQLException {
        log.info("read_side_activate_address  address_id : {}, OFFSET : {}", event.getAddressId(), offset);
        int index = 1;
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE address SET active=? WHERE address_id=?")) {
            statement.setBoolean(index++, event.isValue());
            statement.setString(index, event.getAddressId());
            statement.executeUpdate();
            log.debug("update_address_activate read side query successfully executed for addressId: {}",
                    event.getAddressId());
        } catch (SQLException e) {
            log.error("update_address_activate query failed due to :", e);
        }
    }

    private void processDeactivateAddress(Connection connection, AddressEvent.AddressDeactivated event, Offset offset) throws SQLException {
        int index = 1;
        log.info("read_side_deactivate_address  address_id : {}, OFFSET : {}", event.getAddressId(), offset);
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE address SET active=? WHERE address_id=?")) {
            statement.setBoolean(index++, event.isValue());
            statement.setString(index, event.getAddressId());
            statement.executeUpdate();
            log.debug("update_address_deactivate read side query successfully executed for addressId: {}",
                    event.getAddressId());
        } catch (SQLException e) {
            log.error("update_address_deactivate query failed due to :", e);
        }
    }

    private int isRecordPresentForId(String id, Connection connection) throws SQLException {
        ResultSet resultSet = connection.prepareStatement(
                "select count(*) as COUNT from address where address_id = '" + id + "'")
                .executeQuery();
        resultSet.next();
        return resultSet.getInt(1);
    }

    @Override
    public PSequence<AggregateEventTag<AddressEvent>> aggregateTags() {
        return AddressEvent.TAG.allTags();
    }
}
