package com.aie.arc.account.impl.repository.readside;

import com.aie.arc.account.impl.entity.entities.BusinessAddress;
import com.aie.arc.account.impl.event.BusinessAddressEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.ReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcReadSide;
import lombok.extern.slf4j.Slf4j;
import org.pcollections.PSequence;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Slf4j
public class BusinessAddressReadSide extends ReadSideProcessor<BusinessAddressEvent> {

    private final JdbcReadSide jdbcReadSide;

    @Inject
    public BusinessAddressReadSide(JdbcReadSide jdbcReadSide) {
        this.jdbcReadSide = jdbcReadSide;
    }

    @Override
    public ReadSideHandler<BusinessAddressEvent> buildHandler() {
        return jdbcReadSide.<BusinessAddressEvent>builder("businessAddressEventOffset")
                .setEventHandler(BusinessAddressEvent.AddressCreated.class, this::processCreateAddress)
                .setEventHandler(BusinessAddressEvent.AddressUpdated.class, this::processUpdateAddress)
                .setEventHandler(BusinessAddressEvent.AddressActivated.class, this::processActivateAddress)
                .setEventHandler(BusinessAddressEvent.AddressDeactivated.class, this::processDeactivateAddress)
                .build();
    }

    private void processCreateAddress(Connection connection, BusinessAddressEvent.AddressCreated event) throws SQLException {
        int index = 1;
        BusinessAddress address = event.getAddress();
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO business_address(address_id,address_1,address_2,first_name,last_name,city,state," +
                        "country,postal_code,phone,is_shipping,is_billing,active) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)")) {
            /*statement = connection.prepareStatement(
                    "INSERT INTO address(address_id,flat_number,company,mobile,email,active) VALUES (?,?,?,?,?,?)");*/
            statement.setString(index++, address.getAddressId());
            statement.setString(index++, address.getAddress1());
            statement.setString(index++, address.getAddress2());
            statement.setString(index++, address.getFirstName());
            statement.setString(index++, address.getLastName());
            statement.setString(index++, address.getCity());
            statement.setString(index++, address.getState());
            statement.setString(index++, address.getCountry());
            statement.setString(index++, address.getPostalCode());
            statement.setString(index++, address.getPhone());
            statement.setBoolean(index++, address.isResidenceAddress());
            statement.setBoolean(index++, address.isOfficeAddress());
            statement.setBoolean(index, address.isActive());
            statement.execute();
            log.debug("insert_address_read_side_query_successfully executed for addressId: {}", address.getAddressId());
        } catch (SQLException e) {
            log.error("insert_address_query_failed for address : {} due to :", address, e);
        }
    }

    private void processUpdateAddress(Connection connection, BusinessAddressEvent.AddressUpdated event) throws SQLException {
        int index = 1;
        BusinessAddress address = event.getAddress();
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE business_address SET address_1=?,address_2=?,first_name=?,last_name=?,city=?,state=?," +
                        "country=?,postal_code=?,phone=?,is_shipping=?,is_billing=? WHERE address_id=?")) {
            statement.setString(index++, address.getAddress1());
            statement.setString(index++, address.getAddress2());
            statement.setString(index++, address.getFirstName());
            statement.setString(index++, address.getLastName());
            statement.setString(index++, address.getCity());
            statement.setString(index++, address.getState());
            statement.setString(index++, address.getCountry());
            statement.setString(index++, address.getPostalCode());
            statement.setString(index++, address.getPhone());
            statement.setBoolean(index++, address.isResidenceAddress());
            statement.setBoolean(index++, address.isOfficeAddress());
            statement.setString(index, address.getAddressId());

            statement.executeUpdate();
            log.debug("update_address_read_side_query_successfully executed for addressId: {}", address.getAddressId());
        } catch (SQLException e) {
            log.error("update_address_query_is_failed for address : {} due to :", address, e);
        }
    }

    private void processActivateAddress(Connection connection, BusinessAddressEvent.AddressActivated event) throws SQLException {
        int index = 1;
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE business_address SET active=? WHERE address_id=?")) {
            statement.setBoolean(index++, event.isValue());
            statement.setString(index, event.getAddressId());
            statement.executeUpdate();
            log.debug("update_business_address_activate read side query successfully executed for addressId: {}",
                    event.getAddressId());
        } catch (SQLException e) {
            log.error("update_business_address_activate query failed due to :", e);
        }
    }

    private void processDeactivateAddress(Connection connection, BusinessAddressEvent.AddressDeactivated event) throws SQLException {
        int index = 1;
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE business_address SET active=? WHERE address_id=?")) {
            statement.setBoolean(index++, event.isValue());
            statement.setString(index, event.getAddressId());
            statement.executeUpdate();
            log.debug("update_business_address_deactivate read side query successfully executed for addressId: {}",
                    event.getAddressId());
        } catch (SQLException e) {
            log.error("update_business_address_deactivate query failed due to :", e);
        }
    }

    @Override
    public PSequence<AggregateEventTag<BusinessAddressEvent>> aggregateTags() {
        return BusinessAddressEvent.TAG.allTags();
    }
}
