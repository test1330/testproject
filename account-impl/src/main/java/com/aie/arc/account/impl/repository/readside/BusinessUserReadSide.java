package com.aie.arc.account.impl.repository.readside;

import com.aie.arc.account.impl.entity.entities.BusinessUser;
import com.aie.arc.account.impl.entity.entities.vo.BusinessProfile;
import com.aie.arc.account.impl.event.BusinessUserEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.ReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcReadSide;
import lombok.extern.slf4j.Slf4j;
import org.pcollections.PSequence;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

@Slf4j
public class BusinessUserReadSide extends ReadSideProcessor<BusinessUserEvent> {

    private final JdbcReadSide jdbcReadSide;

    @Inject
    public BusinessUserReadSide(JdbcReadSide jdbcReadSide) {
        this.jdbcReadSide = jdbcReadSide;
    }

    @Override
    public ReadSideHandler<BusinessUserEvent> buildHandler() {
        return jdbcReadSide.<BusinessUserEvent>builder("businessUserEventOffset")
                .setEventHandler(BusinessUserEvent.BusinessUserCreated.class, this::insertRowForBusinessUser)
                .setEventHandler(BusinessUserEvent.BusinessUserUpdated.class, this::updateRowForBusinessUser)
                .setEventHandler(BusinessUserEvent.BusinessUserActivated.class, this::processActivateBusinessUserAccount)
                .setEventHandler(BusinessUserEvent.BusinessUserDeactivated.class, this::processDeactivateBusinessUserAccount)
                .setEventHandler(BusinessUserEvent.RoleAssignedToBusinessUser.class, this::updateRoleToBusinessUser)
                .setEventHandler(BusinessUserEvent.RoleAssignedToBusinessUser.class, this::assignRoleToBusinessUser)
                .build();
    }

    private void insertRowForBusinessUser(Connection connection, BusinessUserEvent.BusinessUserCreated event) {
//        int index = 1;
        BusinessUser businessUser = null;

        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO business_user (id,account_id, first_name," +
                        "middle_name,last_name,email,phone_number,designation,role_id,created_at,modified_at,active) VALUES" +
                        " (?,?,?,?,?,?,?,?,?,?,?,?)")) {
            businessUser = event.getBusinessUser();
            BusinessProfile profile = businessUser.getProfile();
            for(String id : businessUser.getRoleIds()) {
                int index = 1;
                statement.setString(index++, UUID.randomUUID().toString());
                statement.setString(index++, businessUser.getBusinessUserId());
                statement.setString(index++, profile.getFirstName());
                statement.setString(index++, profile.getMiddleName());
                statement.setString(index++, profile.getLastName());
                statement.setString(index++, profile.getEmail());
                statement.setString(index++, profile.getPhone());
                statement.setString(index++, businessUser.getDesignation());
                statement.setString(index++, id);
                statement.setString(index++, profile.getCreatedAt());
                statement.setString(index++, profile.getLastModifiedAt());
                statement.setBoolean(index, businessUser.isActive());

            }
            statement.execute();

            log.info("business_user executed for User Id: {}", businessUser.getBusinessUserId());
        } catch (SQLException e) {
            log.error("business_user for the account : {} due to :", businessUser, e);
        }
    }

    private void updateRowForBusinessUser(Connection connection, BusinessUserEvent.BusinessUserUpdated event) {
        int index = 1;
        BusinessUser businessUser = null;
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE business_user SET first_name=?," +
                        "middle_name=?,last_name=?,email=?,phone_number=?,modified_at=? WHERE account_id=?")) {
            businessUser = event.getBusinessUser();
            BusinessProfile businessProfile = businessUser.getProfile();
            statement.setString(index++, businessProfile.getLastName());
            statement.setString(index++, businessProfile.getMiddleName());
            statement.setString(index++, businessProfile.getLastName());
            statement.setString(index++, businessProfile.getEmail());
            statement.setString(index++, businessProfile.getPhone());
            statement.setString(index++, businessProfile.getLastModifiedAt());
            statement.setString(index, businessUser.getBusinessUserId());
            statement.executeUpdate();
            log.info("business_user executed for UserId: {}", businessUser.getBusinessUserId());
        } catch (SQLException e) {
            log.error("business_user for account : {} due to :", businessUser, e);
        }
    }


    private void assignRoleToBusinessUser(Connection connection, BusinessUserEvent.RoleAssignedToBusinessUser event) {
        event.getRoleIds().forEach(roleId -> {
            int index = 1;
            try (
                    PreparedStatement statement = connection.prepareStatement(
                            "INSERT INTO user_role (id, user_id, role_id, created_at,modified_at) VALUES (?,?,?,?,?)")) {
                statement.setString(index++, UUID.randomUUID().toString());
                statement.setString(index++, event.getBusinessUserId());
                statement.setString(index++, roleId);
                statement.setString(index++, event.getCreatedAt().toString());
                statement.setString(index, event.getLastModifiedAt().toString());
                statement.executeUpdate();
            } catch (SQLException e) {
                log.error("user_role for user id : {} due to :", event.getBusinessUserId(), e);
            }
        });
    }

    private void updateRoleToBusinessUser(Connection connection, BusinessUserEvent.RoleAssignedToBusinessUser event) {
        event.getRoleIds().forEach(roleId -> {
            int index = 1;
            try (
                    PreparedStatement statement = connection.prepareStatement(
                            "UPDATE business_user SET role_id=?,modified_at=? WHERE account_id=?")) {
                statement.setString(index++, roleId);
                statement.setString(index++, event.getLastModifiedAt().toString());
                statement.setString(index, event.getBusinessUserId());
                statement.executeUpdate();
            } catch (SQLException e) {
                log.error("business_user for business user id : {} due to :", event.getBusinessUserId(), e);
            }
        });
    }


    private void processActivateBusinessUserAccount(Connection connection, BusinessUserEvent.BusinessUserActivated event) throws SQLException {
        int index = 1;
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE business_user SET active=? WHERE account_id=?")) {
            statement.setBoolean(index++, event.isValue());
            statement.setString(index, event.getBusinessUserId());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error("business_user for user id : {} due to :", event.getBusinessUserId(), e);
        }
    }

    private void processDeactivateBusinessUserAccount(Connection connection, BusinessUserEvent.BusinessUserDeactivated event) throws SQLException {
        int index = 1;
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE business_user SET active=? WHERE account_id=?")){
        statement.setBoolean(index++, event.isValue());
        statement.setString(index, event.getBusinessUserId());
        statement.executeUpdate();
        } catch (SQLException e) {
            log.error("business_user for user id : {} due to :", event.getBusinessUserId(), e);
        }
    }

    @Override
    public PSequence<AggregateEventTag<BusinessUserEvent>> aggregateTags() {
        return BusinessUserEvent.TAG.allTags();
    }
}