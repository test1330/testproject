package com.aie.arc.account.impl.repository.readside;

import com.aie.arc.account.impl.entity.entities.Card;
import com.aie.arc.account.impl.event.CardEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.Offset;
import com.lightbend.lagom.javadsl.persistence.ReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcReadSide;
import lombok.extern.slf4j.Slf4j;
import org.pcollections.PSequence;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Slf4j
public class CardReadSide extends ReadSideProcessor<CardEvent> {

    private final JdbcReadSide jdbcReadSide;

    @Inject
    public CardReadSide(JdbcReadSide jdbcReadSide) {
        this.jdbcReadSide = jdbcReadSide;
    }

    @Override
    public ReadSideHandler<CardEvent> buildHandler() {
        return jdbcReadSide.<CardEvent>builder("cardEventOffset")
                .setEventHandler(CardEvent.CardCreated.class, this::processCreateCard)
                .setEventHandler(CardEvent.CardUpdated.class, this::processUpdateCard)
                .setEventHandler(CardEvent.CardActivated.class, this::processActivateCard)
                .setEventHandler(CardEvent.CardDeactivated.class, this::processDeactivateCard)
                .build();
    }

    private void processCreateCard(Connection connection, CardEvent.CardCreated event, Offset offset) throws SQLException {
        int index = 1;
        Card card = event.getCard();
        try {
            if(isRecordPresentForId(event.getCardId(), connection) != 0) {
                log.info("Record already present for cardId {} , performing update!", card.getCardId());
                processUpdateCard(connection, new CardEvent.CardUpdated(card), offset);
            } else {
                log.info("read_side_add_card  account_id : {}, card_id : {}, OFFSET : {}", card.getAccountId(), card.getCardId(), offset);
                PreparedStatement statement = connection.prepareStatement(
                        "INSERT INTO card(card_id,account_id,first_name,last_name,card_number,subscription_id , " +
                                "card_type,expiration_month,expiration_year,card_nick_name,active) VALUES " +
                                "(?,?,?,?,?,?,?,?,?,?,?)");
                statement.setString(index++, card.getCardId());
                statement.setString(index++, card.getAccountId());
                statement.setString(index++, card.getFirstName().length() > 40 ? card.getFirstName().substring(0, 39) : card.getFirstName());
                statement.setString(index++, card.getLastName().length() > 40 ? card.getLastName().substring(0, 39) : card.getLastName());
                statement.setString(index++, card.getCardNumber());
                statement.setString(index++, card.getSubscriptionId());
                statement.setString(index++, card.getCardType());
                statement.setString(index++, card.getExpirationMonth());
                statement.setString(index++, card.getExpirationYear());
                statement.setString(index++, card.getCardNickName());
                statement.setBoolean(index, card.isActive());
                statement.execute();
                log.info("insert_card_read_side_query_successfully executed for cardId: {}", card.getCardId());
            }

        } catch (SQLException e) {
            log.error("insert_card_query_failed for card : {} due to :", card, e);
        }

    }

    private void processUpdateCard(Connection connection, CardEvent.CardUpdated event, Offset offset) throws SQLException {
        int index = 1;
        Card card = event.getCard();
        log.info("read_side_update_card  account_id : {}, card_id : {}, OFFSET : {}", card.getAccountId(), card.getCardId(), offset);
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE card SET first_name=?,last_name=?,card_number=?,card_type=?," +
                        "expiration_month=?,expiration_year=?,active=? WHERE card_id=?")) {
            statement.setString(index++, card.getFirstName().length() > 40 ? card.getFirstName().substring(0, 39) : card.getFirstName());
            statement.setString(index++, card.getLastName().length() > 40 ? card.getLastName().substring(0, 39) : card.getLastName());
            statement.setString(index++, card.getCardNumber());
            statement.setString(index++, card.getCardType());
            statement.setString(index++, card.getExpirationMonth());
            statement.setString(index++, card.getExpirationYear());
            statement.setBoolean(index++, card.isActive());
            statement.setString(index, card.getCardId());
            statement.executeUpdate();
            log.info("update_card_read_side_query_successfully executed for cardId: {}", card.getCardId());
        } catch (SQLException e) {
            log.error("update_card_query_failed for card : {} due to :", card, e);
        }

    }

    private void processDeactivateCard(Connection connection, CardEvent.CardDeactivated event, Offset offset) throws SQLException {
        int index = 1;
        log.info("read_side_deactivate_card   card_id : {}, OFFSET : {}", event.getCardId(), offset);
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE card SET active=? WHERE card_id=?")) {
            statement.setBoolean(index++, event.isValue());
            statement.setString(index, event.getCardId());
            statement.executeUpdate();
            log.debug("update_deactivate_card read side query successfully executed for cardId: {}", event.getCardId());
        } catch (Exception e) {
            log.error(" update_deactivate_card query failed due to :", e);
        }
    }

    private void processActivateCard(Connection connection, CardEvent.CardActivated event, Offset offset) throws SQLException {
        int index = 1;
        log.info("read_side_activate_card   card_id : {}, OFFSET : {}", event.getCardId(), offset);
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE card SET active=? WHERE card_id=?")) {

            statement.setBoolean(index++, event.isValue());
            statement.setString(index, event.getCardId());
            statement.executeUpdate();
            log.debug("update_activate_card read side query successfully executed for cardId: {}", event.getCardId());
        } catch (SQLException e) {
            log.error(" update_activate_card query failed due to :", e);
        }
    }

    public int isRecordPresentForId(String id, Connection connection) throws SQLException {
        ResultSet resultSet = connection.prepareStatement(
                                "select count(*) as COUNT from card where card_id = '" + id + "'")
                                .executeQuery();
        resultSet.next();
        return resultSet.getInt(1);
    }

    @Override
    public PSequence<AggregateEventTag<CardEvent>> aggregateTags() {
        return CardEvent.TAG.allTags();
    }
}
