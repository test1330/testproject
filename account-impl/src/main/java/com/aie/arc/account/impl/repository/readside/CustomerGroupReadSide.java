package com.aie.arc.account.impl.repository.readside;

import com.aie.arc.account.impl.entity.entities.CustomerGroup;
import com.aie.arc.account.impl.event.CustomerGroupEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.ReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcReadSide;
import lombok.extern.slf4j.Slf4j;
import org.pcollections.PSequence;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;
@Slf4j
public class CustomerGroupReadSide extends ReadSideProcessor<CustomerGroupEvent> {

    private final JdbcReadSide jdbcReadSide;

    @Inject
    public CustomerGroupReadSide(JdbcReadSide jdbcReadSide) {
        this.jdbcReadSide = jdbcReadSide;
    }

    @Override
    public ReadSideHandler<CustomerGroupEvent> buildHandler() {
        return jdbcReadSide.<CustomerGroupEvent>builder("customerGroupEventOffset")
                .setEventHandler(CustomerGroupEvent.CustomerGroupCreated.class, this::processCreateCustomerGroup)
                .setEventHandler(CustomerGroupEvent.CustomerGroupUpdated.class, this::processUpdateCustomerGroup)
                .setEventHandler(CustomerGroupEvent.CustomerGroupActivated.class, this::processActivateCustomerGroup)
                .setEventHandler(CustomerGroupEvent.CustomerGroupDeactivated.class, this::processDeactivateCustomerGroup)
                .build();
    }

    private void processCreateCustomerGroup(Connection connection, CustomerGroupEvent.CustomerGroupCreated event) {
        event.getCustomerGroup().getLocalization().forEach((code, localizedFieldsVO) -> {
            int index = 1;
            CustomerGroup customerGroup = null;
            try (
                PreparedStatement statement = connection.prepareStatement(
                        "INSERT INTO customer_group (id, customer_group_id, name, recency, frequency, monetary," +
                                "localeCode,active) VALUES (?,?,?,?,?,?,?,?)")){
                customerGroup = event.getCustomerGroup();
                statement.setString(index++, UUID.randomUUID().toString());
                statement.setString(index++, customerGroup.getCustomerGroupId());
                statement.setString(index++, localizedFieldsVO.getName());
                statement.setInt(index++, customerGroup.getRecency());
                statement.setInt(index++, customerGroup.getFrequency());
                statement.setDouble(index++, customerGroup.getMonetary());
                statement.setString(index++, code);
                statement.setBoolean(index, customerGroup.isActive());
                statement.execute();
                log.info("insert_customer_group_side_query_successfully executed for customer ids: {}", customerGroup.getCustomerIds());
            } catch (SQLException e) {
                log.error("insert_customer_group_query_is_failed for customerGroup : {} due to :",customerGroup, e);
            }
        });
    }

    private void processUpdateCustomerGroup(Connection connection, CustomerGroupEvent.CustomerGroupUpdated event) {
        event.getCustomerGroup().getLocalization().forEach((code, localizedFieldsVO) -> {
            int index = 1;
            CustomerGroup customerGroup = null;
            try (
                PreparedStatement statement = connection.prepareStatement(
                        "UPDATE customer_group SET name=?, recency = ?, frequency = ?,monetary=?, active=? WHERE customer_group_id=? and localeCode=?")){
                customerGroup = event.getCustomerGroup();
                statement.setString(index++, localizedFieldsVO.getName());
                statement.setInt(index++, customerGroup.getRecency());
                statement.setInt(index++, customerGroup.getFrequency());
                statement.setDouble(index++, customerGroup.getMonetary());
                statement.setBoolean(index++, customerGroup.isActive());
                statement.setString(index++, customerGroup.getCustomerGroupId());
                statement.setString(index, code);
                statement.executeUpdate();
                log.info("update_customer_group_side_query_successfully executed for customer ids: {}", customerGroup.getCustomerIds());
            } catch (SQLException e) {
                log.error("update_customer_group_query_is_failed for customerGroup : {} due to :",customerGroup, e);
            }
        });
    }

    private void processDeactivateCustomerGroup(Connection connection, CustomerGroupEvent.CustomerGroupDeactivated event) throws
            SQLException {
        int index = 1;
        try (
                PreparedStatement statement = connection.prepareStatement(
                "UPDATE customer_group SET active=? WHERE customer_group_id=?")){
        statement.setBoolean(index++, event.isValue());
        statement.setString(index, event.getCustomerGroupId());
        statement.executeUpdate();
            log.info("Deactivate_customer_group_side_query_successfully executed for customer ids: {}", event.getCustomerGroupId());
        } catch (SQLException e) {
            log.error("Deactivate_customer_group_query_is_failed for customerGroup : {} due to :",event, e);
        }
    }
    private void processActivateCustomerGroup(Connection connection, CustomerGroupEvent.CustomerGroupActivated event) throws
            SQLException {
        int index = 1;
        try(PreparedStatement statement = connection.prepareStatement(
                "UPDATE customer_group SET active=? WHERE customer_group_id=?")){
        statement.setBoolean(index++, event.isValue());
        statement.setString(index, event.getCustomerGroupId());
        statement.executeUpdate();
            log.info("Activate_customer_group_side_query_successfully executed for customer ids: {}", event.getCustomerGroupId());
        } catch (SQLException e) {
            log.error("Activate_customer_group_query_is_failed for customerGroup : {} due to :",event, e);
        }
    }

    @Override
    public PSequence<AggregateEventTag<CustomerGroupEvent>> aggregateTags() {
        return CustomerGroupEvent.TAG.allTags();
    }
}
