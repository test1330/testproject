package com.aie.arc.account.impl.repository.repository;

import com.aie.arc.account.api.common.exceptions.ParameterRequired;
import com.aie.arc.account.impl.repository.repository.readside.LocaleReadSide;
import com.lightbend.lagom.javadsl.persistence.ReadSide;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcSession;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.StringJoiner;
import java.util.concurrent.CompletionStage;

@Singleton
@Slf4j
public class LocaleRepository {
    private final JdbcSession jdbcSession;

    @Inject
    public LocaleRepository(JdbcSession jdbcSession, ReadSide readSide) {
        this.jdbcSession = jdbcSession;
        readSide.register(LocaleReadSide.class);
    }


    public CompletionStage<Integer> validateAllLocaleCodes(StringJoiner codes) {
        StringBuilder builder = new StringBuilder();
        builder.append(" WHERE codes in( ").append(codes.toString()).append(") and isValue=true");
        return jdbcSession.withConnection(connection -> getDistinctCount(connection, builder));
    }

    private int getDistinctCount(Connection connection, StringBuilder dynamicQuery) {
        try(PreparedStatement statement = connection.prepareStatement(
                new StringBuilder("SELECT DISTINCT codes FROM locale").append(dynamicQuery).toString());
            ResultSet countRes = statement.executeQuery())
        {
            int count = 0;
            while (countRes.next()) {
                count++;
            }
            return count;
        } catch (SQLException e) {
            throw new ParameterRequired(new String[]{"SQL Exception encountered!"});
        }
    }
}

