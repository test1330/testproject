package com.aie.arc.account.impl.repository.repository.readside;

import com.aie.arc.tool.master.subscribe.event.LocaleEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.ReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcReadSide;
import lombok.extern.slf4j.Slf4j;
import org.pcollections.PSequence;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

@Slf4j
public class LocaleReadSide extends ReadSideProcessor<LocaleEvent> {
    private final JdbcReadSide jdbcReadSide;

    @Inject
    public LocaleReadSide(JdbcReadSide jdbcReadSide) {
        this.jdbcReadSide = jdbcReadSide;
    }

    @Override
    public ReadSideHandler<LocaleEvent> buildHandler() {
        return jdbcReadSide.<LocaleEvent>builder("localeOffset")
                .setEventHandler(LocaleEvent.LocaleCreated.class, this::processLocaleCreate)
                .setEventHandler(LocaleEvent.LocaleUpdated.class, this::processLocaleUpdate)
                .setEventHandler(LocaleEvent.LocaleDeactivated.class, this::processLocaleRemove)
                .build();
    }

    private void processLocaleCreate(Connection connection, LocaleEvent.LocaleCreated event) {
        event.getLocale().getLocalizedFieldsVOMap().forEach((code, localizedFieldsVO) -> {
            int index = 1;
            try(PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO locale (id, localeId, countryId, name, codes, descriptions,isValue) VALUES (?,?, ?, ?, ?, ?, ?)"))
            {
                statement.setString(index++, UUID.randomUUID().toString());
                statement.setString(index++, event.getLocale().getLocaleId());
                statement.setString(index++, event.getLocale().getCountryId());
                statement.setString(index++, localizedFieldsVO.getName());
                statement.setString(index++, event.getLocale().getCode());
                statement.setString(index++, localizedFieldsVO.getDescription());
                statement.setBoolean(index, event.getLocale().isActive());
                statement.execute();
            } catch (SQLException e) {
                log.error("Error occurred while inserting into table locale "+e);
            }
        });
    }

    private void processLocaleUpdate(Connection connection, LocaleEvent.LocaleUpdated event) {
        event.getLocale().getLocalizedFieldsVOMap().forEach((code, localizedFieldsVO) -> {
            int index = 1;
            try(PreparedStatement statement = connection.prepareStatement(
                    "UPDATE locale SET name = ? , codes=? , descriptions=? WHERE localeId=?"))
            {
                statement.setString(index++, localizedFieldsVO.getName());
                statement.setString(index++, event.getLocale().getCode());
                statement.setString(index++, localizedFieldsVO.getDescription());
                statement.setString(index, event.getLocaleId());
                statement.executeUpdate();
            } catch (SQLException e) {
                log.error("Error occurred while updating table locale "+e);
            }
        });
    }

    private void processLocaleRemove(Connection connection, LocaleEvent.LocaleDeactivated event) throws SQLException {
        try(PreparedStatement statement = connection.prepareStatement(
                "UPDATE locale SET isValue=? WHERE localeId=?"))
        {
            statement.setBoolean(1, false);
            statement.setString(2, event.getLocaleId());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error("Error occurred while updating table locale "+e);
        }
    }

    @Override
    public PSequence<AggregateEventTag<LocaleEvent>> aggregateTags() {
        return LocaleEvent.TAG.allTags();
    }
}