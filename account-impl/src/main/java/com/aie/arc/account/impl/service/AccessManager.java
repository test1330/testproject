package com.aie.arc.account.impl.service;

import akka.Done;
import akka.cluster.sharding.typed.javadsl.EntityRef;
import akka.http.javadsl.model.StatusCodes;
import akka.japi.Pair;
import com.aie.arc.account.api.common.request.ActivationRequest;
import com.aie.arc.account.api.common.request.LocalisedFields;
import com.aie.arc.account.api.common.request.SearchRequest;
import com.aie.arc.account.api.common.response.BaseResponse;
import com.aie.arc.account.impl.entity.entities.vo.common.FilterParameter;
import com.aie.arc.account.impl.repository.repository.LocaleRepository;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.aie.arc.account.impl.util.common.FilterConfig;
import com.aie.arc.rbac.api.constants.RBACURIConstants;
import com.aie.arc.rbac.api.request.AccessRightRequest;
import com.aie.arc.rbac.api.request.UpdateAccessRightRequest;
import com.aie.arc.rbac.api.response.AccessRightDetail;
import com.aie.arc.rbac.api.response.AccessRightDetails;
import com.aie.arc.rbac.impl.command.AccessRightCommand;
import com.aie.arc.rbac.impl.entity.entities.AccessRight;
import com.aie.arc.rbac.impl.entity.entities.RBACLocalizedFieldsVO;
import com.aie.arc.rbac.impl.mapper.RBACMapper;
import com.aie.arc.rbac.impl.repository.AccessRightRepository;
import com.aie.arc.rbac.impl.repository.CommonRepository;
import com.aie.arc.rbac.impl.service.RBACServiceImplCommon;
import com.aie.arc.rbac.impl.service.RBACServiceRequestValidation;
import com.google.inject.Inject;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Singleton;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static com.aie.arc.account.impl.util.MessageConstants.SUCCESS_STATUS;

@Singleton
@Slf4j
public class AccessManager {

    @Inject
    private RBACServiceRequestValidation rbacServiceValidation;

    @Inject
    private RBACServiceImplCommon rbacServiceImplCommon;

    @Inject
    private LocaleRepository localeRepository;

    @Inject
    private AccountServiceValidation accountServiceValidation;

    @Inject
    private AccessRightRepository accessRightRepository;

    @Inject
    private FilterValidation filterValidation;

    @Inject
    private CommonRepository commonRepository;


    public CompletionStage<Pair<ResponseHeader, BaseResponse>> createAccessRight(AccessRightRequest request){
        log.debug("Create Access Right :");
            rbacServiceValidation.validateJsonRequest(request);
            request.getLocalization().values().forEach(field -> rbacServiceValidation.validateIfAccessRightAlreadyCreated(field.getName(), commonRepository));
            String id = StringUtils.isAnyBlank(request.getId())? rbacServiceImplCommon.getNewId():request.getId();
            EntityRef<AccessRightCommand> ref = rbacServiceImplCommon.getEntityRefForAccessRight(id);
            Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap = rbacServiceValidation.validateLocaleCodesAndGetVoMap(request.getLocalization(), localeRepository);
            AccessRight accessRight = AccessRight.createAccessRightByRequest(id, localizedFieldsVOMap, request.getActive());
        log.debug("Create Access Right with : [localization: {}, active: {}]", request.getLocalization().values(), request.getActive());
            return ref.<Done>ask(replyTo -> new AccessRightCommand.CreateAccessRight(accessRight,replyTo), ServiceConstants.ASK_TIMEOUT)
                    .thenApply(done -> rbacServiceValidation.sendResponseByStatusIdAndRequest(id, StatusCodes.CREATED.intValue(), RBACURIConstants.CREATE_ACCESS_RIGHT_URI, RBACURIConstants.ACCESS_RIGHT_CREATED_MESSAGE, request));
        }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> updateAccessRight(UpdateAccessRightRequest request, String accessId){
        log.debug("Update Access Right with accessId: {}", accessId);
            rbacServiceValidation.validateJsonRequest(request);
            AccessRight accessRight = rbacServiceValidation.validateAndGetAccessRightById(accessId);
            rbacServiceValidation.validateLocaleCodesAndGetVoMap(request.getLocalization(), localeRepository);
           AccessRight updateAccessRight = AccessRight.updateAccessRightByRequest(accessRight, request);
        log.debug("Update Access Right with : [localization: {}], active: {} for accessId: {}", request.getLocalization().values(), request.getActive(), accessId);
        return rbacServiceImplCommon.getEntityRefForAccessRight(accessId).<Done>ask(replyTo -> new AccessRightCommand.UpdateAccessRight(updateAccessRight,replyTo), ServiceConstants.ASK_TIMEOUT)
                    .thenApply(done -> rbacServiceValidation.sendResponseByStatusIdAndRequest(accessId, SUCCESS_STATUS, RBACURIConstants.UPDATE_ACCESS_RIGHT_URI, RBACURIConstants.ACCESS_RIGHT_UPDATED_MESSAGE, request));
    }


    public CompletionStage<Pair<ResponseHeader, AccessRightDetail>> getAccessRight(String accessId) {
        log.debug("Get Access Right  with accessId: {}", accessId);
            rbacServiceValidation.validateAccessRightId(accessId);
            return rbacServiceImplCommon.getEntityRefForAccessRight(accessId).ask(AccessRightCommand.GetAccessRight::new, ServiceConstants.ASK_TIMEOUT)
                    .thenApply(accessRight -> Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), RBACMapper.toAccessRightDetails(accessRight.get())));
    }

    public CompletionStage<Pair<ResponseHeader, AccessRightDetails>> getAllAccessRights(SearchRequest request){
        log.debug("Get all Access Rights :");
            accountServiceValidation.validateJsonRequest(request);
            filterValidation.validateFilterParameter(request.getPagination().getPageNo(),
                    request.getPagination().getPageSize(), request.getFilters(), FilterConfig.getAccessRightTypeMap(), request.getSorting().getField());
            FilterParameter filterParameter = FilterParameter.getNewInstance(Integer.valueOf(request.getPagination().getPageNo()),
                    Integer.valueOf(request.getPagination().getPageSize()), request.getSorting().getAscending(), request.getSorting().getField(), request.getFilters());
            return accessRightRepository.getAccessRightIds(filterParameter).thenApply(
                    listWrapper -> {
                        List<AccessRightDetail> accessRightDetails = listWrapper.getList().stream()
                                .map(rbacServiceValidation::validateAndGetAccessRightById)
                                .map(RBACMapper::toAccessRightDetails)
                                .collect(Collectors.toList());
                        AccessRightDetails obj = new AccessRightDetails(accessRightDetails, listWrapper.getTotalSize());
                        return Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), obj);
                    }
            );
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> deactivateAccessRight(ActivationRequest request, String accessId) {
        log.debug("Deactivate Access Right with accessId: {} using request flag : {}", accessId, request.getValue());
        if(request.getValue()) {
            return rbacServiceValidation.validateAndActivateAccessRight(accessId, request);
        }
            return rbacServiceValidation.validateAndDeactivateAccessRight(accessId, request);

    }
}
