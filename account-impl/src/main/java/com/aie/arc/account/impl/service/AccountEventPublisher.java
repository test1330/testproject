
package com.aie.arc.account.impl.service;

import com.aie.arc.account.api.publishedevent.*;
import com.aie.arc.account.impl.command.*;
import com.aie.arc.account.impl.entity.entities.*;
import com.aie.arc.account.impl.event.*;
import com.aie.arc.account.impl.mapper.Mapper;
import com.aie.arc.account.impl.util.ServiceConstants;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static com.aie.arc.account.impl.util.MessageConstants.CONVERTING_NON_PUB_EVENT;

@Singleton
@Slf4j
public class AccountEventPublisher {
    private final AccountServiceImplCommon accountServiceCommon;

    private final Duration askTimeout = Duration.ofSeconds(10);

    @Inject
    public AccountEventPublisher(AccountServiceImplCommon accountServiceCommon) {
        this.accountServiceCommon = accountServiceCommon;
    }

    /*Publish Account events*/
    public CompletionStage<AccountEvents> convertPublishedEventForAccount(AccountEvent event) {
        log.debug("Publishing AccountCreated event :{}", event.getAccountId());
        return getAccount(event)
                .thenApply(item -> {
                    log.debug("Event Publishing type :{}", event.getClass().getName());
                    if(!item.isPresent()) {
                        log.error("No record found in database for accountId: {} and event type is :{} " +
                                "Publishing dummy event for Account", event.getAccountId(),event.getClass().getName());
                        return new AccountEvents.AccountEvent("dummy", "dummy", "dummy",
                                "dummy", "dummy", "dummy", new ArrayList<>(), new ArrayList<>(),
                                "dummy", "dummy", new ArrayList<>(),
                                "dummy", "dummy");
                    }
                    Account account = item.get();
                    return new AccountEvents.AccountEvent(account.getAccountId(), account.getProfile().getEmail(),
                            account.getProfile().getFirstName(), account.getProfile().getLastName(),
                            account.getMyZip(), account.getBmRoleId(), account.getFlashSaleLimitedIds(), account.
                            getAddressIds(), account.getDefaultBillingAddressId(), account.getDefaultBillingAddressId(),
                            account.getCardIds(), account.getDefaultCardId(), event.getClass().getName());
                });
    }

    /*Publish card events*/
    public CompletionStage<CardEvents> convertPublishedEventForCard(CardEvent event) {
        if (event instanceof CardEvent.CardCreated) {
            log.debug("Published CardEvent for card_id: {}", event.getCardId());
            return getCard(event)
                    .thenApply(item -> {
                        if(!item.isPresent()) {
                            log.info("No record found in database for cardId: {}. " +
                                    "Publishing dummy event CardEvents.CardCreated.", event.getCardId());
                            return new CardEvents.CardCreated("dummy", "dummy", "dummy",
                                    "dummy", "dummy", "dummy", "dummy",
                                    "dummy", false, "dummy", false,
                                    "dummy", "dummy", "dummy");
                        }
                        Card card = item.get();
                        return new CardEvents.CardCreated(card.getAccountId(), card.getCardId(),
                                card.getFirstName(), card.getLastName(), card.getCardType(), card.getCardNumber(),
                                card.getExpirationMonth(), card.getExpirationYear(), false, "ADD",
                                card.isDefaultCreditCard(), LocalDateTime.now().toString(),
                                LocalDateTime.now().toString(), card.getSubscriptionId());
                    });
        } else if (event instanceof CardEvent.CardUpdated) {
            log.debug("Published CardUpdated for card_id: {}", event.getCardId());
            return getCard(event)
                    .thenApply(item -> {
                        if(!item.isPresent()) {
                            log.info("No record found in database for cardId: {}. " +
                                    "Publishing dummy event CardEvents.CardUpdated. ", event.getCardId());
                            return new CardEvents.CardUpdated("dummy", "dummy", "dummy",
                                    "dummy", "dummy", "dummy", "dummy",
                                    "dummy", false, "dummy", false,
                                    "dummy");
                        }
                        Card card = item.get();
                        return new CardEvents.CardUpdated(card.getAccountId(), card.getCardId(),
                                card.getFirstName(), card.getLastName(), card.getCardType(), card.getCardNumber(),
                                card.getExpirationMonth(), card.getExpirationYear(), false, "UPDATE",
                                card.isDefaultCreditCard(), LocalDateTime.now().toString());
                    });
        } else if (event instanceof CardEvent.CardDeactivated) {
            log.debug("Published CardDeactivated event for card_id: {}", event.getCardId());
            return getCard(event)
                    .thenApply(item -> {
                        if(!item.isPresent()) {
                            log.info("No record found in database for cardId: {}. " +
                                    "Publishing dummy event CardEvents.CardDeactivated.", event.getCardId());
                            return new CardEvents.CardDeactivated("dummy", "dummy", "dummy");
                        }
                        Card card = item.get();
                        return new CardEvents.CardDeactivated(card.getCardId(), "DELETE",
                                LocalDateTime.now().toString());
                    });
        }
        log.error("Try to convert non public CardEvent: {}", event);
        throw new IllegalArgumentException(CONVERTING_NON_PUB_EVENT);
    }

    /*publish Address events*/
    public CompletionStage<AddressEvents> convertPublishedEventForAddress(AddressEvent event) {
        if (event instanceof AddressEvent.AddressCreated) {
            log.debug("Published AddressCreated event for address_id: {}", event.getAddressId());
            return getAddress(event)
                    .thenApply(item -> {
                        if(!item.isPresent()) {
                            log.debug("No record found in database for addressId: {}. " +
                                    "Publishing dummy event AddressEvent.AddressCreated.", event.getAddressId());
                            return new AddressEvents.AddressCreated("dummy", "dummy", "dummy",
                                    "dummy", "dummy", "dummy", "dummy", "dummy",
                                    "dummy", "dummy", false, false,
                                    false, "dummy", "dummy", "dummy");
                        }
                        Address address = item.get();
                        return new AddressEvents.AddressCreated(address.getAccountId(), address.getAddressId(),
                                address.getFirstName(), address.getLastName(), address.getAddress1(), address.getAddress2(),
                                address.getCity(), address.getState(), address.getPostalCode(), address.getPhone(),
                                address.isDefaultShipping(), address.isDefaultBilling(), false,
                                LocalDateTime.now().toString(), LocalDateTime.now().toString(), "ADD");
                    });
        }
        if (event instanceof AddressEvent.AddressUpdated) {
            log.debug("Published AddressUpdated event for address_id: {}", event.getAddressId());
            return getAddress(event)
                    .thenApply(item -> {
                        if(!item.isPresent()) {
                            log.debug("No record found in database for addressId: {}. " +
                                    "Publishing dummy event AddressEvent.AddressUpdated.", event.getAddressId());
                            return new AddressEvents.AddressUpdated("dummy", "dummy", "dummy",
                                    "dummy", "dummy", "dummy", "dummy", "dummy",
                                    "dummy", "dummy", false, false,
                                    false, "dummy", "dummy", "dummy");
                        }
                        Address address = item.get();
                        return new AddressEvents.AddressCreated(address.getAccountId(), address.getAddressId(),
                                address.getFirstName(), address.getLastName(), address.getAddress1(), address.getAddress2(),
                                address.getCity(), address.getState(), address.getPostalCode(), address.getPhone(),
                                address.isDefaultShipping(), address.isDefaultBilling(), false,
                                LocalDateTime.now().toString(), LocalDateTime.now().toString(), "UPDATE");
                    });
        }
        if (event instanceof AddressEvent.AddressDeactivated) {
            log.debug("Published AddressDeactivated event for address_id: {}", event.getAddressId());
            return getAddress(event)
                    .thenApply(item -> {
                        if(!item.isPresent()) {
                            log.debug("No record found in database for addressId: {}. " +
                                    "Publishing dummy event AddressEvent.AddressDeactivated.", event.getAddressId());
                            return new AddressEvents.AddressDeactivated("dummy", "dummy", "dummy");
                        }
                        Address address = item.get();
                        return new AddressEvents.AddressDeactivated(address.getAddressId(),
                                "DELETE", LocalDateTime.now().toString());
                    });
        }
        log.error("Try to convert non public AddressEvent: {}", event);
        throw new IllegalArgumentException(CONVERTING_NON_PUB_EVENT);
    }


    private CompletionStage<Optional<Account>> getAccount(AccountEvent event) {
        return accountServiceCommon.getEntityRefForAccount(event.getAccountId())
                .ask(AccountCommand.GetAccount::new, askTimeout);
    }

    private CompletionStage<Optional<Card>> getCard(CardEvent event) {
        return accountServiceCommon.getEntityRefForCard(event.getCardId())
                .ask(CardCommand.GetCard::new, ServiceConstants.ASK_TIMEOUT);
    }

    private CompletionStage<Optional<Address>> getAddress(AddressEvent event) {
        return accountServiceCommon.getEntityRefForAddress(event.getAddressId())
                .ask(AddressCommand.GetAddress::new, ServiceConstants.ASK_TIMEOUT);
    }

    /*Publish Customer Group Events*/
    public CompletionStage<CustomerGroupEvents> convertPublishedEventFoCustomerGroup(CustomerGroupEvent event) {
        if (event instanceof CustomerGroupEvent.CustomerGroupCreated) {
            return getCustomerGroup(event)
                    .thenApply(item -> {
                        CustomerGroup customerGroup = item.get();
                        return new CustomerGroupEvents.CustomerGroupCreated(customerGroup.getCustomerGroupId(), customerGroup.getCustomerIds(), customerGroup.getRecency(), customerGroup.getFrequency(), customerGroup.getMonetary(), customerGroup.isActive(), LocalDateTime.now());
                    });
        }

        if (event instanceof CustomerGroupEvent.CustomerGroupUpdated)
            return getCustomerGroup(event)
                    .thenApply(item -> {
                        CustomerGroup customerGroup = item.get();
                        return new CustomerGroupEvents.CustomerGroupUpdated(customerGroup.getCustomerGroupId(), customerGroup.getCustomerIds(), customerGroup.getRecency(), customerGroup.getFrequency(), customerGroup.getMonetary(), customerGroup.isActive(), LocalDateTime.now());
                    });
        if (event instanceof CustomerGroupEvent.CustomerGroupActivated)
            return getCustomerGroup(event)
                    .thenApply(item -> {
                        CustomerGroup customerGroup = item.get();
                        return new CustomerGroupEvents.CustomerGroupActivated(customerGroup.getCustomerGroupId(), customerGroup.isActive(), LocalDateTime.now());
                    });
        if (event instanceof CustomerGroupEvent.CustomerGroupDeactivated)
            return getCustomerGroup(event)
                    .thenApply(item -> {
                        CustomerGroup customerGroup = item.get();
                        return new CustomerGroupEvents.CustomerGroupDeactivated(customerGroup.getCustomerGroupId(), customerGroup.isActive(), LocalDateTime.now());
                    });

        throw new IllegalArgumentException(CONVERTING_NON_PUB_EVENT);
    }

    private CompletionStage<Optional<CustomerGroup>> getCustomerGroup(CustomerGroupEvent event) {
        return accountServiceCommon.getEntityRefForCustomerGroup(event.getCustomerGroupId())
                .ask(CustomerGroupCommand.GetCustomerGroup::new, ServiceConstants.ASK_TIMEOUT);
    }


    /*Publish Business User events*/
    public CompletionStage<BusinessUserEvents> convertPublishedEventForBusinessUser(BusinessUserEvent event) {
        /*if (event instanceof BusinessUserEvent.BusinessUserCreated) {
            log.debug("Publishing BusinessUserCreated event :{}", event.getBusinessUserId());
            return businessUser(event)
                    .thenApply(item -> {
                        BusinessUser businessUser = item.get();
                        return new BusinessUserEvents.BusinessUserCreated(businessUser.getBusinessUserId(), Mapper.mapToBusinessProfileAPI(businessUser), businessUser.getResidenceAddressId(), businessUser.getOfficeAddressId(), businessUser.getDesignation(), businessUser.getRoleIds(), businessUser.isActive(), LocalDateTime.now().toString());
                    });
        }*/
        if (event instanceof BusinessUserEvent.BusinessUserUpdated){
            log.info("Publishing BusinessUserUpdated event :{}", event.getBusinessUserId());
            return businessUser(event)
                    .thenApply(item -> {
                        BusinessUser businessUser = item.get();
                        return new BusinessUserEvents.BusinessUserUpdated(businessUser.getBusinessUserId(), Mapper.mapToBusinessProfileAPI(businessUser),businessUser.getDesignation(), businessUser.getRoleIds(), businessUser.isActive(), LocalDateTime.now().toString());
                    });
        }
        if (event instanceof BusinessUserEvent.BusinessUserActivated){
            log.info("Publishing BusinessUserActivated event :{}", event.getBusinessUserId());
            return businessUser(event)
                    .thenApply(item -> {
                        BusinessUser businessUser = item.get();
                        return new BusinessUserEvents.BusinessUserActivated(businessUser.getBusinessUserId(), Mapper.mapToBusinessProfileAPI(businessUser),businessUser.getDesignation(), businessUser.getRoleIds(), businessUser.isActive(), LocalDateTime.now().toString());
                    });
        }
        if (event instanceof BusinessUserEvent.BusinessUserDeactivated){
            log.info("Publishing BusinessUserActivated event :{}", event.getBusinessUserId());
            return businessUser(event)
                    .thenApply(item -> {
                        BusinessUser businessUser = item.get();
                        return new BusinessUserEvents.BusinessUserDeactivated(businessUser.getBusinessUserId(), Mapper.mapToBusinessProfileAPI(businessUser),businessUser.getDesignation(), businessUser.getRoleIds(), businessUser.isActive(), LocalDateTime.now().toString());
                    });
        }
        log.error("Try to convert non public BusinessUserEvent: {}", event);
        throw new IllegalArgumentException(CONVERTING_NON_PUB_EVENT);
    }

    private CompletionStage<Optional<BusinessUser>> businessUser(BusinessUserEvent event) {
        return accountServiceCommon.getEntityRefForBusinessUser(event.getBusinessUserId())
                .ask(BusinessUserCommand.GetBusinessUser::new, ServiceConstants.ASK_TIMEOUT);
    }

}
