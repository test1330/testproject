package com.aie.arc.account.impl.service;

import akka.Done;
import akka.japi.Pair;
import com.aie.arc.account.api.request.AccountAddressRequest;
import com.aie.arc.account.api.request.CreateCustomerRequest;
import com.aie.arc.account.api.response.CreateCustomerResponse;
import com.aie.arc.account.impl.command.AccountCommand;
import com.aie.arc.account.impl.command.BusinessUserCommand;
import com.aie.arc.account.impl.entity.entities.Account;
import com.aie.arc.account.impl.entity.entities.BusinessUser;
import com.aie.arc.account.impl.entity.entities.vo.*;
import com.aie.arc.account.impl.repository.AccountRepository;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lightbend.lagom.javadsl.api.broker.Message;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import com.lightbend.lagom.javadsl.broker.kafka.KafkaMetadataKeys;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executors;

import static com.aie.arc.account.impl.util.MessageConstants.*;

@Slf4j
@Singleton
class AccountEventSubscriber {
    private final AccountServiceValidation accountServiceValidation;
    private final AccountServiceImplCommon accountServiceImplCommon;
    private final AddressManager addressManager;
    private final IntegrationServiceManager integrationServiceManager;
    @Inject
    private AccountRepository accountRepository;

    @Inject
    AccountEventSubscriber(AccountServiceValidation accountServiceValidation,
                           AccountServiceImplCommon accountServiceImplCommon, AddressManager addressManager,
                           IntegrationServiceManager integrationServiceManager) {
        this.accountServiceValidation = accountServiceValidation;
        this.accountServiceImplCommon = accountServiceImplCommon;
        this.addressManager = addressManager;
        this.integrationServiceManager = integrationServiceManager;
    }

    /**
     * This method is use to Consume User that Publish By User Authentication.
     * throws a Kafka consume exception if any error in the consume.
     */
    CompletionStage<Done> subscribeRegisterUser(Message<String> userKafkaMessage) {
        log.info("subscribe_user kafka_message_key : {} , TOPIC : {}, PARTITION : {} , OFFSET : {} " +
                        "and TIMESTAMP : {}", userKafkaMessage.messageKeyAsString(),
                userKafkaMessage.get(KafkaMetadataKeys.TOPIC), userKafkaMessage.get(KafkaMetadataKeys.PARTITION),
                userKafkaMessage.get(KafkaMetadataKeys.OFFSET), userKafkaMessage.get(KafkaMetadataKeys.TIMESTAMP));
        log.trace("user_kafka_message : {}", userKafkaMessage.getPayload());
        try {
            ObjectMapper obj = new ObjectMapper();
            User user = obj.readValue(userKafkaMessage.getPayload(), User.class);
            log.info("register_user for accountId : {} ", user.getUserId());
            if (ACCOUNT_TYPE_MEMBER.equalsIgnoreCase(user.getAccountType()) ||
                    ACCOUNT_TYPE_WARRANTECH.equalsIgnoreCase(user.getAccountType())) {
                registerAccount(user);
                log.info("user_registered_successfully for accountId : {} ", user.getUserId());
           /* } else if(user.getAccountType().equalsIgnoreCase(ACCOUNT_TYPE_BUSINESS)){
                businessUser(user);
                log.info("user_registered_successfully for accountId : {} ", user.getUserId());*/
            } else {
                log.error("error_in_consuming_register_user_kafka_message : {} with type : {}",
                        userKafkaMessage, user.getAccountType());
            }
        } catch (Exception ex) {
            log.error("error_in_consuming_register_user_kafka_message : {} with error :", userKafkaMessage, ex);
        }
        return CompletableFuture.completedFuture(Done.getInstance());
    }

    public void registerAccount(User user) {
        accountServiceImplCommon.getEntityRefForAccount(user.getUserId())
                .ask(AccountCommand.GetAccount::new, ServiceConstants.ASK_TIMEOUT)
                .thenAccept(optionalAccount -> {
                    log.debug("registerAccount - thenAccept-  thread name: =========" + Thread.currentThread().getName());
                    if (!optionalAccount.isPresent()) {
                        log.info("create_account with ID : {} ", user.getUserId());
                        accountServiceImplCommon.getEntityRefForAccount(user.getUserId())
                                .<Done>ask(replyTo -> new AccountCommand.RegisterAccount(
                                        createAccount(user), replyTo), ServiceConstants.ASK_TIMEOUT)
                                .thenAccept(done ->
                                        CompletableFuture.runAsync(() -> validateAndUpdateCustomerId(user),
                                                Executors.newFixedThreadPool(1))
                                );

                        if (CollectionUtils.isNotEmpty(user.getGuestUserAddresses())) {
                            log.info("Process the Guest User addresses of Account ID : {}", user.getUserId());
                            user.getGuestUserAddresses().stream()
                                    .filter(Objects::nonNull)
                                    .forEach(address -> addressManager.addAccountAddress(
                                            mapToAccountAddressRequest(address), user.getUserId(), null));
                        }

                        /*//Profile sync with BMCIS called from here.
                        integrationServiceManager.validateCreateCustomerSOAPService(new CreateCustomerRequest(
                                user.getUserId(), "web", user.getFirstName(), user.getLastName(), user.getEmail()))
                                .thenAccept(bmcisResponsePair -> log.debug("customerId in response : {}",
                                        bmcisResponsePair == null || bmcisResponsePair.second() == null ? null :
                                                bmcisResponsePair.second().getCustomerId()));*/
                    } else {
                        log.info("update_account with ID : {} ", user.getUserId());
                        accountServiceImplCommon.getEntityRefForAccount(optionalAccount.get().getAccountId())
                                .<Done>ask(replyTo -> new AccountCommand.UpdateAccount(
                                        updateAccount(user, optionalAccount.get()), replyTo), ServiceConstants.ASK_TIMEOUT);
                    }
                });
    }

    private AccountAddressRequest mapToAccountAddressRequest(GuestUserAddress guestUserAddress) {

        return new AccountAddressRequest(guestUserAddress.getFirstName(), guestUserAddress.getLastName(),
                guestUserAddress.getAddress1(), guestUserAddress.getAddress2(), guestUserAddress.getCity(),
                guestUserAddress.getState(), guestUserAddress.getCountry(), guestUserAddress.getPostalCode(),
                guestUserAddress.getPhoneNumber(), guestUserAddress.isDefaultShipping(),
                guestUserAddress.isDefaultBilling(), guestUserAddress.getFipscounty(), guestUserAddress.getFipsstate(),
                guestUserAddress.getPhonenumberAlt(), guestUserAddress.isAvsvalid(), guestUserAddress.isSkipavsvalidation());
    }

    /*Create customer account*/
    private Account createAccount(User user) {
        log.debug("Creating account for user : {} ", user.getUserId());
        SocialIdentity socialIdentity = new SocialIdentity(null, null, null);
        Profile profile = new Profile(user.getFirstName(), null, user.getLastName(), user.getDateOfBirth(),
                null, user.getEmail(), user.getMobileNumber(), socialIdentity, user.isEmailVerified(),
                LocalDateTime.now().toString(), LocalDateTime.now().toString(), user.getStoreId(), user.getShoppingFor(),
                user.getAboutUs());
        List<String> cardIds = new ArrayList<>();
        List<String> addressIds = new ArrayList<>();

        /*//Profile sync with BMCIS called from here.
        CreateCustomerRequest request = new CreateCustomerRequest(user.getUserId(), "web",
                user.getFirstName(), user.getLastName(), user.getEmail());
        CreateCustomerResponse response = integrationServiceManager.validateCreateCustomerSOAPService(request)
                .toCompletableFuture().join().second();
        log.info("BMCIS_customerId : {}", response.getCustomerId());*/

        String roleId = null;
        if (StringUtils.isBlank(user.getBmRoleId())) {
            roleId = accountServiceImplCommon.getRoldIds().get(user.getAccountType().toLowerCase());
        } else {
            roleId = user.getBmRoleId();
        }

        return new Account(user.getUserId(), "",
                gen(), profile, roleId, new ArrayList<>(), addressIds, null,
                null, cardIds, null, user.getStoreId(),
                user.getMyZip(), true, user.isSubscribed(), new CreatedAt(LocalDateTime.now()),
                new LastModifiedAt(LocalDateTime.now()), com.aie.arc.account.impl.status.Status.CREATED, null,
                user.getOnlineStoreId());
    }


    /*Update customer account*/
    private Account updateAccount(User user, Account account) {
        log.debug("Updating Account with ID : {} ", user.getUserId());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
        Profile profile = new Profile(user.getFirstName(), account.getProfile().getMiddleName(), user.getLastName(),
                user.getDateOfBirth(), account.getProfile().getGender(), user.getEmail(),
                user.getMobileNumber(), account.getProfile().getSocialIdentity(), account.getProfile().isEmailVerified(),
                account.getProfile().getCreatedAt(), formatter.format(LocalDateTime.now()), user.getStoreId(),
                user.getShoppingFor(), user.getAboutUs());

        String roleId = accountServiceImplCommon.getRoldIds().get(user.getAccountType().toLowerCase());

        return new Account(user.getUserId(), account.getBmcisProfileId(), account.getAccountNumber(), profile,
                roleId, account.getFlashSaleLimitedIds(), account.getAddressIds(),
                account.getDefaultShippingAddressId(), account.getDefaultBillingAddressId(),
                account.getCardIds(), account.getDefaultCardId(), account.getStoreId(),
                user.getMyZip(), true, user.isSubscribed(), account.getCreatedAt(),
                new LastModifiedAt(LocalDateTime.now()), com.aie.arc.account.impl.status.Status.CREATED,
                account.getCustomerGroupIds(), user.getOnlineStoreId());
    }

    private static Integer gen() {
        Random r = new Random(System.currentTimeMillis());
        return ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
    }

    /**
     * This method is use to Consume User that Publish By User Authentication.
     * throws a Kafka consume exception if any error in the consume.
     */
    CompletionStage<Done> subscribeBusinessUser(String userJson) {
        log.info("subscribe_business_user_data");
        try {
            ObjectMapper obj = new ObjectMapper();
            InternalUser user = obj.readValue(userJson, InternalUser.class);
            log.info("business_user for accountId : {} ", user.getBusinessUserId());
            businessUser(user);
        } catch (Exception ex) {
            log.error("error_in_consuming_register_user_kafka_message : {} with error :", userJson, ex);
        }
        return CompletableFuture.completedFuture(Done.getInstance());
    }

    private void businessUser(InternalUser user) {
        boolean accountAlreadyCreated = true;
        BusinessUser businessUser;
        accountAlreadyCreated = accountServiceImplCommon.getEntityRefForBusinessUser(user.getBusinessUserId()).ask(
                BusinessUserCommand.GetBusinessUser::new, ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().isPresent();
        if (!accountAlreadyCreated) {
            businessUser = createBusinessUser(user);
            BusinessUser finalBusinessUser = businessUser;
            accountServiceImplCommon.getEntityRefForBusinessUser(businessUser.getBusinessUserId()).<Done>ask(replyTo -> new BusinessUserCommand.CreateBusinessUser(finalBusinessUser, replyTo), ServiceConstants.ASK_TIMEOUT);
        } else {
            log.info("business_user with id : {} exists not creating business_user", user.getBusinessUserId());
        }

    }

    /*Create Business User account*/
    private BusinessUser createBusinessUser(InternalUser user) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
        BusinessProfile businessProfile = new BusinessProfile(user.getProfile().getFirstName(), user.getProfile().getUserName(), user.getProfile().getMiddleName(),
                user.getProfile().getLastName(), user.getProfile().getEmail(), true, user.getProfile().getPhone(),
                formatter.format(LocalDateTime.now()), formatter.format(LocalDateTime.now()));
        return new BusinessUser(user.getBusinessUserId(), user.getOrganizationId(), businessProfile,
                user.getDesignation(), user.getRoleIds(), true, new CreatedAt(LocalDateTime.now()), new LastModifiedAt(LocalDateTime.now()), com.aie.arc.account.impl.status.Status.CREATED);
    }

    private void validateAndUpdateCustomerId(User user) {
        log.info("Is this a migrated event T/F : {} ", user.isMigrated());
        if (!user.isMigrated()) {
            log.debug("runAsync BMCIS SOAP call thread name: " + Thread.currentThread().getName());
            integrationServiceManager.validateCreateCustomerSOAPService(new CreateCustomerRequest(user.getUserId(),
                    "web", user.getFirstName(), user.getLastName(), user.getEmail()))
                    .thenAccept(pair -> {
                        CreateCustomerResponse response = pair.second();
                        log.debug("customerId in response : {} ", response.getCustomerId());
                        if (response.getCustomerId() != 0) {
                            accountServiceValidation.validateAndGetAccountById(user.getUserId())
                                    .thenAccept(optionalAccount -> {
                                        Account account = optionalAccount.get();
                                        accountServiceImplCommon.getEntityRefForAccount(user.getUserId())
                                                .<Done>ask(replyTo -> new AccountCommand.UpdateAccount(
                                                        new Account(
                                                                account.getAccountId(), Integer.toString(response.getCustomerId()), account.getAccountNumber(),
                                                                account.getProfile(), account.getBmRoleId(), account.getFlashSaleLimitedIds(),
                                                                account.getAddressIds(), account.getDefaultShippingAddressId(),
                                                                account.getDefaultBillingAddressId(), account.getCardIds(),
                                                                account.getDefaultCardId(), account.getStoreId(), account.getMyZip(),
                                                                account.isActive(), account.isSubscribed(), account.getCreatedAt(),
                                                                account.getLastModifiedAt(), account.getStatus(), account.getCustomerGroupIds(),
                                                                account.getOnlineStoreId()
                                                        ), replyTo), ServiceConstants.ASK_TIMEOUT);
                                    });
                        }
                    });
        }
    }
}
