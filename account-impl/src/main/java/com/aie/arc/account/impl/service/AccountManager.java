package com.aie.arc.account.impl.service;

import akka.Done;
import akka.japi.Pair;
import com.aie.arc.account.api.common.exceptions.ParameterRequired;
import com.aie.arc.account.api.common.response.BaseResponse;
import com.aie.arc.account.api.publishedevent.AccountEvents;
import com.aie.arc.account.api.publishedevent.migration.MigrationAccountEvent;
import com.aie.arc.account.api.request.AddZipCodeRequest;
import com.aie.arc.account.api.request.CreateCustomerRequest;
import com.aie.arc.account.api.request.SubscribeRequest;
import com.aie.arc.account.api.request.TestAccountRequest;
import com.aie.arc.account.api.response.AccountDetail;
import com.aie.arc.account.api.response.CreateCustomerResponse;
import com.aie.arc.account.impl.command.AccountCommand;
import com.aie.arc.account.impl.entity.entities.Account;
import com.aie.arc.account.impl.entity.entities.vo.User;
import com.aie.arc.account.impl.event.AccountEvent;
import com.aie.arc.account.impl.mapper.Mapper;
import com.aie.arc.account.impl.repository.AccountRepository;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.aie.arc.externalservice.api.response.IpInfoServiceResponse;
import com.google.gson.Gson;
import com.lightbend.lagom.javadsl.api.broker.Topic;
import com.lightbend.lagom.javadsl.api.transport.RequestHeader;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import com.lightbend.lagom.javadsl.broker.TopicProducer;
import com.lightbend.lagom.javadsl.client.CircuitBreakersPanel;
import com.lightbend.lagom.javadsl.persistence.Offset;
import com.typesafe.config.Config;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CompletionStage;

import static com.aie.arc.account.api.constants.AccountURIConstants.*;
import static com.aie.arc.account.api.constants.TopicIdConstants.ACCOUNT_SYNC_TOPIC_ID;
import static com.aie.arc.account.api.constants.TopicIdConstants.ACCOUNT_TOPIC_ID;
import static com.aie.arc.account.impl.util.MessageConstants.*;

@Singleton
@Slf4j
public class AccountManager {

    @Inject
    private AccountServiceImplCommon accountServiceCommon;
    @Inject
    private AccountServiceValidation accountServiceValidation;
    @Inject
    private AccountRepository accountRepository;
    @Inject
    private IntegrationServiceManager serviceManager;
    @Inject
    private AccountMigrationEventPublisher accountMigrationEventPublisher;
    @Inject
    private AccountEventPublisher accountEventPublisher;
    @Inject
    private AccountEventSubscriber accountEventSubscriber;
    @Inject
    private CircuitBreakersPanel circuitBreakerPanel;
    @Inject
    private Config config;


    //This method is for testing
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> testCreateAccount(TestAccountRequest request) {
        log.debug("createAccount thread name: ============ " + Thread.currentThread().getName());
        String accountId = accountServiceCommon.getNewId();
        User account = new User(accountId, request.getFirstName(), request.getLastName(),
                request.getEmail(), LocalDateTime.now().toString(), null, request.getAccountType(),
                true, true, true, LocalDateTime.now().toString(), null,
                null, null, null, new ArrayList<>(),
                null,request.getBmRoleId(), false);
        accountEventSubscriber.registerAccount(account);
        return (CompletionStage<Pair<ResponseHeader, BaseResponse>>) Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS),
                accountServiceValidation.sendResponseByStatusIdAndRequest(accountId, SUCCESS_STATUS, POST_TEST_ACC_URI,
                        "Account Created!", request));
    }


    public CompletionStage<Pair<ResponseHeader, AccountDetail>> getAccount(String accountId) {
        log.debug("Get account with accountId: {}", accountId);
        return accountServiceValidation.validateAndGetAccountById(accountId)
        .thenApply(optionalAccount ->
                        Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS),
                                Mapper.toAccountDetails(accountServiceValidation, optionalAccount.get())));
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> subscribe(SubscribeRequest request, String accountId) {
        log.debug("subscribe account with accountId: {}", accountId);
        accountServiceValidation.validateJsonRequest(request);
        if (request.getValue().booleanValue()) {
            return accountServiceValidation.validateAndSubscribe(accountId, request);
        }
        return accountServiceValidation.validateAndUnsubscribe(accountId, request);
    }


    public CompletionStage<Pair<ResponseHeader, String>> getProfileZipCode(RequestHeader header, String accountId) {
        log.debug("Get profile zip code for accountId : {}", accountId);
        List<String> headerParamKeysList = config.getStringList("ipInfoHeaderParamKeys");
        int listSize = headerParamKeysList.size();
        String ipAddress;
        ipAddress = getIpAddress(header, headerParamKeysList, listSize);
        return accountServiceValidation.validateAndGetAccountById(accountId)
                .thenApply(accountOptional -> {
                    Account account = accountOptional.get();
                    log.debug("myZip at account level : {} ", account.getMyZip());
                    if (account.getMyZip() == null && StringUtils.isNotBlank(ipAddress)) {
                        CompletionStage<IpInfoServiceResponse> completionStage;
                        completionStage = serviceManager.validateIPAddress(ipAddress);
                        IpInfoServiceResponse res = completionStage.toCompletableFuture().handle((ipInfoServiceResponse, throwable) -> {
                            if (Objects.nonNull(throwable)) {
                                Gson gson = new Gson();
                                ipInfoServiceResponse = gson.fromJson(throwable.getCause().getMessage(), IpInfoServiceResponse.class);
                                log.debug("throwable. ipInfoServiceResponse : {}", ipInfoServiceResponse);
                                return ipInfoServiceResponse;
                            }
                            log.debug("ipInfoServiceResponse : {}", ipInfoServiceResponse);
                            return ipInfoServiceResponse;
                        }).join();
                        if (res.getIp().equals("")) {
                            return Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), "{}");
                        }
                        if (res.isBogon()) {
                            throw new ParameterRequired(new String[]{"Error 344: IP Address passed is a reserved address!" +
                                    "Geolocation cannot be found!"});
                        }
                        if (res.getError() != null) {
                            throw new ParameterRequired(new String[]{"Error 345: Please provide a valid IP address!"});
                        }
                        if (StringUtils.isNotBlank(res.getPostal())) {
                        return Pair.create(ResponseHeader.OK, res.getPostal());
                        }
                    }
                    return Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), account.getMyZip());
                });
    }

    private String getIpAddress(RequestHeader header, List<String> headerParamKeysList, int listSize) {
        String ipAddress = null;
        for (int i = 0; i < listSize; i++) {
            ipAddress = header.getHeader(headerParamKeysList.get(i)).orElse(null);
            if (StringUtils.isNotBlank(ipAddress)
                    && !UNKNOWN_IP.equalsIgnoreCase(ipAddress)
                    && ipAddress.trim().length() >= 7) {
                log.debug("Ip address: {}", ipAddress);
                if (ipAddress.contains(":")) {
                    ipAddress = ipAddress.substring(0, ipAddress.indexOf(':'));
                }
                break;
            }
        }
        return ipAddress;
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> addZipCodeToAccount(String accountId,
                                                                                   AddZipCodeRequest request) {
        accountServiceValidation.validateJsonRequest(request);
        log.debug("Add zipCode to accountId : {}, zipCode : {}", accountId, request.getMyZip());
        return accountServiceValidation.validateAndGetAccountById(accountId)
                .thenCompose(optionalAccount ->
                        accountServiceCommon.getEntityRefForAccount(accountId)
                                .<Done>ask(replyTo -> new AccountCommand.AddZipCode(accountId, request.getMyZip(),
                                        replyTo), ServiceConstants.ASK_TIMEOUT)
                                .thenApply(done -> accountServiceValidation.sendResponseByStatusIdAndRequest(UUID.randomUUID().toString(),
                                        SUCCESS_STATUS, SET_ZIP_CODE_URI, ZIP_CODE_ADDED_MESSAGE, request)));


    }

    public void runBMCISJob() {
        log.debug("Failed_BMCIS_accountIds : {}", accountRepository.getFailedAccountIds());
        accountRepository.getFailedAccountIds()
                .thenAccept(listOfIds ->
                    listOfIds.stream().map(accountServiceValidation::validateAndGetAccountById)
                            .forEach(accountFuture ->
                                    accountFuture.thenAccept(optionalAccount -> {
                                        Account account = optionalAccount.get();
                                        log.debug(" Invoking BMCIS for account id  : {}", account.getAccountId());
                                        CreateCustomerRequest request = new CreateCustomerRequest(
                                                account.getAccountId(), "web", account.getProfile().getFirstName(),
                                                account.getProfile().getLastName(), account.getProfile().getEmail());
                                        serviceManager.validateCreateCustomerSOAPService(request)
                                                .thenAccept(pair -> {
                                                    CreateCustomerResponse response = pair.second();
                                                    log.debug("customerId in response : {} ", response.getCustomerId());
                                                    if (response.getCustomerId() != 0) {
                                                        accountRepository.updateFailedAccountRecords(account.getAccountId());
                                                        accountServiceCommon.getEntityRefForAccount(account.getAccountId())
                                                                .<Done>ask(replyTo -> new AccountCommand.UpdateAccount(
                                                                        new Account(
                                                                                account.getAccountId(), Integer.toString(response.getCustomerId()), account.getAccountNumber(),
                                                                                account.getProfile(), account.getBmRoleId(), account.getFlashSaleLimitedIds(),
                                                                                account.getAddressIds(), account.getDefaultShippingAddressId(),
                                                                                account.getDefaultBillingAddressId(), account.getCardIds(),
                                                                                account.getDefaultCardId(), account.getStoreId(), account.getMyZip(),
                                                                                account.isActive(), account.isSubscribed(), account.getCreatedAt(),
                                                                                account.getLastModifiedAt(), account.getStatus(), account.getCustomerGroupIds(),
                                                                                account.getOnlineStoreId()
                                                                        ), replyTo), ServiceConstants.ASK_TIMEOUT);
                                                    }
                                                });
                                    })));
    }

    /*Publish Account Migration event*/
    public Topic<MigrationAccountEvent> accountMigratedPublishedEvent() {
        return TopicProducer.taggedStreamWithOffset(AccountEvent.TAG.allTags(),
                (tag, offset) -> accountServiceCommon.registry.eventStream(tag, offset)
                        .filter(p -> p.first().isPublic())
                        .mapConcat(this::filterAccountEventForMigration)
                        .mapAsync(1, eventAndOffset ->
                                accountMigrationEventPublisher.convertPublishedEventForAccount(eventAndOffset.first())
                                        .thenApply(event -> {
                                                    log.info("AccountMigrationTopicName : {},  id : {},  OFFSET : {}"
                                                            , ACCOUNT_SYNC_TOPIC_ID, event.getAccountId(), eventAndOffset.second());
                                                    log.trace("Published AccountMigration Event : {}", event);
                                                    return Pair.create(event, eventAndOffset.second());
                                                }
                                        )));
    }

    /*Account event publish*/
    public Topic<AccountEvents> accountPublishedEvent() {
        return TopicProducer.taggedStreamWithOffset(AccountEvent.TAG.allTags(),
                (tag, offset) -> accountServiceCommon.registry.eventStream(tag, offset)
                        .filter(p -> p.first().isPublic())
                        .mapConcat(this::filterAccountEvent)
                        .mapAsync(1, eventAndOffset ->
                                accountEventPublisher.convertPublishedEventForAccount(eventAndOffset.first())
                                        .thenApply(event -> {
                                                    log.info("AccountTopicName : {},  id : {},  OFFSET : {}"
                                                            , ACCOUNT_TOPIC_ID, event.getAccountId(), eventAndOffset.second());
                                                    log.trace("Published Account Event : {}", event);
                                                    return Pair.create(event, eventAndOffset.second());
                                                }
                                        )));
    }


    private List<Pair<AccountEvent, Offset>> filterAccountEventForMigration(Pair<AccountEvent, Offset> pair) {
        if ((pair.first() instanceof AccountEvent.AddressAddedToAccount
                && ((AccountEvent.AddressAddedToAccount) pair.first()).isMigratedEvent())
                || (pair.first() instanceof AccountEvent.AddressUpdatedToAccount
                && ((AccountEvent.AddressUpdatedToAccount) pair.first()).isMigratedEvent())
                || (pair.first() instanceof AccountEvent.AddressDeactivatedToAccount
                && ((AccountEvent.AddressDeactivatedToAccount) pair.first()).isMigratedEvent())) {
            log.debug(STR_LOG_MIGRATION_ACCOUNT_MSG, pair.first().getClass(),
                    pair.first().getAccountId());
            return Collections.emptyList();
        }
        if ((pair.first() instanceof AccountEvent.CardAddedToAccount
                && ((AccountEvent.CardAddedToAccount) pair.first()).isMigratedEvent())
                || (pair.first() instanceof AccountEvent.CardUpdatedToAccount
                && ((AccountEvent.CardUpdatedToAccount) pair.first()).isMigratedEvent())
                || (pair.first() instanceof AccountEvent.AccountCardDeactivated
                && ((AccountEvent.AccountCardDeactivated) pair.first()).isMigratedEvent())) {
            log.debug(STR_LOG_MIGRATION_ACCOUNT_MSG, pair.first().getClass(),
                    pair.first().getAccountId());
            return Collections.emptyList();
        }
        if (pair.first() instanceof AccountEvent.AccountCreated
                || pair.first() instanceof AccountEvent.AccountUpdated
                || pair.first() instanceof AccountEvent.ZipCodeAdded
                || pair.first() instanceof AccountEvent.Subscribed
                || pair.first() instanceof AccountEvent.Unsubscribed) {
            log.debug(STR_LOG_MIGRATION_ACCOUNT_MSG, pair.first().getClass(),
                    pair.first().getAccountId());
            return Collections.emptyList();
        }
        return Arrays.asList(pair);
    }

    private List<Pair<AccountEvent, Offset>> filterAccountEvent(Pair<AccountEvent, Offset> pair) {
        if (pair.first() instanceof AccountEvent.Subscribed
                || pair.first() instanceof AccountEvent.Unsubscribed) {
            log.debug("Filter unwanted Account event : {}  with accountId : {}", pair.first().getClass(),
                    pair.first().getAccountId());
            return Collections.emptyList();
        }
        return Arrays.asList(pair);
    }



}
