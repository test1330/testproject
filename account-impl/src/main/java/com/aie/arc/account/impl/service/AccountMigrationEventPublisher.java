package com.aie.arc.account.impl.service;

import com.aie.arc.account.api.publishedevent.migration.MigrationAccountEvent;
import com.aie.arc.account.api.publishedevent.migration.vo.Profile;
import com.aie.arc.account.api.publishedevent.migration.vo.ProfileAddress;
import com.aie.arc.account.api.publishedevent.migration.vo.ProfileCard;
import com.aie.arc.account.impl.command.AddressCommand;
import com.aie.arc.account.impl.command.CardCommand;
import com.aie.arc.account.impl.entity.entities.Address;
import com.aie.arc.account.impl.entity.entities.Card;
import com.aie.arc.account.impl.event.AccountEvent;
import com.aie.arc.account.impl.util.MessageConstants;
import com.aie.arc.account.impl.util.ServiceConstants;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static com.aie.arc.account.impl.util.MessageConstants.CONVERTING_NON_PUB_EVENT;

@Singleton
@Slf4j
public class AccountMigrationEventPublisher {

    private final AccountServiceImplCommon accountServiceCommon;

    @Inject
    public AccountMigrationEventPublisher(AccountServiceImplCommon accountServiceCommon) {
        this.accountServiceCommon = accountServiceCommon;
    }

    private CompletionStage<Optional<Address>> getAddress(String addressId) {
        return accountServiceCommon.getEntityRefForAddress(addressId)
                .ask(AddressCommand.GetAddress::new, ServiceConstants.ASK_TIMEOUT);
    }

    private CompletionStage<Optional<Card>> getCard(String cardId) {
        return accountServiceCommon.getEntityRefForCard(cardId)
                .ask(CardCommand.GetCard::new,ServiceConstants.ASK_TIMEOUT);
    }

    /*Account Sync ARC_ATG events*/
    public CompletionStage<MigrationAccountEvent> convertPublishedEventForAccount(AccountEvent event) {
        log.debug("Method convertPublishedEventForAccount started AccountEvent :{}",event);
        if (event instanceof AccountEvent.AddressAddedToAccount) {
            log.debug("Published Migration AddressAddedToAccount event");
            return getAddress(((AccountEvent.AddressAddedToAccount) event).getAddressId())
                    .thenApply(address -> new MigrationAccountEvent.AccountAddressCreated(
                            mapToMigrationAddressEvent(address.get(),
                                    ((AccountEvent.AddressAddedToAccount) event).isDefaultBilling(),
                                    ((AccountEvent.AddressAddedToAccount) event).isDefaultShipping(),
                                    "create")));
        } else if (event instanceof AccountEvent.AddressUpdatedToAccount) {
            log.debug("Published Migration AddressUpdatedToAccount event");
            return getAddress(((AccountEvent.AddressUpdatedToAccount) event).getAddressId())
                    .thenApply(address -> new MigrationAccountEvent.AccountAddressUpdated(
                            mapToMigrationAddressEvent(address.get(),
                                    ((AccountEvent.AddressUpdatedToAccount) event).isDefaultBilling(),
                                    ((AccountEvent.AddressUpdatedToAccount) event).isDefaultShipping(),
                                    MessageConstants.CONVERT_EVENT_ACCOUNT_UPDATE)));
        } else if (event instanceof AccountEvent.DefaultBillingAddressUpdatedToAccount) {
            log.debug("Published Migration DefaultBillingAddressUpdatedToAccount event");
            String addressId = ((AccountEvent.DefaultBillingAddressUpdatedToAccount) event).getBillingAddressId();
            return getAddress(addressId)
                    .thenApply(item -> new MigrationAccountEvent.DefaultBillingAddressUpdatedToAccount(
                            mapToMigrationDefaultBillingAddressEvent(item.get(),
                                    ((AccountEvent.DefaultBillingAddressUpdatedToAccount) event).getShippingAddressId(),
                                    MessageConstants.CONVERT_EVENT_ACCOUNT_UPDATE)));

        } else if (event instanceof AccountEvent.DefaultShippingAddressUpdatedToAccount) {
            log.debug("Published Migration AddressUpdatedToAccount event");
            String addressId = ((AccountEvent.DefaultShippingAddressUpdatedToAccount) event).getShippingAddressId();
            return getAddress(addressId)
                    .thenApply(item -> new MigrationAccountEvent.DefaultShippingAddressUpdatedToAccount(
                            mapToMigrationDefaultShippingAddressEvent(item.get(),
                                    ((AccountEvent.DefaultShippingAddressUpdatedToAccount) event).getBillingAddressId(),
                                    MessageConstants.CONVERT_EVENT_ACCOUNT_UPDATE)));

        } else if (event instanceof AccountEvent.AddressDeactivatedToAccount) {
            log.debug("Published Migration AddressDeactivated event");
            return getAddress(((AccountEvent.AddressDeactivatedToAccount) event).getAddressId())
                    .thenApply(item -> new MigrationAccountEvent.AccountAddressDeleted(
                            mapToMigrationAddressDeleteEvent(item.get(), "delete")));

        } else if (event instanceof AccountEvent.CardAddedToAccount) {
            log.debug("Published Migration CardAddedToAccount event");
            return getCard(((AccountEvent.CardAddedToAccount) event).getCardId())
                    .thenApply(item -> new MigrationAccountEvent.AccountCardCreated(
                            mapToMigrationCardEvent(item.get(), "create")));

        } else if (event instanceof AccountEvent.CardUpdatedToAccount) {
            log.debug("Published Migration CardUpdatedToAccount event");
            return getCard(((AccountEvent.CardUpdatedToAccount) event).getCardId())
                    .thenApply(item -> new MigrationAccountEvent.AccountCardUpdated(
                            mapToMigrationCardEvent(item.get(), MessageConstants.CONVERT_EVENT_ACCOUNT_UPDATE)));

        } else if (event instanceof AccountEvent.DefaultPaymentSet) {
            log.debug("Published Migration DefaultPaymentSeated event");
            return getCard(((AccountEvent.DefaultPaymentSet) event).getCardId())
                    .thenApply(item -> new MigrationAccountEvent.DefaultPaymentSeated(
                            mapToMigrationDefaultCardEvent(item.get(), MessageConstants.CONVERT_EVENT_ACCOUNT_UPDATE)));

        } else if (event instanceof AccountEvent.AccountCardDeactivated) {
            log.debug("Published Migration CardDeactivated event");
            return getCard(((AccountEvent.AccountCardDeactivated) event).getCardId())
                    .thenApply(item -> new MigrationAccountEvent.AccountCardDeleted(
                            mapToMigrationCardDeleteEvent(item.get(), "delete")));

        }
        log.error("Try to convert non public Migration Account Event: {}", event);
        throw new IllegalArgumentException(CONVERTING_NON_PUB_EVENT);
    }

    Profile mapToMigrationCardEvent(Card card, String action) {
        Profile profile = new Profile();
        profile.setUserId(card.getAccountId());
        profile.setCards(Arrays.asList(mapToCardEvent(card, action)));
        if (card.isDefaultCreditCard()) {
            profile.setDefaultCreditCard(card.getCardId());
        }
        log.debug("Published Map To Migration Card event for accountID : {}, cardID : {}, action : {}",
                card.getAccountId(), card.getCardId(), action);
        return profile;
    }

    private ProfileCard mapToCardEvent(Card card, String action) {
        ProfileCard profileCard = new ProfileCard();
        profileCard.setPaymentId(card.getCardId());
        profileCard.setFirstName(card.getFirstName());
        profileCard.setLastName(card.getLastName());
        profileCard.setAction(action);
        profileCard.setCreditCardNumber(card.getCardNumber());
        profileCard.setSubscriptionId(card.getSubscriptionId());
        profileCard.setCreditCardType(card.getCardType());
        profileCard.setExpirationMonth(card.getExpirationMonth());
        profileCard.setExpirationYear(card.getExpirationYear());
        profileCard.setCardNickName(card.getCardNickName());

        return profileCard;
    }

    Profile mapToMigrationDefaultCardEvent(Card card, String action) {
        Profile profile = new Profile();
        profile.setUserId(card.getAccountId());
        profile.setDefaultCreditCard(card.getCardId());
        profile.setCards(Arrays.asList(mapCardToProfileCard(card, action)));
        log.debug("Published Migration Default Card event for accountID : {}, cardID : {}, action : {}",
                card.getAccountId(), card.getCardId(), action);
        return profile;
    }

    Profile mapToMigrationCardDeleteEvent(Card card, String action) {
        Profile profile = new Profile();
        profile.setUserId(card.getAccountId());
        profile.setCards(Arrays.asList(mapCardToProfileCard(card, action)));
        log.debug("Published Migration Card Delete event for accountID : {}, cardID : {}, action : {}",
                card.getAccountId(), card.getCardId(), action);
        return profile;
    }

    private ProfileCard mapCardToProfileCard(Card card, String action) {
        ProfileCard profileCard = new ProfileCard();
        profileCard.setPaymentId(card.getCardId());
        profileCard.setAction(action);
        return profileCard;
    }

    Profile mapToMigrationAddressEvent(Address address, boolean isBillingAddress, boolean isShippingAddress,
                                       String action) {
        Profile profile = new Profile();
        profile.setUserId(address.getAccountId());
        if (isBillingAddress) {
            profile.setDefaultBillingAddress(address.getAddressId());
        }
        if (isShippingAddress) {
            profile.setDefaultShippingAddress(address.getAddressId());
        }
        profile.setAddresses(Arrays.asList(mapToAddressEvent(address, action)));
        log.debug("Published Migration Address event for accountID : {}, addressID : {}, action : {}",
                address.getAccountId(), address.getAddressId(), action);
        return profile;
    }

    private ProfileAddress mapToAddressEvent(Address address, String action) {
        ProfileAddress profileAddress = new ProfileAddress();
        profileAddress.setAddressId(address.getAddressId());
        profileAddress.setAction(action);
        profileAddress.setFirstName(address.getFirstName());
        profileAddress.setLastName(address.getLastName());
        profileAddress.setAddress1(address.getAddress1());
        profileAddress.setAddress2(address.getAddress2());
        profileAddress.setCity(address.getCity());
        profileAddress.setState(address.getState());
        profileAddress.setCountry(address.getCountry());
        profileAddress.setPostalCode(address.getPostalCode());
        profileAddress.setPhoneNumber(address.getPhone());
        profileAddress.setFipsCounty(address.getFipscounty());
        profileAddress.setFipsState(address.getFipsstate());
        profileAddress.setPhoneNumberAlt(address.getPhonenumberAlt());
        profileAddress.setAvsValid(address.isAvsvalid());
        profileAddress.setSkipAVSValidation(address.isSkipavsvalidation());

        return profileAddress;
    }

    Profile mapToMigrationDefaultBillingAddressEvent(Address address, String shippingAddressId, String action) {
        Profile profile = new Profile();
        profile.setUserId(address.getAccountId());
        profile.setDefaultShippingAddress(shippingAddressId);
        profile.setDefaultBillingAddress(address.getAddressId());
        profile.setAddresses(Arrays.asList(mapAddressToProfileAddress(address, action)));
        log.debug("Published Migration Default Billing Address event for accountID : {}, cardID : {}, action : {}",
                address.getAccountId(), address.getAddressId(), action);
        return profile;
    }

    Profile mapToMigrationAddressDeleteEvent(Address address, String action) {
        Profile profile = new Profile();
        profile.setUserId(address.getAccountId());
        profile.setAddresses(Arrays.asList(mapAddressToProfileAddress(address, action)));
        log.debug("Published Migration Address Delete event for accountID : {}, cardID : {}, action : {}",
                address.getAccountId(), address.getAddressId(), action);
        return profile;
    }

    Profile mapToMigrationDefaultShippingAddressEvent(Address address, String billingAddressId, String action) {
        Profile profile = new Profile();
        profile.setUserId(address.getAccountId());
        profile.setDefaultShippingAddress(address.getAddressId());
        profile.setDefaultBillingAddress(billingAddressId);
        profile.setAddresses(Arrays.asList(mapAddressToProfileAddress(address, action)));
        log.debug("Published Migration Default Shipping Address event for accountID : {}, cardID : {}, action : {}",
                address.getAccountId(), address.getAddressId(), action);
        return profile;
    }

    private ProfileAddress mapAddressToProfileAddress(Address address, String action) {
        ProfileAddress profileAddress = new ProfileAddress();
        profileAddress.setAddressId(address.getAddressId());
        profileAddress.setAction(action);
        return profileAddress;
    }

}
