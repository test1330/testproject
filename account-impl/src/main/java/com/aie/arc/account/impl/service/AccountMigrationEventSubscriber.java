package com.aie.arc.account.impl.service;

import akka.Done;
import com.aie.arc.account.api.common.request.ActivationRequest;
import com.aie.arc.account.api.publishedevent.migration.vo.Profile;
import com.aie.arc.account.api.publishedevent.migration.vo.ProfileAddress;
import com.aie.arc.account.api.publishedevent.migration.vo.ProfileCard;
import com.aie.arc.account.api.request.AccountAddressRequest;
import com.aie.arc.account.api.request.UpdateAccountAddressRequest;
import com.aie.arc.account.api.request.CardRequest;
import com.aie.arc.account.api.request.UpdateCardRequest;
import com.aie.arc.account.impl.command.AccountCommand;
import com.aie.arc.account.impl.entity.entities.Account;
import com.aie.arc.account.impl.entity.entities.vo.GuestUserAddress;
import com.aie.arc.account.impl.entity.entities.vo.User;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lightbend.lagom.javadsl.api.broker.Message;
import com.lightbend.lagom.javadsl.broker.kafka.KafkaMetadataKeys;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import play.shaded.ahc.io.netty.util.internal.StringUtil;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Slf4j
@Singleton
public class AccountMigrationEventSubscriber {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final AddressManager addressManager;
    private final AccountServiceImplCommon accountServiceImplCommon;
    private final AccountEventSubscriber accountEventSubscriber;
    private final CardManager cardManager;
    private static final String ACTION_CREATE = "create";
    private static final String ACTION_UPDATE = "update";
    private static final String ACTION_DELETE = "delete";

    @Inject
    public AccountMigrationEventSubscriber(AddressManager addressManager, CardManager cardManager,
                                           AccountServiceImplCommon accountServiceImplCommon, AccountEventSubscriber
                                                   accountEventSubscriber) {
        this.addressManager = addressManager;
        this.cardManager = cardManager;
        this.accountServiceImplCommon = accountServiceImplCommon;
        this.accountEventSubscriber = accountEventSubscriber;
    }

    public CompletionStage<Done> subscribeProfile(Message<String> profileKafkaMessage) {
        log.info("subscribe_address_and_card_profile kafka_message_key : {} , TOPIC : {}, PARTITION : {} , OFFSET : {} " +
                        "and TIMESTAMP : {}", profileKafkaMessage.messageKeyAsString(),
                profileKafkaMessage.get(KafkaMetadataKeys.TOPIC), profileKafkaMessage.get(KafkaMetadataKeys.PARTITION),
                profileKafkaMessage.get(KafkaMetadataKeys.OFFSET), profileKafkaMessage.get(KafkaMetadataKeys.TIMESTAMP));
        log.trace("address_and_card_kafka_message : {}", profileKafkaMessage.getPayload());
        try {
            Profile profile = objectMapper.readValue(profileKafkaMessage.getPayload(), Profile.class);
            log.info("address_and_card_migration  for userId : {} ", profile != null ? profile.getUserId() : null);
            //Below check is required when there is a delay in sync between Auth And Account module data.
            isProfileExist(profile, profileKafkaMessage);
            migrateProfile(profile);
            log.info("address_and_card_profile_data_migrated_successfully  for userId : {} ",
                    profile != null ? profile.getUserId() : null);
        } catch (Exception e) {
            log.error("error_in_consuming_migration_profile_kafka_message : {} with error :", profileKafkaMessage, e);
        }
        return CompletableFuture.completedFuture(Done.getInstance());
    }

    private void migrateProfile(Profile profile) {
        if (profile == null
                || ((profile.getAddresses() == null || profile.getAddresses().isEmpty())
                && (profile.getCards() == null || profile.getCards().isEmpty()))) {
            log.warn("No profile data to migrate");
            return;
        }

        if (profile.getAddresses() != null && !profile.getAddresses().isEmpty()) {
            log.debug("Migrated the addresses");
            profile.getAddresses().stream()
                    .filter(Objects::nonNull)
                    .forEach(address -> migrateAddress(address, profile));
        }

        if (profile.getCards() != null && !profile.getCards().isEmpty()) {
            log.debug("Migrated the cards");
            profile.getCards().stream()
                    .filter(Objects::nonNull)
                    .forEach(card -> migrateCard(card, profile));
        }

    }

    private void isProfileExist(Profile profile, Message<String> userJson) {
        boolean profileAlreadyExist = accountServiceImplCommon.getEntityRefForAccount(profile.getUserId())
                .ask(AccountCommand.GetAccount::new, ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().isPresent();
        if (!profileAlreadyExist) {
            try {
                accountEventSubscriber.subscribeRegisterUser(userJson);
            } catch (Exception e) {
                log.error("error_in_consuming_migration_new_profile_kafka_message : {} with error :", userJson, e);
            }
        }
    }


    private void migrateAddress(ProfileAddress address, Profile profile) {
        if (address.getAction() != null) {
            if (ACTION_CREATE.equalsIgnoreCase(address.getAction())) {
                addressManager.addAccountAddress(mapToAccountAddressRequest(address, profile), profile.getUserId(),
                        address.getAddressId());
            } else if (ACTION_UPDATE.equalsIgnoreCase(address.getAction())) {
                addressManager.updateAccountAddress(mapToUpdateAccountAddressRequest(address, profile), profile.getUserId(),
                        address.getAddressId(), true);
            } else if (ACTION_DELETE.equalsIgnoreCase(address.getAction())) {
                addressManager.activateAddress(new ActivationRequest(Boolean.FALSE), profile.getUserId(),
                        address.getAddressId(), true);
            }
            log.debug("Migrate address : {} of action {} successfully for accountId : {} ", address.getAddress1(),
                    address.getAction(), profile.getUserId());
        }
    }

    private void migrateCard(ProfileCard card, Profile profile) {
        if (card.getAction() != null) {
            if (ACTION_CREATE.equalsIgnoreCase(card.getAction())) {
                cardManager.addCard(mapToCardRequest(card, profile), profile.getUserId(), card.getPaymentId(),
                        card.getCreditCardNumber(), card.getSubscriptionId(), card.getCardNickName());
            } else if (ACTION_UPDATE.equalsIgnoreCase(card.getAction())) {
                cardManager.updateCard(mapToUpdateCardRequest(card, profile), profile.getUserId(),
                        card.getPaymentId(), true);

            } else if (ACTION_DELETE.equalsIgnoreCase(card.getAction())) {
                cardManager.activateCard(new ActivationRequest(Boolean.FALSE), profile.getUserId(),
                        card.getPaymentId(), true);
            }
            log.debug("Migrated Card : {} of Action... {} ...successfully for accountId : {} ", card.getPaymentId(),
                    card.getAction(), profile.getUserId());
        }

    }


    private AccountAddressRequest mapToAccountAddressRequest(ProfileAddress address, Profile profile) {
        boolean isCurrentAddrIsDefaultShipping = false;
        boolean isCurrentAddrIsDefaultBilling = false;

        String firstName = null;
        String lastName = null;
        String address1 = null;
        String address2 = null;
        String city = null;
        String state = null;
        String postalCode = null;

        if (profile.getDefaultShippingAddress() != null &&
                profile.getDefaultShippingAddress().equalsIgnoreCase(address.getAddressId())) {
            isCurrentAddrIsDefaultShipping = true;
        }
        if (profile.getDefaultBillingAddress() != null &&
                profile.getDefaultBillingAddress().equalsIgnoreCase(address.getAddressId())) {
            isCurrentAddrIsDefaultBilling = true;
        }
        if (!StringUtils.isBlank(address.getFirstName()) && address.getFirstName().length() > 50) {
            firstName = address.getFirstName().substring(0, 49);
        } else {
            firstName = address.getFirstName();
        }

        if (!StringUtils.isBlank(address.getLastName()) && address.getLastName().length() > 50) {
            lastName = address.getLastName().substring(0, 49);
        } else {
            lastName = address.getLastName();
        }

        if (!StringUtils.isBlank(address.getAddress1()) && address.getAddress1().length() > 35) {
            address1 = address.getAddress1().substring(0, 34);
        } else {
            address1 = address.getAddress1();
        }

        if (!StringUtils.isBlank(address.getAddress2()) && address.getAddress2().length() > 35) {
            address2 = address.getAddress2().substring(0, 34);
        } else {
            address2 = address.getAddress2();
        }
        if (!StringUtils.isBlank(address.getCity()) && address.getCity().length() > 30) {
            city = address.getCity().substring(0, 29);
        } else {
            city = address.getCity();
        }
        if (!StringUtils.isBlank(address.getState()) && address.getState().length() > 2) {
            state = address.getState().substring(0, 1);
        } else {
            state = address.getState();
        }
        if (!StringUtils.isBlank(address.getPostalCode()) && address.getPostalCode().length() > 5) {
            postalCode = address.getPostalCode().substring(0, 4);
        } else {
            postalCode = address.getPostalCode();
        }
        return new AccountAddressRequest(firstName, lastName, address1, address2, city, state, address.getCountry(), postalCode,
                address.getPhoneNumber(), isCurrentAddrIsDefaultShipping, isCurrentAddrIsDefaultBilling, address.getFipsCounty(),
                address.getFipsState(), address.getPhoneNumberAlt(), address.isAvsValid(), address.isSkipAVSValidation());
    }

    private UpdateAccountAddressRequest mapToUpdateAccountAddressRequest(ProfileAddress address, Profile profile) {
        boolean isCurrentAddrIsDefaultShipping = false;
        boolean isCurrentAddrIsDefaultBilling = false;

        String firstName = null;
        String lastName = null;
        String address1 = null;
        String address2 = null;
        String city = null;
        String state = null;
        String postalCode = null;

        if (profile.getDefaultShippingAddress() != null &&
                profile.getDefaultShippingAddress().equalsIgnoreCase(address.getAddressId())) {
            isCurrentAddrIsDefaultShipping = true;
        }
        if (profile.getDefaultBillingAddress() != null &&
                profile.getDefaultBillingAddress().equalsIgnoreCase(address.getAddressId())) {
            isCurrentAddrIsDefaultBilling = true;
        }
        if (profile.getDefaultShippingAddress() != null &&
                profile.getDefaultShippingAddress().equalsIgnoreCase(address.getAddressId())) {
            isCurrentAddrIsDefaultShipping = true;
        }
        if (profile.getDefaultBillingAddress() != null &&
                profile.getDefaultBillingAddress().equalsIgnoreCase(address.getAddressId())) {
            isCurrentAddrIsDefaultBilling = true;
        }
        if (!StringUtils.isBlank(address.getFirstName()) && address.getFirstName().length() > 50) {
            firstName = address.getFirstName().substring(0, 49);
        } else {
            firstName = address.getFirstName();
        }

        if (!StringUtils.isBlank(address.getLastName()) && address.getLastName().length() > 50) {
            lastName = address.getLastName().substring(0, 49);
        } else {
            lastName = address.getLastName();
        }

        if (!StringUtils.isBlank(address.getAddress1()) && address.getAddress1().length() > 35) {
            address1 = address.getAddress1().substring(0, 34);
        } else {
            address1 = address.getAddress1();
        }

        if (!StringUtils.isBlank(address.getAddress2()) && address.getAddress2().length() > 35) {
            address2 = address.getAddress2().substring(0, 34);
        } else {
            address2 = address.getAddress2();
        }
        if (!StringUtils.isBlank(address.getCity()) && address.getCity().length() > 30) {
            city = address.getCity().substring(0, 29);
        } else {
            city = address.getCity();
        }
        if (!StringUtils.isBlank(address.getState()) && address.getState().length() > 2) {
            state = address.getState().substring(0, 1);
        } else {
            state = address.getState();
        }
        if (!StringUtils.isBlank(address.getPostalCode()) && address.getPostalCode().length() > 5) {
            postalCode = address.getPostalCode().substring(0, 4);
        } else {
            postalCode = address.getPostalCode();
        }

        return new UpdateAccountAddressRequest(firstName, lastName, address1, address2, city, state, address.getCountry(),
                postalCode, address.getPhoneNumber(), isCurrentAddrIsDefaultShipping, isCurrentAddrIsDefaultBilling,
                address.getFipsCounty(), address.getFipsState(), address.getPhoneNumberAlt(), address.isAvsValid(), address.isSkipAVSValidation());
    }


    private CardRequest mapToCardRequest(ProfileCard card, Profile profile) {
        boolean isDefaultCreditCard = false;
        if (profile.getDefaultCreditCard() != null && profile.getDefaultCreditCard().
                equalsIgnoreCase(card.getPaymentId())) {
            isDefaultCreditCard = true;
        }
        // added the dummy card number as it's not consider for migration and used to skip validation
        return new CardRequest(card.getFirstName(), card.getLastName(), isDefaultCreditCard,
                "4111111111111111", card.getCreditCardType(), card.getExpirationMonth(),
                card.getExpirationYear(), true);
    }

    private UpdateCardRequest mapToUpdateCardRequest(ProfileCard card, Profile profile) {
        boolean isDefaultCreditCard = false;
        if (profile.getDefaultCreditCard() != null && profile.getDefaultCreditCard().
                equalsIgnoreCase(card.getPaymentId())) {
            isDefaultCreditCard = true;
        }
        return new UpdateCardRequest(card.getFirstName(), card.getLastName(), card.getExpirationMonth(),
                card.getExpirationYear(), isDefaultCreditCard);
    }
}
