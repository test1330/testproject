package com.aie.arc.account.impl.service;

import akka.NotUsed;
import akka.cluster.sharding.typed.javadsl.ClusterSharding;
import akka.cluster.sharding.typed.javadsl.Entity;
import akka.stream.javadsl.Flow;
import com.aie.arc.account.api.common.request.ActivationRequest;
import com.aie.arc.account.api.common.request.SearchRequest;
import com.aie.arc.account.api.common.response.BaseResponse;
import com.aie.arc.account.api.email.EmailRequest;
import com.aie.arc.account.api.publishedevent.AccountEvents;
import com.aie.arc.account.api.publishedevent.AddressEvents;
import com.aie.arc.account.api.publishedevent.BusinessUserEvents;
import com.aie.arc.account.api.publishedevent.CardEvents;
import com.aie.arc.account.api.publishedevent.migration.MigrationAccountEvent;
import com.aie.arc.account.api.request.*;
import com.aie.arc.account.api.request.brandsmart.FirstDataRequest;
import com.aie.arc.account.api.request.brandsmart.FirstDataResponse;
import com.aie.arc.account.api.response.*;
import com.aie.arc.account.api.service.AccountService;
import com.aie.arc.account.api.service.KafkaService;
import com.aie.arc.account.impl.entity.*;
import com.aie.arc.account.impl.entity.master.subscribe.entity.LocaleEntity;
import com.aie.arc.account.impl.entity.organization.subscribe.entity.OfflineStoreEntity;
import com.aie.arc.account.impl.entity.organization.subscribe.entity.OnlineStoreEntity;
import com.aie.arc.account.impl.service.subscribe.CheckoutEventsSubscriber;
import com.aie.arc.externalservice.api.request.AvalaraRequest;
import com.aie.arc.externalservice.api.response.AvalaraResponse;
import com.aie.arc.externalservice.api.response.IpInfoServiceResponse;
import com.aie.arc.master.api.publishedevent.LocaleEvents;
import com.aie.arc.organization.api.service.OrganizationService;
import com.aie.arc.rbac.api.publishedevent.PermissionEvents;
import com.aie.arc.rbac.api.publishedevent.RoleEvents;
import com.aie.arc.rbac.api.request.*;
import com.aie.arc.rbac.api.response.*;
import com.aie.arc.rbac.impl.entity.*;
import com.aie.arc.rbac.impl.service.EventSubscriber;
import com.aie.arc.rbac.impl.util.ApiTagConstants;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.broker.Message;
import com.lightbend.lagom.javadsl.api.broker.Topic;
import com.lightbend.lagom.javadsl.api.transport.Forbidden;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;
import com.lightbend.lagom.javadsl.server.HeaderServiceCall;
import com.lightbend.lagom.javadsl.server.ServerServiceCall;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Slf4j
public class AccountServiceImpl implements AccountService {

    private final PersistentEntityRegistry registry;
    private final ClusterSharding clusterSharding;
    private final AccountManager accountManager;
    private final AddressManager addressManager;
    private final CardManager cardManager;
    private final IntegrationServiceManager integrationServiceManager;
    private final AccountEventSubscriber subscribeEvents;
    private final CheckoutEventsSubscriber checkoutEventsSubscriber;
    private final AccountEventPublisher accountEventPublisher;
    private final AccountMigrationEventSubscriber accountMigrationEventSubscriber;
    private final AccountMigrationEventPublisher accountMigrationEventPublisher;
    private final BusinessManager businessManager;
    private final CustomerGroupManager customerGroupManager;
    private final RolesManager rolesManager;
    private final PermissionManager permissionManager;
    private final ApiManager apiManager;
    private final StoreManager storeManager;
    private final WorkspaceManager workspaceManager;
    private final AccessManager accessManager;
    private final EventSubscriber orgEventSubscriber;

    @Inject
    AccountServiceImpl(PersistentEntityRegistry registry, ClusterSharding clusterSharding, KafkaService kafkaService, AccountManager accountManager,
                       AddressManager addressManager, CardManager cardManager,
                       IntegrationServiceManager integrationServiceManager, AccountEventPublisher accountEventPublisher,
                       AccountMigrationEventPublisher accountMigrationEventPublisher,
                       AccountEventSubscriber subscribeEvents,
                       AccountMigrationEventSubscriber accountMigrationEventSubscriber, BusinessManager businessManager,
                       CustomerGroupManager customerGroupManager, RolesManager rolesManager,
                       PermissionManager permissionManager, WorkspaceManager workspaceManager,
                       ApiManager apiManager, StoreManager storeManager, AccessManager accessManager, EventSubscriber orgEventSubscriber, OrganizationService organizationService, CheckoutEventsSubscriber checkoutEventsSubscriber) {

        this.registry = registry;
        this.clusterSharding = clusterSharding;
        this.workspaceManager = workspaceManager;
        this.accessManager = accessManager;
        this.orgEventSubscriber = orgEventSubscriber;
        this.checkoutEventsSubscriber = checkoutEventsSubscriber;
        registerAllEntities(registry);
        this.addressManager = addressManager;
        this.cardManager = cardManager;
        this.integrationServiceManager = integrationServiceManager;
        this.subscribeEvents = subscribeEvents;
        this.accountMigrationEventSubscriber = accountMigrationEventSubscriber;
        this.accountManager = accountManager;
        this.accountEventPublisher = accountEventPublisher;
        this.accountMigrationEventPublisher = accountMigrationEventPublisher;
        this.businessManager = businessManager;
        this.customerGroupManager = customerGroupManager;
        this.rolesManager = rolesManager;
        this.permissionManager = permissionManager;
        this.apiManager = apiManager;
        this.storeManager = storeManager;
        subscribeKafkaMessage(kafkaService, organizationService);
    }

    private void registerAllEntities(PersistentEntityRegistry registry) {
        /*Register Account Entities*/

        this.clusterSharding.init(Entity.of(AccountEntity.ENTITY_TYPE_KEY, AccountEntity::create));
        this.clusterSharding.init(Entity.of(AddressEntity.ENTITY_TYPE_KEY, AddressEntity::create));
        // Commented Unused Entities for BM project
        //this.clusterSharding.init(Entity.of(BusinessAddressEntity.ENTITY_TYPE_KEY, BusinessAddressEntity::create));
        this.clusterSharding.init(Entity.of(BusinessUserEntity.ENTITY_TYPE_KEY, BusinessUserEntity::create));
        this.clusterSharding.init(Entity.of(CardEntity.ENTITY_TYPE_KEY, CardEntity::create));
        // Commented Unused Entities for BM project
        //this.clusterSharding.init(Entity.of(CustomerGroupEntity.ENTITY_TYPE_KEY, CustomerGroupEntity::create));
        this.clusterSharding.init(Entity.of(OnlineStoreEntity.ENTITY_TYPE_KEY, OnlineStoreEntity::create));
        this.clusterSharding.init(Entity.of(OfflineStoreEntity.ENTITY_TYPE_KEY, OfflineStoreEntity::create));
        // Commented Unused Entities for BM project
        //this.clusterSharding.init(Entity.of(ApiEntity.ENTITY_TYPE_KEY, ApiEntity::create));
        this.clusterSharding.init(Entity.of(PermissionEntity.ENTITY_TYPE_KEY, PermissionEntity::create));
        this.clusterSharding.init(Entity.of(RoleEntity.ENTITY_TYPE_KEY, RoleEntity::create));
        this.clusterSharding.init(Entity.of(RoleGroupEntity.ENTITY_TYPE_KEY, RoleGroupEntity::create));
        // Commented Unused Entities for BM project
        //this.clusterSharding.init(Entity.of(MicroServiceEntity.ENTITY_TYPE_KEY, MicroServiceEntity::create));
        this.clusterSharding.init(Entity.of(AccessRightEntity.ENTITY_TYPE_KEY, AccessRightEntity::create));
        this.clusterSharding.init(Entity.of(WorkspaceEntity.ENTITY_TYPE_KEY, WorkspaceEntity::create));
        //RBAC subscription
        this.clusterSharding.init(Entity.of(LocaleEntity.ENTITY_TYPE_KEY, LocaleEntity::create));


    }

    /*subscribe Auth, Migration & Organization service events */
    private void subscribeKafkaMessage(KafkaService kafkaService, OrganizationService organizationService) {
        log.info("==================ACCOUNT_SERVICE_READY_TO_SUBSCRIBE_EXTERNAL_KAFKA_MESSAGES=======================");
        kafkaService.usersTopic().subscribe().withMetadata()
                .atLeastOnce(Flow.<Message<String>>create().mapAsync(1, subscribeEvents::subscribeRegisterUser));
        kafkaService.profileSyncForMigrationTopic().subscribe().withMetadata()
                .atLeastOnce(Flow.<Message<String>>create().mapAsync(1, subscribeEvents::subscribeRegisterUser));
        kafkaService.accountMigrationTopic().subscribe().withMetadata()
                .atLeastOnce(Flow.<Message<String>>create().mapAsync(1, accountMigrationEventSubscriber::subscribeProfile));
        kafkaService.businessUsersTopic().subscribe()
                .atLeastOnce(Flow.<String>create().mapAsync(1, subscribeEvents::subscribeBusinessUser));
        organizationService.publishLocaleEvents().subscribe()
                .atLeastOnce(Flow.<LocaleEvents>create().mapAsync(1, orgEventSubscriber::handleLocaleEvents));
        // checkout topics subscribed
        kafkaService.checkoutPaymentTopic().subscribe().withMetadata()
                .atLeastOnce(Flow.<Message<String>>create().mapAsync(1, checkoutEventsSubscriber::consumePaymentEvents));
        kafkaService.checkoutAddressTopic().subscribe().withMetadata()
                .atLeastOnce(Flow.<Message<String>>create().mapAsync(1, checkoutEventsSubscriber::consumeAddressEvents));
    }


    @Override
    public HeaderServiceCall<NotUsed, AccountDetail> getAccount(String accountId) {
        return (header, request) -> accountManager.getAccount(accountId);
    }

    @Override
    public HeaderServiceCall<AccountAddressRequest, SuggestedAddresses> validateAccountAddress(String accountId) {
        return (header, request) -> addressManager.validateAccountAddress(request, accountId);
    }

    @Override
    public HeaderServiceCall<AccountAddressRequest, BaseResponse> addAccountAddress(String accountId) {
        return (header, request) -> addressManager.addAccountAddress(request, accountId, null);
    }

    @Override
    public HeaderServiceCall<UpdateAccountAddressRequest, BaseResponse> updateAccountAddress(String accountId,
                                                                                             String addressId) {
        return (header, request) -> addressManager.updateAccountAddress(request, accountId, addressId,
                false);
    }

    @Override
    public HeaderServiceCall<NotUsed, BaseResponse> setDefaultBillingAddress(String accountId, String addressId) {
        return (header, request) -> addressManager.setDefaultBillingAddress(accountId, addressId);
    }

    @Override
    public HeaderServiceCall<NotUsed, BaseResponse> setDefaultShippingAddress(String accountId, String addressId) {
        return (header, request) -> addressManager.setDefaultShippingAddress(accountId, addressId);
    }

    public HeaderServiceCall<ActivationRequest, BaseResponse> activateAddress(String accountId, String addressId) {
        return (header, request) -> addressManager.activateAddress(request, accountId, addressId, false);
    }

    @Override
    public HeaderServiceCall<NotUsed, List<AccountAddress>> getAddresses(String accountId) {
        return (header, request) -> addressManager.getAddresses(accountId);
    }

    @Override
    public HeaderServiceCall<NotUsed, AddressDetail> getAccountAddress(String accountId, String addressId) {
        return (header, request) -> addressManager.getAccountAddress(accountId, addressId);
    }

    @Override
    public HeaderServiceCall<AddZipCodeRequest, BaseResponse> addZipCodeToAccount(String accountId) {
        return (header, request) -> accountManager.addZipCodeToAccount(accountId, request);
    }


    // For Testing purpose only
    @Override
    public HeaderServiceCall<TestAccountRequest, BaseResponse> testCtAccount(String accountId) {
        return (header, request) -> accountManager.testCreateAccount(request);
    }

    @Override
    public HeaderServiceCall<CardRequest, BaseResponse> addCard(String accountId) {
        return (header, request) -> cardManager.addCard(request, accountId, null,
                null, null, null);
    }

    @Override
    public HeaderServiceCall<UpdateCardRequest, BaseResponse> updateCard(String accountId, String cardId) {
        return (header, request) -> cardManager.updateCard(request, accountId, cardId, false);
    }

    @Override
    public HeaderServiceCall<NotUsed, CardDetail> getCard(String accountId, String cardId) {
        return (header, request) -> cardManager.getCard(accountId, cardId);
    }

    @Override
    public HeaderServiceCall<NotUsed, PaymentDetails> getAccountCards(String accountId) {
        return (header, request) -> cardManager.getAccountCards(accountId);
    }

    @Override
    public HeaderServiceCall<ActivationRequest, BaseResponse> activateCard(String accountId, String cardId) {
        return (header, request) -> cardManager.activateCard(request, accountId, cardId, false);
    }

    @Override
    public HeaderServiceCall<NotUsed, BaseResponse> setDefaultPayment(String accountId, String cardId) {
        return (header, request) -> cardManager.setDefaultPayment(accountId, cardId);
    }

    @Override
    public HeaderServiceCall<NotUsed, CardDetail> getDefaultPayment(String accountId) {
        return (header, request) -> cardManager.getDefaultPayment(accountId);
    }

    @Override
    public HeaderServiceCall<NotUsed, String> getProfileZipCode(String accountId) {
        return (header, request) -> accountManager.getProfileZipCode(header, accountId);
    }

    /*@Override
    public HeaderServiceCall<SubscribeRequest, BaseResponse> subscribe(String accountId) {
        return (header, request) -> accountManager.subscribe(request, accountId);
    }
    */

    @Override
    public HeaderServiceCall<AvalaraRequest, AvalaraResponse> validateAddress() {
        return (header, request) -> integrationServiceManager.validateAddress(request);
    }

    @Override
    public ServiceCall<NotUsed, IpInfoServiceResponse> validateIPAddress(String ipAddress) {
        return request -> integrationServiceManager.validateIPAddress(ipAddress);
    }

    @Override
    public HeaderServiceCall<EmailRequest, BaseResponse> sendTestEmail() {
        return (header, request) -> addressManager.testSendAddressUpdateEmail(request);
    }


    /*....................................Publish Account Event.......................................................*/

    /*Publish Account Migration event*/
    @Override
    public Topic<MigrationAccountEvent> accountMigratedPublishedEvent() {
        return accountManager.accountMigratedPublishedEvent();
    }

    /*Account event publish*/
    @Override
    public Topic<AccountEvents> accountPublishedEvent() {
        return accountManager.accountPublishedEvent();
    }

    /*Card event publish*/
    @Override
    public Topic<CardEvents> cardPublishedEvent() {
        return cardManager.cardPublishedEvent();
    }

    /*Address event publish*/
    @Override
    public Topic<AddressEvents> addressPublishedEvent() {
        return addressManager.addressPublishedEvent();
    }
    /*Business user  event publish*/
    @Override
    public Topic<BusinessUserEvents> businessUserPublishedEvent() {
        return businessManager.businessUserPublishedEvent();
    }

    /*************************************************************RBAC Publishing*************************************/

    /*Role  event publish*/
    @Override
    public Topic<RoleEvents> rolePublishedEvent() {return  rolesManager.rolePublishedEvent(); }

    /*Permission  event publish*/
    @Override
    public Topic<PermissionEvents> permissionPublishedEvent() {
        return  permissionManager.permissionPublishedEvent();
    }

    /******************************************************** RBAC ***************************************************/


    @Override
    public ServerServiceCall<UpdateBusinessUserRequest, BaseResponse> updateBusinessUser(String businessUserId) {
        return authorize(ApiTagConstants.UPDATE_BUSINESS_USER_API, HeaderServiceCall.of(
                (header, request) -> businessManager.updateBusinessUser(request, businessUserId)));
    }

    @Override
    public ServerServiceCall<NotUsed, BusinessUserDetail> getBusinessUser(String businessUserId) {
       return authorize(ApiTagConstants.GET_BUSINESS_USER_API, HeaderServiceCall.of(
                (header, request) -> businessManager.getBusinessUser(businessUserId)));
    }

    @Override
    public ServerServiceCall<NotUsed, GetBusinessUserDetail> getBusinessUserDetails(String businessUserId) {
        return authorize(ApiTagConstants.GET_BUSINESS_USER_API, HeaderServiceCall.of(
                (header, request) -> businessManager.getBusinessUserDetails(businessUserId)));
    }

    @Override
    public ServerServiceCall<SearchRequest, BusinessUserDetails> getBusinessUsers() {
        return authorize(ApiTagConstants.GET_BUSINESS_USER_API, HeaderServiceCall.of(
                (header, request) -> businessManager.getBusinessUsers(request)));
    }

    @Override
    public ServerServiceCall<ActivationRequest, BaseResponse> activateBusinessUser(String id) {
        return authorize(ApiTagConstants.UPDATE_BUSINESS_USER_API, HeaderServiceCall.of(
                (header, request) -> businessManager.activateBusinessUser(request, id)));
    }

    // Commented Unused API's for BM project
    /*@Override
    public HeaderServiceCall<CreateCustomerGroupRequest, BaseResponse> createCustomerGroup() {
        return (header, request) -> customerGroupManager.createCustomerGroup(request);
    }

    @Override
    public HeaderServiceCall<UpdateCustomerGroupRequest, BaseResponse> updateCustomerGroup(String customerGroupId) {
        return (header, request) -> customerGroupManager.updateCustomerGroup(request, customerGroupId);
    }

    @Override
    public HeaderServiceCall<NotUsed, CustomerGroupDetail> getCustomerGroup(String customerGroupId) {
        return (header, request) -> customerGroupManager.getCustomerGroup(customerGroupId);
    }

    @Override
    public HeaderServiceCall<SearchRequest, CustomerGroupDetails> getCustomerGroups() {
        return (header, request) -> customerGroupManager.getCustomerGroups(request);
    }

    @Override
    public HeaderServiceCall<ActivationRequest, BaseResponse> activateCustomerGroup(String id) {
        return (header, request) -> customerGroupManager.activateCustomerGroup(request, id);
    }*/

    @Override
    public HeaderServiceCall<AddStoreRequest, BaseResponse> addStore(String accountId) {
        return (header, request) -> storeManager.addStore(request, accountId);
    }

    @Override
    public HeaderServiceCall<AddStoreRequest, BaseResponse> updateStore(String accountId) {
        return (header, request) -> storeManager.updateStore(request, accountId);
    }

    @Override
    public HeaderServiceCall<RoleGroupRequest, BaseResponse> createRoleGroup() {
        return (header, request) -> rolesManager.createRoleGroup(request);
    }

    @Override
    public HeaderServiceCall<RoleGroupRequest, BaseResponse> updateRoleGroup(String id) {
        return (header, request) -> rolesManager.updateRoleGroup(request, id);
    }

    @Override
    public HeaderServiceCall<NotUsed, RoleGroupDetail> getRoleGroup(String id) {
        return (header, request) -> rolesManager.getRoleGroup(id);
    }

    @Override
    public HeaderServiceCall<SearchRequest, RoleGroupDetails> getRoleGroups() {
        return (header, request) -> rolesManager.getRoleGroups(request);
    }

    @Override
    public HeaderServiceCall<ActivationRequest, BaseResponse> activateRoleGroup(String id) {
        return (header, request) -> rolesManager.activateRoleGroup(request, id);
    }

    @Override
    public ServerServiceCall<RoleRequest, BaseResponse> createRole() {
        return authorize(ApiTagConstants.CREATE_ROLE_API, HeaderServiceCall.of(
                (header, request) ->   rolesManager.createRole(request)));
    }

    @Override
    public ServerServiceCall<RoleRequest, BaseResponse> updateRole(String id) {
        return authorize(ApiTagConstants.UPDATE_ROLE_API, HeaderServiceCall.of(
                (header, request) ->   rolesManager.updateRole(request, id)));
    }

    @Override
    public ServerServiceCall<NotUsed, RoleDetail> getRole(String id) {
        return authorize(ApiTagConstants.GET_ROLE_API, HeaderServiceCall.of(
                (header, request) ->   rolesManager.getRole(id)));
    }

    @Override
    public ServerServiceCall<SearchRequest, RoleDetails> getRoles() {
       return authorize(ApiTagConstants.GET_ROLE_API, HeaderServiceCall.of(
                (header, request) ->   rolesManager.getRoles(request)));
    }

    @Override
    public ServerServiceCall<ActivationRequest, BaseResponse> activateRole(String id) {
        return authorize(ApiTagConstants.UPDATE_ROLE_API, HeaderServiceCall.of(
                (header, request) ->  rolesManager.activateRole(request, id)));
    }

    @Override
    public HeaderServiceCall<AssignRole, BaseResponse> assignRoleToUser(String userId) {
        return (header, request) -> rolesManager.assignRoleToUser(request, userId);
    }


    @Override
    public HeaderServiceCall<AssignBusinessRole, BaseResponse> assignRoleToBusinessUser() {
        return (header, request) -> rolesManager.assignRoleToBusinessUser(request);
    }

    // Commented Unused API's for BM project
    /*@Override
    public HeaderServiceCall<MicroServiceRequest, BaseResponse> createMicroService() {
        return (header, request) -> apiManager.createMicroService(request);
    }

    @Override
    public HeaderServiceCall<MicroServiceRequest, BaseResponse> updateMicroService(String microServiceId) {
        return (header, request) -> apiManager.updateMicroService(request, microServiceId);
    }

    @Override
    public HeaderServiceCall<NotUsed, MicroServiceDetail> getMicroService(String microServiceId) {
        return (header, request) -> apiManager.getMicroService(microServiceId);
    }

    @Override
    public HeaderServiceCall<SearchRequest, MicroServiceDetails> getMicroServices() {
        return (header, request) -> apiManager.getMicroServices(request);
    }

    @Override
    public HeaderServiceCall<ActivationRequest, BaseResponse> activateMicroService(String id) {
        return (header, request) -> apiManager.activateMicroService(request, id);
    }

    @Override
    public HeaderServiceCall<NotUsed, ApiDetail> getApi(String microServiceApiId, String apiId) {
        return (header, request) -> apiManager.getApi(microServiceApiId, apiId);
    }

    @Override
    public HeaderServiceCall<ApiRequest, BaseResponse> addApiToMicroService(String microServiceId) {
        return (header, request) -> apiManager.addApiToMicroService(request, microServiceId);
    }

    @Override
    public HeaderServiceCall<ApiRequest, BaseResponse> updateApiToMicroService(String microServiceId,
                                                                               String microServiceApiId) {
        return (header, request) -> apiManager.updateApiToMicroService(request, microServiceId, microServiceApiId);
    }

    @Override
    public HeaderServiceCall<ActivationRequest, BaseResponse> activateApi(String id) {
        return (header, request) -> apiManager.activateApi(request, id);
    }

    @Override
    public HeaderServiceCall<ActivationRequest, BaseResponse> activateApiFromMicroService(String microServiceId, String apiId) {
        return (header, request) -> apiManager.activateApiFromMicroService(request, microServiceId, apiId);
    }

    @Override
    public HeaderServiceCall<SearchRequest, ApiDetails> getApisInMicroService(String microServiceId) {
        return (header, request) -> apiManager.getApisInMicroService(request, microServiceId);
    }*/

    @Override
    public ServerServiceCall<PermissionRequest, BaseResponse> createPermission() {
       return authorize(ApiTagConstants.CREATE_PERMISSION_API, HeaderServiceCall.of(
                (header, request) ->  permissionManager.createPermission(request)));
    }

    @Override
    public ServerServiceCall<PermissionRequest, BaseResponse> updatePermission(String permissionId) {
       return authorize(ApiTagConstants.UPDATE_PERMISSION_API, HeaderServiceCall.of(
                (header, request) ->  permissionManager.updatePermission(request, permissionId)));
    }

    @Override
    public ServerServiceCall<NotUsed, PermissionDetailWithTag> getPermission(String permissionId) {
        return authorize(ApiTagConstants.GET_PERMISSION_API, HeaderServiceCall.of(
                (header, request) ->  permissionManager.getPermission(permissionId)));
    }

    @Override
    public ServerServiceCall<ActivationRequest, BaseResponse> activatePermission(String id) {
        return authorize(ApiTagConstants.UPDATE_PERMISSION_API, HeaderServiceCall.of(
                (header, request) ->  permissionManager.activatePermission(request, id)));
    }

    @Override
    public ServerServiceCall<SearchRequest, PermissionDetails> getPermissions() {
      return authorize(ApiTagConstants.GET_PERMISSION_API, HeaderServiceCall.of(
                (header, request) ->  permissionManager.getPermissions(request)));
    }

    @Override
    public HeaderServiceCall<CreateCustomerRequest, CreateCustomerResponse> validateCreateCustomerSOAPService() {
        return (header, request) -> integrationServiceManager.validateCreateCustomerSOAPService(request);
    }

    public HeaderServiceCall<CMPWSApiServiceRequest, CMPWSApiServiceResponse> validateSubscriptionSOAPService() {
        return (header, request) -> integrationServiceManager.testInvokeCompassClientService(request);
    }

    //Workspace
    @Override
    public ServerServiceCall<WorkspaceRequest, BaseResponse> createWorkspace() {
        return authorize(ApiTagConstants.CREATE_WORKSAPCE_API, HeaderServiceCall.of(
                (header, request) ->  workspaceManager.createWorkspace(request)));
     }

    @Override
    public ServerServiceCall<UpdateWorkspaceRequest, BaseResponse> updateWorkspace(String workspaceId) {
        return authorize(ApiTagConstants.UPDATE_WORKSAPCE_API, HeaderServiceCall.of(
                (header, request) ->  workspaceManager.updateWorkspace(request, workspaceId)));
    }

    @Override
    public ServerServiceCall<NotUsed, WorkspaceDetail> getWorkspace(String workspaceId) {
        return authorize(ApiTagConstants.GET_WORKSAPCE_API, HeaderServiceCall.of(
                (header, request) ->  workspaceManager.getWorkspace(workspaceId)));
    }

    @Override
    public ServerServiceCall<SearchRequest, WorkspaceDetails> getAllWorkspace() {
        return authorize(ApiTagConstants.GET_WORKSAPCE_API, HeaderServiceCall.of(
                (header, request) ->  workspaceManager.getAllWorkspace(request)));
    }

    @Override
    public ServerServiceCall<ActivationRequest, BaseResponse> deactivateWorkspace(String workspaceId) {
        return authorize(ApiTagConstants.UPDATE_WORKSAPCE_API, HeaderServiceCall.of(
                (header, request) ->  workspaceManager.deactivateWorkspace(request, workspaceId)));
    }

    /*  Access Right  */
    @Override
    public ServerServiceCall<AccessRightRequest, BaseResponse> createAccessRight() {
        return authorize(ApiTagConstants.CREATE_ACCESS_RIGHTS_API, HeaderServiceCall.of(
                (header, request) ->  accessManager.createAccessRight(request)));
    }

    @Override
    public ServerServiceCall<UpdateAccessRightRequest, BaseResponse> updateAccessRight(String accessId) {
        return authorize(ApiTagConstants.UPDATE_ACCESS_RIGHTS_API, HeaderServiceCall.of(
                (header, request) ->  accessManager.updateAccessRight(request, accessId)));
    }

    @Override
    public ServerServiceCall<NotUsed, AccessRightDetail> getAccessRight(String accessId) {
        return authorize(ApiTagConstants.GET_ACCESS_RIGHTS_API, HeaderServiceCall.of(
                (header, request) -> accessManager.getAccessRight(accessId)));
    }

    @Override
    public ServerServiceCall<SearchRequest, AccessRightDetails> getAllAccessRights() {
        return authorize(ApiTagConstants.GET_ACCESS_RIGHTS_API, HeaderServiceCall.of(
                (header, request) -> accessManager.getAllAccessRights(request)));
    }

    @Override
    public ServerServiceCall<ActivationRequest, BaseResponse> deactivateAccessRight(String accessId) {
        return authorize(ApiTagConstants.UPDATE_ACCESS_RIGHTS_API, HeaderServiceCall.of(
                (header, request) -> accessManager.deactivateAccessRight(request, accessId)));
    }

    @Override
    public ServerServiceCall<NotUsed, APITagDetail> getBusinessUserApis(String businessUserId) {
        return authorize(ApiTagConstants.UPDATE_BUSINESS_USER_ACLLIST_API, HeaderServiceCall.of(
                (header, request) -> businessManager.getBusinessUserApis(businessUserId)));
    }

    public <Request, Response> ServerServiceCall<Request, Response> authorize(String apiTagName,
       ServerServiceCall<Request, Response> serviceCall) {
        return HeaderServiceCall.compose(
            requestHeader -> {
                Optional<String> apiTagList = requestHeader.getHeader(ApiTagConstants.X_USR_API_TAG);
                if(apiTagList.isPresent() && Stream.of(apiTagList.get().split(","))
                        .anyMatch(apiTagName::equals)){
                    log.info("Received request:{},{} Access Allowed",  requestHeader.method(),requestHeader.uri());
                    return serviceCall;
                }else {
                    log.info("Received request:{},{} Access Denied",  requestHeader.method(),requestHeader.uri());
                    throw new Forbidden("User Dont have the access for this service call");
                }
            });
    }

}