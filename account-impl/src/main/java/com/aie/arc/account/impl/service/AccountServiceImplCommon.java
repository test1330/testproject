package com.aie.arc.account.impl.service;

import akka.cluster.sharding.typed.javadsl.ClusterSharding;
import akka.cluster.sharding.typed.javadsl.EntityRef;
import com.aie.arc.account.impl.command.*;
import com.aie.arc.account.impl.entity.*;
import com.aie.arc.account.impl.entity.organization.subscribe.entity.OfflineStoreEntity;
import com.aie.arc.account.impl.entity.organization.subscribe.entity.OnlineStoreEntity;
import com.aie.arc.tool.organization.subscribe.command.OfflineStoreCommand;
import com.aie.arc.tool.organization.subscribe.command.OnlineStoreCommand;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRef;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;
import com.typesafe.config.Config;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Singleton
public class AccountServiceImplCommon {
    @Inject
    public PersistentEntityRegistry registry;

    @Inject
    public ClusterSharding clusterSharding;

    @Inject
    private Config config;

    /**
     * Obtain a reference to the PersistentEntity with the specified id.
     *
     * @param id a unique identifier for Address.
     * @return a PersistentEntityRef for Address.
     */
    public EntityRef<AddressCommand> getEntityRefForAddress(String id) {
        return clusterSharding.entityRefFor(AddressEntity.ENTITY_TYPE_KEY, id);
    }

    /**
     * Obtain a reference to the PersistentEntity with the specified id.
     *
     * @param id a unique identifier for Account.
     * @return a PersistentEntityRef for Account.
     */
    public EntityRef<AccountCommand> getEntityRefForAccount(String id) {
        return clusterSharding.entityRefFor(AccountEntity.ENTITY_TYPE_KEY, id);
    }

    /**
     * Obtain a reference to the PersistentEntity with the specified id.
     *
     * @param id a unique identifier for Card.
     * @return a PersistentEntityRef for Card.
     */
    public EntityRef<CardCommand> getEntityRefForCard(String id) {
        return clusterSharding.entityRefFor(CardEntity.ENTITY_TYPE_KEY, id);
    }

    public List<String> getSupportedCards() {
        return config.getStringList("payment.supportedCards");
    }

    public Map<String, String> getCardNickNameMap() {
        return Arrays.stream(config.getString("payment.cardNickNameMap").split(","))
                .map(e -> e.split("="))
                .collect(Collectors.toMap(s -> s[0], s -> s[1]));
    }

    public Map<String, String> getCardCodeMap() {
        return Arrays.stream(config.getString("payment.cardCodeMap").split(","))
                .map(e -> e.split("="))
                .collect(Collectors.toMap(s -> s[0], s -> s[1]));
    }

    public Map<String, List<String>> getCardLengthsMap() {
        return Arrays.stream(config.getString("payment.cardLengthsMap").split(","))
                .map(e -> e.split(":"))
                .collect(Collectors.groupingBy(a -> a[1],
                        Collectors.mapping(a -> a[0], Collectors.toList())));
    }

    public Map<String, List<String>> getCardPrefixesMap() {
        return Arrays.stream(config.getString("payment.cardPrefixesMap").split(","))
                .map(e -> e.split(":"))
                .collect(Collectors.groupingBy(a -> a[1],
                        Collectors.mapping(a -> a[0], Collectors.toList())));
    }

    public Map<String, String> getRoldIds() {
        return Arrays.stream(config.getString("account.roleIds").split(","))
                .map(e -> e.split("="))
                .collect(Collectors.toMap(s -> s[0], s -> s[1]));
    }

    String getNewId() {
        return UUID.randomUUID().toString();
    }

    /**
     * Obtain a reference to the PersistentEntity with the specified id.
     *
     * @param id a unique identifier for BusinessUser.
     * @return a PersistentEntityRef for BusinessUser.
     */
    EntityRef<BusinessUserCommand> getEntityRefForBusinessUser(String id) {
        return clusterSharding.entityRefFor(BusinessUserEntity.ENTITY_TYPE_KEY, id);
    }

    /**
     * Obtain a reference to the PersistentEntity with the specified id.
     *
     * @param id a unique identifier for CustomerGroup.
     * @return a PersistentEntityRef for CustomerGroup.
     */
    EntityRef<CustomerGroupCommand> getEntityRefForCustomerGroup(String id) {
        return clusterSharding.entityRefFor(CustomerGroupEntity.ENTITY_TYPE_KEY, id);
    }

    /**
     * Obtain a reference to the PersistentEntity with the specified id.
     *
     * @param id a unique identifier for Address.
     * @return a PersistentEntityRef for Address.
     */
    EntityRef<BusinessAddressCommand> getEntityRefForBusinessAddress(String id) {
        return clusterSharding.entityRefFor(BusinessAddressEntity.ENTITY_TYPE_KEY, id);
    }

    EntityRef<OnlineStoreCommand> getEntityRefForOnlineStore(String id) {
        return clusterSharding.entityRefFor(OnlineStoreEntity.ENTITY_TYPE_KEY, id);
    }

    public EntityRef<StoreCommand> getEntityRefForStore(String storeId) {
        return clusterSharding.entityRefFor(StoreEntity.ENTITY_TYPE_KEY, storeId);
    }

    public EntityRef<OfflineStoreCommand> getEntityRefForOfflineStore(String storeId) {
        return clusterSharding.entityRefFor(OfflineStoreEntity.ENTITY_TYPE_KEY, storeId);
    }

    public PersistentEntityRef<ErrorProcessorCommand> entityRefForErrorProcessor(String id) {
        return registry.refFor(ErrorProcessorEntity.class, id);
    }


}