package com.aie.arc.account.impl.service;

import akka.Done;
import akka.japi.Pair;
import com.aie.arc.account.api.common.exceptions.ParameterRequired;
import com.aie.arc.account.api.common.request.ActivationRequest;
import com.aie.arc.account.api.common.request.BaseRequest;
import com.aie.arc.account.api.common.request.LocalisedFields;
import com.aie.arc.account.api.common.response.BaseResponse;
import com.aie.arc.account.api.constants.AccountURIConstants;
import com.aie.arc.account.api.request.SubscribeRequest;
import com.aie.arc.account.impl.command.*;
import com.aie.arc.account.impl.entity.entities.*;
import com.aie.arc.account.impl.entity.entities.vo.LocalizedFields;
import com.aie.arc.account.impl.repository.repository.LocaleRepository;
import com.aie.arc.account.impl.util.LogUtil;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import com.typesafe.config.Config;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.CompletionStage;

import static com.aie.arc.account.impl.util.MessageConstants.*;

@Slf4j
@Singleton
public class AccountServiceValidation {
    private final AccountServiceImplCommon accountServiceCommon;
    private final Validator validator;
    @Inject
    Config config;

    public static final String STR_IS_ALREADY_ACTIVE = "' is already active.";
    public static final String STR_ADDRESS_ID_ERROR = "addressId: Error301:";
    public static final String STR_ADDRESS = "Address '";
    public static final String STR_ALREADY_DEACTIVATED = "' is already deactivated.";

    @Inject
    public AccountServiceValidation(AccountServiceImplCommon accountServiceCommon) {
        this.accountServiceCommon = accountServiceCommon;
        this.validator = Validation.buildDefaultValidatorFactory().getValidator();
    }


    public void validateJsonRequest(BaseRequest request) {
        //Handling annotation based data validation here
        log.debug("Start : validateJsonRequest");
        Set<ConstraintViolation<BaseRequest>> violations = validator.validate(request);
        // if violations size > 0, It means, BaseRequest has some error
        if (violations.size() > 0) {
            log.error("Request validation error is : {}", violations.stream()
                    .map(x -> getValueFromConfig(x.getPropertyPath().toString(), x.getMessage()))
                    .findFirst().orElse(null));
            throw new ParameterRequired(violations.stream().map(x ->
                    getValueFromConfig(x.getPropertyPath().toString(), x.getMessage())).toArray(String[]::new));
        }
        log.debug("End : validateJsonRequest");
    }

    private String getValueFromConfig(String property, String message) {
        try {
            return property + ":" + config.getString(message + "_" + property);

        } catch (Exception e) {
            return property + COMMON_ERROR_CODE + message;
        }
    }

    /*public CompletionStage<Void> validateAccountById(String accountId) {
        log.debug("Here here -----------");
        return accountServiceCommon.getEntityRefForAccount(accountId).ask(AccountCommand.GetAccount::new, ServiceConstants.ASK_TIMEOUT)
                .thenAccept(account -> {
                    log.debug("accepted --------------- ");
                    log.debug("account : {} ", account);
                    log.debug("!account.isPresent() : {} ", !account.isPresent());
                    if (!account.isPresent()) {
                        log.debug("validating account id ");
                        throw new ParameterRequired(new String[]{"accountId: Error300:" + " Account id (" + accountId + ") " + IS_NOT_VALID});
                    }
                });
    }*/

    /*public void validateAddressById(String addressId) {
        log.debug("Here here -----------");
        accountServiceCommon.getEntityRefForAddress(addressId).ask(AddressCommand.GetAddress::new, ServiceConstants.ASK_TIMEOUT)
                .thenAccept(address -> {
                    log.debug("accepted --------------- ");
                    if (!address.isPresent())
                        throw new ParameterRequired(new String[]{STR_ADDRESS_ID_ERROR + "(" + addressId + ") " + IS_NOT_VALID});
                });
    }*/

    /*public void validateCardById(String cardId) {
        accountServiceCommon.getEntityRefForCard(cardId).ask(CardCommand.GetCard::new, ServiceConstants.ASK_TIMEOUT)
                .thenAccept(optionalCard -> {
                    if (!optionalCard.isPresent())
                        throw new ParameterRequired(new String[]{"cardId:Error302:" + "(" + cardId + ") " + IS_NOT_VALID});
                });

    }*/

    public CompletionStage<Optional<Account>> validateAndGetAccountById(String accountId) {
        log.debug("validate accountId : {}", accountId);
        return accountServiceCommon.getEntityRefForAccount(accountId).ask(AccountCommand.GetAccount::new, ServiceConstants.ASK_TIMEOUT)
                .thenApply(account -> {
                    if (!account.isPresent())
                        throw new ParameterRequired(new String[]{"accountId:Error300:" + "Account id(" + accountId + ") " + IS_NOT_VALID});
                    return account;
                });
    }

    /**
     * Get an Address object using the specified entity id.
     *
     * @param addressId a unique identity for the Address whose object is to be returned.
     * @return object of Address having the specified id if present, throws a NotFound exception if
     * Address with specified id does not exist.
     */
    public CompletionStage<Optional<Address>> validateAndGetAddressById(String addressId) {
        log.debug("validate addressId : {}", addressId);
        return accountServiceCommon.getEntityRefForAddress(addressId).ask(AddressCommand.GetAddress::new, ServiceConstants.ASK_TIMEOUT)
                .thenApply(address -> {
                    if (!address.isPresent())
                        throw new ParameterRequired(new String[]{"addressId: Error300: " + "Address id (" + addressId + ") " + IS_NOT_VALID});
                    return address;
                });
    }

    public CompletionStage<Optional<Card>> validateAndGetCardById(String cardId) {
        log.debug("validate cardId : {} ", cardId);
        return accountServiceCommon.getEntityRefForCard(cardId).ask(CardCommand.GetCard::new, ServiceConstants.ASK_TIMEOUT)
                .thenApply(optionalCard -> {
                    if (!optionalCard.isPresent())
                        throw new ParameterRequired(new String[]{"cardId: Error302: Card id (" + cardId + ") " + IS_NOT_VALID});
                    return optionalCard;
                });

    }


    public void validateAccountIds(List<String> accountIds) {
        accountIds.forEach(this::validateAndGetAccountById);
    }


    /* Business User activate or deactivate*/
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndSubscribe(String id, SubscribeRequest request) {
        return validateAndGetAccountById(id)
                .thenCompose(optionalAccount -> {
                    Account account = optionalAccount.get();
                    if (account.isSubscribed()) {
                        throw new ParameterRequired(new String[]{"Account '" + id + "' is already subscribe."});
                    }
                    return accountServiceCommon.getEntityRefForAccount(id).<Done>ask(replyTo -> new AccountCommand.Subscribe(id, true, replyTo), ServiceConstants.ASK_TIMEOUT)
                            .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS,
                                    AccountURIConstants.SUBSCRIBE_URI, AccountURIConstants.SUBSCRIBE_MESSAGE, request));
                });
    }


    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndUnsubscribe(String id, SubscribeRequest request) {
        return validateAndGetAccountById(id)
                .thenCompose(optionalAccount -> {
                    Account account = optionalAccount.get();
                    if (!account.isSubscribed()) {
                        throw new ParameterRequired(new String[]{"Account'" + id + "' is already unsubscribe."});
                    }
                    return accountServiceCommon.getEntityRefForAccount(id).<Done>ask(replyTo -> new AccountCommand.Unsubscribe(id, false, replyTo), ServiceConstants.ASK_TIMEOUT)
                            .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS,
                                    AccountURIConstants.SUBSCRIBE_URI, AccountURIConstants.UNSUBSCRIBE_MESSAGE, request));
                });

    }


    /* address activate or deactivate*/
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndActivateAddress(
            String id, String accountId, ActivationRequest request) {
        return validateAndGetAccountById(accountId)
                .thenCompose(optionalAccount -> validateAndGetAddressById(id)
                        .thenCompose(address -> {
                            if (address.get().isActive()) {
                                throw new ParameterRequired(new String[]{STR_ADDRESS + id + STR_IS_ALREADY_ACTIVE});
                            }
                            return accountServiceCommon.getEntityRefForAddress(id)
                                    .<Done>ask(replyTo -> new AddressCommand.ActivateAddress(id, true, replyTo),
                                            ServiceConstants.ASK_TIMEOUT)
                                    .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS,
                                            AccountURIConstants.ACTIVE_ADDRESS_URI,
                                            AccountURIConstants.ADDRESS_ACTIVATE_MESSAGE, request));
                        })
                );
    }

    /* address activate or deactivate*/
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndDeactivateAddress(
            String id, String accountId, ActivationRequest request, boolean isMigrationEvt) {
        return validateAndGetAccountById(accountId)
                .thenCompose(optionalAccount -> validateAndGetAddressById(id)
                        .thenCompose(optionalAddress -> {
                            if (!optionalAddress.get().isActive()) {
                                throw new ParameterRequired(new String[]{STR_ADDRESS + id + STR_ALREADY_DEACTIVATED});
                            }
                            return accountServiceCommon.getEntityRefForAddress(id)
                                    .<Done>ask(replyTo -> new AddressCommand.DeactivateAddress(id, false, replyTo),
                                            ServiceConstants.ASK_TIMEOUT)
                                    .thenApply(done -> accountServiceCommon.getEntityRefForAccount(accountId)
                                            .<Done>ask(replyTo -> new AccountCommand.DeactivateAddressToAccount(
                                                    accountId, id, isMigrationEvt, replyTo), ServiceConstants.ASK_TIMEOUT))
                                    .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS,
                                            AccountURIConstants.ACTIVE_ADDRESS_URI,
                                            AccountURIConstants.ADDRESS_DEACTIVATE_MESSAGE, request));
                        })
                );
    }

    /* card activate or deactivate*/
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndActivateCard(
            String id, ActivationRequest request) {
        return validateAndGetCardById(id)
                .thenCompose(optionalCard -> {
                    if (optionalCard.get().isActive()) {
                        throw new ParameterRequired(new String[]{"Card '" + id + STR_IS_ALREADY_ACTIVE});
                    }
                    return accountServiceCommon.getEntityRefForCard(id)
                            .<Done>ask(replyTo -> new CardCommand.ActivateCard(id, true, replyTo),
                                    ServiceConstants.ASK_TIMEOUT)
                            .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS,
                                    AccountURIConstants.ACTIVE_CARD_URI,
                                    AccountURIConstants.CARD_ACTIVATE_MESSAGE, request));
                });
    }

    /* address activate or deactivate*/
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndDeactivateCard(
            String cardId, String accountId, ActivationRequest request, boolean isMigrationEvt) {
        return validateAndGetAccountById(accountId)
                .thenCompose(optionalAccount ->
                        validateAndGetCardById(cardId)
                                .thenCompose(optionalCard -> {
                                    if (!optionalCard.get().isActive()) {
                                        throw new ParameterRequired(new String[]{"Card '" + cardId + STR_ALREADY_DEACTIVATED});
                                    }
                                    return accountServiceCommon.getEntityRefForCard(cardId).<Done>ask(replyTo -> new CardCommand.DeactivateCard(cardId, false, replyTo), ServiceConstants.ASK_TIMEOUT)
                                            .thenCompose(linkCardToAccount -> accountServiceCommon.getEntityRefForAccount(accountId)
                                                    .<Done>ask(replyTo -> new AccountCommand.DeactivateAccountCard(accountId, cardId, false, isMigrationEvt, replyTo), ServiceConstants.ASK_TIMEOUT))
                                            .thenApply(done -> sendResponseByStatusIdAndRequest(cardId, SUCCESS_STATUS,
                                                    AccountURIConstants.ACTIVE_CARD_URI, AccountURIConstants.CARD_DEACTIVATE_MESSAGE, request));
                                })
                );
    }

    public Pair<ResponseHeader, BaseResponse> sendResponseByStatusIdAndRequest(String Id, Integer STATUS, String URI,
                                                                               String MESSAGE, BaseRequest request) {
        log.debug("Start : sendResponseByStatusIdAndRequest");
        BaseResponse response = BaseResponse.getResponseByStatusAndMessage(Id, STATUS, MESSAGE);
        LogUtil.log(new StringBuilder(LOG_RESPONSE).append(URI).append(LINE_SEPARATOR).append("REQ: ")
                .append(request.toString()).append(LINE_SEPARATOR).append("RES: ")
                .append(response).append(LINE_SEPARATOR).toString());
        log.debug("End : sendResponseByStatusIdAndRequest");
        return Pair.create(ResponseHeader.OK.withStatus(STATUS), response);
    }

    public Pair<ResponseHeader, BaseResponse> sendResponseByStatusId(Integer STATUS, String URI,
                                                                     String MESSAGE) {
        BaseResponse response = BaseResponse.getResponseByStatusAndMessage(STATUS, MESSAGE);
        LogUtil.log(new StringBuilder(LOG_RESPONSE).append(URI).append(LINE_SEPARATOR).append(LINE_SEPARATOR)
                .append("RES: ").append(response).append(LINE_SEPARATOR).toString());
        return Pair.create(ResponseHeader.OK.withStatus(STATUS), response);
    }

    public void validateExpiryDate(String localDateStr) {
        LocalDate localDate = LocalDate.parse(localDateStr);
        if (localDate.isBefore(LocalDate.now())) {
            throw new ParameterRequired(new String[]{"expiryDate should be grated than currentDate"});
        }
    }

    /**
     * Get a BusinessUser object using the specified entity id.
     *
     * @param businessUserId a unique identity for the BusinessUser whose object is to be returned.
     * @return object of BusinessUser having the specified id if present, throws a NotFound exception if
     * BusinessUser with specified id does not exist.
     */
    public BusinessUser validateAndGetBusinessUserById(String businessUserId) {
        return accountServiceCommon.getEntityRefForBusinessUser(businessUserId).ask(BusinessUserCommand.GetBusinessUser::new, ServiceConstants.ASK_TIMEOUT)
                .toCompletableFuture().join().orElseThrow(() -> new ParameterRequired(new String[]{"businessUserId: " +
                        "BusinessUser id(" + businessUserId + IS_NOT_VALID}));
    }

    /*validating Business User ...*/
    public void validateBusinessUserById(String businessUserId) {
        if (!accountServiceCommon.getEntityRefForBusinessUser(businessUserId).ask(BusinessUserCommand.GetBusinessUser::new, ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().isPresent())
            throw new ParameterRequired(new String[]{"businessUserId: " + "BusinessUser id(" + businessUserId + IS_NOT_VALID});
    }

    /*validating CustomerGroup ...*/
    public void validateCustomerGroupId(String customerGroupId) {
        if (!accountServiceCommon.getEntityRefForCustomerGroup(customerGroupId).ask(CustomerGroupCommand.GetCustomerGroup::new, ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().isPresent())
            throw new ParameterRequired(new String[]{"customerGroupId: " + "CustomerGroup id(" + customerGroupId + IS_NOT_VALID});
    }

    public CustomerGroup validateAndGetCustomerGroupById(String customerGroupId) {
        return accountServiceCommon.getEntityRefForCustomerGroup(customerGroupId).ask(CustomerGroupCommand.GetCustomerGroup::new, ServiceConstants.ASK_TIMEOUT)
                .toCompletableFuture().join().orElseThrow(() -> new ParameterRequired(new String[]{"customerGroupId: " +
                        "CustomerGroup id(" + customerGroupId + IS_NOT_VALID}));
    }


    Map<String, LocalizedFields> validateLocaleCodesAndGetVoMap(Map<String, LocalisedFields> requestMap, LocaleRepository localeRepository) {
        Map<String, LocalizedFields> localizedFieldsVOMap = new HashMap<>();
        StringJoiner localCodes = new StringJoiner(",");
        requestMap.forEach((code, localisedFields) -> {
            localCodes.add("'" + code + "'");
            localizedFieldsVOMap.put(code, new LocalizedFields(localisedFields.getName(), localisedFields.getDescription()));
        });
        localeRepository.validateAllLocaleCodes(localCodes)
                .thenAccept(count -> {
                    if (count != localizedFieldsVOMap.size())
                        throw new com.aie.arc.rbac.impl.exceptions.ParameterRequired(new String[]{"given codes: (" + localCodes.toString() +
                                ") are not valid"});
                });
        return localizedFieldsVOMap;
    }

    /* Customer Group activate or deactivate*/
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndActivateCustomerGroup(String id, ActivationRequest request) {
        CustomerGroup customerGroup = validateAndGetCustomerGroupById(id);
        if (customerGroup.isActive()) {
            throw new ParameterRequired(new String[]{"Customer Group '" + id + STR_IS_ALREADY_ACTIVE});
        }
        return accountServiceCommon.getEntityRefForCustomerGroup(id).<Done>ask(replyTo -> new CustomerGroupCommand.ActivateCustomerGroup(id, true, replyTo), ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS, AccountURIConstants.
                        ACTIVE_CUSTOMER_GROUP_URI, AccountURIConstants.CUSTOMER_GROUP_ACTIVATE_MESSAGE, request));
    }

    /* Customer Group  activate or deactivate*/
    public CompletionStage<Pair<ResponseHeader, com.aie.arc.account.api.common.response.BaseResponse>> validateAndDeactivateCustomerGroup(
            String id, ActivationRequest request) {
        CustomerGroup customerGroup = validateAndGetCustomerGroupById(id);
        if (!customerGroup.isActive()) {
            throw new ParameterRequired(new String[]{"Customer Group '" + id + STR_ALREADY_DEACTIVATED});
        }
        return accountServiceCommon.getEntityRefForCustomerGroup(id).<Done>ask(replyTo -> new CustomerGroupCommand.DeactivateCustomerGroup(id, false, replyTo), ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS, AccountURIConstants.
                        ACTIVE_CUSTOMER_GROUP_URI, AccountURIConstants.CUSTOMER_GROUP_DEACTIVATE_MESSAGE, request));
    }

    /* Account activate or deactivate*/
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndActivateBusinessUser(String id, ActivationRequest request) {
        BusinessUser businessUser = validateAndGetBusinessUserById(id);
        if (businessUser.isActive()) {
            throw new ParameterRequired(new String[]{"BusinessUser '" + id + STR_IS_ALREADY_ACTIVE});
        }
        return accountServiceCommon.getEntityRefForBusinessUser(id).<Done>ask(replyTo -> new BusinessUserCommand.ActivateBusinessUser(id, true, replyTo), ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS, AccountURIConstants.
                        ACTIVE_BUSINESS_USER_URI, AccountURIConstants.BUSINESS_USER_ACTIVATE_MESSAGE, request));
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndDeactivateBusinessUser(String id, ActivationRequest request) {
        BusinessUser businessUser = validateAndGetBusinessUserById(id);
        if (!businessUser.isActive()) {
            throw new ParameterRequired(new String[]{"BusinessUser '" + id + STR_ALREADY_DEACTIVATED});
        }
        return accountServiceCommon.getEntityRefForBusinessUser(id).<Done>ask(replyTo -> new BusinessUserCommand.DeactivateBusinessUser(id, true, replyTo), ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS, AccountURIConstants.
                        ACTIVE_BUSINESS_USER_URI, AccountURIConstants.BUSINESS_USER_DEACTIVATE_MESSAGE, request));
    }

    public BusinessAddress validateAndGetBusinessAddressById(String addressId) {
        log.debug("validate addressId : {}", addressId);
        return accountServiceCommon.getEntityRefForBusinessAddress(addressId).ask(BusinessAddressCommand.GetAddress::new, ServiceConstants.ASK_TIMEOUT)
                .toCompletableFuture().join()
                .orElseThrow(() -> new ParameterRequired(new String[]{STR_ADDRESS_ID_ERROR + "(" + addressId + ") "
                        + IS_NOT_VALID}));

    }

    /* address activate or deactivate*/
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndActivateBusinessAddress(String id, ActivationRequest request) {
        BusinessAddress address = validateAndGetBusinessAddressById(id);

        if (address.isActive()) {
            throw new ParameterRequired(new String[]{STR_ADDRESS + id + STR_IS_ALREADY_ACTIVE});
        }
        return accountServiceCommon.getEntityRefForAddress(id).<Done>ask(replyTo -> new AddressCommand.ActivateAddress(id, true, replyTo), ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS, AccountURIConstants.ACTIVE_ADDRESS_URI,
                        AccountURIConstants.ADDRESS_ACTIVATE_MESSAGE, request));
    }

    /* address activate or deactivate*/
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndDeactivateBusinessAddress(String id, ActivationRequest request) {
        BusinessAddress address = validateAndGetBusinessAddressById(id);
        if (!address.isActive()) {
            throw new ParameterRequired(new String[]{STR_ADDRESS + id + STR_ALREADY_DEACTIVATED});
        }
        return accountServiceCommon.getEntityRefForAddress(id).<Done>ask(replyTo -> new AddressCommand.DeactivateAddress(id, false, replyTo), ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS, AccountURIConstants.ACTIVE_ADDRESS_URI,
                        AccountURIConstants.ADDRESS_DEACTIVATE_MESSAGE, request));
    }

    public void validateBusinessAddressById(String addressId) {
        if (!accountServiceCommon.getEntityRefForBusinessAddress(addressId).ask(BusinessAddressCommand.GetAddress::new, ServiceConstants.ASK_TIMEOUT)
                .toCompletableFuture().join().isPresent())
            throw new ParameterRequired(new String[]{STR_ADDRESS_ID_ERROR + "(" + addressId + ") " + IS_NOT_VALID});
    }

    public void validateStoreById(String storeId) {
        if (!accountServiceCommon.getEntityRefForStore(storeId).ask(StoreCommand.GetStore::new, ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().isPresent())
            throw new ParameterRequired(new String[]{"Store Id: " + "(" + storeId + IS_NOT_VALID});
    }

    public void validateLengthForAddress2(String address2) {
        if (StringUtils.isNotBlank(address2) && (address2.trim().length() > 35)) {
            throw new ParameterRequired(new String[]{"address2: Error352: Please enter valid field length(Max 35 characters)."});
        }
    }
}