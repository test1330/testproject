package com.aie.arc.account.impl.service;

import akka.Done;
import akka.cluster.sharding.typed.javadsl.EntityRef;
import akka.http.javadsl.model.StatusCodes;
import akka.japi.Pair;
import com.aie.arc.account.api.common.exceptions.CommonException;
import com.aie.arc.account.api.common.exceptions.InternalServerError;
import com.aie.arc.account.api.common.exceptions.ParameterRequired;
import com.aie.arc.account.api.common.request.ActivationRequest;
import com.aie.arc.account.api.common.response.BaseResponse;
import com.aie.arc.account.api.email.EmailRequest;
import com.aie.arc.account.api.email.Personalizations;
import com.aie.arc.account.api.email.To;
import com.aie.arc.account.api.publishedevent.AddressEvents;
import com.aie.arc.account.api.request.AccountAddressRequest;
import com.aie.arc.account.api.request.UpdateAccountAddressRequest;
import com.aie.arc.account.api.response.AccountAddress;
import com.aie.arc.account.api.response.AddressDetail;
import com.aie.arc.account.api.response.SuggestedAddress;
import com.aie.arc.account.api.response.SuggestedAddresses;
import com.aie.arc.account.impl.command.AccountCommand;
import com.aie.arc.account.impl.command.AddressCommand;
import com.aie.arc.account.impl.email.EmailService;
import com.aie.arc.account.impl.entity.entities.Account;
import com.aie.arc.account.impl.entity.entities.Address;
import com.aie.arc.account.impl.entity.entities.vo.Profile;
import com.aie.arc.account.impl.event.AddressEvent;
import com.aie.arc.account.impl.mapper.Mapper;
import com.aie.arc.account.impl.repository.AddressRepository;
import com.aie.arc.account.impl.util.MessageConstants;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.aie.arc.externalservice.api.request.AvalaraRequest;
import com.aie.arc.externalservice.api.response.AddressError;
import com.aie.arc.externalservice.api.response.AvalaraResponse;
import com.aie.arc.externalservice.api.response.ValidatedAddressInfo;
import com.lightbend.lagom.javadsl.api.broker.Topic;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import com.lightbend.lagom.javadsl.broker.TopicProducer;
import com.typesafe.config.Config;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.common.util.CollectionUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.StringWriter;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static com.aie.arc.account.api.constants.AccountURIConstants.*;
import static com.aie.arc.account.impl.util.MessageConstants.*;

@Slf4j
@Singleton
public class AddressManager {
    @Inject
    private AccountServiceImplCommon accountServiceCommon;

    @Inject
    private AccountServiceValidation accountServiceValidation;

    @Inject
    private AddressRepository addressRepository;

    @Inject
    private IntegrationServiceManager integrationServiceManager;

    @Inject
    private EmailService emailService;

    @Inject
    private AccountEventPublisher accountEventPublisher;
    @Inject
    private Config config;

    private String addressChangeTemplateId = null;

    public CompletionStage<Pair<ResponseHeader, SuggestedAddresses>> validateAccountAddress(
            AccountAddressRequest request, String accountId) {
        log.debug("Start : Validate Add address to accountId : {}", accountId);
        accountServiceValidation.validateJsonRequest(request);
        //accountServiceValidation.validateAndGetAccountById(accountId);
        AvalaraRequest avalaraRequest = constructAvalaraRequest(request);
        ResponseHeader responseHeader = null;
        AvalaraResponse avalaraResponse = null;

        try {
            CompletionStage<Pair<ResponseHeader, AvalaraResponse>> completionStage =
                    integrationServiceManager.validateAddress(avalaraRequest);
            responseHeader = completionStage.toCompletableFuture().get().first();
            avalaraResponse = completionStage.toCompletableFuture().get().second();

            log.debug(" Avalara Response :{}", avalaraResponse);

        } catch (Exception e) {
            log.error("Exception occurred while avalara response parsing" + e.getMessage());
        }
        if (avalaraResponse != null && !CollectionUtils.isEmpty(avalaraResponse.getMessages())) {
            String[] message = new String[avalaraResponse.getMessages().size()];
            int index = INTEGER_ZERO;
            for (AddressError addressError : avalaraResponse.getMessages()) {
                String errorSummary = null;
                if (ADDRESS_OUT_OF_RANGE.equalsIgnoreCase(addressError.getSummary())) {
                    errorSummary = BRANDSMART_UNABLE_TO_VALIDATE_ADDRESS;
                } else {
                    errorSummary = addressError.getSummary();
                }
                message[index] = COMMON + ":" + AVALARA_SERVICE_BAD_REQUEST_STATUS + ":" + errorSummary;
                index++;
            }
            throw new CommonException(message);
        } else if (avalaraResponse != null && !CollectionUtils.isEmpty(avalaraResponse.getValidatedAddresses())) {
            SuggestedAddresses suggestedAddresses = null;
            List<SuggestedAddress> suggestedAddress = avalaraResponse.getValidatedAddresses().stream()
                    .map(validatedAddressInfo -> new SuggestedAddress(validatedAddressInfo.getAddressType(),
                            validatedAddressInfo.getLine1(), validatedAddressInfo.getLine2(),
                            validatedAddressInfo.getLine3(), validatedAddressInfo.getCity(),
                            validatedAddressInfo.getRegion(), validatedAddressInfo.getCountry(),
                            validatedAddressInfo.getPostalCode().substring(INTEGER_ZERO, INTEGER_FIVE), validatedAddressInfo.getLatitude(),
                            validatedAddressInfo.getLongitude()))
                    .collect(Collectors.toList());
            if (avalaraResponse.getValidatedAddresses().size() == ONE
                    && isSuggestedAddressAndRequestedAddressSame(avalaraResponse.getValidatedAddresses().get(INTEGER_ZERO), request)) {
                suggestedAddresses = new SuggestedAddresses(false, suggestedAddress);
                return CompletableFuture.completedFuture(Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), suggestedAddresses));
            } else {
                suggestedAddresses = new SuggestedAddresses(true, suggestedAddress);
                log.debug("Address has been improved");
                return CompletableFuture.completedFuture(Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), suggestedAddresses));
            }
        } else {
            throw new InternalServerError(new String[]{COMMON + ":" + AVALARA_SERVICE_FAILURE_STATUS + ":"
                    + AVALARA_SERVICE_FAILURE_MESSAGE});
        }
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> addAccountAddress(AccountAddressRequest request,
                                                                                 String accountId, String addressId) {
        log.debug("Add address to accountId : {}", accountId);
        accountServiceValidation.validateJsonRequest(request);
        accountServiceValidation.validateAndGetAccountById(accountId);
        accountServiceValidation.validateLengthForAddress2(request.getAddress2());
        String id = StringUtils.isBlank(addressId) ? accountServiceCommon.getNewId() : addressId;
        EntityRef<AddressCommand> addressRef = accountServiceCommon.getEntityRefForAddress(id);
        Address address = Address.createAddressByCustomerRequest(id, accountId, request);
        log.debug("Add address : with : [city: {}, state: {}, zipCode: {}] to addressId: {} and accountId: {}",
                request.getCity(), request.getState(), request.getPostalCode(), address.getAddressId(), accountId);
        return addressRef.<Done>ask(replyTo -> new AddressCommand.CreateAddress(address, replyTo), ServiceConstants.ASK_TIMEOUT)
                .thenCompose(linkAddressToAccount -> accountServiceCommon.getEntityRefForAccount(accountId)
                        .<Done>ask(replyTo -> new AccountCommand.AddAddressToAccount(accountId, address.getAddressId(),
                                request.getPostalCode(), request.isDefaultShipping(), request.isDefaultBilling(),
                                StringUtils.isNotBlank(addressId), replyTo), ServiceConstants.ASK_TIMEOUT)
                        .thenApply(response -> accountServiceValidation.sendResponseByStatusIdAndRequest(
                                address.getAddressId(), StatusCodes.CREATED.intValue(),
                                ADD_ACCOUNT_ADDRESS_URI, ACCOUNT_ADDRESS_CREATE_MESSAGE, request)));
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> updateAccountAddress(
            UpdateAccountAddressRequest request, String accountId, String addressId, boolean isMigrationEvt) {
        log.debug("Update address with addressId : {} for accountId : {}", addressId, accountId);
        accountServiceValidation.validateJsonRequest(request);
        accountServiceValidation.validateLengthForAddress2(request.getAddress2());
        return accountServiceValidation.validateAndGetAddressById(addressId)
                .thenCompose(optionalAddress ->
                        accountServiceValidation.validateAndGetAccountById(accountId)
                                .thenApply(optionalAccount -> {
                                    Account account = optionalAccount.get();
                                    Address updateAddress = Address.updateAddressByCustomerRequest(
                                            optionalAddress.get(), request);
                                    log.debug("Update address : with : [city: {}, state: {}, zipCode: {}] to " +
                                                    "addressId: {} and accountId: {}", request.getCity(),
                                            request.getState(), request.getPostalCode(),
                                            optionalAddress.get().getAddressId(), accountId);
                                    accountServiceCommon.getEntityRefForAddress(addressId)
                                            .<Done>ask(replyTo -> new AddressCommand.UpdateAddress(updateAddress, replyTo),
                                                    ServiceConstants.ASK_TIMEOUT)
                                            .thenCompose(linkAddressToAccount ->
                                                    accountServiceCommon.getEntityRefForAccount(account.getAccountId())
                                                            .<Done>ask(replyTo -> new AccountCommand.UpdateAddressToAccount(
                                                                            accountId, addressId, request.getPostalCode(),
                                                                            request.isDefaultShipping(),
                                                                            request.isDefaultBilling(), isMigrationEvt,
                                                                            replyTo),
                                                                    ServiceConstants.ASK_TIMEOUT));
                                    if (config.getBoolean(MessageConstants.ENABLE_ARC_EMAIL)) {
                                        log.debug("Send Address Update Email called..");
                                        sendAddressUpdateEmail(account.getProfile());
                                    }
                                    return accountServiceValidation.sendResponseByStatusIdAndRequest(addressId,
                                            SUCCESS_STATUS, UPDATE_ACCOUNT_ADDRESS_URI, ACCOUNT_ADDRESS_UPDATE_MESSAGE,
                                            request);
                                })
                );


    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> setDefaultBillingAddress(String accountId, String addressId) {
        log.debug("Set default billing address with accountId: {} , addressId : {}", accountId, addressId);
        accountServiceValidation.validateAndGetAddressById(addressId);
        return accountServiceValidation.validateAndGetAccountById(accountId)
                .thenCompose(optionalAccount -> {
                    Account account = optionalAccount.get();
                    if (!account.getAddressIds().contains(addressId)) {
                        throw new ParameterRequired(new String[]{"addressId: " + "(" + addressId + ") is not valid Billing Address"});
                    }
                    return accountServiceCommon.getEntityRefForAccount(account.getAccountId())
                            .<Done>ask(replyTo -> new AccountCommand.UpdateDefaultBillingAddress(account.getAccountId(), addressId,
                                    account.getDefaultShippingAddressId(), replyTo), ServiceConstants.ASK_TIMEOUT)
                            .thenApply(response -> accountServiceValidation.sendResponseByStatusId(
                                    SUCCESS_STATUS, SET_DEFAULT_BILLING_ADDRESS_URI, DEFAULT_BILLING_ADDRESS_SET_MESSAGE));
                });

    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> setDefaultShippingAddress(
            String accountId, String addressId) {
        log.debug("Set default shipping address with accountId: {} , addressId : {}", accountId, addressId);
        return accountServiceValidation.validateAndGetAccountById(accountId)
                .thenCompose(optionalAccount -> {
                    Account account = optionalAccount.get();
                    return accountServiceValidation.validateAndGetAddressById(addressId)
                            .thenCompose(optionalAddress -> {
                                if (!account.getAddressIds().contains(addressId)) {
                                    throw new ParameterRequired(new String[]{"addressId: " + "(" + addressId + ") is not valid Shipping Address"});
                                }
                                EntityRef<AccountCommand> refForAccount = accountServiceCommon.getEntityRefForAccount(account.getAccountId());
                                return refForAccount.<Done>ask(replyTo -> new AccountCommand.UpdateDefaultShippingAddress(account.getAccountId(),
                                        addressId, account.getDefaultBillingAddressId(), replyTo), ServiceConstants.ASK_TIMEOUT)
                                        .thenApply(done -> refForAccount
                                                .<Done>ask(replyTo -> new AccountCommand.AddZipCode(account.getAccountId(),
                                                        optionalAddress.get().getPostalCode(), replyTo), ServiceConstants.ASK_TIMEOUT))
                                        .thenApply(response -> accountServiceValidation.sendResponseByStatusId(
                                                SUCCESS_STATUS, SET_DEFAULT_SHIPPING_ADDRESS_URI, DEFAULT_SHIPPING_ADDRESS_SET_MESSAGE));
                            });
                });

    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> activateAddress(
            ActivationRequest request, String accountId, String addressId, boolean isMigrationEvt) {
        log.debug("activate or deactivate address : {} using request flag : {}", addressId, request.getValue());
        if (request.getValue().booleanValue()) {
            return accountServiceValidation.validateAndActivateAddress(addressId, accountId, request);
        }
        return accountServiceValidation.validateAndDeactivateAddress(addressId, accountId, request, isMigrationEvt);
    }

    public CompletionStage<Pair<ResponseHeader, List<AccountAddress>>> getAddresses(String accountId) {
        log.debug("Get all the account addresses for accountId: {}", accountId);
        return accountServiceValidation.validateAndGetAccountById(accountId)
                .thenApply(optionalAccount -> {
                    Account account = optionalAccount.get();
                    return Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS),
                            account.getAddressIds().stream()
                                    .filter(Objects::nonNull)
                                    .map(accountServiceValidation::validateAndGetAddressById)
                                    .map(futureOfAddress ->
                                            Mapper.mapToAddress(futureOfAddress.toCompletableFuture().join().get(), account)
                                    ).collect(Collectors.toList()));
                });
    }

    public CompletionStage<Pair<ResponseHeader, AddressDetail>> getAccountAddress(String accountId, String addressId) {
        log.debug("Get address with accountId: {} , addressId : {}", accountId, addressId);
        return accountServiceValidation.validateAndGetAccountById(accountId)
                .thenCompose(optionalAccount ->
                        accountServiceValidation.validateAndGetAddressById(addressId)
                                .thenApply(optionalAddress ->
                                        Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS),
                                                Mapper.toAddressDetails(optionalAddress.get()))
                                )
                );


    }


    private AvalaraRequest constructAvalaraRequest(AccountAddressRequest request) {
        log.debug("Start : constructAvalaraRequest");
        AvalaraRequest avalaraRequest = new AvalaraRequest();
        avalaraRequest.setLine1(request.getAddress1());
        avalaraRequest.setLine2(request.getAddress2());
        avalaraRequest.setCity(request.getCity());
        avalaraRequest.setRegion(request.getState());
        avalaraRequest.setCountry(request.getCountry());
        avalaraRequest.setPostalCode(request.getPostalCode());
        log.debug("End : constructAvalaraRequest");
        return avalaraRequest;
    }

    private boolean isSuggestedAddressAndRequestedAddressSame(ValidatedAddressInfo validatedAddressInfo,
                                                              AccountAddressRequest request) {
        return request.getAddress1().equalsIgnoreCase(validatedAddressInfo.getLine1())
                && request.getAddress2().equalsIgnoreCase(validatedAddressInfo.getLine2())
                && request.getCity().equalsIgnoreCase(validatedAddressInfo.getCity())
                && request.getState().equalsIgnoreCase(validatedAddressInfo.getRegion())
                && request.getCountry().equalsIgnoreCase(validatedAddressInfo.getCountry())
                && validatedAddressInfo.getPostalCode().contains(request.getPostalCode());
    }

    private void sendAddressUpdateEmail(Profile profile) {
        StringWriter writer = new StringWriter();
        try {

            EmailRequest emailRequest = new EmailRequest();

            if (addressChangeTemplateId == null) {
                addressChangeTemplateId = config.getConfig(MessageConstants.CONFIG_EMAIL_SERVICE).getString("account.addresschange.email.template.id");
            }
            emailRequest.setTemplateId(addressChangeTemplateId);
            Personalizations personalizations = new Personalizations();
            personalizations.getContent().put("firstName",profile.getFirstName());
            personalizations.getContent().put("lastName",profile.getLastName());
            To toEmail = new To();
            toEmail.setEmail(profile.getEmail());
            toEmail.setName(new StringBuilder().append(profile.getFirstName()).append(profile.getLastName()).toString());
            personalizations.getTo().add(toEmail);
            personalizations.setSubject(ADDRESS_CHANGE_EMAIL);
            emailRequest.getPersonalizations().add(personalizations);
            emailService.sendMail(emailRequest);

        } catch (Exception e) {
            log.error("Address Update Send Email failed : {}", e.getMessage());
        }
    }

    public Topic<AddressEvents> addressPublishedEvent() {
        return TopicProducer.taggedStreamWithOffset(AddressEvent.TAG.allTags(),
                (tag, offset) -> accountServiceCommon.registry.eventStream(tag, offset)
                        .filter(p -> p.first().isPublic())
                        .mapAsync(1, eventAndOffset ->
                                accountEventPublisher.convertPublishedEventForAddress(eventAndOffset.first())
                                        .thenApply(event -> Pair.create(event, eventAndOffset.second()))));
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> testSendAddressUpdateEmail(EmailRequest request) {
        log.debug("testSendAddressUpdateEmail inside this method");

        try {
            log.debug("EmailRequest :{}",request);
            emailService.sendMail(request);

        } catch (Exception exp) {
            log.error("Email Sending failed exception : ", exp);
        }
        return CompletableFuture.completedFuture(accountServiceValidation.sendResponseByStatusId(
                SUCCESS_STATUS, "", TEST_EMAIL_SENT));
    }

}
