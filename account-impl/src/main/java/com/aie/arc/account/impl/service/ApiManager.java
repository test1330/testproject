package com.aie.arc.account.impl.service;

import akka.Done;
import akka.cluster.sharding.typed.javadsl.EntityRef;
import akka.http.javadsl.model.StatusCodes;
import akka.japi.Pair;
import com.aie.arc.account.api.common.request.ActivationRequest;
import com.aie.arc.account.api.common.request.SearchRequest;
import com.aie.arc.account.api.common.response.BaseResponse;
import com.aie.arc.account.impl.entity.entities.vo.common.FilterParameter;
import com.aie.arc.account.impl.repository.repository.LocaleRepository;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.aie.arc.account.impl.util.common.FilterConfig;
import com.aie.arc.rbac.api.constants.RBACURIConstants;
import com.aie.arc.rbac.api.publishedevent.ApiEvents;
import com.aie.arc.rbac.api.publishedevent.MicroServiceEvents;
import com.aie.arc.rbac.api.request.ApiRequest;
import com.aie.arc.rbac.api.request.MicroServiceRequest;
import com.aie.arc.rbac.api.response.ApiDetail;
import com.aie.arc.rbac.api.response.ApiDetails;
import com.aie.arc.rbac.api.response.MicroServiceDetail;
import com.aie.arc.rbac.api.response.MicroServiceDetails;
import com.aie.arc.rbac.impl.command.ApiCommand;
import com.aie.arc.rbac.impl.command.MicroServiceCommand;
import com.aie.arc.rbac.impl.entity.entities.Api;
import com.aie.arc.rbac.impl.entity.entities.MicroService;
import com.aie.arc.rbac.impl.entity.entities.RBACLocalizedFieldsVO;
import com.aie.arc.rbac.impl.event.ApiEvent;
import com.aie.arc.rbac.impl.event.MicroServiceEvent;
import com.aie.arc.rbac.impl.mapper.RBACMapper;
import com.aie.arc.rbac.impl.repository.MicroServiceRepository;
import com.aie.arc.rbac.impl.service.EventPublisher;
import com.aie.arc.rbac.impl.service.RBACServiceImplCommon;
import com.aie.arc.rbac.impl.service.RBACServiceRequestValidation;
import com.lightbend.lagom.javadsl.api.broker.Topic;
import com.lightbend.lagom.javadsl.api.transport.NotFound;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import com.lightbend.lagom.javadsl.broker.TopicProducer;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static com.aie.arc.account.impl.util.MessageConstants.SUCCESS_STATUS;
import static com.aie.arc.rbac.impl.entity.entities.Api.updateApiByRequest;

@Singleton
@Slf4j
public class ApiManager {

    @Inject
    private PersistentEntityRegistry registry;

    @Inject
    private RBACServiceRequestValidation rbacServiceValidation;

    @Inject
    private RBACServiceImplCommon rbacServiceImplCommon;

    @Inject
    private LocaleRepository localeRepository;

    @Inject
    private AccountServiceImplCommon accountServiceCommon;

    @Inject
    private AccountServiceValidation accountServiceValidation;

    @Inject
    private MicroServiceRepository microServiceRepository;

    @Inject
    private EventPublisher eventPublisher;

    @Inject
    private FilterValidation filterValidation ;

    /**
     * Create a MicroService.
     *
     * @return a success response if the request is correct and MicroService is created,
     * throws a ParameterRequired exception if any error in the request.
     */
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> createMicroService(MicroServiceRequest request ) {
            rbacServiceValidation.validateJsonRequest(request);
            String id = rbacServiceImplCommon.getNewId();
            EntityRef<MicroServiceCommand> ref = rbacServiceImplCommon.getEntityRefForMicroService(id);
            List<String> microServiceApiIds = new ArrayList<>();
            Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap = rbacServiceValidation.validateLocaleCodesAndGetVoMap(
                    request.getLocalization(), localeRepository);
            MicroService microService = MicroService.createMicroServiceByRequest(id, localizedFieldsVOMap,
                    microServiceApiIds, request.getActive());
            return ref.<Done>ask(replyTo -> new MicroServiceCommand.CreateMicroService(microService,replyTo),ServiceConstants.ASK_TIMEOUT)
                    .thenApply(done -> rbacServiceValidation.sendResponseByStatusIdAndRequest(id,
                            StatusCodes.CREATED.intValue(), RBACURIConstants.CREATE_MICRO_SERVICE_URI, RBACURIConstants.
                                    MICRO_SERVICE_CREATED_MESSAGE, request));
    }

    /**
     * Update an existing MicroService and return success response or throw exception in case of error in request.
     *
     * @param microServiceId a unique identifier for the MicroService to be updated.
     * @return a success response if the request is correct and the MicroService has been updated,
     * * throws a ParameterRequired exception if any error in the request.
     */

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> updateMicroService(MicroServiceRequest request,
                                                                                  String microServiceId) {
            rbacServiceValidation.validateJsonRequest(request);
            EntityRef<MicroServiceCommand> ref = rbacServiceImplCommon.getEntityRefForMicroService(microServiceId);
            MicroService microService = rbacServiceValidation.validateAndGetMicroServiceById(microServiceId);
            List<String> microServiceApiIds = new ArrayList<>();
            microService = MicroService.updateMicroServiceByRequest(microService, microServiceApiIds, request);
            MicroService finalMicroService = microService;
            return ref.<Done>ask(replyTo -> new MicroServiceCommand.UpdateMicroService(finalMicroService,replyTo),ServiceConstants.ASK_TIMEOUT)
                    .thenApply(done -> rbacServiceValidation.sendResponseByStatusIdAndRequest(microServiceId, SUCCESS_STATUS,
                            RBACURIConstants.UPDATE_MICRO_SERVICE_URI, RBACURIConstants.MICRO_SERVICE_UPDATED_MESSAGE, request));
    }

    public CompletionStage<Pair<ResponseHeader, MicroServiceDetail>>  getMicroService(String microServiceId) {
            rbacServiceValidation.validateMicroServiceId(microServiceId);
            return rbacServiceImplCommon.getEntityRefForMicroService(microServiceId).ask(MicroServiceCommand.
                    GetMicroService::new, ServiceConstants.ASK_TIMEOUT).thenApply(microService ->Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS),
                    RBACMapper.toMicroServiceDetails(microService.get())));
    }

    public CompletionStage<Pair<ResponseHeader, MicroServiceDetails>> getMicroServices(SearchRequest request) {
            accountServiceValidation.validateJsonRequest(request);
            filterValidation.validateFilterParameter(request.getPagination().getPageNo(),
                    request.getPagination().getPageSize(), request.getFilters(), FilterConfig.getMicroServiceTypeMap(), request.getSorting().getField());
            FilterParameter filterParameter = FilterParameter.getNewInstance(Integer.valueOf(request.getPagination().getPageNo()),
                    Integer.valueOf(request.getPagination().getPageSize()), request.getSorting().getAscending(), request.getSorting().getField(), request.getFilters());
            return microServiceRepository.getMicroServiceIds(filterParameter).thenApply(
                    listWrapper -> {
                        MicroServiceDetails obj = new MicroServiceDetails(
                                listWrapper.getList().stream()
                                        .map(rbacServiceValidation::validateAndGetMicroServiceById)
                                        .map(RBACMapper::toMicroServiceDetails)
                                        .collect(Collectors.toList()), listWrapper.getTotalSize());
                        return Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), obj);
                    }
            );
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> activateMicroService(ActivationRequest request, String id) {
            if (request.getValue()) {
                return rbacServiceValidation.validateAndActivateMicroService(id, request);
            }
            return rbacServiceValidation.validateAndDeactivateMicroService(id, request);
    }


    public CompletionStage<Pair<ResponseHeader, ApiDetail>>  getApi(String microServiceAId, String apiId) {
            rbacServiceValidation.validateMicroServiceId(microServiceAId);
            rbacServiceValidation.validateApiId(apiId);
            return rbacServiceImplCommon.getEntityRefForApi(apiId).ask(ApiCommand.GetAPI::new,ServiceConstants.ASK_TIMEOUT).thenApply
                    (microServiceApi -> Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), RBACMapper.
                            toMicroServiceApiDetails(microServiceApi.get())));
    }

    /**
     * Create a Api for given MicroService.
     *
     * @return a success response if the request is correct and Api is created,
     * throws a ParameterRequired exception if any error in the request.
     */
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> addApiToMicroService(ApiRequest request, String microServiceId) {
            rbacServiceValidation.validateJsonRequest(request);
            String id = rbacServiceImplCommon.getNewId();
            EntityRef<ApiCommand> ref = rbacServiceImplCommon.getEntityRefForApi(id);
            Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap = rbacServiceValidation.validateLocaleCodesAndGetVoMap(
                    request.getLocalization(), localeRepository);
            MicroService microService = rbacServiceValidation.validateAndGetMicroServiceById(microServiceId);
            Api api = Api.createApiByRequest(id, localizedFieldsVOMap, request.getActive());
            return ref.<Done>ask(replyTo -> new ApiCommand.AddAPI(api,replyTo), ServiceConstants.ASK_TIMEOUT).thenCompose(done -> rbacServiceImplCommon.
                    getEntityRefForMicroService(microService.getMicroServiceId()).<Done>ask(replyTo -> new MicroServiceCommand.
                    AddApiToMicroService(microServiceId, api.getId(), localizedFieldsVOMap, api.isActive(),replyTo),ServiceConstants.ASK_TIMEOUT)
                    .thenApply(done1 -> rbacServiceValidation.sendResponseByStatusIdAndRequest(id,
                            StatusCodes.CREATED.intValue(), RBACURIConstants.ADD_API_TO_MICRO_SERVICE_URI, RBACURIConstants.
                                    ADD_API_CREATED_MESSAGE, request)));
    }

    /**
     * Update a Api for given MicroService.
     *
     * @return a success response if the request is correct and Api is updated,
     * <p>
     * throws a ParameterRequired exception if any error in the request.
     */
    public CompletionStage<Pair<ResponseHeader, BaseResponse>>  updateApiToMicroService(ApiRequest request,
                                                                                        String microServiceId, String apiId) {
            rbacServiceValidation.validateJsonRequest(request);
            MicroService microService = rbacServiceValidation.validateAndGetMicroServiceById(microServiceId);
            if (!microService.getApiIds().contains(apiId)) {
                throw new NotFound("Api Ids don't belong to the given MicroService : " + microServiceId);
            }
            Api api = rbacServiceValidation.validateAndGetApiById(apiId);
            Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap = rbacServiceValidation.validateLocaleCodesAndGetVoMap
                    (request.getLocalization(), localeRepository);
            api = updateApiByRequest(api, request);
            Api finalApi = api;
            return rbacServiceImplCommon.getEntityRefForApi(apiId).<Done>ask(replyTo -> new ApiCommand.UpdateAPI(finalApi,replyTo),ServiceConstants.ASK_TIMEOUT).thenCompose(
                    done -> rbacServiceImplCommon.getEntityRefForMicroService(microService.getMicroServiceId()).<Done>ask(replyTo ->
                            new MicroServiceCommand.UpdateApiToMicroService(microServiceId, finalApi.getId(),
                                    localizedFieldsVOMap, finalApi.isActive(),replyTo),ServiceConstants.ASK_TIMEOUT)
                    .thenApply(done1 -> rbacServiceValidation.sendResponseByStatusIdAndRequest(apiId, SUCCESS_STATUS,
                            RBACURIConstants.UPDATE_API_IN_MICRO_SERVICE_URI, RBACURIConstants.ADD_API_UPDATED_MESSAGE, request)));
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> activateApi(ActivationRequest request,String id) {
            if (request.getValue()) {
                return rbacServiceValidation.validateAndActivateApi(id, request);
            }
            return rbacServiceValidation.validateAndDeactivateApi(id, request);
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> activateApiFromMicroService(ActivationRequest request,
                                                                                           String microServiceId, String id) {
            rbacServiceValidation.validateMicroServiceId(microServiceId);
            if (request.getValue()) {
                return rbacServiceValidation.validateAndActivateApiToMicroService(microServiceId, id, request);
            }
            return rbacServiceValidation.validateAndDeactivateApiInMicroService(microServiceId, id, request);
    }

    public CompletionStage<Pair<ResponseHeader, ApiDetails>> getApisInMicroService(SearchRequest request,String microServiceId) {
            accountServiceValidation.validateJsonRequest(request);
            MicroService microService = rbacServiceValidation.validateAndGetMicroServiceById(microServiceId);
            filterValidation.validateFilterParameter(request.getPagination().getPageNo(),
                    request.getPagination().getPageSize(), request.getFilters(), FilterConfig.getApiFieldTypeMap(), request.getSorting().getField());
            FilterParameter filterParameter = FilterParameter.getNewInstance(Integer.valueOf(request.getPagination().getPageNo()),
                    Integer.valueOf(request.getPagination().getPageSize()), request.getSorting().getAscending(), request.getSorting().getField(), request.getFilters());
            return microServiceRepository.getApiIds(filterParameter, microService.getApiIds()).thenApply(
                    listWrapper -> {
                        ApiDetails obj = new ApiDetails(
                                listWrapper.getList().stream()
                                        .map(rbacServiceValidation::validateAndGetApiById)
                                        .map(RBACMapper::toMicroServiceApiDetails)
                                        .collect(Collectors.toList()), listWrapper.getTotalSize());
                        return Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), obj);
                    }
            );
    }

    /*MicroService event publish*/
    public Topic<MicroServiceEvents> microServicePublishedEvent() {
        return TopicProducer.taggedStreamWithOffset(MicroServiceEvent.TAG.allTags(),
                (tag, offset) -> registry.eventStream(tag, offset)
                        .filter(p -> p.first().isPublic())
                        .mapAsync(1, eventAndOffset ->
                                eventPublisher.convertPublishedEventForMicroService(eventAndOffset.first()).thenApply(event ->
                                        Pair.create(event, eventAndOffset.second()))));
    }

    /*Api event publish*/
    public Topic<ApiEvents> apiPublishedEvent() {
        return TopicProducer.taggedStreamWithOffset(ApiEvent.TAG.allTags(),
                (tag, offset) -> registry.eventStream(tag, offset)
                        .filter(p -> p.first().isPublic())
                        .mapAsync(1, eventAndOffset ->
                                eventPublisher.convertPublishedEventForApi(eventAndOffset.first()).thenApply(event ->
                                        Pair.create(event, eventAndOffset.second()))));
    }



}
