package com.aie.arc.account.impl.service;

import akka.Done;
import akka.cluster.sharding.typed.javadsl.EntityRef;
import akka.http.javadsl.model.StatusCodes;
import akka.japi.Pair;
import com.aie.arc.account.api.common.request.ActivationRequest;
import com.aie.arc.account.api.common.request.SearchRequest;
import com.aie.arc.account.api.common.response.BaseResponse;
import com.aie.arc.account.api.publishedevent.BusinessUserEvents;
import com.aie.arc.account.api.request.CreateBusinessUserAddressRequest;
import com.aie.arc.account.api.request.UpdateBusinessUserAddressRequest;
import com.aie.arc.account.api.request.UpdateBusinessUserRequest;
import com.aie.arc.account.api.response.*;
import com.aie.arc.account.impl.command.BusinessAddressCommand;
import com.aie.arc.account.impl.command.BusinessUserCommand;
import com.aie.arc.account.impl.entity.entities.BusinessAddress;
import com.aie.arc.account.impl.entity.entities.BusinessUser;
import com.aie.arc.account.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.account.impl.entity.entities.vo.common.FilterParameter;
import com.aie.arc.account.impl.event.BusinessUserEvent;
import com.aie.arc.account.impl.mapper.Mapper;
import com.aie.arc.account.impl.repository.AddressRepository;
import com.aie.arc.account.impl.repository.BusinessAddressRepository;
import com.aie.arc.account.impl.repository.BusinessUserRepository;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.aie.arc.account.impl.util.common.FilterConfig;
import com.aie.arc.rbac.api.response.APITagDetail;
import com.aie.arc.rbac.impl.mapper.RBACMapper;
import com.aie.arc.rbac.impl.service.RBACServiceRequestValidation;
import com.lightbend.lagom.javadsl.api.broker.Topic;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import com.lightbend.lagom.javadsl.broker.TopicProducer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.aie.arc.account.api.constants.AccountURIConstants.*;
import static com.aie.arc.account.impl.util.MessageConstants.SUCCESS_STATUS;

@Singleton
@Slf4j
public class BusinessManager {
    @Inject
    private  AccountServiceImplCommon accountServiceCommon;
    @Inject
    private AccountServiceValidation accountServiceValidation;
    @Inject
    private BusinessUserRepository businessUserRepository;
    @Inject
    private BusinessAddressRepository businessAddressRepository;
    @Inject
    private IntegrationServiceManager serviceManager;
    @Inject
    private AccountEventPublisher accountEventPublisher;
    @Inject
    private FilterValidation filterValidation;
    @Inject
    private AddressRepository addressRepository;
    @Inject
    private RolesManager rolesManager;
    @Inject
    private RBACServiceRequestValidation rbacServiceRequestValidation;



    public CompletionStage<Pair<ResponseHeader, BaseResponse>> activateBusinessUser(ActivationRequest request, String id) {
        if (request.getValue()) {
            return accountServiceValidation.validateAndActivateBusinessUser(id, request);
        }
        return accountServiceValidation.validateAndDeactivateBusinessUser(id, request);
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> updateBusinessUser(UpdateBusinessUserRequest
                                                                                             request, String businessUserId) {
        accountServiceValidation.validateJsonRequest(request);
        BusinessUser businessUser = accountServiceValidation.validateAndGetBusinessUserById(businessUserId);
        if(request.getRoleIds()!= null){
            rbacServiceRequestValidation.validateRoleIds(request.getRoleIds());
        }
        BusinessUser updateBusinessUser = BusinessUser.updateBusinessUser(businessUser, request);
        return accountServiceCommon.getEntityRefForBusinessUser(businessUserId).<Done>ask(replyTo->new BusinessUserCommand.UpdateBusinessUser(businessUserId, updateBusinessUser, replyTo),ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> accountServiceValidation.sendResponseByStatusIdAndRequest(businessUserId,
                        SUCCESS_STATUS, UPDATE_BUSINESS_USER_URI, BUSINESS_USER_UPDATED_MESSAGE, request));
    }

    /**
     * @param businessUserId a unique identifier for the BusinessUserDetail to be retrieved.
     *                       .
     */
    public CompletionStage<Pair<ResponseHeader, BusinessUserDetail>> getBusinessUser(String businessUserId) {
        log.debug("getBusinessUser for id:{}",businessUserId);
        BusinessUser user = accountServiceValidation.validateAndGetBusinessUserById(businessUserId);
            return accountServiceCommon.getEntityRefForBusinessUser(businessUserId).ask(BusinessUserCommand.
                    GetBusinessUser::new, ServiceConstants.ASK_TIMEOUT).thenApply(businessUser -> Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS),
                    Mapper.toBusinessUserDetails(businessUser.get(),
                            user.getRoleIds().stream().filter(Objects::nonNull)
                                    .map(rbacServiceRequestValidation::validateAndGetRoleById)
                                    .map(role -> RBACMapper.toRoleDetails(role))
                                    .collect(Collectors.toList()))));
        }


    public CompletionStage<Pair<ResponseHeader, GetBusinessUserDetail>> getBusinessUserDetails(String businessUserId) {
        log.debug("getBusinessUser for id:{}",businessUserId);
        BusinessUser user = accountServiceValidation.validateAndGetBusinessUserById(businessUserId);
            return accountServiceCommon.getEntityRefForBusinessUser(businessUserId).ask(BusinessUserCommand.
                    GetBusinessUser::new, ServiceConstants.ASK_TIMEOUT).thenApply(businessUser -> Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS),
                    Mapper.toGetBusinessUserDetails(businessUser.get(),
                            user.getRoleIds().stream().filter(Objects::nonNull)
                                    .map(rbacServiceRequestValidation::validateAndGetRoleById)
                                    .map(role -> RBACMapper.toGetRoleDetails(role,
                                            role.getPermissionIds().stream().filter(Objects::nonNull)
                                                    .map(rbacServiceRequestValidation::validateAndGetPermissionById)
                                                    .map(permission -> RBACMapper.toGetPermissionDetails(permission,
                                                            RBACMapper.toWorkspaceDetails(rbacServiceRequestValidation.validateAndGetWorkspaceById(permission.getWorkspaceId())
                                                            ))).collect(Collectors.toList())))
                                    .collect(Collectors.toList()))));

    }


    /**
     * @param businessUserId a unique identifier for the BusinessUserDetail to be retrieved.
     *                      .
     */
    public CompletionStage<Pair<ResponseHeader, APITagDetail>> getBusinessUserApis(String businessUserId) {
        log.debug("GetBusinessUser API list for id:{}",businessUserId);
        BusinessUser user = accountServiceValidation.validateAndGetBusinessUserById(businessUserId);
        return accountServiceCommon.getEntityRefForBusinessUser(businessUserId).ask(BusinessUserCommand.
                GetBusinessUser::new,ServiceConstants.ASK_TIMEOUT).thenApply(businessUser -> Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS),
                RBACMapper.toBusinessUserAPIDetails(rolesManager.getApiList(user.getRoleIds()))));

    }


    public CompletionStage<Pair<ResponseHeader, BusinessUserDetails>> getBusinessUsers(SearchRequest request) {
        log.debug("Get All BusinessUser API  request is:{}",request);
        accountServiceValidation.validateJsonRequest(request);
        filterValidation.validateFilterParameter(request.getPagination().getPageNo(),
                request.getPagination().getPageSize(), request.getFilters(), FilterConfig.getBusinessUserTypeMap(), request.getSorting().getField());
        FilterParameter filterParameter = FilterParameter.getNewInstance(Integer.valueOf(request.getPagination().getPageNo()),
                Integer.valueOf(request.getPagination().getPageSize()), request.getSorting().getAscending(), request.getSorting().getField(), request.getFilters());
        return businessUserRepository.getBusinessUserIds(filterParameter).thenApply(
                listWrapper -> {
                    BusinessUserDetails obj = new BusinessUserDetails(
                            listWrapper.getList().stream().map(accountServiceValidation::validateAndGetBusinessUserById)
                                    .map(businessUser -> Mapper.toBusinessUserDetails(businessUser,
                                            businessUser.getRoleIds().stream().filter(Objects::nonNull)
                                                    .map(rbacServiceRequestValidation::validateAndGetRoleById)
                                                    .map(role -> RBACMapper.toRoleDetails(role)).collect(Collectors.toList())))
                                    .collect(Collectors.toList()), listWrapper.getTotalSize());
                    return Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), obj);
                }
        );
    }

    /*Business User event publish*/
    public Topic<BusinessUserEvents> businessUserPublishedEvent() {
        return TopicProducer.taggedStreamWithOffset(BusinessUserEvent.TAG.allTags(),
                (tag, offset) -> accountServiceCommon.registry.eventStream(tag, offset)
                        .filter(p -> p.first().isPublic() && p.first() instanceof BusinessUserEvent.BusinessUserUpdated ||
                                p.first() instanceof BusinessUserEvent.BusinessUserActivated || p.first() instanceof BusinessUserEvent.BusinessUserDeactivated)
                        .mapAsync(1, eventAndOffset ->
                                accountEventPublisher.convertPublishedEventForBusinessUser(eventAndOffset.first()).thenApply(event ->
                                        Pair.create(event, eventAndOffset.second()))));
    }

}
