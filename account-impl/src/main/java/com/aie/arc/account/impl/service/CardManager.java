package com.aie.arc.account.impl.service;

import akka.Done;
import akka.http.javadsl.model.StatusCodes;
import akka.japi.Pair;
import com.aie.arc.account.api.common.exceptions.*;
import com.aie.arc.account.api.common.request.ActivationRequest;
import com.aie.arc.account.api.common.response.BaseResponse;
import com.aie.arc.account.api.email.EmailRequest;
import com.aie.arc.account.api.email.Personalizations;
import com.aie.arc.account.api.email.To;
import com.aie.arc.account.api.publishedevent.CardEvents;
import com.aie.arc.account.api.request.CardRequest;
import com.aie.arc.account.api.request.UpdateCardRequest;
import com.aie.arc.account.api.request.brandsmart.FirstDataResponse;
import com.aie.arc.account.api.response.CardDetail;
import com.aie.arc.account.api.response.PaymentDetails;
import com.aie.arc.account.impl.command.AccountCommand;
import com.aie.arc.account.impl.command.CardCommand;
import com.aie.arc.account.impl.email.EmailService;
import com.aie.arc.account.impl.encryption.DESEncryptor;
import com.aie.arc.account.impl.entity.entities.Account;
import com.aie.arc.account.impl.entity.entities.Card;
import com.aie.arc.account.impl.entity.entities.vo.Profile;
import com.aie.arc.account.impl.event.CardEvent;
import com.aie.arc.account.impl.mapper.Mapper;
import com.aie.arc.account.impl.repository.CardRepository;
import com.aie.arc.account.impl.util.MessageConstants;
import com.lightbend.lagom.javadsl.api.broker.Topic;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import com.lightbend.lagom.javadsl.broker.TopicProducer;
import com.typesafe.config.Config;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.aie.arc.account.api.constants.AccountURIConstants.*;
import static com.aie.arc.account.impl.util.MessageConstants.*;
import static com.aie.arc.account.impl.util.ServiceConstants.*;

@Singleton
@Slf4j
public class CardManager {

    private final DESEncryptor desEnc = new DESEncryptor();

    public static final Pattern BM_NUMBER_PATTERN = Pattern.compile("[0-9]{16}");

    @Inject
    private AccountServiceImplCommon accountServiceCommon;

    @Inject
    private AccountServiceValidation accountServiceValidation;

    @Inject
    private CardRepository cardRepository;

    @Inject
    private IntegrationServiceManager integrationServiceManager;


    @Inject
    private CompassClientHelper campassClientHelper;

    @Inject
    private EmailService emailService;

    @Inject
    private AccountEventPublisher accountEventPublisher;
    @Inject
    private Config config;

    private String cardChangeTemplateId;


    public CompletionStage<Pair<ResponseHeader, BaseResponse>> addCard(CardRequest request, String accountId,
                                                                       String migrateCardId, String migrateEncryptCardNumber,
                                                                       String migrateSubscriptionId, String paymentNickName) {
        log.debug("Add card to accountId: {}", accountId);
        accountServiceValidation.validateJsonRequest(request);
        Boolean cardExists = accountServiceValidation.validateAndGetAccountById(accountId).thenApply(account -> {
            Boolean flag = false;
            if (migrateEncryptCardNumber == null) {
                validateCard(request, accountId);
                String cardNickName = getCardNickname(request.getCardNumber(), request.getCardType());
                flag = cardRepository.cardExist(cardNickName, accountId).toCompletableFuture().join();
            }
            return flag;
        }).toCompletableFuture().join();
        if(cardExists) throw new DuplicateCardException(new String[]{"[Error210]: Credit Card already exists for this profile"});
        if (migrateEncryptCardNumber == null) {
            String cardNickName = getCardNickname(request.getCardNumber(), request.getCardType());
            if (!StringUtils.equalsIgnoreCase(CARD_TYPE_BM, request.getCardType())) {
                return integrationServiceManager.compassSubscriptionCall(request)
                        .thenCompose(firstDataResponse -> handleCompassSubscriptionResponse(
                                request, accountId, cardNickName, firstDataResponse));
            }
            return createCard(request, accountId, accountServiceCommon.getNewId(),
                    encryptPaymentNumber(request.getCardNumber()), null, cardNickName);

        } else {
            return createCard(request, accountId, migrateCardId, migrateEncryptCardNumber,
                    migrateSubscriptionId, paymentNickName);
        }
    }

    private CompletionStage<Pair<ResponseHeader, BaseResponse>> handleCompassSubscriptionResponse(
            CardRequest request, String accountId, String cardNickName, FirstDataResponse firstDataResponse) {
        log.debug("reasonCode returned is : {} ", firstDataResponse.getResponseReasonCode());
        String reasonCode = firstDataResponse.getResponseReasonCode();
        if (!Arrays.asList("100", "105", "106").contains(reasonCode)) {
            log.info("Reason code received from external token is invalid. Returning response with appropriate message.");
            return CompletableFuture.completedFuture(accountServiceValidation.
                    sendResponseByStatusIdAndRequest(accountServiceCommon.getNewId(), STATUS_BAD_REQUEST,
                            ADD_CARD_URI, "common: " + reasonCode + ": " +
                                    config.getString("ERROR_" + reasonCode), request));

        }
        return createCard(request, accountId, accountServiceCommon.getNewId(),
                encryptPaymentNumber(request.getCardNumber()), firstDataResponse.getAccountNumber(), cardNickName);
    }

    private CompletionStage<Pair<ResponseHeader, BaseResponse>> createCard(CardRequest request, String accountId,
                                                                           String cardId, String encryptCardNumber,
                                                                           String subscriptionId, String cardNickName) {
        Card card = Card.createCardByRequest(cardId, accountId, encryptCardNumber, subscriptionId, cardNickName, request);
        return accountServiceCommon.getEntityRefForCard(cardId)
                .<Done>ask(replyTo -> new CardCommand.AddCard(card, replyTo), ASK_TIMEOUT)
                .thenCompose(linkCardToAccount -> accountServiceCommon.getEntityRefForAccount(accountId)
                        .<Done>ask(replyTo -> new AccountCommand.AddCardToAccount(accountId, cardId,
                                request.isDefaultCreditCard(), StringUtils.isNotBlank(cardId), replyTo), ASK_TIMEOUT)
                        .thenApply(response -> accountServiceValidation.sendResponseByStatusIdAndRequest(card.getCardId(),
                                StatusCodes.CREATED.intValue(), ADD_CARD_URI, CARD_CREATED_MESSAGE, request)));

    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> updateCard(UpdateCardRequest request,
                                                                          String accountId, String cardId,
                                                                          boolean isMigrationEvt) {
        log.debug("Update card with cardId: {} to accountId: {}", cardId, accountId);
        accountServiceValidation.validateJsonRequest(request);
        return accountServiceValidation.validateAndGetAccountById(accountId)
                .thenCompose(optionalAccount -> {
                    Account account = optionalAccount.get();
                    return accountServiceValidation.validateAndGetCardById(cardId)
                            .thenCompose(optionalCard -> {
                                if (!optionalCard.get().getCardType().equalsIgnoreCase(CARD_TYPE_BM)) {
                                    validateCardExpiryDate(request.getExpirationMonth(), request.getExpirationYear());
                                }
                                Card updateCard = Card.updateCardByRequest(optionalCard.get(), request);
                                accountServiceCommon.getEntityRefForCard(optionalCard.get().getCardId())
                                        .<Done>ask(replyTo -> new CardCommand.UpdateCard(updateCard, replyTo), ASK_TIMEOUT)
                                        .thenCompose(linkCardToAccount -> accountServiceCommon.getEntityRefForAccount(account.getAccountId())
                                                .<Done>ask(replyTo -> new AccountCommand.UpdateCardToAccount(
                                                                accountId, optionalCard.get().getCardId(), request.isDefaultCreditCard(), isMigrationEvt, replyTo),
                                                        ASK_TIMEOUT)).toCompletableFuture().join();
                                if (config.getBoolean(MessageConstants.ENABLE_ARC_EMAIL)) {
                                    sendCardUpdateEmail(account.getProfile());
                                }
                                return CompletableFuture.completedFuture(accountServiceValidation.sendResponseByStatusIdAndRequest(cardId,
                                        SUCCESS_STATUS, UPDATE_CARD_URI, CARD_UPDATED_MESSAGE, request));
                            });

                });

    }

    public CompletionStage<Pair<ResponseHeader, PaymentDetails>> getAccountCards(String accountId) {
        log.debug("Get all account cards with accountId: {}", accountId);
        return accountServiceValidation.validateAndGetAccountById(accountId)
                .thenApply(optionalAccount -> {
                    Account account = optionalAccount.get();
                    return Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS),
                            Mapper.toPaymentDetails(
                                    account.getDefaultCardId(),
                                    account.getCardIds().stream().filter(Objects::nonNull)
                                            .map(accountServiceValidation::validateAndGetCardById)
                                            .map(card -> Mapper.toCardDetails(card.toCompletableFuture().join().get(), account.getDefaultCardId()))
                                            .collect(Collectors.toList())));
                });

    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> activateCard(
            ActivationRequest request, String accountId, String cardId, boolean isMigrationEvt) {
        log.debug("Activate card with cardId: {} to accountId: {} using activation flag : {}", cardId, accountId,
                request.getValue());
        if (request.getValue()) {
            return accountServiceValidation.validateAndActivateCard(cardId, request);
        }
        return accountServiceValidation.validateAndDeactivateCard(cardId, accountId, request, isMigrationEvt);
    }


    public CompletionStage<Pair<ResponseHeader, BaseResponse>> setDefaultPayment(String accountId, String cardId) {
        log.debug("Set default Payment with : {} accountId: {} , cardId : {}", accountId, cardId);
        return accountServiceValidation.validateAndGetCardById(cardId)
                .thenCompose(optionalCard ->
                        accountServiceValidation.validateAndGetAccountById(accountId)
                                .thenCompose(optionalAccount -> {
                                    Account account = optionalAccount.get();
                                    if (!account.getCardIds().contains(cardId)) {
                                        throw new ParameterRequired(new String[]{"cardId: " + "(" + cardId + ") is not valid Card"});
                                    }
                                    return accountServiceCommon.getEntityRefForAccount(account.getAccountId())
                                            .<Done>ask(replyTo -> new AccountCommand.SetDefaultPayment(account.getAccountId(), cardId, replyTo),
                                                    ASK_TIMEOUT)
                                            .thenApply(response -> accountServiceValidation.sendResponseByStatusId(
                                                    SUCCESS_STATUS, DEFAULT_PAYMENT_URI, DEFAULT_PAYMENT_MESSAGE));
                                })
                );

    }

    public CompletionStage<Pair<ResponseHeader, CardDetail>> getCard(String accountId, String cardId) {
        log.debug("Get card with cardId: {} to accountId: {}", cardId, accountId);
        return accountServiceValidation.validateAndGetCardById(cardId)
                .thenCompose(optionalCard ->
                        accountServiceValidation.validateAndGetAccountById(accountId)
                                .thenApply(optionalAccount ->
                                        Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS),
                                                Mapper.toCardDetails(optionalCard.get(),
                                                        optionalAccount.get().getDefaultCardId()))
                                )
                );
    }


    public CompletionStage<Pair<ResponseHeader, CardDetail>> getDefaultPayment(String accountId) {
        log.debug("Get Default Payment with accountId: {}", accountId);
        return accountServiceValidation.validateAndGetAccountById(accountId)
                .thenCompose(optionalAccount -> {
                    Account account = optionalAccount.get();
                    return accountServiceCommon.getEntityRefForCard(account.getDefaultCardId()).ask(CardCommand.GetCard::new, ASK_TIMEOUT)
                            .thenApply(card -> Pair.create(
                                    ResponseHeader.OK.withStatus(SUCCESS_STATUS),
                                    Mapper.toCardDetails(card.get(), account.getDefaultCardId())));
                });

    }

    private String encryptPaymentNumber(String paymentNumber) {
        log.debug("Start : Encrypt Payment Number");
        String encryptedCardNumber = null;
        try {
            encryptedCardNumber = desEnc.encrypt(paymentNumber);
        } catch (EncryptorException e) {
            log.error("Unable to Encrypt card number");
            throw new DuplicateCardException(new String[]{"Unable to encrypt card number"});
        }
        log.debug("End : Encrypt Payment Number");
        return encryptedCardNumber;
    }

    private String getCardNickname(String paymentNumber, String paymentType) {
        return new StringBuffer(accountServiceCommon.getCardNickNameMap().get(paymentType))
                .append(NICK_NAME_ENDING_WITH)
                .append(StringUtils.isNotBlank(paymentNumber) && paymentNumber.trim().length() > FOUR
                        ? StringUtils.substring(paymentNumber, paymentNumber.trim().length() - FOUR) : null)
                .toString();
    }

    private void validateCardExpiryDate(String month, String year) {
        log.debug("Card Expiry Date Validation");
        int expirationMonth;
        int expirationYear;
        try {
            expirationMonth = Integer.valueOf(month);
            expirationYear = Integer.valueOf(year);
        } catch (NumberFormatException ne) {
            throw new ParameterRequired(new String[]{"[Error334]: Please enter only integers for date."});
        }
        if (expirationYear < LocalDateTime.now().getYear()
                || (expirationYear == LocalDateTime.now().getYear()
                && expirationMonth < LocalDateTime.now().getMonthValue()))
            throw new ParameterRequired(new String[]{"[Error208]: This card has expired. " +
                    "Please update the expiration date."});
    }

    private void validateCard(CardRequest request, String accountId) {
        log.debug("Card Validation for accountId {}", accountId);
        if (!accountServiceCommon.getSupportedCards().contains(request.getCardType())) {
            log.error("card Type : {} not supported", request.getCardType());
            throw new DuplicateCardException(new String[]{
                    "Card type : " + request.getCardType() + " not supported : " + accountId + "!"});
        }

        validateCardNumberLength(request.getCardType(), request.getCardNumber());
        if (!request.getCardType().equalsIgnoreCase(CARD_TYPE_BM)) {
            if (!GenericValidator.isCreditCard(request.getCardNumber())) {
                log.error("The card number is invalid, failed in commons-validator");
                throw new ParameterRequired(new String[]{CARD_FAILURE_MESSAGE});
            }
            validateCardNumberPrefix(request.getCardType(), request.getCardNumber());
            validateCardExpiryDate(request.getExpirationMonth(), request.getExpirationYear());
        } else {
            validateBMCardNumber(request.getCardNumber());
        }
    }

    private void validateCardNumberLength(String cardType, String cardNumber) {
        if (MapUtils.isNotEmpty(accountServiceCommon.getCardLengthsMap())
                && !CollectionUtils.containsAny(
                accountServiceCommon.getCardLengthsMap().get(cardType), String.valueOf(cardNumber.length()))) {
            log.error("cardType: {} supported with cardNumber length : {}", cardType,
                    accountServiceCommon.getCardLengthsMap().get(cardType));
            throw new ParameterRequired(new String[]{CARD_FAILURE_MESSAGE});
        }
    }

    private void validateCardNumberPrefix(String cardType, String cardNumber) {
        if (MapUtils.isNotEmpty(accountServiceCommon.getCardPrefixesMap())
                && accountServiceCommon.getCardPrefixesMap().get(cardType).stream()
                .noneMatch(prefix -> StringUtils.startsWith(cardNumber, prefix))) {
            log.error("cardType: {} supported with cardNumber startWith : {}", cardType,
                    accountServiceCommon.getCardPrefixesMap().get(cardType));
            throw new ParameterRequired(new String[]{CARD_FAILURE_MESSAGE});
        }
    }

    private void validateBMCardNumber(String cardNumber) {
        if (BM_NUMBER_PATTERN.matcher(cardNumber).matches()) {
            int checkDigit = Integer.parseInt(cardNumber.substring(cardNumber.length() - 1));
            int resultCheckDigit = getCheckSumValue(
                    getMultipliedNumber(
                            convertStringToIntArray(
                                    getReverseWithoutCheckNumber(cardNumber))));
            if (resultCheckDigit == checkDigit) {
                return;
            }
        }
        throw new ParameterRequired(new String[]{CARD_FAILURE_MESSAGE});
    }

    private static int[] getMultipliedNumber(int[] reverseNumberArray) {
        int mod = 0;
        for (int i = 0; i < reverseNumberArray.length; i++) {
            mod = (i + 1) % 2;
            if (mod != 0) {
                reverseNumberArray[i] = reverseNumberArray[i] * 2;
                if (reverseNumberArray[i] >= 10) {
                    int second = reverseNumberArray[i] % 10;
                    reverseNumberArray[i] = (reverseNumberArray[i] / 10) + second;
                }
            }
        }
        return reverseNumberArray;
    }

    private static int getCheckSumValue(int[] integerArray) {
        int sum = Arrays.stream(integerArray).sum();
        if (sum > 0) {
            int mod = sum % 10;
            if (mod == 0) {
                sum = mod;
            } else {
                sum = 10 - mod;
            }
        }
        return sum;
    }

    private static String getReverseWithoutCheckNumber(String cardNumber) {
        String withoutCheck = cardNumber.substring(0, cardNumber.length() - 1);
        return (new StringBuffer(withoutCheck)).reverse().toString();
    }

    private static int[] convertStringToIntArray(String cardNumber) {
        int[] intArray = new int[cardNumber.length()];
        for (int i = 0; i < cardNumber.length(); i++) {
            intArray[i] = Character.digit(cardNumber.charAt(i), 10);
        }
        return intArray;
    }

    private void sendCardUpdateEmail(Profile profile) {
        StringWriter writer = new StringWriter();
        try {

            if (cardChangeTemplateId == null) {
                cardChangeTemplateId = config.getConfig(MessageConstants.CONFIG_EMAIL_SERVICE).getString("account.cardchange.email.template.id");
            }

            EmailRequest emailRequest = new EmailRequest();

            emailRequest.setTemplateId(cardChangeTemplateId);
            Personalizations personalizations = new Personalizations();
            personalizations.getContent().put("firstName", profile.getFirstName());
            personalizations.getContent().put("lastName", profile.getLastName());
            To toEmail = new To();
            toEmail.setEmail(profile.getEmail());
            toEmail.setName(new StringBuilder().append(profile.getFirstName()).append(profile.getLastName()).toString());
            personalizations.getTo().add(toEmail);
            personalizations.setSubject(CREDIT_CARD_UPDATE_EMAIL);
            emailRequest.getPersonalizations().add(personalizations);
            emailService.sendMail(emailRequest);

        } catch (Exception e) {
            log.error("Card Update Send Email failed : {}", e.getMessage());
        }
    }

    public Topic<CardEvents> cardPublishedEvent() {
        return TopicProducer.taggedStreamWithOffset(CardEvent.TAG.allTags(),
                (tag, offset) -> accountServiceCommon.registry.eventStream(tag, offset)
                        .filter(p -> p.first().isPublic())
                        .mapAsync(1, eventAndOffset ->
                                accountEventPublisher.convertPublishedEventForCard(eventAndOffset.first())
                                        .thenApply(event -> Pair.create(event, eventAndOffset.second()))));
    }

}
