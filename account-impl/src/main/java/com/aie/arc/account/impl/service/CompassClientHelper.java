package com.aie.arc.account.impl.service;

import akka.japi.Pair;
import com.aie.arc.account.api.common.exceptions.InvalidParameter;
import com.aie.arc.account.api.request.CardRequest;
import com.aie.arc.account.api.request.brandsmart.AdditionalFormats;
import com.aie.arc.account.api.request.brandsmart.FirstDataRequest;
import com.aie.arc.account.api.request.brandsmart.FirstDataResponse;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.concurrent.CompletionStage;

import static com.aie.arc.account.impl.util.MessageConstants.*;

@Singleton
public class CompassClientHelper {

    @Inject
    private IntegrationServiceManager integrationServiceManager;

    @Inject
    private AccountServiceImplCommon accountServiceCommon;

}
