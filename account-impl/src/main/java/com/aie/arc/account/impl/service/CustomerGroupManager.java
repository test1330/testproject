package com.aie.arc.account.impl.service;

import akka.Done;
import akka.NotUsed;
import akka.cluster.sharding.typed.javadsl.EntityRef;
import akka.http.javadsl.model.StatusCodes;
import akka.japi.Pair;
import com.aie.arc.account.api.common.request.ActivationRequest;
import com.aie.arc.account.api.common.request.SearchRequest;
import com.aie.arc.account.api.common.response.BaseResponse;
import com.aie.arc.account.api.publishedevent.CustomerGroupEvents;
import com.aie.arc.account.api.request.CreateCustomerGroupRequest;
import com.aie.arc.account.api.request.UpdateCustomerGroupRequest;
import com.aie.arc.account.api.response.BusinessAddressDetails;
import com.aie.arc.account.api.response.CustomerGroupDetail;
import com.aie.arc.account.api.response.CustomerGroupDetails;
import com.aie.arc.account.impl.command.CustomerGroupCommand;
import com.aie.arc.account.impl.entity.entities.CustomerGroup;
import com.aie.arc.account.impl.entity.entities.vo.LocalizedFields;
import com.aie.arc.account.impl.entity.entities.vo.common.FilterParameter;
import com.aie.arc.account.impl.event.CustomerGroupEvent;
import com.aie.arc.account.impl.repository.BusinessUserRepository;
import com.aie.arc.account.impl.repository.CustomerGroupRepository;
import com.aie.arc.account.impl.repository.repository.LocaleRepository;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.aie.arc.account.impl.util.common.FilterConfig;
import com.aie.arc.rbac.impl.service.EventPublisher;
import com.aie.arc.rbac.impl.service.EventSubscriber;
import com.lightbend.lagom.javadsl.api.broker.Topic;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import com.lightbend.lagom.javadsl.broker.TopicProducer;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRef;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;
import com.lightbend.lagom.javadsl.server.HeaderServiceCall;
import com.aie.arc.account.impl.mapper.Mapper;
import lombok.extern.slf4j.Slf4j;


import static com.aie.arc.account.api.constants.AccountURIConstants.*;


import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;


import static com.aie.arc.account.impl.util.MessageConstants.SUCCESS_STATUS;

@Singleton
@Slf4j
public class CustomerGroupManager {

    @Inject
    private AccountServiceImplCommon accountServiceCommon;
    @Inject
    private AccountServiceValidation accountServiceValidation;
    @Inject
    private BusinessUserRepository businessRepository;
    @Inject
    private IntegrationServiceManager serviceManager;
    @Inject
    private FilterValidation filterValidation;
    @Inject
    private LocaleRepository localeRepository;
    @Inject
    private CustomerGroupRepository customerGroupRepository;
    @Inject
    private AccountEventPublisher accountEventPublisher;



    public CompletionStage<Pair<ResponseHeader, BaseResponse>> createCustomerGroup(CreateCustomerGroupRequest request) {
            accountServiceValidation.validateJsonRequest(request);
            accountServiceValidation.validateAccountIds(request.getAccountIds());
            String id=accountServiceCommon.getNewId();
            Map<String, LocalizedFields> localizedFieldsVOMap = accountServiceValidation.validateLocaleCodesAndGetVoMap
                    (request.getLocalization(), localeRepository);
            CustomerGroup customerGroup = CustomerGroup.createCustomerGroupByRequest(id, localizedFieldsVOMap, request.getAccountIds(), request);
            return accountServiceCommon.getEntityRefForCustomerGroup(id).<Done>ask(replyTo->new CustomerGroupCommand.CreateCustomerGroup(customerGroup,replyTo),ServiceConstants.ASK_TIMEOUT)
                    .thenApply(done -> accountServiceValidation.sendResponseByStatusIdAndRequest(customerGroup.
                            getCustomerGroupId(), StatusCodes.CREATED.intValue(), CREATE_CUSTOMER_GROUP_URI,
                            CUSTOMER_GROUP_CREATED_MESSAGE, request));
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> updateCustomerGroup(UpdateCustomerGroupRequest request, String customerGroupId) {
            accountServiceValidation.validateJsonRequest(request);
            //check if customer exists, if not return with error message.
            accountServiceValidation.validateAccountIds(request.getAccountIds());
            CustomerGroup customerGroup = accountServiceValidation.validateAndGetCustomerGroupById(customerGroupId);
            customerGroup = CustomerGroup.updateCustomerGroupByRequest(customerGroup, request.getAccountIds(), request);
        CustomerGroup finalCustomerGroup = customerGroup;
        return accountServiceCommon.getEntityRefForCustomerGroup(customerGroupId).<Done>ask(replyTo->new CustomerGroupCommand.UpdateCustomerGroup(finalCustomerGroup,replyTo),ServiceConstants.ASK_TIMEOUT)
                    .thenApply(done -> accountServiceValidation.sendResponseByStatusIdAndRequest(customerGroupId, SUCCESS_STATUS, UPDATE_CUSTOMER_GROUP_URI, CUSTOMER_GROUP_UPDATED_MESSAGE, request));
    }

    public CompletionStage<Pair<ResponseHeader, CustomerGroupDetail>> getCustomerGroup(String customerGroupId) {
            accountServiceValidation.validateCustomerGroupId(customerGroupId);
            return accountServiceCommon.getEntityRefForCustomerGroup(customerGroupId).ask(CustomerGroupCommand.GetCustomerGroup::new,ServiceConstants.ASK_TIMEOUT).thenApply(customerGroup -> Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), Mapper.toCustomerGroupDetails(customerGroup.get())));
    }

    public CompletionStage<Pair<ResponseHeader, CustomerGroupDetails>>  getCustomerGroups(SearchRequest request) {
            accountServiceValidation.validateJsonRequest(request);
            filterValidation.validateFilterParameter(request.getPagination().getPageNo(),
                    request.getPagination().getPageSize(), request.getFilters(), FilterConfig.getCustomerGroupTypeMap(), request.getSorting().getField());
            FilterParameter filterParameter = FilterParameter.getNewInstance(Integer.valueOf(request.getPagination().getPageNo()),
                    Integer.valueOf(request.getPagination().getPageSize()), request.getSorting().getAscending(), request.getSorting().getField(), request.getFilters());
            return customerGroupRepository.getCustomerGroupIds(filterParameter).thenApply(
                    listWrapper -> {
                        CustomerGroupDetails obj = new CustomerGroupDetails(
                                listWrapper.getList().stream().map(accountServiceValidation::validateAndGetCustomerGroupById).map(Mapper::toCustomerGroupDetails)
                                        .collect(Collectors.toList()), listWrapper.getTotalSize());
                        return Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), obj);
                    }
            );
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> activateCustomerGroup(ActivationRequest request,String id) {
            if (request.getValue()) {
                return accountServiceValidation.validateAndActivateCustomerGroup(id, request);
            }
            return accountServiceValidation.validateAndDeactivateCustomerGroup(id, request);
    }

    /*Customer Group event publish*/
    public Topic<CustomerGroupEvents> customerGroupPublishedEvent() {
        return TopicProducer.taggedStreamWithOffset(CustomerGroupEvent.TAG.allTags(),
                (tag, offset) -> accountServiceCommon.registry.eventStream(tag, offset)
                        .filter(p -> p.first().isPublic())
                        .mapAsync(1, eventAndOffset ->
                                accountEventPublisher.convertPublishedEventFoCustomerGroup(eventAndOffset.first()).thenApply(event ->
                                        Pair.create(event, eventAndOffset.second()))));
    }

}
