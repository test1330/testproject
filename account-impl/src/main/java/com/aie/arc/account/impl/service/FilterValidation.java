package com.aie.arc.account.impl.service;

import com.aie.arc.account.api.common.exceptions.ParameterRequired;
import com.aie.arc.account.impl.entity.entities.vo.common.MappingData;

import org.apache.commons.lang3.StringUtils;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.Map;
import java.util.StringJoiner;

@Singleton
@Slf4j
public class FilterValidation {
    public void validateFilterParameter(String pageNo, String pageSize, Map<String, String> filters, Map<String, MappingData> fieldMap, String sortingField) {

        validateIntegerValue("pageNo", pageNo);
        validateIntegerValue("pageSize", pageSize);
        if (StringUtils.isNotBlank(sortingField) && !fieldMap.containsKey(sortingField)) {
            throw new ParameterRequired(new String[]{"Invalid sorting field"});
        }
        if (!filters.isEmpty())
            validateFilters(filters, fieldMap);
    }

    private void validateFilters(Map<String, String> filters, Map<String, MappingData> fieldMap) {
        StringJoiner joiner = new StringJoiner(",");

        filters.forEach((key, value) -> {
            if (!fieldMap.containsKey(key)) {
                joiner.add(key);
            }
        });
        if (joiner.length() > 0) {
            throw new ParameterRequired(new String[]{"Invalid filter fields(" + joiner.toString() + ")"});
        }

        validateFilterTypeValue(filters, fieldMap);
    }

    private void validateFilterTypeValue(Map<String, String> filters, Map<String, MappingData> fieldMap) {
        filters.forEach((key, value) -> {
            switch (fieldMap.get(key).getType()) {
                case BOOLEAN:
                    validateBooleanValue(key, value);
                    break;
                case INTEGER:
                    validateIntegerValue(key, value);
                    break;
                case DATE:
                    validateDateValue(key, value);
                    break;
                default:
                    break;
            }
        });
    }

    private void validateBooleanValue(String key, String value) {
        try
        {
            if(value == null)
                throw new ParameterRequired(new String[]{"You need to pass boolean value for filter(" + key + ")"});
        }
        catch(Exception e)
        {
            throw new ParameterRequired(new String[]{"You need to pass boolean value for filter(" + key + ")"});
        }

    }

    private void validateIntegerValue(String key, String value) {
        try {
            if (Integer.valueOf(value)  < 0)
                throw new ParameterRequired(new String[]{"You need to pass positive integer value for filter(" + key + ")"});
        } catch (NumberFormatException ex) {
            throw new ParameterRequired(new String[]{"You need to pass integer value for filter(" + key + ")"});
        }
    }

    private void validateDateValue(String key, String value) {

        try
        {
            if(value == null)
                throw new ParameterRequired(new String[]{"You need to pass date value for filter(" + key + ")"});
        }
        catch(Exception e)
        {
            throw new ParameterRequired(new String[]{"You need to pass date value for filter(" + key + ")"});
        }
    }
}