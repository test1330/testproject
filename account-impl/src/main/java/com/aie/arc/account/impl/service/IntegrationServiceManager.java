package com.aie.arc.account.impl.service;

import akka.japi.Pair;
import com.aie.arc.account.api.common.exceptions.InvalidParameter;
import com.aie.arc.account.api.request.CMPWSApiServiceRequest;
import com.aie.arc.account.api.request.CardRequest;
import com.aie.arc.account.api.request.CreateCustomerRequest;
import com.aie.arc.account.api.request.brandsmart.AdditionalFormats;
import com.aie.arc.account.api.request.brandsmart.FirstDataRequest;
import com.aie.arc.account.api.request.brandsmart.FirstDataResponse;
import com.aie.arc.account.api.response.CMPWSApiServiceResponse;
import com.aie.arc.account.api.response.CreateCustomerResponse;
import com.aie.arc.account.impl.repository.AccountRepository;
import com.aie.arc.account.impl.soap.handler.CompassClientSOAPHandler;
import com.aie.arc.account.impl.soap.handler.IntegrationsSOAPHandler;
import com.aie.arc.externalservice.api.AvalaraService;
import com.aie.arc.externalservice.api.FreshAddressService;
import com.aie.arc.externalservice.api.IpInfoService;
import com.aie.arc.externalservice.api.request.AvalaraRequest;
import com.aie.arc.externalservice.api.response.Error;
import com.aie.arc.externalservice.api.response.*;
import com.brandsmartusa.services.atgwebbroker.AtgWebBroker;
import com.brandsmartusa.services.atgwebbroker.AtgWebBroker_Service;
import com.brandsmartusa.services.atgwebbroker.Customer;
import com.brandsmartusa.services.atgwebbroker.CustomerTypeEnum;
import com.brandsmartusa.services.compassclient.CMPWSApiService;
import com.brandsmartusa.services.compassclient.OnlineAF;
import com.brandsmartusa.services.compassclient.OnlineTransRequest;
import com.brandsmartusa.services.compassclient.TA;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import com.lightbend.lagom.javadsl.client.CircuitBreakersPanel;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

import static com.aie.arc.account.impl.util.MessageConstants.*;

@Singleton
@Slf4j
public class IntegrationServiceManager {

    @Inject
    private FreshAddressService freshAddressService;
    @Inject
    private AvalaraService avalaraService;
    @Inject
    private IpInfoService ipInfoService;
    @Inject
    private AtgWebBroker_Service atgWebBrokerService;
    @Inject
    private AccountServiceImplCommon accountServiceCommon;
    @Inject
    private AccountRepository accountRepository;
    @Inject
    private Config appConfig;
    @Inject
    private CircuitBreakersPanel circuitBreakerPanel;

    @Inject
    CMPWSApiService cmpwsApiService;

    private final String CIRCUIT_BREAKER_OPEN_EXCEPTION = "akka.pattern.CircuitBreakerOpenException";
    private final String FIRST_DATA_SUCCESS_RESPONSE = "FirstData successful response";

    public CompletionStage<Pair<ResponseHeader, AvalaraResponse>> validateAddress(AvalaraRequest request) {
        Config config = appConfig.getConfig("config.externalservices.avalara");
        final String clientId = String.format("%s; %s; %s; %s; %s", config.getString("appName"),
                config.getString("appVersion"), "JavaRestClient", "18.6.1.215", config.getString("machineName"));
        String authHeader;
        authHeader = Base64.encodeBase64String((config.getString("accountId") + ":"
                + config.getString("licenceKey")).getBytes(StandardCharsets.UTF_8));
        final String auth = authHeader;
        boolean isAvalaraServiceEnabled = config.getBoolean(SERVICE_ENABLED);
        log.debug("validate Address isAvalaraServiceEnabled:\t" + isAvalaraServiceEnabled);
        log.debug("validate Address Request:{}", request);
        if (isAvalaraServiceEnabled) {
            int count = INTEGER_ZERO;
            int maxRetries = config.getInt("maxRetries");
            AvalaraResponse avalaraResponse = null;
            boolean isTimeOutException = false;
            while (count < maxRetries) {
                log.info("Invoking Avalara service to validate Address - Service invocation attempt : {}", count + ONE);
                AvalaraServiceResponse avalaraServiceResponse = null;
                try {
                    avalaraServiceResponse = avalaraService.resolveAddress()
                            .handleRequestHeader(requestHeader -> requestHeader.withHeader("Authorization",
                                    "Basic " + auth)
                                    .withHeader("X-Avalara-Client", clientId))
                            .invoke(request).toCompletableFuture().handle((response, exception) -> new AvalaraServiceResponse(response, exception))
                            .get(config.getInt("timeOutInSeconds"), TimeUnit.SECONDS);
                } catch (Exception exp) {
                    log.error("Exception occured for avalara service : ", exp);
                    isTimeOutException = true;
                }
                log.debug(" Avalara service Response :{}", avalaraServiceResponse);
                if (isTimeOutException || Objects.nonNull(avalaraServiceResponse.getThrowable())) {
                    count++;
                    isTimeOutException = false;
                    if (count == maxRetries) {
                        ArrayList<ValidatedAddressInfo> validatedAddressInfo = new ArrayList(Collections.singleton(
                                new ValidatedAddressInfo("UnknownAddressType", request.getLine1(),
                                        request.getLine2(), null, request.getCity(),
                                        request.getRegion(), request.getCountry(), request.getPostalCode(), null,
                                        null)));
                        avalaraResponse = new AvalaraResponse(null, validatedAddressInfo, null, null,
                                null, null, null);
                        log.info("Avalara Service invocation reached max retry limit Hence manual response has been constructed");
                        break;
                    }
                } else {
                    avalaraResponse = avalaraServiceResponse.getAvalaraResponse();
                    /*List<String> invalidAddressTypes = getInvalidAddressTypes(avalaraResponse.getValidatedAddresses());
                    if(invalidAddressTypes.size() == 0) {
                        ArrayList<AddressError> addressErrorList = (ArrayList<AddressError>) invalidAddressTypes.stream()
                                .map(addressType -> new AddressError("addressType invalid",
                                null, addressType, null, null))
                                .collect(Collectors.toList());
                        avalaraResponse = new AvalaraResponse(avalaraResponse.getAddress(), avalaraResponse.getValidatedAddresses(),
                                avalaraResponse.getCoordinates(), avalaraResponse.getResolutionQuality(), avalaraResponse.getTaxAuthorities(),
                                addressErrorList, avalaraResponse.getError());
                    }*/
                    break;
                }
            }
            return CompletableFuture.completedFuture(Pair.create(ResponseHeader.OK, avalaraResponse));
        } else {
            log.debug("Avalara service is disabled and returning the dummy response");
            AuthError error = new AuthError("", "Avalara service is disabled " +
                    "and returning the dummy response", null);
            return CompletableFuture.completedFuture(Pair.create(ResponseHeader.NO_CONTENT, new AvalaraResponse(null, null, null,
                    null, null, null, error)));
        }
    }

    /*private List<String> getInvalidAddressTypes(List<ValidatedAddressInfo> validatedAddressInfoList) {
        List<String> addressResolutionAddressType = appConfig.getConfig("config.externalservices.avalara")
                .getStringList("addressResolutionAddressType");
        return validatedAddressInfoList.stream()
                .map(ValidatedAddressInfo::getAddressType)
                .filter(addressResolutionAddressType::contains)
                .collect(Collectors.toList());
    }*/

    public CompletionStage<IpInfoServiceResponse> validateIPAddress(String ipAddress) {
        Config config = appConfig.getConfig("config.externalservices.ipinfo");
        boolean isIPInfoServiceEnabled = config.getBoolean(SERVICE_ENABLED);
        String tokenId = config.getString("apiTokenId");
        log.debug("isIPInfoServiceEnabled : {}", isIPInfoServiceEnabled);
        log.debug("IP address to validate : {} ", ipAddress);
        if (isIPInfoServiceEnabled) {
            log.debug("enabled");
            return CompletableFuture.completedFuture(getIpInformation(ipAddress, tokenId));
        } else {
            log.debug("validateIPAddress is disabled. Returning dummy response");
            return CompletableFuture.completedFuture(new IpInfoServiceResponse("", "", "",
                    "", "", "", "", "", Boolean.FALSE, null,
                    new Error("", "validateIPAddress is disabled. Returning dummy response")));
        }
    }

    private IpInfoServiceResponse getIpInformation(String ipAddress, String tokenId) {
        int attempt = INTEGER_ZERO;
        IpInfoServiceResponse ipInformation;
        do {
            attempt++;
            log.debug("attempt : {}", attempt);
            ipInformation = invokeIpInformationCall(ipAddress, tokenId).toCompletableFuture().join();
            log.debug("IpInfo response : {} ", ipInformation);
            if (ipInformation.getIp().equals(CIRCUIT_BREAKER_OPEN_EXCEPTION)) {
                return new IpInfoServiceResponse("", null, null, null, null,
                        null, null, null, false, null, null);
            }
        } while (StringUtils.isBlank(ipInformation.getIp()) && attempt < MAX_ATTEMPTS);
        if (attempt == MAX_ATTEMPTS) {
            log.info("MAX_ATTEMPTS({}) exceeded. Service is down. Sending empty zipCode in response.", MAX_ATTEMPTS);
            return new IpInfoServiceResponse("", null, null, null, null,
                    null, null, null, false, null, null);
        }
        return ipInformation;
    }

    CompletionStage<IpInfoServiceResponse> invokeIpInformationCall(String ipAddress, String tokenId) {
        log.debug("getting IpInformation");
        return circuitBreakerPanel.withCircuitBreaker("ipInfoService", () -> {
            log.debug("circuit breaker check");
            return ipInfoService.invokeAPI(ipAddress, tokenId).invoke();
        }).exceptionally(exception -> {
            log.error("Exception encountered in IpInfo call: ", exception);
            if (exception.getClass().getName().equals(CIRCUIT_BREAKER_OPEN_EXCEPTION)) {
                return new IpInfoServiceResponse(CIRCUIT_BREAKER_OPEN_EXCEPTION, null, null, null, null,
                        null, null, null, false, null, new Error("Error", CIRCUIT_BREAKER_OPEN_EXCEPTION));
            }
            return new IpInfoServiceResponse("", null, null, null, null,
                    null, null, null, false, null, new Error("Error", exception.getMessage()));
        });
    }

    public CompletionStage<Pair<ResponseHeader, CreateCustomerResponse>> validateCreateCustomerSOAPService(
            CreateCustomerRequest request) {
        log.debug("Inside of validateCreateCustomerSOAPService");
        boolean isMockEnabled = appConfig.getBoolean("config.soap.service.createcustomer.mock.enabled");
        log.debug("Is MockEnabled for BMCIS Create Customer: {}", isMockEnabled);
        log.debug("CreateCustomerRequest:{}", request);
        if (!isMockEnabled) {
            Customer customer = new Customer();
            customer.setAtgProfileId(request.getAtgProfileId());
            customer.setAtgProfileUserType(request.getAtgProfileUserType());
            customer.setEmail(request.getEmail());
            customer.setFirstName(request.getFirstName());
            customer.setLastName(request.getLastName());
            customer.setType(CustomerTypeEnum.BRANDS_MART);
            log.debug("customer type :{} and profileType :{} ", customer.getType(), customer.getAtgProfileUserType());
            /* when ever java files generated from WSDL newly we need to add this property(address) inside the
            atgWebBrokerService manually and replace the default url value with newly created address property */
            String serviceEndPoint = appConfig.getString("config.soap.service.createcustomer.endPoint.url");
            log.debug("Service End point:{}", serviceEndPoint);
            CreateCustomerResponse createCustomerResponse = getCustomerInformation(customer);
            if (createCustomerResponse.getCustomerId() == 0 && createCustomerResponse.getErrorId() == 0) {
                log.error("CreateCustomer SOAP call exception :{} ", createCustomerResponse);
                accountRepository.processFailedAccountRecords(request.getAtgProfileId());
            }
            return CompletableFuture.completedFuture(Pair.create(ResponseHeader.OK, createCustomerResponse));
        } else {
            log.debug("Service mock is enabled");
            return CompletableFuture.completedFuture(Pair.create(ResponseHeader.OK,
                    new CreateCustomerResponse("Ok", 0, "This is a mock response",
                            123456789)));
        }
    }

    private CreateCustomerResponse getCustomerInformation(Customer customer) {
        int attempt = INTEGER_ZERO;
        CreateCustomerResponse createCustomerResponse;
        do {
            attempt++;
            log.debug("attempt : {}", attempt);
            createCustomerResponse = invokeCustomerServiceSOAPCall(customer).toCompletableFuture().join();
            log.debug("createCustomerResponse : {} ", createCustomerResponse);
            if (createCustomerResponse.getResponseStatus().equals(CIRCUIT_BREAKER_OPEN_EXCEPTION)) {
                return new CreateCustomerResponse(null, 0, null, 0);
            }
        } while (createCustomerResponse.getCustomerId() == 0 && attempt < MAX_ATTEMPTS);
        if (attempt == MAX_ATTEMPTS) {
            log.info("MAX_ATTEMPTS({}) exceeded. Service is down. Sending customerId as '0' in response.", MAX_ATTEMPTS);
            return new CreateCustomerResponse(null, 0, null, 0);
        }
        return createCustomerResponse;
    }

    CompletionStage<CreateCustomerResponse> invokeCustomerServiceSOAPCall(Customer customer) {
        log.debug("BMCIS Create Customer Request : {}", customer);
        return circuitBreakerPanel.withCircuitBreaker("create-customer-soap-service", () -> {
            log.debug("Inside the circuit breaker");
            Config playConfig = ConfigFactory.load(this.getClass().getClassLoader()).getConfig("play.soap.services").
                    getConfig("com.brandsmartusa.services.atgwebbroker.AtgWebBroker_Service");
            IntegrationsSOAPHandler soapHandler = new IntegrationsSOAPHandler(CREATE_CUSTOMER_ACTION, playConfig.getString("address"));
            AtgWebBroker wsHttpBindingAtgWebBroker = atgWebBrokerService.getWSHttpBindingAtgWebBroker(soapHandler);
            return wsHttpBindingAtgWebBroker.createCustomer(customer)
                    .thenApply(createCustomerResponse2 -> {
                                log.debug("BMCIS CreateCustomer response :{}", createCustomerResponse2);
                                return new CreateCustomerResponse(
                                        createCustomerResponse2.getType() == null ? null : createCustomerResponse2.getType().value(),
                                        createCustomerResponse2.getErrorId() == null ? 0 : createCustomerResponse2.getErrorId(),
                                        createCustomerResponse2.getMessage() == null ? null : createCustomerResponse2.getMessage().getValue(),
                                        createCustomerResponse2.getCustomerId() == null ? 0 : createCustomerResponse2.getCustomerId());
                            }
                    );
        }).exceptionally(exception -> {
            log.error("Exception encountered in CreateCustomer SOAP call : ", exception);
            if (exception.getClass().getName().equals(CIRCUIT_BREAKER_OPEN_EXCEPTION)) {
                return new CreateCustomerResponse(CIRCUIT_BREAKER_OPEN_EXCEPTION, 0, null, 0);
            }
            return new CreateCustomerResponse("", 0, null, 0);
        });
    }

    private String getValue(String pValue) {
        if (null != pValue) {
            return pValue.trim();
        }
        return pValue;
    }

    public CompletionStage<Pair<ResponseHeader, FirstDataResponse>> invokeCompassClientService(FirstDataRequest request) {
        log.debug(" Method :: invokeCompassClientService");

        FirstDataResponse response = new FirstDataResponse();
        log.debug("*** FirstData request *** : {}", request);

        if (appConfig.getBoolean("payment.firstdataEnabled")) {
            com.brandsmartusa.services.compassclient.Transaction transaction = getCompassClientTransaction(request);
            transaction.setDivisionNumber(FIRST_DATA_DIVISION_NUMBER);
            OnlineTransRequest onlineTransRequest = new OnlineTransRequest();
            onlineTransRequest.setTransaction(transaction);
            if (request.getAdditionalFormats() != null) {
                onlineTransRequest.setAdditionalFormats(getAdditionalFormats(request));
            }

            CompassClientSOAPHandler soapHandler = new CompassClientSOAPHandler(FIRST_DATA_USER_ID, FIRST_DATA_PASSWORD);
            return cmpwsApiService.getCMPWSApiPort(soapHandler).onlineTrans(onlineTransRequest)
                    .handle((serviceResponse, throwable) -> {
                        log.error("Response:{} ", serviceResponse);
                        if (Objects.nonNull(throwable)) {
                            log.error("Exception occurred invoking compass client: ", throwable);
                            response.setResponseReasonCode(throwable.getMessage());
                        }
                        updateSuccessResponseFields(response, serviceResponse);
                        return Pair.create(ResponseHeader.OK.withStatus(200), response);
                    });
        } else {
            String message = "CompassClientService service has been disabled. Please enable it and test";
            log.debug(message);
            response.setResponseReasonCode(message);
        }
        return CompletableFuture.completedFuture(Pair.create(ResponseHeader.OK, response));
    }

    private com.brandsmartusa.services.compassclient.Transaction getCompassClientTransaction(FirstDataRequest request) {
        com.brandsmartusa.services.compassclient.Transaction transaction = new com.brandsmartusa.services.compassclient.Transaction();
        transaction.setAccountNumber(request.getAccountNumber());
        transaction.setAccountNumberDesc(request.getAccountNumberDesc());
        transaction.setActionCode(request.getActionCode());
        transaction.setAmount(request.getAmount());
        transaction.setExpirationDate(request.getExpirationDate());
        transaction.setCurrencyCode(request.getCurrencyCode());
        transaction.setOrderNumber(FIRST_DATA_NO_ORDER);
        transaction.setMop(request.getMop());
        transaction.setTransactionType(FIRST_DATA_TRANSACTION_TYPE_ECI_INDICATOR_SECURE);
        return transaction;
    }


    private com.brandsmartusa.services.compassclient.Transaction getTestCompassClientTransaction(CMPWSApiServiceRequest request) {
        com.brandsmartusa.services.compassclient.Transaction transaction = new com.brandsmartusa.services.compassclient.Transaction();
        transaction.setAccountNumber(request.getAccountNumber());
        transaction.setAccountNumberDesc(FIRST_DATA_ACCOUNT_NUMBER_DESCRIPTION);
        transaction.setActionCode("GT");
        transaction.setAmount(FIRST_DATA_AMOUNT);
        transaction.setExpirationDate(request.getExpirationDate());
        transaction.setCurrencyCode(FIRST_DATA_CURRENCY_CODE);
        transaction.setOrderNumber(FIRST_DATA_NO_ORDER);
        transaction.setMop(request.getMop());
        transaction.setTransactionType(FIRST_DATA_TRANSACTION_TYPE_ECI_INDICATOR_SECURE);
        return transaction;
    }

    private OnlineAF getAdditionalFormats(FirstDataRequest request) {
        OnlineAF additionalFormats = new OnlineAF();
        TA parameter = new TA();
        parameter.setTokenType(FIRST_DATA_TOKEN_TYPE);
        additionalFormats.setTA(parameter);
        return additionalFormats;
    }

    private OnlineAF getAdditionalFormats(CMPWSApiServiceRequest request) {
        OnlineAF additionalFormats = new OnlineAF();
        TA parameter = new TA();
        parameter.setTokenType(FIRST_DATA_TOKEN_TYPE);
        additionalFormats.setTA(parameter);
        return additionalFormats;
    }

    private void updateSuccessResponseFields(FirstDataResponse response, com.brandsmartusa.services.compassclient.OnlineTransResponse serviceResponse) {
        if (null != serviceResponse) {
            response.setAccountNumber(getValue(serviceResponse.getAccountNumber()));
            response.setAuthorizationCode(getValue(serviceResponse.getAuthorizationCode()));
            response.setAvsResponseCode(getValue(serviceResponse.getAVSResponseCode()));
            response.setCsvResponseCode(getValue(serviceResponse.getCSVResponseCode()));
            response.setResponseDate(getValue(serviceResponse.getResponseDate()));
            response.setResponseReasonCode(getValue(serviceResponse.getResponseReasonCode()));
            response.setOrderNumber(getValue(serviceResponse.getOrderNumber()));
            response.setAmount(getValue(serviceResponse.getAmount()));
            log.debug("*** FirstDataResponse *** :{} ",response);
        }
    }

    private void updateTestSuccessResponseFields(CMPWSApiServiceResponse response, com.brandsmartusa.services.compassclient.OnlineTransResponse serviceResponse) {
        if (null != serviceResponse) {
            response.setAccountNumber(getValue(serviceResponse.getAccountNumber()));
            response.setAuthorizationCode(getValue(serviceResponse.getAuthorizationCode()));
            response.setAvsResponseCode(getValue(serviceResponse.getAVSResponseCode()));
            response.setCsvResponseCode(getValue(serviceResponse.getCSVResponseCode()));
            response.setResponseDate(getValue(serviceResponse.getResponseDate()));
            response.setResponseReasonCode(getValue(serviceResponse.getResponseReasonCode()));
            response.setOrderNumber(getValue(serviceResponse.getOrderNumber()));
            response.setAmount(getValue(serviceResponse.getAmount()));
        }
    }

    private String requestWithMaskNumber(String document, String accountNumber) {

        StringBuilder maskedNumber = new StringBuilder();

        for (int index = 0; index < accountNumber.length(); index++) {
            if (index < (accountNumber.length() - 4)) {
                maskedNumber.append("*");
            } else {
                maskedNumber.append(accountNumber.charAt(index));
            }
        }
        return document.replace(accountNumber, maskedNumber);
    }

    public CompletionStage<FirstDataResponse> compassSubscriptionCall(CardRequest request) {
        FirstDataRequest firstDataRequest = new FirstDataRequest();
        firstDataRequest.setCurrencyCode(FIRST_DATA_CURRENCY_CODE);
        firstDataRequest.setTransactionType(FIRST_DATA_TRANSACTION_TYPE_ECI_INDICATOR_SECURE);
        firstDataRequest.setExpirationDate(constructExpiryDate(request));
        firstDataRequest.setAccountNumberDesc(FIRST_DATA_ACCOUNT_NUMBER_DESCRIPTION);
        firstDataRequest.setAmount(FIRST_DATA_AMOUNT);
        firstDataRequest.setActionCode("GT");
        firstDataRequest.setOrderNumber(FIRST_DATA_NO_ORDER);
        firstDataRequest.setAccountNumber(request.getCardNumber());
        firstDataRequest.setMop(accountServiceCommon.getCardCodeMap().get(request.getCardType()));
        AdditionalFormats additionalFormats = new AdditionalFormats();
        additionalFormats.setTokenType(FIRST_DATA_TOKEN_TYPE);
        firstDataRequest.setAdditionalFormats(additionalFormats);
        return invokeCompassClientService(firstDataRequest)
                .thenApply(res -> res.second());
    }

    private String constructExpiryDate(CardRequest request) {
        String expMonth = StringUtils.isNotBlank(request.getExpirationMonth())
                && request.getExpirationMonth().length() == 1
                ? new StringBuffer(ZERO).append(request.getExpirationMonth()).toString() : request.getExpirationMonth();
        String expYear = StringUtils.isNotBlank(request.getExpirationYear())
                && request.getExpirationYear().length() == 4
                ? request.getExpirationYear().substring(2, 4) : request.getExpirationYear();
        return new StringBuffer(expMonth).append(expYear).toString();
    }

    public CompletionStage<Pair<ResponseHeader, CMPWSApiServiceResponse>> testInvokeCompassClientService(CMPWSApiServiceRequest request) {
        CMPWSApiServiceResponse response = new CMPWSApiServiceResponse();
        log.debug("CompassClientService request: {}", request);
        com.brandsmartusa.services.compassclient.Transaction transaction = getTestCompassClientTransaction(request);
        transaction.setDivisionNumber(FIRST_DATA_DIVISION_NUMBER);
        OnlineTransRequest onlineTransRequest = new OnlineTransRequest();
        onlineTransRequest.setTransaction(transaction);
        if (request.getAdditionalFormats() != null) {
            onlineTransRequest.setAdditionalFormats(getAdditionalFormats(request));
        }
        CompassClientSOAPHandler soapHandler = new CompassClientSOAPHandler(FIRST_DATA_USER_ID, FIRST_DATA_PASSWORD);
        return cmpwsApiService.getCMPWSApiPort(soapHandler).onlineTrans(onlineTransRequest).handle((serviceResponse, throwable) -> {
            if (Objects.nonNull(throwable)) {
                log.error("Exception occurred invoking compass client: ", throwable);
                response.setResponseReasonCode(throwable.getMessage());
            }
            updateTestSuccessResponseFields(response, serviceResponse);
            return Pair.create(ResponseHeader.OK.withStatus(200), response);
        });
    }

}