package com.aie.arc.account.impl.service;

import akka.Done;
import akka.cluster.sharding.typed.javadsl.EntityRef;
import akka.http.javadsl.model.StatusCodes;
import akka.japi.Pair;
import com.aie.arc.account.api.common.request.ActivationRequest;
import com.aie.arc.account.api.common.request.LocalisedFields;
import com.aie.arc.account.api.common.request.SearchRequest;
import com.aie.arc.account.api.common.response.BaseResponse;
import com.aie.arc.account.impl.entity.entities.vo.common.FilterParameter;
import com.aie.arc.account.impl.repository.repository.LocaleRepository;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.aie.arc.account.impl.util.common.FilterConfig;
import com.aie.arc.rbac.api.constants.RBACURIConstants;
import com.aie.arc.rbac.api.publishedevent.PermissionEvents;
import com.aie.arc.rbac.api.request.PermissionRequest;
import com.aie.arc.rbac.api.response.PermissionDetail;
import com.aie.arc.rbac.api.response.PermissionDetailWithTag;
import com.aie.arc.rbac.api.response.PermissionDetails;
import com.aie.arc.rbac.impl.command.PermissionCommand;
import com.aie.arc.rbac.impl.entity.entities.AccessRight;
import com.aie.arc.rbac.impl.entity.entities.Permission;
import com.aie.arc.rbac.impl.entity.entities.RBACLocalizedFieldsVO;
import com.aie.arc.rbac.impl.entity.entities.Workspace;
import com.aie.arc.rbac.impl.event.PermissionEvent;
import com.aie.arc.rbac.impl.exceptions.ParameterRequired;
import com.aie.arc.rbac.impl.mapper.RBACMapper;
import com.aie.arc.rbac.impl.repository.CommonRepository;
import com.aie.arc.rbac.impl.repository.PermissionRepository;
import com.aie.arc.rbac.impl.service.EventPublisher;
import com.aie.arc.rbac.impl.service.RBACServiceImplCommon;
import com.aie.arc.rbac.impl.service.RBACServiceRequestValidation;
import com.aie.arc.rbac.impl.util.ApiTagConstants;
import com.lightbend.lagom.javadsl.api.broker.Topic;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import com.lightbend.lagom.javadsl.broker.TopicProducer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static com.aie.arc.account.impl.util.MessageConstants.SUCCESS_STATUS;

@Singleton
@Slf4j
public class PermissionManager {

    @Inject
    private RBACServiceRequestValidation rbacServiceValidation;

    @Inject
    private RBACServiceImplCommon rbacServiceImplCommon;

    @Inject
    private LocaleRepository localeRepository;

    @Inject
    private AccountServiceValidation accountServiceValidation;

    @Inject
    private PermissionRepository permissionRepository;

    @Inject
    private EventPublisher eventPublisher;

    @Inject
    private FilterValidation filterValidation;

    @Inject
    private CommonRepository commonRepository;


    public CompletionStage<Pair<ResponseHeader, BaseResponse>>  createPermission(PermissionRequest request) {
        log.debug("Create Permission request is:{}",request);
        rbacServiceValidation.validateJsonRequest(request);
          request.getLocalization().values().forEach(field ->  rbacServiceValidation.validateIfPermissionAlreadyCreated(field.getName(), commonRepository));
        Workspace workspace = rbacServiceValidation.validateAndGetWorkspaceById(request.getWorkspaceId());
        rbacServiceValidation.validateAccessRightId(request.getAccessRightId());
        Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap = rbacServiceValidation.validateLocaleCodesAndGetVoMap
                        (request.getLocalization(), localeRepository);
        String id = getNewIdForPermission(request);
        EntityRef<PermissionCommand> ref = rbacServiceImplCommon.getEntityRefForPermission(id);
        Permission permission = Permission.createPermissionByRequest(id, localizedFieldsVOMap, request);
            return ref.<Done>ask(replyTo -> new PermissionCommand.CreatePermission(permission, replyTo), ServiceConstants.ASK_TIMEOUT)
                    .thenApply(done1 -> rbacServiceValidation.sendResponseByStatusIdAndRequest(id,
                            StatusCodes.CREATED.intValue(), RBACURIConstants.CREATE_PERMISSION_URI, RBACURIConstants.
                                    PERMISSION_CREATE_MESSAGE, request));
    }

    private String getNewIdForPermission(PermissionRequest request) {
        log.debug("inside getNewIdForPermission method");
        if(StringUtils.isNotBlank(request.getWorkspaceId()) && StringUtils.isNotBlank(request.getAccessRightId())){
            return new StringBuilder(request.getWorkspaceId()).append(ApiTagConstants.UNDER_SCORE).append(request.getAccessRightId()).toString();
        }
        return rbacServiceImplCommon.getNewId();
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> updatePermission(PermissionRequest request,String permissionId) {
            log.debug("Update Permission request is:{}",request);
            rbacServiceValidation.validateJsonRequest(request);
            rbacServiceValidation.validateAccessRightId(request.getAccessRightId());
            rbacServiceValidation.validateWorkspaceId(request.getWorkspaceId());
            Permission permission = rbacServiceValidation.validateAndGetPermissionById(permissionId);
            Permission updatePermission = permission.updatePermission(permission, request);
            return rbacServiceImplCommon.getEntityRefForPermission(permissionId).<Done>ask(replyTo -> new PermissionCommand.
                    UpdatePermission(updatePermission,replyTo),ServiceConstants.ASK_TIMEOUT)
                    .thenApply(done1 -> rbacServiceValidation.sendResponseByStatusIdAndRequest(permissionId,
                            SUCCESS_STATUS, RBACURIConstants.UPDATE_PERMISSION_URI, RBACURIConstants.PERMISSION_UPDATE_MESSAGE, request));
    }

    public CompletionStage<Pair<ResponseHeader, PermissionDetailWithTag>> getPermission(String permissionId) {
            log.debug("Get Permission for permission id:{}",permissionId);
            Permission permissions = rbacServiceValidation.validateAndGetPermissionById(permissionId);
            AccessRight accessRight = rbacServiceValidation.validateAndGetAccessRightById(permissions.getAccessRightId());
            Workspace workspace = rbacServiceValidation.validateAndGetWorkspaceById(permissions.getWorkspaceId());
            return rbacServiceImplCommon.getEntityRefForPermission(permissionId).ask(PermissionCommand.
                    GetPermission::new,ServiceConstants.ASK_TIMEOUT).thenApply(permission -> Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS),
                    RBACMapper.toPermissionDetailsWithTag(permission.get(),RBACMapper.toAccessRightDetails(accessRight),RBACMapper.toWorkspaceDetails(workspace))));
    }



    public CompletionStage<Pair<ResponseHeader, BaseResponse>> activatePermission(ActivationRequest request,String permissionId) {
            log.debug("Activate Permission for permission id:{}",permissionId);
            if (request.getValue()) {
                return rbacServiceValidation.validateAndActivatePermission(permissionId, request);
            }
            return rbacServiceValidation.validateAndDeactivatePermission(permissionId, request);
    }

    public CompletionStage<Pair<ResponseHeader, PermissionDetails>> getPermissions(SearchRequest request) {
            accountServiceValidation.validateJsonRequest(request);
            filterValidation.validateFilterParameter(request.getPagination().getPageNo(),
                    request.getPagination().getPageSize(), request.getFilters(), FilterConfig.getPermissionTypeMap(),
                             request.getSorting().getField());
            FilterParameter filterParameter = FilterParameter.getNewInstance(Integer.valueOf(request.getPagination().getPageNo()),
                    Integer.valueOf(request.getPagination().getPageSize()), request.getSorting().getAscending(),
                             request.getSorting().getField(), request.getFilters());
            return permissionRepository.getPermissionIds(filterParameter).thenApply(
                    listWrapper -> {
                        PermissionDetails obj = new PermissionDetails(
                                listWrapper.getList().stream()
                                        .map(rbacServiceValidation::validateAndGetPermissionById)
                                        .map(permission -> RBACMapper.toPermissionDetailsWithTag(permission,
                                                RBACMapper.toAccessRightDetails(rbacServiceValidation.validateAndGetAccessRightById(permission.getAccessRightId())),
                                                RBACMapper.toWorkspaceDetails(rbacServiceValidation.validateAndGetWorkspaceById(permission.getWorkspaceId()))))
                                        .collect(Collectors.toList()), listWrapper.getTotalSize());
                        return Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), obj);
                    }
            );
    }

    /*Permission  event publish*/
    public Topic<PermissionEvents> permissionPublishedEvent() {
        return TopicProducer.taggedStreamWithOffset(PermissionEvent.TAG.allTags(),
                (tag, offset) -> rbacServiceImplCommon.registry.eventStream(tag, offset)
                        .filter(p -> p.first().isPublic())
                        .mapAsync(1, eventAndOffset ->
                                eventPublisher.convertPublishedEventForPermission(eventAndOffset.first()).thenApply(event ->
                                        Pair.create(event, eventAndOffset.second()))));
    }


}
