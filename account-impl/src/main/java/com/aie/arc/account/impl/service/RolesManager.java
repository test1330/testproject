package com.aie.arc.account.impl.service;

import akka.Done;
import akka.cluster.sharding.typed.javadsl.EntityRef;
import akka.http.javadsl.model.StatusCodes;
import akka.japi.Pair;
import com.aie.arc.account.api.common.request.ActivationRequest;
import com.aie.arc.account.api.common.request.SearchRequest;
import com.aie.arc.account.api.common.response.BaseResponse;
import com.aie.arc.account.impl.command.BusinessUserCommand;
import com.aie.arc.account.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.account.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.account.impl.entity.entities.vo.common.FilterParameter;
import com.aie.arc.account.impl.repository.CardRepository;
import com.aie.arc.account.impl.repository.repository.LocaleRepository;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.aie.arc.account.impl.util.common.FilterConfig;
import com.aie.arc.rbac.api.constants.RBACURIConstants;
import com.aie.arc.rbac.api.publishedevent.RoleEvents;
import com.aie.arc.rbac.api.publishedevent.RoleGroupEvents;
import com.aie.arc.rbac.api.request.AssignBusinessRole;
import com.aie.arc.rbac.api.request.AssignRole;
import com.aie.arc.rbac.api.request.RoleGroupRequest;
import com.aie.arc.rbac.api.request.RoleRequest;
import com.aie.arc.rbac.api.response.*;
import com.aie.arc.rbac.impl.command.RoleCommand;
import com.aie.arc.rbac.impl.command.RoleGroupCommand;
import com.aie.arc.rbac.impl.entity.entities.Permission;
import com.aie.arc.rbac.impl.entity.entities.RBACLocalizedFieldsVO;
import com.aie.arc.rbac.impl.entity.entities.Role;
import com.aie.arc.rbac.impl.entity.entities.RoleGroup;
import com.aie.arc.rbac.impl.event.RoleEvent;
import com.aie.arc.rbac.impl.event.RoleGroupEvent;
import com.aie.arc.rbac.impl.mapper.RBACMapper;
import com.aie.arc.rbac.impl.repository.RoleGroupRepository;
import com.aie.arc.rbac.impl.repository.RoleRepository;
import com.aie.arc.rbac.impl.service.EventPublisher;
import com.aie.arc.rbac.impl.service.RBACServiceImplCommon;
import com.aie.arc.rbac.impl.service.RBACServiceRequestValidation;
import com.lightbend.lagom.javadsl.api.broker.Topic;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import com.lightbend.lagom.javadsl.broker.TopicProducer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static com.aie.arc.account.impl.util.MessageConstants.SUCCESS_STATUS;

@Singleton
@Slf4j
public class RolesManager {

    @Inject
    private RBACServiceRequestValidation rbacServiceValidation;

    @Inject
    private  RBACServiceImplCommon rbacServiceImplCommon;

    @Inject
    private RoleRepository roleRepository;

    @Inject
    private RoleGroupRepository roleGroupRepository;

    @Inject
    private LocaleRepository localeRepository;

    @Inject
    private AccountServiceImplCommon accountServiceCommon;

    @Inject
    private AccountServiceValidation accountServiceValidation;

    @Inject
    private CardRepository cardRepository;

    @Inject
    private  FilterValidation filterValidation;

    @Inject
    private  EventPublisher eventPublisher;




    /**
     * Create a RoleGroup.
     *
     * @return a success response if the request is correct and RoleGroup is created,
     * throws a ParameterRequired exception if any error in the request.
     */
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> createRoleGroup(RoleGroupRequest request) {
            rbacServiceValidation.validateJsonRequest(request);
            String id = rbacServiceImplCommon.getNewId();
            EntityRef<RoleGroupCommand> ref = rbacServiceImplCommon.getEntityRefForRoleGroup(id);
            Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap = rbacServiceValidation.validateLocaleCodesAndGetVoMap
                    (request.getLocalization(), localeRepository);
            RoleGroup roleGroup = RoleGroup.createRoleGroupByRequest(id, localizedFieldsVOMap, request.getActive());
            return ref.<Done>ask(replyTo -> new RoleGroupCommand.CreateRoleGroup(roleGroup,replyTo),ServiceConstants.ASK_TIMEOUT)
                    .thenApply(done -> rbacServiceValidation.sendResponseByStatusIdAndRequest(id,
                            StatusCodes.CREATED.intValue(), RBACURIConstants.CREATE_ROLE_GROUP_URI, RBACURIConstants.
                                    ROLE_GROUP_CREATED_MESSAGE, request));
    }

    /**
     * Update an existing RoleGroup and return success response or throw exception in case of error in request.
     *
     * @param roleGroupId a unique identifier for the RoleGroup to be updated.
     * @return a success response if the request is correct and the RoleGroup has been updated,
     * * throws a ParameterRequired exception if any error in the request.
     */
    public CompletionStage<Pair<ResponseHeader, BaseResponse>>  updateRoleGroup(RoleGroupRequest request,String roleGroupId) {
            rbacServiceValidation.validateJsonRequest(request);
            EntityRef<RoleGroupCommand> ref = rbacServiceImplCommon.getEntityRefForRoleGroup(roleGroupId);
            RoleGroup roleGroup = rbacServiceValidation.validateAndGetRoleGroupById(roleGroupId);
            rbacServiceValidation.validateLocaleCodesAndGetVoMap(request.getLocalization(), localeRepository);
            roleGroup = RoleGroup.updateRoleGroupByRequest(roleGroup, request);
            RoleGroup finalRoleGroup = roleGroup;
            return ref.<Done>ask(replyTo -> new RoleGroupCommand.UpdateRoleGroup(finalRoleGroup,replyTo),ServiceConstants.ASK_TIMEOUT)
                    .thenApply(done -> rbacServiceValidation.sendResponseByStatusIdAndRequest(roleGroupId, SUCCESS_STATUS, RBACURIConstants.UPDATE_ROLE_GROUP_URI, RBACURIConstants.ROLE_GROUP_UPDATED_MESSAGE, request));
    }

    /**
     * Retrieve information about an existing RoleGroup or throw exception if the specified RoleGroup is not found.
     *
     * @param roleGroupId a unique identifier for the RoleGroup to be retrieved.
     * @return information about the RoleGroup requested for, throws a NotFound exception if the RoleGroup with the
     * specified customerGroupId does not exist.
     */
    public CompletionStage<Pair<ResponseHeader, RoleGroupDetail>>  getRoleGroup(String roleGroupId) {
            rbacServiceValidation.validateRoleGroupId(roleGroupId);
            return rbacServiceImplCommon.getEntityRefForRoleGroup(roleGroupId).ask(RoleGroupCommand.GetRoleGroup::new,ServiceConstants.ASK_TIMEOUT).thenApply(roleGroup -> Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS),
                    RBACMapper.toRoleGroupDetails(roleGroup.get())));
    }

    public CompletionStage<Pair<ResponseHeader, RoleGroupDetails>>  getRoleGroups(SearchRequest request) {
            accountServiceValidation.validateJsonRequest(request);
            filterValidation.validateFilterParameter(request.getPagination().getPageNo(),
                    request.getPagination().getPageSize(), request.getFilters(), FilterConfig.getRoleGroupTypeMap(), request.getSorting().getField());
            FilterParameter filterParameter = FilterParameter.getNewInstance(Integer.valueOf(request.getPagination().getPageNo()),
                    Integer.valueOf(request.getPagination().getPageSize()), request.getSorting().getAscending(), request.getSorting().getField(), request.getFilters());
            return roleGroupRepository.getRoleGroupIds(filterParameter).thenApply(
                    listWrapper -> {
                        List<RoleGroupDetail> roleGroupDetails = listWrapper.getList().stream()
                                .map(rbacServiceValidation::validateAndGetRoleGroupById)
                                .map(RBACMapper::toRoleGroupDetails)
                                .collect(Collectors.toList());
                        RoleGroupDetails obj = new RoleGroupDetails(roleGroupDetails, listWrapper.getTotalSize());
                        return Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), obj);
                    }
            );
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>>  activateRoleGroup(ActivationRequest  request,String id) {
            if (request.getValue()) {
                return rbacServiceValidation.validateAndActivateRoleGroup(id, request);
            }
            return rbacServiceValidation.validateAndDeactivateRoleGroup(id, request);
    }

    /**
     * Create a Role.
     *
     * @return a success response if the request is correct and Role is created,
     * throws a ParameterRequired exception if any error in the request.
     */
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> createRole(RoleRequest request) {
            log.debug("createRole request is:{}",request);
            rbacServiceValidation.validateJsonRequest(request);
            String id = rbacServiceImplCommon.getNewId();
            EntityRef<RoleCommand> ref = rbacServiceImplCommon.getEntityRefForRole(id);
            if(StringUtils.isNoneBlank(request.getRoleGroupId())) {
                rbacServiceValidation.validateRoleGroupId(request.getRoleGroupId());
            }
            rbacServiceValidation.validatePermissionByIds(request.getPermissionIds());
            Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap = rbacServiceValidation.validateLocaleCodesAndGetVoMap(request.getLocalization(), localeRepository);
            Role role = Role.createRoleByRequest(id, request.getRoleGroupId(), request.getPermissionIds(),localizedFieldsVOMap, request.getActive());
            return ref.<Done>ask(replyTo -> new RoleCommand.CreateRole(role,replyTo),ServiceConstants.ASK_TIMEOUT)
                    .thenApply(done -> rbacServiceValidation.sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS, RBACURIConstants.CREATE_ROLE_URI, RBACURIConstants.ROLE_CREATED_MESSAGE, request));
    }

    /**
     * Update an existing Role and return success response or throw exception in case of error in request.
     *
     * @param roleId a unique identifier for the Role to be updated.
     * @return a success response if the request is correct and the Role has been updated,
     * * throws a ParameterRequired exception if any error in the request.
     */
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> updateRole(RoleRequest request,String roleId) {
        log.debug("updateRole request is:{}for roleid:{}",request,roleId);
            rbacServiceValidation.validateJsonRequest(request);
            EntityRef<RoleCommand> ref = rbacServiceImplCommon.getEntityRefForRole(roleId);
            if(StringUtils.isNoneBlank(request.getRoleGroupId())) {
                rbacServiceValidation.validateRoleGroupId(request.getRoleGroupId());
            }
            rbacServiceValidation.validatePermissionByIds(request.getPermissionIds());
            Role role = rbacServiceValidation.validateAndGetRoleById(roleId);
            rbacServiceValidation.validateLocaleCodesAndGetVoMap(request.getLocalization(), localeRepository);
            role = Role.updateRoleByRequest(role, request);
            Role finalRole = role;
            return ref.<Done>ask(replyTo -> new RoleCommand.UpdateRole(finalRole,replyTo),ServiceConstants.ASK_TIMEOUT)
                    .thenApply(done -> rbacServiceValidation.sendResponseByStatusIdAndRequest(roleId, SUCCESS_STATUS, RBACURIConstants.UPDATE_ROLE_URI, RBACURIConstants.ROLE_UPDATED_MESSAGE, request));
    }

    /**
     * Retrieve information about an existing Role or throw exception if the specified Role is not found.
     *
     * @param roleId a unique identifier for the Role to be retrieved.
     * @return information about the Role requested for, throws a NotFound exception if the Role with the
     * specified customerGroupId does not exist.
     */
    public CompletionStage<Pair<ResponseHeader, RoleDetail>> getRole(String roleId) {
        log.debug("getRole roleid:{}",roleId);
            Role roles = rbacServiceValidation.validateAndGetRoleById(roleId);
            return rbacServiceImplCommon.getEntityRefForRole(roleId).ask(RoleCommand.GetRole::new,ServiceConstants.ASK_TIMEOUT).
                    thenApply(role -> Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), RBACMapper.toRoleDetails(role.get(),
                            roles.getPermissionIds().stream().filter(Objects::nonNull)
                                    .map(rbacServiceValidation::validateAndGetPermissionById)
                                    .map(permission -> RBACMapper.toPermissionDetails(permission,
                                            RBACMapper.toAccessRightDetails(rbacServiceValidation.validateAndGetAccessRightById(permission.getAccessRightId())),
                                            RBACMapper.toWorkspaceDetails(rbacServiceValidation.validateAndGetWorkspaceById(permission.getWorkspaceId()))
                                    )).collect(Collectors.toList()))));
    }

    public CompletionStage<Pair<ResponseHeader, RoleDetails>> getRoles(SearchRequest request) {
        log.debug("getRoles request is:{}",request);
            accountServiceValidation.validateJsonRequest(request);
            filterValidation.validateFilterParameter(request.getPagination().getPageNo(),
                    request.getPagination().getPageSize(), request.getFilters(), FilterConfig.getRoleTypeMap(), request.getSorting().getField());
            FilterParameter filterParameter = FilterParameter.getNewInstance(Integer.valueOf(request.getPagination().getPageNo()),
                    Integer.valueOf(request.getPagination().getPageSize()), request.getSorting().getAscending(), request.getSorting().getField(), request.getFilters());
            return roleRepository.getRoleIds(filterParameter).thenApply(
                    listWrapper -> {
                        RoleDetails obj = new RoleDetails(
                                listWrapper.getList().stream()
                                        .map(rbacServiceValidation::validateAndGetRoleById)
                                        .map(RBACMapper::toRoleDetails)
                                        .collect(Collectors.toList()), listWrapper.getTotalSize());
                        return Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), obj);
                    }
            );
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>>  activateRole(ActivationRequest request,String id) {
        log.debug("activateRole request is:{}",request);
        if (request.getValue()) {
                return rbacServiceValidation.validateAndActivateRole(id, request);
        }
            return rbacServiceValidation.validateAndDeactivateRole(id, request);
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> assignRoleToUser(AssignRole request,String businessUserId) {
        log.debug("assignRoleToUser request is:{} for  user id :{}",request,businessUserId);
        rbacServiceValidation.validateJsonRequest(request);
            accountServiceValidation.validateBusinessUserById(businessUserId);
            rbacServiceValidation.validateRoleIds(request.getRoleIds());
            return accountServiceCommon.getEntityRefForBusinessUser(businessUserId).<Done>ask(replyTo->new BusinessUserCommand.AssignRoleToBusinessUser(businessUserId, request.getRoleIds(),
                            new CreatedAt(LocalDateTime.now()), new LastModifiedAt(LocalDateTime.now()),replyTo),ServiceConstants.ASK_TIMEOUT).
                    thenApply(done -> {
                BaseResponse response = BaseResponse.getResponseByStatusAndMessage(businessUserId, SUCCESS_STATUS, RBACURIConstants.ROLE_ASSIGN_MESSAGE);
                return Pair.create(ResponseHeader.OK.withStatus(StatusCodes.OK.intValue()), response);
            });
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> assignRoleToBusinessUser(AssignBusinessRole request) {
        log.debug("assignRoleToBusinessUser request is:{}",request);
        rbacServiceValidation.validateJsonRequest(request);
        accountServiceValidation.validateBusinessUserById(request.getUserId());
        rbacServiceValidation.validateRoleIds(request.getRoleIds());
        return accountServiceCommon.getEntityRefForBusinessUser(request.getUserId()).<Done>ask(replyTo->new BusinessUserCommand.AssignRoleToBusinessUser(request.getUserId(), request.getRoleIds(),
                new CreatedAt(LocalDateTime.now()), new LastModifiedAt(LocalDateTime.now()),replyTo),ServiceConstants.ASK_TIMEOUT).
                thenApply(done -> {
                    BaseResponse response = BaseResponse.getResponseByStatusAndMessage(request.getUserId(), SUCCESS_STATUS, RBACURIConstants.ROLE_ASSIGN_TO_BUSINESS_MESSAGE);
                    return Pair.create(ResponseHeader.OK.withStatus(StatusCodes.OK.intValue()), response);
                });
    }

    public Topic<RoleGroupEvents> roleGroupPublishedEvent() {
        return TopicProducer.taggedStreamWithOffset(RoleGroupEvent.TAG.allTags(),
                (tag, offset) -> rbacServiceImplCommon.registry.eventStream(tag, offset)
                        .filter(p -> p.first().isPublic())
                        .mapAsync(1, eventAndOffset ->
                                eventPublisher.convertPublishedEventForRoleGroup(eventAndOffset.first()).thenApply(event ->
                                        Pair.create(event, eventAndOffset.second()))));
    }

    public Topic<RoleEvents> rolePublishedEvent() {
        return TopicProducer.taggedStreamWithOffset(RoleEvent.TAG.allTags(),
                (tag, offset) -> rbacServiceImplCommon.registry.eventStream(tag, offset)
                        .filter(p -> p.first().isPublic())
                        .mapAsync(1, eventAndOffset ->
                                eventPublisher.convertPublishedEventForRole(eventAndOffset.first()).thenApply(event ->
                                        Pair.create(event, eventAndOffset.second()))));
    }

    public  Set<String> getApiList(List<String> roleIds) {
        log.debug("Start getApiList for roles:{}",roleIds);
        Set<String> apiTagDetails=new HashSet<String>();
        if(CollectionUtils.isNotEmpty(roleIds)){
            roleIds.stream().filter(Objects::nonNull).forEach(roleid -> {
                Role role = rbacServiceValidation.validateAndGetRoleById(roleid);
                if(role!=null)
                    role.getPermissionIds().stream().filter(Objects::nonNull).forEach(permissionId -> {
                        Permission permission = rbacServiceValidation.validateAndGetPermissionById(permissionId);
                        apiTagDetails.addAll(permission.getApiTags().stream().filter(Objects::nonNull).collect(Collectors.toList()));

                });
            });
        }
        log.debug("End getApiList apitag details are :{}",apiTagDetails);
        return apiTagDetails;
    }

}
