package com.aie.arc.account.impl.service;

import akka.Done;
import akka.http.javadsl.model.StatusCodes;
import akka.japi.Pair;
import com.aie.arc.account.api.common.response.BaseResponse;
import com.aie.arc.account.api.request.AddStoreRequest;
import com.aie.arc.account.impl.command.AccountCommand;
import com.aie.arc.account.impl.repository.BusinessUserRepository;
import com.aie.arc.account.impl.repository.CustomerGroupRepository;
import com.aie.arc.account.impl.repository.repository.LocaleRepository;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;
import com.lightbend.lagom.javadsl.server.HeaderServiceCall;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletionStage;

import static com.aie.arc.account.impl.util.MessageConstants.*;
import static com.aie.arc.account.api.constants.AccountURIConstants.*;

@Singleton
@Slf4j
public class StoreManager {

    @Inject
    private AccountServiceImplCommon accountServiceCommon;
    @Inject
    private AccountServiceValidation accountServiceValidation;


    public CompletionStage<Pair<ResponseHeader, BaseResponse>> addStore(AddStoreRequest request,String accountId) {
            return accountServiceValidation.validateAndGetAccountById(accountId)
                    .thenCompose(optionalAccount -> {
                        accountServiceValidation.validateStoreById(request.getStoreId());
                        return accountServiceCommon.getEntityRefForAccount(accountId).<Done>ask(replyTo->new AccountCommand.AddStoreToAccount(
                                accountId, request.getStoreId(),replyTo),ServiceConstants.ASK_TIMEOUT)
                                .thenApply(done -> accountServiceValidation.sendResponseByStatusIdAndRequest(accountId,
                                        StatusCodes.CREATED.intValue(), ADD_STORE, ADD_STORE_MESSAGE, request));
                    });

    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> updateStore(AddStoreRequest request,String accountId) {
            return accountServiceValidation.validateAndGetAccountById(accountId)
                    .thenCompose(optionalAccount -> {
                        accountServiceValidation.validateStoreById(request.getStoreId());
                        return accountServiceCommon.getEntityRefForAccount(accountId)
                                .<Done>ask(replyTo->new AccountCommand.UpdateStoreToAccount(accountId,
                                        request.getStoreId(),replyTo),ServiceConstants.ASK_TIMEOUT)
                                .thenApply(done -> accountServiceValidation.sendResponseByStatusIdAndRequest(accountId,
                                        SUCCESS_STATUS, UPDATE_STORE, UPDATE_STORE_MESSAGE, request));
                    });

    }

}
