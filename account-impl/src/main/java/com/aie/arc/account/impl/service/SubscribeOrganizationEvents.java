package com.aie.arc.account.impl.service;

import akka.Done;
import com.aie.arc.account.api.common.exceptions.ParameterRequired;
import com.aie.arc.account.api.common.request.BaseRequest;
import com.aie.arc.account.impl.command.ErrorProcessorCommand;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.aie.arc.organization.api.common.request.LocalizedFieldsRequest;
import com.aie.arc.organization.api.event.OfflineStoreEvents;
import com.aie.arc.organization.api.event.OnlineStoreEvents;
import com.aie.arc.organization.api.request.dto.*;
import com.aie.arc.tool.common.status.Status;
import com.aie.arc.tool.common.vo.LocalizedFieldsTools;
import com.aie.arc.tool.organization.subscribe.command.OfflineStoreCommand;
import com.aie.arc.tool.organization.subscribe.command.OnlineStoreCommand;
import com.aie.arc.tool.organization.subscribe.entities.OfflineStore;
import com.aie.arc.tool.organization.subscribe.entities.OnlineStore;
import com.aie.arc.tool.organization.subscribe.entities.dto.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class SubscribeOrganizationEvents {
    private final AccountServiceValidation validationObj;
    private final AccountServiceImplCommon commonObj;


    public SubscribeOrganizationEvents(AccountServiceValidation validationObj, AccountServiceImplCommon commonObj) {
        this.validationObj = validationObj;
        this.commonObj = commonObj;
    }

    public CompletionStage<Done> handleOfflineStoreEvents(OfflineStoreEvents offlineStoreEvent) {
        OfflineStoreEvents.OfflineStoreCreated offlineStoreCreated = null;
        if (offlineStoreEvent instanceof OfflineStoreEvents.OfflineStoreCreated) {
            offlineStoreCreated = (OfflineStoreEvents.OfflineStoreCreated) offlineStoreEvent;
            Optional<OfflineStore> offlineStore = commonObj.getEntityRefForOfflineStore(offlineStoreCreated.getOfflineStoreId()).ask(OfflineStoreCommand.GetOfflineStore::new, ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join();

            if (!offlineStore.isPresent()) {
                OfflineStore store = new OfflineStore(offlineStoreCreated.getOfflineStoreId(), offlineStoreCreated.
                        getCode(), toLocalizedFieldVoMap(offlineStoreCreated.getLocalizationMap()), offlineStoreCreated.
                        getType(), toAddressApi(offlineStoreCreated.getAddress()), toGeoLocationApi(offlineStoreCreated.
                        getGeoLocation()), offlineStoreCreated.isActive(), offlineStoreCreated.getCreatedAt(),
                        offlineStoreCreated.getLastModifiedAt(), Status.CREATED);
                return commonObj.getEntityRefForOfflineStore(offlineStoreCreated.getOfflineStoreId()).ask(replyTo -> new OfflineStoreCommand.CreateOfflineStore(store, replyTo), ServiceConstants.ASK_TIMEOUT);
            }
        }
        if (offlineStoreEvent instanceof OfflineStoreEvents.OfflineStoreUpdated) {
            OfflineStoreEvents.OfflineStoreUpdated storeUpdated = (OfflineStoreEvents.OfflineStoreUpdated) offlineStoreEvent;
            OfflineStore offlineStore = new OfflineStore(storeUpdated.getOfflineStoreId(), storeUpdated.getCode(),
                    toLocalizedFieldVoMap(storeUpdated.getLocalizationMap()), storeUpdated.getType(),
                    toAddressApi(storeUpdated.getAddress()), toGeoLocationApi(storeUpdated.getGeoLocation()),
                    storeUpdated.isActive(), storeUpdated.getCreatedAt(), storeUpdated.getLastModifiedAt(), Status.CREATED);
            return commonObj.getEntityRefForOfflineStore(storeUpdated.getOfflineStoreId()).ask(replyTo->new OfflineStoreCommand.UpdateOfflineStore(offlineStore,replyTo),ServiceConstants.ASK_TIMEOUT);
            //}
        }
        if (offlineStoreEvent instanceof OfflineStoreEvents.OfflineStoreActivated) {
            OfflineStoreEvents.OfflineStoreActivated offlineStoreActivated = (OfflineStoreEvents.OfflineStoreActivated)
                    offlineStoreEvent;
            return commonObj.getEntityRefForOfflineStore(offlineStoreActivated.getOfflineStoreId()).ask(replyTo->new
                    OfflineStoreCommand.ActivateOfflineStore(offlineStoreActivated.getOfflineStoreId(), offlineStoreActivated.isActive(),replyTo),ServiceConstants.ASK_TIMEOUT);
            //}
        }
        if (offlineStoreEvent instanceof OfflineStoreEvents.OfflineStoreDeactivated) {
            OfflineStoreEvents.OfflineStoreDeactivated offlineStoreDeactivated = (OfflineStoreEvents.
                    OfflineStoreDeactivated) offlineStoreEvent;
            return commonObj.getEntityRefForOfflineStore(offlineStoreDeactivated.getOfflineStoreId()).
                    ask(replyTo->new OfflineStoreCommand.DeactivateOfflineStore(offlineStoreDeactivated.getOfflineStoreId(),
                            offlineStoreDeactivated.isActive(),replyTo),ServiceConstants.ASK_TIMEOUT);
        }
        return CompletableFuture.completedFuture(Done.getInstance());
    }

    public CompletionStage<Done> handleOnlineStoreEvents(OnlineStoreEvents onlineStoreEvents) {
        OnlineStoreEvents.OnlineStoreCreated onlineStoreCreated = null;
        if (onlineStoreEvents instanceof OnlineStoreEvents.OnlineStoreCreated) {
            onlineStoreCreated = (OnlineStoreEvents.OnlineStoreCreated) onlineStoreEvents;
            Optional<OnlineStore> onlineStore = commonObj.getEntityRefForOnlineStore(onlineStoreCreated.
                    getOnlineStoreId()).ask(OnlineStoreCommand.GetOnlineStore::new, ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join();

            if (!onlineStore.isPresent()) {
                OnlineStore onlineStore1 = new OnlineStore(
                        onlineStoreCreated.getOnlineStoreId(),
                        toLocalizedFieldVoMap(onlineStoreCreated.getLocalization()),
                        onlineStoreCreated.getCode(),
                        onlineStoreCreated.getDefaultLanguageId(),
                        onlineStoreCreated.getAllowedLanguageIds(),
                        onlineStoreCreated.getBaseCurrencyId(),
                        onlineStoreCreated.getDefaultCurrencyId(),
                        onlineStoreCreated.getAllowedCurrencyIds(),
                        onlineStoreCreated.getDefaultCountryId(),
                        onlineStoreCreated.getAllowedCountryIds(),
                        onlineStoreCreated.getDefaultLocaleId(),
                        onlineStoreCreated.getAllowedLocaleIds(),
                        onlineStoreCreated.getDefaultShippingTypeId(),
                        onlineStoreCreated.getShippingTypeIds(),
                        onlineStoreCreated.getDefaultPaymentMethodId(),
                        onlineStoreCreated.getPaymentMethodIds(),
                        onlineStoreCreated.getDefaultShippingMethodId(),
                        onlineStoreCreated.getShippingMethodIds(),
                        onlineStoreCreated.getWeightUnit(),
                        onlineStoreCreated.getLengthUnit(),
                        onlineStoreCreated.getFirstDayOfTheWeek(),
                        onlineStoreCreated.getWeekendDays(),
                        onlineStoreCreated.getDaysOfTheWeek(),
                        onlineStoreCreated.getSenderName(),
                        onlineStoreCreated.getSenderEmail(),
                        onlineStoreCreated.getReceiveEmailsAt(),
                        toLocaleSiteConfigurationApi(onlineStoreCreated.getLocaleSiteConfigurations()),
                        onlineStoreCreated.getRootCategoryId(),
                        toShippingApi(onlineStoreCreated.getShipping()),
                        onlineStoreCreated.getSiteIcon(),
                        onlineStoreCreated.getFavIcon(),
                        onlineStoreCreated.getSitePriority(),
                        onlineStoreCreated.isB2B(),
                        onlineStoreCreated.getWebsite(),
                        onlineStoreCreated.getAlternateURLs(),
                        onlineStoreCreated.getPreviewURL(),
                        onlineStoreCreated.getSiteDisabledURL(),
                        toSocialMediaApi(onlineStoreCreated.getSocialMedia()),
                        onlineStoreCreated.isActive(),
                        onlineStoreCreated.getCreatedAt(),
                        onlineStoreCreated.getLastModifiedAt(),
                        Status.CREATED);
                return commonObj.getEntityRefForOnlineStore(onlineStoreCreated.getOnlineStoreId()).<Done>ask(replyTo->new OnlineStoreCommand.CreateOnlineStore(onlineStore1,replyTo),ServiceConstants.ASK_TIMEOUT);
            }
        }
        if (onlineStoreEvents instanceof OnlineStoreEvents.OnlineStoreUpdated) {
            OnlineStoreEvents.OnlineStoreUpdated onlineStoreUpdated = (OnlineStoreEvents.OnlineStoreUpdated) onlineStoreEvents;
            OnlineStore updatedOnlineStore = new OnlineStore(
                    onlineStoreUpdated.getOnlineStoreId(),
                    toLocalizedFieldVoMap(onlineStoreUpdated.getLocalization()),
                    onlineStoreUpdated.getCode(),
                    onlineStoreUpdated.getDefaultLanguageId(),
                    onlineStoreUpdated.getAllowedLanguageIds(),
                    onlineStoreUpdated.getBaseCurrencyId(),
                    onlineStoreUpdated.getDefaultCurrencyId(),
                    onlineStoreUpdated.getAllowedCurrencyIds(),
                    onlineStoreUpdated.getDefaultCountryId(),
                    onlineStoreUpdated.getAllowedCountryIds(),
                    onlineStoreUpdated.getDefaultLocaleId(),
                    onlineStoreUpdated.getAllowedLocaleIds(),
                    onlineStoreUpdated.getDefaultShippingTypeId(),
                    onlineStoreUpdated.getShippingTypeIds(),
                    onlineStoreUpdated.getDefaultPaymentMethodId(),
                    onlineStoreUpdated.getPaymentMethodIds(),
                    onlineStoreUpdated.getDefaultShippingMethodId(),
                    onlineStoreUpdated.getShippingMethodIds(),
                    onlineStoreUpdated.getWeightUnit(),
                    onlineStoreUpdated.getLengthUnit(),
                    onlineStoreUpdated.getFirstDayOfTheWeek(),
                    onlineStoreUpdated.getWeekendDays(),
                    onlineStoreUpdated.getDaysOfTheWeek(),
                    onlineStoreUpdated.getSenderName(),
                    onlineStoreUpdated.getSenderEmail(),
                    onlineStoreUpdated.getReceiveEmailsAt(),
                    toLocaleSiteConfigurationApi(onlineStoreUpdated.getLocaleSiteConfigurations()),
                    onlineStoreUpdated.getRootCategoryId(),
                    toShippingApi(onlineStoreUpdated.getShipping()),
                    onlineStoreUpdated.getSiteIcon(),
                    onlineStoreUpdated.getFavIcon(),
                    onlineStoreUpdated.getSitePriority(),
                    onlineStoreUpdated.isB2B(),
                    onlineStoreUpdated.getWebsite(),
                    onlineStoreUpdated.getAlternateURLs(),
                    onlineStoreUpdated.getPreviewURL(),
                    onlineStoreUpdated.getSiteDisabledURL(),
                    toSocialMediaApi(onlineStoreUpdated.getSocialMedia()),
                    onlineStoreUpdated.isActive(),
                    onlineStoreUpdated.getCreatedAt(),
                    onlineStoreUpdated.getLastModifiedAt(),
                    Status.CREATED);
            return commonObj.getEntityRefForOnlineStore(onlineStoreCreated.getOnlineStoreId()).<Done>ask(replyTo->new OnlineStoreCommand.UpdateOnlineStore(updatedOnlineStore,replyTo),ServiceConstants.ASK_TIMEOUT);
            //}
        }
        return CompletableFuture.completedFuture(Done.getInstance());
    }

    private Map<String, LocalizedFieldsTools> toLocalizedFieldVoMap(Map<String, LocalizedFieldsRequest> localizedFields) {
        Map<String, LocalizedFieldsTools> localisedFieldsMap = new HashMap<>();
        localizedFields.forEach(((code, localised) -> {
            localisedFieldsMap.put(code, new LocalizedFieldsTools(localised.getName(), localised.getDescription()));
        }));
        return localisedFieldsMap;
    }

    private Address toAddressApi(AddressRequest addressRequest) {
        return new Address(addressRequest.getFlatNumber(), addressRequest.getApartmentNumber(), addressRequest.
                getBuilding(), addressRequest.getApartment(), addressRequest.getStreetName(), addressRequest.
                getStreetNumber(), addressRequest.getAdditionalStreetInfo(), addressRequest.getZipCodeId(),
                addressRequest.getCityId(), addressRequest.getStateId(), addressRequest.getCountryId(),
                addressRequest.getRegion(), addressRequest.getCompany(), addressRequest.getDepartment(),
                addressRequest.getPOBox(), addressRequest.getPhone(), addressRequest.getMobile(),
                addressRequest.getEmail(), addressRequest.getFax(), addressRequest.getAdditionalAddressInfo());
    }

    private Shipping toShippingApi(ShippingRequest shippingRequest) {
        return new Shipping(shippingRequest.getPreOrderEnabled(), shippingRequest.getBopisEnabled(), shippingRequest.getBossEnabled());
    }

    private GeoLocation toGeoLocationApi(GeoLocationRequest geoLocationRequest) {
        return new GeoLocation(geoLocationRequest.getLongitude(), geoLocationRequest.getLatitude());
    }


    private List<LocaleSiteConfiguration> toLocaleSiteConfigurationApi(List<LocaleSiteConfigurationRequest> configurationRequests) {
        List<LocaleSiteConfiguration> localeSiteConfigurations = new ArrayList<>();
        configurationRequests.forEach(localeSiteConfigRequest -> {
            localeSiteConfigurations.add(new LocaleSiteConfiguration(localeSiteConfigRequest.getLocaleCode(), localeSiteConfigRequest.getSiteURL()));

        });
        return localeSiteConfigurations;
    }

    private SocialMedia toSocialMediaApi(SocialMediaRequest socialMediaRequest) {
        Facebook facebook = new Facebook(socialMediaRequest.getFacebook().getAppId(), socialMediaRequest.getFacebook().getCaption(), socialMediaRequest.getFacebook().getDescription(), socialMediaRequest.getFacebook().getLinkName(), socialMediaRequest.getFacebook().getPicture());
        Twitter twitter = new Twitter(socialMediaRequest.getTwitter().getTwitterUserName(), socialMediaRequest.getTwitter().getTwitterLinkDescription(), socialMediaRequest.getTwitter().getTwitterUrl());
        return new SocialMedia(facebook, twitter);
    }

    private void generateErrorProcessorEventIfError(BaseRequest baseRequest, String id, LocalDateTime timestamp, String eventName) {
        try {
            validationObj.validateJsonRequest(baseRequest);
        } catch (ParameterRequired e) {
            try {
                commonObj.entityRefForErrorProcessor(id).ask(new ErrorProcessorCommand.CreateErrorProcessor(id, eventName,
                        new ObjectMapper().writeValueAsString(baseRequest), e.exceptionMessage().getErrors().toString(), timestamp));
            } catch (JsonProcessingException json) {
                json.printStackTrace();
            }
        }
    }
}