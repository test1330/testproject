package com.aie.arc.account.impl.service;

import akka.Done;
import akka.cluster.sharding.typed.javadsl.EntityRef;
import akka.http.javadsl.model.StatusCodes;
import akka.japi.Pair;
import com.aie.arc.account.api.common.request.ActivationRequest;
import com.aie.arc.account.api.common.request.LocalisedFields;
import com.aie.arc.account.api.common.request.SearchRequest;
import com.aie.arc.account.api.common.response.BaseResponse;
import com.aie.arc.account.impl.entity.entities.vo.common.FilterParameter;
import com.aie.arc.account.impl.repository.repository.LocaleRepository;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.aie.arc.account.impl.util.common.FilterConfig;
import com.aie.arc.rbac.api.constants.RBACURIConstants;
import com.aie.arc.rbac.api.request.UpdateWorkspaceRequest;
import com.aie.arc.rbac.api.request.WorkspaceRequest;
import com.aie.arc.rbac.api.response.WorkspaceDetail;
import com.aie.arc.rbac.api.response.WorkspaceDetails;
import com.aie.arc.rbac.impl.command.WorkspaceCommand;
import com.aie.arc.rbac.impl.entity.entities.RBACLocalizedFieldsVO;
import com.aie.arc.rbac.impl.entity.entities.Workspace;
import com.aie.arc.rbac.impl.mapper.RBACMapper;
import com.aie.arc.rbac.impl.repository.CommonRepository;
import com.aie.arc.rbac.impl.repository.WorkspaceRepository;
import com.aie.arc.rbac.impl.service.RBACServiceImplCommon;
import com.aie.arc.rbac.impl.service.RBACServiceRequestValidation;
import com.google.inject.Inject;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRef;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static com.aie.arc.account.impl.util.MessageConstants.SUCCESS_STATUS;

@Slf4j
public class WorkspaceManager {
    @Inject
    private RBACServiceRequestValidation rbacServiceValidation;
    @Inject
    private RBACServiceImplCommon rbacServiceImplCommon;
    @Inject
    private LocaleRepository localeRepository;
    @Inject
    private AccountServiceValidation accountServiceValidation;
    @Inject
    private WorkspaceRepository workspaceRepository;
    @Inject
    private FilterValidation filterValidation;
    @Inject
    private CommonRepository commonRepository;

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> createWorkspace(WorkspaceRequest request){
        log.debug("Create Workspace");
        rbacServiceValidation.validateJsonRequest(request);
        request.getLocalization().values().forEach(field-> rbacServiceValidation.validateIfWorkspaceAlreadyCreated(field.getName(), commonRepository));
        String id = StringUtils.isAnyBlank(request.getId())? rbacServiceImplCommon.getNewId():request.getId();
        EntityRef<WorkspaceCommand> ref = rbacServiceImplCommon.getEntityRefForWorkspace(id);

        Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap = rbacServiceValidation.validateLocaleCodesAndGetVoMap(request.getLocalization(), localeRepository);
        Workspace workspace = Workspace.createWorkspaceByRequest(id, localizedFieldsVOMap, request);
        return ref.<Done>ask(replyTo-> new WorkspaceCommand.CreateWorkspace(workspace,replyTo), ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> rbacServiceValidation.sendResponseByStatusIdAndRequest(id, StatusCodes.CREATED.intValue(), RBACURIConstants.CREATE_WORKSPACE_URI, RBACURIConstants.WORKSPACE_CREATE_MESSAGE, request));
    }
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> updateWorkspace(UpdateWorkspaceRequest request, String workspaceId){
        log.debug("Update Workspace  with workspaceId: {}", workspaceId);
        rbacServiceValidation.validateJsonRequest(request);
        EntityRef<WorkspaceCommand> ref = rbacServiceImplCommon.getEntityRefForWorkspace(workspaceId);
        Workspace workspace = rbacServiceValidation.validateAndGetWorkspaceById(workspaceId);
        rbacServiceValidation.validateLocaleCodesAndGetVoMap(request.getLocalization(), localeRepository);
        workspace = Workspace.updateWorkspace(workspace, request);
        Workspace finalWorkspace = workspace;
        return ref.<Done>ask(replyTo-> new WorkspaceCommand.UpdateWorkspace(finalWorkspace,replyTo),ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> rbacServiceValidation.sendResponseByStatusIdAndRequest(workspaceId, SUCCESS_STATUS, RBACURIConstants.UPDATE_WORKSPACE_URI, RBACURIConstants.WORKSPACE_UPDATE_MESSAGE, request));
    }

    public CompletionStage<Pair<ResponseHeader, WorkspaceDetail>> getWorkspace(String workspaceId) {
        log.debug("Get Workspace   with id: {}", workspaceId);
        rbacServiceValidation.validateWorkspaceId(workspaceId);
        return rbacServiceImplCommon.getEntityRefForWorkspace(workspaceId).ask(WorkspaceCommand.GetWorkspace::new ,ServiceConstants.ASK_TIMEOUT)
                .thenApply(accessRight -> Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), RBACMapper.toWorkspaceDetails(accessRight.get())));
    }
    public CompletionStage<Pair<ResponseHeader, WorkspaceDetails>> getAllWorkspace(SearchRequest request){
        log.debug("Get all Work space");
        accountServiceValidation.validateJsonRequest(request);
        filterValidation.validateFilterParameter(request.getPagination().getPageNo(),
                request.getPagination().getPageSize(), request.getFilters(), FilterConfig.getWorkspaceMap(), request.getSorting().getField());
        FilterParameter filterParameter = FilterParameter.getNewInstance(Integer.valueOf(request.getPagination().getPageNo()),
                Integer.valueOf(request.getPagination().getPageSize()), request.getSorting().getAscending(), request.getSorting().getField(), request.getFilters());
        return workspaceRepository.getWorkspaceIds(filterParameter).thenApply(
                listWrapper -> {
                    List<WorkspaceDetail> workspaceDetails = listWrapper.getList().stream()
                            .map(rbacServiceValidation::validateAndGetWorkspaceById)
                            .map(RBACMapper::toWorkspaceDetails)
                            .collect(Collectors.toList());
                    WorkspaceDetails obj = new WorkspaceDetails(workspaceDetails, listWrapper.getTotalSize());
                    return Pair.create(ResponseHeader.OK.withStatus(SUCCESS_STATUS), obj);
                }
        );
    }
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> deactivateWorkspace(ActivationRequest request, String workspaceId) {
        log.debug("Deactivate Workspace with workspaceId: {} using request flag : {}", workspaceId, request.getValue());
        if(request.getValue()) {
            return rbacServiceValidation.validateAndActivateWorkspace(workspaceId, request);
        }
        return rbacServiceValidation.validateAndDeactivateWorkspace(workspaceId, request);
    }
}
