package com.aie.arc.account.impl.service.subscribe;

import akka.Done;
import com.aie.arc.account.api.subscribe.vo.CncCard;
import com.aie.arc.account.impl.command.AccountCommand;
import com.aie.arc.account.impl.command.AddressCommand;
import com.aie.arc.account.impl.command.CardCommand;
import com.aie.arc.account.impl.entity.entities.Address;
import com.aie.arc.account.impl.entity.entities.Card;
import com.aie.arc.account.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.account.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.account.impl.service.AccountServiceImplCommon;
import com.aie.arc.account.impl.status.Status;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lightbend.lagom.javadsl.api.broker.Message;
import com.lightbend.lagom.javadsl.broker.kafka.KafkaMetadataKeys;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static com.aie.arc.account.impl.util.ServiceConstants.ASK_TIMEOUT;

@Slf4j
@Singleton
public class CheckoutEventsSubscriber {

    private final AccountServiceImplCommon accountServiceImplCommon;

    @Inject
    public CheckoutEventsSubscriber(AccountServiceImplCommon accountServiceImplCommon) {
        this.accountServiceImplCommon = accountServiceImplCommon;
    }

    public CompletionStage<Done> consumePaymentEvents(Message<String> userKafkaMessage) {
        log.debug("userKafkaMessage . Payload -------------: {} ", userKafkaMessage.getPayload());
        log.info("Payment event from Checkout consumed in Account");
        log.info("subscribe_user kafka_message_key : {} , TOPIC : {}, PARTITION : {} , OFFSET : {} " +
                        "and TIMESTAMP : {}", userKafkaMessage.messageKeyAsString(),
                userKafkaMessage.get(KafkaMetadataKeys.TOPIC), userKafkaMessage.get(KafkaMetadataKeys.PARTITION),
                userKafkaMessage.get(KafkaMetadataKeys.OFFSET), userKafkaMessage.get(KafkaMetadataKeys.TIMESTAMP));
        log.trace("user_kafka_message : {}", userKafkaMessage.getPayload());
        try {
            ObjectMapper obj = new ObjectMapper();
            CncCard cncCard = obj.readValue(userKafkaMessage.getPayload(), CncCard.class);
            accountServiceImplCommon.getEntityRefForCard(cncCard.getCardId())
                    .<Done>ask(replyTo -> new CardCommand.AddCard(convertCncCardToAccountCard(cncCard), replyTo), ASK_TIMEOUT)
                    .thenCompose(linkCardToAccount -> accountServiceImplCommon.getEntityRefForAccount(cncCard.getAccountId())
                            .<Done>ask(replyTo -> new AccountCommand.AddCardToAccount(cncCard.getAccountId(), cncCard.getCardId(),
                                    cncCard.isDefaultCreditCard(), StringUtils.isNotBlank(cncCard.getCardId()), replyTo), ASK_TIMEOUT));
        } catch (Exception ex) {
            log.error("Exception encountered while reading data from kafka message :", ex);
        }
        return CompletableFuture.completedFuture(Done.getInstance());
    }

    private Card convertCncCardToAccountCard(CncCard cncCard) {
        return new Card(cncCard.getCardId(), cncCard.getAccountId(), cncCard.getFirstName(), cncCard.getLastName(),
                cncCard.isDefaultCreditCard(), cncCard.getCardNumber(), cncCard.getCardType(), cncCard.getSubscriptionId(),
                cncCard.getExpirationMonth(), cncCard.getExpirationYear(), cncCard.getCardNickName(), cncCard.isActive(),
                new CreatedAt(LocalDateTime.now()), new LastModifiedAt(LocalDateTime.now()), Status.CREATED);
    }

    public CompletionStage<Done> consumeAddressEvents(Message<String> userKafkaMessage) {
        log.info("Address event from Checkout consumed in Account");
        log.info("subscribe_user kafka_message_key : {} , TOPIC : {}, PARTITION : {} , OFFSET : {} " +
                        "and TIMESTAMP : {}", userKafkaMessage.messageKeyAsString(),
                userKafkaMessage.get(KafkaMetadataKeys.TOPIC), userKafkaMessage.get(KafkaMetadataKeys.PARTITION),
                userKafkaMessage.get(KafkaMetadataKeys.OFFSET), userKafkaMessage.get(KafkaMetadataKeys.TIMESTAMP));
        log.trace("user_kafka_message : {}", userKafkaMessage.getPayload());
        try {
            ObjectMapper obj = new ObjectMapper();
            Address address = obj.readValue(userKafkaMessage.getPayload(), Address.class);
            accountServiceImplCommon.getEntityRefForAddress(address.getAddressId())
                    .<Done>ask(replyTo -> new AddressCommand.CreateAddress(address, replyTo), ASK_TIMEOUT)
                    .thenCompose(linkAddressToAccount -> accountServiceImplCommon.getEntityRefForAccount(address.getAccountId())
                            .<Done>ask(replyTo -> new AccountCommand.AddAddressToAccount(address.getAccountId(), address.getAddressId(),
                                    address.getPostalCode(), address.isDefaultShipping(), address.isDefaultBilling(),
                                    StringUtils.isNotBlank(address.getAccountId()), replyTo), ServiceConstants.ASK_TIMEOUT));
        } catch (Exception ex) {
            log.error("error_in_consuming_address_kafka_message : {} with error :", userKafkaMessage, ex);
        }
        return CompletableFuture.completedFuture(Done.getInstance());
    }

}
