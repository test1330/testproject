package com.aie.arc.account.impl.soap.handler;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;

import javax.xml.namespace.QName;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Slf4j
@SuppressWarnings({"unchecked"})
public class CompassClientSOAPHandler implements SOAPHandler<SOAPMessageContext> {
    String userName;
    String passCode;

    public CompassClientSOAPHandler(String userName, String passCode) {
        this.userName = userName;
        this.passCode = passCode;
    }

    public boolean handleMessage(SOAPMessageContext context) {
        Boolean outbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if (null != outbound && outbound) {
            String authString = userName + ":" + passCode;
            String authHeader = Base64.encodeBase64String(authString.getBytes(StandardCharsets.UTF_8));
            Map<String, List<String>> headers = new HashMap<>();
            headers.put("Authorization", Collections.singletonList("Basic "+authHeader));
            Map<String, List<String>> existingHeaders = (Map<String, List<String>>)context.get(MessageContext.HTTP_REQUEST_HEADERS);
            if (null != existingHeaders && !existingHeaders.isEmpty()) {
                headers.putAll(existingHeaders);
            }
            context.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        }
        return Boolean.TRUE;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return Boolean.FALSE;
    }

    /**
     * @param context default
     */
    @Override
    public void close(MessageContext context) {
        log.debug("Inside close method");
    }

    @Override
    public Set<QName> getHeaders() {
        return Collections.emptySet();
    }
}
