package com.aie.arc.account.impl.soap.handler;

import lombok.extern.slf4j.Slf4j;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.util.Collections;
import java.util.Set;

@Slf4j
public class IntegrationsSOAPHandler implements SOAPHandler<SOAPMessageContext> {
    static final String XMLNS_WSA = "xmlns:wsa";
    static final String ACTION = "Action";
    static final String TO = "To";
    static final String WSA = "wsa";
    static final String NAME_SPACE_URI = "http://www.w3.org/2005/08/addressing";
    String actionURI;
    String toURI;

    public IntegrationsSOAPHandler(String actionURI, String toURI) {
        this.actionURI = actionURI;
        this.toURI = toURI;
    }

    public boolean handleMessage(SOAPMessageContext context) {
        Boolean outbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if (null != outbound && outbound) {
            try {
                SOAPMessage message = context.getMessage();
                SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
                SOAPHeader header = envelope.addHeader();
                header.setAttribute(XMLNS_WSA, NAME_SPACE_URI);
                header.addChildElement(new QName(NAME_SPACE_URI, ACTION, WSA)).addTextNode(actionURI);
                header.addChildElement(new QName(NAME_SPACE_URI, TO, WSA)).addTextNode(toURI);
                message.saveChanges();
            } catch (SOAPException ex) {
                log.error("Exception occurred while adding SOAP message headers: ", ex);
            }
        }
        return Boolean.TRUE;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return Boolean.FALSE;
    }

    /**
     * @param context default
     */
    @Override
    public void close(MessageContext context) {
        log.debug("Inside close method");
    }

    @Override
    public Set<QName> getHeaders() {
        return Collections.emptySet();
    }
}