package com.aie.arc.account.impl.state;

import com.aie.arc.account.impl.entity.entities.Account;
import com.aie.arc.account.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.CompressedJsonable;
import lombok.Value;

import java.util.Optional;
import java.util.function.Function;

@Value
@JsonDeserialize
public class AccountState implements CompressedJsonable  {
    public final Optional<Account> account;

    @JsonCreator
    public AccountState(Optional<Account> account) {
        this.account = account;
    }

    public static AccountState empty() {
        return new AccountState(Optional.empty());
    }

    public static AccountState create(Account account) {
        return new AccountState(Optional.of(account));
    }

    public Status getStatus() {
        return account.map(Account::getStatus).orElse(Status.NOT_CREATED);
    }

    public AccountState updateDetails(Account details) {
        return update(i -> i.withDetails(details));
    }
    public AccountState updateDetails(String storeId) { return update(i -> i.withDetails(storeId)); }
    public AccountState updateDetails(boolean value) { return update(i -> i.withDetails(value)); }
    public AccountState updateDefaultPaymentDetails(String cardId) { return update(i -> i.updateDefaultPaymentDetails(cardId)); }

    public AccountState addZipCode(String myZip) {
        return update(i -> i.addZipCode(myZip));
    }

    private AccountState update(Function<Account, Account> updateFunction) {
        assert account.isPresent();
        return new AccountState(account.map(updateFunction));
    }


}
