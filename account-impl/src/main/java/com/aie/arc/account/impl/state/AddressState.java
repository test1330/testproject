package com.aie.arc.account.impl.state;

import com.aie.arc.account.impl.entity.entities.Address;
import com.aie.arc.account.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.CompressedJsonable;
import lombok.Value;

import java.util.Optional;
import java.util.function.Function;
@Value
@JsonDeserialize
public class AddressState implements CompressedJsonable {

    public final Optional<Address> address;

    @JsonCreator
    public AddressState(Optional<Address> address) {
        this.address = address;
    }

    public static AddressState empty() {
        return new AddressState(Optional.empty());
    }

    public static AddressState create(Address asset) {
        return new AddressState(Optional.of(asset));
    }

    public Status getStatus() {
        return address.map(Address::getStatus).orElse(Status.NOT_CREATED);
    }

    public AddressState updateDetails(Address details) {
        return update(i -> i.withDetails(details));
    }

    public AddressState updateDetails() {
        return update(i -> i.withDetails(this.address.get()));
    }

    public AddressState updateDetails(boolean value) {
        return update(i -> i.withDetails(value));
    }

    private AddressState update(Function<Address, Address> updateFunction) {
        assert address.isPresent();
        return new AddressState(address.map(updateFunction));
    }

}

