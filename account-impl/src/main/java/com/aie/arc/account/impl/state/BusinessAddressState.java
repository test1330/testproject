package com.aie.arc.account.impl.state;

import com.aie.arc.account.impl.entity.entities.BusinessAddress;
import com.aie.arc.account.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.CompressedJsonable;
import lombok.Value;

import java.util.Optional;
import java.util.function.Function;
@Value
@JsonDeserialize
public class BusinessAddressState implements CompressedJsonable {

    public final Optional<BusinessAddress> address;

    @JsonCreator
    public BusinessAddressState(Optional<BusinessAddress> address) {
        this.address = address;
    }

    public static BusinessAddressState empty() {
        return new BusinessAddressState(Optional.empty());
    }

    public static BusinessAddressState create(BusinessAddress asset) {
        return new BusinessAddressState(Optional.of(asset));
    }

    public Status getStatus() {
        return address.map(BusinessAddress::getStatus).orElse(Status.NOT_CREATED);
    }

    public BusinessAddressState updateDetails(BusinessAddress details) {
        return update(i -> i.withDetails(details));
    }

    public BusinessAddressState updateDetails() {
        return update(i -> i.withDetails(this.address.get()));
    }

    public BusinessAddressState updateDetails(boolean value) {
        return update(i -> i.withDetails(value));
    }

    private BusinessAddressState update(Function<BusinessAddress, BusinessAddress> updateFunction) {
        assert address.isPresent();
        return new BusinessAddressState(address.map(updateFunction));
    }

}

