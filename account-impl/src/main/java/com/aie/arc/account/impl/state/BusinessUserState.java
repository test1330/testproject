package com.aie.arc.account.impl.state;

import com.aie.arc.account.impl.entity.entities.BusinessUser;
import com.aie.arc.account.impl.entity.entities.vo.BusinessProfile;
import com.aie.arc.account.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.CompressedJsonable;
import lombok.Value;

import java.util.Optional;
import java.util.function.Function;
@Value
@JsonDeserialize
public class BusinessUserState implements CompressedJsonable {

    public final Optional<BusinessUser> businessUser;
    @JsonCreator
    public BusinessUserState(Optional<BusinessUser> businessUser) {
        this.businessUser = businessUser;
    }

    public static BusinessUserState empty() {
        return new BusinessUserState(Optional.empty());
    }

    public static BusinessUserState create(BusinessUser businessUser) {
        return new BusinessUserState(Optional.of(businessUser));
    }

    public Status getStatus() {
        return businessUser.map(BusinessUser::getStatus).orElse(Status.NOT_CREATED);
    }

    public BusinessUserState updateDetails(BusinessUser details) {
        return update(i -> i.withDetails(details));
    }

    public BusinessUserState updateDetails(BusinessProfile profile) {
        return update(i -> i.withDetails(profile));
    }

    public BusinessUserState updateDetails() {
        return update(i -> i.withDetails(this.businessUser.get()));
    }

    public BusinessUserState updateDetails(boolean value) {
        return update(i -> i.withDetails(value));
    }

    private BusinessUserState update(Function<BusinessUser, BusinessUser> updateFunction) {
        assert businessUser.isPresent();
        return new BusinessUserState(businessUser.map(updateFunction));
    }

}

