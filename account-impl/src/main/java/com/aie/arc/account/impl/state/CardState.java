package com.aie.arc.account.impl.state;

import com.aie.arc.account.impl.entity.entities.Card;
import com.aie.arc.account.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.CompressedJsonable;
import lombok.Value;

import java.util.Optional;
import java.util.function.Function;
@Value
@JsonDeserialize
public class CardState implements CompressedJsonable {

    public final Optional<Card> card;
    @JsonCreator
    public CardState(Optional<Card> card) {
        this.card = card;
    }

    public static CardState empty() {
        return new CardState(Optional.empty());
    }

    public static CardState create(Card card) {
        return new CardState(Optional.of(card));
    }

    public Status getStatus() {
        return card.map(Card::getStatus).orElse(Status.NOT_CREATED);
    }

    public CardState updateDetails(Card details) {
        return update(i -> i.withDetails(details));
    }

    public CardState updateDetails() {
        return update(i -> i.withDetails(this.card.get()));
    }
    public CardState updateDetails(boolean value) {
        return update(i -> i.withDetails(value));
    }

    private CardState update(Function<Card, Card> updateFunction) {
        assert card.isPresent();
        return new CardState(card.map(updateFunction));
    }

}

