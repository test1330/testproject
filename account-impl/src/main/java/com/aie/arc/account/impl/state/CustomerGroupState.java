package com.aie.arc.account.impl.state;

import com.aie.arc.account.impl.entity.entities.CustomerGroup;
import com.aie.arc.account.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.CompressedJsonable;
import lombok.Value;

import java.util.Optional;
import java.util.function.Function;
@Value
@JsonDeserialize
public class CustomerGroupState  implements CompressedJsonable {

    public final Optional<CustomerGroup> customerGroup;
    @JsonCreator
    public CustomerGroupState(Optional<CustomerGroup> customerGroup) {
        this.customerGroup = customerGroup;
    }

    public static CustomerGroupState empty() {
        return new CustomerGroupState(Optional.empty());
    }

    public static CustomerGroupState create(CustomerGroup customerGroup) {
        return new CustomerGroupState(Optional.of(customerGroup));
    }

    public Status getStatus() {
        return customerGroup.map(CustomerGroup::getStatus).orElse(Status.NOT_CREATED);
    }

    public CustomerGroupState updateDetails(CustomerGroup details) {
        return update(i -> i.withDetails(details));
    }
    public CustomerGroupState updateDetails(boolean value) {
        return update(i -> i.withDetails(value));
    }

    private CustomerGroupState update(Function<CustomerGroup, CustomerGroup> updateFunction) {
        assert customerGroup.isPresent();
        return new CustomerGroupState(customerGroup.map(updateFunction));
    }

}
