package com.aie.arc.account.impl.state;

import com.aie.arc.account.impl.entity.entities.ErrorProcessor;
import com.aie.arc.account.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.CompressedJsonable;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.util.Optional;
@Value
@JsonDeserialize
public class ErrorProcessorState implements CompressedJsonable {
    public final Optional<ErrorProcessor> errorProcessorVO;
    @JsonCreator
    public ErrorProcessorState(Optional<ErrorProcessor> errorProcessorVO) {
        this.errorProcessorVO = errorProcessorVO;
    }

    public static ErrorProcessorState empty() {
        return new ErrorProcessorState(Optional.empty());
    }

    public static ErrorProcessorState create(ErrorProcessor errorProcessorVO) {
        return new ErrorProcessorState(Optional.of(errorProcessorVO));
    }

    public Status getStatus() {
        return Status.NOT_CREATED;
    }

}