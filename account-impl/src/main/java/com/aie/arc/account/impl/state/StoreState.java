package com.aie.arc.account.impl.state;

import com.aie.arc.account.impl.entity.entities.Store;
import com.aie.arc.account.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.CompressedJsonable;
import lombok.Value;

import java.util.Optional;
import java.util.function.Function;
@Value
@JsonDeserialize
public class StoreState implements CompressedJsonable {
    public  final transient  Optional<Store> storeVO;

    @JsonCreator
    public StoreState(Optional<Store> storeVO) {
        this.storeVO = storeVO;
    }

    public static StoreState empty() {
        return new StoreState(Optional.empty());
    }

    public static StoreState create(Store storeVO) {
        return new StoreState(Optional.of(storeVO));
    }

    public Status getStatus() {
        return storeVO.map(Store::getStatus).orElse(Status.NOT_CREATED);
    }

    public StoreState updateDetails(Store details) {
        return update(i -> i.withDetails(details));
    }

    private StoreState update(Function<Store, Store> updateFunction) {
        assert storeVO.isPresent();
        return new StoreState(storeVO.map(updateFunction));
    }
}