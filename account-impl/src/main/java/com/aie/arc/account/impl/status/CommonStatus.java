package com.aie.arc.account.impl.status;

public enum CommonStatus {
    /**
     * The Entry has been created.
     */
    CREATED
}
