package com.aie.arc.account.impl.util;

import play.Logger;
import play.Logger.ALogger;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.GZIPInputStream;

public class LogUtil {

    private static final ALogger accessLogger = Logger.of("access");
    private static final String LOG_FILE_PATH = "lockProduct-impl/logs/access.log";
    private static final String ABSLT_PATH = "lockProduct-impl/logs";

    public static void log(String logMessage) {
        accessLogger.info(logMessage);
    }

    private LogUtil()
    {

    }

    public static String fetchLog(String date) {
        BufferedReader br = null;
        try {
            String sCurrentLine;
            String fileName = "arch-access/access-log-xxx.gz";
            StringBuilder strBuilder = new StringBuilder();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = dateFormat.format(new Date());
            if (date.equals(strDate)) {
                try(FileReader fileReader = new FileReader(LOG_FILE_PATH);BufferedReader bufferedReader = new BufferedReader(fileReader))
                {
                    br = bufferedReader;
                }
                catch (Exception e)
                {
                    accessLogger.error("Error occurred in if block :"+e);
                }
            } else {
                fileName = fileName.replace("xxx", date);
                try(InputStream fileInputStream = new FileInputStream(ABSLT_PATH + File.separatorChar + fileName);
                InputStream gzipInputStream = new GZIPInputStream(fileInputStream); Reader inputStreamReader = new InputStreamReader(gzipInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader))
                {
                    br = bufferedReader;
                }
                catch (Exception e)
                {
                    accessLogger.error("Error occurred in else block :"+e);
                }
            }
            while (br != null && (sCurrentLine = br.readLine()) != null) {
                strBuilder.append("\n").append(sCurrentLine);
            }
            return strBuilder.toString();
        } catch (FileNotFoundException e) {
            accessLogger.error("Error occurred in FileNotFoundException catch block :"+e);
        } catch (IOException e) {
            accessLogger.error("Error occurred in IOException catch block :"+e);
        }
        return "no recodrs found";
    }

}