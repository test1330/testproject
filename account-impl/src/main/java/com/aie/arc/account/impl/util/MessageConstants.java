package com.aie.arc.account.impl.util;

public class MessageConstants {
    private MessageConstants()
    {

    }
    // Common MessageConstants
    public static final String CONVERTING_NON_PUB_EVENT = "Converting non public event";
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    public static final String LOG_RESPONSE = "RES:  ";
    public static final Integer SUCCESS_STATUS = 200;
    public static final Integer STATUS_BAD_REQUEST = 400;
    public static final String ACCOUNT_TYPE_BUSINESS = "Business";
    public static final String ACCOUNT_TYPE_MEMBER = "member";
    public static final String ACCOUNT_TYPE_WARRANTECH = "warrantech";
    public static final String DATE_TIME_FORMAT="yyyy-MM-dd HH:mm:ss";
    public static final String IS_NOT_VALID = "is not valid.";
    public static final String ADDRESS_ID="address_id";
    public static final String ADDRESS="address";
    public static final String CARD_ID="card_id";
    public static final String ZERO = "0";
    public static final Integer ONE = 1;
    public static final int FOUR = 4;
    public static final String CARD_TYPE_BM = "BM";
    public static final String NICK_NAME_ENDING_WITH = " ending in ";
    public static final String AM_EVENT_UPDATE = "update";
    public static final String UNKNOWN_IP = "unknown";
    public static final String FILTER_CONFIG_ACTIVE = "active";
    public static final String FILTER_CONFIG_EMAIL = "email";
    public static final String SERVICE_ENABLED = "enabled";

    public static final String ENABLE_ARC_EMAIL = "enable.arc.email";
    public static final String CONFIG_EMAIL_SERVICE = "config.email.service";
    public static final String CONFIG_GMAIL_SERVICE = "config.gmail.service";
    public static final String MAIL_SMTP_HOST = "mail.smtp.host";
    public static final String MAIL_SMTP_PORT = "mail.smtp.port";
    public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
    public static final String MAIL_SMTP_TLS_ENABLE = "mail.smtp.starttls.enable";

    public static final String MAIL_SMTP_SOCKET_FACTORY_PORT = "mail.smtp.socketFactory.port";
    public static final String MAIL_SMTP_SOCKET_FACTORY_CLASS = "mail.smtp.socketFactory.class";
    public static final String USER_NAME = "userName";
    public static final String PASSWORD = "password";
    public static final String FROM_ADDRESS = "mailFromAddress";
    public static final String FROM_EMAIL_ADDRESS = "fromEmailAddress";
    public static final String CREDENTIALS_PATH = "credential.path";
    public static final String EMAIL_CONTENT_TYPE = "text/html";
    public static final String USER_DIRECTORY = "user.dir";
    public static final String EMAIL_TEMPLATE_PATH = "\\account-impl\\src\\main\\java\\com\\aie\\arc\\account\\impl\\util\\mailtemplates";
    public static final String CREDIT_CARD_UPDATE_EMAIL = "Credit Card Update Notification";
    public static final String ADDRESS_CHANGE_EMAIL = "Address Change Notification";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String AM_EVENT_CREATE = "create";
    public static final String AM_EVENT_DELETE = "delete";
    public static final String COMMON = "Common";
    public static final String AVALARA_SERVICE_FAILURE_STATUS = "Error500";
    public static final String AVALARA_SERVICE_BAD_REQUEST_STATUS = "Error400";
    public static final String ADDRESS_OUT_OF_RANGE="The address number is out of range";
    public static final String BRANDSMART_UNABLE_TO_VALIDATE_ADDRESS="BrandsMart USA is unable to validate this address. " +
            "For your protection, please provide a different, valid shipping address.";
    public static final String AVALARA_SERVICE_FAILURE_MESSAGE = "Oops, Please try again later, presently service not available.";
    public static final Integer INTEGER_ZERO = 0;
    public static final Integer INTEGER_FIVE = 5;
    public static final String FIRST_DATA_CURRENCY_CODE =  "840";
    public static final String FIRST_DATA_NO_ORDER = "NoOrder";
    public static final String FIRST_DATA_AMOUNT = "0.00";
    public static final String FIRST_DATA_ACCOUNT_NUMBER_DESCRIPTION = "PAN";
    public static final String FIRST_DATA_TRANSACTION_TYPE_ECI_INDICATOR_SECURE = "7";
    public static final String FIRST_DATA_TOKEN_TYPE = "1263";
    public static final String FIRST_DATA_DIVISION_NUMBER = "801655";
    public static final String FIRST_DATA_USER_ID = "000132";
    public static final String FIRST_DATA_PASSWORD = "FD000132";
    public static final String CARD_FAILURE_MESSAGE = "Error207: Please enter a valid credit card number.";
    public static final String COMMON_ERROR_CODE = ": Error707: ";

    // log constants
    public static final String PUBLISHED_CARD_ADDED_TO_ACCOUNT_EVENT = "Published CardAddedToAccount event";
    public static final String PUBLISHED_CARD_UPDATED_TO_ACCOUNT_EVENT = "Published CardUpdatedToAccount event";

    public static final String CONVERT_EVENT_ACCOUNT_UPDATE = "update";
    public static final String ACCOUNT_NOT_FOUND = "Account not found.";

   public static final String STR_LOG_MIGRATION_ACCOUNT_MSG = "Skip the Account event : {} for migration with accountId : {}";

   public static final int MAX_ATTEMPTS = 3;

   public static final String BMCIS_SCHEDULER_FLAG = "bmcis.scheduler.enabled";

    public static final String CONFIG_PATH_FRESH_ADDRESS = "config.externalservices.freshaddress";
    public static final String FRESH_ADDRESS_COMPANY = "company";
    public static final String FRESH_ADDRESS_CONTRACT = "contract";
    public static final String FRESH_ADDRESS_SERVICE = "service";
    public static final String CREATE_CUSTOMER_ACTION = "http://brandsmartusa.com/Services/AtgWebBroker/AtgWebBroker/CreateCustomer";

}