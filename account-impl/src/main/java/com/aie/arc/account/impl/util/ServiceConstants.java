package com.aie.arc.account.impl.util;

import java.time.Duration;

public interface ServiceConstants {
    Duration ASK_TIMEOUT = Duration.ofSeconds(30);
}
