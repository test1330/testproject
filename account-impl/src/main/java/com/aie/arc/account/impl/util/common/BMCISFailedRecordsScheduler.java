package com.aie.arc.account.impl.util.common;

import akka.actor.ActorSystem;
import com.aie.arc.account.impl.service.AccountManager;
import com.typesafe.config.Config;
import lombok.extern.slf4j.Slf4j;
import scala.concurrent.ExecutionContext;
import scala.concurrent.duration.Duration;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.TimeUnit;

import static com.aie.arc.account.impl.util.MessageConstants.BMCIS_SCHEDULER_FLAG;

@Slf4j
@Singleton
public class BMCISFailedRecordsScheduler {
    private final ActorSystem actorSystem;
    private final ExecutionContext executionContext;
    private AccountManager accountManager;
    private final Config config;


    @Inject
    public BMCISFailedRecordsScheduler(Config config, ActorSystem actorSystem, ExecutionContext executionContext, AccountManager accountManager) {
        this.actorSystem = actorSystem;
        this.executionContext = executionContext;
        this.accountManager = accountManager;
        this.config = config;
        log.debug("BMCIS_SCHEDULER_FLAG : {}", config.getBoolean(BMCIS_SCHEDULER_FLAG));
        if (config.getBoolean(BMCIS_SCHEDULER_FLAG) == true) {
            this.initialize();
        }else{
            log.info("BMCIS_SCHEDULER_FLAG is not enabled : {}", config.getBoolean(BMCIS_SCHEDULER_FLAG));
        }
    }

    private void initialize() {
        log.debug("initializing BMCISFailedRecordsScheduler");
        this.actorSystem
                .scheduler()
                .schedule(
                        Duration.create(config.getInt("bmcisFailedRecordsJobInitialDelay"), TimeUnit.SECONDS), // initialDelay
                        Duration.create(config.getInt("bmcisFailedRecordsJobInterval"), TimeUnit.SECONDS), // interval
                        accountManager::runBMCISJob,
                        this.executionContext);
    }
}
