package com.aie.arc.account.impl.util.common;

import com.aie.arc.account.impl.entity.entities.vo.common.FilterType;
import com.aie.arc.account.impl.entity.entities.vo.common.MappingData;
import static com.aie.arc.account.impl.util.MessageConstants.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class FilterConfig {
    private FilterConfig() {

    }

    private static final Map<String, MappingData> accountTypeFieldMap = getAccountTypeFieldMap();
    private static final Map<String, MappingData> businessUserTypeFieldMap = getBusinessUserTypeFieldMap();
    private static final Map<String, MappingData> addressTypeFieldMap = getAddressTypeFieldMap();
    private static final Map<String, MappingData> cardTypeFieldMap = getCardTypeFieldMap();
    private static final Map<String, MappingData> customerGroupTypeFieldMap = getCustomerGroupTypeFieldMap();

    private static final Map<String, MappingData> roleGroupTypeFieldMap = getRoleGroupTypeFieldMap();
    private static final Map<String, MappingData> roleTypeFieldMap = getRoleTypeFieldMap();
    private static final Map<String, MappingData> permissionTypeFieldMap = getPermissionTypeFieldMap();
    private static final Map<String, MappingData> microServiceTypeFieldMap = getMicroServiceTypeFieldMap();
    private static final Map<String, MappingData> apiTypeFieldMap = getApiTypeFieldMap();
    private static final Map<String, MappingData> workspaceTypeFieldMap = getWorkspaceTypeFieldMap();
    private static final Map<String, MappingData> uiPermissionTypeFieldMap = getUiPermissionTypeFieldMap();
    private static final Map<String, MappingData> accessRightTypeFieldMap = getAccessRightTypeFieldMap();


    private static Map<String, MappingData> getAccountTypeFieldMap() {
        Map<String, MappingData> fieldMap = new HashMap<>();
        fieldMap.put("firstName", new MappingData("first_name", FilterType.STRING));
        fieldMap.put("lastName", new MappingData("last_name", FilterType.STRING));
        fieldMap.put(FILTER_CONFIG_EMAIL, new MappingData(FILTER_CONFIG_EMAIL, FilterType.STRING));
        fieldMap.put(FILTER_CONFIG_ACTIVE, new MappingData(FILTER_CONFIG_ACTIVE, FilterType.BOOLEAN));
        return Collections.unmodifiableMap(fieldMap);
    }

    private static Map<String, MappingData> getBusinessUserTypeFieldMap() {
        Map<String, MappingData> businessFieldMap = new HashMap<>();
        businessFieldMap.put("firstName", new MappingData("first_name", FilterType.STRING));
        businessFieldMap.put("lastName", new MappingData("last_name", FilterType.STRING));
        businessFieldMap.put(FILTER_CONFIG_EMAIL, new MappingData(FILTER_CONFIG_EMAIL, FilterType.STRING));
        businessFieldMap.put(FILTER_CONFIG_ACTIVE, new MappingData(FILTER_CONFIG_ACTIVE, FilterType.BOOLEAN));
        return Collections.unmodifiableMap(businessFieldMap);
    }

    private static Map<String, MappingData> getAddressTypeFieldMap() {
        Map<String, MappingData> fieldMap = new HashMap<>();
        fieldMap.put("company", new MappingData("company", FilterType.STRING));
        fieldMap.put(FILTER_CONFIG_ACTIVE, new MappingData(FILTER_CONFIG_ACTIVE, FilterType.BOOLEAN));
        return Collections.unmodifiableMap(fieldMap);
    }

    private static Map<String, MappingData> getCardTypeFieldMap() {
        Map<String, MappingData> fieldMap = new HashMap<>();
        fieldMap.put("name", new MappingData("name", FilterType.STRING));
        fieldMap.put("flatNumber", new MappingData("flat_number", FilterType.STRING));
        fieldMap.put(FILTER_CONFIG_ACTIVE, new MappingData(FILTER_CONFIG_ACTIVE, FilterType.BOOLEAN));
        return Collections.unmodifiableMap(fieldMap);
    }

    private static Map<String, MappingData> getCustomerGroupTypeFieldMap() {
        Map<String, MappingData> fieldMap = new HashMap<>();
        fieldMap.put("name", new MappingData("name", FilterType.STRING));
        fieldMap.put(FILTER_CONFIG_ACTIVE, new MappingData(FILTER_CONFIG_ACTIVE, FilterType.BOOLEAN));
        return Collections.unmodifiableMap(fieldMap);
    }

    private static Map<String, MappingData> getRoleGroupTypeFieldMap() {
        Map<String, MappingData> groupFieldMap = new HashMap<>();
        groupFieldMap.put("name", new MappingData("name", FilterType.STRING));
        groupFieldMap.put(FILTER_CONFIG_ACTIVE, new MappingData(FILTER_CONFIG_ACTIVE, FilterType.BOOLEAN));
        return Collections.unmodifiableMap(groupFieldMap);
    }

    private static Map<String, MappingData> getRoleTypeFieldMap() {
        Map<String, MappingData> roleFieldMap = new HashMap<>();
        roleFieldMap.put("name", new MappingData("name", FilterType.STRING));
        roleFieldMap.put(FILTER_CONFIG_ACTIVE, new MappingData(FILTER_CONFIG_ACTIVE, FilterType.BOOLEAN));
        return Collections.unmodifiableMap(roleFieldMap);
    }

    private static Map<String, MappingData> getPermissionTypeFieldMap() {
        Map<String, MappingData> perFieldMap = new HashMap<>();
        perFieldMap.put("name", new MappingData("name", FilterType.STRING));
        perFieldMap.put(FILTER_CONFIG_ACTIVE, new MappingData(FILTER_CONFIG_ACTIVE, FilterType.BOOLEAN));
        return Collections.unmodifiableMap(perFieldMap);
    }

    private static Map<String, MappingData> getUiPermissionTypeFieldMap() {
        Map<String, MappingData> perFieldMap = new HashMap<>();
        perFieldMap.put("name", new MappingData("name", FilterType.STRING));
        perFieldMap.put(FILTER_CONFIG_ACTIVE, new MappingData(FILTER_CONFIG_ACTIVE, FilterType.BOOLEAN));
        return Collections.unmodifiableMap(perFieldMap);
    }

    private static Map<String, MappingData> getWorkspaceTypeFieldMap() {
        Map<String, MappingData> perFieldMap = new HashMap<>();
        perFieldMap.put("name", new MappingData("name", FilterType.STRING));
        perFieldMap.put(FILTER_CONFIG_ACTIVE, new MappingData(FILTER_CONFIG_ACTIVE, FilterType.BOOLEAN));
        return Collections.unmodifiableMap(perFieldMap);
    }

    private static Map<String, MappingData> getMicroServiceTypeFieldMap() {
        Map<String, MappingData> microFieldMap = new HashMap<>();
        microFieldMap.put("name", new MappingData("name", FilterType.STRING));
        microFieldMap.put(FILTER_CONFIG_ACTIVE, new MappingData(FILTER_CONFIG_ACTIVE, FilterType.BOOLEAN));
        return Collections.unmodifiableMap(microFieldMap);
    }

    private static Map<String, MappingData> getApiTypeFieldMap() {
        Map<String, MappingData> apiFieldMap = new HashMap<>();
        apiFieldMap.put("name", new MappingData("name", FilterType.STRING));
        apiFieldMap.put(FILTER_CONFIG_ACTIVE, new MappingData(FILTER_CONFIG_ACTIVE, FilterType.BOOLEAN));
        return Collections.unmodifiableMap(apiFieldMap);
    }

    private static Map<String, MappingData> getAccessRightTypeFieldMap() {
        Map<String, MappingData> fieldMap = new HashMap<>();
        fieldMap.put("name", new MappingData("name", FilterType.STRING));
        fieldMap.put("active", new MappingData("active", FilterType.BOOLEAN));
        return Collections.unmodifiableMap(fieldMap);

    }

    public static Map<String, MappingData> getAccountTypeMap() {
        return accountTypeFieldMap;
    }

    public static Map<String, MappingData> getBusinessUserTypeMap() {
        return businessUserTypeFieldMap;
    }

    public static Map<String, MappingData> getAddressTypeMap() {
        return addressTypeFieldMap;
    }

    public static Map<String, MappingData> getCardTypeMap() {
        return cardTypeFieldMap;
    }

    public static Map<String, MappingData> getCustomerGroupTypeMap() {
        return customerGroupTypeFieldMap;
    }

    public static Map<String, MappingData> getRoleGroupTypeMap() {
        return roleGroupTypeFieldMap;
    }

    public static Map<String, MappingData> getRoleTypeMap() {
        return roleTypeFieldMap;
    }

    public static Map<String, MappingData> getPermissionTypeMap() {
        return permissionTypeFieldMap;
    }


    public static Map<String, MappingData> getMicroServiceTypeMap() {
        return microServiceTypeFieldMap;
    }

    public static Map<String, MappingData> getApiFieldTypeMap() {
        return apiTypeFieldMap;
    }

    public static Map<String, MappingData> getWorkspaceMap() {
        return workspaceTypeFieldMap;

    }
    public static Map<String, MappingData> getUiPermissionTypeMap() {
        return uiPermissionTypeFieldMap;
    }

    public static Map<String, MappingData> getAccessRightTypeMap() { return accessRightTypeFieldMap; }
}
