package com.aie.arc.account.impl.util.common;


import com.aie.arc.account.api.common.exceptions.ParameterRequired;
import org.apache.commons.lang3.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class RepositoryUtil {

    private static final String WHERE = " WHERE " ;
    private static final String AND = " AND ";
    private static final String ORDER_BY = " ORDER BY ";

    private RepositoryUtil()
    {

    }
    public static StringBuilder appendStringParameter(StringBuilder builder, String column, String value) {
        if (StringUtils.isBlank(value)) return builder;
        return Optional.of(builder)
                .filter(RepositoryUtil::isEmpty)
                .map(b -> builder.append(WHERE + column + " = '" + value + "' "))
                .orElseGet(() -> builder.append(AND + column + " = '" + value + "' "));
    }

    public static StringBuilder appendInParameter(StringBuilder builder, String column, String value) {
        if (StringUtils.isBlank(value)) return builder;
        return Optional.of(builder)
                .filter(RepositoryUtil::isEmpty)
                .map(b -> builder.append(WHERE + column + " in( " + value + ") "))
                .orElseGet(() -> builder.append(AND + column + " in (" + value + ") "));
    }

    public static StringBuilder addIntegerFieldInDynamicQuery(StringBuilder builder, String column, String value) {
        if (value == null) return builder;
        int integer = Integer.parseInt(value);
        return Optional.of(builder)
                .filter(RepositoryUtil::isEmpty)
                .map(b -> builder.append(WHERE + column + " = " + integer + " "))
                .orElseGet(() -> builder.append(AND + column + " = " + integer + " "));
    }

    public static StringBuilder appendBooleanParameter(StringBuilder builder, String field, boolean value) {
        if (isEmpty(builder)) {
            builder.append(WHERE + field + " = " + value + " ");
        } else {
            builder.append(AND + field + " = " + value + " ");
        }
        return builder;
    }

    public static StringBuilder appendBooleanParameter(StringBuilder builder, String column, String value) {
        if (value == null) return builder;
        boolean booleanValue = Boolean.parseBoolean(value);
        return Optional.of(builder)
                .filter(RepositoryUtil::isEmpty)
                .map(b -> builder.append(WHERE + column + " = " + booleanValue + " "))
                .orElseGet(() -> builder.append(AND + column + " = " + booleanValue + " "));
    }

    public static StringBuilder addSortColumnInDynamicQuery(StringBuilder builder, String field, String ascending) {
        if (ascending == null) return builder;
        return Optional.of(builder)
                .filter(b -> Boolean.valueOf(ascending))
                .map(b -> builder.append(ORDER_BY + field + " ASC "))
                .orElseGet(() -> builder.append(ORDER_BY + field + " DESC "));
    }

    public static boolean isEmpty(StringBuilder input) {
        return input.length() == 0;
    }

    public static int getCount(Connection connection, StringBuilder dynamicQuery, String tableName, String columnName) {
        try (PreparedStatement statement = connection.prepareStatement(new StringBuilder("SELECT COUNT(DISTINCT " + columnName + ")" +
                " as rowCount FROM " + tableName).append(dynamicQuery).toString()); ResultSet countRes = statement.executeQuery()) {
            countRes.next();
            return countRes.getInt("rowCount");
        } catch (SQLException e) {
            throw new ParameterRequired(new String[]{"SQL Exception encountered!"});
        }
    }
}