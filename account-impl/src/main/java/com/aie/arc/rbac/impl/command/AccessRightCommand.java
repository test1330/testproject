package com.aie.arc.rbac.impl.command;

import akka.Done;
import akka.actor.typed.ActorRef;
import com.aie.arc.rbac.impl.entity.entities.AccessRight;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.util.Optional;

public interface AccessRightCommand extends Jsonable {
    @Value
    @JsonDeserialize
    final class CreateAccessRight implements AccessRightCommand {
        public final AccessRight accessRight;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public CreateAccessRight(AccessRight accessRight, ActorRef<Done> replyTo) {
            this.accessRight = accessRight;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateAccessRight implements AccessRightCommand {
        public final AccessRight accessRight;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateAccessRight(AccessRight accessRight, ActorRef<Done> replyTo) {
            this.accessRight = accessRight;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class DeactivateAccessRight implements AccessRightCommand {
        public final String accessId;
        public final boolean active;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public DeactivateAccessRight(String accessId, boolean active, ActorRef<Done> replyTo) {
            this.accessId = accessId;
            this.active = active;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class ActivateAccessRight implements AccessRightCommand {
        public final String accessId;
        public final boolean active;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public ActivateAccessRight(String accessId, boolean active, ActorRef<Done> replyTo) {
            this.accessId = accessId;
            this.active = active;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class GetAccessRight implements AccessRightCommand {
        public final ActorRef<Optional<AccessRight>> replyTo;

        @JsonCreator
        public GetAccessRight(ActorRef<Optional<AccessRight>> replyTo) {
            this.replyTo = replyTo;
        }
    }
}
