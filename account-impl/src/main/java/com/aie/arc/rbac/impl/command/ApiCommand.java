package com.aie.arc.rbac.impl.command;

import akka.Done;
import akka.actor.typed.ActorRef;
import com.aie.arc.rbac.impl.entity.entities.Api;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.util.Optional;

public interface ApiCommand extends Jsonable {
    @Value
    @JsonDeserialize
    final class AddAPI implements ApiCommand {
        public final Api api;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public AddAPI(Api api, ActorRef<Done> replyTo) {
            this.api = api;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateAPI implements ApiCommand {
        public final Api api;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateAPI(Api api, ActorRef<Done> replyTo) {
            this.api = api;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class ActivateAPI implements ApiCommand {
        public final String apiId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public ActivateAPI(String apiId, boolean value, ActorRef<Done> replyTo) {
            this.apiId = apiId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class DeactivateAPI implements ApiCommand {
        public final String apiId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public DeactivateAPI(String apiId, boolean value, ActorRef<Done> replyTo) {
            this.apiId = apiId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class GetAPI implements ApiCommand {
        public final ActorRef<Optional<Api>> replyTo;

        @JsonCreator
        public GetAPI(ActorRef<Optional<Api>> replyTo) {
            this.replyTo = replyTo;
        }
    }
}
