package com.aie.arc.rbac.impl.command;

import akka.Done;
import akka.actor.typed.ActorRef;
import com.aie.arc.rbac.impl.entity.entities.MicroService;
import com.aie.arc.rbac.impl.entity.entities.RBACLocalizedFieldsVO;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.util.Map;
import java.util.Optional;

public interface MicroServiceCommand extends Jsonable {
    @Value
    @JsonDeserialize
    final class CreateMicroService implements MicroServiceCommand {
        public final MicroService microservice;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public CreateMicroService(MicroService microservice, ActorRef<Done> replyTo) {
            this.microservice = microservice;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateMicroService implements MicroServiceCommand {
        public final MicroService microservice;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateMicroService(MicroService microservice, ActorRef<Done> replyTo) {
            this.microservice = microservice;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class ActivateMicroService implements MicroServiceCommand {
        public final String microServiceId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public ActivateMicroService(String microServiceId, boolean value, ActorRef<Done> replyTo) {
            this.microServiceId = microServiceId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class DeactivateMicroService implements MicroServiceCommand {
        public final String microServiceId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public DeactivateMicroService(String microServiceId, boolean value, ActorRef<Done> replyTo) {
            this.microServiceId = microServiceId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class AddApiToMicroService implements MicroServiceCommand {
        public final String microServiceId;
        public final String apiId;
        public final Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap;
        public final boolean active;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public AddApiToMicroService(String microServiceId, String apiId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, boolean active, ActorRef<Done> replyTo) {
            this.microServiceId = microServiceId;
            this.apiId = apiId;
            this.localizedFieldsVOMap = localizedFieldsVOMap;
            this.active = active;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateApiToMicroService implements MicroServiceCommand {
        public final String microServiceId;
        public final String apiId;
        public final Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap;
        public final boolean active;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateApiToMicroService(String microServiceId, String apiId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, boolean active, ActorRef<Done> replyTo) {
            this.microServiceId = microServiceId;
            this.apiId = apiId;
            this.localizedFieldsVOMap = localizedFieldsVOMap;
            this.active = active;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class DeactivateApiToMicroService implements MicroServiceCommand {
        public final String microServiceId;
        public final String apiId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public DeactivateApiToMicroService(String microServiceId, String apiId, boolean value, ActorRef<Done> replyTo) {
            this.microServiceId = microServiceId;
            this.apiId = apiId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }
    @Value
    @JsonDeserialize
    final class ActivateApiToMicroService implements MicroServiceCommand {
        public final String microServiceId;
        public final String apiId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public ActivateApiToMicroService(String microServiceId, String apiId, boolean value, ActorRef<Done> replyTo) {
            this.microServiceId = microServiceId;
            this.apiId = apiId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class GetMicroService implements MicroServiceCommand {
        public final ActorRef<Optional<MicroService>> replyTo;

        @JsonCreator
        public GetMicroService(ActorRef<Optional<MicroService>> replyTo) {
            this.replyTo = replyTo;
        }
    }
}
