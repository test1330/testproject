package com.aie.arc.rbac.impl.command;

import akka.Done;
import akka.actor.typed.ActorRef;
import com.aie.arc.rbac.impl.entity.entities.Permission;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.util.Optional;

public interface PermissionCommand extends Jsonable {
    @Value
    @JsonDeserialize
    final class CreatePermission implements PermissionCommand {
        public final Permission permission;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public CreatePermission(Permission permission, ActorRef<Done> replyTo) {
            this.permission = permission;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdatePermission implements PermissionCommand {
        public final Permission permission;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdatePermission(Permission permission, ActorRef<Done> replyTo) {
            this.permission = permission;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class ActivatePermission implements PermissionCommand {
        public final String permissionId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public ActivatePermission(String permissionId, boolean value, ActorRef<Done> replyTo) {
            this.permissionId = permissionId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class DeactivatePermission implements PermissionCommand {
        public final String permissionId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public DeactivatePermission(String permissionId, boolean value, ActorRef<Done> replyTo) {
            this.permissionId = permissionId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class GetPermission implements PermissionCommand {
        public final ActorRef<Optional<Permission>> replyTo;

        @JsonCreator
        public GetPermission(ActorRef<Optional<Permission>> replyTo) {
            this.replyTo = replyTo;
        }
    }
}
