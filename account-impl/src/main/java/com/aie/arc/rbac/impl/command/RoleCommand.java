package com.aie.arc.rbac.impl.command;

import akka.Done;
import akka.actor.typed.ActorRef;
import com.aie.arc.rbac.impl.entity.entities.Role;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.util.Optional;

/*command for create ,update and get Role*/
public interface RoleCommand extends Jsonable {

    @Value
    @JsonDeserialize
    final class CreateRole implements RoleCommand {
        public final Role role;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public CreateRole(Role role, ActorRef<Done> replyTo) {
            this.role = role;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateRole implements RoleCommand {
        public final Role role;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateRole(Role role, ActorRef<Done> replyTo) {
            this.role = role;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class ActivateRole implements RoleCommand {
        public final String roleId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public ActivateRole(String roleId, boolean value, ActorRef<Done> replyTo) {
            this.roleId = roleId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class DeactivateRole implements RoleCommand {
        public final String roleId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public DeactivateRole(String roleId, boolean value, ActorRef<Done> replyTo) {
            this.roleId = roleId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class GetRole implements RoleCommand {
        public final ActorRef<Optional<Role>> replyTo;

        @JsonCreator
        public GetRole(ActorRef<Optional<Role>> replyTo) {
            this.replyTo = replyTo;
        }
    }
}
