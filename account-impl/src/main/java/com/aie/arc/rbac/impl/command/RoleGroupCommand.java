package com.aie.arc.rbac.impl.command;

import akka.Done;
import akka.actor.typed.ActorRef;
import com.aie.arc.rbac.impl.entity.entities.RoleGroup;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.util.Optional;

public interface RoleGroupCommand extends Jsonable {
    @Value
    @JsonDeserialize
    final class CreateRoleGroup implements RoleGroupCommand {
        public final RoleGroup roleGroup;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public CreateRoleGroup(RoleGroup roleGroup, ActorRef<Done> replyTo) {
            this.roleGroup = roleGroup;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateRoleGroup implements RoleGroupCommand {
        public final RoleGroup roleGroup;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateRoleGroup(RoleGroup roleGroup, ActorRef<Done> replyTo) {
            this.roleGroup = roleGroup;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class ActivateRoleGroup implements RoleGroupCommand {
        public final String roleGroupId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public ActivateRoleGroup(String roleGroupId, boolean value, ActorRef<Done> replyTo) {
            this.roleGroupId = roleGroupId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class DeactivateRoleGroup implements RoleGroupCommand {
        public final String roleGroupId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public DeactivateRoleGroup(String roleGroupId, boolean value, ActorRef<Done> replyTo) {
            this.roleGroupId = roleGroupId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }
    @Value
    @JsonDeserialize
    final class GetRoleGroup implements RoleGroupCommand {
        public final ActorRef<Optional<RoleGroup>> replyTo;

        @JsonCreator
        public GetRoleGroup(ActorRef<Optional<RoleGroup>> replyTo) {
            this.replyTo = replyTo;
        }
    }
}
