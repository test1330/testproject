package com.aie.arc.rbac.impl.command;

import akka.Done;
import akka.actor.typed.ActorRef;
import com.aie.arc.rbac.impl.entity.entities.Workspace;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.util.Optional;

public interface WorkspaceCommand extends Jsonable {
    @Value
    @JsonDeserialize
    final class CreateWorkspace implements WorkspaceCommand{
        public final Workspace workspace;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public CreateWorkspace(Workspace workspace, ActorRef<Done> replyTo) {
            this.workspace = workspace;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class UpdateWorkspace implements WorkspaceCommand{
        public final Workspace workspace;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public UpdateWorkspace(Workspace workspace, ActorRef<Done> replyTo) {
            this.workspace = workspace;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class DeactivateWorkspace implements WorkspaceCommand {
        public final String workspaceId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public DeactivateWorkspace(String workspaceId, boolean value, ActorRef<Done> replyTo) {
            this.workspaceId = workspaceId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }

    @Value
    @JsonDeserialize
    final class ActivateWorkspace implements WorkspaceCommand {
        public final String workspaceId;
        public final boolean value;
        public final ActorRef<Done> replyTo;

        @JsonCreator
        public ActivateWorkspace(String workspaceId, boolean value, ActorRef<Done> replyTo) {
            this.workspaceId = workspaceId;
            this.value = value;
            this.replyTo = replyTo;
        }
    }
    @Value
    @JsonDeserialize
    final class GetWorkspace implements WorkspaceCommand{
        public final ActorRef< Optional<Workspace>> replyTo;

        @JsonCreator
        public GetWorkspace(ActorRef<Optional<Workspace>> replyTo) {
            this.replyTo = replyTo;
        }
    }
}
