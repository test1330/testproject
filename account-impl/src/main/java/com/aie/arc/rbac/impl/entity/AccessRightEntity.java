package com.aie.arc.rbac.impl.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.rbac.impl.command.AccessRightCommand;
import com.aie.arc.rbac.impl.event.AccessRightEvent;
import com.aie.arc.rbac.impl.state.AccessRightState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;


public class AccessRightEntity extends EventSourcedBehaviorWithEnforcedReplies<AccessRightCommand, AccessRightEvent, AccessRightState> {

    final private EntityContext<AccessRightCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<AccessRightCommand> ENTITY_TYPE_KEY = EntityTypeKey
                    .create(AccessRightCommand.class, "AccessRightEntity");

    AccessRightEntity(EntityContext<AccessRightCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public AccessRightState emptyState() {
        return AccessRightState.empty();
    }

    @Override
    public CommandHandlerWithReply<AccessRightCommand, AccessRightEvent, AccessRightState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(AccessRightCommand.GetAccessRight.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.accessRight))

                .onCommand(AccessRightCommand.CreateAccessRight.class, (state, cmd) -> Effect()
                        .persist(new AccessRightEvent.AccessRightCreated(cmd.getAccessRight()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccessRightCommand.UpdateAccessRight.class, (state, cmd) -> Effect()
                        .persist(new AccessRightEvent.AccessRightUpdated(cmd.getAccessRight()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccessRightCommand.DeactivateAccessRight.class, (state, cmd) -> Effect()
                        .persist(new AccessRightEvent.AccessRightDeactivated(cmd.getAccessId(), cmd.isActive()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(AccessRightCommand.ActivateAccessRight.class, (state, cmd) -> Effect()
                        .persist(new AccessRightEvent.AccessRightActivated(cmd.getAccessId(), cmd.isActive()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<AccessRightState, AccessRightEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(AccessRightEvent.AccessRightCreated.class, (state, evt) -> AccessRightState.create(evt.getAccessRight()))
                .onEvent(AccessRightEvent.AccessRightUpdated.class, (state, evt) -> state.updateDetails(evt.getAccessRight()))
                .onEvent(AccessRightEvent.AccessRightActivated.class, (state, evt) -> state.updateDetails(evt.isActive()))
                .onEvent(AccessRightEvent.AccessRightDeactivated.class, (state, evt) -> state.updateDetails(evt.isActive()))
                .build();
    }

    @Override
    public Set<String> tagsFor(AccessRightEvent accessRightEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, AccessRightEvent.TAG).apply(accessRightEvent);
    }

    public static Behavior<AccessRightCommand> create(EntityContext<AccessRightCommand> entityContext) {
        return new AccessRightEntity(entityContext);
    }

}
