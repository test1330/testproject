package com.aie.arc.rbac.impl.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.rbac.impl.command.ApiCommand;
import com.aie.arc.rbac.impl.event.ApiEvent;
import com.aie.arc.rbac.impl.state.ApiState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;

public class ApiEntity extends EventSourcedBehaviorWithEnforcedReplies<ApiCommand, ApiEvent, ApiState> {

    final private EntityContext<ApiCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<ApiCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(ApiCommand.class, "ApiEntity");

    ApiEntity(EntityContext<ApiCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public ApiState emptyState() {
        return ApiState.empty();
    }

    @Override
    public CommandHandlerWithReply<ApiCommand, ApiEvent, ApiState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(ApiCommand.GetAPI.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.api))

                .onCommand(ApiCommand.AddAPI.class, (state, cmd) -> Effect()
                        .persist(new ApiEvent.ApiAdded(cmd.getApi()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(ApiCommand.UpdateAPI.class, (state, cmd) -> Effect()
                        .persist(new ApiEvent.ApiUpdated(cmd.getApi()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(ApiCommand.DeactivateAPI.class, (state, cmd) -> Effect()
                        .persist(new ApiEvent.ApiDeactivated(cmd.getApiId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(ApiCommand.ActivateAPI.class, (state, cmd) -> Effect()
                        .persist(new ApiEvent.ApiActivated(cmd.getApiId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<ApiState, ApiEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(ApiEvent.ApiAdded.class, (state, evt) -> ApiState.create(evt.getApi()))
                .onEvent(ApiEvent.ApiUpdated.class, (state, evt) -> state.updateDetails(evt.getApi()))
                .onEvent(ApiEvent.ApiActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(ApiEvent.ApiDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public Set<String> tagsFor(ApiEvent apiEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, ApiEvent.TAG).apply(apiEvent);
    }

    public static Behavior<ApiCommand> create(EntityContext<ApiCommand> entityContext) {
        return new ApiEntity(entityContext);
    }
}