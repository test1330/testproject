package com.aie.arc.rbac.impl.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.rbac.impl.command.MicroServiceCommand;
import com.aie.arc.rbac.impl.entity.entities.MicroService;
import com.aie.arc.rbac.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.rbac.impl.event.MicroServiceEvent;
import com.aie.arc.rbac.impl.state.MicroServiceState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MicroServiceEntity extends EventSourcedBehaviorWithEnforcedReplies<MicroServiceCommand, MicroServiceEvent, MicroServiceState> {

    final private EntityContext<MicroServiceCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<MicroServiceCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(MicroServiceCommand.class, "MicroServiceEntity");

    MicroServiceEntity(EntityContext<MicroServiceCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public MicroServiceState emptyState() {
        return MicroServiceState.empty();
    }

    @Override
    public CommandHandlerWithReply<MicroServiceCommand, MicroServiceEvent, MicroServiceState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(MicroServiceCommand.GetMicroService.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.microService))

                .onCommand(MicroServiceCommand.CreateMicroService.class, (state, cmd) -> Effect()
                        .persist(new MicroServiceEvent.MicroServiceCreated(cmd.getMicroservice()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(MicroServiceCommand.UpdateMicroService.class, (state, cmd) -> Effect()
                        .persist(new MicroServiceEvent.MicroServiceUpdated(cmd.getMicroservice()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(MicroServiceCommand.DeactivateMicroService.class, (state, cmd) -> Effect()
                        .persist(new MicroServiceEvent.MicroServiceDeactivated(cmd.getMicroServiceId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(MicroServiceCommand.ActivateMicroService.class, (state, cmd) -> Effect()
                        .persist(new MicroServiceEvent.MicroServiceActivated(cmd.getMicroServiceId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(MicroServiceCommand.AddApiToMicroService.class, (state, cmd) -> Effect()
                        .persist(new MicroServiceEvent.ApiAddedToMicroService(cmd.getMicroServiceId(),cmd.getApiId(),cmd.getLocalizedFieldsVOMap(),cmd.isActive()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(MicroServiceCommand.UpdateApiToMicroService.class, (state, cmd) -> Effect()
                        .persist(new MicroServiceEvent.ApiUpdatedInMicroService(cmd.getMicroServiceId(),cmd.getApiId(),cmd.getLocalizedFieldsVOMap(),cmd.isActive()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(MicroServiceCommand.ActivateApiToMicroService.class,(state,cmd) -> Effect()
                        .persist(new MicroServiceEvent.ApiActivateInMicroService(cmd.getMicroServiceId(),cmd.getApiId(),cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(MicroServiceCommand.DeactivateApiToMicroService.class,(state, cmd) -> Effect()
                        .persist(new MicroServiceEvent.ApiDeactivateInMicroService(cmd.getMicroServiceId(),cmd.getApiId(),cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<MicroServiceState, MicroServiceEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(MicroServiceEvent.MicroServiceCreated.class, (state, evt) -> MicroServiceState.create(evt.getMicroservice()))
                .onEvent(MicroServiceEvent.MicroServiceUpdated.class, (state, evt) -> state.updateDetails(evt.getMicroservice()))
                .onEvent(MicroServiceEvent.MicroServiceActivated.class, (state, evt) -> state.updateDetails(evt.isActive()))
                .onEvent(MicroServiceEvent.MicroServiceDeactivated.class, (state, evt) -> state.updateDetails(evt.isActive()))
                .onEvent(MicroServiceEvent.ApiAddedToMicroService.class,(state,evt) -> addApisToMicroService(evt,state))
                .onEvent(MicroServiceEvent.ApiUpdatedInMicroService.class,(state,evt) -> updateApisInMicroService(evt,state))
                .onEvent(MicroServiceEvent.ApiActivateInMicroService.class,(state,evt) -> state.updateDetails(state.microService.get()))
                .onEvent(MicroServiceEvent.ApiDeactivateInMicroService.class,(state,evt) -> state.updateDetails(state.microService.get()))
                .build();
    }

    private MicroServiceState addApisToMicroService(MicroServiceEvent.ApiAddedToMicroService evt,MicroServiceState state) {
        MicroService microService = state.microService.get();
        if(microService.getApiIds() == null) {
            List<String> ids = new ArrayList<>();
            ids.add(evt.getApiId());
            microService = microService.updateIfNotEmptyMicroService(microService.getMicroServiceId(),microService.getLocalizedFieldsVOMap(),ids,microService.isActive(),microService.getCreatedAt(), new LastModifiedAt(LocalDateTime.now()),microService.getStatus());
        } else {
            microService.getApiIds().add(evt.getApiId());
        }
        return state.updateDetails(microService);
    }

    private MicroServiceState updateApisInMicroService(MicroServiceEvent.ApiUpdatedInMicroService evt,MicroServiceState state) {
     MicroService microService = state.microService.get();
     return state.updateDetails(microService);
    }

    @Override
    public Set<String> tagsFor(MicroServiceEvent microServiceEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, MicroServiceEvent.TAG).apply(microServiceEvent);
    }

    public static Behavior<MicroServiceCommand> create(EntityContext<MicroServiceCommand> entityContext) {
        return new MicroServiceEntity(entityContext);
    }
}

