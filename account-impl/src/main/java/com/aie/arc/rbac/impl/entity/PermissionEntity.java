package com.aie.arc.rbac.impl.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.rbac.impl.command.PermissionCommand;
import com.aie.arc.rbac.impl.event.PermissionEvent;
import com.aie.arc.rbac.impl.state.PermissionState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;


public class PermissionEntity extends EventSourcedBehaviorWithEnforcedReplies<PermissionCommand, PermissionEvent, PermissionState> {

    final private EntityContext<PermissionCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<PermissionCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(PermissionCommand.class, "PermissionEntity");

    PermissionEntity(EntityContext<PermissionCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public PermissionState emptyState() {
        return PermissionState.empty();
    }

    @Override
    public CommandHandlerWithReply<PermissionCommand, PermissionEvent, PermissionState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(PermissionCommand.GetPermission.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.permission))

                .onCommand(PermissionCommand.CreatePermission.class, (state, cmd) -> Effect()
                        .persist(new PermissionEvent.PermissionCreated(cmd.getPermission()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(PermissionCommand.UpdatePermission.class, (state, cmd) -> Effect()
                        .persist(new PermissionEvent.PermissionUpdated(cmd.getPermission()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(PermissionCommand.DeactivatePermission.class, (state, cmd) -> Effect()
                        .persist(new PermissionEvent.PermissionDeactivated(cmd.getPermissionId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(PermissionCommand.ActivatePermission.class, (state, cmd) -> Effect()
                        .persist(new PermissionEvent.PermissionActivated(cmd.getPermissionId(),cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<PermissionState, PermissionEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(PermissionEvent.PermissionCreated.class, (state, evt) -> PermissionState.create(evt.getPermission()))
                .onEvent(PermissionEvent.PermissionUpdated.class, (state, evt) -> state.updateDetails(evt.getPermission()))
                .onEvent(PermissionEvent.PermissionActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(PermissionEvent.PermissionDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public Set<String> tagsFor(PermissionEvent permissionEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, PermissionEvent.TAG).apply(permissionEvent);
    }

    public static Behavior<PermissionCommand> create(EntityContext<PermissionCommand> entityContext) {
        return new PermissionEntity(entityContext);
    }
}
