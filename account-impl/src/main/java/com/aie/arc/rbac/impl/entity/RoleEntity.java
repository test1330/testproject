package com.aie.arc.rbac.impl.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.rbac.impl.command.RoleCommand;
import com.aie.arc.rbac.impl.event.RoleEvent;
import com.aie.arc.rbac.impl.state.RoleState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;

public class RoleEntity extends EventSourcedBehaviorWithEnforcedReplies<RoleCommand, RoleEvent, RoleState> {

    final private EntityContext<RoleCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<RoleCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(RoleCommand.class, "RoleEntity");

    RoleEntity(EntityContext<RoleCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public RoleState emptyState() {
        return RoleState.empty();
    }

    @Override
    public CommandHandlerWithReply<RoleCommand, RoleEvent, RoleState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(RoleCommand.GetRole.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.role))

                .onCommand(RoleCommand.CreateRole.class, (state, cmd) -> Effect()
                        .persist(new RoleEvent.RoleCreated(cmd.getRole()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(RoleCommand.UpdateRole.class, (state, cmd) -> Effect()
                        .persist(new RoleEvent.RoleUpdated(cmd.getRole()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(RoleCommand.DeactivateRole.class, (state, cmd) -> Effect()
                        .persist(new RoleEvent.RoleDeactivated(cmd.getRoleId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(RoleCommand.ActivateRole.class, (state, cmd) -> Effect()
                        .persist(new RoleEvent.RoleActivated(cmd.getRoleId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<RoleState, RoleEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(RoleEvent.RoleCreated.class, (state, evt) -> RoleState.create(evt.getRole()))
                .onEvent(RoleEvent.RoleUpdated.class, (state, evt) -> state.updateDetails(evt.getRole()))
                .onEvent(RoleEvent.RoleActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(RoleEvent.RoleDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public Set<String> tagsFor(RoleEvent roleEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, RoleEvent.TAG).apply(roleEvent);
    }

    public static Behavior<RoleCommand> create(EntityContext<RoleCommand> entityContext) {
        return new RoleEntity(entityContext);
    }
}

