package com.aie.arc.rbac.impl.entity;

import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.rbac.impl.command.RoleGroupCommand;
import com.aie.arc.rbac.impl.event.RoleGroupEvent;
import com.aie.arc.rbac.impl.state.RoleGroupState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;

public class RoleGroupEntity extends EventSourcedBehaviorWithEnforcedReplies<RoleGroupCommand, RoleGroupEvent, RoleGroupState> {

    final private EntityContext<RoleGroupCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<RoleGroupCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(RoleGroupCommand.class, "RoleGroupEntity");

    RoleGroupEntity(EntityContext<RoleGroupCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public RoleGroupState emptyState() {
        return RoleGroupState.empty();
    }

    @Override
    public CommandHandlerWithReply<RoleGroupCommand, RoleGroupEvent, RoleGroupState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(RoleGroupCommand.GetRoleGroup.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.roleGroup))

                .onCommand(RoleGroupCommand.CreateRoleGroup.class, (state, cmd) -> Effect()
                        .persist(new RoleGroupEvent.RoleGroupCreated(cmd.getRoleGroup()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(RoleGroupCommand.UpdateRoleGroup.class, (state, cmd) -> Effect()
                        .persist(new RoleGroupEvent.RoleGroupUpdated(cmd.getRoleGroup()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(RoleGroupCommand.DeactivateRoleGroup.class, (state, cmd) -> Effect()
                        .persist(new RoleGroupEvent.RoleGroupDeactivated(cmd.getRoleGroupId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(RoleGroupCommand.ActivateRoleGroup.class, (state, cmd) -> Effect()
                        .persist(new RoleGroupEvent.RoleGroupActivated(cmd.getRoleGroupId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))
                .build();
    }

    @Override
    public EventHandler<RoleGroupState, RoleGroupEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(RoleGroupEvent.RoleGroupCreated.class, (state, evt) -> RoleGroupState.create(evt.getRoleGroup()))
                .onEvent(RoleGroupEvent.RoleGroupUpdated.class, (state, evt) -> state.updateDetails(evt.getRoleGroup()))
                .onEvent(RoleGroupEvent.RoleGroupActivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .onEvent(RoleGroupEvent.RoleGroupDeactivated.class, (state, evt) -> state.updateDetails(evt.isValue()))
                .build();
    }

    @Override
    public Set<String> tagsFor(RoleGroupEvent roleGroupEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, RoleGroupEvent.TAG).apply(roleGroupEvent);
    }

    public static Behavior<RoleGroupCommand> create(EntityContext<RoleGroupCommand> entityContext) {
        return new RoleGroupEntity(entityContext);
    }
}

