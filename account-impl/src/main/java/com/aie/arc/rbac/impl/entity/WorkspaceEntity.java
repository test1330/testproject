package com.aie.arc.rbac.impl.entity;
import akka.Done;
import akka.actor.typed.Behavior;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import com.aie.arc.rbac.impl.command.WorkspaceCommand;
import com.aie.arc.rbac.impl.event.WorkspaceEvent;
import com.aie.arc.rbac.impl.state.WorkspaceState;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;

import java.util.Set;


public class WorkspaceEntity extends EventSourcedBehaviorWithEnforcedReplies<WorkspaceCommand, WorkspaceEvent, WorkspaceState> {

    final private EntityContext<WorkspaceCommand> entityContext;
    final private String entityId;

    public static EntityTypeKey<WorkspaceCommand> ENTITY_TYPE_KEY =
            EntityTypeKey
                    .create(WorkspaceCommand.class, "WorkspaceEntity");

    WorkspaceEntity(EntityContext<WorkspaceCommand> entityContext) {
        super(PersistenceId.of(entityContext.getEntityTypeKey().name(), entityContext.getEntityId()));
        this.entityContext = entityContext;
        this.entityId = entityContext.getEntityId();
    }

    @Override
    public WorkspaceState emptyState() {
        return WorkspaceState.empty();
    }

    @Override
    public CommandHandlerWithReply<WorkspaceCommand, WorkspaceEvent, WorkspaceState> commandHandler() {
        return newCommandHandlerWithReplyBuilder()
                .forAnyState()

                .onCommand(WorkspaceCommand.GetWorkspace.class, (state, cmd) -> Effect()
                        .none()
                        .thenReply(cmd.replyTo, __ -> state.workspace))

                .onCommand(WorkspaceCommand.CreateWorkspace.class, (state, cmd) -> Effect()
                        .persist(new WorkspaceEvent.WorkspaceCreated(cmd.getWorkspace()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(WorkspaceCommand.UpdateWorkspace.class, (state, cmd) -> Effect()
                        .persist(new WorkspaceEvent.WorkspaceUpdated(cmd.getWorkspace()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(WorkspaceCommand.DeactivateWorkspace.class, (state, cmd) -> Effect()
                        .persist(new WorkspaceEvent.WorkspaceDeactivated(cmd.getWorkspaceId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))

                .onCommand(WorkspaceCommand.ActivateWorkspace.class, (state, cmd) -> Effect()
                        .persist(new WorkspaceEvent.WorkspaceActivated(cmd.getWorkspaceId(), cmd.isValue()))
                        .thenReply(cmd.replyTo, __ -> Done.getInstance()))


                .build();
    }

    @Override
    public EventHandler<WorkspaceState, WorkspaceEvent> eventHandler() {
        return newEventHandlerBuilder().
                forAnyState()
                .onEvent(WorkspaceEvent.WorkspaceCreated.class, (state, evt) -> WorkspaceState.create(evt.getWorkspace()))
                .onEvent(WorkspaceEvent.WorkspaceUpdated.class, (state, evt) -> state.updateDetails(evt.getWorkspace()))
                .onEvent(WorkspaceEvent.WorkspaceDeactivated.class, (state, evt) -> state.updateDetails(evt.isActive()))
                .onEvent(WorkspaceEvent.WorkspaceActivated.class, (state, evt) -> state.updateDetails(evt.isActive()))
                .build();
    }

    @Override
    public Set<String> tagsFor(WorkspaceEvent WorkspaceEvent) {
        return AkkaTaggerAdapter.fromLagom(entityContext, WorkspaceEvent.TAG).apply(WorkspaceEvent);
    }

    public static Behavior<WorkspaceCommand> create(EntityContext<WorkspaceCommand> entityContext) {
        return new WorkspaceEntity(entityContext);
    }
}
