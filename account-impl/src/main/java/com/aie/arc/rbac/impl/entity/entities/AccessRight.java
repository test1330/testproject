package com.aie.arc.rbac.impl.entity.entities;

import com.aie.arc.account.api.common.request.LocalisedFields;
import com.aie.arc.rbac.api.request.UpdateAccessRightRequest;
import com.aie.arc.rbac.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.rbac.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.rbac.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.Map;

@Value
@JsonDeserialize
public class AccessRight implements Jsonable {

    String accessId;
    Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap;
    boolean active;
    CreatedAt createdAt;
    LastModifiedAt lastModifiedAt;
    Status status;

    public AccessRight() {
        this.accessId = null;
        this.localizedFieldsVOMap = null;
        this.active = false;
        this.status = null;
        this.createdAt = null;
        this.lastModifiedAt = null;


    }
    @JsonCreator
    public AccessRight(String accessId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, boolean active, CreatedAt createdAt,
            LastModifiedAt lastModifiedAt, Status status) {
        this.accessId = accessId;
        this.localizedFieldsVOMap = localizedFieldsVOMap;
        this.active = active;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
        this.status = status;
    }

    public static AccessRight createAccessRightByRequest(String accessId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, boolean active) {
        return new AccessRight(
                accessId,
                localizedFieldsVOMap,
                active,
                new CreatedAt(LocalDateTime.now()),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED
        );
    }

    public static AccessRight updateAccessRightByRequest(AccessRight accessRight, UpdateAccessRightRequest request) {
        Map<String, RBACLocalizedFieldsVO> currentLocalization = accessRight.getLocalizedFieldsVOMap();
        Map<String, LocalisedFields> requestedLocalization = request.getLocalization();
        requestedLocalization.forEach((code, value) -> currentLocalization.put(code, new RBACLocalizedFieldsVO(value.getName(), value.getDescription())));
        return new AccessRight(
                accessRight.accessId,
                currentLocalization,
                request.getActive(),
                accessRight.getCreatedAt(),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED
        );
    }

    public AccessRight withDetails(AccessRight details) {
        return updateIfNotEmptyAccessRight(
                details.accessId,
                details.getLocalizedFieldsVOMap(),
                details.active,
                details.getCreatedAt(),
                details.getLastModifiedAt(),
                details.status
        );
    }
    public AccessRight withDetails(boolean active) {
        return updateIfNotEmptyAccessRight(
                this.accessId,
                this.getLocalizedFieldsVOMap(),
                active,
                this.getCreatedAt(),
                this.getLastModifiedAt(),
                this.status
        );
    }

    public AccessRight updateIfNotEmptyAccessRight(String accessId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, boolean active, CreatedAt createdAt, LastModifiedAt lastModifiedAt, Status status) {
        return new AccessRight(
                updateIfFoundInRequest(accessId, getAccessId()),
                localizedFieldsVOMap.isEmpty() ? getLocalizedFieldsVOMap() : localizedFieldsVOMap,
                active,
                createdAt,
                lastModifiedAt,
                Status.CREATED
        );
    }

    private String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

}
