package com.aie.arc.rbac.impl.entity.entities;

import com.aie.arc.account.api.common.request.LocalisedFields;
import com.aie.arc.rbac.api.request.ApiRequest;
import com.aie.arc.rbac.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.rbac.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.rbac.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Value
@JsonDeserialize
public class Api implements Jsonable {

    private String id;
    private Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap;
    private boolean active;
    private CreatedAt createdAt;
    private LastModifiedAt lastModifiedAt;
    private Status status;

    @JsonCreator
    public Api(String id, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap,boolean active, CreatedAt createdAt, LastModifiedAt lastModifiedAt, Status status) {
        this.id = id;
        this.localizedFieldsVOMap = localizedFieldsVOMap;
        this.active = active;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
        this.status = status;
    }

    public static Api createApiByRequest(String apiId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, boolean active) {
        return new Api(apiId, localizedFieldsVOMap,active, new CreatedAt(LocalDateTime.now()), new LastModifiedAt(LocalDateTime.now()),Status.CREATED);
    }

    public static Api updateApiByRequest(Api api, ApiRequest request) {
        Map<String, RBACLocalizedFieldsVO> currentLocalization = api.getLocalizedFieldsVOMap();
        Map<String, LocalisedFields> requestedLocalization = request.getLocalization();
        requestedLocalization.forEach((code, value) -> currentLocalization.put(code, new RBACLocalizedFieldsVO(value.getName(), value.getDescription())));
        return new Api(
                api.getId(),
                currentLocalization,
                request.getActive(),
                api.getCreatedAt(),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED
        );
    }

    public Api withDetails(Api microServiceApi) {
        return updateIfNotEmptyApi(
                microServiceApi.getId(),
                microServiceApi.localizedFieldsVOMap,
                microServiceApi.isActive(),
                microServiceApi.getCreatedAt(),
                microServiceApi.getLastModifiedAt(),
                Status.CREATED);
    }
    public Api withDetails(boolean active) {
        return updateIfNotEmptyApi(
                this.getId(),
                this.localizedFieldsVOMap,
                active,
                this.getCreatedAt(),
                this.getLastModifiedAt(),
                Status.CREATED);
    }

    public Api updateIfNotEmptyApi(String id, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, boolean active,CreatedAt createdAt, LastModifiedAt lastModifiedAt,  Status status) {
        return new Api(
                updateIfFoundInRequest(id, getId()),
                localizedFieldsVOMap.isEmpty() ? getLocalizedFieldsVOMap() : localizedFieldsVOMap,
                active,
                createdAt,
                lastModifiedAt,
                Status.CREATED
        );
    }

    //utils
    private String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

    //utils
    private <T> List<T> updateIfFoundInRequest(List<T> valueInRequest, List<T> valueInCurrentState) {
        if (valueInRequest == null) return valueInCurrentState;
        return !valueInRequest.isEmpty() ? valueInRequest : valueInCurrentState;
    }

}

