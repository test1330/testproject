package com.aie.arc.rbac.impl.entity.entities;

import com.aie.arc.account.api.common.request.LocalisedFields;
import com.aie.arc.rbac.api.request.MicroServiceRequest;
import com.aie.arc.rbac.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.rbac.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.rbac.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Value
@JsonDeserialize
public class MicroService implements Jsonable {
    private String microServiceId;
    private Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap;
    private List<String> apiIds;
    private boolean active;
    private CreatedAt createdAt;
    private LastModifiedAt lastModifiedAt;
    private Status status;

    @JsonCreator
    public MicroService(String microServiceId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, List<String> apiIds,  boolean active,CreatedAt createdAt, LastModifiedAt lastModifiedAt,Status status) {
        this.microServiceId = microServiceId;
        this.localizedFieldsVOMap = localizedFieldsVOMap;
        this.apiIds = apiIds;
        this.active = active;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
        this.status = status;
    }

    public static MicroService createMicroServiceByRequest(String microServiceId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, List<String> apiIds, boolean active) {
        return new MicroService(microServiceId, localizedFieldsVOMap, apiIds,active,new CreatedAt(LocalDateTime.now()),
                new LastModifiedAt(LocalDateTime.now()), Status.CREATED);
    }

    public static MicroService updateMicroServiceByRequest(MicroService microService, List<String> apiId, MicroServiceRequest request) {
        Map<String, RBACLocalizedFieldsVO> currentLocalization = microService.getLocalizedFieldsVOMap();
        Map<String, LocalisedFields> requestedLocalization = request.getLocalization();
        requestedLocalization.forEach((code, value) -> currentLocalization.put(code, new RBACLocalizedFieldsVO(value.getName(), value.getDescription())));
        return new MicroService(
                microService.getMicroServiceId(),
                currentLocalization,
                apiId,
                request.getActive(),
                microService.getCreatedAt(),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED
        );
    }

    public MicroService withDetails(MicroService microService) {
        return updateIfNotEmptyMicroService(
                microService.getMicroServiceId(),
                microService.getLocalizedFieldsVOMap(),
                microService.getApiIds(),
                microService.isActive(),
                microService.getCreatedAt(),
                microService.getLastModifiedAt(),
                Status.CREATED
        );
    }
    public MicroService withDetails(boolean active) {
        return updateIfNotEmptyMicroService(
                this.getMicroServiceId(),
                this.getLocalizedFieldsVOMap(),
                this.getApiIds(),
                active,
                this.getCreatedAt(),
                this.getLastModifiedAt(),
                Status.CREATED
        );
    }

    public MicroService updateIfNotEmptyMicroService(String microServiceId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, List<String> apiId,boolean active,CreatedAt createdAt, LastModifiedAt lastModifiedAt,  Status status) {
        return new MicroService(
                updateIfFoundInRequest(microServiceId, getMicroServiceId()),
                localizedFieldsVOMap.isEmpty() ? getLocalizedFieldsVOMap() : localizedFieldsVOMap,
                updateIfFoundInRequest(apiId, getApiIds()),
                active,
                createdAt,
                lastModifiedAt,
                Status.CREATED
        );
    }

    //utils
    private String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

    //utils
    private <T> List<T> updateIfFoundInRequest(List<T> valueInRequest, List<T> valueInCurrentState) {
        if (valueInRequest == null) return valueInCurrentState;
        return !valueInRequest.isEmpty() ? valueInRequest : valueInCurrentState;
    }
}
