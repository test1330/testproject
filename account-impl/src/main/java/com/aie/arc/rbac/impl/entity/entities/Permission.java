package com.aie.arc.rbac.impl.entity.entities;

import com.aie.arc.account.api.common.request.LocalisedFields;
import com.aie.arc.rbac.api.request.PermissionRequest;
import com.aie.arc.rbac.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.rbac.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.rbac.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Value
@JsonDeserialize
public class Permission implements Jsonable {
     String permissionId;
     Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap;
     String accessRightId;
     String workspaceId;
     List<String> apiTags;
     boolean active;
     CreatedAt createdAt;
     LastModifiedAt lastModifiedAt;
     Status status;

    @JsonCreator
    public  Permission(String permissionId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, String accessRightId, String workspaceId, List<String> apiTags, boolean active, CreatedAt createdAt, LastModifiedAt lastModifiedAt, Status status) {
        this.permissionId = permissionId;
        this.localizedFieldsVOMap = localizedFieldsVOMap;
        this.accessRightId = accessRightId;
        this.workspaceId = workspaceId;
        this.apiTags = apiTags;
        this.active = active;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
        this.status = status;
    }


    public static Permission createPermissionByRequest(String permissionId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, PermissionRequest request) {
        return new Permission(
                permissionId,
                localizedFieldsVOMap,
                request.getAccessRightId(),
                request.getWorkspaceId(),
                request.getApiTags(),
                request.getActive(),
                new CreatedAt(LocalDateTime.now()),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED
        );
    }

    public Permission updatePermission(Permission permission, PermissionRequest request) {
        Map<String, RBACLocalizedFieldsVO> currentLocalization = permission.getLocalizedFieldsVOMap();
        Map<String, LocalisedFields> requestedLocalization = request.getLocalization();
        requestedLocalization.forEach((code, value) -> currentLocalization.put(code, new RBACLocalizedFieldsVO(value.getName(), value.getDescription())));
        return new Permission(
                permission.getPermissionId(),
                currentLocalization,
                request.getAccessRightId(),
                request.getWorkspaceId(),
                request.getApiTags(),
                request.getActive(),
                permission.getCreatedAt(),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED
        );
    }

    public Permission withDetails(Permission permission) {
        return updateIfNotEmptyPermission(
                permission.getPermissionId(),
                permission.getLocalizedFieldsVOMap(),
                permission.getAccessRightId(),
                permission.getWorkspaceId(),
                permission.getApiTags(),
                permission.isActive(),
                permission.getCreatedAt(),
                permission.getLastModifiedAt(),
                permission.getStatus());
    }
    public Permission withDetails(boolean active) {
        return updateIfNotEmptyPermission(
                this.getPermissionId(),
                this.getLocalizedFieldsVOMap(),
                this.getAccessRightId(),
                this.getWorkspaceId(),
                this.getApiTags(),
                active,
                this.getCreatedAt(),
                this.getLastModifiedAt(),
                this.getStatus());
    }

    public Permission updateIfNotEmptyPermission(String permissionId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, String accessRightId, String workspaceId,
                                                 List<String> apiTag, boolean active, CreatedAt createdAt, LastModifiedAt lastModifiedAt, Status status) {
        return new Permission(
                updateIfFoundInRequest(permissionId, getPermissionId()),
                localizedFieldsVOMap.isEmpty() ? getLocalizedFieldsVOMap() : localizedFieldsVOMap,
                accessRightId,
                workspaceId,
                updateIfFoundInRequest(apiTag, getApiTags()),
                active,
                createdAt,
                lastModifiedAt,
                Status.CREATED
        );
    }

    //utils
    private String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

    //utils
    private <T> List<T> updateIfFoundInRequest(List<T> valueInRequest, List<T> valueInCurrentState) {
        if (valueInRequest == null) return valueInCurrentState;
        return !valueInRequest.isEmpty() ? valueInRequest : valueInCurrentState;
    }

}
