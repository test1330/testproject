package com.aie.arc.rbac.impl.entity.entities;

import lombok.Value;

/**
 * The state for the {@link RBACCommonFilterParameter} entity.
 */
@Value
public final class RBACCommonFilterParameter {
    private int pageNo;
    private int pageSize;
    private boolean active;
    private boolean ascending;
    private boolean all;
    private String name;

    public static RBACCommonFilterParameter getInstance(int pageNo, int pageSize, boolean active, boolean ascending, boolean all, String name) {
        return new RBACCommonFilterParameter(pageNo, pageSize, active, ascending, all, name);
    }
}
