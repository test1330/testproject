package com.aie.arc.rbac.impl.entity.entities;

import lombok.Value;

import java.util.List;

@Value
public class RBACListWrapper {
    private List<String> list;
    private Integer totalSize;
}
