package com.aie.arc.rbac.impl.entity.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
@JsonDeserialize
public class RBACLocalizedFieldsVO implements Jsonable {
    @NotBlank
    private String name;
    @NotBlank
    private String description;

    @JsonCreator
    public RBACLocalizedFieldsVO(@NotBlank String name, @NotBlank String description) {
        this.name = name;
        this.description = description;
    }
}
