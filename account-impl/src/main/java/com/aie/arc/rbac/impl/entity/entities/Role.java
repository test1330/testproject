package com.aie.arc.rbac.impl.entity.entities;

import com.aie.arc.account.api.common.request.LocalisedFields;
import com.aie.arc.account.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.account.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.rbac.api.request.RoleRequest;
import com.aie.arc.rbac.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Value
@JsonDeserialize
public class Role implements Jsonable {
    private String roleId;
    private String roleGroupId;
    private List<String> permissionIds;
    private Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap;
    private boolean active;
    private CreatedAt createdAt;
    private LastModifiedAt lastModifiedAt;
    Status status;

    @JsonCreator
    public Role(String roleId, String roleGroupId, List<String> permissionIds, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, boolean active, CreatedAt createdAt, LastModifiedAt lastModifiedAt, Status status) {
        this.roleId = roleId;
        this.roleGroupId = roleGroupId;
        this.permissionIds = permissionIds;
        this.localizedFieldsVOMap = localizedFieldsVOMap;
        this.active = active;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
        this.status = status;
    }

    public static Role createRoleByRequest(String roleId, String roleGroupId, List<String> permissionIds, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, Boolean active) {
        return new Role(
                roleId,
                roleGroupId!=null ? roleGroupId : "",
                permissionIds,
                localizedFieldsVOMap,
                active,
                new CreatedAt(LocalDateTime.now()),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED
        );
    }


    public static Role updateRoleByRequest(Role role, RoleRequest request) {
        Map<String, RBACLocalizedFieldsVO> currentLocalization = role.getLocalizedFieldsVOMap();
        Map<String, LocalisedFields> requestedLocalization = request.getLocalization();
        requestedLocalization.forEach((code, value) -> currentLocalization.put(code, new RBACLocalizedFieldsVO(value.getName(), value.getDescription())));
        if(request.getRoleGroupId()!=null)
            request.getRoleGroupId();

        return new Role(
                role.getRoleId(),
                request.getRoleGroupId(),
                request.getPermissionIds(),
                currentLocalization,
                request.getActive(),
                role.getCreatedAt(),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED
        );
    }

    public Role withDetails(Role details) {
        return updateIfNotEmptyRole(
                details.roleId,
                details.roleGroupId,
                details.getPermissionIds(),
                details.getLocalizedFieldsVOMap(),
                details.isActive(),
                details.getCreatedAt(),
                details.getLastModifiedAt(),
                details.status
        );
    }

    public Role withDetails(boolean active) {
        return updateIfNotEmptyRole(
                this.getRoleId(),
                this.getRoleGroupId(),
                this.getPermissionIds(),
                this.getLocalizedFieldsVOMap(),
                active,
                this.getCreatedAt(),
                this.getLastModifiedAt(),
                this.getStatus()
        );
    }


    private Role updateIfNotEmptyRole(String roleId, String roleGroupId, List<String> permissionIds, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, boolean active, CreatedAt createdAt, LastModifiedAt lastModifiedAt, Status status) {
        return new Role(
                updateIfFoundInRequest(roleId, getRoleId()),
                updateIfFoundInRequest(roleGroupId, getRoleGroupId()),
                updateIfFoundInRequest(permissionIds, getPermissionIds()),
                localizedFieldsVOMap.isEmpty() ? getLocalizedFieldsVOMap() : localizedFieldsVOMap,
                active,
                createdAt,
                lastModifiedAt,
                Status.CREATED
        );
    }

    //utils
    private String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

    //utils
    private <T> List<T> updateIfFoundInRequest(List<T> valueInRequest, List<T> valueInCurrentState) {
        if (valueInRequest == null) return valueInCurrentState;
        return !valueInRequest.isEmpty() ? valueInRequest : valueInCurrentState;
    }
}
