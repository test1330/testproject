package com.aie.arc.rbac.impl.entity.entities;

import com.aie.arc.account.api.common.request.LocalisedFields;
import com.aie.arc.rbac.api.request.RoleGroupRequest;
import com.aie.arc.rbac.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.rbac.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.rbac.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Value
@JsonDeserialize
public class RoleGroup implements Jsonable {
    private String roleGroupId;
    private Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap;
    private boolean active;
    private CreatedAt createdAt;
    private LastModifiedAt lastModifiedAt;
    private Status status;

    @JsonCreator
    public RoleGroup(String roleGroupId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, boolean active, CreatedAt createdAt, LastModifiedAt lastModifiedAt, Status status) {
        this.roleGroupId = roleGroupId;
        this.localizedFieldsVOMap = localizedFieldsVOMap;
        this.active = active;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
        this.status = status;
    }

    public static RoleGroup createRoleGroupByRequest(String roleGroupId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, boolean active) {
        return new RoleGroup(
                roleGroupId,
                localizedFieldsVOMap,
                active,
                new CreatedAt(LocalDateTime.now()),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED
        );
    }

    public static RoleGroup updateRoleGroupByRequest(RoleGroup roleGroup, RoleGroupRequest request) {
        Map<String, RBACLocalizedFieldsVO> currentLocalization = roleGroup.getLocalizedFieldsVOMap();
        Map<String, LocalisedFields> requestedLocalization = request.getLocalization();
        requestedLocalization.forEach((code, value) -> currentLocalization.put(code, new RBACLocalizedFieldsVO(value.getName(), value.getDescription())));
        return new RoleGroup(
                roleGroup.roleGroupId,
                currentLocalization,
                request.getActive(),
                roleGroup.getCreatedAt(),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED
        );
    }

    public RoleGroup withDetails(RoleGroup details) {
        return updateIfNotEmptyRoleGroup(
                details.roleGroupId,
                details.getLocalizedFieldsVOMap(),
                details.active,
                details.getCreatedAt(),
                details.getLastModifiedAt(),
                details.status
        );
    }
    public RoleGroup withDetails(boolean active) {
        return updateIfNotEmptyRoleGroup(
                this.roleGroupId,
                this.getLocalizedFieldsVOMap(),
                active,
                this.getCreatedAt(),
                this.getLastModifiedAt(),
                this.status
        );
    }

    public RoleGroup updateIfNotEmptyRoleGroup(String roleGroupId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, boolean active,CreatedAt createdAt, LastModifiedAt lastModifiedAt ,Status status) {
        return new RoleGroup(
                updateIfFoundInRequest(roleGroupId, getRoleGroupId()),
                localizedFieldsVOMap.isEmpty() ? getLocalizedFieldsVOMap() : localizedFieldsVOMap,
                active,
                createdAt,
                lastModifiedAt,
                Status.CREATED
        );
    }

    //utils
    private String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

    //utils
    private <T> List<T> updateIfFoundInRequest(List<T> valueInRequest, List<T> valueInCurrentState) {
        if (valueInRequest == null) return valueInCurrentState;
        return !valueInRequest.isEmpty() ? valueInRequest : valueInCurrentState;
    }
}
