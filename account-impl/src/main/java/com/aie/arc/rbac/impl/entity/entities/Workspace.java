package com.aie.arc.rbac.impl.entity.entities;

import com.aie.arc.account.api.common.request.LocalisedFields;
import com.aie.arc.rbac.api.request.PermissionType;
import com.aie.arc.rbac.api.request.UpdateWorkspaceRequest;
import com.aie.arc.rbac.api.request.WorkspaceRequest;
import com.aie.arc.rbac.impl.entity.entities.vo.CreatedAt;
import com.aie.arc.rbac.impl.entity.entities.vo.LastModifiedAt;
import com.aie.arc.rbac.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.Map;

@Value
@JsonDeserialize
public class Workspace implements Jsonable {
    String workspaceId;
    Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap;
    String module;
    PermissionType permissionType;
    boolean active;
    CreatedAt createdAt;
    LastModifiedAt lastModifiedAt;
    Status status;

    @JsonCreator
    public Workspace(String workspaceId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, String module, PermissionType permissionType, boolean active,CreatedAt createdAt,
            LastModifiedAt lastModifiedAt, Status status) {
        this.workspaceId = workspaceId;
        this.localizedFieldsVOMap = localizedFieldsVOMap;
        this.module = module;
        this.permissionType = permissionType;
        this.active = active;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
        this.status = status;
    }
    public static Workspace createWorkspaceByRequest(String workspaceId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, WorkspaceRequest request) {
        return new Workspace(
                workspaceId,
                localizedFieldsVOMap,
                request.getModule(),
                request.getPermissionType(),
                request.getActive(),
                new CreatedAt(LocalDateTime.now()),
                new LastModifiedAt(LocalDateTime.now()),
                Status.CREATED
        );

}
    public static Workspace updateWorkspace(Workspace workspace, UpdateWorkspaceRequest request) {
        Map<String, RBACLocalizedFieldsVO> currentLocalization = workspace.getLocalizedFieldsVOMap();
        Map<String, LocalisedFields> requestedLocalization = request.getLocalization();
        requestedLocalization.forEach((code, value) -> currentLocalization.put(code, new RBACLocalizedFieldsVO(value.getName(), value.getDescription())));

                return new Workspace(
                        workspace.getWorkspaceId(),
                        currentLocalization,
                        request.getModule(),
                        request.getPermissionType(),
                        request.getActive(),
                        workspace.getCreatedAt(),
                        new LastModifiedAt(LocalDateTime.now()),
                        Status.CREATED
                );

    }

    public Workspace withDetails(Workspace workspace) {
        return updateIfNotEmptyWorkspace(
                workspace.getWorkspaceId(),
                workspace.getLocalizedFieldsVOMap(),
                workspace.getModule(),
                workspace.getPermissionType(),
                workspace.isActive(),
                workspace.getCreatedAt(),
                workspace.getLastModifiedAt(),
                workspace.getStatus());

    }
    public Workspace withDetails(boolean active) {
        return updateIfNotEmptyWorkspace(
                this.getWorkspaceId(),
                this.getLocalizedFieldsVOMap(),
                this.getModule(),
                this.getPermissionType(),
                active,
                this.getCreatedAt(),
                this.getLastModifiedAt(),
                this.getStatus());
    }



    public Workspace updateIfNotEmptyWorkspace(String workspaceId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, String module, PermissionType permissionType, boolean active, CreatedAt createdAt, LastModifiedAt lastModifiedAt, Status status) {
        return new Workspace(
                updateIfFoundInRequest(workspaceId, getWorkspaceId()),
                localizedFieldsVOMap.isEmpty() ? getLocalizedFieldsVOMap() : localizedFieldsVOMap,
                updateIfDataInRequest(module,getModule()),
                permissionType == null ? getPermissionType() : permissionType,
                active,
                createdAt,
                lastModifiedAt,
                status.CREATED
        );
    }

    //utils
    private <T> String updateIfFoundInRequest(String valueInRequest, String valueInCurrentState) {
        if (valueInRequest == null) return valueInCurrentState;
        return !valueInRequest.isEmpty() ? valueInRequest : valueInCurrentState;
    }

/*    //utils
    private <T> List<T> updateIfDataInRequest(List<T> valueInRequest, List<T> valueInCurrentState) {
        if (valueInRequest == null) return valueInCurrentState;
        return !valueInRequest.isEmpty() ? valueInRequest : valueInCurrentState;
    }*/

    //utils
    private static String updateIfDataInRequest(String valueInRequest, String valueInCurrentState) {
        return StringUtils.isNotEmpty(valueInRequest) ? valueInRequest : valueInCurrentState;
    }

}
