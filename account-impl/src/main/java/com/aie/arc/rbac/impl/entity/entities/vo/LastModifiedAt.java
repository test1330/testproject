package com.aie.arc.rbac.impl.entity.entities.vo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.time.LocalDateTime;

@Value
@JsonDeserialize
public class LastModifiedAt implements Jsonable {
    private LocalDateTime lastModifiedAt;

    public LastModifiedAt(){
        lastModifiedAt = null;
    }
    @JsonCreator
    public LastModifiedAt(LocalDateTime lastModifiedAt){
        this.lastModifiedAt = lastModifiedAt;
    }

}
