package com.aie.arc.rbac.impl.event;

import com.aie.arc.rbac.impl.entity.entities.AccessRight;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;


public interface AccessRightEvent extends Jsonable, AggregateEvent<AccessRightEvent> {

    int NUM_SHARDS = 4;
    AggregateEventShards<AccessRightEvent> TAG = AggregateEventTag.sharded(AccessRightEvent.class, NUM_SHARDS);

    String getAccessId();

    default boolean isPublic() {
        return true;
    }


    @Value
    @JsonDeserialize
    class AccessRightCreated implements AccessRightEvent {
        public final AccessRight accessRight;

        @JsonCreator
        public AccessRightCreated(AccessRight accessRight) {
            this.accessRight = accessRight;
        }

        @Override
        public String getAccessId() {
            return accessRight.getAccessId();
        }
    }



    @Value
    @JsonDeserialize
    class AccessRightUpdated implements AccessRightEvent {
        public final AccessRight accessRight;

        @JsonCreator
        public AccessRightUpdated(AccessRight accessRight)
        {
            this.accessRight = accessRight;
        }

        @Override
        public String getAccessId() {
            return accessRight.getAccessId();
        }
    }


    @Value
    @JsonDeserialize
    class AccessRightDeactivated implements AccessRightEvent {
        public final String accessId;
        public final boolean active;

        @JsonCreator
        public AccessRightDeactivated(String accessId, boolean active)
        {
            this.accessId = accessId;
            this.active = active;
        }

        @Override
        public String getAccessId() {
            return accessId;
        }
    }


    @Value
    @JsonDeserialize
    class AccessRightActivated implements AccessRightEvent {
        public final String accessId;
        public final boolean active;

        @JsonCreator
        public AccessRightActivated(String accessId, boolean active)
        {
            this.accessId = accessId;
            this.active = active;
        }

        @Override
        public String getAccessId() {
            return accessId;
        }
    }


    @Override
    default AggregateEventTagger<AccessRightEvent> aggregateTag() {
        return TAG;
    }
}



