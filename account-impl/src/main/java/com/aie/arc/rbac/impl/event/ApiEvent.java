package com.aie.arc.rbac.impl.event;

import com.aie.arc.rbac.impl.entity.entities.Api;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

public interface ApiEvent extends Jsonable, AggregateEvent<ApiEvent> {
    int NUM_SHARDS = 4;
    AggregateEventShards<ApiEvent> TAG = AggregateEventTag.sharded(ApiEvent.class, NUM_SHARDS);

    String getApiId();

    default boolean isPublic() {
        return true;
    }
    @Override
    default AggregateEventTagger<ApiEvent> aggregateTag() {
        return TAG;
    }

    /**
     * Indicates a API has been created.
     */
    @Value
    @JsonDeserialize
    class ApiAdded implements ApiEvent {
        public final Api api;

        public ApiAdded() {
            api=null;
        }
        @JsonCreator
        public ApiAdded(Api api) {
            this.api = api;
        }

        @Override
        public String getApiId() {
            return api.getId();
        }
    }

    /**
     * Indicates a API has been updated.
     */
    @Value
    @JsonDeserialize
    class ApiUpdated implements ApiEvent {
        public final Api api;

        public ApiUpdated() {
            api=null;
        }
        @JsonCreator
        public ApiUpdated(Api api) {
            this.api = api;
        }

        @Override
        public String getApiId() {
            return api.getId();
        }
    }

    /**
     * Indicates a API Removed
     */
    @Value
    @JsonDeserialize
    class ApiActivated implements ApiEvent {
        public final String apiId;
        public final boolean value;

        public ApiActivated() {
            apiId=null;
            value =false;
        }
        @JsonCreator
        public ApiActivated(String apiId, boolean value) {
            this.apiId = apiId;
            this.value = value;
        }
        @Override
        public String getApiId() {
            return apiId;
        }
    }
    @Value
    @JsonDeserialize
    class ApiDeactivated implements ApiEvent {
        public final String apiId;
        public final boolean value;

        public ApiDeactivated() {
            apiId = null;
            value = false;
        }
        @JsonCreator
        public ApiDeactivated(String apiId, boolean value) {
            this.apiId = apiId;
            this.value = value;
        }

        @Override
        public String getApiId() {
            return apiId;
        }
    }
}
