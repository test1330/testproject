package com.aie.arc.rbac.impl.event;

import com.aie.arc.rbac.impl.entity.entities.MicroService;
import com.aie.arc.rbac.impl.entity.entities.RBACLocalizedFieldsVO;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import java.util.Map;

public interface MicroServiceEvent extends Jsonable, AggregateEvent<MicroServiceEvent> {
    int NUM_SHARDS = 4;
    AggregateEventShards<MicroServiceEvent> TAG = AggregateEventTag.sharded(MicroServiceEvent.class, NUM_SHARDS);

    String getMicroServiceId();

    default boolean isPublic() {
        return true;
    }

    @Override
    default AggregateEventTagger<MicroServiceEvent> aggregateTag() {
        return TAG;
    }

    /**
     * Indicates a MicroService has been created.
     */
    @Value
    @JsonDeserialize
    class MicroServiceCreated implements MicroServiceEvent {
        public final MicroService microservice;

        public MicroServiceCreated() {
            microservice = null;
        }

        @JsonCreator
        public MicroServiceCreated(MicroService microservice) {
            this.microservice = microservice;
        }

        @Override
        public String getMicroServiceId() {
            return microservice.getMicroServiceId();
        }
    }

    /**
     * Indicates a MicroService has been updated.
     */
    @Value
    @JsonDeserialize
    class MicroServiceUpdated implements MicroServiceEvent {
        public final MicroService microservice;

        public MicroServiceUpdated() {
            microservice = null;
        }

        @JsonCreator
        public MicroServiceUpdated(MicroService microservice) {
            this.microservice = microservice;
        }

        @Override
        public String getMicroServiceId() {
            return microservice.getMicroServiceId();
        }
    }

    /**
     * Indicates a MicroService Removed
     */
    @Value
    @JsonDeserialize
    class MicroServiceActivated implements MicroServiceEvent {
        public final String microServiceId;
        public final boolean active;

        public MicroServiceActivated() {
            microServiceId = null;
            active = false;
        }
        @JsonCreator
        public MicroServiceActivated(String microServiceId, boolean active) {
            this.microServiceId = microServiceId;
            this.active = active;
        }

        @Override
        public String getMicroServiceId() {
            return microServiceId;
        }
    }
    @Value
    @JsonDeserialize
    class MicroServiceDeactivated implements MicroServiceEvent {
        public final String microServiceId;
        public final boolean active;

        public MicroServiceDeactivated() {
            microServiceId = null;
            active = false;
        }
        @JsonCreator
        public MicroServiceDeactivated(String microServiceId, boolean active) {
            this.microServiceId = microServiceId;
            this.active = active;
        }

        @Override
        public String getMicroServiceId() {
            return microServiceId;
        }
    }

    /**
     * Indicates a Api has been added to MicroService.
     */
    @Value
    @JsonDeserialize
    class ApiAddedToMicroService implements MicroServiceEvent {
        public final String microServiceId;
        public final String apiId;
        public final  Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap;
        public final boolean active;

        public ApiAddedToMicroService() {
            microServiceId = null;
            apiId = null;
            localizedFieldsVOMap = null;
            active = false;
        }
        @JsonCreator
        public ApiAddedToMicroService(String microServiceId, String apiId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, boolean active) {
            this.microServiceId = microServiceId;
            this.apiId = apiId;
            this.localizedFieldsVOMap = localizedFieldsVOMap;
            this.active = active;
        }

        @Override
        public String getMicroServiceId() {
            return microServiceId;
        }
    }

    @Value
    @JsonDeserialize
    class ApiUpdatedInMicroService implements MicroServiceEvent {
        public final String microServiceId;
        public final String apiId;
        public final Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap;
        public final boolean active;

        public ApiUpdatedInMicroService() {
            microServiceId = null;
            apiId = null;
            localizedFieldsVOMap = null;
            active = false;
        }
        @JsonCreator
        public ApiUpdatedInMicroService(String microServiceId, String apiId, Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap, boolean active) {
            this.microServiceId = microServiceId;
            this.apiId = apiId;
            this.localizedFieldsVOMap = localizedFieldsVOMap;
            this.active = active;
        }

        @Override
        public String getMicroServiceId() {
            return microServiceId;
        }
    }

    @Value
    @JsonDeserialize
    class ApiDeactivateInMicroService implements MicroServiceEvent {
        public final String microServiceId;
        public final String apiId;
        public final boolean value;

        @JsonCreator
        public ApiDeactivateInMicroService(String microServiceId,String apiId, boolean value) {
            this.microServiceId = microServiceId;
            this.apiId = apiId;
            this.value = value;
        }

        @Override
        public String getMicroServiceId() {
            return microServiceId;
        }
    }
    @Value
    @JsonDeserialize
    class ApiActivateInMicroService implements MicroServiceEvent {
        public final String microServiceId;
        public final String apiId;
        public final boolean value;

        @JsonCreator
        public ApiActivateInMicroService(String microServiceId,String apiId, boolean value) {
            this.microServiceId = microServiceId;
            this.apiId = apiId;
            this.value = value;
        }

        @Override
        public String getMicroServiceId() {
            return microServiceId;
        }
    }
}
