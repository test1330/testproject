package com.aie.arc.rbac.impl.event;

import com.aie.arc.rbac.impl.entity.entities.Permission;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

public interface PermissionEvent extends Jsonable, AggregateEvent<PermissionEvent> {
    int NUM_SHARDS = 4;
    AggregateEventShards<PermissionEvent> TAG = AggregateEventTag.sharded(PermissionEvent.class, NUM_SHARDS);

    String getPermissionId();

    default boolean isPublic() {
        return true;
    }

    @Override
    default AggregateEventTagger<PermissionEvent> aggregateTag() {
        return TAG;
    }

    /**
     * Indicates a Permission has been created.
     */
    @Value
    @JsonDeserialize
    class PermissionCreated implements PermissionEvent {
        public final Permission permission;

        public PermissionCreated() {
            permission = null;
        }
        @JsonCreator
        public PermissionCreated(Permission permission) {
            this.permission = permission;
        }

        @Override
        public String getPermissionId() {
            return permission.getPermissionId();
        }
    }

    /**
     * Indicates a Permission has been updated.
     */
    @Value
    @JsonDeserialize
    class PermissionUpdated implements PermissionEvent {
        public final Permission permission;

        public PermissionUpdated() {
            permission = null;
        }
        @JsonCreator
        public PermissionUpdated(Permission permission) {
            this.permission = permission;
        }

        @Override
        public String getPermissionId() {
            return permission.getPermissionId();
        }
    }


    /**
     * Indicates a Permission Removed
     */
    @Value
    @JsonDeserialize
    class PermissionActivated implements PermissionEvent {
        public final String permissionId;
        public final boolean value;

        public PermissionActivated() {
            permissionId = null;
            value = false;
        }
        @JsonCreator
        public PermissionActivated(String permissionId, boolean value) {
            this.permissionId = permissionId;
            this.value = value;
        }

        @Override
        public String getPermissionId() {
            return permissionId;
        }
    }
    @Value
    @JsonDeserialize
    class PermissionDeactivated implements PermissionEvent {
        public final String permissionId;
        public final boolean value;

        public PermissionDeactivated() {
            permissionId = null;
            value = false;
        }
        @JsonCreator
        public PermissionDeactivated(String permissionId, boolean value) {
            this.permissionId = permissionId;
            this.value = value;
        }

        @Override
        public String getPermissionId() {
            return permissionId;
        }
    }
}
