package com.aie.arc.rbac.impl.event;

import com.aie.arc.rbac.impl.entity.entities.Role;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

/*Role Event..*/
public interface RoleEvent extends Jsonable, AggregateEvent<RoleEvent> {
    int NUM_SHARDS = 4;
    AggregateEventShards<RoleEvent> TAG = AggregateEventTag.sharded(RoleEvent.class, NUM_SHARDS);

    String getRoleId();

    default boolean isPublic() {
        return true;
    }
    @Override
    default AggregateEventTagger<RoleEvent> aggregateTag() {
        return TAG;
    }

    /**
     * Indicates a Role has been created.
     */
    @Value
    @JsonDeserialize
    class RoleCreated implements RoleEvent {
        public final Role role;

        public RoleCreated() {
            role=null;
        }
        @JsonCreator
        public RoleCreated(Role role) {
            this.role = role;
        }

        @Override
        public String getRoleId() {
            return role.getRoleId();
        }
    }

    /**
     * Indicates a Role has been updated.
     */
    @Value
    @JsonDeserialize
    class RoleUpdated implements RoleEvent {
        public final Role role;

        public RoleUpdated() {
            role=null;
        }
        @JsonCreator
        public RoleUpdated(Role role) {
            this.role = role;
        }

        @Override
        public String getRoleId() {
            return role.getRoleId();
        }
    }

    /**
     * Indicates a Role has been updated.
     */
    @Value
    @JsonDeserialize
    class RoleActivated implements RoleEvent {
        public final String roleId;
        public final boolean value;

        public RoleActivated() {
            roleId=null;
            value =false;
        }
        @JsonCreator
        public RoleActivated(String roleId, boolean value) {
            this.roleId = roleId;
            this.value = value;
        }

        @Override
        public String getRoleId() {
            return roleId;
        }
    }
    @Value
    @JsonDeserialize
    class RoleDeactivated implements RoleEvent {
        public final String roleId;
        public final boolean value;

        public RoleDeactivated() {
            roleId=null;
            value =false;
        }
        @JsonCreator
        public RoleDeactivated(String roleId, boolean value) {
            this.roleId = roleId;
            this.value = value;
        }

        @Override
        public String getRoleId() {
            return roleId;
        }
    }
}