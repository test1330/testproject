package com.aie.arc.rbac.impl.event;

import com.aie.arc.rbac.impl.entity.entities.RoleGroup;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

import javax.inject.Inject;

public interface RoleGroupEvent extends Jsonable, AggregateEvent<RoleGroupEvent> {
    int NUM_SHARDS = 4;
    AggregateEventShards<RoleGroupEvent> TAG = AggregateEventTag.sharded(RoleGroupEvent.class, NUM_SHARDS);

    String getRoleGroupId();

    default boolean isPublic() {
        return true;
    }
    /**
     * Indicates a RoleGroup has been created.
     */
    @Value
    @JsonDeserialize
    class RoleGroupCreated implements RoleGroupEvent {
        public final RoleGroup roleGroup;

        RoleGroupCreated() {
            this.roleGroup = null;
        }

        @JsonCreator
        public RoleGroupCreated(RoleGroup roleGroup) {
            this.roleGroup = roleGroup;
        }

        @Override
        public String getRoleGroupId() {
            return roleGroup.getRoleGroupId();
        }
    }

    /**
     * Indicates a RoleGroup has been updated.
     */
    @Value
    @JsonDeserialize
    class RoleGroupUpdated implements RoleGroupEvent {
        public final RoleGroup roleGroup;

        public RoleGroupUpdated() {
            roleGroup=null;
        }
        @JsonCreator
        public RoleGroupUpdated(RoleGroup roleGroup) {
            this.roleGroup = roleGroup;
        }

        @Override
        public String getRoleGroupId() {
            return roleGroup.getRoleGroupId();
        }
    }

    /**
     * Indicates a Project Removed
     */
    @Value
    @JsonDeserialize
    class RoleGroupActivated implements RoleGroupEvent {
        public final String roleGroupId;
        public final boolean value;

        public RoleGroupActivated() {
            roleGroupId=null;
            value =false;
        }
        @JsonCreator
        public RoleGroupActivated(String roleGroupId, boolean value) {
            this.roleGroupId = roleGroupId;
            this.value = value;
        }

        @Override
        public String getRoleGroupId() {
            return roleGroupId;
        }
    }

    @Value
    @JsonDeserialize
    class RoleGroupDeactivated implements RoleGroupEvent {
        public final String roleGroupId;
        public final boolean value;

        public RoleGroupDeactivated() {
            roleGroupId=null;
            value =false;
        }
        @JsonCreator
        public RoleGroupDeactivated(String roleGroupId, boolean value) {
            this.roleGroupId = roleGroupId;
            this.value = value;
        }

        @Override
        public String getRoleGroupId() {
            return roleGroupId;
        }
    }

    @Override
    default AggregateEventTagger<RoleGroupEvent> aggregateTag() {
        return TAG;
    }
}
