package com.aie.arc.rbac.impl.event;

import com.aie.arc.rbac.impl.entity.entities.Workspace;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

public interface WorkspaceEvent extends Jsonable, AggregateEvent<WorkspaceEvent> {
    int NUM_SHARDS = 4;
    AggregateEventShards<WorkspaceEvent> TAG = AggregateEventTag.sharded(WorkspaceEvent.class, NUM_SHARDS);
    String getWorkspaceId();
    default boolean isPublic() {
        return true;
    }

    @Value
    @JsonDeserialize
    class WorkspaceCreated implements WorkspaceEvent {
        public final Workspace workspace;
        @JsonCreator
        public WorkspaceCreated( Workspace workspace) {
            this.workspace = workspace;
        }
        @Override
        public String getWorkspaceId(){
            return workspace.getWorkspaceId();
        }
    }

    @Value
    @JsonDeserialize
    class WorkspaceUpdated implements WorkspaceEvent {
        public final Workspace workspace;
        @JsonCreator
        public WorkspaceUpdated(Workspace workspace)
        {
            this.workspace = workspace;
        }
        @Override
        public String getWorkspaceId() {
            return workspace.getWorkspaceId();
        }
    }

    @Value
    @JsonDeserialize
    class WorkspaceDeactivated implements WorkspaceEvent {
        public final String workspaceId;
        public final boolean active;
        @JsonCreator
        public WorkspaceDeactivated(String workspaceId, boolean active)
        {
            this.workspaceId = workspaceId;
            this.active = active;
        }
        @Override
        public String getWorkspaceId() {
            return workspaceId;
        }
    }

    @Value
    @JsonDeserialize
    class WorkspaceActivated implements WorkspaceEvent {
        public final String workspaceId;
        public final boolean active;
        @JsonCreator
        public WorkspaceActivated(String workspaceId, boolean active)
        {
            this.workspaceId = workspaceId;
            this.active = active;
        }
        @Override
        public String getWorkspaceId() {
            return workspaceId;
        }
    }

    @Override
    default AggregateEventTagger<WorkspaceEvent> aggregateTag() {
        return TAG;
    }
}




