package com.aie.arc.rbac.impl.mapper;

import com.aie.arc.account.api.common.request.LocalisedFields;
import com.aie.arc.rbac.api.response.*;
import com.aie.arc.rbac.impl.entity.entities.*;
import com.aie.arc.rbac.impl.service.RBACServiceRequestValidation;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lightbend.lagom.javadsl.api.transport.NotFound;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/*mapper lass*/
public class RBACMapper {
    /*map Role to Role Details */
    public static RoleDetail toRoleDetails(Role role,List<PermissionDetail> permissionDetails) {
        return new RoleDetail(
                role.getRoleId(),
                toLocalisedFieldMap(role.getLocalizedFieldsVOMap()),
                permissionDetails,
                role.getRoleGroupId(),
                role.isActive()
        );
    }

    public static APITagDetail toBusinessUserAPIDetails(Set<String> apiTagDetail) {
        return new APITagDetail(
                apiTagDetail
        );
    }


    public static RoleDetail toRoleDetails(Role role) {
        return new RoleDetail(
                role.getRoleId(),
                toLocalisedFieldMap(role.getLocalizedFieldsVOMap()),
                role.getPermissionIds(),
                null,
                role.getRoleGroupId(),
                role.isActive()
        );
    }
    /*map RoleGroup to RoleGroupDetail */
    public static RoleGroupDetail toRoleGroupDetails(RoleGroup roleGroup) {
        return new RoleGroupDetail(
                roleGroup.getRoleGroupId(),
                toLocalisedFieldMap(roleGroup.getLocalizedFieldsVOMap()),
                roleGroup.isActive()
        );
    }

    /*map Role to MicroService Details */
    public static MicroServiceDetail toMicroServiceDetails(MicroService microService) {
        return new MicroServiceDetail(
                microService.getMicroServiceId(),
                toLocalisedFieldMap(microService.getLocalizedFieldsVOMap()),
                microService.getApiIds(),
                microService.isActive()
        );
    }

    /*map Api to Api Details */
    public static ApiDetail toMicroServiceApiDetails(Api api) {
        return new ApiDetail(
                api.getId(),
                toLocalisedFieldMap(api.getLocalizedFieldsVOMap()),
                api.isActive()
        );
    }

    /*map Api to Api Details */
    public static PermissionDetailWithTag toPermissionDetailsWithTag(Permission permission) {
        return new PermissionDetailWithTag(
                permission.getPermissionId(),
                toLocalisedFieldMap(permission.getLocalizedFieldsVOMap()),
                null,
                null,
                permission.getWorkspaceId(),
                permission.getApiTags(),
                permission.isActive()
        );
    }
    public static PermissionDetailWithTag toPermissionDetailsWithTag(Permission permission,AccessRightDetail accessRight, WorkspaceDetail workspace) {
        return new PermissionDetailWithTag(
                permission.getPermissionId(),
                toLocalisedFieldMap(permission.getLocalizedFieldsVOMap()),
                accessRight,
                workspace,
                permission.getWorkspaceId(),
                permission.getApiTags(),
                permission.isActive()
        );
    }

    public static PermissionDetail toPermissionDetails(Permission permission,AccessRightDetail accessRight, WorkspaceDetail workspace) {
        return new PermissionDetail(
                permission.getPermissionId(),
                toLocalisedFieldMap(permission.getLocalizedFieldsVOMap()),
                accessRight,
                workspace,
                permission.getWorkspaceId(),
                permission.isActive()
        );
    }
    //Map workspace

    public static WorkspaceDetail toWorkspaceDetails(Workspace workspace) {
        return new WorkspaceDetail(
                workspace.getWorkspaceId(),
                toLocalisedFieldMap(workspace.getLocalizedFieldsVOMap()),
                workspace.getModule(),
                workspace.getPermissionType(),
                workspace.isActive()
        );
    }


    public static AccessRightDetail toAccessRightDetails(AccessRight accessRight) {
        return new AccessRightDetail(
                accessRight.getAccessId(),
                toLocalisedFieldMap(accessRight.getLocalizedFieldsVOMap()),
                accessRight.isActive()
        );
    }

    public static List<String> toRoleGroupList(List<String> roleGroupIds, RBACServiceRequestValidation roleGroupValidator) {
        return roleGroupIds.stream().map(roleGroupId -> {
            try {
                return getJsonOfRoleGroupByRoleGroupId(roleGroupId, roleGroupValidator);
            } catch (JsonProcessingException e) {
                throw new NotFound("JsonProcessingException encountered");
            }
        }).collect(Collectors.toList());
    }

    public static List<String> toRoleList(List<String> roleIds, RBACServiceRequestValidation roleValidator) {
        return roleIds.stream().map(roleId -> {
            try {
                return getJsonOfRoleByRoleId(roleId, roleValidator);
            } catch (JsonProcessingException e) {
                throw new NotFound("JsonProcessingException encountered");
            }
        }).collect(Collectors.toList());
    }

    public static List<String> toMicroServiceList(List<String> microServiceIds, RBACServiceRequestValidation microServiceValidator) {
        return microServiceIds.stream().map(microServiceId -> {
            try {
                return getJsonOfMicroServiceByMicroServiceId(microServiceId, microServiceValidator);
            } catch (JsonProcessingException e) {
                throw new NotFound("JsonProcessingException encountered");
            }
        }).collect(Collectors.toList());
    }
    public static List<String> toPermissionList(List<String> permissionIds, RBACServiceRequestValidation permissionValidator) {
        return permissionIds.stream().map(permissionId -> {
            try {
                return getJsonOfPermissionByPermissionId(permissionId, permissionValidator);
            } catch (JsonProcessingException e) {
                throw new NotFound("JsonProcessingException encountered");
            }
        }).collect(Collectors.toList());
    }

    public static List<String> toMicroServiceApiList(List<String> apiIds, RBACServiceRequestValidation apiValidator) {
        return apiIds.stream().map(apiId -> {
            try {
                return getJsonOfApiByApiId(apiId, apiValidator);
            } catch (JsonProcessingException e) {
                throw new NotFound("JsonProcessingException encountered");
            }
        }).collect(Collectors.toList());
    }

    private static Map<String, LocalisedFields> toLocalisedFieldMap(Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap) {
        Map<String, LocalisedFields> localisedFieldsMap = new HashMap<>();
        localizedFieldsVOMap.forEach(((code, localised) -> {
            localisedFieldsMap.put(code, new LocalisedFields(localised.getName(), localised.getDescription()));
        }));
        return localisedFieldsMap;
    }
    private static String getJsonOfPermissionByPermissionId(String permissionId, RBACServiceRequestValidation permissionValidator) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(permissionValidator.validateAndGetPermissionById(permissionId));
    }

    private static String getJsonOfApiByApiId(String apiId, RBACServiceRequestValidation apiValidator) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(apiValidator.validateAndGetApiById(apiId));
    }

    private static String getJsonOfMicroServiceByMicroServiceId(String microServiceId, RBACServiceRequestValidation microServiceValidator) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(microServiceValidator.validateAndGetMicroServiceById(microServiceId));
    }

    private static String getJsonOfRoleGroupByRoleGroupId(String roleGroupId, RBACServiceRequestValidation roleGroupValidator) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(roleGroupValidator.validateAndGetRoleGroupById(roleGroupId));
    }

    private static String getJsonOfRoleByRoleId(String roleId, RBACServiceRequestValidation roleValidator) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(roleValidator.validateAndGetRoleById(roleId));
    }


    public static GetRoleDetail toGetRoleDetails(Role role,List<GetPermissionDetail> permissionDetails) {
        return new GetRoleDetail(
                role.getRoleId(),
                toLocalisedFieldMap(role.getLocalizedFieldsVOMap()),
                permissionDetails,
                role.getRoleGroupId(),
                role.isActive()
        );
    }

    public static GetPermissionDetail toGetPermissionDetails(Permission permission, WorkspaceDetail workspace) {
        return new GetPermissionDetail(
                permission.getPermissionId(),
                workspace.getModule(),
                workspace.getPermissionType(),
                permission.isActive()
        );
    }
}
