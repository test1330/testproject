package com.aie.arc.rbac.impl.repository;

import com.aie.arc.account.impl.entity.entities.ListWrapper;
import com.aie.arc.account.impl.entity.entities.vo.common.FilterParameter;
import com.aie.arc.account.impl.entity.entities.vo.common.MappingData;
import com.aie.arc.account.impl.util.common.FilterConfig;
import com.aie.arc.account.impl.util.common.RepositoryUtil;
import com.aie.arc.rbac.impl.repository.readside.AccessRightReadSide;
import com.lightbend.lagom.javadsl.persistence.ReadSide;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcSession;
import lombok.extern.slf4j.Slf4j;
import org.pcollections.PSequence;
import org.pcollections.TreePVector;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;
import java.util.concurrent.CompletionStage;

@Singleton
@Slf4j
public class AccessRightRepository {

    private final JdbcSession session;

    @Inject
    public AccessRightRepository(JdbcSession session, ReadSide readSide) {
        this.session = session;
        readSide.register(AccessRightReadSide.class);
    }

    public CompletionStage<ListWrapper> getAccessRightIds(FilterParameter filter) {
        return session.withConnection(
                connection -> {
                    Map<String, MappingData> assetTypeFieldMap = FilterConfig.getAccessRightTypeMap();
                    StringBuilder dynamicQuery = new StringBuilder();
                    filter.getFilters().forEach((key, value) -> {
                        MappingData mappingData = assetTypeFieldMap.get(key);
                        switch (mappingData.getType()) {
                            case BOOLEAN:
                                RepositoryUtil.appendBooleanParameter(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case STRING:
                                RepositoryUtil.appendStringParameter(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case INTEGER:
                                RepositoryUtil.addIntegerFieldInDynamicQuery(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case DATE:
                                break;
                        }
                    });
                    StringBuilder selectQuery = new StringBuilder("SELECT DISTINCT(access_right_id),* FROM access_right");
                    int count = RepositoryUtil.getCount(connection, dynamicQuery, "access_right", "access_right_id");
                    RepositoryUtil.addSortColumnInDynamicQuery(dynamicQuery, assetTypeFieldMap.get(filter.getAscendingField()).getColumnName(),  filter.getAscending());
                    selectQuery.append(dynamicQuery);
                    int offset = filter.getPageNo() * filter.getPageSize();
                    // return empty if offset is greater than count
                    if (offset > count) {
                        return new ListWrapper(TreePVector.empty(), count);
                    }
                    selectQuery.append(" LIMIT " + filter.getPageSize() + " OFFSET " + offset);
                    PreparedStatement statement = connection.prepareStatement(selectQuery.toString());
                    log.debug("selectQuery : {}" ,selectQuery);
                    try (ResultSet resultSet = statement.executeQuery()) {
                        PSequence<String> accessRightIds = TreePVector.empty();
                        while (resultSet.next()) {
                            accessRightIds = accessRightIds.plus(resultSet.getString("access_right_id"));
                        }
                        return new ListWrapper(accessRightIds, count);
                    }
                }
        );
    }
}
