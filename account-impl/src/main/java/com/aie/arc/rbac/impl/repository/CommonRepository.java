package com.aie.arc.rbac.impl.repository;

import com.aie.arc.rbac.impl.exceptions.ParameterRequired;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcSession;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.CompletionStage;

@Singleton
public class CommonRepository {

        private final JdbcSession jdbcSession;

        @Inject
        public CommonRepository(JdbcSession jdbcSession) {
            this.jdbcSession = jdbcSession;
        }

        public boolean doesNameAlreadyExist(String table, String column, String name) {
            return jdbcSession.withConnection(connection -> {
                PreparedStatement statement;
                try {
                    statement = connection.prepareStatement(new StringBuilder("SELECT COUNT ( * ) as count FROM ")
                            .append(table).append(" WHERE ").append(column).append(" = '").append(name).append("'").toString());
                    ResultSet countRes = statement.executeQuery();
                    countRes.next();
                    int count = countRes.getInt("count");
                    if (count != 0) {
                        return true;
                    }
                    return false;
                } catch (SQLException e) {
                    throw new ParameterRequired(new String[]{"SQL Exception encountered!", e.toString()});
                }
            }).toCompletableFuture().join();
        }
}
