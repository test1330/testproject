package com.aie.arc.rbac.impl.repository;

import com.aie.arc.account.impl.entity.entities.ListWrapper;
import com.aie.arc.account.impl.entity.entities.vo.common.FilterParameter;
import com.aie.arc.account.impl.entity.entities.vo.common.MappingData;
import com.aie.arc.account.impl.util.common.FilterConfig;
import com.aie.arc.account.impl.util.common.RepositoryUtil;
import com.aie.arc.rbac.impl.repository.readside.MicroServiceReadSide;
import com.lightbend.lagom.javadsl.persistence.ReadSide;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcSession;
import org.pcollections.PSequence;
import org.pcollections.TreePVector;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.concurrent.CompletionStage;

@Singleton
public class MicroServiceRepository {
    private final JdbcSession session;

    @Inject
    public MicroServiceRepository(JdbcSession session, ReadSide readSide) {
        this.session = session;
        //commented out unused Read side
        //readSide.register(MicroServiceReadSide.class);
    }

    public CompletionStage<ListWrapper> getMicroServiceIds(FilterParameter filter) {
        return session.withConnection(
                connection -> {
                    Map<String, MappingData> assetTypeFieldMap = FilterConfig.getMicroServiceTypeMap();
                    StringBuilder dynamicQuery = new StringBuilder();
                    filter.getFilters().forEach((key, value) -> {
                        MappingData mappingData = assetTypeFieldMap.get(key);
                        switch (mappingData.getType()) {
                            case BOOLEAN:
                                RepositoryUtil.appendBooleanParameter(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case STRING:
                                RepositoryUtil.appendStringParameter(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case INTEGER:
                                RepositoryUtil.addIntegerFieldInDynamicQuery(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case DATE:
                                break;
                        }
                    });
                    StringBuilder selectQuery = new StringBuilder("SELECT DISTINCT(micro_service_id),* FROM micro_service");
                    int count = RepositoryUtil.getCount(connection, dynamicQuery, "micro_service", "micro_service_id");
                    RepositoryUtil.addSortColumnInDynamicQuery(dynamicQuery, assetTypeFieldMap.get(filter.getAscendingField()).getColumnName(),  filter.getAscending());
                    selectQuery.append(dynamicQuery);
                    int offset = filter.getPageNo() * filter.getPageSize();
                    // return empty if offset is greater than count
                    if (offset > count) {
                        return new ListWrapper(TreePVector.empty(), count);
                    }
                    selectQuery.append(" LIMIT " + filter.getPageSize() + " OFFSET " + offset);
                    PreparedStatement statement = connection.prepareStatement(selectQuery.toString());
                    System.out.println("selectQuery = " + selectQuery);
                    try (ResultSet resultSet = statement.executeQuery()) {
                        PSequence<String> microServiceIds = TreePVector.empty();
                        while (resultSet.next()) {
                            microServiceIds = microServiceIds.plus(resultSet.getString("micro_service_id"));
                        }
                        return new ListWrapper(microServiceIds, count);
                    }
                }
        );
    }

    public CompletionStage<ListWrapper> getApiIds(FilterParameter filter, List<String> ids) {
        return session.withConnection(
                connection -> {
                    Map<String, MappingData> assetTypeFieldMap = FilterConfig.getApiFieldTypeMap();
                    StringBuilder dynamicQuery = new StringBuilder();
                    filter.getFilters().forEach((key, value) -> {
                        MappingData mappingData = assetTypeFieldMap.get(key);
                        switch (mappingData.getType()) {
                            case BOOLEAN:
                                RepositoryUtil.appendBooleanParameter(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case STRING:
                                RepositoryUtil.appendStringParameter(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case INTEGER:
                                RepositoryUtil.addIntegerFieldInDynamicQuery(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case DATE:
                                break;
                        }
                        RepositoryUtil.appendInParameter(dynamicQuery, mappingData.getColumnName(), value);
                    });
                    StringJoiner stringJoiner = new StringJoiner(",");
                    // Removed this where api_id in( " + stringJoiner.toString() + ")  because of two WHERE in query
                    ids.forEach(id -> stringJoiner.add("'" + id + "'"));
                    StringBuilder selectQuery = new StringBuilder("SELECT DISTINCT(api_id),* FROM api ");
                    int count = RepositoryUtil.getCount(connection, dynamicQuery, "api", "api_id");
                    RepositoryUtil.addSortColumnInDynamicQuery(dynamicQuery, assetTypeFieldMap.get(filter.getAscendingField()).getColumnName(),  filter.getAscending());
                    selectQuery.append(dynamicQuery);
                    int offset = filter.getPageNo() * filter.getPageSize();
                    // return empty if offset is greater than count
                    if (offset > count) {
                        return new ListWrapper(TreePVector.empty(), count);
                    }
                    selectQuery.append(" LIMIT " + filter.getPageSize() + " OFFSET " + offset);
                    PreparedStatement statement = connection.prepareStatement(selectQuery.toString());
                    System.out.println("selectQuery = " + selectQuery);
                    try (ResultSet resultSet = statement.executeQuery()) {
                        PSequence<String> apiIds = TreePVector.empty();
                        while (resultSet.next()) {
                            apiIds = apiIds.plus(resultSet.getString("api_id"));
                        }
                        return new ListWrapper(apiIds, count);
                    }
                }
        );
    }
}