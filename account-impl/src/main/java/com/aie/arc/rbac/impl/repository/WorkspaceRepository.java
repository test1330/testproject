

package com.aie.arc.rbac.impl.repository;


import com.aie.arc.account.impl.entity.entities.ListWrapper;
import com.aie.arc.account.impl.entity.entities.vo.common.FilterParameter;
import com.aie.arc.account.impl.entity.entities.vo.common.MappingData;
import com.aie.arc.account.impl.util.common.FilterConfig;
import com.aie.arc.account.impl.util.common.RepositoryUtil;
import com.aie.arc.rbac.impl.repository.readside.WorkspaceReadSide;
import com.lightbend.lagom.javadsl.persistence.ReadSide;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcSession;
import org.pcollections.PSequence;
import org.pcollections.TreePVector;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;
import java.util.concurrent.CompletionStage;

@Singleton
public class WorkspaceRepository {
    private final JdbcSession session;

    @Inject
    public WorkspaceRepository(JdbcSession session, ReadSide readSide) {
        this.session = session;
        readSide.register(WorkspaceReadSide.class);
    }


    public CompletionStage<ListWrapper> getWorkspaceIds(FilterParameter filter) {
        return session.withConnection(
                connection -> {
                    Map<String, MappingData> assetTypeFieldMap = FilterConfig.getPermissionTypeMap();
                    StringBuilder dynamicQuery = new StringBuilder();
                    filter.getFilters().forEach((key, value) -> {
                        MappingData mappingData = assetTypeFieldMap.get(key);
                        switch (mappingData.getType()) {
                            case BOOLEAN:
                                RepositoryUtil.appendBooleanParameter(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case STRING:
                                RepositoryUtil.appendStringParameter(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case INTEGER:
                                RepositoryUtil.addIntegerFieldInDynamicQuery(dynamicQuery, mappingData.getColumnName(), value);
                                break;
                            case DATE:
                                break;
                        }
                    });

                    StringBuilder selectQuery = new StringBuilder("SELECT DISTINCT(workspace_id),* FROM workspace");
                    int count = RepositoryUtil.getCount(connection, dynamicQuery, "workspace", "workspace_id");
                    RepositoryUtil.addSortColumnInDynamicQuery(dynamicQuery, assetTypeFieldMap.get(filter.getAscendingField()).getColumnName(),  filter.getAscending());
                    selectQuery.append(dynamicQuery);
                    int offset = filter.getPageNo() * filter.getPageSize();
                    // return empty if offset is greater than count
                    if (offset > count) {
                        return new ListWrapper(TreePVector.empty(), count);
                    }
                    selectQuery.append(" LIMIT " + filter.getPageSize() + " OFFSET " + offset);
                    PreparedStatement statement = connection.prepareStatement(selectQuery.toString());
                    System.out.println("selectQuery = " + selectQuery);
                    try (ResultSet resultSet = statement.executeQuery()) {
                        PSequence<String> workspaceIds = TreePVector.empty();
                        while (resultSet.next()) {
                            workspaceIds = workspaceIds.plus(resultSet.getString("workspace_id"));
                        }
                        return new ListWrapper(workspaceIds, count);
                    }
                }
        );
    }
}


