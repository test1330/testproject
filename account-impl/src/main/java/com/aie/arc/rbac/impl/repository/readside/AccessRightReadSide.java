package com.aie.arc.rbac.impl.repository.readside;

import com.aie.arc.rbac.impl.event.AccessRightEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.ReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcReadSide;
import lombok.extern.slf4j.Slf4j;
import org.pcollections.PSequence;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

@Slf4j
public class AccessRightReadSide extends ReadSideProcessor<AccessRightEvent> {
    private final JdbcReadSide jdbcReadSide;

    @Inject
    public AccessRightReadSide(JdbcReadSide jdbcReadSide) {
        this.jdbcReadSide = jdbcReadSide;
    }

    @Override
    public ReadSideHandler<AccessRightEvent> buildHandler() {
        return jdbcReadSide.<AccessRightEvent>builder("accessRightOffset")
                .setEventHandler(AccessRightEvent.AccessRightCreated.class, this::processAccessRightCreate)
                .setEventHandler(AccessRightEvent.AccessRightUpdated.class, this::processAccessRightUpdate)
                .setEventHandler(AccessRightEvent.AccessRightDeactivated.class, this::processAccessRightDeactivate)
                .setEventHandler(AccessRightEvent.AccessRightActivated.class, this::processAccessRightActivate)
                .build();
    }



    private void processAccessRightCreate(Connection connection, AccessRightEvent.AccessRightCreated event) {
        event.getAccessRight().getLocalizedFieldsVOMap().forEach((code, localizedFieldsVO) -> {
            int index = 1;
            PreparedStatement statement;
            try {
                statement = connection.prepareStatement(
                        "INSERT INTO access_right (id,access_right_id,name,description,locale_code,active) VALUES (?, ?, ?, ? ,? ,?)");
                statement.setString(index++, UUID.randomUUID().toString());
                statement.setString(index++, event.getAccessRight().getAccessId());
                statement.setString(index++, localizedFieldsVO.getName());
                statement.setString(index++, localizedFieldsVO.getDescription());
                statement.setString(index++, code);
                statement.setBoolean(index, event.getAccessRight().isActive());
                statement.execute();
                log.debug("Insert into access_right table query successfully executed for acessId: {}", event.getAccessRight().getAccessId());
            } catch (SQLException e) {
                log.error("Insert into access_right table query has failed for accessRight: {}  due to : ", event.getAccessRight(), e);
            }
        });
    }

    private void processAccessRightUpdate(Connection connection, AccessRightEvent.AccessRightUpdated event) {
        event.getAccessRight().getLocalizedFieldsVOMap().forEach((code, localizedFieldsVO) -> {
            int index = 1;
            PreparedStatement statement;
            try {
                statement = connection.prepareStatement(
                        "UPDATE  access_right SET name=? ,description=?,active=? WHERE access_right_id=? and locale_code=?");
                statement.setString(index++, localizedFieldsVO.getName());
                statement.setString(index++, localizedFieldsVO.getDescription());
                statement.setBoolean(index++, event.getAccessRight().isActive());
                statement.setString(index++, event.getAccessId());
                statement.setString(index, code);
                statement.executeUpdate();
                log.debug("Update into access_right table query successfully executed for acessId: {}", event.getAccessId());
            } catch (SQLException e) {
                log.error("Update into access_right table query has failed for accessId: {}  due to : ", event.getAccessId(), e);
            }
        });
    }

    private void processAccessRightDeactivate(Connection connection, AccessRightEvent.AccessRightDeactivated event) {
        int index = 1;
        PreparedStatement statement;

        try {
            statement = connection.prepareStatement("UPDATE access_right SET active=? WHERE access_right_id=?");
            statement.setBoolean(index++,event.isActive());
            statement.setString(index,event.getAccessId());
            statement.executeUpdate();
            log.debug("Update into access_right table query successfully executed for acessId: {} ",event.getAccessId());

        } catch (SQLException e) {
            log.error("Update into access_right table query has failed for accessId: {}  due to : ",event.getAccessId(), e);
        }


    }


    private void processAccessRightActivate(Connection connection, AccessRightEvent.AccessRightActivated event) {
        int index = 1;
        PreparedStatement statement;

        try {
            statement = connection.prepareStatement("UPDATE access_right SET active=? WHERE access_right_id=?");
            statement.setBoolean(index++,event.isActive());
            statement.setString(index,event.getAccessId());
            statement.executeUpdate();
            log.debug("Update into access_right table query successfully executed for acessId: {} ",event.getAccessId());

        } catch (SQLException e) {
            log.error("Update into access_right table query has failed for accessId: {}  due to : ",event.getAccessId(), e);
        }


    }

    @Override
    public PSequence<AggregateEventTag<AccessRightEvent>> aggregateTags() {

        return AccessRightEvent.TAG.allTags();
    }

}
