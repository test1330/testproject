package com.aie.arc.rbac.impl.repository.readside;

import com.aie.arc.rbac.impl.event.MicroServiceEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.ReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcReadSide;
import org.pcollections.PSequence;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Event processor that handles {@link MicroServiceEvent} events
 * by writing {@link } rows to the read-side database table.
 */
public class MicroServiceReadSide extends ReadSideProcessor<MicroServiceEvent> {
    private final JdbcReadSide jdbcReadSide;

    @Inject
    public MicroServiceReadSide(JdbcReadSide jdbcReadSide) {
        this.jdbcReadSide = jdbcReadSide;
    }

    @Override
    public ReadSideHandler<MicroServiceEvent> buildHandler() {
        return jdbcReadSide.<MicroServiceEvent>builder("microServiceOffset")
                .setEventHandler(MicroServiceEvent.MicroServiceCreated.class, this::processMicroServiceCreate)
                .setEventHandler(MicroServiceEvent.MicroServiceUpdated.class, this::processMicroServiceUpdate)
                .setEventHandler(MicroServiceEvent.MicroServiceActivated.class, this::processMicroServiceActivate)
                .setEventHandler(MicroServiceEvent.MicroServiceDeactivated.class, this::processMicroServiceDeactivate)
                .setEventHandler(MicroServiceEvent.ApiAddedToMicroService.class, this::processApiAdded)
                .setEventHandler(MicroServiceEvent.ApiUpdatedInMicroService.class, this::processApiUpdated)
                .setEventHandler(MicroServiceEvent.ApiDeactivateInMicroService.class, this::processApiDeactivate)
                .setEventHandler(MicroServiceEvent.ApiActivateInMicroService.class,this::processApiActivate)
                .build();
    }

    // create table script commented
    /*private void createTable(Connection connection) throws SQLException {
        connection.prepareStatement(
                "CREATE TABLE IF NOT EXISTS micro_service ( "
                        + "id VARCHAR(36) NOT NULL , "
                        + "micro_service_id VARCHAR(36) NOT NULL , "
                        + "name VARCHAR(64) NOT NULL, "
                        + "locale_code  VARCHAR(10), "
                        + "active boolean, "
                        + " PRIMARY KEY (id))").execute();
        connection.prepareStatement(
                "CREATE TABLE IF NOT EXISTS api ( "
                        + "id VARCHAR(36) NOT NULL , "
                        + "api_id VARCHAR(36) NOT NULL , "
                        + "micro_service_id VARCHAR(36) NOT NULL , "
                        + "name VARCHAR(64) NOT NULL, "
                        + "locale_code  VARCHAR(10), "
                        + "active boolean, "
                        + " PRIMARY KEY (id))").execute();
    }*/

    private void processMicroServiceCreate(Connection connection, MicroServiceEvent.MicroServiceCreated event) {
        event.getMicroservice().getLocalizedFieldsVOMap().forEach((code, localizedFieldsVO) -> {
            int index = 1;
            PreparedStatement statement;
            try {
                statement = connection.prepareStatement(
                        "INSERT INTO micro_service (id,micro_service_id, name,locale_code,active) VALUES (?, ?, ?, ?, ?)");
                statement.setString(index++, UUID.randomUUID().toString());
                statement.setString(index++, event.getMicroservice().getMicroServiceId());
                statement.setString(index++, localizedFieldsVO.getName());
                statement.setString(index++, code);
                statement.setBoolean(index, event.getMicroservice().isActive());
                statement.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void processMicroServiceUpdate(Connection connection, MicroServiceEvent.MicroServiceUpdated event) {
        event.getMicroservice().getLocalizedFieldsVOMap().forEach((code, localizedFieldsVO) -> {
            PreparedStatement statement;
            int index = 1;
            try {
                statement = connection.prepareStatement(
                        "UPDATE micro_service SET name=?,active=? WHERE micro_service_id=? and locale_code=?");
                statement.setString(index++, localizedFieldsVO.getName());
                statement.setBoolean(index, event.getMicroservice().isActive());
                statement.setString(index++, event.getMicroservice().getMicroServiceId());
                statement.setString(index, code);
                statement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void processMicroServiceActivate(Connection connection, MicroServiceEvent.MicroServiceActivated event) throws SQLException {
        int index = 1;
        PreparedStatement statement = connection.prepareStatement(
                "UPDATE micro_service SET active=? WHERE micro_service_id=?");
        statement.setBoolean(index++, event.isActive());
        statement.setString(index, event.getMicroServiceId());
        statement.executeUpdate();
    }

    private void processMicroServiceDeactivate(Connection connection, MicroServiceEvent.MicroServiceDeactivated event) throws SQLException {
        int index = 1;
        PreparedStatement statement = connection.prepareStatement(
                "UPDATE micro_service SET active=? WHERE micro_service_id=?");
        statement.setBoolean(index++, event.isActive());
        statement.setString(index, event.getMicroServiceId());
        statement.executeUpdate();
    }

    private void processApiAdded(Connection connection, MicroServiceEvent.ApiAddedToMicroService event) {
        event.getLocalizedFieldsVOMap().forEach((code, localizedFieldsVO) -> {
            int index = 1;
            PreparedStatement statement;
            try {
                statement = connection.prepareStatement(
                        "INSERT INTO api (id,api_id,micro_service_id, name,locale_code,active) VALUES (?, ?, ? ,? ,? ,?)");
                statement.setString(index++, UUID.randomUUID().toString());
                statement.setString(index++, event.getApiId());
                statement.setString(index++, event.getMicroServiceId());
                statement.setString(index++, localizedFieldsVO.getName());
                statement.setString(index++, code);
                statement.setBoolean(index, event.isActive());
                statement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void processApiUpdated(Connection connection, MicroServiceEvent.ApiUpdatedInMicroService event) {
        event.getLocalizedFieldsVOMap().forEach((code, localizedFieldsVO) -> {
            int index = 1;
            PreparedStatement statement;
            try {
                statement = connection.prepareStatement(
                        "UPDATE  api SET name=?,active=? WHERE api_id=? and micro_service_id=? and locale_code=?");
                statement.setString(index++, localizedFieldsVO.getName());
                statement.setBoolean(index++, event.isActive());
                statement.setString(index++, event.getApiId());
                statement.setString(index++, event.getMicroServiceId());
                statement.setString(index, code);
                statement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void processApiDeactivate(Connection connection, MicroServiceEvent.ApiDeactivateInMicroService event) {
            int index = 1;
            PreparedStatement statement;
            try {
                statement = connection.prepareStatement(
                        "UPDATE  api SET active=? WHERE api_id=? and micro_service_id=?");
                statement.setBoolean(index++, event.isValue());
                statement.setString(index++, event.getApiId());
                statement.setString(index, event.getMicroServiceId());
                statement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
    }

    private void processApiActivate(Connection connection, MicroServiceEvent.ApiActivateInMicroService event) {
        int index = 1;
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(
                    "UPDATE  api SET active=? WHERE api_id=? and micro_service_id=?");
            statement.setBoolean(index++, event.isValue());
            statement.setString(index++, event.getApiId());
            statement.setString(index, event.getMicroServiceId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public PSequence<AggregateEventTag<MicroServiceEvent>> aggregateTags() {
        return MicroServiceEvent.TAG.allTags();
    }

}
