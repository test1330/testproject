package com.aie.arc.rbac.impl.repository.readside;

import com.aie.arc.rbac.impl.event.PermissionEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.ReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcReadSide;
import org.pcollections.PSequence;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

public class PermissionReadSide extends ReadSideProcessor<PermissionEvent> {
    private final JdbcReadSide jdbcReadSide;

    @Inject
    public PermissionReadSide(JdbcReadSide jdbcReadSide) {
        this.jdbcReadSide = jdbcReadSide;
    }

    @Override
    public ReadSideProcessor.ReadSideHandler<PermissionEvent> buildHandler() {
        return jdbcReadSide.<PermissionEvent>builder("permissionOffset")
                .setEventHandler(PermissionEvent.PermissionCreated.class, this::processPermissionCreate)
                .setEventHandler(PermissionEvent.PermissionUpdated.class, this::processPermissionUpdate)
                .setEventHandler(PermissionEvent.PermissionActivated.class, this::processPermissionActivate)
                .setEventHandler(PermissionEvent.PermissionDeactivated.class, this::processPermissionDeactivate)
                .build();
    }

    private void processPermissionCreate(Connection connection, PermissionEvent.PermissionCreated event) {
        event.getPermission().getLocalizedFieldsVOMap().forEach((code, localizedFieldsVO) -> {
                int index = 1;
                PreparedStatement statement;
                try {
                    statement = connection.prepareStatement(
                            "INSERT INTO permission (id,permission_id,name,locale_code,access_right_id,workspace_id,active) VALUES (?, ?, ?, ?, ?, ?, ?)");
                    statement.setString(index++, UUID.randomUUID().toString());
                    statement.setString(index++, event.getPermission().getPermissionId());
                    statement.setString(index++, localizedFieldsVO.getName());
                    statement.setString(index++, code);
                    statement.setString(index++, event.getPermission().getAccessRightId());
                    statement.setString(index++, event.getPermission().getWorkspaceId());
                    statement.setBoolean(index, event.getPermission().isActive());
                    statement.execute();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
    }


    private void processPermissionUpdate(Connection connection, PermissionEvent.PermissionUpdated event) {
        event.getPermission().getLocalizedFieldsVOMap().forEach((code, localizedFieldsVO) -> {
                PreparedStatement statement;
                int index = 1;
                try {
                    statement = connection.prepareStatement(
                            "UPDATE permission SET name=?, access_right_id=?, workspace_id=?, active=? WHERE permission_id=? and locale_code=?");
                    statement.setString(index++, localizedFieldsVO.getName());
                    statement.setString(index++, event.getPermission().getAccessRightId());
                    statement.setString(index++, event.getPermission().getWorkspaceId());
                    statement.setBoolean(index++, event.getPermission().isActive());
                    statement.setString(index++, event.getPermission().getPermissionId());
                    statement.setString(index, code);
                    statement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        }

    private void processPermissionActivate(Connection connection, PermissionEvent.PermissionActivated event) throws SQLException {
        int index = 1;
        PreparedStatement statement = connection.prepareStatement(
                "UPDATE permission SET active=? WHERE permission_id=?");
        statement.setBoolean(index++, event.isValue());
        statement.setString(index, event.getPermissionId());
        statement.executeUpdate();
    }

    private void processPermissionDeactivate(Connection connection, PermissionEvent.PermissionDeactivated event) throws SQLException {
        int index = 1;
        PreparedStatement statement = connection.prepareStatement(
                "UPDATE permission SET active=? WHERE permission_id=?");
        statement.setBoolean(index++, event.isValue());
        statement.setString(index, event.getPermissionId());
        statement.executeUpdate();
    }

    @Override
    public PSequence<AggregateEventTag<PermissionEvent>> aggregateTags() {
        return PermissionEvent.TAG.allTags();
    }

}
