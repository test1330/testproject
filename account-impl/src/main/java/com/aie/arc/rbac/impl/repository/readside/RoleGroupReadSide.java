package com.aie.arc.rbac.impl.repository.readside;

import com.aie.arc.rbac.impl.event.RoleGroupEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.ReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcReadSide;
import org.pcollections.PSequence;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

public class RoleGroupReadSide extends ReadSideProcessor<RoleGroupEvent> {
    private final JdbcReadSide jdbcReadSide;

    @Inject
    public RoleGroupReadSide(JdbcReadSide jdbcReadSide) {
        this.jdbcReadSide = jdbcReadSide;
    }

    @Override
    public ReadSideProcessor.ReadSideHandler<RoleGroupEvent> buildHandler() {
        return jdbcReadSide.<RoleGroupEvent>builder("roleGroupOffset")
                .setEventHandler(RoleGroupEvent.RoleGroupCreated.class, this::processRoleGroupCreate)
                .setEventHandler(RoleGroupEvent.RoleGroupUpdated.class, this::processRoleGroupUpdated)
                .setEventHandler(RoleGroupEvent.RoleGroupActivated.class, this::processRoleGroupActivate)
                .setEventHandler(RoleGroupEvent.RoleGroupDeactivated.class, this::processRoleGroupDeactivate)
                .build();
    }




    private void processRoleGroupCreate(Connection connection, RoleGroupEvent.RoleGroupCreated event) {
        event.getRoleGroup().getLocalizedFieldsVOMap().forEach((code, localizedFieldsVO) -> {
            int index = 1;
            PreparedStatement statement;
            try {
                statement = connection.prepareStatement(
                        "INSERT INTO role_group (id,role_group_id,name,description,locale_code,active) VALUES (?, ?, ?, ? ,? ,?)");
                statement.setString(index++, UUID.randomUUID().toString());
                statement.setString(index++, event.getRoleGroup().getRoleGroupId());
                statement.setString(index++, localizedFieldsVO.getName());
                statement.setString(index++, localizedFieldsVO.getDescription());
                statement.setString(index++, code);
                statement.setBoolean(index, event.getRoleGroup().isActive());
                statement.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void processRoleGroupUpdated(Connection connection, RoleGroupEvent.RoleGroupUpdated event) {
        event.getRoleGroup().getLocalizedFieldsVOMap().forEach((code, localizedFieldsVO) -> {
            int index = 1;
            PreparedStatement statement;
            try {
                statement = connection.prepareStatement(
                        "UPDATE  role_group SET name=? ,description=?,active=? WHERE role_group_id=? and locale_code=?");
                statement.setString(index++, localizedFieldsVO.getName());
                statement.setString(index++, localizedFieldsVO.getDescription());
                statement.setBoolean(index++, event.getRoleGroup().isActive());
                statement.setString(index++, event.getRoleGroupId());
                statement.setString(index, code);
                statement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void processRoleGroupActivate(Connection connection, RoleGroupEvent.RoleGroupActivated event) throws SQLException {
        int index = 1;
        PreparedStatement statement = connection.prepareStatement(
                "UPDATE role_group SET active=? WHERE role_group_id=?");
        statement.setBoolean(index++, event.isValue());
        statement.setString(index, event.getRoleGroupId());
        statement.executeUpdate();
    }

    private void processRoleGroupDeactivate(Connection connection, RoleGroupEvent.RoleGroupDeactivated event) throws SQLException {
        int index = 1;
        PreparedStatement statement = connection.prepareStatement(
                "UPDATE role_group SET active=? WHERE role_group_id=?");
        statement.setBoolean(index++, event.isValue());
        statement.setString(index, event.getRoleGroupId());
        statement.executeUpdate();
    }

    @Override
    public PSequence<AggregateEventTag<RoleGroupEvent>> aggregateTags() {
        return RoleGroupEvent.TAG.allTags();
    }
}
