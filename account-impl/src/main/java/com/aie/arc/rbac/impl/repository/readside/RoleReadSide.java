package com.aie.arc.rbac.impl.repository.readside;

import com.aie.arc.rbac.impl.event.RoleEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.ReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcReadSide;
import org.pcollections.PSequence;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

/**
 * Event processor that handles {@link RoleEvent} events
 * by writing {@link } rows to the read-side database table.
 */
public class RoleReadSide extends ReadSideProcessor<RoleEvent> {
    private final JdbcReadSide jdbcReadSide;

    @Inject
    public RoleReadSide(JdbcReadSide jdbcReadSide) {
        this.jdbcReadSide = jdbcReadSide;
    }

    @Override
    public ReadSideProcessor.ReadSideHandler<RoleEvent> buildHandler() {
        return jdbcReadSide.<RoleEvent>builder("roleOffset")
                .setEventHandler(RoleEvent.RoleCreated.class, this::processRoleCreate)
                .setEventHandler(RoleEvent.RoleUpdated.class, this::processRoleUpdate)
                .setEventHandler(RoleEvent.RoleActivated.class, this::processRoleActivated)
                .setEventHandler(RoleEvent.RoleDeactivated.class, this::processRoleDeactivated)
                .build();
    }


    private void processRoleCreate(Connection connection, RoleEvent.RoleCreated event) throws SQLException {
        event.getRole().getLocalizedFieldsVOMap().forEach((code, localizedFieldsVO) -> {
            if(event.getRole().getRoleGroupId()!=null) {
                int index = 1;
                PreparedStatement statement;
                try {
                    statement = connection.prepareStatement(
                            "INSERT INTO role (id,role_id,role_group_id,name,locale_code,active) VALUES (?, ?, ?, ?, ?, ?)");
                    statement.setString(index++, UUID.randomUUID().toString());
                    statement.setString(index++, event.getRole().getRoleId());
                    statement.setString(index++, event.getRole().getRoleGroupId());
                    statement.setString(index++, localizedFieldsVO.getName());
                    statement.setString(index++, code);
                    statement.setBoolean(index, event.getRole().isActive());
                    statement.execute();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            else{
                int index = 1;
                PreparedStatement statement;
                try {
                    statement = connection.prepareStatement(
                            "INSERT INTO role (id,role_id,name,locale_code,active) VALUES (?, ?, ?, ?, ?)");
                    statement.setString(index++, UUID.randomUUID().toString());
                    statement.setString(index++, event.getRole().getRoleId());
//                    statement.setString(index++, event.getRole().getRoleGroupId());
                    statement.setString(index++, localizedFieldsVO.getName());
                    statement.setString(index++, code);
                    statement.setBoolean(index, event.getRole().isActive());
                    statement.execute();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }


        });
    }

    private void processRoleUpdate(Connection connection, RoleEvent.RoleUpdated event) throws SQLException {
        event.getRole().getLocalizedFieldsVOMap().forEach((code, localizedFieldsVO) -> {
            if(event.getRole().getRoleGroupId()!=null) {
                int index = 1;
                PreparedStatement statement;
                try {
                    statement = connection.prepareStatement(
                            "UPDATE role SET name=?,role_group_id=?,active=? WHERE role_id=? and locale_code=?");
                    statement.setString(index++, localizedFieldsVO.getName());
                    statement.setString(index++, event.getRole().getRoleGroupId());
                    statement.setBoolean(index++, event.getRole().isActive());
                    statement.setString(index++, event.getRole().getRoleId());
                    statement.setString(index, code);
                    statement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                int index = 1;
                PreparedStatement statement;
                try {
                    statement = connection.prepareStatement(
                            "UPDATE role SET name=?,active=? WHERE role_id=? and locale_code=?");
                    statement.setString(index++, localizedFieldsVO.getName());
//                    statement.setString(index++, event.getRole().getRoleGroupId());
                    statement.setBoolean(index++, event.getRole().isActive());
                    statement.setString(index++, event.getRole().getRoleId());
                    statement.setString(index, code);
                    statement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void processRoleActivated(Connection connection, RoleEvent.RoleActivated event) throws SQLException {
        int index = 1;
        PreparedStatement statement = connection.prepareStatement(
                "UPDATE role SET active=? WHERE role_id=?");
        statement.setBoolean(index++, event.isValue());
        statement.setString(index, event.getRoleId());
        statement.executeUpdate();
    }


    private void processRoleDeactivated(Connection connection, RoleEvent.RoleDeactivated event) throws SQLException {
        int index = 1;
        PreparedStatement statement = connection.prepareStatement(
                "UPDATE role SET active=? WHERE role_id=?");
        statement.setBoolean(index++, event.isValue());
        statement.setString(index, event.getRoleId());
        statement.executeUpdate();
    }

    @Override
    public PSequence<AggregateEventTag<RoleEvent>> aggregateTags() {
        return RoleEvent.TAG.allTags();
    }

}
