

package com.aie.arc.rbac.impl.repository.readside;


import com.aie.arc.rbac.impl.event.WorkspaceEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.ReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.jdbc.JdbcReadSide;
import org.pcollections.PSequence;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

public class WorkspaceReadSide extends ReadSideProcessor<WorkspaceEvent> {
    private final JdbcReadSide jdbcReadSide;

    @Inject
    public WorkspaceReadSide(JdbcReadSide jdbcReadSide) {
        this.jdbcReadSide = jdbcReadSide;
    }

    @Override
    public ReadSideHandler<WorkspaceEvent> buildHandler() {
        return jdbcReadSide.<WorkspaceEvent>builder("workspaceOffset")
                .setEventHandler(WorkspaceEvent.WorkspaceCreated.class, this::processWorkspaceCreate)
                .setEventHandler(WorkspaceEvent.WorkspaceUpdated.class, this::processWorkspaceUpdate)
                .setEventHandler(WorkspaceEvent.WorkspaceDeactivated.class, this::processWorkspaceDeactivate)
                .setEventHandler(WorkspaceEvent.WorkspaceActivated.class, this::processWorkspaceActivate)
                .build();
    }

    @Override
    public PSequence<AggregateEventTag<WorkspaceEvent>> aggregateTags() {
        return WorkspaceEvent.TAG.allTags();
    }


    private void processWorkspaceCreate(Connection connection, WorkspaceEvent.WorkspaceCreated event) {
        event.getWorkspace().getLocalizedFieldsVOMap().forEach((code, localizedFieldsVO) -> {

            int index = 1;
            PreparedStatement statement;
            try {
                statement = connection.prepareStatement(
                        "INSERT INTO workspace (id,workspace_id,name,locale_code,module,permission_type,active) VALUES (?, ?, ?, ?, ?, ?, ?)");
                statement.setString(index++, UUID.randomUUID().toString());
                statement.setString(index++, event.getWorkspace().getWorkspaceId());
                statement.setString(index++, localizedFieldsVO.getName());
                statement.setString(index++, code);
                statement.setString(index++,event.getWorkspace().getModule());
                statement.setString(index++, event.getWorkspace().getPermissionType().toString());
                statement.setBoolean(index, event.getWorkspace().isActive());
                statement.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

    }


    private void processWorkspaceUpdate(Connection connection, WorkspaceEvent.WorkspaceUpdated event) {
        event.getWorkspace().getLocalizedFieldsVOMap().forEach((code, localizedFieldsVO) -> {

            PreparedStatement statement;
            int index = 1;
            try {
                statement = connection.prepareStatement(
                        "UPDATE workspace SET name=?, module=?, permission_type=?,active=? WHERE workspace_id=? and locale_code=?");
                statement.setString(index++, localizedFieldsVO.getName());
                statement.setString(index++,event.getWorkspace().getModule());
                statement.setString(index++, event.getWorkspace().getPermissionType().toString());
                statement.setBoolean(index++, event.getWorkspace().isActive());
                statement.setString(index++, event.getWorkspaceId());
                statement.setString(index, code);
                statement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });


    }


    private void processWorkspaceDeactivate(Connection connection, WorkspaceEvent.WorkspaceDeactivated event) throws SQLException {
        int index = 1;
        PreparedStatement statement = connection.prepareStatement(
                "UPDATE workspace SET active=? WHERE workspace_id=?");
        statement.setBoolean(index++, event.isActive());
        statement.setString(index, event.getWorkspaceId());
        statement.executeUpdate();
    }

    private void processWorkspaceActivate(Connection connection, WorkspaceEvent.WorkspaceActivated event) throws SQLException {
        int index = 1;
        PreparedStatement statement = connection.prepareStatement(
                "UPDATE workspace SET active=? WHERE workspace_id=?");
        statement.setBoolean(index++, event.isActive());
        statement.setString(index, event.getWorkspaceId());
        statement.executeUpdate();
    }
}





