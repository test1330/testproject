package com.aie.arc.rbac.impl.service;

import com.aie.arc.account.impl.util.ServiceConstants;
import com.aie.arc.rbac.api.publishedevent.*;
import com.aie.arc.rbac.api.request.dto.LocalizedFieldsRequest;
import com.aie.arc.rbac.impl.command.*;
import com.aie.arc.rbac.impl.entity.entities.*;
import com.aie.arc.rbac.impl.event.*;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static com.aie.arc.account.impl.util.MessageConstants.CONVERTING_NON_PUB_EVENT;


@Singleton
@Slf4j
public class EventPublisher {

    @Inject
    private RBACServiceImplCommon serviceImplCommon;

    /*Publish Role Group Events*/
    public CompletionStage<RoleGroupEvents> convertPublishedEventForRoleGroup(RoleGroupEvent event) {
        if (event instanceof RoleGroupEvent.RoleGroupCreated) {
            return getRoleGroup(event)
                    .thenApply(item -> {
                        RoleGroup roleGroup = item.get();
                        return new RoleGroupEvents.RoleGroupCreated(roleGroup.getRoleGroupId(), mapLocalizeFieldVoToLocalizeRequest(roleGroup.getLocalizedFieldsVOMap()), roleGroup.isActive(), LocalDateTime.now());
                    });
        }

        if (event instanceof RoleGroupEvent.RoleGroupUpdated)
            return getRoleGroup(event)
                    .thenApply(item -> {
                        RoleGroup roleGroup = item.get();
                        return new RoleGroupEvents.RoleGroupUpdated(roleGroup.getRoleGroupId(), mapLocalizeFieldVoToLocalizeRequest(roleGroup.getLocalizedFieldsVOMap()), roleGroup.isActive(), LocalDateTime.now());
                    });
        if (event instanceof RoleGroupEvent.RoleGroupActivated)
            return getRoleGroup(event)
                    .thenApply(item -> {
                        RoleGroup roleGroup = item.get();
                        return new RoleGroupEvents.RoleGroupActivated(roleGroup.getRoleGroupId(), roleGroup.isActive(), LocalDateTime.now());
                    });
        if (event instanceof RoleGroupEvent.RoleGroupDeactivated)
            return getRoleGroup(event)
                    .thenApply(item -> {
                        RoleGroup roleGroup = item.get();
                        return new RoleGroupEvents.RoleGroupDeactivated(roleGroup.getRoleGroupId(), roleGroup.isActive(), LocalDateTime.now());
                    });

        throw new IllegalArgumentException(CONVERTING_NON_PUB_EVENT);
    }


    /*Publish Role Events*/
    public CompletionStage<RoleEvents> convertPublishedEventForRole(RoleEvent event) {
        if (event instanceof RoleEvent.RoleCreated) {
            log.debug("Publishing RoleCreated event :{}", event.getRoleId());
            return getRole(event)
                    .thenApply(item -> {
                        Role role = item.get();
                        return new RoleEvents.RoleCreated(role.getRoleId(), role.getRoleGroupId(), role.getPermissionIds(), mapLocalizeFieldVoToLocalizeRequest(role.getLocalizedFieldsVOMap()), role.isActive(), LocalDateTime.now().toString());
                    });
        }

        if (event instanceof RoleEvent.RoleUpdated){
            log.debug("Publishing RoleUpdated event :{}", event.getRoleId());
            return getRole(event)
                    .thenApply(item -> {
                        Role role = item.get();
                        return new RoleEvents.RoleUpdated(role.getRoleId(), role.getRoleGroupId(), role.getPermissionIds(), mapLocalizeFieldVoToLocalizeRequest(role.getLocalizedFieldsVOMap()), role.isActive(), LocalDateTime.now().toString());
                    });
        }
        if (event instanceof RoleEvent.RoleActivated){
            log.debug("Publishing RoleActivated event :{}", event.getRoleId());
            return getRole(event)
                    .thenApply(item -> {
                        Role role = item.get();
                        return new RoleEvents.RoleActivated(role.getRoleId(), role.getRoleGroupId(), role.getPermissionIds(), mapLocalizeFieldVoToLocalizeRequest(role.getLocalizedFieldsVOMap()), role.isActive(), LocalDateTime.now().toString());
                    });
        }

        if (event instanceof RoleEvent.RoleDeactivated){
            log.debug("Publishing RoleDeactivated event :{}", event.getRoleId());
              return getRole(event)
                    .thenApply(item -> {
                        Role role = item.get();
                        return new RoleEvents.RoleDeactivated(role.getRoleId(), role.getRoleGroupId(), role.getPermissionIds(), mapLocalizeFieldVoToLocalizeRequest(role.getLocalizedFieldsVOMap()), role.isActive(), LocalDateTime.now().toString());
                    });
        }

        throw new IllegalArgumentException(CONVERTING_NON_PUB_EVENT);
    }

    /*Publish Role Events*/
    public CompletionStage<MicroServiceEvents> convertPublishedEventForMicroService(MicroServiceEvent event) {
        if (event instanceof MicroServiceEvent.MicroServiceCreated) {
            return getMicroService(event)
                    .thenApply(item -> {
                        MicroService microService = item.get();
                        return new MicroServiceEvents.MicroServiceCreated(microService.getMicroServiceId(), mapLocalizeFieldVoToLocalizeRequest(microService.getLocalizedFieldsVOMap()), microService.getApiIds(), microService.isActive(), LocalDateTime.now());
                    });
        }

        if (event instanceof MicroServiceEvent.MicroServiceUpdated)
            return getMicroService(event)
                    .thenApply(item -> {
                        MicroService microService = item.get();
                        return new MicroServiceEvents.MicroServiceCreated(microService.getMicroServiceId(), mapLocalizeFieldVoToLocalizeRequest(microService.getLocalizedFieldsVOMap()), microService.getApiIds(), microService.isActive(), LocalDateTime.now());
                    });
        if (event instanceof MicroServiceEvent.MicroServiceActivated)
            return getMicroService(event)
                    .thenApply(item -> {
                        MicroService microService = item.get();
                        return new MicroServiceEvents.MicroServiceActivated(microService.getMicroServiceId(), microService.isActive(), LocalDateTime.now());
                    });
        if (event instanceof MicroServiceEvent.MicroServiceDeactivated)
            return getMicroService(event)
                    .thenApply(item -> {
                        MicroService microService = item.get();
                        return new MicroServiceEvents.MicroServiceDeactivated(microService.getMicroServiceId(), microService.isActive(), LocalDateTime.now());
                    });
        throw new IllegalArgumentException(CONVERTING_NON_PUB_EVENT);
    }


    /*Publish api Events*/
    public CompletionStage<ApiEvents> convertPublishedEventForApi(ApiEvent event) {
        if (event instanceof ApiEvent.ApiAdded) {
            return getApi(event)
                    .thenApply(item -> {
                        Api api = item.get();
                        return new ApiEvents.ApiCreated(api.getId(), mapLocalizeFieldVoToLocalizeRequest(api.getLocalizedFieldsVOMap()), api.isActive(), LocalDateTime.now());
                    });
        }
        if (event instanceof ApiEvent.ApiUpdated)
            return getApi(event)
                    .thenApply(item -> {
                        Api api = item.get();
                        return new ApiEvents.ApiUpdated(api.getId(), mapLocalizeFieldVoToLocalizeRequest(api.getLocalizedFieldsVOMap()), api.isActive(), LocalDateTime.now());
                    });
        if (event instanceof ApiEvent.ApiActivated)
            return getApi(event)
                    .thenApply(item -> {
                        Api api = item.get();
                        return new ApiEvents.ApiActivated(api.getId(), api.isActive(), LocalDateTime.now());
                    });
        if (event instanceof ApiEvent.ApiDeactivated)
            return getApi(event)
                    .thenApply(item -> {
                        Api api = item.get();
                        return new ApiEvents.ApiDeactivated(api.getId(), api.isActive(), LocalDateTime.now());
                    });
        throw new IllegalArgumentException(CONVERTING_NON_PUB_EVENT);
    }


    /*Publish api Events*/
    public CompletionStage<PermissionEvents> convertPublishedEventForPermission(PermissionEvent event) {
        if (event instanceof PermissionEvent.PermissionCreated) {
            log.debug("Publishing PermissionCreated event :{}", event.getPermissionId());
            return getPermission(event)
                    .thenApply(item -> {
                        Permission permission = item.get();
                        return new PermissionEvents.PermissionCreated(permission.getPermissionId(), mapLocalizeFieldVoToLocalizeRequest(permission.getLocalizedFieldsVOMap()), permission.getApiTags(), permission.isActive(), LocalDateTime.now().toString());
                    });
        }
        if (event instanceof PermissionEvent.PermissionUpdated){
            log.debug("Publishing PermissionUpdated event :{}", event.getPermissionId());
            return getPermission(event)
                    .thenApply(item -> {
                        Permission permission = item.get();
                        return new PermissionEvents.PermissionUpdated(permission.getPermissionId(), mapLocalizeFieldVoToLocalizeRequest(permission.getLocalizedFieldsVOMap()), permission.getApiTags(), permission.isActive(), LocalDateTime.now().toString());
                    });
        }

        if (event instanceof PermissionEvent.PermissionActivated){
            log.debug("Publishing PermissionActivated event :{}", event.getPermissionId());
            return getPermission(event)
                    .thenApply(item -> {
                        Permission permission = item.get();
                        return new PermissionEvents.PermissionActivated(permission.getPermissionId(), mapLocalizeFieldVoToLocalizeRequest(permission.getLocalizedFieldsVOMap()), permission.getApiTags(), permission.isActive(), LocalDateTime.now().toString());
                    });
        }

        if (event instanceof PermissionEvent.PermissionDeactivated){
            log.debug("Publishing PermissionDeactivated event :{}", event.getPermissionId());
            return getPermission(event)
                    .thenApply(item -> {
                        Permission permission = item.get();
                        return new PermissionEvents.PermissionDeactivated(permission.getPermissionId(), mapLocalizeFieldVoToLocalizeRequest(permission.getLocalizedFieldsVOMap()), permission.getApiTags(), permission.isActive(), LocalDateTime.now().toString());
                    });
        }

        throw new IllegalArgumentException(CONVERTING_NON_PUB_EVENT);
    }

    private CompletionStage<Optional<RoleGroup>> getRoleGroup(RoleGroupEvent event) {
        return serviceImplCommon.getEntityRefForRoleGroup(event.getRoleGroupId())
                .ask(RoleGroupCommand.GetRoleGroup::new,ServiceConstants.ASK_TIMEOUT);
    }

    private CompletionStage<Optional<Role>> getRole(RoleEvent event) {
        return serviceImplCommon.getEntityRefForRole(event.getRoleId())
                .ask(RoleCommand.GetRole::new,ServiceConstants.ASK_TIMEOUT);
    }

    private CompletionStage<Optional<MicroService>> getMicroService(MicroServiceEvent event) {
        return serviceImplCommon.getEntityRefForMicroService(event.getMicroServiceId())
                .ask(MicroServiceCommand.GetMicroService::new, ServiceConstants.ASK_TIMEOUT);
    }

    private CompletionStage<Optional<Api>> getApi(ApiEvent event) {
        return serviceImplCommon.getEntityRefForApi(event.getApiId())
                .ask(ApiCommand.GetAPI::new, ServiceConstants.ASK_TIMEOUT);
    }


    private CompletionStage<Optional<Permission>> getPermission(PermissionEvent event) {
        return serviceImplCommon.getEntityRefForPermission(event.getPermissionId())
                .ask(PermissionCommand.GetPermission::new,ServiceConstants.ASK_TIMEOUT);
    }

    private Map<String, LocalizedFieldsRequest> mapLocalizeFieldVoToLocalizeRequest(Map<String, RBACLocalizedFieldsVO> requestMap) {
        Map<String, LocalizedFieldsRequest> localizedFieldsVOMap = new HashMap<>();
        requestMap.forEach((code, localisedFields) -> {
            localizedFieldsVOMap.put(code, new LocalizedFieldsRequest(localisedFields.getName(), localisedFields.getDescription()));
        });
        return localizedFieldsVOMap;
    }
}
