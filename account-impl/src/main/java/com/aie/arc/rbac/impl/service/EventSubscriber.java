package com.aie.arc.rbac.impl.service;

import akka.Done;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.aie.arc.master.api.publishedevent.LocaleEvents;
import com.aie.arc.organization.api.common.request.LocalizedFieldsRequest;
import com.aie.arc.tool.common.status.Status;
import com.aie.arc.tool.common.vo.LocalizedFieldsTools;
import com.aie.arc.tool.master.subscribe.command.LocaleCommand;
import com.aie.arc.tool.master.subscribe.entities.Locale;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Slf4j
@Singleton
public class EventSubscriber {

    private final RBACServiceImplCommon rbacServiceImplCommon;
    private final RBACServiceRequestValidation validationObj;

    @Inject
    public EventSubscriber(RBACServiceImplCommon rbacServiceImplCommon, RBACServiceRequestValidation validationObj) {
        this.rbacServiceImplCommon = rbacServiceImplCommon;
        this.validationObj = validationObj;
    }
    /*Subscribing localize event...
     *
     * */
    public CompletionStage<Done> handleLocaleEvents(LocaleEvents localeEvent) {
        log.info("Locale event received :: ");
        LocaleEvents.LocaleCreated create = null;
        LocalDateTime previousTimestamp = null, currentTimestamp = null;

        if (localeEvent instanceof LocaleEvents.LocaleCreated) {
            create = (LocaleEvents.LocaleCreated) localeEvent;
            Optional<Locale> subscribedLocale = rbacServiceImplCommon.getEntityRefForLocale(localeEvent.getLocaleId()).ask(LocaleCommand.GetLocale::new, ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join();

            if (!subscribedLocale.isPresent()) {
                // create Country and persist it..
                Locale createLocale = new Locale(create.getLocaleId(), "", create.getCode(),
                        toLocalisedFieldVoMap(create.getLocalization()), create.isActive(), create.getCreatedAt(), create.getLastModifiedAt(), Status.CREATED);
                return rbacServiceImplCommon.getEntityRefForLocale(create.getLocaleId()).ask(replyTo -> new LocaleCommand.CreateLocale(createLocale, replyTo), ServiceConstants.ASK_TIMEOUT);
            }
        }
        if (localeEvent instanceof LocaleEvents.LocaleUpdated) {
            LocaleEvents.LocaleUpdated update = (LocaleEvents.LocaleUpdated) localeEvent;
            // currentTimestamp = update.getTimestamp();
            // Locale updateLocale = common.getEntityRefForLocale(update.getLocaleId()).ask(LocaleCommand.GetLocale.INSTANCE).toCompletableFuture().join().get();
            // previousTimestamp = updateLocale.getLastModifiedAt();
            //if (currentTimestamp.isAfter(previousTimestamp)) {
            Locale updatedLocale = new Locale(update.getLocaleId(), update.getCountryId(), update.getCode(), toLocalisedFieldVoMap(update.getLocalization()),
                    update.isActive(), update.getCreatedAt(), update.getLastModifiedAt(), Status.CREATED);
            return rbacServiceImplCommon.getEntityRefForLocale(update.getLocaleId()).ask(replyTo -> new LocaleCommand.UpdateLocale(updatedLocale, replyTo), ServiceConstants.ASK_TIMEOUT);
            //}
        }
        return CompletableFuture.completedFuture(Done.getInstance());
    }

    private Map<String, LocalizedFieldsTools> toLocalisedFieldVoMap(Map<String, LocalizedFieldsRequest> localizedFields) {
        Map<String, LocalizedFieldsTools> localisedFieldsMap = new HashMap<>();
        localizedFields.forEach(((code, localised) -> localisedFieldsMap.put(code, new LocalizedFieldsTools(localised.getName(), localised.getDescription()))));
        return localisedFieldsMap;
    }
}
