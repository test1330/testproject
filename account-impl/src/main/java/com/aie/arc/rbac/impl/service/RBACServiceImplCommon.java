package com.aie.arc.rbac.impl.service;

import akka.cluster.sharding.typed.javadsl.ClusterSharding;
import akka.cluster.sharding.typed.javadsl.EntityRef;
import akka.japi.Pair;
import com.aie.arc.account.api.common.response.BaseResponse;
import com.aie.arc.account.impl.entity.master.subscribe.entity.LocaleEntity;
import com.aie.arc.rbac.impl.command.*;
import com.aie.arc.rbac.impl.entity.*;
import com.aie.arc.rbac.impl.util.LogUtil;
import com.aie.arc.tool.master.subscribe.command.LocaleCommand;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRef;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.UUID;

import static com.aie.arc.rbac.impl.util.MessageConstants.LINE_SEPARATOR;
import static com.aie.arc.rbac.impl.util.MessageConstants.LOG_RESPONSE;

@Singleton
@Slf4j
public class RBACServiceImplCommon {
    @Inject
    public PersistentEntityRegistry registry;

    @Inject
    public ClusterSharding clusterSharding;

    /**
     * Obtain a reference to the PersistentEntity with the specified id.
     *
     * @param id a unique identifier for Role.
     * @return a PersistentEntityRef for Role.
     */
    public EntityRef<RoleCommand> getEntityRefForRole(String id) {
        return clusterSharding.entityRefFor(RoleEntity.ENTITY_TYPE_KEY,id);
    }

    /**
     * Obtain a reference to the PersistentEntity with the specified id.
     *
     * @param id a unique identifier for RoleGroup.
     * @return a PersistentEntityRef for RoleGroup.
     */
    public EntityRef<RoleGroupCommand> getEntityRefForRoleGroup(String id) {
        return clusterSharding.entityRefFor(RoleGroupEntity.ENTITY_TYPE_KEY,id);
    }

    /**
     * Obtain a reference to the PersistentEntity with the specified id.
     *
     * @param id a unique identifier for MicroService.
     * @return a PersistentEntityRef for MicroService.
     */
    public EntityRef<MicroServiceCommand> getEntityRefForMicroService(String id) {
        return clusterSharding.entityRefFor(MicroServiceEntity.ENTITY_TYPE_KEY,id);
    }

    /**
     * Obtain a reference to the PersistentEntity with the specified id.
     *
     * @param id a unique identifier for Api.
     * @return a PersistentEntityRef for Api.
     */
    public EntityRef<ApiCommand> getEntityRefForApi(String id) {
        return clusterSharding.entityRefFor(ApiEntity.ENTITY_TYPE_KEY, id);
    }

    /**
     * Obtain a reference to the PersistentEntity with the specified id.
     *
     * @param id a unique identifier for Permission.
     * @return a PersistentEntityRef for Permission.
     */
    public EntityRef<PermissionCommand> getEntityRefForPermission(String id) {
        return clusterSharding.entityRefFor(PermissionEntity.ENTITY_TYPE_KEY, id);
    }
    public EntityRef<WorkspaceCommand> getEntityRefForWorkspace(String id) {
        return clusterSharding.entityRefFor(WorkspaceEntity.ENTITY_TYPE_KEY, id);
    }
    /**
     * Obtain a reference to the PersistentEntity with the specified customerGroupId.
     *
     * @param id a unique identifier for Country.
     * @return a PersistentEntityRef for Country.
     */
    public EntityRef<LocaleCommand> getEntityRefForLocale(String id) {
        return clusterSharding.entityRefFor(LocaleEntity.ENTITY_TYPE_KEY, id);
    }

    /**
     * Obtain a reference to the PersistentEntity with the specified id.
     *
     * @param id a unique identifier for AccessRight.
     * @return a PersistentEntityRef for AccessRight.
     */
    public EntityRef<AccessRightCommand> getEntityRefForAccessRight(String id) {
        return clusterSharding.entityRefFor(AccessRightEntity.ENTITY_TYPE_KEY, id);
    }

    /**
     * Obtain a reference to the PersistentEntity with the specified id.
     *
     * @return a PersistentEntityRef for User.
     */
    public Pair<ResponseHeader, BaseResponse> sendResponseByStatusAndRequest(Integer STATUS, String URI, String MESSAGE, Object request) {
        // creating response object here
        BaseResponse response = BaseResponse.getResponseByStatusAndMessage(STATUS, MESSAGE);
        // Storing logs
        LogUtil.log(new StringBuilder(LOG_RESPONSE).append(URI).append(request).append("  ").append(response).append(LINE_SEPARATOR).toString());
        return Pair.create(ResponseHeader.OK.withStatus(STATUS), response);
    }

    public Pair<ResponseHeader, BaseResponse> sendResponseByStatusIdAndRequest(String Id,Integer STATUS, String URI, String MESSAGE, Object request) {
        // creating response object here
        BaseResponse response = BaseResponse.getResponseByStatusAndMessage(Id,STATUS, MESSAGE);
        // Storing logs
        LogUtil.log(new StringBuilder(LOG_RESPONSE).append(URI).append(request).append("  ").append(response).append(LINE_SEPARATOR).toString());
        return Pair.create(ResponseHeader.OK.withStatus(STATUS), response);
    }

    /**
     * To obtain a random UUID.
     *
     * @return a random UUID in String format.
     */
    public String getNewId() {
        return UUID.randomUUID().toString();
    }

}