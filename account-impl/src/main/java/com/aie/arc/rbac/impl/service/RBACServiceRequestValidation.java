package com.aie.arc.rbac.impl.service;

import akka.Done;
import akka.japi.Pair;
import com.aie.arc.account.api.common.request.ActivationRequest;
import com.aie.arc.account.api.common.request.BaseRequest;
import com.aie.arc.account.api.common.request.LocalisedFields;
import com.aie.arc.account.api.common.response.BaseResponse;
import com.aie.arc.account.impl.repository.repository.LocaleRepository;
import com.aie.arc.account.impl.util.LogUtil;
import com.aie.arc.account.impl.util.ServiceConstants;
import com.aie.arc.rbac.api.constants.RBACURIConstants;
import com.aie.arc.rbac.impl.command.*;
import com.aie.arc.rbac.impl.entity.entities.*;
import com.aie.arc.rbac.impl.exceptions.ParameterRequired;
import com.aie.arc.rbac.impl.repository.CommonRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lightbend.lagom.javadsl.api.transport.NotFound;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.*;
import java.util.concurrent.CompletionStage;

import static com.aie.arc.account.impl.util.MessageConstants.*;

@Singleton
@Slf4j
public class RBACServiceRequestValidation {

    @Inject
    private PersistentEntityRegistry registry;

    public final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Inject
    private RBACServiceImplCommon serviceImplCommon;

    public void validateJsonRequest(BaseRequest request) {
        Set<ConstraintViolation<BaseRequest>> violations = validator.validate(request);
        if (violations.size() > 0) {
            throw new ParameterRequired(violations.stream().map(x -> (x.getPropertyPath() + ":" + x.getMessage())).toArray(String[]::new));
        }
    }

    /*validating MicroService...*/
    public void validateMicroServiceIds(Set<String> microServiceIds) {
        microServiceIds.forEach(this::validateMicroServiceId);
    }

    /*validating Api ...*/
    public void validateMicroServiceId(String microServiceId) {
        if (!serviceImplCommon.getEntityRefForMicroService(microServiceId).ask(MicroServiceCommand.GetMicroService::new,ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().isPresent())
            throw new ParameterRequired(new String[]{"microServiceId: " + "MicroService Id(" + microServiceId + ") not valid"});
    }

    /*validating Apis...*/
    public void validateApiIds(List<String> apiIds) {
        apiIds.forEach(this::validateApiId);
    }

    /*validating Api ...*/
    public void validateApiId(String apiId) {
        if (!serviceImplCommon.getEntityRefForApi(apiId).ask(ApiCommand.GetAPI::new, ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().isPresent())
            throw new ParameterRequired(new String[]{"apiId: " + "Api id(" + apiId + ") is not valid"});
    }

    /*validating Apis...*/
    public void validateRoleIds(List<String> roleIds) {
        roleIds.forEach(this::validateRoleId);
    }

    /*validating Api ...*/
    public void validateRoleId(String roleId) {
        if (!serviceImplCommon.getEntityRefForRole(roleId).ask(RoleCommand.GetRole::new,ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().isPresent())
            throw new ParameterRequired(new String[]{"roleId: " + "Role id(" + roleId + ") is not valid"});
    }

    /*validating Api ...*/
    public Api validateAndGetApiById(String apiId) {
        return serviceImplCommon.getEntityRefForApi(apiId).ask(ApiCommand.GetAPI::new,ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().orElseThrow(() -> new ParameterRequired(new String[]{"apiId: " + "Api id(" + apiId + ") is not valid"}));
    }

    /**
     * Get an Microservice object using the specified entity id.
     *
     * @param microServiceId a unique identity for the MicroService whose object is to be returned.
     * @return object of MicroService having the specified id if present, throws a NotFound exception if
     * MicroService with specified id does not exist.
     */
    public MicroService validateAndGetMicroServiceById(String microServiceId) {
        return serviceImplCommon.getEntityRefForMicroService(microServiceId).ask(MicroServiceCommand.GetMicroService::new,ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().orElseThrow(() -> new ParameterRequired(new String[]{"microServiceId: " + "MicroService id(" + microServiceId + ") is not valid"}));
    }

    /**
     * Get an Role object using the specified entity id.
     *
     * @param roleId a unique identity for the Role whose object is to be returned.
     * @return object of Role having the specified id if present, throws a NotFound exception if
     * Role with specified id does not exist.
     */
    public Role validateAndGetRoleById(String roleId) {
        return serviceImplCommon.getEntityRefForRole(roleId).ask(RoleCommand.GetRole::new,ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().orElseThrow(() -> new ParameterRequired(new String[]{"roleId: " + "Role id(" + roleId + ") is not valid"}));
    }

    public RoleGroup validateAndGetRoleGroupById(String roleGroupId) {
        return serviceImplCommon.getEntityRefForRoleGroup(roleGroupId).ask(RoleGroupCommand.GetRoleGroup::new,ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().orElseThrow(() -> new ParameterRequired(new String[]{"roleGroupId: " + "RoleGroupId id(" + roleGroupId + ") is not valid"}));
    }

    public void validateRoleGroupId(String roleGroupId) {
        if (!serviceImplCommon.getEntityRefForRoleGroup(roleGroupId).ask(RoleGroupCommand.GetRoleGroup::new,ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().isPresent())
            throw new ParameterRequired(new String[]{"roleGroupId: " + "RoleGroup id(" + roleGroupId + ") is not valid"});
    }

    /*validating RoleGroups...*/
    public void validateRoleGroupIds(List<String> roleGroupIds) {
        roleGroupIds.forEach(this::validateRoleGroupId);
    }

    public void validatePermissionIds(List<String> permissionIds) {
        permissionIds.forEach(this::validatePermissionById);
    }

    /*validating MicroService...*/
    public void validatePermissionByIds(List<String> permissionIds) {
        permissionIds.forEach(this::validatePermissionById);
    }

    public void validatePermissionById(String permissionId) {
        if (!serviceImplCommon.getEntityRefForPermission(permissionId).ask(PermissionCommand.GetPermission::new,ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().isPresent())
            throw new ParameterRequired(new String[]{"permissionId: " + "Permission id(" + permissionId + ") is not valid"});
    }
    public Permission validateAndGetPermissionById(String permissionId) {
        return serviceImplCommon.getEntityRefForPermission(permissionId).ask(PermissionCommand.GetPermission::new,ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().orElseThrow(() -> new ParameterRequired(new String[]{"permissionId: " + "Permission id(" + permissionId + ") is not valid"}));
    }

    public void validateAccessRightId(String accessId) {
        log.debug("Validate accessRight_Id : {}", accessId);
        if (!serviceImplCommon.getEntityRefForAccessRight(accessId).ask(AccessRightCommand.GetAccessRight::new,ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().isPresent())
            throw new ParameterRequired(new String[]{"accessRightId: " + "AccessRight id(" + accessId + ") is not valid"});
    }

    public AccessRight validateAndGetAccessRightById(String accessId) {
        log.debug("Validate accessRight_Id : {}", accessId);
        return serviceImplCommon.getEntityRefForAccessRight(accessId).ask(AccessRightCommand.GetAccessRight::new,ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().orElseThrow(() -> new ParameterRequired(new String[]{"accessRightId: " + "Access id(" + accessId + ") is not valid"}));
    }

    public void validateAccessRightIds(List<String> accessRightIds) {
        accessRightIds.forEach(this::validateAccessRightId);
    }

    public Map<String, RBACLocalizedFieldsVO> validateLocaleCodesAndGetVoMap(Map<String, LocalisedFields> requestMap,
                                                                             LocaleRepository localeRepository) {
        Map<String, RBACLocalizedFieldsVO> localizedFieldsVOMap = new HashMap<>();
        StringJoiner localCodes = new StringJoiner(",");

        requestMap.forEach((code, localisedFields) -> {
            localCodes.add("'" + code + "'");
            localizedFieldsVOMap.put(code, new RBACLocalizedFieldsVO(localisedFields.getName(),
                    localisedFields.getDescription()));
        });
        localeRepository.validateAllLocaleCodes(localCodes)
                .thenAccept(count -> {
                    if (count != localizedFieldsVOMap.size())
                        throw new ParameterRequired(new String[]{"given codes: (" + localCodes.toString() +
                                ") are not valid"});
                });
        return localizedFieldsVOMap;
    }

    /* Role Group activate or deactivate*/
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndActivateRoleGroup(String id,
                                                                                            ActivationRequest request) {
        RoleGroup roleGroup = validateAndGetRoleGroupById(id);
        if (roleGroup.isActive()) {
            throw new ParameterRequired(new String[]{"Role Group '" + id + "' is already active."});
        }
        return serviceImplCommon.getEntityRefForRoleGroup(id).<Done>ask(replyTo ->
                new RoleGroupCommand.ActivateRoleGroup(id, true,replyTo),ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS,
                        RBACURIConstants.ACTIVE_ROLE_GROUP_URI, RBACURIConstants.ROLE_GROUP_ACTIVATE_MESSAGE, request));
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndDeactivateRoleGroup(
            String id, ActivationRequest request) {
        RoleGroup roleGroup = validateAndGetRoleGroupById(id);
        if (!roleGroup.isActive()) {
            throw new ParameterRequired(new String[]{"Role Group '" + id + "' is already not active."});
        }
        return serviceImplCommon.getEntityRefForRoleGroup(id).<Done>ask(replyTo ->
                new RoleGroupCommand.DeactivateRoleGroup(id, false,replyTo),ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS, RBACURIConstants.
                        ACTIVE_ROLE_GROUP_URI, RBACURIConstants.ROLE_GROUP_DEACTIVATE_MESSAGE, request));
    }

    /* Role activate or deactivate*/
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndActivateRole(
            String id, ActivationRequest request) {
        Role role = validateAndGetRoleById(id);
        if (role.isActive()) {
            throw new ParameterRequired(new String[]{"Role '" + id + "' is already active."});
        }
        return serviceImplCommon.getEntityRefForRole(id).<Done>ask(replyTo ->
                new RoleCommand.ActivateRole(id, true,replyTo),ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS,
                        RBACURIConstants.ACTIVE_ROLE_URI, RBACURIConstants.ROLE_ACTIVATE_MESSAGE, request));
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndDeactivateRole(
            String id, ActivationRequest request) {
        Role role = validateAndGetRoleById(id);
        if (!role.isActive()) {
            throw new ParameterRequired(new String[]{"Role '" + id + "' is already not active."});
        }
        return serviceImplCommon.getEntityRefForRole(id).<Done>ask(replyTo ->
                new RoleCommand.DeactivateRole(id, false,replyTo),ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS,
                        RBACURIConstants.ACTIVE_ROLE_URI, RBACURIConstants.ROLE_DEACTIVATE_MESSAGE, request));
    }

    /* MicroService activate or deactivate*/
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndActivateMicroService(
            String id, ActivationRequest request) {
        MicroService microService = validateAndGetMicroServiceById(id);
        if (microService.isActive()) {
            throw new ParameterRequired(new String[]{"MicroService '" + id + "' is already active."});
        }
        return serviceImplCommon.getEntityRefForMicroService(id).<Done>ask(replyTo ->
                new MicroServiceCommand.ActivateMicroService(id, true,replyTo),ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS,
                        RBACURIConstants.ACTIVE_ROLE_URI, RBACURIConstants.MICRO_SERVICE_ACTIVATE_MESSAGE, request));
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndDeactivateMicroService(
            String id, ActivationRequest request) {
        MicroService microService = validateAndGetMicroServiceById(id);
        if (!microService.isActive()) {
            throw new ParameterRequired(new String[]{"MicroService '" + id + "' is already deactivate."});
        }
        return serviceImplCommon.getEntityRefForMicroService(id).<Done>ask(replyTo ->
                new MicroServiceCommand.DeactivateMicroService(id, false,replyTo),ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS,
                        RBACURIConstants.ACTIVE_ROLE_URI, RBACURIConstants.MICRO_SERVICE_DEACTIVATE_MESSAGE, request));
    }

    /* Api activate or deactivate*/
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndActivateApi(String id, ActivationRequest request) {
        Api api = validateAndGetApiById(id);
        if (api.isActive()) {
            throw new ParameterRequired(new String[]{"API '" + id + "' is already active."});
        }
        return serviceImplCommon.getEntityRefForApi(id).<Done>ask(replyTo -> new ApiCommand.ActivateAPI(id, true,replyTo),ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS, RBACURIConstants.ACTIVE_API_URI, RBACURIConstants.API_ACTIVATE_MESSAGE, request));
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndDeactivateApi(String id, ActivationRequest request) {
        Api api = validateAndGetApiById(id);
        if (!api.isActive()) {
            throw new ParameterRequired(new String[]{"API '" + id + "' is already deactivate."});
        }
        return serviceImplCommon.getEntityRefForApi(id).<Done>ask(replyTo -> new ApiCommand.DeactivateAPI(id, false,replyTo),ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS, RBACURIConstants.ACTIVE_API_URI, RBACURIConstants.API_DEACTIVATE_MESSAGE, request));
    }

    /* Api activate or deactivate*/
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndActivateApiToMicroService(String microServiceId, String id, ActivationRequest request) {
        Api api = validateAndGetApiById(id);
        MicroService microService = validateAndGetMicroServiceById(microServiceId);
        if (!microService.getApiIds().contains(id)) {
            throw new NotFound("Api Id does't belong to the given MicroService : " + microServiceId);
        }
        if (api.isActive()) {
            throw new ParameterRequired(new String[]{"API '" + id + "' is already active."});
        }
        return serviceImplCommon.getEntityRefForApi(id).<Done>ask(replyTo -> new ApiCommand.ActivateAPI(id, true,replyTo),ServiceConstants.ASK_TIMEOUT)
                .thenCompose(done -> serviceImplCommon.getEntityRefForMicroService(microServiceId).<Done>ask(replyTo -> new MicroServiceCommand.ActivateApiToMicroService(microServiceId, id, false,replyTo),ServiceConstants.ASK_TIMEOUT)
                        .thenApply(done1 -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS, RBACURIConstants.ACTIVE_API_URI, RBACURIConstants.API_ACTIVATE_MESSAGE, request)));
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndDeactivateApiInMicroService(String microServiceId, String id, ActivationRequest request) {
        Api api = validateAndGetApiById(id);
        MicroService microService = validateAndGetMicroServiceById(microServiceId);
        if (!microService.getApiIds().contains(id)) {
            throw new NotFound("Api Id does't belong to the given MicroService : " + microServiceId);
        }
        if (!api.isActive()) {
            throw new ParameterRequired(new String[]{"API '" + id + "' is already deactivate."});
        }
        return serviceImplCommon.getEntityRefForApi(id).<Done>ask(replyTo -> new ApiCommand.DeactivateAPI(id, true,replyTo),ServiceConstants.ASK_TIMEOUT)
                .thenCompose(done -> serviceImplCommon.getEntityRefForMicroService(microServiceId).<Done>ask(replyTo -> new MicroServiceCommand.DeactivateApiToMicroService(microServiceId, id, false,replyTo),ServiceConstants.ASK_TIMEOUT)
                        .thenApply(done1 -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS, RBACURIConstants.ACTIVE_API_URI, RBACURIConstants.API_ACTIVATE_MESSAGE, request)));
    }
       /* Permission activate or deactivate*/
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndActivatePermission(String id, ActivationRequest request) {
        Permission permission = validateAndGetPermissionById(id);
        if (permission.isActive()) {
            throw new ParameterRequired(new String[]{"Permission '" + id + "' is already active."});
        }
        return serviceImplCommon.getEntityRefForPermission(id).<Done>ask(replyTo -> new PermissionCommand.ActivatePermission(id, true,replyTo),ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS, RBACURIConstants.ACTIVE_PERMISSION_URI, RBACURIConstants.PERMISSION_ACTIVATED_MESSAGE, request));
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndDeactivatePermission(String id, ActivationRequest request) {
        Permission permission = validateAndGetPermissionById(id);
        if (!permission.isActive()) {
            throw new ParameterRequired(new String[]{"Permission '" + id + "' is already deactivate."});
        }
        return serviceImplCommon.getEntityRefForPermission(id).<Done>ask(replyTo -> new PermissionCommand.DeactivatePermission(id, false,replyTo),ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS, RBACURIConstants.ACTIVE_PERMISSION_URI, RBACURIConstants.PERMISSION_DEACTIVATED_MESSAGE, request));
    }

// AccessRight Activate & Deactivate
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndDeactivateAccessRight(String accessId, ActivationRequest request) {
        AccessRight accessRight = validateAndGetAccessRightById(accessId);
        if (!accessRight.isActive()) {
            throw new ParameterRequired(new String[]{"Access Right '" + accessId + "' is already deactivate"});
        }
        return serviceImplCommon.getEntityRefForAccessRight(accessId).<Done>ask(replyTo -> new AccessRightCommand.DeactivateAccessRight(accessId, false,replyTo),ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(accessId, SUCCESS_STATUS, RBACURIConstants.ACTIVE_ACCESS_RIGHT_URI, RBACURIConstants.ACCESS_RIGHT_DEACTIVATED_MESSAGE, request));
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndActivateAccessRight(String accessId, ActivationRequest request) {
        AccessRight accessRight = validateAndGetAccessRightById(accessId);
        if (accessRight.isActive()) {
            throw new ParameterRequired(new String[]{"Access Right '" + accessId + "' is already active"});
        }
        return serviceImplCommon.getEntityRefForAccessRight(accessId).<Done>ask(replyTo -> new AccessRightCommand.ActivateAccessRight(accessId, true,replyTo), ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(accessId, SUCCESS_STATUS, RBACURIConstants.ACTIVE_ACCESS_RIGHT_URI, RBACURIConstants.ACCESS_RIGHT_ACTIVATED_MESSAGE, request));
    }

    public Pair<ResponseHeader, BaseResponse> sendResponseByStatusIdAndRequest(String Id, Integer STATUS, String URI, String MESSAGE, BaseRequest request) {
        BaseResponse response = BaseResponse.getResponseByStatusAndMessage(Id, STATUS, MESSAGE);
        try {
            LogUtil.log(new StringBuilder(LOG_RESPONSE).append(URI).append(LINE_SEPARATOR).append("REQ: ").append(new ObjectMapper().writeValueAsString(request)).append(LINE_SEPARATOR).append("RES: ").append(response).append(LINE_SEPARATOR).toString());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return Pair.create(ResponseHeader.OK.withStatus(STATUS), response);
    }

    public RBACCommonFilterParameter filterParameterValidation(String pageNo, String pageSize, String name, String active, String ascending, String all) {
        try {
            int pageNoInt = Integer.valueOf(pageNo);
            int pageSizeInt = Integer.valueOf(pageSize);
            boolean activeBool = Boolean.valueOf(active);
            boolean ascendingBool = Boolean.valueOf(ascending);
            boolean allBool = Boolean.valueOf(all);
            return RBACCommonFilterParameter.getInstance(pageNoInt, pageSizeInt, activeBool, ascendingBool, allBool, name);

        } catch (NumberFormatException ex) {
            throw new ParameterRequired(new String[]{"You need to pass integer in pageNo, pageSize"});
        }
    }


    // Workspace
    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndDeactivateWorkspace(String id, ActivationRequest request) {
        Workspace workspace = validateAndGetWorkspaceById(id);
        if (!workspace.isActive()) {
            throw new ParameterRequired(new String[]{"Workspace '" + id + "' is already deactivate."});
        }
        return serviceImplCommon.getEntityRefForWorkspace(id).<Done>ask(replyTo-> new WorkspaceCommand.DeactivateWorkspace(id, false,replyTo),ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(id, SUCCESS_STATUS, RBACURIConstants.DEACTIVATE_WORKSPACE_URI, RBACURIConstants.WORKSPACE_DEACTIVATED_MESSAGE, request));
    }

    public CompletionStage<Pair<ResponseHeader, BaseResponse>> validateAndActivateWorkspace(String workspaceId, ActivationRequest request) {
        Workspace workspace = validateAndGetWorkspaceById(workspaceId);
        if (workspace.isActive()) {
            throw new ParameterRequired(new String[]{"Workspace '" + workspaceId + "' is already active"});
        }
        return serviceImplCommon.getEntityRefForWorkspace(workspaceId).<Done>ask(replyTo -> new WorkspaceCommand.ActivateWorkspace(workspaceId, true,replyTo), ServiceConstants.ASK_TIMEOUT)
                .thenApply(done -> sendResponseByStatusIdAndRequest(workspaceId, SUCCESS_STATUS, RBACURIConstants.ACTIVATE_WORKSPACE_URI, RBACURIConstants.WORKSPACE_ACTIVATED_MESSAGE, request));
    }

    public Workspace validateAndGetWorkspaceById(String workspaceId) {
        return serviceImplCommon.getEntityRefForWorkspace(workspaceId).ask(WorkspaceCommand.GetWorkspace::new ,ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().orElseThrow(() -> new ParameterRequired(new String[]{"workspaceId: " + "Workspace id(" + workspaceId + ") is not valid"}));
    }

    public void validateWorkspaceId(String workspaceId) {
        if (!serviceImplCommon.getEntityRefForWorkspace(workspaceId).ask(WorkspaceCommand.GetWorkspace::new ,ServiceConstants.ASK_TIMEOUT).toCompletableFuture().join().isPresent())
            throw new ParameterRequired(new String[]{"workspaceId: " + "Workspace id(" + workspaceId + ") is not valid"});
    }

    public void validateWorkspaceIds(Set<String> workspaceIds) {

        workspaceIds.forEach(this::validateWorkspaceId);
    }

    public void validateIfWorkspaceAlreadyCreated(String name, CommonRepository commonRepository) {
        if(commonRepository.doesNameAlreadyExist("workspace", "name", name))
            throw new ParameterRequired(new String[]{"Workspace already exists with name: " + name + "!"});
    }

    public void validateIfAccessRightAlreadyCreated(String name, CommonRepository commonRepository) {
        if(commonRepository.doesNameAlreadyExist("access_right", "name", name))
            throw new ParameterRequired(new String[]{"AccessRight already exists with name: " + name + "!"});
    }

    public void validateIfPermissionAlreadyCreated(String name, CommonRepository commonRepository) {
        if(commonRepository.doesNameAlreadyExist("permission", "name", name))
            throw new ParameterRequired(new String[]{"Permission already exists with name: " + name + "!"});
    }

}