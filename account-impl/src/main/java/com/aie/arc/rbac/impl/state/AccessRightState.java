package com.aie.arc.rbac.impl.state;

import com.aie.arc.rbac.impl.entity.entities.AccessRight;
import com.aie.arc.rbac.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.CompressedJsonable;
import lombok.Value;

import java.util.Optional;
import java.util.function.Function;

@Value
@JsonDeserialize
public class AccessRightState implements CompressedJsonable {

    public final Optional<AccessRight> accessRight;
    @JsonCreator
    public AccessRightState(Optional<AccessRight> accessRight) {

        this.accessRight = accessRight;
    }

    public static AccessRightState empty() {

        return new AccessRightState(Optional.empty());
    }

    public static AccessRightState create(AccessRight accessRight) {
        return new AccessRightState(Optional.of(accessRight));
    }

    public Status getStatus() {
        return accessRight.map(AccessRight::getStatus).orElse(Status.NOT_CREATED);
    }

    public AccessRightState updateDetails(AccessRight details) {

        return update(i -> i.withDetails(details));
    }

    private AccessRightState update(Function<AccessRight, AccessRight> updateFunction) {
        assert accessRight.isPresent();
        return new AccessRightState(accessRight.map(updateFunction));
    }

    public AccessRightState updateDetails(boolean active) {

        return update(i -> i.withDetails(active));
    }
}
