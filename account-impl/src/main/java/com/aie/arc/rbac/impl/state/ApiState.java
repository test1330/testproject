package com.aie.arc.rbac.impl.state;

import com.aie.arc.rbac.impl.entity.entities.Api;
import com.aie.arc.rbac.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.CompressedJsonable;
import lombok.Value;

import java.util.Optional;
import java.util.function.Function;

@Value
@JsonDeserialize
public class ApiState implements CompressedJsonable {
    public final Optional<Api> api;
    @JsonCreator
    public ApiState(Optional<Api> api) {
        this.api = api;
    }

    public static ApiState empty() {
        return new ApiState(Optional.empty());
    }

    public static ApiState create(Api api) {
        return new ApiState(Optional.of(api));
    }

    public Status getStatus() {
        return api.map(Api::getStatus).orElse(Status.NOT_CREATED);
    }

    public ApiState updateDetails(Api details) {
        return update(i -> i.withDetails(details));
    }
    public ApiState updateDetails(boolean active) {
        return update(i -> i.withDetails(active));
    }

    private ApiState update(Function<Api, Api> updateFunction) {
        assert api.isPresent();
        return new ApiState(api.map(updateFunction));
    }
}
