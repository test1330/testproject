package com.aie.arc.rbac.impl.state;

import com.aie.arc.rbac.impl.entity.entities.MicroService;
import com.aie.arc.rbac.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.CompressedJsonable;
import lombok.Value;

import java.util.Optional;
import java.util.function.Function;

@Value
@JsonDeserialize
public class MicroServiceState implements CompressedJsonable {

    public final Optional<MicroService> microService;
    @JsonCreator
    public MicroServiceState(Optional<MicroService> microService) {
        this.microService = microService;
    }

    public static MicroServiceState empty() {
        return new MicroServiceState(Optional.empty());
    }

    public static MicroServiceState create(MicroService microService) {
        return new MicroServiceState(Optional.of(microService));
    }

    public Status getStatus() {
        return microService.map(MicroService::getStatus).orElse(Status.NOT_CREATED);
    }

    public MicroServiceState updateDetails(MicroService details) {
        return update(i -> i.withDetails(details));
    }
    public MicroServiceState updateDetails(boolean active) {
        return update(i -> i.withDetails(active));
    }

    private MicroServiceState update(Function<MicroService, MicroService> updateFunction) {
        assert microService.isPresent();
        return new MicroServiceState(microService.map(updateFunction));
    }

}
