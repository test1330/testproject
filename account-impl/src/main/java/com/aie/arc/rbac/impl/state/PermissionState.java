package com.aie.arc.rbac.impl.state;

import com.aie.arc.rbac.impl.entity.entities.Permission;
import com.aie.arc.rbac.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.CompressedJsonable;
import lombok.Value;

import java.util.Optional;
import java.util.function.Function;

@Value
@JsonDeserialize
public class PermissionState implements CompressedJsonable {
    public final Optional<Permission> permission;
    @JsonCreator
    public PermissionState(Optional<Permission> permission) {
        this.permission = permission;
    }

    public static PermissionState empty() {
        return new PermissionState(Optional.empty());
    }

    public static PermissionState create(Permission permission) {
        return new PermissionState(Optional.of(permission));
    }

    public Status getStatus() {
        return permission.map(Permission::getStatus).orElse(Status.NOT_CREATED);
    }

    public PermissionState updateDetails(Permission details) {
        return update(i -> i.withDetails(details));
    }
    public PermissionState updateDetails(boolean active) {
        return update(i -> i.withDetails(active));
    }

    private PermissionState update(Function<Permission, Permission> updateFunction) {
        assert permission.isPresent();
        return new PermissionState(permission.map(updateFunction));
    }
}
