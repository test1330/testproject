package com.aie.arc.rbac.impl.state;

import com.aie.arc.rbac.impl.entity.entities.RoleGroup;
import com.aie.arc.rbac.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.CompressedJsonable;
import lombok.Value;

import java.util.Optional;
import java.util.function.Function;

@Value
@JsonDeserialize
public class RoleGroupState implements CompressedJsonable {

    public final Optional<RoleGroup> roleGroup;
    @JsonCreator
    public RoleGroupState(Optional<RoleGroup> roleGroup) {
        this.roleGroup = roleGroup;
    }

    public static RoleGroupState empty() {
        return new RoleGroupState(Optional.empty());
    }

    public static RoleGroupState create(RoleGroup roleGroup) {
        return new RoleGroupState(Optional.of(roleGroup));
    }

    public Status getStatus() {
        return roleGroup.map(RoleGroup::getStatus).orElse(Status.NOT_CREATED);
    }

    public RoleGroupState updateDetails(RoleGroup details) {
        return update(i -> i.withDetails(details));
    }
    public RoleGroupState updateDetails(boolean active) {
        return update(i -> i.withDetails(active));
    }

    private RoleGroupState update(Function<RoleGroup, RoleGroup> updateFunction) {
        assert roleGroup.isPresent();
        return new RoleGroupState(roleGroup.map(updateFunction));
    }
}
