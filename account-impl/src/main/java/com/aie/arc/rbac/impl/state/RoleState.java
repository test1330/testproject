package com.aie.arc.rbac.impl.state;
import com.aie.arc.rbac.impl.entity.entities.Role;
import com.aie.arc.rbac.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.CompressedJsonable;
import lombok.Value;

import java.util.Optional;
import java.util.function.Function;

@Value
@JsonDeserialize
public class RoleState implements CompressedJsonable {

    public final Optional<Role> role;
    @JsonCreator
    public RoleState(Optional<Role> role) {
        this.role = role;
    }

    public static RoleState empty() {
        return new RoleState(Optional.empty());
    }

    public static RoleState create(Role role) {
        return new RoleState(Optional.of(role));
    }

    public Status getStatus() {
        return role.map(Role::getStatus).orElse(Status.NOT_CREATED);
    }

    public RoleState updateDetails(Role details) {
        return update(i -> i.withDetails(details));
    }
    public RoleState updateDetails(boolean active) {
        return update(i -> i.withDetails(active));
    }

    private RoleState update(Function<Role, Role> updateFunction) {
        assert role.isPresent();
        return new RoleState(role.map(updateFunction));
    }

}

