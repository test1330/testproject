package com.aie.arc.rbac.impl.state;


import com.aie.arc.rbac.impl.entity.entities.Workspace;
import com.aie.arc.rbac.impl.status.Status;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.CompressedJsonable;
import lombok.Value;

import java.util.Optional;
import java.util.function.Function;

@Value
@JsonDeserialize
public class WorkspaceState implements CompressedJsonable {

    public final Optional<Workspace> workspace;

    @JsonCreator
    public WorkspaceState(Optional<Workspace> workspace) {
        this.workspace = workspace;
    }
    public static WorkspaceState empty() {
        return new WorkspaceState(Optional.empty());
    }
    public static WorkspaceState create(Workspace workspace) {
        return new WorkspaceState(Optional.of(workspace));
    }
    public Status getStatus() {
        return workspace.map(Workspace::getStatus).orElse(Status.NOT_CREATED);
    }
    public WorkspaceState updateDetails(Workspace details) {
        return update(i -> i.withDetails(details));
    }
    private WorkspaceState update(Function<Workspace, Workspace> updateFunction) {
        assert workspace.isPresent();
        return new WorkspaceState(workspace.map(updateFunction));
    }
    public WorkspaceState updateDetails(boolean active) {
        return update(i -> i.withDetails(active));
    }
}


