package com.aie.arc.rbac.impl.status;

public enum CommonStatus {
    /**
     * The Entry has been created.
     */
    CREATED
}
