package com.aie.arc.rbac.impl.status;

public enum Status {

    NOT_CREATED {
        @Override
        CommonStatus toCommonStatus() {
            throw new IllegalStateException("Publicly exposed entry can't be not created");
        }
    },
    CREATED {
        @Override
        CommonStatus toCommonStatus() {
            return CommonStatus.CREATED;
        }
    };

    abstract CommonStatus toCommonStatus();
}