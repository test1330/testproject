package com.aie.arc.rbac.impl.util;

public class ApiTagConstants {

    public static final String GET_BUSINESS_USER_API = "rbac.businessuser.get";
    public static final String CREATE_BUSINESS_USER_API = "rbac.businessuser.create";
    public static final String UPDATE_BUSINESS_USER_API = "rbac.businessuser.update";
    public static final String UPDATE_BUSINESS_USER_ACLLIST_API = "rbac.businessuser.apiacllist";
    public static final String UPDATE_ROLE_API = "rbac.role.update";
    public static final String CREATE_ROLE_API = "rbac.role.create";
    public static final String GET_ROLE_API = "rbac.role.get";
    public static final String UPDATE_PERMISSION_API = "rbac.permission.update";
    public static final String CREATE_PERMISSION_API = "rbac.permission.create";
    public static final String GET_PERMISSION_API = "rbac.permission.get";
    public static final String GET_ACCESS_RIGHTS_API = "rbac.accessright.get";
    public static final String UPDATE_ACCESS_RIGHTS_API = "rbac.accessright.update";
    public static final String CREATE_ACCESS_RIGHTS_API = "rbac.accessright.create";
    public static final String GET_WORKSAPCE_API = "rbac.workspace.get";
    public static final String CREATE_WORKSAPCE_API = "rbac.workspace.create";
    public static final String UPDATE_WORKSAPCE_API = "rbac.workspace.update";

    public static final String ACL_ENABLED = "enableacl";
    public static final String X_USR_API_TAG= "x-usr-api-tag";
    public static final String IGNORE_PERMISSION= "Ignore-Permission";
    public static final String TRUE= "TRUE";
    public static final String UNDER_SCORE = "_";
    public static final String ACCESS_DENIED_MESSAGE= "AccessToken";
}
