package com.aie.arc.rbac.impl.util;

import play.Logger;
import play.Logger.ALogger;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.GZIPInputStream;

public class LogUtil {
    private final static ALogger accessLogger = Logger.of("access");
    private static final String LOG_FILE_PATH = "lockProduct-impl/logs/access.log";
    private static final String ABSLT_PATH = "lockProduct-impl/logs";

    public static void log(String logMessage) {
        accessLogger.info(logMessage);
    }

    public static String fetchLog(String date) {
        BufferedReader br = null;
        FileReader fileReader = null;
        InputStream fileStream = null;
        InputStream gzipStream = null;
        try {
            String sCurrentLine;
            String fileName = "arch-access/access-log-xxx.gz";
            //response().setContentType("text/plain; charset=iso-8859-1");
            StringBuilder strBuilder = new StringBuilder();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = dateFormat.format(new Date());
            if (date.equals(strDate)) {
                fileReader = new FileReader(LOG_FILE_PATH);
                br = new BufferedReader(fileReader);
            } else {
                fileName = fileName.replace("xxx", date);
                fileStream = new FileInputStream(ABSLT_PATH + File.separatorChar + fileName);
                gzipStream = new GZIPInputStream(fileStream);
                Reader decoder = new InputStreamReader(gzipStream);
                br = new BufferedReader(decoder);
            }
            while ((sCurrentLine = br.readLine()) != null) {
                strBuilder.append("\n").append(sCurrentLine);
            }
            return strBuilder.toString();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {

            try {
                if (fileReader != null)
                    fileReader.close();
                if (fileStream != null)
                    fileStream.close();
                if (gzipStream != null)
                    gzipStream.close();
                if (br != null)
                    br.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return "no recodrs found";
    }

}