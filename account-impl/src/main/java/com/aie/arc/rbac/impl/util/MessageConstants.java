package com.aie.arc.rbac.impl.util;

public class MessageConstants {

    // Common MessageConstants
    public static final String CONVERTING_NON_PUB_EVENT = "Converting non public event";
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    public static final String LOG_RESPONSE = "RES:  ";
    public static final String NEED_LOCK_TO_PRODUCT_MESSAGE = "You need to lock product first";
    public static final Integer SUCCESS_STATUS = 200;
    public static final String BUSINESS = "Business";
}