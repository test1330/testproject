
package com.brandsmartusa.services.atgwebbroker;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfAtgService_Error complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfAtgService_Error"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AtgService_Error" type="{http://brandsmartusa.com/Services/AtgWebBroker}AtgService_Error" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfAtgService_Error", propOrder = {
    "atgServiceError"
})
public class ArrayOfAtgServiceError {

    @XmlElement(name = "AtgService_Error", nillable = true)
    protected List<AtgServiceError> atgServiceError;

    /**
     * Gets the value of the atgServiceError property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the atgServiceError property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAtgServiceError().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AtgServiceError }
     * 
     * 
     */
    public List<AtgServiceError> getAtgServiceError() {
        if (atgServiceError == null) {
            atgServiceError = new ArrayList<AtgServiceError>();
        }
        return this.atgServiceError;
    }

}
