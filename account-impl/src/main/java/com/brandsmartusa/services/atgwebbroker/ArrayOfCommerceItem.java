
package com.brandsmartusa.services.atgwebbroker;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfCommerceItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCommerceItem"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CommerceItem" type="{http://brandsmartusa.com/Services/AtgWebBroker}CommerceItem" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCommerceItem", propOrder = {
    "commerceItem"
})
public class ArrayOfCommerceItem {

    @XmlElement(name = "CommerceItem", nillable = true)
    protected List<CommerceItem> commerceItem;

    /**
     * Gets the value of the commerceItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commerceItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommerceItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommerceItem }
     * 
     * 
     */
    public List<CommerceItem> getCommerceItem() {
        if (commerceItem == null) {
            commerceItem = new ArrayList<CommerceItem>();
        }
        return this.commerceItem;
    }

}
