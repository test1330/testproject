
package com.brandsmartusa.services.atgwebbroker;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfShippingGroupStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfShippingGroupStatus"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShippingGroupStatus" type="{http://brandsmartusa.com/Services/AtgWebBroker}ShippingGroupStatus" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfShippingGroupStatus", propOrder = {
    "shippingGroupStatus"
})
public class ArrayOfShippingGroupStatus {

    @XmlElement(name = "ShippingGroupStatus", nillable = true)
    protected List<ShippingGroupStatus> shippingGroupStatus;

    /**
     * Gets the value of the shippingGroupStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shippingGroupStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShippingGroupStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShippingGroupStatus }
     * 
     * 
     */
    public List<ShippingGroupStatus> getShippingGroupStatus() {
        if (shippingGroupStatus == null) {
            shippingGroupStatus = new ArrayList<ShippingGroupStatus>();
        }
        return this.shippingGroupStatus;
    }

}
