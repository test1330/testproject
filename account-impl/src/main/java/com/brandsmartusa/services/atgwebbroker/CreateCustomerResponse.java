
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreateCustomerResult" type="{http://brandsmartusa.com/Services/AtgWebBroker}CreateCustomer_Response" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createCustomerResult"
})
@XmlRootElement(name = "CreateCustomerResponse")
public class CreateCustomerResponse {

    @XmlElementRef(name = "CreateCustomerResult", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<CreateCustomerResponse2> createCustomerResult;

    /**
     * Gets the value of the createCustomerResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CreateCustomerResponse2 }{@code >}
     *     
     */
    public JAXBElement<CreateCustomerResponse2> getCreateCustomerResult() {
        return createCustomerResult;
    }

    /**
     * Sets the value of the createCustomerResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CreateCustomerResponse2 }{@code >}
     *     
     */
    public void setCreateCustomerResult(JAXBElement<CreateCustomerResponse2> value) {
        this.createCustomerResult = value;
    }

}
