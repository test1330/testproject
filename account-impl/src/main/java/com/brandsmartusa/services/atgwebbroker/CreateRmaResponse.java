
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreateRmaResult" type="{http://brandsmartusa.com/Services/AtgWebBroker}CreateRma_Response" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createRmaResult"
})
@XmlRootElement(name = "CreateRmaResponse")
public class CreateRmaResponse {

    @XmlElementRef(name = "CreateRmaResult", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<CreateRmaResponse2> createRmaResult;

    /**
     * Gets the value of the createRmaResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CreateRmaResponse2 }{@code >}
     *     
     */
    public JAXBElement<CreateRmaResponse2> getCreateRmaResult() {
        return createRmaResult;
    }

    /**
     * Sets the value of the createRmaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CreateRmaResponse2 }{@code >}
     *     
     */
    public void setCreateRmaResult(JAXBElement<CreateRmaResponse2> value) {
        this.createRmaResult = value;
    }

}
