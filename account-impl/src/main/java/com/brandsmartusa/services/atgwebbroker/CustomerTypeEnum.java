
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerTypeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CustomerTypeEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="BrandsMart"/&gt;
 *     &lt;enumeration value="Affinity"/&gt;
 *     &lt;enumeration value="WarranTech"/&gt;
 *     &lt;enumeration value="Assurant"/&gt;
 *     &lt;enumeration value="FlexShopper"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CustomerTypeEnum")
@XmlEnum
public enum CustomerTypeEnum {

    @XmlEnumValue("BrandsMart")
    BRANDS_MART("BrandsMart"),
    @XmlEnumValue("Affinity")
    AFFINITY("Affinity"),
    @XmlEnumValue("WarranTech")
    WARRAN_TECH("WarranTech"),
    @XmlEnumValue("Assurant")
    ASSURANT("Assurant"),
    @XmlEnumValue("FlexShopper")
    FLEX_SHOPPER("FlexShopper");
    private final String value;

    CustomerTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CustomerTypeEnum fromValue(String v) {
        for (CustomerTypeEnum c: CustomerTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
