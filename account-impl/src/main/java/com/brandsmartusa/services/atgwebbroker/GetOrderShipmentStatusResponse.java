
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetOrderShipmentStatusResult" type="{http://brandsmartusa.com/Services/AtgWebBroker}GetOrderShipmentStatus_Response" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getOrderShipmentStatusResult"
})
@XmlRootElement(name = "GetOrderShipmentStatusResponse")
public class GetOrderShipmentStatusResponse {

    @XmlElementRef(name = "GetOrderShipmentStatusResult", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<GetOrderShipmentStatusResponse2> getOrderShipmentStatusResult;

    /**
     * Gets the value of the getOrderShipmentStatusResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetOrderShipmentStatusResponse2 }{@code >}
     *     
     */
    public JAXBElement<GetOrderShipmentStatusResponse2> getGetOrderShipmentStatusResult() {
        return getOrderShipmentStatusResult;
    }

    /**
     * Sets the value of the getOrderShipmentStatusResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetOrderShipmentStatusResponse2 }{@code >}
     *     
     */
    public void setGetOrderShipmentStatusResult(JAXBElement<GetOrderShipmentStatusResponse2> value) {
        this.getOrderShipmentStatusResult = value;
    }

}
