
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetOrderStatusResult" type="{http://brandsmartusa.com/Services/AtgWebBroker}GetOrderStatus_Response" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getOrderStatusResult"
})
@XmlRootElement(name = "GetOrderStatusResponse")
public class GetOrderStatusResponse {

    @XmlElementRef(name = "GetOrderStatusResult", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<GetOrderStatusResponse2> getOrderStatusResult;

    /**
     * Gets the value of the getOrderStatusResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetOrderStatusResponse2 }{@code >}
     *     
     */
    public JAXBElement<GetOrderStatusResponse2> getGetOrderStatusResult() {
        return getOrderStatusResult;
    }

    /**
     * Sets the value of the getOrderStatusResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetOrderStatusResponse2 }{@code >}
     *     
     */
    public void setGetOrderStatusResult(JAXBElement<GetOrderStatusResponse2> value) {
        this.getOrderStatusResult = value;
    }

}
