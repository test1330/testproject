
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetServiceInfoResult" type="{http://brandsmartusa.com/Services/}Service_Info" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getServiceInfoResult"
})
@XmlRootElement(name = "GetServiceInfoResponse")
public class GetServiceInfoResponse {

    @XmlElementRef(name = "GetServiceInfoResult", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<ServiceInfo> getServiceInfoResult;

    /**
     * Gets the value of the getServiceInfoResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ServiceInfo }{@code >}
     *     
     */
    public JAXBElement<ServiceInfo> getGetServiceInfoResult() {
        return getServiceInfoResult;
    }

    /**
     * Sets the value of the getServiceInfoResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ServiceInfo }{@code >}
     *     
     */
    public void setGetServiceInfoResult(JAXBElement<ServiceInfo> value) {
        this.getServiceInfoResult = value;
    }

}
