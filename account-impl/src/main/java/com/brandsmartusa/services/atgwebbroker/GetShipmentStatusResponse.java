
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetShipmentStatusResult" type="{http://brandsmartusa.com/Services/AtgWebBroker}GetShipmentStatus_Response" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getShipmentStatusResult"
})
@XmlRootElement(name = "GetShipmentStatusResponse")
public class GetShipmentStatusResponse {

    @XmlElementRef(name = "GetShipmentStatusResult", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<GetShipmentStatusResponse2> getShipmentStatusResult;

    /**
     * Gets the value of the getShipmentStatusResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetShipmentStatusResponse2 }{@code >}
     *     
     */
    public JAXBElement<GetShipmentStatusResponse2> getGetShipmentStatusResult() {
        return getShipmentStatusResult;
    }

    /**
     * Sets the value of the getShipmentStatusResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetShipmentStatusResponse2 }{@code >}
     *     
     */
    public void setGetShipmentStatusResult(JAXBElement<GetShipmentStatusResponse2> value) {
        this.getShipmentStatusResult = value;
    }

}
