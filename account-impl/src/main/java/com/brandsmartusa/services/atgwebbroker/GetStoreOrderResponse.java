
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetStoreOrderResult" type="{http://brandsmartusa.com/Services/AtgWebBroker}GetStoreOrder_Response" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getStoreOrderResult"
})
@XmlRootElement(name = "GetStoreOrderResponse")
public class GetStoreOrderResponse {

    @XmlElementRef(name = "GetStoreOrderResult", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<GetStoreOrderResponse2> getStoreOrderResult;

    /**
     * Gets the value of the getStoreOrderResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetStoreOrderResponse2 }{@code >}
     *     
     */
    public JAXBElement<GetStoreOrderResponse2> getGetStoreOrderResult() {
        return getStoreOrderResult;
    }

    /**
     * Sets the value of the getStoreOrderResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetStoreOrderResponse2 }{@code >}
     *     
     */
    public void setGetStoreOrderResult(JAXBElement<GetStoreOrderResponse2> value) {
        this.getStoreOrderResult = value;
    }

}
