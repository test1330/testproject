
package com.brandsmartusa.services.atgwebbroker;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.brandsmartusa.services.atgwebbroker package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfint_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfint");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _Order_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Order");
    private final static QName _Customer_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Customer");
    private final static QName _CustomerTypeEnum_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "CustomerTypeEnum");
    private final static QName _Address_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Address");
    private final static QName _ArrayOfPayment_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ArrayOfPayment");
    private final static QName _Payment_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Payment");
    private final static QName _PaymentTypeEnum_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "PaymentTypeEnum");
    private final static QName _ArrayOfPromo_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ArrayOfPromo");
    private final static QName _Promo_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Promo");
    private final static QName _ArrayOfCommerceItem_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ArrayOfCommerceItem");
    private final static QName _CommerceItem_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "CommerceItem");
    private final static QName _ArrayOfShipment_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ArrayOfShipment");
    private final static QName _Shipment_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Shipment");
    private final static QName _ArrayOfShipmentItem_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ArrayOfShipmentItem");
    private final static QName _ShipmentItem_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ShipmentItem");
    private final static QName _ArrayOfPromotion_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ArrayOfPromotion");
    private final static QName _Promotion_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Promotion");
    private final static QName _PromotionTypeEnum_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "PromotionTypeEnum");
    private final static QName _OrderSourceEnum_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "OrderSourceEnum");
    private final static QName _TaxRateSource_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "TaxRateSource");
    private final static QName _CreateOrderResponse_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "CreateOrder_Response");
    private final static QName _ResponseStatus_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ResponseStatus");
    private final static QName _CreateCustomerResponse_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "CreateCustomer_Response");
    private final static QName _Rma_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Rma");
    private final static QName _ArrayOfRmaItem_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ArrayOfRmaItem");
    private final static QName _RmaItem_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "RmaItem");
    private final static QName _RmaReasonEnum_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "RmaReasonEnum");
    private final static QName _CreateRmaResponse_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "CreateRma_Response");
    private final static QName _LoggerInfo_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "LoggerInfo");
    private final static QName _GetOrderShipmentStatusResponse_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "GetOrderShipmentStatus_Response");
    private final static QName _ArrayOfShippingGroupStatus_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ArrayOfShippingGroupStatus");
    private final static QName _ShippingGroupStatus_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ShippingGroupStatus");
    private final static QName _GetOrderStatusResponse_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "GetOrderStatus_Response");
    private final static QName _GetShipmentStatusResponse_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "GetShipmentStatus_Response");
    private final static QName _GetStoreOrderResponse_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "GetStoreOrder_Response");
    private final static QName _StoreOrder_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "StoreOrder");
    private final static QName _StoreLocation_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "StoreLocation");
    private final static QName _ArrayOfStoreOrderItem_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ArrayOfStoreOrderItem");
    private final static QName _StoreOrderItem_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "StoreOrderItem");
    private final static QName _ArrayOfRmaDeliveryItem_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ArrayOfRmaDeliveryItem");
    private final static QName _RmaDeliveryItem_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "RmaDeliveryItem");
    private final static QName _RequestRmaResponse_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "RequestRma_Response");
    private final static QName _ArrayOfAtgServiceError_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ArrayOfAtgService_Error");
    private final static QName _AtgServiceError_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "AtgService_Error");
    private final static QName _VerificationAddress_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "VerificationAddress");
    private final static QName _ArrayOfVerificationAddress_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ArrayOfVerificationAddress");
    private final static QName _ServiceInfo_QNAME = new QName("http://brandsmartusa.com/Services/", "Service_Info");
    private final static QName _VerifyAddressResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", "VerifyAddress_Response");
    private final static QName _CreateTestExceptionMessage_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "message");
    private final static QName _CreateOrderResponseCreateOrderResult_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "CreateOrderResult");
    private final static QName _CreateCustomerAtgCustomer_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "AtgCustomer");
    private final static QName _CreateCustomerResponseCreateCustomerResult_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "CreateCustomerResult");
    private final static QName _CreateRmaResponseCreateRmaResult_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "CreateRmaResult");
    private final static QName _GetLoggerInfoResponseGetLoggerInfoResult_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "GetLoggerInfoResult");
    private final static QName _GetOrderShipmentStatusAtgOrderId_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "AtgOrderId");
    private final static QName _GetOrderShipmentStatusResponseGetOrderShipmentStatusResult_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "GetOrderShipmentStatusResult");
    private final static QName _GetOrderStatusResponseGetOrderStatusResult_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "GetOrderStatusResult");
    private final static QName _GetServiceInfoResponseGetServiceInfoResult_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "GetServiceInfoResult");
    private final static QName _GetShipmentStatusAtgShippingGroupId_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "AtgShippingGroupId");
    private final static QName _GetShipmentStatusResponseGetShipmentStatusResult_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "GetShipmentStatusResult");
    private final static QName _GetStoreOrderReceiptNumber_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ReceiptNumber");
    private final static QName _GetStoreOrderPurchaseDate_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "PurchaseDate");
    private final static QName _GetStoreOrderResponseGetStoreOrderResult_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "GetStoreOrderResult");
    private final static QName _ProcessAccertifyResolutionAtgOrderId_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "atgOrderId");
    private final static QName _ProcessAccertifyResolutionActionCode_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "actionCode");
    private final static QName _ProcessAccertifyResolutionRecomendation_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "recomendation");
    private final static QName _ProcessAccertifyResolutionRemarks_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "remarks");
    private final static QName _RequestAtgRmaItems_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Items");
    private final static QName _RequestAtgRmaResponseRequestAtgRmaResult_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "RequestAtgRmaResult");
    private final static QName _ToggleLoggerResponseToggleLoggerResult_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ToggleLoggerResult");
    private final static QName _VerifyAddressAddress_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "address");
    private final static QName _VerifyAddressResponseVerifyAddressResult_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "VerifyAddressResult");
    private final static QName _VerifyAddressResponse2Errors_QNAME = new QName("http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", "Errors");
    private final static QName _VerifyAddressResponse2Message_QNAME = new QName("http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", "Message");
    private final static QName _VerifyAddressResponse2SuggestedAddresses_QNAME = new QName("http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", "SuggestedAddresses");
    private final static QName _VerificationAddressLine2_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Line2");
    private final static QName _RequestRmaResponseErrors_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Errors");
    private final static QName _RequestRmaResponseRmaIds_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "RmaIds");
    private final static QName _StoreLocationAddress2_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Address2");
    private final static QName _GetStoreOrderResponse2Message_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Message");
    private final static QName _GetShipmentStatusResponse2StatusText_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "StatusText");
    private final static QName _RmaItemStatus_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Status");
    private final static QName _PromotionCode_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Code");
    private final static QName _ShipmentItemAtgParentCommerceItemId_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "AtgParentCommerceItemId");
    private final static QName _ShipmentItemPromotionCode_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "PromotionCode");
    private final static QName _ShipmentItemGWPCommerceItemId_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "GWPCommerceItemId");
    private final static QName _ShipmentItemAvalaraCode_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "AvalaraCode");
    private final static QName _ShipmentShippingPromotionCode_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ShippingPromotionCode");
    private final static QName _ShipmentShippingDate_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ShippingDate");
    private final static QName _ShipmentSpecialInstructions_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "SpecialInstructions");
    private final static QName _ShipmentExportCity_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ExportCity");
    private final static QName _ShipmentExportCountry_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ExportCountry");
    private final static QName _ShipmentAlternatePickupAddress_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "AlternatePickupAddress");
    private final static QName _ShipmentShippingAvalaraCode_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "ShippingAvalaraCode");
    private final static QName _PaymentAccountEmail_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "AccountEmail");
    private final static QName _PaymentAccountFirst6_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "AccountFirst6");
    private final static QName _PaymentAccountLast4_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "AccountLast4");
    private final static QName _PaymentAuthoriationCode_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "AuthoriationCode");
    private final static QName _PaymentPassedAVS_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "PassedAVS");
    private final static QName _PaymentPassedCVV_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "PassedCVV");
    private final static QName _PaymentIsVerified_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "IsVerified");
    private final static QName _PaymentPromoCode_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "PromoCode");
    private final static QName _PaymentPromos_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Promos");
    private final static QName _AddressFirstName_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "FirstName");
    private final static QName _AddressLastName_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "LastName");
    private final static QName _AddressCompanyName_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "CompanyName");
    private final static QName _AddressAddress1_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Address1");
    private final static QName _AddressCity_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "City");
    private final static QName _AddressState_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "State");
    private final static QName _AddressZip_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Zip");
    private final static QName _AddressFipsState_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "FipsState");
    private final static QName _AddressFipsCounty_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "FipsCounty");
    private final static QName _AddressEmail_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Email");
    private final static QName _AddressPhone_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Phone");
    private final static QName _AddressAltPhone_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "AltPhone");
    private final static QName _AddressMobilePhone_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "MobilePhone");
    private final static QName _AddressRegion_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Region");
    private final static QName _AddressCountry_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Country");
    private final static QName _AddressFipsCity_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "FipsCity");
    private final static QName _OrderIpAddress_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "IpAddress");
    private final static QName _OrderPromotions_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Promotions");
    private final static QName _OrderSource_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "Source");
    private final static QName _OrderSourceRef_QNAME = new QName("http://brandsmartusa.com/Services/AtgWebBroker", "SourceRef");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.brandsmartusa.services.atgwebbroker
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfint }
     * 
     */
    public ArrayOfint createArrayOfint() {
        return new ArrayOfint();
    }

    /**
     * Create an instance of {@link CreateTestException }
     * 
     */
    public CreateTestException createCreateTestException() {
        return new CreateTestException();
    }

    /**
     * Create an instance of {@link CreateTestExceptionResponse }
     * 
     */
    public CreateTestExceptionResponse createCreateTestExceptionResponse() {
        return new CreateTestExceptionResponse();
    }

    /**
     * Create an instance of {@link CreateOrder }
     * 
     */
    public CreateOrder createCreateOrder() {
        return new CreateOrder();
    }

    /**
     * Create an instance of {@link Order }
     * 
     */
    public Order createOrder() {
        return new Order();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link ArrayOfPayment }
     * 
     */
    public ArrayOfPayment createArrayOfPayment() {
        return new ArrayOfPayment();
    }

    /**
     * Create an instance of {@link Payment }
     * 
     */
    public Payment createPayment() {
        return new Payment();
    }

    /**
     * Create an instance of {@link ArrayOfPromo }
     * 
     */
    public ArrayOfPromo createArrayOfPromo() {
        return new ArrayOfPromo();
    }

    /**
     * Create an instance of {@link Promo }
     * 
     */
    public Promo createPromo() {
        return new Promo();
    }

    /**
     * Create an instance of {@link ArrayOfCommerceItem }
     * 
     */
    public ArrayOfCommerceItem createArrayOfCommerceItem() {
        return new ArrayOfCommerceItem();
    }

    /**
     * Create an instance of {@link CommerceItem }
     * 
     */
    public CommerceItem createCommerceItem() {
        return new CommerceItem();
    }

    /**
     * Create an instance of {@link ArrayOfShipment }
     * 
     */
    public ArrayOfShipment createArrayOfShipment() {
        return new ArrayOfShipment();
    }

    /**
     * Create an instance of {@link Shipment }
     * 
     */
    public Shipment createShipment() {
        return new Shipment();
    }

    /**
     * Create an instance of {@link ArrayOfShipmentItem }
     * 
     */
    public ArrayOfShipmentItem createArrayOfShipmentItem() {
        return new ArrayOfShipmentItem();
    }

    /**
     * Create an instance of {@link ShipmentItem }
     * 
     */
    public ShipmentItem createShipmentItem() {
        return new ShipmentItem();
    }

    /**
     * Create an instance of {@link ArrayOfPromotion }
     * 
     */
    public ArrayOfPromotion createArrayOfPromotion() {
        return new ArrayOfPromotion();
    }

    /**
     * Create an instance of {@link Promotion }
     * 
     */
    public Promotion createPromotion() {
        return new Promotion();
    }

    /**
     * Create an instance of {@link CreateOrderResponse }
     * 
     */
    public CreateOrderResponse createCreateOrderResponse() {
        return new CreateOrderResponse();
    }

    /**
     * Create an instance of {@link CreateOrderResponse2 }
     * 
     */
    public CreateOrderResponse2 createCreateOrderResponse2() {
        return new CreateOrderResponse2();
    }

    /**
     * Create an instance of {@link CreateCustomer }
     * 
     */
    public CreateCustomer createCreateCustomer() {
        return new CreateCustomer();
    }

    /**
     * Create an instance of {@link CreateCustomerResponse }
     * 
     */
    public CreateCustomerResponse createCreateCustomerResponse() {
        return new CreateCustomerResponse();
    }

    /**
     * Create an instance of {@link CreateCustomerResponse2 }
     * 
     */
    public CreateCustomerResponse2 createCreateCustomerResponse2() {
        return new CreateCustomerResponse2();
    }

    /**
     * Create an instance of {@link CreateRma }
     * 
     */
    public CreateRma createCreateRma() {
        return new CreateRma();
    }

    /**
     * Create an instance of {@link Rma }
     * 
     */
    public Rma createRma() {
        return new Rma();
    }

    /**
     * Create an instance of {@link ArrayOfRmaItem }
     * 
     */
    public ArrayOfRmaItem createArrayOfRmaItem() {
        return new ArrayOfRmaItem();
    }

    /**
     * Create an instance of {@link RmaItem }
     * 
     */
    public RmaItem createRmaItem() {
        return new RmaItem();
    }

    /**
     * Create an instance of {@link CreateRmaResponse }
     * 
     */
    public CreateRmaResponse createCreateRmaResponse() {
        return new CreateRmaResponse();
    }

    /**
     * Create an instance of {@link CreateRmaResponse2 }
     * 
     */
    public CreateRmaResponse2 createCreateRmaResponse2() {
        return new CreateRmaResponse2();
    }

    /**
     * Create an instance of {@link GetLoggerInfo }
     * 
     */
    public GetLoggerInfo createGetLoggerInfo() {
        return new GetLoggerInfo();
    }

    /**
     * Create an instance of {@link GetLoggerInfoResponse }
     * 
     */
    public GetLoggerInfoResponse createGetLoggerInfoResponse() {
        return new GetLoggerInfoResponse();
    }

    /**
     * Create an instance of {@link LoggerInfo }
     * 
     */
    public LoggerInfo createLoggerInfo() {
        return new LoggerInfo();
    }

    /**
     * Create an instance of {@link GetOrderShipmentStatus }
     * 
     */
    public GetOrderShipmentStatus createGetOrderShipmentStatus() {
        return new GetOrderShipmentStatus();
    }

    /**
     * Create an instance of {@link GetOrderShipmentStatusResponse }
     * 
     */
    public GetOrderShipmentStatusResponse createGetOrderShipmentStatusResponse() {
        return new GetOrderShipmentStatusResponse();
    }

    /**
     * Create an instance of {@link GetOrderShipmentStatusResponse2 }
     * 
     */
    public GetOrderShipmentStatusResponse2 createGetOrderShipmentStatusResponse2() {
        return new GetOrderShipmentStatusResponse2();
    }

    /**
     * Create an instance of {@link ArrayOfShippingGroupStatus }
     * 
     */
    public ArrayOfShippingGroupStatus createArrayOfShippingGroupStatus() {
        return new ArrayOfShippingGroupStatus();
    }

    /**
     * Create an instance of {@link ShippingGroupStatus }
     * 
     */
    public ShippingGroupStatus createShippingGroupStatus() {
        return new ShippingGroupStatus();
    }

    /**
     * Create an instance of {@link GetOrderStatus }
     * 
     */
    public GetOrderStatus createGetOrderStatus() {
        return new GetOrderStatus();
    }

    /**
     * Create an instance of {@link GetOrderStatusResponse }
     * 
     */
    public GetOrderStatusResponse createGetOrderStatusResponse() {
        return new GetOrderStatusResponse();
    }

    /**
     * Create an instance of {@link GetOrderStatusResponse2 }
     * 
     */
    public GetOrderStatusResponse2 createGetOrderStatusResponse2() {
        return new GetOrderStatusResponse2();
    }

    /**
     * Create an instance of {@link GetServiceInfo }
     * 
     */
    public GetServiceInfo createGetServiceInfo() {
        return new GetServiceInfo();
    }

    /**
     * Create an instance of {@link GetServiceInfoResponse }
     * 
     */
    public GetServiceInfoResponse createGetServiceInfoResponse() {
        return new GetServiceInfoResponse();
    }

    /**
     * Create an instance of {@link ServiceInfo }
     * 
     */
    public ServiceInfo createServiceInfo() {
        return new ServiceInfo();
    }

    /**
     * Create an instance of {@link GetShipmentStatus }
     * 
     */
    public GetShipmentStatus createGetShipmentStatus() {
        return new GetShipmentStatus();
    }

    /**
     * Create an instance of {@link GetShipmentStatusResponse }
     * 
     */
    public GetShipmentStatusResponse createGetShipmentStatusResponse() {
        return new GetShipmentStatusResponse();
    }

    /**
     * Create an instance of {@link GetShipmentStatusResponse2 }
     * 
     */
    public GetShipmentStatusResponse2 createGetShipmentStatusResponse2() {
        return new GetShipmentStatusResponse2();
    }

    /**
     * Create an instance of {@link GetStoreOrder }
     * 
     */
    public GetStoreOrder createGetStoreOrder() {
        return new GetStoreOrder();
    }

    /**
     * Create an instance of {@link GetStoreOrderResponse }
     * 
     */
    public GetStoreOrderResponse createGetStoreOrderResponse() {
        return new GetStoreOrderResponse();
    }

    /**
     * Create an instance of {@link GetStoreOrderResponse2 }
     * 
     */
    public GetStoreOrderResponse2 createGetStoreOrderResponse2() {
        return new GetStoreOrderResponse2();
    }

    /**
     * Create an instance of {@link StoreOrder }
     * 
     */
    public StoreOrder createStoreOrder() {
        return new StoreOrder();
    }

    /**
     * Create an instance of {@link StoreLocation }
     * 
     */
    public StoreLocation createStoreLocation() {
        return new StoreLocation();
    }

    /**
     * Create an instance of {@link ArrayOfStoreOrderItem }
     * 
     */
    public ArrayOfStoreOrderItem createArrayOfStoreOrderItem() {
        return new ArrayOfStoreOrderItem();
    }

    /**
     * Create an instance of {@link StoreOrderItem }
     * 
     */
    public StoreOrderItem createStoreOrderItem() {
        return new StoreOrderItem();
    }

    /**
     * Create an instance of {@link ProcessAccertifyResolution }
     * 
     */
    public ProcessAccertifyResolution createProcessAccertifyResolution() {
        return new ProcessAccertifyResolution();
    }

    /**
     * Create an instance of {@link ProcessAccertifyResolutionResponse }
     * 
     */
    public ProcessAccertifyResolutionResponse createProcessAccertifyResolutionResponse() {
        return new ProcessAccertifyResolutionResponse();
    }

    /**
     * Create an instance of {@link RequestAtgRma }
     * 
     */
    public RequestAtgRma createRequestAtgRma() {
        return new RequestAtgRma();
    }

    /**
     * Create an instance of {@link ArrayOfRmaDeliveryItem }
     * 
     */
    public ArrayOfRmaDeliveryItem createArrayOfRmaDeliveryItem() {
        return new ArrayOfRmaDeliveryItem();
    }

    /**
     * Create an instance of {@link RmaDeliveryItem }
     * 
     */
    public RmaDeliveryItem createRmaDeliveryItem() {
        return new RmaDeliveryItem();
    }

    /**
     * Create an instance of {@link RequestAtgRmaResponse }
     * 
     */
    public RequestAtgRmaResponse createRequestAtgRmaResponse() {
        return new RequestAtgRmaResponse();
    }

    /**
     * Create an instance of {@link RequestRmaResponse }
     * 
     */
    public RequestRmaResponse createRequestRmaResponse() {
        return new RequestRmaResponse();
    }

    /**
     * Create an instance of {@link ArrayOfAtgServiceError }
     * 
     */
    public ArrayOfAtgServiceError createArrayOfAtgServiceError() {
        return new ArrayOfAtgServiceError();
    }

    /**
     * Create an instance of {@link AtgServiceError }
     * 
     */
    public AtgServiceError createAtgServiceError() {
        return new AtgServiceError();
    }

    /**
     * Create an instance of {@link ToggleLogger }
     * 
     */
    public ToggleLogger createToggleLogger() {
        return new ToggleLogger();
    }

    /**
     * Create an instance of {@link ToggleLoggerResponse }
     * 
     */
    public ToggleLoggerResponse createToggleLoggerResponse() {
        return new ToggleLoggerResponse();
    }

    /**
     * Create an instance of {@link TransmitCancellations }
     * 
     */
    public TransmitCancellations createTransmitCancellations() {
        return new TransmitCancellations();
    }

    /**
     * Create an instance of {@link TransmitSubstitutions }
     * 
     */
    public TransmitSubstitutions createTransmitSubstitutions() {
        return new TransmitSubstitutions();
    }

    /**
     * Create an instance of {@link TransmitRefunds }
     * 
     */
    public TransmitRefunds createTransmitRefunds() {
        return new TransmitRefunds();
    }

    /**
     * Create an instance of {@link TransmitShipments }
     * 
     */
    public TransmitShipments createTransmitShipments() {
        return new TransmitShipments();
    }

    /**
     * Create an instance of {@link TransmitStorePickups }
     * 
     */
    public TransmitStorePickups createTransmitStorePickups() {
        return new TransmitStorePickups();
    }

    /**
     * Create an instance of {@link TransmitRmaItems }
     * 
     */
    public TransmitRmaItems createTransmitRmaItems() {
        return new TransmitRmaItems();
    }

    /**
     * Create an instance of {@link TransmitAll }
     * 
     */
    public TransmitAll createTransmitAll() {
        return new TransmitAll();
    }

    /**
     * Create an instance of {@link UpdateCustomerIndex }
     * 
     */
    public UpdateCustomerIndex createUpdateCustomerIndex() {
        return new UpdateCustomerIndex();
    }

    /**
     * Create an instance of {@link VerifyAddress }
     * 
     */
    public VerifyAddress createVerifyAddress() {
        return new VerifyAddress();
    }

    /**
     * Create an instance of {@link VerificationAddress }
     * 
     */
    public VerificationAddress createVerificationAddress() {
        return new VerificationAddress();
    }

    /**
     * Create an instance of {@link VerifyAddressResponse }
     * 
     */
    public VerifyAddressResponse createVerifyAddressResponse() {
        return new VerifyAddressResponse();
    }

    /**
     * Create an instance of {@link VerifyAddressResponse2 }
     * 
     */
    public VerifyAddressResponse2 createVerifyAddressResponse2() {
        return new VerifyAddressResponse2();
    }

    /**
     * Create an instance of {@link ArrayOfVerificationAddress }
     * 
     */
    public ArrayOfVerificationAddress createArrayOfVerificationAddress() {
        return new ArrayOfVerificationAddress();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfint }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfint }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", name = "ArrayOfint")
    public JAXBElement<ArrayOfint> createArrayOfint(ArrayOfint value) {
        return new JAXBElement<ArrayOfint>(_ArrayOfint_QNAME, ArrayOfint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Double }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Float }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link QName }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Short }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Short }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Order }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Order }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Order")
    public JAXBElement<Order> createOrder(Order value) {
        return new JAXBElement<Order>(_Order_QNAME, Order.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Customer }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Customer }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Customer")
    public JAXBElement<Customer> createCustomer(Customer value) {
        return new JAXBElement<Customer>(_Customer_QNAME, Customer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerTypeEnum }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CustomerTypeEnum }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "CustomerTypeEnum")
    public JAXBElement<CustomerTypeEnum> createCustomerTypeEnum(CustomerTypeEnum value) {
        return new JAXBElement<CustomerTypeEnum>(_CustomerTypeEnum_QNAME, CustomerTypeEnum.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Address }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Address")
    public JAXBElement<Address> createAddress(Address value) {
        return new JAXBElement<Address>(_Address_QNAME, Address.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPayment }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfPayment }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ArrayOfPayment")
    public JAXBElement<ArrayOfPayment> createArrayOfPayment(ArrayOfPayment value) {
        return new JAXBElement<ArrayOfPayment>(_ArrayOfPayment_QNAME, ArrayOfPayment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Payment }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Payment }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Payment")
    public JAXBElement<Payment> createPayment(Payment value) {
        return new JAXBElement<Payment>(_Payment_QNAME, Payment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentTypeEnum }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PaymentTypeEnum }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "PaymentTypeEnum")
    public JAXBElement<PaymentTypeEnum> createPaymentTypeEnum(PaymentTypeEnum value) {
        return new JAXBElement<PaymentTypeEnum>(_PaymentTypeEnum_QNAME, PaymentTypeEnum.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPromo }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfPromo }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ArrayOfPromo")
    public JAXBElement<ArrayOfPromo> createArrayOfPromo(ArrayOfPromo value) {
        return new JAXBElement<ArrayOfPromo>(_ArrayOfPromo_QNAME, ArrayOfPromo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Promo }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Promo }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Promo")
    public JAXBElement<Promo> createPromo(Promo value) {
        return new JAXBElement<Promo>(_Promo_QNAME, Promo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCommerceItem }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfCommerceItem }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ArrayOfCommerceItem")
    public JAXBElement<ArrayOfCommerceItem> createArrayOfCommerceItem(ArrayOfCommerceItem value) {
        return new JAXBElement<ArrayOfCommerceItem>(_ArrayOfCommerceItem_QNAME, ArrayOfCommerceItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommerceItem }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CommerceItem }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "CommerceItem")
    public JAXBElement<CommerceItem> createCommerceItem(CommerceItem value) {
        return new JAXBElement<CommerceItem>(_CommerceItem_QNAME, CommerceItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfShipment }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfShipment }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ArrayOfShipment")
    public JAXBElement<ArrayOfShipment> createArrayOfShipment(ArrayOfShipment value) {
        return new JAXBElement<ArrayOfShipment>(_ArrayOfShipment_QNAME, ArrayOfShipment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Shipment }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Shipment }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Shipment")
    public JAXBElement<Shipment> createShipment(Shipment value) {
        return new JAXBElement<Shipment>(_Shipment_QNAME, Shipment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfShipmentItem }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfShipmentItem }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ArrayOfShipmentItem")
    public JAXBElement<ArrayOfShipmentItem> createArrayOfShipmentItem(ArrayOfShipmentItem value) {
        return new JAXBElement<ArrayOfShipmentItem>(_ArrayOfShipmentItem_QNAME, ArrayOfShipmentItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShipmentItem }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ShipmentItem }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ShipmentItem")
    public JAXBElement<ShipmentItem> createShipmentItem(ShipmentItem value) {
        return new JAXBElement<ShipmentItem>(_ShipmentItem_QNAME, ShipmentItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPromotion }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfPromotion }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ArrayOfPromotion")
    public JAXBElement<ArrayOfPromotion> createArrayOfPromotion(ArrayOfPromotion value) {
        return new JAXBElement<ArrayOfPromotion>(_ArrayOfPromotion_QNAME, ArrayOfPromotion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Promotion }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Promotion }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Promotion")
    public JAXBElement<Promotion> createPromotion(Promotion value) {
        return new JAXBElement<Promotion>(_Promotion_QNAME, Promotion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PromotionTypeEnum }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PromotionTypeEnum }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "PromotionTypeEnum")
    public JAXBElement<PromotionTypeEnum> createPromotionTypeEnum(PromotionTypeEnum value) {
        return new JAXBElement<PromotionTypeEnum>(_PromotionTypeEnum_QNAME, PromotionTypeEnum.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderSourceEnum }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link OrderSourceEnum }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "OrderSourceEnum")
    public JAXBElement<OrderSourceEnum> createOrderSourceEnum(OrderSourceEnum value) {
        return new JAXBElement<OrderSourceEnum>(_OrderSourceEnum_QNAME, OrderSourceEnum.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxRateSource }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link TaxRateSource }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "TaxRateSource")
    public JAXBElement<TaxRateSource> createTaxRateSource(TaxRateSource value) {
        return new JAXBElement<TaxRateSource>(_TaxRateSource_QNAME, TaxRateSource.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOrderResponse2 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateOrderResponse2 }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "CreateOrder_Response")
    public JAXBElement<CreateOrderResponse2> createCreateOrderResponse(CreateOrderResponse2 value) {
        return new JAXBElement<CreateOrderResponse2>(_CreateOrderResponse_QNAME, CreateOrderResponse2 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseStatus }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ResponseStatus }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ResponseStatus")
    public JAXBElement<ResponseStatus> createResponseStatus(ResponseStatus value) {
        return new JAXBElement<ResponseStatus>(_ResponseStatus_QNAME, ResponseStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCustomerResponse2 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateCustomerResponse2 }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "CreateCustomer_Response")
    public JAXBElement<CreateCustomerResponse2> createCreateCustomerResponse(CreateCustomerResponse2 value) {
        return new JAXBElement<CreateCustomerResponse2>(_CreateCustomerResponse_QNAME, CreateCustomerResponse2 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Rma }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Rma }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Rma")
    public JAXBElement<Rma> createRma(Rma value) {
        return new JAXBElement<Rma>(_Rma_QNAME, Rma.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRmaItem }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfRmaItem }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ArrayOfRmaItem")
    public JAXBElement<ArrayOfRmaItem> createArrayOfRmaItem(ArrayOfRmaItem value) {
        return new JAXBElement<ArrayOfRmaItem>(_ArrayOfRmaItem_QNAME, ArrayOfRmaItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RmaItem }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RmaItem }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "RmaItem")
    public JAXBElement<RmaItem> createRmaItem(RmaItem value) {
        return new JAXBElement<RmaItem>(_RmaItem_QNAME, RmaItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RmaReasonEnum }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RmaReasonEnum }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "RmaReasonEnum")
    public JAXBElement<RmaReasonEnum> createRmaReasonEnum(RmaReasonEnum value) {
        return new JAXBElement<RmaReasonEnum>(_RmaReasonEnum_QNAME, RmaReasonEnum.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateRmaResponse2 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateRmaResponse2 }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "CreateRma_Response")
    public JAXBElement<CreateRmaResponse2> createCreateRmaResponse(CreateRmaResponse2 value) {
        return new JAXBElement<CreateRmaResponse2>(_CreateRmaResponse_QNAME, CreateRmaResponse2 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoggerInfo }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoggerInfo }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "LoggerInfo")
    public JAXBElement<LoggerInfo> createLoggerInfo(LoggerInfo value) {
        return new JAXBElement<LoggerInfo>(_LoggerInfo_QNAME, LoggerInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOrderShipmentStatusResponse2 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetOrderShipmentStatusResponse2 }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "GetOrderShipmentStatus_Response")
    public JAXBElement<GetOrderShipmentStatusResponse2> createGetOrderShipmentStatusResponse(GetOrderShipmentStatusResponse2 value) {
        return new JAXBElement<GetOrderShipmentStatusResponse2>(_GetOrderShipmentStatusResponse_QNAME, GetOrderShipmentStatusResponse2 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfShippingGroupStatus }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfShippingGroupStatus }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ArrayOfShippingGroupStatus")
    public JAXBElement<ArrayOfShippingGroupStatus> createArrayOfShippingGroupStatus(ArrayOfShippingGroupStatus value) {
        return new JAXBElement<ArrayOfShippingGroupStatus>(_ArrayOfShippingGroupStatus_QNAME, ArrayOfShippingGroupStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShippingGroupStatus }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ShippingGroupStatus }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ShippingGroupStatus")
    public JAXBElement<ShippingGroupStatus> createShippingGroupStatus(ShippingGroupStatus value) {
        return new JAXBElement<ShippingGroupStatus>(_ShippingGroupStatus_QNAME, ShippingGroupStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOrderStatusResponse2 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetOrderStatusResponse2 }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "GetOrderStatus_Response")
    public JAXBElement<GetOrderStatusResponse2> createGetOrderStatusResponse(GetOrderStatusResponse2 value) {
        return new JAXBElement<GetOrderStatusResponse2>(_GetOrderStatusResponse_QNAME, GetOrderStatusResponse2 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetShipmentStatusResponse2 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetShipmentStatusResponse2 }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "GetShipmentStatus_Response")
    public JAXBElement<GetShipmentStatusResponse2> createGetShipmentStatusResponse(GetShipmentStatusResponse2 value) {
        return new JAXBElement<GetShipmentStatusResponse2>(_GetShipmentStatusResponse_QNAME, GetShipmentStatusResponse2 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStoreOrderResponse2 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetStoreOrderResponse2 }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "GetStoreOrder_Response")
    public JAXBElement<GetStoreOrderResponse2> createGetStoreOrderResponse(GetStoreOrderResponse2 value) {
        return new JAXBElement<GetStoreOrderResponse2>(_GetStoreOrderResponse_QNAME, GetStoreOrderResponse2 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StoreOrder }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link StoreOrder }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "StoreOrder")
    public JAXBElement<StoreOrder> createStoreOrder(StoreOrder value) {
        return new JAXBElement<StoreOrder>(_StoreOrder_QNAME, StoreOrder.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StoreLocation }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link StoreLocation }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "StoreLocation")
    public JAXBElement<StoreLocation> createStoreLocation(StoreLocation value) {
        return new JAXBElement<StoreLocation>(_StoreLocation_QNAME, StoreLocation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfStoreOrderItem }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfStoreOrderItem }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ArrayOfStoreOrderItem")
    public JAXBElement<ArrayOfStoreOrderItem> createArrayOfStoreOrderItem(ArrayOfStoreOrderItem value) {
        return new JAXBElement<ArrayOfStoreOrderItem>(_ArrayOfStoreOrderItem_QNAME, ArrayOfStoreOrderItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StoreOrderItem }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link StoreOrderItem }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "StoreOrderItem")
    public JAXBElement<StoreOrderItem> createStoreOrderItem(StoreOrderItem value) {
        return new JAXBElement<StoreOrderItem>(_StoreOrderItem_QNAME, StoreOrderItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRmaDeliveryItem }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfRmaDeliveryItem }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ArrayOfRmaDeliveryItem")
    public JAXBElement<ArrayOfRmaDeliveryItem> createArrayOfRmaDeliveryItem(ArrayOfRmaDeliveryItem value) {
        return new JAXBElement<ArrayOfRmaDeliveryItem>(_ArrayOfRmaDeliveryItem_QNAME, ArrayOfRmaDeliveryItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RmaDeliveryItem }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RmaDeliveryItem }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "RmaDeliveryItem")
    public JAXBElement<RmaDeliveryItem> createRmaDeliveryItem(RmaDeliveryItem value) {
        return new JAXBElement<RmaDeliveryItem>(_RmaDeliveryItem_QNAME, RmaDeliveryItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestRmaResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RequestRmaResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "RequestRma_Response")
    public JAXBElement<RequestRmaResponse> createRequestRmaResponse(RequestRmaResponse value) {
        return new JAXBElement<RequestRmaResponse>(_RequestRmaResponse_QNAME, RequestRmaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAtgServiceError }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfAtgServiceError }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ArrayOfAtgService_Error")
    public JAXBElement<ArrayOfAtgServiceError> createArrayOfAtgServiceError(ArrayOfAtgServiceError value) {
        return new JAXBElement<ArrayOfAtgServiceError>(_ArrayOfAtgServiceError_QNAME, ArrayOfAtgServiceError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AtgServiceError }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AtgServiceError }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "AtgService_Error")
    public JAXBElement<AtgServiceError> createAtgServiceError(AtgServiceError value) {
        return new JAXBElement<AtgServiceError>(_AtgServiceError_QNAME, AtgServiceError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerificationAddress }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link VerificationAddress }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "VerificationAddress")
    public JAXBElement<VerificationAddress> createVerificationAddress(VerificationAddress value) {
        return new JAXBElement<VerificationAddress>(_VerificationAddress_QNAME, VerificationAddress.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfVerificationAddress }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfVerificationAddress }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ArrayOfVerificationAddress")
    public JAXBElement<ArrayOfVerificationAddress> createArrayOfVerificationAddress(ArrayOfVerificationAddress value) {
        return new JAXBElement<ArrayOfVerificationAddress>(_ArrayOfVerificationAddress_QNAME, ArrayOfVerificationAddress.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceInfo }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ServiceInfo }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/", name = "Service_Info")
    public JAXBElement<ServiceInfo> createServiceInfo(ServiceInfo value) {
        return new JAXBElement<ServiceInfo>(_ServiceInfo_QNAME, ServiceInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyAddressResponse2 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link VerifyAddressResponse2 }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", name = "VerifyAddress_Response")
    public JAXBElement<VerifyAddressResponse2> createVerifyAddressResponse(VerifyAddressResponse2 value) {
        return new JAXBElement<VerifyAddressResponse2>(_VerifyAddressResponse_QNAME, VerifyAddressResponse2 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "message", scope = CreateTestException.class)
    public JAXBElement<String> createCreateTestExceptionMessage(String value) {
        return new JAXBElement<String>(_CreateTestExceptionMessage_QNAME, String.class, CreateTestException.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Order }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Order }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Order", scope = CreateOrder.class)
    public JAXBElement<Order> createCreateOrderOrder(Order value) {
        return new JAXBElement<Order>(_Order_QNAME, Order.class, CreateOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOrderResponse2 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateOrderResponse2 }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "CreateOrderResult", scope = CreateOrderResponse.class)
    public JAXBElement<CreateOrderResponse2> createCreateOrderResponseCreateOrderResult(CreateOrderResponse2 value) {
        return new JAXBElement<CreateOrderResponse2>(_CreateOrderResponseCreateOrderResult_QNAME, CreateOrderResponse2 .class, CreateOrderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Customer }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Customer }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "AtgCustomer", scope = CreateCustomer.class)
    public JAXBElement<Customer> createCreateCustomerAtgCustomer(Customer value) {
        return new JAXBElement<Customer>(_CreateCustomerAtgCustomer_QNAME, Customer.class, CreateCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCustomerResponse2 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateCustomerResponse2 }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "CreateCustomerResult", scope = CreateCustomerResponse.class)
    public JAXBElement<CreateCustomerResponse2> createCreateCustomerResponseCreateCustomerResult(CreateCustomerResponse2 value) {
        return new JAXBElement<CreateCustomerResponse2>(_CreateCustomerResponseCreateCustomerResult_QNAME, CreateCustomerResponse2 .class, CreateCustomerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Rma }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Rma }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Rma", scope = CreateRma.class)
    public JAXBElement<Rma> createCreateRmaRma(Rma value) {
        return new JAXBElement<Rma>(_Rma_QNAME, Rma.class, CreateRma.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateRmaResponse2 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateRmaResponse2 }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "CreateRmaResult", scope = CreateRmaResponse.class)
    public JAXBElement<CreateRmaResponse2> createCreateRmaResponseCreateRmaResult(CreateRmaResponse2 value) {
        return new JAXBElement<CreateRmaResponse2>(_CreateRmaResponseCreateRmaResult_QNAME, CreateRmaResponse2 .class, CreateRmaResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoggerInfo }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoggerInfo }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "GetLoggerInfoResult", scope = GetLoggerInfoResponse.class)
    public JAXBElement<LoggerInfo> createGetLoggerInfoResponseGetLoggerInfoResult(LoggerInfo value) {
        return new JAXBElement<LoggerInfo>(_GetLoggerInfoResponseGetLoggerInfoResult_QNAME, LoggerInfo.class, GetLoggerInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "AtgOrderId", scope = GetOrderShipmentStatus.class)
    public JAXBElement<String> createGetOrderShipmentStatusAtgOrderId(String value) {
        return new JAXBElement<String>(_GetOrderShipmentStatusAtgOrderId_QNAME, String.class, GetOrderShipmentStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOrderShipmentStatusResponse2 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetOrderShipmentStatusResponse2 }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "GetOrderShipmentStatusResult", scope = GetOrderShipmentStatusResponse.class)
    public JAXBElement<GetOrderShipmentStatusResponse2> createGetOrderShipmentStatusResponseGetOrderShipmentStatusResult(GetOrderShipmentStatusResponse2 value) {
        return new JAXBElement<GetOrderShipmentStatusResponse2>(_GetOrderShipmentStatusResponseGetOrderShipmentStatusResult_QNAME, GetOrderShipmentStatusResponse2 .class, GetOrderShipmentStatusResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "AtgOrderId", scope = GetOrderStatus.class)
    public JAXBElement<String> createGetOrderStatusAtgOrderId(String value) {
        return new JAXBElement<String>(_GetOrderShipmentStatusAtgOrderId_QNAME, String.class, GetOrderStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOrderStatusResponse2 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetOrderStatusResponse2 }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "GetOrderStatusResult", scope = GetOrderStatusResponse.class)
    public JAXBElement<GetOrderStatusResponse2> createGetOrderStatusResponseGetOrderStatusResult(GetOrderStatusResponse2 value) {
        return new JAXBElement<GetOrderStatusResponse2>(_GetOrderStatusResponseGetOrderStatusResult_QNAME, GetOrderStatusResponse2 .class, GetOrderStatusResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceInfo }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ServiceInfo }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "GetServiceInfoResult", scope = GetServiceInfoResponse.class)
    public JAXBElement<ServiceInfo> createGetServiceInfoResponseGetServiceInfoResult(ServiceInfo value) {
        return new JAXBElement<ServiceInfo>(_GetServiceInfoResponseGetServiceInfoResult_QNAME, ServiceInfo.class, GetServiceInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "AtgShippingGroupId", scope = GetShipmentStatus.class)
    public JAXBElement<String> createGetShipmentStatusAtgShippingGroupId(String value) {
        return new JAXBElement<String>(_GetShipmentStatusAtgShippingGroupId_QNAME, String.class, GetShipmentStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetShipmentStatusResponse2 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetShipmentStatusResponse2 }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "GetShipmentStatusResult", scope = GetShipmentStatusResponse.class)
    public JAXBElement<GetShipmentStatusResponse2> createGetShipmentStatusResponseGetShipmentStatusResult(GetShipmentStatusResponse2 value) {
        return new JAXBElement<GetShipmentStatusResponse2>(_GetShipmentStatusResponseGetShipmentStatusResult_QNAME, GetShipmentStatusResponse2 .class, GetShipmentStatusResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ReceiptNumber", scope = GetStoreOrder.class)
    public JAXBElement<String> createGetStoreOrderReceiptNumber(String value) {
        return new JAXBElement<String>(_GetStoreOrderReceiptNumber_QNAME, String.class, GetStoreOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "PurchaseDate", scope = GetStoreOrder.class)
    public JAXBElement<String> createGetStoreOrderPurchaseDate(String value) {
        return new JAXBElement<String>(_GetStoreOrderPurchaseDate_QNAME, String.class, GetStoreOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStoreOrderResponse2 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetStoreOrderResponse2 }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "GetStoreOrderResult", scope = GetStoreOrderResponse.class)
    public JAXBElement<GetStoreOrderResponse2> createGetStoreOrderResponseGetStoreOrderResult(GetStoreOrderResponse2 value) {
        return new JAXBElement<GetStoreOrderResponse2>(_GetStoreOrderResponseGetStoreOrderResult_QNAME, GetStoreOrderResponse2 .class, GetStoreOrderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "atgOrderId", scope = ProcessAccertifyResolution.class)
    public JAXBElement<String> createProcessAccertifyResolutionAtgOrderId(String value) {
        return new JAXBElement<String>(_ProcessAccertifyResolutionAtgOrderId_QNAME, String.class, ProcessAccertifyResolution.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "actionCode", scope = ProcessAccertifyResolution.class)
    public JAXBElement<String> createProcessAccertifyResolutionActionCode(String value) {
        return new JAXBElement<String>(_ProcessAccertifyResolutionActionCode_QNAME, String.class, ProcessAccertifyResolution.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "recomendation", scope = ProcessAccertifyResolution.class)
    public JAXBElement<String> createProcessAccertifyResolutionRecomendation(String value) {
        return new JAXBElement<String>(_ProcessAccertifyResolutionRecomendation_QNAME, String.class, ProcessAccertifyResolution.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "remarks", scope = ProcessAccertifyResolution.class)
    public JAXBElement<String> createProcessAccertifyResolutionRemarks(String value) {
        return new JAXBElement<String>(_ProcessAccertifyResolutionRemarks_QNAME, String.class, ProcessAccertifyResolution.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRmaDeliveryItem }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfRmaDeliveryItem }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Items", scope = RequestAtgRma.class)
    public JAXBElement<ArrayOfRmaDeliveryItem> createRequestAtgRmaItems(ArrayOfRmaDeliveryItem value) {
        return new JAXBElement<ArrayOfRmaDeliveryItem>(_RequestAtgRmaItems_QNAME, ArrayOfRmaDeliveryItem.class, RequestAtgRma.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestRmaResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RequestRmaResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "RequestAtgRmaResult", scope = RequestAtgRmaResponse.class)
    public JAXBElement<RequestRmaResponse> createRequestAtgRmaResponseRequestAtgRmaResult(RequestRmaResponse value) {
        return new JAXBElement<RequestRmaResponse>(_RequestAtgRmaResponseRequestAtgRmaResult_QNAME, RequestRmaResponse.class, RequestAtgRmaResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoggerInfo }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoggerInfo }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ToggleLoggerResult", scope = ToggleLoggerResponse.class)
    public JAXBElement<LoggerInfo> createToggleLoggerResponseToggleLoggerResult(LoggerInfo value) {
        return new JAXBElement<LoggerInfo>(_ToggleLoggerResponseToggleLoggerResult_QNAME, LoggerInfo.class, ToggleLoggerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerificationAddress }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link VerificationAddress }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "address", scope = VerifyAddress.class)
    public JAXBElement<VerificationAddress> createVerifyAddressAddress(VerificationAddress value) {
        return new JAXBElement<VerificationAddress>(_VerifyAddressAddress_QNAME, VerificationAddress.class, VerifyAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyAddressResponse2 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link VerifyAddressResponse2 }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "VerifyAddressResult", scope = VerifyAddressResponse.class)
    public JAXBElement<VerifyAddressResponse2> createVerifyAddressResponseVerifyAddressResult(VerifyAddressResponse2 value) {
        return new JAXBElement<VerifyAddressResponse2>(_VerifyAddressResponseVerifyAddressResult_QNAME, VerifyAddressResponse2 .class, VerifyAddressResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAtgServiceError }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfAtgServiceError }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", name = "Errors", scope = VerifyAddressResponse2 .class)
    public JAXBElement<ArrayOfAtgServiceError> createVerifyAddressResponse2Errors(ArrayOfAtgServiceError value) {
        return new JAXBElement<ArrayOfAtgServiceError>(_VerifyAddressResponse2Errors_QNAME, ArrayOfAtgServiceError.class, VerifyAddressResponse2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", name = "Message", scope = VerifyAddressResponse2 .class)
    public JAXBElement<String> createVerifyAddressResponse2Message(String value) {
        return new JAXBElement<String>(_VerifyAddressResponse2Message_QNAME, String.class, VerifyAddressResponse2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfVerificationAddress }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfVerificationAddress }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", name = "SuggestedAddresses", scope = VerifyAddressResponse2 .class)
    public JAXBElement<ArrayOfVerificationAddress> createVerifyAddressResponse2SuggestedAddresses(ArrayOfVerificationAddress value) {
        return new JAXBElement<ArrayOfVerificationAddress>(_VerifyAddressResponse2SuggestedAddresses_QNAME, ArrayOfVerificationAddress.class, VerifyAddressResponse2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Line2", scope = VerificationAddress.class)
    public JAXBElement<String> createVerificationAddressLine2(String value) {
        return new JAXBElement<String>(_VerificationAddressLine2_QNAME, String.class, VerificationAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAtgServiceError }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfAtgServiceError }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Errors", scope = RequestRmaResponse.class)
    public JAXBElement<ArrayOfAtgServiceError> createRequestRmaResponseErrors(ArrayOfAtgServiceError value) {
        return new JAXBElement<ArrayOfAtgServiceError>(_RequestRmaResponseErrors_QNAME, ArrayOfAtgServiceError.class, RequestRmaResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfint }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfint }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "RmaIds", scope = RequestRmaResponse.class)
    public JAXBElement<ArrayOfint> createRequestRmaResponseRmaIds(ArrayOfint value) {
        return new JAXBElement<ArrayOfint>(_RequestRmaResponseRmaIds_QNAME, ArrayOfint.class, RequestRmaResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Address2", scope = StoreLocation.class)
    public JAXBElement<String> createStoreLocationAddress2(String value) {
        return new JAXBElement<String>(_StoreLocationAddress2_QNAME, String.class, StoreLocation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Address }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Address", scope = StoreOrder.class)
    public JAXBElement<Address> createStoreOrderAddress(Address value) {
        return new JAXBElement<Address>(_Address_QNAME, Address.class, StoreOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Message", scope = GetStoreOrderResponse2 .class)
    public JAXBElement<String> createGetStoreOrderResponse2Message(String value) {
        return new JAXBElement<String>(_GetStoreOrderResponse2Message_QNAME, String.class, GetStoreOrderResponse2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StoreOrder }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link StoreOrder }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Order", scope = GetStoreOrderResponse2 .class)
    public JAXBElement<StoreOrder> createGetStoreOrderResponse2Order(StoreOrder value) {
        return new JAXBElement<StoreOrder>(_Order_QNAME, StoreOrder.class, GetStoreOrderResponse2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Message", scope = GetShipmentStatusResponse2 .class)
    public JAXBElement<String> createGetShipmentStatusResponse2Message(String value) {
        return new JAXBElement<String>(_GetStoreOrderResponse2Message_QNAME, String.class, GetShipmentStatusResponse2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "StatusText", scope = GetShipmentStatusResponse2 .class)
    public JAXBElement<String> createGetShipmentStatusResponse2StatusText(String value) {
        return new JAXBElement<String>(_GetShipmentStatusResponse2StatusText_QNAME, String.class, GetShipmentStatusResponse2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Message", scope = GetOrderStatusResponse2 .class)
    public JAXBElement<String> createGetOrderStatusResponse2Message(String value) {
        return new JAXBElement<String>(_GetStoreOrderResponse2Message_QNAME, String.class, GetOrderStatusResponse2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "StatusText", scope = GetOrderStatusResponse2 .class)
    public JAXBElement<String> createGetOrderStatusResponse2StatusText(String value) {
        return new JAXBElement<String>(_GetShipmentStatusResponse2StatusText_QNAME, String.class, GetOrderStatusResponse2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Message", scope = GetOrderShipmentStatusResponse2 .class)
    public JAXBElement<String> createGetOrderShipmentStatusResponse2Message(String value) {
        return new JAXBElement<String>(_GetStoreOrderResponse2Message_QNAME, String.class, GetOrderShipmentStatusResponse2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Message", scope = CreateRmaResponse2 .class)
    public JAXBElement<String> createCreateRmaResponse2Message(String value) {
        return new JAXBElement<String>(_GetStoreOrderResponse2Message_QNAME, String.class, CreateRmaResponse2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Status", scope = RmaItem.class)
    public JAXBElement<String> createRmaItemStatus(String value) {
        return new JAXBElement<String>(_RmaItemStatus_QNAME, String.class, RmaItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Message", scope = CreateCustomerResponse2 .class)
    public JAXBElement<String> createCreateCustomerResponse2Message(String value) {
        return new JAXBElement<String>(_GetStoreOrderResponse2Message_QNAME, String.class, CreateCustomerResponse2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Message", scope = CreateOrderResponse2 .class)
    public JAXBElement<String> createCreateOrderResponse2Message(String value) {
        return new JAXBElement<String>(_GetStoreOrderResponse2Message_QNAME, String.class, CreateOrderResponse2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Code", scope = Promotion.class)
    public JAXBElement<String> createPromotionCode(String value) {
        return new JAXBElement<String>(_PromotionCode_QNAME, String.class, Promotion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "AtgParentCommerceItemId", scope = ShipmentItem.class)
    public JAXBElement<String> createShipmentItemAtgParentCommerceItemId(String value) {
        return new JAXBElement<String>(_ShipmentItemAtgParentCommerceItemId_QNAME, String.class, ShipmentItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "PromotionCode", scope = ShipmentItem.class)
    public JAXBElement<String> createShipmentItemPromotionCode(String value) {
        return new JAXBElement<String>(_ShipmentItemPromotionCode_QNAME, String.class, ShipmentItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "GWPCommerceItemId", scope = ShipmentItem.class)
    public JAXBElement<String> createShipmentItemGWPCommerceItemId(String value) {
        return new JAXBElement<String>(_ShipmentItemGWPCommerceItemId_QNAME, String.class, ShipmentItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "AvalaraCode", scope = ShipmentItem.class)
    public JAXBElement<String> createShipmentItemAvalaraCode(String value) {
        return new JAXBElement<String>(_ShipmentItemAvalaraCode_QNAME, String.class, ShipmentItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ShippingPromotionCode", scope = Shipment.class)
    public JAXBElement<String> createShipmentShippingPromotionCode(String value) {
        return new JAXBElement<String>(_ShipmentShippingPromotionCode_QNAME, String.class, Shipment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ShippingDate", scope = Shipment.class)
    public JAXBElement<String> createShipmentShippingDate(String value) {
        return new JAXBElement<String>(_ShipmentShippingDate_QNAME, String.class, Shipment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "SpecialInstructions", scope = Shipment.class)
    public JAXBElement<String> createShipmentSpecialInstructions(String value) {
        return new JAXBElement<String>(_ShipmentSpecialInstructions_QNAME, String.class, Shipment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ExportCity", scope = Shipment.class)
    public JAXBElement<String> createShipmentExportCity(String value) {
        return new JAXBElement<String>(_ShipmentExportCity_QNAME, String.class, Shipment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ExportCountry", scope = Shipment.class)
    public JAXBElement<String> createShipmentExportCountry(String value) {
        return new JAXBElement<String>(_ShipmentExportCountry_QNAME, String.class, Shipment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Address }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "AlternatePickupAddress", scope = Shipment.class)
    public JAXBElement<Address> createShipmentAlternatePickupAddress(Address value) {
        return new JAXBElement<Address>(_ShipmentAlternatePickupAddress_QNAME, Address.class, Shipment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "ShippingAvalaraCode", scope = Shipment.class)
    public JAXBElement<String> createShipmentShippingAvalaraCode(String value) {
        return new JAXBElement<String>(_ShipmentShippingAvalaraCode_QNAME, String.class, Shipment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "AccountEmail", scope = Payment.class)
    public JAXBElement<String> createPaymentAccountEmail(String value) {
        return new JAXBElement<String>(_PaymentAccountEmail_QNAME, String.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "AccountFirst6", scope = Payment.class)
    public JAXBElement<String> createPaymentAccountFirst6(String value) {
        return new JAXBElement<String>(_PaymentAccountFirst6_QNAME, String.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "AccountLast4", scope = Payment.class)
    public JAXBElement<String> createPaymentAccountLast4(String value) {
        return new JAXBElement<String>(_PaymentAccountLast4_QNAME, String.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "AuthoriationCode", scope = Payment.class)
    public JAXBElement<String> createPaymentAuthoriationCode(String value) {
        return new JAXBElement<String>(_PaymentAuthoriationCode_QNAME, String.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "PassedAVS", scope = Payment.class)
    public JAXBElement<Boolean> createPaymentPassedAVS(Boolean value) {
        return new JAXBElement<Boolean>(_PaymentPassedAVS_QNAME, Boolean.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "PassedCVV", scope = Payment.class)
    public JAXBElement<Boolean> createPaymentPassedCVV(Boolean value) {
        return new JAXBElement<Boolean>(_PaymentPassedCVV_QNAME, Boolean.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "IsVerified", scope = Payment.class)
    public JAXBElement<Boolean> createPaymentIsVerified(Boolean value) {
        return new JAXBElement<Boolean>(_PaymentIsVerified_QNAME, Boolean.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "PromoCode", scope = Payment.class)
    public JAXBElement<String> createPaymentPromoCode(String value) {
        return new JAXBElement<String>(_PaymentPromoCode_QNAME, String.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPromo }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfPromo }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Promos", scope = Payment.class)
    public JAXBElement<ArrayOfPromo> createPaymentPromos(ArrayOfPromo value) {
        return new JAXBElement<ArrayOfPromo>(_PaymentPromos_QNAME, ArrayOfPromo.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "FirstName", scope = Address.class)
    public JAXBElement<String> createAddressFirstName(String value) {
        return new JAXBElement<String>(_AddressFirstName_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "LastName", scope = Address.class)
    public JAXBElement<String> createAddressLastName(String value) {
        return new JAXBElement<String>(_AddressLastName_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "CompanyName", scope = Address.class)
    public JAXBElement<String> createAddressCompanyName(String value) {
        return new JAXBElement<String>(_AddressCompanyName_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Address1", scope = Address.class)
    public JAXBElement<String> createAddressAddress1(String value) {
        return new JAXBElement<String>(_AddressAddress1_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Address2", scope = Address.class)
    public JAXBElement<String> createAddressAddress2(String value) {
        return new JAXBElement<String>(_StoreLocationAddress2_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "City", scope = Address.class)
    public JAXBElement<String> createAddressCity(String value) {
        return new JAXBElement<String>(_AddressCity_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "State", scope = Address.class)
    public JAXBElement<String> createAddressState(String value) {
        return new JAXBElement<String>(_AddressState_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Zip", scope = Address.class)
    public JAXBElement<String> createAddressZip(String value) {
        return new JAXBElement<String>(_AddressZip_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "FipsState", scope = Address.class)
    public JAXBElement<String> createAddressFipsState(String value) {
        return new JAXBElement<String>(_AddressFipsState_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "FipsCounty", scope = Address.class)
    public JAXBElement<String> createAddressFipsCounty(String value) {
        return new JAXBElement<String>(_AddressFipsCounty_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Email", scope = Address.class)
    public JAXBElement<String> createAddressEmail(String value) {
        return new JAXBElement<String>(_AddressEmail_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Phone", scope = Address.class)
    public JAXBElement<String> createAddressPhone(String value) {
        return new JAXBElement<String>(_AddressPhone_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "AltPhone", scope = Address.class)
    public JAXBElement<String> createAddressAltPhone(String value) {
        return new JAXBElement<String>(_AddressAltPhone_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "MobilePhone", scope = Address.class)
    public JAXBElement<String> createAddressMobilePhone(String value) {
        return new JAXBElement<String>(_AddressMobilePhone_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Region", scope = Address.class)
    public JAXBElement<String> createAddressRegion(String value) {
        return new JAXBElement<String>(_AddressRegion_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Country", scope = Address.class)
    public JAXBElement<String> createAddressCountry(String value) {
        return new JAXBElement<String>(_AddressCountry_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "FipsCity", scope = Address.class)
    public JAXBElement<String> createAddressFipsCity(String value) {
        return new JAXBElement<String>(_AddressFipsCity_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "CompanyName", scope = Customer.class)
    public JAXBElement<String> createCustomerCompanyName(String value) {
        return new JAXBElement<String>(_AddressCompanyName_QNAME, String.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Address }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Address", scope = Customer.class)
    public JAXBElement<Address> createCustomerAddress(Address value) {
        return new JAXBElement<Address>(_Address_QNAME, Address.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "PromotionCode", scope = Order.class)
    public JAXBElement<String> createOrderPromotionCode(String value) {
        return new JAXBElement<String>(_ShipmentItemPromotionCode_QNAME, String.class, Order.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "IpAddress", scope = Order.class)
    public JAXBElement<String> createOrderIpAddress(String value) {
        return new JAXBElement<String>(_OrderIpAddress_QNAME, String.class, Order.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPromotion }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfPromotion }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Promotions", scope = Order.class)
    public JAXBElement<ArrayOfPromotion> createOrderPromotions(ArrayOfPromotion value) {
        return new JAXBElement<ArrayOfPromotion>(_OrderPromotions_QNAME, ArrayOfPromotion.class, Order.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderSourceEnum }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link OrderSourceEnum }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "Source", scope = Order.class)
    public JAXBElement<OrderSourceEnum> createOrderSource(OrderSourceEnum value) {
        return new JAXBElement<OrderSourceEnum>(_OrderSource_QNAME, OrderSourceEnum.class, Order.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://brandsmartusa.com/Services/AtgWebBroker", name = "SourceRef", scope = Order.class)
    public JAXBElement<String> createOrderSourceRef(String value) {
        return new JAXBElement<String>(_OrderSourceRef_QNAME, String.class, Order.class, value);
    }

}
