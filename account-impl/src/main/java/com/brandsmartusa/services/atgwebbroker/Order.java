
package com.brandsmartusa.services.atgwebbroker;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Order complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Order"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="AtgOrderId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Customer" type="{http://brandsmartusa.com/Services/AtgWebBroker}Customer"/&gt;
 *         &lt;element name="BillingAddress" type="{http://brandsmartusa.com/Services/AtgWebBroker}Address"/&gt;
 *         &lt;element name="Sales" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="Tax" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="Shipping" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="PromotionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PromotionAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="IpAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AffiliateId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="AccertifyResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Payments" type="{http://brandsmartusa.com/Services/AtgWebBroker}ArrayOfPayment"/&gt;
 *         &lt;element name="Shipments" type="{http://brandsmartusa.com/Services/AtgWebBroker}ArrayOfShipment"/&gt;
 *         &lt;element name="Promotions" type="{http://brandsmartusa.com/Services/AtgWebBroker}ArrayOfPromotion" minOccurs="0"/&gt;
 *         &lt;element name="Source" type="{http://brandsmartusa.com/Services/AtgWebBroker}OrderSourceEnum" minOccurs="0"/&gt;
 *         &lt;element name="SourceRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TaxRateSource" type="{http://brandsmartusa.com/Services/AtgWebBroker}TaxRateSource" minOccurs="0"/&gt;
 *         &lt;element name="AvalaraSubmitted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Order", propOrder = {
    "id",
    "atgOrderId",
    "customer",
    "billingAddress",
    "sales",
    "tax",
    "shipping",
    "promotionCode",
    "promotionAmount",
    "ipAddress",
    "affiliateId",
    "accertifyResponseCode",
    "payments",
    "shipments",
    "promotions",
    "source",
    "sourceRef",
    "taxRateSource",
    "avalaraSubmitted"
})
public class Order {

    @XmlElement(name = "Id")
    protected int id;
    @XmlElement(name = "AtgOrderId", required = true, nillable = true)
    protected String atgOrderId;
    @XmlElement(name = "Customer", required = true, nillable = true)
    protected Customer customer;
    @XmlElement(name = "BillingAddress", required = true, nillable = true)
    protected Address billingAddress;
    @XmlElement(name = "Sales", required = true)
    protected BigDecimal sales;
    @XmlElement(name = "Tax", required = true)
    protected BigDecimal tax;
    @XmlElement(name = "Shipping", required = true)
    protected BigDecimal shipping;
    @XmlElementRef(name = "PromotionCode", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<String> promotionCode;
    @XmlElement(name = "PromotionAmount")
    protected BigDecimal promotionAmount;
    @XmlElementRef(name = "IpAddress", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ipAddress;
    @XmlElement(name = "AffiliateId")
    protected Integer affiliateId;
    @XmlElement(name = "AccertifyResponseCode", required = true, nillable = true)
    protected String accertifyResponseCode;
    @XmlElement(name = "Payments", required = true, nillable = true)
    protected ArrayOfPayment payments;
    @XmlElement(name = "Shipments", required = true, nillable = true)
    protected ArrayOfShipment shipments;
    @XmlElementRef(name = "Promotions", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfPromotion> promotions;
    @XmlElementRef(name = "Source", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<OrderSourceEnum> source;
    @XmlElementRef(name = "SourceRef", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sourceRef;
    @XmlElement(name = "TaxRateSource")
    @XmlSchemaType(name = "string")
    protected TaxRateSource taxRateSource;
    @XmlElement(name = "AvalaraSubmitted")
    protected Boolean avalaraSubmitted;

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the atgOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtgOrderId() {
        return atgOrderId;
    }

    /**
     * Sets the value of the atgOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtgOrderId(String value) {
        this.atgOrderId = value;
    }

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link Customer }
     *     
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Customer }
     *     
     */
    public void setCustomer(Customer value) {
        this.customer = value;
    }

    /**
     * Gets the value of the billingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getBillingAddress() {
        return billingAddress;
    }

    /**
     * Sets the value of the billingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setBillingAddress(Address value) {
        this.billingAddress = value;
    }

    /**
     * Gets the value of the sales property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSales() {
        return sales;
    }

    /**
     * Sets the value of the sales property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSales(BigDecimal value) {
        this.sales = value;
    }

    /**
     * Gets the value of the tax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTax() {
        return tax;
    }

    /**
     * Sets the value of the tax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTax(BigDecimal value) {
        this.tax = value;
    }

    /**
     * Gets the value of the shipping property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getShipping() {
        return shipping;
    }

    /**
     * Sets the value of the shipping property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setShipping(BigDecimal value) {
        this.shipping = value;
    }

    /**
     * Gets the value of the promotionCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPromotionCode() {
        return promotionCode;
    }

    /**
     * Sets the value of the promotionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPromotionCode(JAXBElement<String> value) {
        this.promotionCode = value;
    }

    /**
     * Gets the value of the promotionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPromotionAmount() {
        return promotionAmount;
    }

    /**
     * Sets the value of the promotionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPromotionAmount(BigDecimal value) {
        this.promotionAmount = value;
    }

    /**
     * Gets the value of the ipAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIpAddress() {
        return ipAddress;
    }

    /**
     * Sets the value of the ipAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIpAddress(JAXBElement<String> value) {
        this.ipAddress = value;
    }

    /**
     * Gets the value of the affiliateId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAffiliateId() {
        return affiliateId;
    }

    /**
     * Sets the value of the affiliateId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAffiliateId(Integer value) {
        this.affiliateId = value;
    }

    /**
     * Gets the value of the accertifyResponseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccertifyResponseCode() {
        return accertifyResponseCode;
    }

    /**
     * Sets the value of the accertifyResponseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccertifyResponseCode(String value) {
        this.accertifyResponseCode = value;
    }

    /**
     * Gets the value of the payments property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPayment }
     *     
     */
    public ArrayOfPayment getPayments() {
        return payments;
    }

    /**
     * Sets the value of the payments property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPayment }
     *     
     */
    public void setPayments(ArrayOfPayment value) {
        this.payments = value;
    }

    /**
     * Gets the value of the shipments property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfShipment }
     *     
     */
    public ArrayOfShipment getShipments() {
        return shipments;
    }

    /**
     * Sets the value of the shipments property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfShipment }
     *     
     */
    public void setShipments(ArrayOfShipment value) {
        this.shipments = value;
    }

    /**
     * Gets the value of the promotions property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPromotion }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPromotion> getPromotions() {
        return promotions;
    }

    /**
     * Sets the value of the promotions property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPromotion }{@code >}
     *     
     */
    public void setPromotions(JAXBElement<ArrayOfPromotion> value) {
        this.promotions = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OrderSourceEnum }{@code >}
     *     
     */
    public JAXBElement<OrderSourceEnum> getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OrderSourceEnum }{@code >}
     *     
     */
    public void setSource(JAXBElement<OrderSourceEnum> value) {
        this.source = value;
    }

    /**
     * Gets the value of the sourceRef property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSourceRef() {
        return sourceRef;
    }

    /**
     * Sets the value of the sourceRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSourceRef(JAXBElement<String> value) {
        this.sourceRef = value;
    }

    /**
     * Gets the value of the taxRateSource property.
     * 
     * @return
     *     possible object is
     *     {@link TaxRateSource }
     *     
     */
    public TaxRateSource getTaxRateSource() {
        return taxRateSource;
    }

    /**
     * Sets the value of the taxRateSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxRateSource }
     *     
     */
    public void setTaxRateSource(TaxRateSource value) {
        this.taxRateSource = value;
    }

    /**
     * Gets the value of the avalaraSubmitted property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAvalaraSubmitted() {
        return avalaraSubmitted;
    }

    /**
     * Sets the value of the avalaraSubmitted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAvalaraSubmitted(Boolean value) {
        this.avalaraSubmitted = value;
    }

}
