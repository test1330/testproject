
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderSourceEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrderSourceEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ATG"/&gt;
 *     &lt;enumeration value="eBay"/&gt;
 *     &lt;enumeration value="GoogleExpress"/&gt;
 *     &lt;enumeration value="ManualEntry"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "OrderSourceEnum")
@XmlEnum
public enum OrderSourceEnum {

    ATG("ATG"),
    @XmlEnumValue("eBay")
    E_BAY("eBay"),
    @XmlEnumValue("GoogleExpress")
    GOOGLE_EXPRESS("GoogleExpress"),
    @XmlEnumValue("ManualEntry")
    MANUAL_ENTRY("ManualEntry");
    private final String value;

    OrderSourceEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OrderSourceEnum fromValue(String v) {
        for (OrderSourceEnum c: OrderSourceEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
