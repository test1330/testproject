
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentTypeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PaymentTypeEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Visa"/&gt;
 *     &lt;enumeration value="MasterCard"/&gt;
 *     &lt;enumeration value="Discover"/&gt;
 *     &lt;enumeration value="AmericanExpress"/&gt;
 *     &lt;enumeration value="PayPal"/&gt;
 *     &lt;enumeration value="GiftCard"/&gt;
 *     &lt;enumeration value="BrandsMartCard"/&gt;
 *     &lt;enumeration value="WireTransfer"/&gt;
 *     &lt;enumeration value="Assurant"/&gt;
 *     &lt;enumeration value="WarranTech"/&gt;
 *     &lt;enumeration value="FlexShopper"/&gt;
 *     &lt;enumeration value="Fortiva"/&gt;
 *     &lt;enumeration value="GoogleExpress"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PaymentTypeEnum")
@XmlEnum
public enum PaymentTypeEnum {

    @XmlEnumValue("Visa")
    VISA("Visa"),
    @XmlEnumValue("MasterCard")
    MASTER_CARD("MasterCard"),
    @XmlEnumValue("Discover")
    DISCOVER("Discover"),
    @XmlEnumValue("AmericanExpress")
    AMERICAN_EXPRESS("AmericanExpress"),
    @XmlEnumValue("PayPal")
    PAY_PAL("PayPal"),
    @XmlEnumValue("GiftCard")
    GIFT_CARD("GiftCard"),
    @XmlEnumValue("BrandsMartCard")
    BRANDS_MART_CARD("BrandsMartCard"),
    @XmlEnumValue("WireTransfer")
    WIRE_TRANSFER("WireTransfer"),
    @XmlEnumValue("Assurant")
    ASSURANT("Assurant"),
    @XmlEnumValue("WarranTech")
    WARRAN_TECH("WarranTech"),
    @XmlEnumValue("FlexShopper")
    FLEX_SHOPPER("FlexShopper"),
    @XmlEnumValue("Fortiva")
    FORTIVA("Fortiva"),
    @XmlEnumValue("GoogleExpress")
    GOOGLE_EXPRESS("GoogleExpress");
    private final String value;

    PaymentTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PaymentTypeEnum fromValue(String v) {
        for (PaymentTypeEnum c: PaymentTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
