
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PromotionTypeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PromotionTypeEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Order"/&gt;
 *     &lt;enumeration value="Item"/&gt;
 *     &lt;enumeration value="Shipping"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PromotionTypeEnum")
@XmlEnum
public enum PromotionTypeEnum {

    @XmlEnumValue("Order")
    ORDER("Order"),
    @XmlEnumValue("Item")
    ITEM("Item"),
    @XmlEnumValue("Shipping")
    SHIPPING("Shipping");
    private final String value;

    PromotionTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PromotionTypeEnum fromValue(String v) {
        for (PromotionTypeEnum c: PromotionTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
