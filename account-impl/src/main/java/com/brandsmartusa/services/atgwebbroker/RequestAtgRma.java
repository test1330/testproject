
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Items" type="{http://brandsmartusa.com/Services/AtgWebBroker}ArrayOfRmaDeliveryItem" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "items"
})
@XmlRootElement(name = "RequestAtgRma")
public class RequestAtgRma {

    @XmlElementRef(name = "Items", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfRmaDeliveryItem> items;

    /**
     * Gets the value of the items property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfRmaDeliveryItem }{@code >}
     *     
     */
    public JAXBElement<ArrayOfRmaDeliveryItem> getItems() {
        return items;
    }

    /**
     * Sets the value of the items property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfRmaDeliveryItem }{@code >}
     *     
     */
    public void setItems(JAXBElement<ArrayOfRmaDeliveryItem> value) {
        this.items = value;
    }

}
