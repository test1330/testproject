
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RequestAtgRmaResult" type="{http://brandsmartusa.com/Services/AtgWebBroker}RequestRma_Response" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestAtgRmaResult"
})
@XmlRootElement(name = "RequestAtgRmaResponse")
public class RequestAtgRmaResponse {

    @XmlElementRef(name = "RequestAtgRmaResult", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<RequestRmaResponse> requestAtgRmaResult;

    /**
     * Gets the value of the requestAtgRmaResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link RequestRmaResponse }{@code >}
     *     
     */
    public JAXBElement<RequestRmaResponse> getRequestAtgRmaResult() {
        return requestAtgRmaResult;
    }

    /**
     * Sets the value of the requestAtgRmaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link RequestRmaResponse }{@code >}
     *     
     */
    public void setRequestAtgRmaResult(JAXBElement<RequestRmaResponse> value) {
        this.requestAtgRmaResult = value;
    }

}
