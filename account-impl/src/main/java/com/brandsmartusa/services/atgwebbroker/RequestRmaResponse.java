
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RequestRma_Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestRma_Response"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Type" type="{http://brandsmartusa.com/Services/AtgWebBroker}ResponseStatus"/&gt;
 *         &lt;element name="Errors" type="{http://brandsmartusa.com/Services/AtgWebBroker}ArrayOfAtgService_Error" minOccurs="0"/&gt;
 *         &lt;element name="RmaIds" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfint" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestRma_Response", propOrder = {
    "type",
    "errors",
    "rmaIds"
})
public class RequestRmaResponse {

    @XmlElement(name = "Type", required = true)
    @XmlSchemaType(name = "string")
    protected ResponseStatus type;
    @XmlElementRef(name = "Errors", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfAtgServiceError> errors;
    @XmlElementRef(name = "RmaIds", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfint> rmaIds;

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseStatus }
     *     
     */
    public ResponseStatus getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseStatus }
     *     
     */
    public void setType(ResponseStatus value) {
        this.type = value;
    }

    /**
     * Gets the value of the errors property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAtgServiceError }{@code >}
     *     
     */
    public JAXBElement<ArrayOfAtgServiceError> getErrors() {
        return errors;
    }

    /**
     * Sets the value of the errors property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAtgServiceError }{@code >}
     *     
     */
    public void setErrors(JAXBElement<ArrayOfAtgServiceError> value) {
        this.errors = value;
    }

    /**
     * Gets the value of the rmaIds property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfint }{@code >}
     *     
     */
    public JAXBElement<ArrayOfint> getRmaIds() {
        return rmaIds;
    }

    /**
     * Sets the value of the rmaIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfint }{@code >}
     *     
     */
    public void setRmaIds(JAXBElement<ArrayOfint> value) {
        this.rmaIds = value;
    }

}
