
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Rma complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Rma"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AtgOrderId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Items" type="{http://brandsmartusa.com/Services/AtgWebBroker}ArrayOfRmaItem"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Rma", propOrder = {
    "atgOrderId",
    "items"
})
public class Rma {

    @XmlElement(name = "AtgOrderId", required = true, nillable = true)
    protected String atgOrderId;
    @XmlElement(name = "Items", required = true, nillable = true)
    protected ArrayOfRmaItem items;

    /**
     * Gets the value of the atgOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtgOrderId() {
        return atgOrderId;
    }

    /**
     * Sets the value of the atgOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtgOrderId(String value) {
        this.atgOrderId = value;
    }

    /**
     * Gets the value of the items property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRmaItem }
     *     
     */
    public ArrayOfRmaItem getItems() {
        return items;
    }

    /**
     * Sets the value of the items property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRmaItem }
     *     
     */
    public void setItems(ArrayOfRmaItem value) {
        this.items = value;
    }

}
