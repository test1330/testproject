
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RmaDeliveryItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RmaDeliveryItem"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DeliveryItemId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RmaQty" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ReturnReasonId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RmaDeliveryItem", propOrder = {
    "deliveryItemId",
    "rmaQty",
    "returnReasonId"
})
public class RmaDeliveryItem {

    @XmlElement(name = "DeliveryItemId")
    protected int deliveryItemId;
    @XmlElement(name = "RmaQty")
    protected int rmaQty;
    @XmlElement(name = "ReturnReasonId")
    protected Integer returnReasonId;

    /**
     * Gets the value of the deliveryItemId property.
     * 
     */
    public int getDeliveryItemId() {
        return deliveryItemId;
    }

    /**
     * Sets the value of the deliveryItemId property.
     * 
     */
    public void setDeliveryItemId(int value) {
        this.deliveryItemId = value;
    }

    /**
     * Gets the value of the rmaQty property.
     * 
     */
    public int getRmaQty() {
        return rmaQty;
    }

    /**
     * Sets the value of the rmaQty property.
     * 
     */
    public void setRmaQty(int value) {
        this.rmaQty = value;
    }

    /**
     * Gets the value of the returnReasonId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReturnReasonId() {
        return returnReasonId;
    }

    /**
     * Sets the value of the returnReasonId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReturnReasonId(Integer value) {
        this.returnReasonId = value;
    }

}
