
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RmaItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RmaItem"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AtgRmaItemId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AtgCommerceItemId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AtgSkuId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Reason" type="{http://brandsmartusa.com/Services/AtgWebBroker}RmaReasonEnum"/&gt;
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RmaItem", propOrder = {
    "atgRmaItemId",
    "atgCommerceItemId",
    "atgSkuId",
    "reason",
    "status"
})
public class RmaItem {

    @XmlElement(name = "AtgRmaItemId", required = true, nillable = true)
    protected String atgRmaItemId;
    @XmlElement(name = "AtgCommerceItemId", required = true, nillable = true)
    protected String atgCommerceItemId;
    @XmlElement(name = "AtgSkuId", required = true, nillable = true)
    protected String atgSkuId;
    @XmlElement(name = "Reason", required = true)
    @XmlSchemaType(name = "string")
    protected RmaReasonEnum reason;
    @XmlElementRef(name = "Status", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<String> status;

    /**
     * Gets the value of the atgRmaItemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtgRmaItemId() {
        return atgRmaItemId;
    }

    /**
     * Sets the value of the atgRmaItemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtgRmaItemId(String value) {
        this.atgRmaItemId = value;
    }

    /**
     * Gets the value of the atgCommerceItemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtgCommerceItemId() {
        return atgCommerceItemId;
    }

    /**
     * Sets the value of the atgCommerceItemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtgCommerceItemId(String value) {
        this.atgCommerceItemId = value;
    }

    /**
     * Gets the value of the atgSkuId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtgSkuId() {
        return atgSkuId;
    }

    /**
     * Sets the value of the atgSkuId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtgSkuId(String value) {
        this.atgSkuId = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link RmaReasonEnum }
     *     
     */
    public RmaReasonEnum getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link RmaReasonEnum }
     *     
     */
    public void setReason(RmaReasonEnum value) {
        this.reason = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStatus(JAXBElement<String> value) {
        this.status = value;
    }

}
