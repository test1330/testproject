
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RmaReasonEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RmaReasonEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Unspecified"/&gt;
 *     &lt;enumeration value="Defective"/&gt;
 *     &lt;enumeration value="Damaged"/&gt;
 *     &lt;enumeration value="DidNotWant"/&gt;
 *     &lt;enumeration value="WrongSize"/&gt;
 *     &lt;enumeration value="WrongColor"/&gt;
 *     &lt;enumeration value="Other"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "RmaReasonEnum")
@XmlEnum
public enum RmaReasonEnum {

    @XmlEnumValue("Unspecified")
    UNSPECIFIED("Unspecified"),
    @XmlEnumValue("Defective")
    DEFECTIVE("Defective"),
    @XmlEnumValue("Damaged")
    DAMAGED("Damaged"),
    @XmlEnumValue("DidNotWant")
    DID_NOT_WANT("DidNotWant"),
    @XmlEnumValue("WrongSize")
    WRONG_SIZE("WrongSize"),
    @XmlEnumValue("WrongColor")
    WRONG_COLOR("WrongColor"),
    @XmlEnumValue("Other")
    OTHER("Other");
    private final String value;

    RmaReasonEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RmaReasonEnum fromValue(String v) {
        for (RmaReasonEnum c: RmaReasonEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
