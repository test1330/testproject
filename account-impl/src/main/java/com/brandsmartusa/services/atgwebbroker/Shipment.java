
package com.brandsmartusa.services.atgwebbroker;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Shipment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Shipment"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AtgShippingGroupId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Sales" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="Tax" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="Shipping" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="ShippingTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="ShippingAddress" type="{http://brandsmartusa.com/Services/AtgWebBroker}Address"/&gt;
 *         &lt;element name="ShippingPromotionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShippingPromotionAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="ShippingLevel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ShippingLocationId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ShippingDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SpecialInstructions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StateTaxRate" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="CountyTaxRate" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="CityTaxRate" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="Items" type="{http://brandsmartusa.com/Services/AtgWebBroker}ArrayOfShipmentItem"/&gt;
 *         &lt;element name="ExportCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExportCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AlternatePickupAddress" type="{http://brandsmartusa.com/Services/AtgWebBroker}Address" minOccurs="0"/&gt;
 *         &lt;element name="ShippingAvalaraCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TextStatusUpdates" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Shipment", propOrder = {
    "atgShippingGroupId",
    "sales",
    "tax",
    "shipping",
    "shippingTax",
    "shippingAddress",
    "shippingPromotionCode",
    "shippingPromotionAmount",
    "shippingLevel",
    "shippingLocationId",
    "shippingDate",
    "specialInstructions",
    "stateTaxRate",
    "countyTaxRate",
    "cityTaxRate",
    "items",
    "exportCity",
    "exportCountry",
    "alternatePickupAddress",
    "shippingAvalaraCode",
    "textStatusUpdates"
})
public class Shipment {

    @XmlElement(name = "AtgShippingGroupId", required = true, nillable = true)
    protected String atgShippingGroupId;
    @XmlElement(name = "Sales", required = true)
    protected BigDecimal sales;
    @XmlElement(name = "Tax", required = true)
    protected BigDecimal tax;
    @XmlElement(name = "Shipping", required = true)
    protected BigDecimal shipping;
    @XmlElement(name = "ShippingTax")
    protected BigDecimal shippingTax;
    @XmlElement(name = "ShippingAddress", required = true, nillable = true)
    protected Address shippingAddress;
    @XmlElementRef(name = "ShippingPromotionCode", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shippingPromotionCode;
    @XmlElement(name = "ShippingPromotionAmount")
    protected BigDecimal shippingPromotionAmount;
    @XmlElement(name = "ShippingLevel", required = true, nillable = true)
    protected String shippingLevel;
    @XmlElement(name = "ShippingLocationId")
    protected int shippingLocationId;
    @XmlElementRef(name = "ShippingDate", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shippingDate;
    @XmlElementRef(name = "SpecialInstructions", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<String> specialInstructions;
    @XmlElement(name = "StateTaxRate", required = true)
    protected BigDecimal stateTaxRate;
    @XmlElement(name = "CountyTaxRate", required = true)
    protected BigDecimal countyTaxRate;
    @XmlElement(name = "CityTaxRate", required = true)
    protected BigDecimal cityTaxRate;
    @XmlElement(name = "Items", required = true, nillable = true)
    protected ArrayOfShipmentItem items;
    @XmlElementRef(name = "ExportCity", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<String> exportCity;
    @XmlElementRef(name = "ExportCountry", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<String> exportCountry;
    @XmlElementRef(name = "AlternatePickupAddress", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<Address> alternatePickupAddress;
    @XmlElementRef(name = "ShippingAvalaraCode", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shippingAvalaraCode;
    @XmlElement(name = "TextStatusUpdates")
    protected Boolean textStatusUpdates;

    /**
     * Gets the value of the atgShippingGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtgShippingGroupId() {
        return atgShippingGroupId;
    }

    /**
     * Sets the value of the atgShippingGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtgShippingGroupId(String value) {
        this.atgShippingGroupId = value;
    }

    /**
     * Gets the value of the sales property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSales() {
        return sales;
    }

    /**
     * Sets the value of the sales property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSales(BigDecimal value) {
        this.sales = value;
    }

    /**
     * Gets the value of the tax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTax() {
        return tax;
    }

    /**
     * Sets the value of the tax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTax(BigDecimal value) {
        this.tax = value;
    }

    /**
     * Gets the value of the shipping property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getShipping() {
        return shipping;
    }

    /**
     * Sets the value of the shipping property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setShipping(BigDecimal value) {
        this.shipping = value;
    }

    /**
     * Gets the value of the shippingTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getShippingTax() {
        return shippingTax;
    }

    /**
     * Sets the value of the shippingTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setShippingTax(BigDecimal value) {
        this.shippingTax = value;
    }

    /**
     * Gets the value of the shippingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getShippingAddress() {
        return shippingAddress;
    }

    /**
     * Sets the value of the shippingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setShippingAddress(Address value) {
        this.shippingAddress = value;
    }

    /**
     * Gets the value of the shippingPromotionCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShippingPromotionCode() {
        return shippingPromotionCode;
    }

    /**
     * Sets the value of the shippingPromotionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShippingPromotionCode(JAXBElement<String> value) {
        this.shippingPromotionCode = value;
    }

    /**
     * Gets the value of the shippingPromotionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getShippingPromotionAmount() {
        return shippingPromotionAmount;
    }

    /**
     * Sets the value of the shippingPromotionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setShippingPromotionAmount(BigDecimal value) {
        this.shippingPromotionAmount = value;
    }

    /**
     * Gets the value of the shippingLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingLevel() {
        return shippingLevel;
    }

    /**
     * Sets the value of the shippingLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingLevel(String value) {
        this.shippingLevel = value;
    }

    /**
     * Gets the value of the shippingLocationId property.
     * 
     */
    public int getShippingLocationId() {
        return shippingLocationId;
    }

    /**
     * Sets the value of the shippingLocationId property.
     * 
     */
    public void setShippingLocationId(int value) {
        this.shippingLocationId = value;
    }

    /**
     * Gets the value of the shippingDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShippingDate() {
        return shippingDate;
    }

    /**
     * Sets the value of the shippingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShippingDate(JAXBElement<String> value) {
        this.shippingDate = value;
    }

    /**
     * Gets the value of the specialInstructions property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSpecialInstructions() {
        return specialInstructions;
    }

    /**
     * Sets the value of the specialInstructions property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSpecialInstructions(JAXBElement<String> value) {
        this.specialInstructions = value;
    }

    /**
     * Gets the value of the stateTaxRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStateTaxRate() {
        return stateTaxRate;
    }

    /**
     * Sets the value of the stateTaxRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStateTaxRate(BigDecimal value) {
        this.stateTaxRate = value;
    }

    /**
     * Gets the value of the countyTaxRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCountyTaxRate() {
        return countyTaxRate;
    }

    /**
     * Sets the value of the countyTaxRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCountyTaxRate(BigDecimal value) {
        this.countyTaxRate = value;
    }

    /**
     * Gets the value of the cityTaxRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCityTaxRate() {
        return cityTaxRate;
    }

    /**
     * Sets the value of the cityTaxRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCityTaxRate(BigDecimal value) {
        this.cityTaxRate = value;
    }

    /**
     * Gets the value of the items property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfShipmentItem }
     *     
     */
    public ArrayOfShipmentItem getItems() {
        return items;
    }

    /**
     * Sets the value of the items property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfShipmentItem }
     *     
     */
    public void setItems(ArrayOfShipmentItem value) {
        this.items = value;
    }

    /**
     * Gets the value of the exportCity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExportCity() {
        return exportCity;
    }

    /**
     * Sets the value of the exportCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExportCity(JAXBElement<String> value) {
        this.exportCity = value;
    }

    /**
     * Gets the value of the exportCountry property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExportCountry() {
        return exportCountry;
    }

    /**
     * Sets the value of the exportCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExportCountry(JAXBElement<String> value) {
        this.exportCountry = value;
    }

    /**
     * Gets the value of the alternatePickupAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public JAXBElement<Address> getAlternatePickupAddress() {
        return alternatePickupAddress;
    }

    /**
     * Sets the value of the alternatePickupAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public void setAlternatePickupAddress(JAXBElement<Address> value) {
        this.alternatePickupAddress = value;
    }

    /**
     * Gets the value of the shippingAvalaraCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShippingAvalaraCode() {
        return shippingAvalaraCode;
    }

    /**
     * Sets the value of the shippingAvalaraCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShippingAvalaraCode(JAXBElement<String> value) {
        this.shippingAvalaraCode = value;
    }

    /**
     * Gets the value of the textStatusUpdates property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTextStatusUpdates() {
        return textStatusUpdates;
    }

    /**
     * Sets the value of the textStatusUpdates property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTextStatusUpdates(Boolean value) {
        this.textStatusUpdates = value;
    }

}
