
package com.brandsmartusa.services.atgwebbroker;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ShipmentItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentItem"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AtgCommerceItemId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AtgParentCommerceItemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AtgSkuId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="UnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="Tax" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="PromotionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PromotionAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="Qty" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="BoxWeight" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="IsWarranty" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="IsBundle" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="IsComponent" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="IsTaxable" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="IsTaxRebate" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="GWPCommerceItemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsPreOrder" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="PreOrderMessageTypeId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="AvalaraCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentItem", propOrder = {
    "atgCommerceItemId",
    "atgParentCommerceItemId",
    "atgSkuId",
    "unitPrice",
    "tax",
    "promotionCode",
    "promotionAmount",
    "qty",
    "boxWeight",
    "isWarranty",
    "isBundle",
    "isComponent",
    "isTaxable",
    "isTaxRebate",
    "gwpCommerceItemId",
    "isPreOrder",
    "preOrderMessageTypeId",
    "avalaraCode"
})
public class ShipmentItem {

    @XmlElement(name = "AtgCommerceItemId", required = true, nillable = true)
    protected String atgCommerceItemId;
    @XmlElementRef(name = "AtgParentCommerceItemId", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<String> atgParentCommerceItemId;
    @XmlElement(name = "AtgSkuId", required = true, nillable = true)
    protected String atgSkuId;
    @XmlElement(name = "UnitPrice", required = true)
    protected BigDecimal unitPrice;
    @XmlElement(name = "Tax", required = true)
    protected BigDecimal tax;
    @XmlElementRef(name = "PromotionCode", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<String> promotionCode;
    @XmlElement(name = "PromotionAmount")
    protected BigDecimal promotionAmount;
    @XmlElement(name = "Qty")
    protected int qty;
    @XmlElement(name = "BoxWeight", required = true)
    protected BigDecimal boxWeight;
    @XmlElement(name = "IsWarranty")
    protected boolean isWarranty;
    @XmlElement(name = "IsBundle")
    protected boolean isBundle;
    @XmlElement(name = "IsComponent")
    protected boolean isComponent;
    @XmlElement(name = "IsTaxable")
    protected boolean isTaxable;
    @XmlElement(name = "IsTaxRebate")
    protected boolean isTaxRebate;
    @XmlElementRef(name = "GWPCommerceItemId", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<String> gwpCommerceItemId;
    @XmlElement(name = "IsPreOrder")
    protected Boolean isPreOrder;
    @XmlElement(name = "PreOrderMessageTypeId")
    protected Integer preOrderMessageTypeId;
    @XmlElementRef(name = "AvalaraCode", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<String> avalaraCode;

    /**
     * Gets the value of the atgCommerceItemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtgCommerceItemId() {
        return atgCommerceItemId;
    }

    /**
     * Sets the value of the atgCommerceItemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtgCommerceItemId(String value) {
        this.atgCommerceItemId = value;
    }

    /**
     * Gets the value of the atgParentCommerceItemId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAtgParentCommerceItemId() {
        return atgParentCommerceItemId;
    }

    /**
     * Sets the value of the atgParentCommerceItemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAtgParentCommerceItemId(JAXBElement<String> value) {
        this.atgParentCommerceItemId = value;
    }

    /**
     * Gets the value of the atgSkuId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtgSkuId() {
        return atgSkuId;
    }

    /**
     * Sets the value of the atgSkuId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtgSkuId(String value) {
        this.atgSkuId = value;
    }

    /**
     * Gets the value of the unitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /**
     * Sets the value of the unitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUnitPrice(BigDecimal value) {
        this.unitPrice = value;
    }

    /**
     * Gets the value of the tax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTax() {
        return tax;
    }

    /**
     * Sets the value of the tax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTax(BigDecimal value) {
        this.tax = value;
    }

    /**
     * Gets the value of the promotionCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPromotionCode() {
        return promotionCode;
    }

    /**
     * Sets the value of the promotionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPromotionCode(JAXBElement<String> value) {
        this.promotionCode = value;
    }

    /**
     * Gets the value of the promotionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPromotionAmount() {
        return promotionAmount;
    }

    /**
     * Sets the value of the promotionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPromotionAmount(BigDecimal value) {
        this.promotionAmount = value;
    }

    /**
     * Gets the value of the qty property.
     * 
     */
    public int getQty() {
        return qty;
    }

    /**
     * Sets the value of the qty property.
     * 
     */
    public void setQty(int value) {
        this.qty = value;
    }

    /**
     * Gets the value of the boxWeight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBoxWeight() {
        return boxWeight;
    }

    /**
     * Sets the value of the boxWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBoxWeight(BigDecimal value) {
        this.boxWeight = value;
    }

    /**
     * Gets the value of the isWarranty property.
     * 
     */
    public boolean isIsWarranty() {
        return isWarranty;
    }

    /**
     * Sets the value of the isWarranty property.
     * 
     */
    public void setIsWarranty(boolean value) {
        this.isWarranty = value;
    }

    /**
     * Gets the value of the isBundle property.
     * 
     */
    public boolean isIsBundle() {
        return isBundle;
    }

    /**
     * Sets the value of the isBundle property.
     * 
     */
    public void setIsBundle(boolean value) {
        this.isBundle = value;
    }

    /**
     * Gets the value of the isComponent property.
     * 
     */
    public boolean isIsComponent() {
        return isComponent;
    }

    /**
     * Sets the value of the isComponent property.
     * 
     */
    public void setIsComponent(boolean value) {
        this.isComponent = value;
    }

    /**
     * Gets the value of the isTaxable property.
     * 
     */
    public boolean isIsTaxable() {
        return isTaxable;
    }

    /**
     * Sets the value of the isTaxable property.
     * 
     */
    public void setIsTaxable(boolean value) {
        this.isTaxable = value;
    }

    /**
     * Gets the value of the isTaxRebate property.
     * 
     */
    public boolean isIsTaxRebate() {
        return isTaxRebate;
    }

    /**
     * Sets the value of the isTaxRebate property.
     * 
     */
    public void setIsTaxRebate(boolean value) {
        this.isTaxRebate = value;
    }

    /**
     * Gets the value of the gwpCommerceItemId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGWPCommerceItemId() {
        return gwpCommerceItemId;
    }

    /**
     * Sets the value of the gwpCommerceItemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGWPCommerceItemId(JAXBElement<String> value) {
        this.gwpCommerceItemId = value;
    }

    /**
     * Gets the value of the isPreOrder property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPreOrder() {
        return isPreOrder;
    }

    /**
     * Sets the value of the isPreOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPreOrder(Boolean value) {
        this.isPreOrder = value;
    }

    /**
     * Gets the value of the preOrderMessageTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPreOrderMessageTypeId() {
        return preOrderMessageTypeId;
    }

    /**
     * Sets the value of the preOrderMessageTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPreOrderMessageTypeId(Integer value) {
        this.preOrderMessageTypeId = value;
    }

    /**
     * Gets the value of the avalaraCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAvalaraCode() {
        return avalaraCode;
    }

    /**
     * Sets the value of the avalaraCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAvalaraCode(JAXBElement<String> value) {
        this.avalaraCode = value;
    }

}
