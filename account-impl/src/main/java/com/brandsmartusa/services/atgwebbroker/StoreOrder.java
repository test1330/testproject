
package com.brandsmartusa.services.atgwebbroker;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StoreOrder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StoreOrder"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Invoice" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="IsDelivery" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="Sales" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="Tax" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="Shipping" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="StatusText" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Address" type="{http://brandsmartusa.com/Services/AtgWebBroker}Address" minOccurs="0"/&gt;
 *         &lt;element name="Store" type="{http://brandsmartusa.com/Services/AtgWebBroker}StoreLocation"/&gt;
 *         &lt;element name="Items" type="{http://brandsmartusa.com/Services/AtgWebBroker}ArrayOfStoreOrderItem"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StoreOrder", propOrder = {
    "invoice",
    "isDelivery",
    "sales",
    "tax",
    "shipping",
    "statusText",
    "address",
    "store",
    "items"
})
public class StoreOrder {

    @XmlElement(name = "Invoice")
    protected int invoice;
    @XmlElement(name = "IsDelivery")
    protected boolean isDelivery;
    @XmlElement(name = "Sales", required = true)
    protected BigDecimal sales;
    @XmlElement(name = "Tax", required = true)
    protected BigDecimal tax;
    @XmlElement(name = "Shipping", required = true)
    protected BigDecimal shipping;
    @XmlElement(name = "StatusText", required = true, nillable = true)
    protected String statusText;
    @XmlElementRef(name = "Address", namespace = "http://brandsmartusa.com/Services/AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<Address> address;
    @XmlElement(name = "Store", required = true, nillable = true)
    protected StoreLocation store;
    @XmlElement(name = "Items", required = true, nillable = true)
    protected ArrayOfStoreOrderItem items;

    /**
     * Gets the value of the invoice property.
     * 
     */
    public int getInvoice() {
        return invoice;
    }

    /**
     * Sets the value of the invoice property.
     * 
     */
    public void setInvoice(int value) {
        this.invoice = value;
    }

    /**
     * Gets the value of the isDelivery property.
     * 
     */
    public boolean isIsDelivery() {
        return isDelivery;
    }

    /**
     * Sets the value of the isDelivery property.
     * 
     */
    public void setIsDelivery(boolean value) {
        this.isDelivery = value;
    }

    /**
     * Gets the value of the sales property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSales() {
        return sales;
    }

    /**
     * Sets the value of the sales property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSales(BigDecimal value) {
        this.sales = value;
    }

    /**
     * Gets the value of the tax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTax() {
        return tax;
    }

    /**
     * Sets the value of the tax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTax(BigDecimal value) {
        this.tax = value;
    }

    /**
     * Gets the value of the shipping property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getShipping() {
        return shipping;
    }

    /**
     * Sets the value of the shipping property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setShipping(BigDecimal value) {
        this.shipping = value;
    }

    /**
     * Gets the value of the statusText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusText() {
        return statusText;
    }

    /**
     * Sets the value of the statusText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusText(String value) {
        this.statusText = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public JAXBElement<Address> getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public void setAddress(JAXBElement<Address> value) {
        this.address = value;
    }

    /**
     * Gets the value of the store property.
     * 
     * @return
     *     possible object is
     *     {@link StoreLocation }
     *     
     */
    public StoreLocation getStore() {
        return store;
    }

    /**
     * Sets the value of the store property.
     * 
     * @param value
     *     allowed object is
     *     {@link StoreLocation }
     *     
     */
    public void setStore(StoreLocation value) {
        this.store = value;
    }

    /**
     * Gets the value of the items property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfStoreOrderItem }
     *     
     */
    public ArrayOfStoreOrderItem getItems() {
        return items;
    }

    /**
     * Sets the value of the items property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfStoreOrderItem }
     *     
     */
    public void setItems(ArrayOfStoreOrderItem value) {
        this.items = value;
    }

}
