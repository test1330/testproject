
package com.brandsmartusa.services.atgwebbroker;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaxRateSource.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TaxRateSource"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="BrandsMart"/&gt;
 *     &lt;enumeration value="Avalara"/&gt;
 *     &lt;enumeration value="External"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TaxRateSource")
@XmlEnum
public enum TaxRateSource {

    @XmlEnumValue("BrandsMart")
    BRANDS_MART("BrandsMart"),
    @XmlEnumValue("Avalara")
    AVALARA("Avalara"),
    @XmlEnumValue("External")
    EXTERNAL("External");
    private final String value;

    TaxRateSource(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TaxRateSource fromValue(String v) {
        for (TaxRateSource c: TaxRateSource.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
