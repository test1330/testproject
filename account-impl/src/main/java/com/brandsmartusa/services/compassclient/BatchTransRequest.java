
package com.brandsmartusa.services.compassclient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PID" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}String10max"/&gt;
 *         &lt;element name="SID" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}String10max"/&gt;
 *         &lt;element name="BatchId" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}String10max"/&gt;
 *         &lt;element name="TPP" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}TPP" minOccurs="0"/&gt;
 *         &lt;element name="MRecord" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}MRecord" minOccurs="0"/&gt;
 *         &lt;element name="BatchRecord" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}BatchRecord" maxOccurs="10000"/&gt;
 *         &lt;element name="BatchTotals" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}BatchTotals" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pid",
    "sid",
    "batchId",
    "tpp",
    "mRecord",
    "batchRecord",
    "batchTotals"
})
@XmlRootElement(name = "BatchTransRequest", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi")
public class BatchTransRequest {

    @XmlElement(name = "PID", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi", required = true)
    protected String pid;
    @XmlElement(name = "SID", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi", required = true)
    protected String sid;
    @XmlElement(name = "BatchId", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi", required = true)
    protected String batchId;
    @XmlElement(name = "TPP", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi")
    protected TPP tpp;
    @XmlElement(name = "MRecord", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi")
    protected MRecord mRecord;
    @XmlElement(name = "BatchRecord", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi", required = true)
    protected List<BatchRecord> batchRecord;
    @XmlElement(name = "BatchTotals", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi")
    protected BatchTotals batchTotals;

    /**
     * Gets the value of the pid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPID() {
        return pid;
    }

    /**
     * Sets the value of the pid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPID(String value) {
        this.pid = value;
    }

    /**
     * Gets the value of the sid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSID() {
        return sid;
    }

    /**
     * Sets the value of the sid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSID(String value) {
        this.sid = value;
    }

    /**
     * Gets the value of the batchId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * Sets the value of the batchId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBatchId(String value) {
        this.batchId = value;
    }

    /**
     * Gets the value of the tpp property.
     * 
     * @return
     *     possible object is
     *     {@link TPP }
     *     
     */
    public TPP getTPP() {
        return tpp;
    }

    /**
     * Sets the value of the tpp property.
     * 
     * @param value
     *     allowed object is
     *     {@link TPP }
     *     
     */
    public void setTPP(TPP value) {
        this.tpp = value;
    }

    /**
     * Gets the value of the mRecord property.
     * 
     * @return
     *     possible object is
     *     {@link MRecord }
     *     
     */
    public MRecord getMRecord() {
        return mRecord;
    }

    /**
     * Sets the value of the mRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link MRecord }
     *     
     */
    public void setMRecord(MRecord value) {
        this.mRecord = value;
    }

    /**
     * Gets the value of the batchRecord property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the batchRecord property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBatchRecord().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BatchRecord }
     * 
     * 
     */
    public List<BatchRecord> getBatchRecord() {
        if (batchRecord == null) {
            batchRecord = new ArrayList<BatchRecord>();
        }
        return this.batchRecord;
    }

    /**
     * Gets the value of the batchTotals property.
     * 
     * @return
     *     possible object is
     *     {@link BatchTotals }
     *     
     */
    public BatchTotals getBatchTotals() {
        return batchTotals;
    }

    /**
     * Sets the value of the batchTotals property.
     * 
     * @param value
     *     allowed object is
     *     {@link BatchTotals }
     *     
     */
    public void setBatchTotals(BatchTotals value) {
        this.batchTotals = value;
    }

}
