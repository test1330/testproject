
package com.brandsmartusa.services.compassclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillToAddress complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BillToAddress"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LN" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}LN" minOccurs="0"/&gt;
 *         &lt;element name="LA" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}ContactAddressWOTel" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillToAddress", propOrder = {
    "ln",
    "la"
})
public class BillToAddress {

    @XmlElement(name = "LN")
    protected LN ln;
    @XmlElement(name = "LA")
    protected ContactAddressWOTel la;

    /**
     * Gets the value of the ln property.
     * 
     * @return
     *     possible object is
     *     {@link LN }
     *     
     */
    public LN getLN() {
        return ln;
    }

    /**
     * Sets the value of the ln property.
     * 
     * @param value
     *     allowed object is
     *     {@link LN }
     *     
     */
    public void setLN(LN value) {
        this.ln = value;
    }

    /**
     * Gets the value of the la property.
     * 
     * @return
     *     possible object is
     *     {@link ContactAddressWOTel }
     *     
     */
    public ContactAddressWOTel getLA() {
        return la;
    }

    /**
     * Sets the value of the la property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactAddressWOTel }
     *     
     */
    public void setLA(ContactAddressWOTel value) {
        this.la = value;
    }

}
