
package com.brandsmartusa.services.compassclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EMC001 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EMC001"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="POSCapabilityCode" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}POSCapabilityCode"/&gt;
 *         &lt;element name="POSEntryMode" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}POSEntryMode2"/&gt;
 *         &lt;element name="POSAuthorizationSource" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}POSAuthorizationSource"/&gt;
 *         &lt;element name="POSCardIDMethod" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}POSCardIDMethod2"/&gt;
 *         &lt;element name="BankNetReferenceNumber"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="9"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="AuthorizedAmount" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="[0-9]{0,12}"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="BankNetDate"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="[0-9]{4}"/&gt;
 *               &lt;maxLength value="4"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="MCC"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="[0-9]{0,4}"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Reserved1" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Reserved" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="68"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EMC001", propOrder = {
    "posCapabilityCode",
    "posEntryMode",
    "posAuthorizationSource",
    "posCardIDMethod",
    "bankNetReferenceNumber",
    "authorizedAmount",
    "bankNetDate",
    "mcc",
    "reserved1",
    "reserved"
})
public class EMC001 {

    @XmlElement(name = "POSCapabilityCode", required = true)
    protected String posCapabilityCode;
    @XmlElement(name = "POSEntryMode", required = true)
    protected String posEntryMode;
    @XmlElement(name = "POSAuthorizationSource", required = true)
    protected String posAuthorizationSource;
    @XmlElement(name = "POSCardIDMethod", required = true)
    protected String posCardIDMethod;
    @XmlElement(name = "BankNetReferenceNumber", required = true)
    protected String bankNetReferenceNumber;
    @XmlElement(name = "AuthorizedAmount")
    protected String authorizedAmount;
    @XmlElement(name = "BankNetDate", required = true)
    protected String bankNetDate;
    @XmlElement(name = "MCC", required = true)
    protected String mcc;
    @XmlElement(name = "Reserved1")
    protected String reserved1;
    @XmlElement(name = "Reserved")
    protected String reserved;

    /**
     * Gets the value of the posCapabilityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOSCapabilityCode() {
        return posCapabilityCode;
    }

    /**
     * Sets the value of the posCapabilityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOSCapabilityCode(String value) {
        this.posCapabilityCode = value;
    }

    /**
     * Gets the value of the posEntryMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOSEntryMode() {
        return posEntryMode;
    }

    /**
     * Sets the value of the posEntryMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOSEntryMode(String value) {
        this.posEntryMode = value;
    }

    /**
     * Gets the value of the posAuthorizationSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOSAuthorizationSource() {
        return posAuthorizationSource;
    }

    /**
     * Sets the value of the posAuthorizationSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOSAuthorizationSource(String value) {
        this.posAuthorizationSource = value;
    }

    /**
     * Gets the value of the posCardIDMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOSCardIDMethod() {
        return posCardIDMethod;
    }

    /**
     * Sets the value of the posCardIDMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOSCardIDMethod(String value) {
        this.posCardIDMethod = value;
    }

    /**
     * Gets the value of the bankNetReferenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankNetReferenceNumber() {
        return bankNetReferenceNumber;
    }

    /**
     * Sets the value of the bankNetReferenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankNetReferenceNumber(String value) {
        this.bankNetReferenceNumber = value;
    }

    /**
     * Gets the value of the authorizedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizedAmount() {
        return authorizedAmount;
    }

    /**
     * Sets the value of the authorizedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizedAmount(String value) {
        this.authorizedAmount = value;
    }

    /**
     * Gets the value of the bankNetDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankNetDate() {
        return bankNetDate;
    }

    /**
     * Sets the value of the bankNetDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankNetDate(String value) {
        this.bankNetDate = value;
    }

    /**
     * Gets the value of the mcc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMCC() {
        return mcc;
    }

    /**
     * Sets the value of the mcc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMCC(String value) {
        this.mcc = value;
    }

    /**
     * Gets the value of the reserved1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved1() {
        return reserved1;
    }

    /**
     * Sets the value of the reserved1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved1(String value) {
        this.reserved1 = value;
    }

    /**
     * Gets the value of the reserved property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * Sets the value of the reserved property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved(String value) {
        this.reserved = value;
    }

}
