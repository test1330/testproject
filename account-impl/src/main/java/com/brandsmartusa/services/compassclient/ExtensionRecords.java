
package com.brandsmartusa.services.compassclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExtensionRecords complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExtensionRecords"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="EAX001" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}EAX001" minOccurs="0"/&gt;
 *         &lt;element name="EAX002" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}EAX002" minOccurs="0"/&gt;
 *         &lt;element name="EAX003" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}EAX003" minOccurs="0"/&gt;
 *         &lt;element name="EAX004" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}EAX004" minOccurs="0"/&gt;
 *         &lt;element name="EMC001" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}EMC001" minOccurs="0"/&gt;
 *         &lt;element name="EMC002" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}EMC002" minOccurs="0"/&gt;
 *         &lt;element name="EMD001" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}EMC001" minOccurs="0"/&gt;
 *         &lt;element name="EVI001" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}EVI001" minOccurs="0"/&gt;
 *         &lt;element name="EVI002" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}EVI002" minOccurs="0"/&gt;
 *         &lt;element name="EEC001" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}EEC001" minOccurs="0"/&gt;
 *         &lt;element name="EEC002" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}EEC002" minOccurs="0"/&gt;
 *         &lt;element name="EED001" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}EED001" minOccurs="0"/&gt;
 *         &lt;element name="EBL001" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}EBL001" minOccurs="0"/&gt;
 *         &lt;element name="ESW001" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}ESW001" minOccurs="0"/&gt;
 *         &lt;element name="EGE001" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}EGE001" minOccurs="0"/&gt;
 *         &lt;element name="EFC001" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}EFC001" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtensionRecords", propOrder = {

})
public class ExtensionRecords {

    @XmlElement(name = "EAX001")
    protected EAX001 eax001;
    @XmlElement(name = "EAX002")
    protected EAX002 eax002;
    @XmlElement(name = "EAX003")
    protected EAX003 eax003;
    @XmlElement(name = "EAX004")
    protected EAX004 eax004;
    @XmlElement(name = "EMC001")
    protected EMC001 emc001;
    @XmlElement(name = "EMC002")
    protected EMC002 emc002;
    @XmlElement(name = "EMD001")
    protected EMC001 emd001;
    @XmlElement(name = "EVI001")
    protected EVI001 evi001;
    @XmlElement(name = "EVI002")
    protected EVI002 evi002;
    @XmlElement(name = "EEC001")
    protected EEC001 eec001;
    @XmlElement(name = "EEC002")
    protected EEC002 eec002;
    @XmlElement(name = "EED001")
    protected EED001 eed001;
    @XmlElement(name = "EBL001")
    protected EBL001 ebl001;
    @XmlElement(name = "ESW001")
    protected ESW001 esw001;
    @XmlElement(name = "EGE001")
    protected EGE001 ege001;
    @XmlElement(name = "EFC001")
    protected EFC001 efc001;

    /**
     * Gets the value of the eax001 property.
     * 
     * @return
     *     possible object is
     *     {@link EAX001 }
     *     
     */
    public EAX001 getEAX001() {
        return eax001;
    }

    /**
     * Sets the value of the eax001 property.
     * 
     * @param value
     *     allowed object is
     *     {@link EAX001 }
     *     
     */
    public void setEAX001(EAX001 value) {
        this.eax001 = value;
    }

    /**
     * Gets the value of the eax002 property.
     * 
     * @return
     *     possible object is
     *     {@link EAX002 }
     *     
     */
    public EAX002 getEAX002() {
        return eax002;
    }

    /**
     * Sets the value of the eax002 property.
     * 
     * @param value
     *     allowed object is
     *     {@link EAX002 }
     *     
     */
    public void setEAX002(EAX002 value) {
        this.eax002 = value;
    }

    /**
     * Gets the value of the eax003 property.
     * 
     * @return
     *     possible object is
     *     {@link EAX003 }
     *     
     */
    public EAX003 getEAX003() {
        return eax003;
    }

    /**
     * Sets the value of the eax003 property.
     * 
     * @param value
     *     allowed object is
     *     {@link EAX003 }
     *     
     */
    public void setEAX003(EAX003 value) {
        this.eax003 = value;
    }

    /**
     * Gets the value of the eax004 property.
     * 
     * @return
     *     possible object is
     *     {@link EAX004 }
     *     
     */
    public EAX004 getEAX004() {
        return eax004;
    }

    /**
     * Sets the value of the eax004 property.
     * 
     * @param value
     *     allowed object is
     *     {@link EAX004 }
     *     
     */
    public void setEAX004(EAX004 value) {
        this.eax004 = value;
    }

    /**
     * Gets the value of the emc001 property.
     * 
     * @return
     *     possible object is
     *     {@link EMC001 }
     *     
     */
    public EMC001 getEMC001() {
        return emc001;
    }

    /**
     * Sets the value of the emc001 property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMC001 }
     *     
     */
    public void setEMC001(EMC001 value) {
        this.emc001 = value;
    }

    /**
     * Gets the value of the emc002 property.
     * 
     * @return
     *     possible object is
     *     {@link EMC002 }
     *     
     */
    public EMC002 getEMC002() {
        return emc002;
    }

    /**
     * Sets the value of the emc002 property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMC002 }
     *     
     */
    public void setEMC002(EMC002 value) {
        this.emc002 = value;
    }

    /**
     * Gets the value of the emd001 property.
     * 
     * @return
     *     possible object is
     *     {@link EMC001 }
     *     
     */
    public EMC001 getEMD001() {
        return emd001;
    }

    /**
     * Sets the value of the emd001 property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMC001 }
     *     
     */
    public void setEMD001(EMC001 value) {
        this.emd001 = value;
    }

    /**
     * Gets the value of the evi001 property.
     * 
     * @return
     *     possible object is
     *     {@link EVI001 }
     *     
     */
    public EVI001 getEVI001() {
        return evi001;
    }

    /**
     * Sets the value of the evi001 property.
     * 
     * @param value
     *     allowed object is
     *     {@link EVI001 }
     *     
     */
    public void setEVI001(EVI001 value) {
        this.evi001 = value;
    }

    /**
     * Gets the value of the evi002 property.
     * 
     * @return
     *     possible object is
     *     {@link EVI002 }
     *     
     */
    public EVI002 getEVI002() {
        return evi002;
    }

    /**
     * Sets the value of the evi002 property.
     * 
     * @param value
     *     allowed object is
     *     {@link EVI002 }
     *     
     */
    public void setEVI002(EVI002 value) {
        this.evi002 = value;
    }

    /**
     * Gets the value of the eec001 property.
     * 
     * @return
     *     possible object is
     *     {@link EEC001 }
     *     
     */
    public EEC001 getEEC001() {
        return eec001;
    }

    /**
     * Sets the value of the eec001 property.
     * 
     * @param value
     *     allowed object is
     *     {@link EEC001 }
     *     
     */
    public void setEEC001(EEC001 value) {
        this.eec001 = value;
    }

    /**
     * Gets the value of the eec002 property.
     * 
     * @return
     *     possible object is
     *     {@link EEC002 }
     *     
     */
    public EEC002 getEEC002() {
        return eec002;
    }

    /**
     * Sets the value of the eec002 property.
     * 
     * @param value
     *     allowed object is
     *     {@link EEC002 }
     *     
     */
    public void setEEC002(EEC002 value) {
        this.eec002 = value;
    }

    /**
     * Gets the value of the eed001 property.
     * 
     * @return
     *     possible object is
     *     {@link EED001 }
     *     
     */
    public EED001 getEED001() {
        return eed001;
    }

    /**
     * Sets the value of the eed001 property.
     * 
     * @param value
     *     allowed object is
     *     {@link EED001 }
     *     
     */
    public void setEED001(EED001 value) {
        this.eed001 = value;
    }

    /**
     * Gets the value of the ebl001 property.
     * 
     * @return
     *     possible object is
     *     {@link EBL001 }
     *     
     */
    public EBL001 getEBL001() {
        return ebl001;
    }

    /**
     * Sets the value of the ebl001 property.
     * 
     * @param value
     *     allowed object is
     *     {@link EBL001 }
     *     
     */
    public void setEBL001(EBL001 value) {
        this.ebl001 = value;
    }

    /**
     * Gets the value of the esw001 property.
     * 
     * @return
     *     possible object is
     *     {@link ESW001 }
     *     
     */
    public ESW001 getESW001() {
        return esw001;
    }

    /**
     * Sets the value of the esw001 property.
     * 
     * @param value
     *     allowed object is
     *     {@link ESW001 }
     *     
     */
    public void setESW001(ESW001 value) {
        this.esw001 = value;
    }

    /**
     * Gets the value of the ege001 property.
     * 
     * @return
     *     possible object is
     *     {@link EGE001 }
     *     
     */
    public EGE001 getEGE001() {
        return ege001;
    }

    /**
     * Sets the value of the ege001 property.
     * 
     * @param value
     *     allowed object is
     *     {@link EGE001 }
     *     
     */
    public void setEGE001(EGE001 value) {
        this.ege001 = value;
    }

    /**
     * Gets the value of the efc001 property.
     * 
     * @return
     *     possible object is
     *     {@link EFC001 }
     *     
     */
    public EFC001 getEFC001() {
        return efc001;
    }

    /**
     * Sets the value of the efc001 property.
     * 
     * @param value
     *     allowed object is
     *     {@link EFC001 }
     *     
     */
    public void setEFC001(EFC001 value) {
        this.efc001 = value;
    }

}
