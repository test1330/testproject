
package com.brandsmartusa.services.compassclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InformationRecords complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InformationRecords"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="IOI" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}IOI" minOccurs="0"/&gt;
 *         &lt;element name="IPI001" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}IPI001" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InformationRecords", propOrder = {

})
public class InformationRecords {

    @XmlElement(name = "IOI")
    protected IOI ioi;
    @XmlElement(name = "IPI001")
    protected IPI001 ipi001;

    /**
     * Gets the value of the ioi property.
     * 
     * @return
     *     possible object is
     *     {@link IOI }
     *     
     */
    public IOI getIOI() {
        return ioi;
    }

    /**
     * Sets the value of the ioi property.
     * 
     * @param value
     *     allowed object is
     *     {@link IOI }
     *     
     */
    public void setIOI(IOI value) {
        this.ioi = value;
    }

    /**
     * Gets the value of the ipi001 property.
     * 
     * @return
     *     possible object is
     *     {@link IPI001 }
     *     
     */
    public IPI001 getIPI001() {
        return ipi001;
    }

    /**
     * Sets the value of the ipi001 property.
     * 
     * @param value
     *     allowed object is
     *     {@link IPI001 }
     *     
     */
    public void setIPI001(IPI001 value) {
        this.ipi001 = value;
    }

}
