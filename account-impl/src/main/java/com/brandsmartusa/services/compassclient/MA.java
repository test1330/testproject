
package com.brandsmartusa.services.compassclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MA complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MA"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AccountholderAuthenticationValue"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="32"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MA", propOrder = {
    "accountholderAuthenticationValue"
})
public class MA {

    @XmlElement(name = "AccountholderAuthenticationValue", required = true)
    protected String accountholderAuthenticationValue;

    /**
     * Gets the value of the accountholderAuthenticationValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountholderAuthenticationValue() {
        return accountholderAuthenticationValue;
    }

    /**
     * Sets the value of the accountholderAuthenticationValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountholderAuthenticationValue(String value) {
        this.accountholderAuthenticationValue = value;
    }

}
