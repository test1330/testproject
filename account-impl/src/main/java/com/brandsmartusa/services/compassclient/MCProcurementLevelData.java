
package com.brandsmartusa.services.compassclient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MCProcurementLevelData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MCProcurementLevelData"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Level3Record" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}MCLevel3Data" maxOccurs="98"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MCProcurementLevelData", propOrder = {
    "level3Record"
})
public class MCProcurementLevelData {

    @XmlElement(name = "Level3Record", required = true)
    protected List<MCLevel3Data> level3Record;

    /**
     * Gets the value of the level3Record property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the level3Record property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLevel3Record().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MCLevel3Data }
     * 
     * 
     */
    public List<MCLevel3Data> getLevel3Record() {
        if (level3Record == null) {
            level3Record = new ArrayList<MCLevel3Data>();
        }
        return this.level3Record;
    }

}
