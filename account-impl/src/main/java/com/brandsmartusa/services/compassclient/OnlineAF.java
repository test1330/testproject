
package com.brandsmartusa.services.compassclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OnlineAF complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OnlineAF"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="AA" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}AA" minOccurs="0"/&gt;
 *         &lt;element name="AB" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}ContactAddress" minOccurs="0"/&gt;
 *         &lt;element name="AC" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}AC" minOccurs="0"/&gt;
 *         &lt;element name="AH" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}AH" minOccurs="0"/&gt;
 *         &lt;element name="AI" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}AI" minOccurs="0"/&gt;
 *         &lt;element name="AL" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}AL" minOccurs="0"/&gt;
 *         &lt;element name="AR" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}AR" minOccurs="0"/&gt;
 *         &lt;element name="AS" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}ContactAddress" minOccurs="0"/&gt;
 *         &lt;element name="AZ" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}AZ" minOccurs="0"/&gt;
 *         &lt;element name="BL" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}BL" minOccurs="0"/&gt;
 *         &lt;element name="VD" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}VD" minOccurs="0"/&gt;
 *         &lt;element name="DT" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}DT" minOccurs="0"/&gt;
 *         &lt;element name="VL" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}VL" minOccurs="0"/&gt;
 *         &lt;element name="FR" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}FR" minOccurs="0"/&gt;
 *         &lt;element name="HN" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}ContactName" minOccurs="0"/&gt;
 *         &lt;element name="LN" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}ContactName" minOccurs="0"/&gt;
 *         &lt;element name="MA" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}MA" minOccurs="0"/&gt;
 *         &lt;element name="ME" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}ME" minOccurs="0"/&gt;
 *         &lt;element name="OI" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}OI" minOccurs="0"/&gt;
 *         &lt;element name="O2" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}O2" minOccurs="0"/&gt;
 *         &lt;element name="O3" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}O3" minOccurs="0"/&gt;
 *         &lt;element name="PA" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}PA" minOccurs="0"/&gt;
 *         &lt;element name="PB" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}PB" minOccurs="0"/&gt;
 *         &lt;element name="PI" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}PI" minOccurs="0"/&gt;
 *         &lt;element name="SM" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}SM" minOccurs="0"/&gt;
 *         &lt;element name="TP" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}TP" minOccurs="0"/&gt;
 *         &lt;element name="VA" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}VA" minOccurs="0"/&gt;
 *         &lt;element name="TA" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}TA" minOccurs="0"/&gt;
 *         &lt;element name="DC" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}DC" minOccurs="0"/&gt;
 *         &lt;element name="SA" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}SA" minOccurs="0"/&gt;
 *         &lt;element name="SW" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}SW" minOccurs="0"/&gt;
 *         &lt;element name="SO" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}SO" minOccurs="0"/&gt;
 *         &lt;element name="ED" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}ED" minOccurs="0"/&gt;
 *         &lt;element name="RM" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}RM" minOccurs="0"/&gt;
 *         &lt;element name="R3" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}R3" minOccurs="0"/&gt;
 *         &lt;element name="R2" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}R2" minOccurs="0"/&gt;
 *         &lt;element name="FF" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}FF" minOccurs="0"/&gt;
 *         &lt;element name="CT" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}CT" minOccurs="0"/&gt;
 *         &lt;element name="CI" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}CI" minOccurs="0"/&gt;
 *         &lt;element name="LP" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}LP" minOccurs="0"/&gt;
 *         &lt;element name="VT" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}VT" minOccurs="0"/&gt;
 *         &lt;element name="DP" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}DP" minOccurs="0"/&gt;
 *         &lt;element name="CS" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}CS" minOccurs="0"/&gt;
 *         &lt;element name="RR" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}RR" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OnlineAF", propOrder = {

})
public class OnlineAF {

    @XmlElement(name = "AA")
    protected AA aa;
    @XmlElement(name = "AB")
    protected ContactAddress ab;
    @XmlElement(name = "AC")
    protected AC ac;
    @XmlElement(name = "AH")
    protected AH ah;
    @XmlElement(name = "AI")
    protected AI ai;
    @XmlElement(name = "AL")
    protected AL al;
    @XmlElement(name = "AR")
    protected AR ar;
    @XmlElement(name = "AS")
    protected ContactAddress as;
    @XmlElement(name = "AZ")
    protected AZ az;
    @XmlElement(name = "BL")
    protected BL bl;
    @XmlElement(name = "VD")
    protected VD vd;
    @XmlElement(name = "DT")
    protected DT dt;
    @XmlElement(name = "VL")
    protected VL vl;
    @XmlElement(name = "FR")
    protected FR fr;
    @XmlElement(name = "HN")
    protected ContactName hn;
    @XmlElement(name = "LN")
    protected ContactName ln;
    @XmlElement(name = "MA")
    protected MA ma;
    @XmlElement(name = "ME")
    protected ME me;
    @XmlElement(name = "OI")
    protected OI oi;
    @XmlElement(name = "O2")
    protected O2 o2;
    @XmlElement(name = "O3")
    protected O3 o3;
    @XmlElement(name = "PA")
    protected PA pa;
    @XmlElement(name = "PB")
    protected PB pb;
    @XmlElement(name = "PI")
    protected PI pi;
    @XmlElement(name = "SM")
    protected SM sm;
    @XmlElement(name = "TP")
    protected TP tp;
    @XmlElement(name = "VA")
    protected VA va;
    @XmlElement(name = "TA")
    protected TA ta;
    @XmlElement(name = "DC")
    protected DC dc;
    @XmlElement(name = "SA")
    protected SA sa;
    @XmlElement(name = "SW")
    protected SW sw;
    @XmlElement(name = "SO")
    protected SO so;
    @XmlElement(name = "ED")
    protected ED ed;
    @XmlElement(name = "RM")
    protected RM rm;
    @XmlElement(name = "R3")
    protected R3 r3;
    @XmlElement(name = "R2")
    protected R2 r2;
    @XmlElement(name = "FF")
    protected FF ff;
    @XmlElement(name = "CT")
    protected CT ct;
    @XmlElement(name = "CI")
    protected CI ci;
    @XmlElement(name = "LP")
    protected LP lp;
    @XmlElement(name = "VT")
    protected VT vt;
    @XmlElement(name = "DP")
    protected DP dp;
    @XmlElement(name = "CS")
    protected CS cs;
    @XmlElement(name = "RR")
    protected RR rr;

    /**
     * Gets the value of the aa property.
     * 
     * @return
     *     possible object is
     *     {@link AA }
     *     
     */
    public AA getAA() {
        return aa;
    }

    /**
     * Sets the value of the aa property.
     * 
     * @param value
     *     allowed object is
     *     {@link AA }
     *     
     */
    public void setAA(AA value) {
        this.aa = value;
    }

    /**
     * Gets the value of the ab property.
     * 
     * @return
     *     possible object is
     *     {@link ContactAddress }
     *     
     */
    public ContactAddress getAB() {
        return ab;
    }

    /**
     * Sets the value of the ab property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactAddress }
     *     
     */
    public void setAB(ContactAddress value) {
        this.ab = value;
    }

    /**
     * Gets the value of the ac property.
     * 
     * @return
     *     possible object is
     *     {@link AC }
     *     
     */
    public AC getAC() {
        return ac;
    }

    /**
     * Sets the value of the ac property.
     * 
     * @param value
     *     allowed object is
     *     {@link AC }
     *     
     */
    public void setAC(AC value) {
        this.ac = value;
    }

    /**
     * Gets the value of the ah property.
     * 
     * @return
     *     possible object is
     *     {@link AH }
     *     
     */
    public AH getAH() {
        return ah;
    }

    /**
     * Sets the value of the ah property.
     * 
     * @param value
     *     allowed object is
     *     {@link AH }
     *     
     */
    public void setAH(AH value) {
        this.ah = value;
    }

    /**
     * Gets the value of the ai property.
     * 
     * @return
     *     possible object is
     *     {@link AI }
     *     
     */
    public AI getAI() {
        return ai;
    }

    /**
     * Sets the value of the ai property.
     * 
     * @param value
     *     allowed object is
     *     {@link AI }
     *     
     */
    public void setAI(AI value) {
        this.ai = value;
    }

    /**
     * Gets the value of the al property.
     * 
     * @return
     *     possible object is
     *     {@link AL }
     *     
     */
    public AL getAL() {
        return al;
    }

    /**
     * Sets the value of the al property.
     * 
     * @param value
     *     allowed object is
     *     {@link AL }
     *     
     */
    public void setAL(AL value) {
        this.al = value;
    }

    /**
     * Gets the value of the ar property.
     * 
     * @return
     *     possible object is
     *     {@link AR }
     *     
     */
    public AR getAR() {
        return ar;
    }

    /**
     * Sets the value of the ar property.
     * 
     * @param value
     *     allowed object is
     *     {@link AR }
     *     
     */
    public void setAR(AR value) {
        this.ar = value;
    }

    /**
     * Gets the value of the as property.
     * 
     * @return
     *     possible object is
     *     {@link ContactAddress }
     *     
     */
    public ContactAddress getAS() {
        return as;
    }

    /**
     * Sets the value of the as property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactAddress }
     *     
     */
    public void setAS(ContactAddress value) {
        this.as = value;
    }

    /**
     * Gets the value of the az property.
     * 
     * @return
     *     possible object is
     *     {@link AZ }
     *     
     */
    public AZ getAZ() {
        return az;
    }

    /**
     * Sets the value of the az property.
     * 
     * @param value
     *     allowed object is
     *     {@link AZ }
     *     
     */
    public void setAZ(AZ value) {
        this.az = value;
    }

    /**
     * Gets the value of the bl property.
     * 
     * @return
     *     possible object is
     *     {@link BL }
     *     
     */
    public BL getBL() {
        return bl;
    }

    /**
     * Sets the value of the bl property.
     * 
     * @param value
     *     allowed object is
     *     {@link BL }
     *     
     */
    public void setBL(BL value) {
        this.bl = value;
    }

    /**
     * Gets the value of the vd property.
     * 
     * @return
     *     possible object is
     *     {@link VD }
     *     
     */
    public VD getVD() {
        return vd;
    }

    /**
     * Sets the value of the vd property.
     * 
     * @param value
     *     allowed object is
     *     {@link VD }
     *     
     */
    public void setVD(VD value) {
        this.vd = value;
    }

    /**
     * Gets the value of the dt property.
     * 
     * @return
     *     possible object is
     *     {@link DT }
     *     
     */
    public DT getDT() {
        return dt;
    }

    /**
     * Sets the value of the dt property.
     * 
     * @param value
     *     allowed object is
     *     {@link DT }
     *     
     */
    public void setDT(DT value) {
        this.dt = value;
    }

    /**
     * Gets the value of the vl property.
     * 
     * @return
     *     possible object is
     *     {@link VL }
     *     
     */
    public VL getVL() {
        return vl;
    }

    /**
     * Sets the value of the vl property.
     * 
     * @param value
     *     allowed object is
     *     {@link VL }
     *     
     */
    public void setVL(VL value) {
        this.vl = value;
    }

    /**
     * Gets the value of the fr property.
     * 
     * @return
     *     possible object is
     *     {@link FR }
     *     
     */
    public FR getFR() {
        return fr;
    }

    /**
     * Sets the value of the fr property.
     * 
     * @param value
     *     allowed object is
     *     {@link FR }
     *     
     */
    public void setFR(FR value) {
        this.fr = value;
    }

    /**
     * Gets the value of the hn property.
     * 
     * @return
     *     possible object is
     *     {@link ContactName }
     *     
     */
    public ContactName getHN() {
        return hn;
    }

    /**
     * Sets the value of the hn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactName }
     *     
     */
    public void setHN(ContactName value) {
        this.hn = value;
    }

    /**
     * Gets the value of the ln property.
     * 
     * @return
     *     possible object is
     *     {@link ContactName }
     *     
     */
    public ContactName getLN() {
        return ln;
    }

    /**
     * Sets the value of the ln property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactName }
     *     
     */
    public void setLN(ContactName value) {
        this.ln = value;
    }

    /**
     * Gets the value of the ma property.
     * 
     * @return
     *     possible object is
     *     {@link MA }
     *     
     */
    public MA getMA() {
        return ma;
    }

    /**
     * Sets the value of the ma property.
     * 
     * @param value
     *     allowed object is
     *     {@link MA }
     *     
     */
    public void setMA(MA value) {
        this.ma = value;
    }

    /**
     * Gets the value of the me property.
     * 
     * @return
     *     possible object is
     *     {@link ME }
     *     
     */
    public ME getME() {
        return me;
    }

    /**
     * Sets the value of the me property.
     * 
     * @param value
     *     allowed object is
     *     {@link ME }
     *     
     */
    public void setME(ME value) {
        this.me = value;
    }

    /**
     * Gets the value of the oi property.
     * 
     * @return
     *     possible object is
     *     {@link OI }
     *     
     */
    public OI getOI() {
        return oi;
    }

    /**
     * Sets the value of the oi property.
     * 
     * @param value
     *     allowed object is
     *     {@link OI }
     *     
     */
    public void setOI(OI value) {
        this.oi = value;
    }

    /**
     * Gets the value of the o2 property.
     * 
     * @return
     *     possible object is
     *     {@link O2 }
     *     
     */
    public O2 getO2() {
        return o2;
    }

    /**
     * Sets the value of the o2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link O2 }
     *     
     */
    public void setO2(O2 value) {
        this.o2 = value;
    }

    /**
     * Gets the value of the o3 property.
     * 
     * @return
     *     possible object is
     *     {@link O3 }
     *     
     */
    public O3 getO3() {
        return o3;
    }

    /**
     * Sets the value of the o3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link O3 }
     *     
     */
    public void setO3(O3 value) {
        this.o3 = value;
    }

    /**
     * Gets the value of the pa property.
     * 
     * @return
     *     possible object is
     *     {@link PA }
     *     
     */
    public PA getPA() {
        return pa;
    }

    /**
     * Sets the value of the pa property.
     * 
     * @param value
     *     allowed object is
     *     {@link PA }
     *     
     */
    public void setPA(PA value) {
        this.pa = value;
    }

    /**
     * Gets the value of the pb property.
     * 
     * @return
     *     possible object is
     *     {@link PB }
     *     
     */
    public PB getPB() {
        return pb;
    }

    /**
     * Sets the value of the pb property.
     * 
     * @param value
     *     allowed object is
     *     {@link PB }
     *     
     */
    public void setPB(PB value) {
        this.pb = value;
    }

    /**
     * Gets the value of the pi property.
     * 
     * @return
     *     possible object is
     *     {@link PI }
     *     
     */
    public PI getPI() {
        return pi;
    }

    /**
     * Sets the value of the pi property.
     * 
     * @param value
     *     allowed object is
     *     {@link PI }
     *     
     */
    public void setPI(PI value) {
        this.pi = value;
    }

    /**
     * Gets the value of the sm property.
     * 
     * @return
     *     possible object is
     *     {@link SM }
     *     
     */
    public SM getSM() {
        return sm;
    }

    /**
     * Sets the value of the sm property.
     * 
     * @param value
     *     allowed object is
     *     {@link SM }
     *     
     */
    public void setSM(SM value) {
        this.sm = value;
    }

    /**
     * Gets the value of the tp property.
     * 
     * @return
     *     possible object is
     *     {@link TP }
     *     
     */
    public TP getTP() {
        return tp;
    }

    /**
     * Sets the value of the tp property.
     * 
     * @param value
     *     allowed object is
     *     {@link TP }
     *     
     */
    public void setTP(TP value) {
        this.tp = value;
    }

    /**
     * Gets the value of the va property.
     * 
     * @return
     *     possible object is
     *     {@link VA }
     *     
     */
    public VA getVA() {
        return va;
    }

    /**
     * Sets the value of the va property.
     * 
     * @param value
     *     allowed object is
     *     {@link VA }
     *     
     */
    public void setVA(VA value) {
        this.va = value;
    }

    /**
     * Gets the value of the ta property.
     * 
     * @return
     *     possible object is
     *     {@link TA }
     *     
     */
    public TA getTA() {
        return ta;
    }

    /**
     * Sets the value of the ta property.
     * 
     * @param value
     *     allowed object is
     *     {@link TA }
     *     
     */
    public void setTA(TA value) {
        this.ta = value;
    }

    /**
     * Gets the value of the dc property.
     * 
     * @return
     *     possible object is
     *     {@link DC }
     *     
     */
    public DC getDC() {
        return dc;
    }

    /**
     * Sets the value of the dc property.
     * 
     * @param value
     *     allowed object is
     *     {@link DC }
     *     
     */
    public void setDC(DC value) {
        this.dc = value;
    }

    /**
     * Gets the value of the sa property.
     * 
     * @return
     *     possible object is
     *     {@link SA }
     *     
     */
    public SA getSA() {
        return sa;
    }

    /**
     * Sets the value of the sa property.
     * 
     * @param value
     *     allowed object is
     *     {@link SA }
     *     
     */
    public void setSA(SA value) {
        this.sa = value;
    }

    /**
     * Gets the value of the sw property.
     * 
     * @return
     *     possible object is
     *     {@link SW }
     *     
     */
    public SW getSW() {
        return sw;
    }

    /**
     * Sets the value of the sw property.
     * 
     * @param value
     *     allowed object is
     *     {@link SW }
     *     
     */
    public void setSW(SW value) {
        this.sw = value;
    }

    /**
     * Gets the value of the so property.
     * 
     * @return
     *     possible object is
     *     {@link SO }
     *     
     */
    public SO getSO() {
        return so;
    }

    /**
     * Sets the value of the so property.
     * 
     * @param value
     *     allowed object is
     *     {@link SO }
     *     
     */
    public void setSO(SO value) {
        this.so = value;
    }

    /**
     * Gets the value of the ed property.
     * 
     * @return
     *     possible object is
     *     {@link ED }
     *     
     */
    public ED getED() {
        return ed;
    }

    /**
     * Sets the value of the ed property.
     * 
     * @param value
     *     allowed object is
     *     {@link ED }
     *     
     */
    public void setED(ED value) {
        this.ed = value;
    }

    /**
     * Gets the value of the rm property.
     * 
     * @return
     *     possible object is
     *     {@link RM }
     *     
     */
    public RM getRM() {
        return rm;
    }

    /**
     * Sets the value of the rm property.
     * 
     * @param value
     *     allowed object is
     *     {@link RM }
     *     
     */
    public void setRM(RM value) {
        this.rm = value;
    }

    /**
     * Gets the value of the r3 property.
     * 
     * @return
     *     possible object is
     *     {@link R3 }
     *     
     */
    public R3 getR3() {
        return r3;
    }

    /**
     * Sets the value of the r3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link R3 }
     *     
     */
    public void setR3(R3 value) {
        this.r3 = value;
    }

    /**
     * Gets the value of the r2 property.
     * 
     * @return
     *     possible object is
     *     {@link R2 }
     *     
     */
    public R2 getR2() {
        return r2;
    }

    /**
     * Sets the value of the r2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link R2 }
     *     
     */
    public void setR2(R2 value) {
        this.r2 = value;
    }

    /**
     * Gets the value of the ff property.
     * 
     * @return
     *     possible object is
     *     {@link FF }
     *     
     */
    public FF getFF() {
        return ff;
    }

    /**
     * Sets the value of the ff property.
     * 
     * @param value
     *     allowed object is
     *     {@link FF }
     *     
     */
    public void setFF(FF value) {
        this.ff = value;
    }

    /**
     * Gets the value of the ct property.
     * 
     * @return
     *     possible object is
     *     {@link CT }
     *     
     */
    public CT getCT() {
        return ct;
    }

    /**
     * Sets the value of the ct property.
     * 
     * @param value
     *     allowed object is
     *     {@link CT }
     *     
     */
    public void setCT(CT value) {
        this.ct = value;
    }

    /**
     * Gets the value of the ci property.
     * 
     * @return
     *     possible object is
     *     {@link CI }
     *     
     */
    public CI getCI() {
        return ci;
    }

    /**
     * Sets the value of the ci property.
     * 
     * @param value
     *     allowed object is
     *     {@link CI }
     *     
     */
    public void setCI(CI value) {
        this.ci = value;
    }

    /**
     * Gets the value of the lp property.
     * 
     * @return
     *     possible object is
     *     {@link LP }
     *     
     */
    public LP getLP() {
        return lp;
    }

    /**
     * Sets the value of the lp property.
     * 
     * @param value
     *     allowed object is
     *     {@link LP }
     *     
     */
    public void setLP(LP value) {
        this.lp = value;
    }

    /**
     * Gets the value of the vt property.
     * 
     * @return
     *     possible object is
     *     {@link VT }
     *     
     */
    public VT getVT() {
        return vt;
    }

    /**
     * Sets the value of the vt property.
     * 
     * @param value
     *     allowed object is
     *     {@link VT }
     *     
     */
    public void setVT(VT value) {
        this.vt = value;
    }

    /**
     * Gets the value of the dp property.
     * 
     * @return
     *     possible object is
     *     {@link DP }
     *     
     */
    public DP getDP() {
        return dp;
    }

    /**
     * Sets the value of the dp property.
     * 
     * @param value
     *     allowed object is
     *     {@link DP }
     *     
     */
    public void setDP(DP value) {
        this.dp = value;
    }

    /**
     * Gets the value of the cs property.
     * 
     * @return
     *     possible object is
     *     {@link CS }
     *     
     */
    public CS getCS() {
        return cs;
    }

    /**
     * Sets the value of the cs property.
     * 
     * @param value
     *     allowed object is
     *     {@link CS }
     *     
     */
    public void setCS(CS value) {
        this.cs = value;
    }

    /**
     * Gets the value of the rr property.
     * 
     * @return
     *     possible object is
     *     {@link RR }
     *     
     */
    public RR getRR() {
        return rr;
    }

    /**
     * Sets the value of the rr property.
     * 
     * @param value
     *     allowed object is
     *     {@link RR }
     *     
     */
    public void setRR(RR value) {
        this.rr = value;
    }

}
