
package com.brandsmartusa.services.compassclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Transaction" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}Transaction"/&gt;
 *         &lt;element name="AdditionalFormats" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}OnlineAF" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transaction",
    "additionalFormats"
})
@XmlRootElement(name = "OnlineTransRequest", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi")
public class OnlineTransRequest {

    @XmlElement(name = "Transaction", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi", required = true)
    protected Transaction transaction;
    @XmlElement(name = "AdditionalFormats", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi")
    protected OnlineAF additionalFormats;

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link Transaction }
     *     
     */
    public Transaction getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link Transaction }
     *     
     */
    public void setTransaction(Transaction value) {
        this.transaction = value;
    }

    /**
     * Gets the value of the additionalFormats property.
     * 
     * @return
     *     possible object is
     *     {@link OnlineAF }
     *     
     */
    public OnlineAF getAdditionalFormats() {
        return additionalFormats;
    }

    /**
     * Sets the value of the additionalFormats property.
     * 
     * @param value
     *     allowed object is
     *     {@link OnlineAF }
     *     
     */
    public void setAdditionalFormats(OnlineAF value) {
        this.additionalFormats = value;
    }

}
