
package com.brandsmartusa.services.compassclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="OrderNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ResponseReasonCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ResponseDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AuthorizationCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AVSResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CSVResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AccountNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ExpirationDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Mop" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RPAdviceCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CAVVResponseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AEVVResponseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ProcessingFormats" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}OnlineResponseAF" minOccurs="0"/&gt;
 *         &lt;element name="ErrorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "OnlineTransResponse", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi")
public class OnlineTransResponse {

    @XmlElement(name = "OrderNumber", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi", required = true)
    protected String orderNumber;
    @XmlElement(name = "ResponseReasonCode", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi", required = true)
    protected String responseReasonCode;
    @XmlElement(name = "ResponseDate", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi", required = true)
    protected String responseDate;
    @XmlElement(name = "AuthorizationCode", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi", required = true)
    protected String authorizationCode;
    @XmlElement(name = "AVSResponseCode", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi", required = true)
    protected String avsResponseCode;
    @XmlElement(name = "CSVResponseCode", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi", required = true)
    protected String csvResponseCode;
    @XmlElement(name = "AccountNumber", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi", required = true)
    protected String accountNumber;
    @XmlElement(name = "ExpirationDate", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi", required = true)
    protected String expirationDate;
    @XmlElement(name = "Mop", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi", required = true)
    protected String mop;
    @XmlElement(name = "RPAdviceCode", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi", required = true)
    protected String rpAdviceCode;
    @XmlElement(name = "CAVVResponseCode", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi")
    protected String cavvResponseCode;
    @XmlElement(name = "AEVVResponseCode", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi")
    protected String aevvResponseCode;
    @XmlElement(name = "Amount", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi", required = true)
    protected String amount;
    @XmlElement(name = "ProcessingFormats", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi")
    protected OnlineResponseAF processingFormats;
    @XmlElement(name = "ErrorMessage", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi")
    protected String errorMessage;

    /**
     * Gets the value of the orderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the value of the orderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderNumber(String value) {
        this.orderNumber = value;
    }

    /**
     * Gets the value of the responseReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseReasonCode() {
        return responseReasonCode;
    }

    /**
     * Sets the value of the responseReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseReasonCode(String value) {
        this.responseReasonCode = value;
    }

    /**
     * Gets the value of the responseDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseDate() {
        return responseDate;
    }

    /**
     * Sets the value of the responseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseDate(String value) {
        this.responseDate = value;
    }

    /**
     * Gets the value of the authorizationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationCode() {
        return authorizationCode;
    }

    /**
     * Sets the value of the authorizationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationCode(String value) {
        this.authorizationCode = value;
    }

    /**
     * Gets the value of the avsResponseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAVSResponseCode() {
        return avsResponseCode;
    }

    /**
     * Sets the value of the avsResponseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAVSResponseCode(String value) {
        this.avsResponseCode = value;
    }

    /**
     * Gets the value of the csvResponseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCSVResponseCode() {
        return csvResponseCode;
    }

    /**
     * Sets the value of the csvResponseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCSVResponseCode(String value) {
        this.csvResponseCode = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpirationDate(String value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the mop property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMop() {
        return mop;
    }

    /**
     * Sets the value of the mop property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMop(String value) {
        this.mop = value;
    }

    /**
     * Gets the value of the rpAdviceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRPAdviceCode() {
        return rpAdviceCode;
    }

    /**
     * Sets the value of the rpAdviceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRPAdviceCode(String value) {
        this.rpAdviceCode = value;
    }

    /**
     * Gets the value of the cavvResponseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAVVResponseCode() {
        return cavvResponseCode;
    }

    /**
     * Sets the value of the cavvResponseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAVVResponseCode(String value) {
        this.cavvResponseCode = value;
    }

    /**
     * Gets the value of the aevvResponseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAEVVResponseCode() {
        return aevvResponseCode;
    }

    /**
     * Sets the value of the aevvResponseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAEVVResponseCode(String value) {
        this.aevvResponseCode = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmount(String value) {
        this.amount = value;
    }

    /**
     * Gets the value of the processingFormats property.
     * 
     * @return
     *     possible object is
     *     {@link OnlineResponseAF }
     *     
     */
    public OnlineResponseAF getProcessingFormats() {
        return processingFormats;
    }

    /**
     * Sets the value of the processingFormats property.
     * 
     * @param value
     *     allowed object is
     *     {@link OnlineResponseAF }
     *     
     */
    public void setProcessingFormats(OnlineResponseAF value) {
        this.processingFormats = value;
    }

    /**
     * Gets the value of the errorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the value of the errorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMessage(String value) {
        this.errorMessage = value;
    }

}
