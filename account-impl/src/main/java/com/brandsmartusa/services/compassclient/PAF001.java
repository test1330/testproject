
package com.brandsmartusa.services.compassclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PAF001 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PAF001"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ZipCodeMatchIndicator" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}MatchValues" minOccurs="0"/&gt;
 *         &lt;element name="StreetMatchIndicator" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}MatchValues" minOccurs="0"/&gt;
 *         &lt;element name="NameMatchIndicator" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}MatchValues" minOccurs="0"/&gt;
 *         &lt;element name="TelephoneMatchIndicator" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}MatchValues" minOccurs="0"/&gt;
 *         &lt;element name="EmailMatchIndicator" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}MatchValues" minOccurs="0"/&gt;
 *         &lt;element name="AddrChngNotInd" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="3"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Reserved" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="106"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PAF001", propOrder = {
    "zipCodeMatchIndicator",
    "streetMatchIndicator",
    "nameMatchIndicator",
    "telephoneMatchIndicator",
    "emailMatchIndicator",
    "addrChngNotInd",
    "reserved"
})
public class PAF001 {

    @XmlElement(name = "ZipCodeMatchIndicator")
    protected String zipCodeMatchIndicator;
    @XmlElement(name = "StreetMatchIndicator")
    protected String streetMatchIndicator;
    @XmlElement(name = "NameMatchIndicator")
    protected String nameMatchIndicator;
    @XmlElement(name = "TelephoneMatchIndicator")
    protected String telephoneMatchIndicator;
    @XmlElement(name = "EmailMatchIndicator")
    protected String emailMatchIndicator;
    @XmlElement(name = "AddrChngNotInd")
    protected String addrChngNotInd;
    @XmlElement(name = "Reserved")
    protected String reserved;

    /**
     * Gets the value of the zipCodeMatchIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZipCodeMatchIndicator() {
        return zipCodeMatchIndicator;
    }

    /**
     * Sets the value of the zipCodeMatchIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZipCodeMatchIndicator(String value) {
        this.zipCodeMatchIndicator = value;
    }

    /**
     * Gets the value of the streetMatchIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreetMatchIndicator() {
        return streetMatchIndicator;
    }

    /**
     * Sets the value of the streetMatchIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreetMatchIndicator(String value) {
        this.streetMatchIndicator = value;
    }

    /**
     * Gets the value of the nameMatchIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameMatchIndicator() {
        return nameMatchIndicator;
    }

    /**
     * Sets the value of the nameMatchIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameMatchIndicator(String value) {
        this.nameMatchIndicator = value;
    }

    /**
     * Gets the value of the telephoneMatchIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelephoneMatchIndicator() {
        return telephoneMatchIndicator;
    }

    /**
     * Sets the value of the telephoneMatchIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelephoneMatchIndicator(String value) {
        this.telephoneMatchIndicator = value;
    }

    /**
     * Gets the value of the emailMatchIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailMatchIndicator() {
        return emailMatchIndicator;
    }

    /**
     * Sets the value of the emailMatchIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailMatchIndicator(String value) {
        this.emailMatchIndicator = value;
    }

    /**
     * Gets the value of the addrChngNotInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddrChngNotInd() {
        return addrChngNotInd;
    }

    /**
     * Sets the value of the addrChngNotInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddrChngNotInd(String value) {
        this.addrChngNotInd = value;
    }

    /**
     * Gets the value of the reserved property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * Sets the value of the reserved property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved(String value) {
        this.reserved = value;
    }

}
