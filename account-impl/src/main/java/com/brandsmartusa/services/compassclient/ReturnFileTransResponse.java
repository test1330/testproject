
package com.brandsmartusa.services.compassclient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReturnRecord" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}BatchRowResp" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ErrorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "returnRecord",
    "errorMessage"
})
@XmlRootElement(name = "ReturnFileTransResponse", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi")
public class ReturnFileTransResponse {

    @XmlElement(name = "ReturnRecord", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi")
    protected List<BatchRowResp> returnRecord;
    @XmlElement(name = "ErrorMessage", namespace = "http://firstdata.com/cmpwsapi/schemas/cmpapi")
    protected String errorMessage;

    /**
     * Gets the value of the returnRecord property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the returnRecord property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReturnRecord().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BatchRowResp }
     * 
     * 
     */
    public List<BatchRowResp> getReturnRecord() {
        if (returnRecord == null) {
            returnRecord = new ArrayList<BatchRowResp>();
        }
        return this.returnRecord;
    }

    /**
     * Gets the value of the errorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the value of the errorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMessage(String value) {
        this.errorMessage = value;
    }

}
