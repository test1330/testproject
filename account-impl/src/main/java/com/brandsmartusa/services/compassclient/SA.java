
package com.brandsmartusa.services.compassclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SA complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SA"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AAV"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="32"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SA", propOrder = {
    "aav"
})
public class SA {

    @XmlElement(name = "AAV", required = true)
    protected String aav;

    /**
     * Gets the value of the aav property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAAV() {
        return aav;
    }

    /**
     * Sets the value of the aav property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAAV(String value) {
        this.aav = value;
    }

}
