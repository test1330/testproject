
package com.brandsmartusa.services.compassclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ShipToAddress complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipToAddress"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="HN" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}LN" minOccurs="0"/&gt;
 *         &lt;element name="HA" type="{http://firstdata.com/cmpwsapi/schemas/cmpmsg}ContactAddressWOTel"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipToAddress", propOrder = {
    "hn",
    "ha"
})
public class ShipToAddress {

    @XmlElement(name = "HN")
    protected LN hn;
    @XmlElement(name = "HA", required = true)
    protected ContactAddressWOTel ha;

    /**
     * Gets the value of the hn property.
     * 
     * @return
     *     possible object is
     *     {@link LN }
     *     
     */
    public LN getHN() {
        return hn;
    }

    /**
     * Sets the value of the hn property.
     * 
     * @param value
     *     allowed object is
     *     {@link LN }
     *     
     */
    public void setHN(LN value) {
        this.hn = value;
    }

    /**
     * Gets the value of the ha property.
     * 
     * @return
     *     possible object is
     *     {@link ContactAddressWOTel }
     *     
     */
    public ContactAddressWOTel getHA() {
        return ha;
    }

    /**
     * Sets the value of the ha property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactAddressWOTel }
     *     
     */
    public void setHA(ContactAddressWOTel value) {
        this.ha = value;
    }

}
