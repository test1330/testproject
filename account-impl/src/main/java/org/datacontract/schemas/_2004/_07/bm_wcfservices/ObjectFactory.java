
package org.datacontract.schemas._2004._07.bm_wcfservices;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.brandsmartusa.services.atgwebbroker.ArrayOfAtgServiceError;
import com.brandsmartusa.services.atgwebbroker.ArrayOfVerificationAddress;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.bm_wcfservices package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _VerifyAddressResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", "VerifyAddress_Response");
    private final static QName _VerifyAddressResponseErrors_QNAME = new QName("http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", "Errors");
    private final static QName _VerifyAddressResponseMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", "Message");
    private final static QName _VerifyAddressResponseSuggestedAddresses_QNAME = new QName("http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", "SuggestedAddresses");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.bm_wcfservices
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link VerifyAddressResponse }
     * 
     */
    public VerifyAddressResponse createVerifyAddressResponse() {
        return new VerifyAddressResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyAddressResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", name = "VerifyAddress_Response")
    public JAXBElement<VerifyAddressResponse> createVerifyAddressResponse(VerifyAddressResponse value) {
        return new JAXBElement<VerifyAddressResponse>(_VerifyAddressResponse_QNAME, VerifyAddressResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAtgServiceError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", name = "Errors", scope = VerifyAddressResponse.class)
    public JAXBElement<ArrayOfAtgServiceError> createVerifyAddressResponseErrors(ArrayOfAtgServiceError value) {
        return new JAXBElement<ArrayOfAtgServiceError>(_VerifyAddressResponseErrors_QNAME, ArrayOfAtgServiceError.class, VerifyAddressResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", name = "Message", scope = VerifyAddressResponse.class)
    public JAXBElement<String> createVerifyAddressResponseMessage(String value) {
        return new JAXBElement<String>(_VerifyAddressResponseMessage_QNAME, String.class, VerifyAddressResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfVerificationAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", name = "SuggestedAddresses", scope = VerifyAddressResponse.class)
    public JAXBElement<ArrayOfVerificationAddress> createVerifyAddressResponseSuggestedAddresses(ArrayOfVerificationAddress value) {
        return new JAXBElement<ArrayOfVerificationAddress>(_VerifyAddressResponseSuggestedAddresses_QNAME, ArrayOfVerificationAddress.class, VerifyAddressResponse.class, value);
    }

}
