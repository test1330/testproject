
package org.datacontract.schemas._2004._07.bm_wcfservices;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.brandsmartusa.services.atgwebbroker.ArrayOfAtgServiceError;
import com.brandsmartusa.services.atgwebbroker.ArrayOfVerificationAddress;
import com.brandsmartusa.services.atgwebbroker.ResponseStatus;


/**
 * <p>Java class for VerifyAddress_Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VerifyAddress_Response"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Errors" type="{http://brandsmartusa.com/Services/AtgWebBroker}ArrayOfAtgService_Error" minOccurs="0"/&gt;
 *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ResponseType" type="{http://brandsmartusa.com/Services/AtgWebBroker}ResponseStatus" minOccurs="0"/&gt;
 *         &lt;element name="SuggestedAddresses" type="{http://brandsmartusa.com/Services/AtgWebBroker}ArrayOfVerificationAddress" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifyAddress_Response", propOrder = {
    "errors",
    "message",
    "responseType",
    "suggestedAddresses"
})
public class VerifyAddressResponse {

    @XmlElementRef(name = "Errors", namespace = "http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfAtgServiceError> errors;
    @XmlElementRef(name = "Message", namespace = "http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<String> message;
    @XmlElement(name = "ResponseType")
    @XmlSchemaType(name = "string")
    protected ResponseStatus responseType;
    @XmlElementRef(name = "SuggestedAddresses", namespace = "http://schemas.datacontract.org/2004/07/BM.WcfServices.AtgWebBroker", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfVerificationAddress> suggestedAddresses;

    /**
     * Gets the value of the errors property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAtgServiceError }{@code >}
     *     
     */
    public JAXBElement<ArrayOfAtgServiceError> getErrors() {
        return errors;
    }

    /**
     * Sets the value of the errors property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAtgServiceError }{@code >}
     *     
     */
    public void setErrors(JAXBElement<ArrayOfAtgServiceError> value) {
        this.errors = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMessage(JAXBElement<String> value) {
        this.message = value;
    }

    /**
     * Gets the value of the responseType property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseStatus }
     *     
     */
    public ResponseStatus getResponseType() {
        return responseType;
    }

    /**
     * Sets the value of the responseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseStatus }
     *     
     */
    public void setResponseType(ResponseStatus value) {
        this.responseType = value;
    }

    /**
     * Gets the value of the suggestedAddresses property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfVerificationAddress }{@code >}
     *     
     */
    public JAXBElement<ArrayOfVerificationAddress> getSuggestedAddresses() {
        return suggestedAddresses;
    }

    /**
     * Sets the value of the suggestedAddresses property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfVerificationAddress }{@code >}
     *     
     */
    public void setSuggestedAddresses(JAXBElement<ArrayOfVerificationAddress> value) {
        this.suggestedAddresses = value;
    }

}
