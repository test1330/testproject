import Dependencies._
import com.typesafe.sbt.packager.docker.DockerPlugin.autoImport.dockerBaseImage
organization in ThisBuild := "com.aie.arc"

// the Scala version that will be used for cross-compiled libraries
scalaVersion in ThisBuild := "2.13.0"
lagomKafkaEnabled in ThisBuild := false
lagomKafkaPort in ThisBuild := 9092

lazy val `account` = (project in file("."))
  .aggregate(`account-api`,`account-impl`)

lazy val cinnamonSettings = Seq(
  libraryDependencies ++= Seq(
    Cinnamon.library.cinnamonSlf4jMdc,
    "com.lightbend.lagom" %% "lagom-javadsl-akka-discovery-service-locator" % com.lightbend.lagom.core.LagomVersion.current
  ),
  cinnamon in test := true,
  cinnamonSuppressRepoWarnings := true,
  cinnamonLogLevel := "INFO")

//lazy val localProdEnvSettings = Seq(Test / run / javaOptions += "-Dconfig.resource=local-application.conf" )

lazy val `account-api` = (project in file("account-api"))
  .settings(common: _*)
  .settings(
    libraryDependencies ++= Seq(
      lagomJavadslApi,
      lombok,
      javaxValidation,
      hibernateValidator,
      javaxElApi,
      typeConfig,
      javaxEl,
      lagomJavadslJackson,
      apacheCommons
    )
  ).dependsOn(`external-service-api`)


lazy val `account-impl` = (project in file("account-impl"))
  .enablePlugins(LagomJava, Cinnamon)
  .settings(common,cinnamonSettings)
  .settings(
    dockerExposedPorts := Seq(9000, 8558, 9091, 10001),
    dockerBaseImage := "openjdk:8-jre-slim",
    sources in (Compile, doc) := Seq.empty,
    WsdlKeys.futureApi := WsdlKeys.PlayJavaFutureApi,
    WsdlKeys.wsdlToCodeArgs += "-autoNameResolution",
    libraryDependencies ++= Seq(
      lagomJavadslKafkaBroker,
      lagomLogback,
      lagomJavadslTestKit,
      akkaMgmtClusterhttp,
      lagomJavadslAkkaDiscovery,
      akkaDiscoveryK8s,
      lagomJavadslCluster,
      lagomJavadslPersistenceJdbc,
      lombok,
      javaxValidation,
      hibernateValidator,
      javaxElApi,
      mapUtils,
      collectionUtils,
      commonsValidator,
      typeConfig,
      javaxEl,
      jsonLibrary,
      "com.aie.arc" %% "tools" % "1.0-SNAPSHOT",
      "com.aie.arc" %% "organization-api" % "1.0-SNAPSHOT",
      sendGrid,
      uniRest
    )
  ).dependsOn(`account-api`)

lazy val `external-service-api` = (project in file("external-service-api"))
  .settings(common: _*)
  .settings(
    libraryDependencies ++= Seq(
      lagomJavadslApi,
      lombok,
      javaxValidation,
      hibernateValidator,
      javaxElApi,
      javaxEl,
      commonsCodecLibrary
    )
  )

val lombok = "org.projectlombok" % "lombok" % "1.18.2"

def common = Seq(
  javacOptions in compile += "-parameters"
)

sources in (Compile, doc) := Seq.empty
libraryDependencies += "com.typesafe.akka" %% "akka-persistence" % "2.6.5"


val javaxValidation = "javax.validation" % "validation-api" % "2.0.1.Final"
val hibernateValidator = "org.hibernate.validator" % "hibernate-validator" % "6.0.13.Final"
val javaxElApi = "javax.el" % "javax.el-api" % "3.0.0"
val javaxEl = "org.glassfish.web" % "javax.el" % "2.2.4" //Mind that the import is needed.
val postgresql = "org.postgresql" % "postgresql" % "9.4-1200-jdbc41"
val hibernate = "org.hibernate" % "hibernate-core" % "5.2.12.Final"
val sendGrid = "com.sendgrid" % "sendgrid-java" % "4.6.0"
val uniRest = "com.mashape.unirest" % "unirest-java" % "1.4.9"
val alpakkaSlick= "com.lightbend.akka" %% "akka-stream-alpakka-slick" % "2.0.1"

val apacheText = "org.apache.commons" % "commons-text" % "1.8"
// https://mvnrepository.com/artifact/org.apache.commons/commons-lang3
val apacheCommons = "org.apache.commons" % "commons-lang3" % "3.0"
// https://mvnrepository.com/artifact/commons-dbutils/commons-dbutils
val apacheDbUtils = "commons-dbutils" % "commons-dbutils" % "1.6"
// https://mvnrepository.com/artifact/io.vavr/vavr
val vavrLibrary = "io.vavr" % "vavr" % "0.10.2"
// https://mvnrepository.com/artifact/commons-codec/commons-codec
val commonsCodecLibrary = "commons-codec" % "commons-codec" % "1.7"
// https://mvnrepository.com/artifact/com.google.code.gson/gson
val jsonLibrary = "com.google.code.gson" % "gson" % "2.8.6"
val typeConfig = "com.typesafe" % "config" % "1.4.0"
val mapUtils = "org.apache.commons" % "commons-collections4" % "4.4"
val collectionUtils  = "org.apache.commons" % "commons-collections4" % "4.4"
val commonsValidator = "commons-validator" % "commons-validator" % "1.6"
// https://mvnrepository.com/artifact/javax.mail/mail
val javaMail = "javax.mail" % "mail" % "1.4"


lagomServiceLocatorAddress in ThisBuild := "0.0.0.0"
lagomServiceGatewayAddress in ThisBuild := "0.0.0.0"

//Below configuration used for cleaning up the data in the cassandra.
//**** Warning start :: Never uncomment and commit to the Git Hub ***
//lagomCassandraCleanOnStart in ThisBuild := false
//**** Warning End ***

// Below line used for local env.
//lagomKafkaEnabled in ThisBuild := true
//Below line used for QA env
lagomKafkaAddress in ThisBuild := "10.207.19.211:9092"
//Below line used for local env
//lagomCassandraEnabled in ThisBuild := true
lagomCassandraEnabled in ThisBuild := false

lagomUnmanagedServices in ThisBuild := Map("avalara-service" -> "https://sandbox-rest.avatax.com",
                                            "ipinfo-service" -> "http://ipinfo.io")

