package com.aie.arc.externalservice.api;

import com.aie.arc.externalservice.api.request.AvalaraRequest;
import com.aie.arc.externalservice.api.response.AvalaraResponse;
import com.lightbend.lagom.javadsl.api.CircuitBreaker;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.transport.Method;

public interface AvalaraService extends Service {
    ServiceCall<AvalaraRequest, AvalaraResponse> resolveAddress();
    @Override
    default Descriptor descriptor() {
        return Service.named("avalara-service")
                .withCalls(
                        Service.restCall(Method.POST, "/api/v2/addresses/resolve", this::resolveAddress)
                ).withCircuitBreaker(CircuitBreaker.identifiedBy("avalara")).withAutoAcl(Boolean.TRUE);
    }
}
