package com.aie.arc.externalservice.api;

import akka.NotUsed;
import com.aie.arc.externalservice.api.response.FreshAddressResponse;
import com.lightbend.lagom.javadsl.api.CircuitBreaker;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.transport.Method;

public interface FreshAddressService extends Service {
    ServiceCall<NotUsed, FreshAddressResponse> isValidEmail(String company, String contract, String service, String email);

    @Override
    default Descriptor descriptor() {
        return Service.named("freshaddress-service")
                .withCalls(
                        Service.restCall(Method.POST, "/v7.2/?company&contract&service&email", this::isValidEmail)
                ).withCircuitBreaker(CircuitBreaker.identifiedBy("freshAddress")).withAutoAcl(Boolean.TRUE);
    }
}
