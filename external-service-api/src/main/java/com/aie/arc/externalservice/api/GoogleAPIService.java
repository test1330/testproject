package com.aie.arc.externalservice.api;

import akka.NotUsed;
import com.aie.arc.externalservice.api.response.GoogleAPIServiceResponse;
import com.lightbend.lagom.javadsl.api.CircuitBreaker;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.transport.Method;

public interface GoogleAPIService extends Service {
    ServiceCall<NotUsed, GoogleAPIServiceResponse> getDistanceByZipCode(String key, String origins, String destinations, String mode, String sensor, String units);
    @Override
    default Descriptor descriptor() {
        return Service.named("googleAPI-service")
                .withCalls(Service.restCall(Method.GET,
                        "/maps/api/distancematrix/json?key&origins&destinations&mode&sensor&units",
                        this::getDistanceByZipCode))
                .withCircuitBreaker(CircuitBreaker.identifiedBy("googleAPIService"))
                .withAutoAcl(Boolean.TRUE);
    }
}
