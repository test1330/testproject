package com.aie.arc.externalservice.api;

import akka.NotUsed;
import com.aie.arc.externalservice.api.response.IpInfoServiceResponse;
import com.lightbend.lagom.javadsl.api.CircuitBreaker;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.transport.Method;

public interface IpInfoService extends Service {
    ServiceCall<NotUsed, IpInfoServiceResponse> invokeAPI(String ipAddress, String token);
    @Override
    default Descriptor descriptor() {
        return Service.named("ipinfo-service")
                .withCalls(Service.restCall(Method.GET, "/:ipAddress/json?token", this::invokeAPI))
                .withAutoAcl(Boolean.TRUE);
    }
}