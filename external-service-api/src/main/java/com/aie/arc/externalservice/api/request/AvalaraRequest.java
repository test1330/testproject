package com.aie.arc.externalservice.api.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import lombok.Value;

import java.math.BigDecimal;

@Data
@JsonDeserialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AvalaraRequest {
    private String textCase;
    private String line1;
    private String line2;
    private String city;
    private String region;
    private String country;
    private String postalCode;
    private BigDecimal latitude;
    private BigDecimal longitude;
}
