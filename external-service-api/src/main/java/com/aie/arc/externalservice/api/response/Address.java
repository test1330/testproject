package com.aie.arc.externalservice.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

@Value
@JsonDeserialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Address {
    private String textCase;
    private String line1;
    private String line2;
    private String city;
    private String region;
    private String postalCode;
}
