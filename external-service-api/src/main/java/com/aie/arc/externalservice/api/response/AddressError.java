package com.aie.arc.externalservice.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;


@Value
@JsonDeserialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddressError {
    private String summary;
    private String details;
    private String refersTo;
    private String severity;
    private String source;
}
