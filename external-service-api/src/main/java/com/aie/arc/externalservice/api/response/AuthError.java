package com.aie.arc.externalservice.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

import java.util.ArrayList;

@Value
@JsonDeserialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthError {
    private String code;
    private String message;
    private String target;
    ArrayList< Object > details = new ArrayList <> ();
}
