package com.aie.arc.externalservice.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

import java.util.ArrayList;

@Value
@JsonDeserialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AvalaraResponse {
    Address address;
    ArrayList<ValidatedAddressInfo> validatedAddresses;
    Coordinates coordinates;
    private String resolutionQuality;
    ArrayList <TaxAuthoritie> taxAuthorities;
    ArrayList <AddressError> messages;
    AuthError error;
}
