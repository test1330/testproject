package com.aie.arc.externalservice.api.response;

import lombok.Value;

@Value
public class AvalaraServiceResponse {
    private AvalaraResponse avalaraResponse;
    private  Throwable throwable;
}
