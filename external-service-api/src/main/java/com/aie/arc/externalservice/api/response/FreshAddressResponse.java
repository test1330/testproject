package com.aie.arc.externalservice.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;

@Value
@JsonDeserialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FreshAddressResponse {
    @JsonProperty("SUGG_COMMENT")
    private String suggComment;
    @JsonProperty("FINDING")
    private String finding;
    @JsonProperty("ERROR_RESPONSE")
    private String errorResponse;
    @JsonProperty("ERROR")
    private String error;
    @JsonProperty("COMMENT_CODE")
    private String code;
    @JsonProperty("EMAIL")
    private String email;
    @JsonProperty("SUGG_EMAIL")
    private String suggEmail;
    @JsonProperty("UUID")
    private String uuID;
    @JsonProperty("COMMENT")
    private String comment;
}