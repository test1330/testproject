package com.aie.arc.externalservice.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

import java.util.List;

@Value
@JsonDeserialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GoogleAPIServiceResponse {
    List<String> destination_addresses;
    List<String> origin_addresses;
    List<Record> rows;
    String status;
}
