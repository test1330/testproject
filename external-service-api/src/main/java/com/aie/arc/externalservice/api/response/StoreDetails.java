package com.aie.arc.externalservice.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

@Value
@JsonDeserialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StoreDetails {
    private Distance distance;
    private Duration duration;
    private String status;
}
