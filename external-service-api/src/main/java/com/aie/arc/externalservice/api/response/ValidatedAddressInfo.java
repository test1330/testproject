package com.aie.arc.externalservice.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

import java.math.BigDecimal;

@Value
@JsonDeserialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ValidatedAddressInfo {
    private String addressType;
    private String line1;
    private String line2;
    private String line3;
    private String city;
    private String region;
    private String country;
    private String postalCode;
    private BigDecimal latitude;
    private BigDecimal longitude;
}
