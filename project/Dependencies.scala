import sbt._

object Dependencies {
//akka mgmt
lazy val akkaMgmtVersion = "1.0.6"

lazy val akkaDiscoveryK8s =  "com.lightbend.akka.discovery" %% "akka-discovery-kubernetes-api" % akkaMgmtVersion

lazy val akkaMgmtClusterhttp = "com.lightbend.akka.management" %% "akka-management-cluster-http" % akkaMgmtVersion

}
