// The Lagom plugin
addSbtPlugin("com.lightbend.lagom" % "lagom-sbt-plugin" % "1.6.2")
// Needed for importing the project into Eclipse
addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "5.2.4")
resolvers += Resolver.url("play-sbt-plugins", url("https://dl.bintray.com/playframework/sbt-plugin-releases/"))(Resolver.ivyStylePatterns)
addSbtPlugin("com.typesafe.sbt" % "sbt-play-soap" % "1.1.5")
addSbtPlugin("com.lightbend.cinnamon" % "sbt-cinnamon" % "2.13.4")
addSbtPlugin("io.github.davidmweber" % "flyway-sbt" % "6.4.2")
addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.13")