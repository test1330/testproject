CREATE TABLE ACCOUNT (
						 ACCOUNT_ID VARCHAR(40) NOT NULL ,
						 STORE_ID VARCHAR(40) NULL ,
						 ACCOUNT_NUMBER VARCHAR(36) NULL ,
						 FIRST_NAME VARCHAR(40) NOT NULL ,
						 MIDDLE_NAME VARCHAR(40) NULL ,
						 LAST_NAME VARCHAR(40) NOT NULL ,
						 EMAIL VARCHAR(255) NOT NULL ,
						 PHONE VARCHAR(40) ,
						 CREATED_AT VARCHAR(40) NOT NULL ,
						 MODIFIED_AT VARCHAR(40) NOT NULL ,
						 SUBSCRIBED BOOLEAN NOT NULL ,
						 ACTIVE BOOLEAN NOT NULL ,
						 DATE_OF_BIRTH VARCHAR(40) NULL,
						 SHOPPING_FOR VARCHAR(40) NULL,
						 ABOUT_US VARCHAR(40) NULL,
						 MY_ZIP VARCHAR(10) NULL,
						 DEFAULT_SHIPPING_ID VARCHAR(40) NULL,
						 DEFAULT_BILLING_ID VARCHAR(40) NULL,
						 DEFAULT_CARD_ID VARCHAR(40) NULL,
						  PRIMARY KEY (ACCOUNT_ID)
					);


CREATE TABLE BMCIS_FAILED_RECORDS (
                        ACCOUNT_ID VARCHAR(40) NOT NULL ,
                        STATUS VARCHAR(40) NOT NULL ,
                        MODIFIED_AT VARCHAR(40) NOT NULL ,
                        PRIMARY KEY (ACCOUNT_ID)
					);

CREATE TABLE ADDRESS (
						ADDRESS_ID VARCHAR(40) NOT NULL ,
						ACCOUNT_ID VARCHAR(40) NOT NULL ,
						ADDRESS_1 VARCHAR(50) NOT NULL ,
						ADDRESS_2 VARCHAR(50) NULL ,
						FIRST_NAME VARCHAR(100) NOT NULL ,
						LAST_NAME VARCHAR(100) NULL ,
						CITY VARCHAR(30) NOT NULL ,
						STATE VARCHAR(20) NOT NULL,
						COUNTRY VARCHAR(40) NOT NULL,
						POSTAL_CODE VARCHAR(10) NOT NULL ,
						PHONE VARCHAR(30) NOT NULL,
						IS_SHIPPING BOOLEAN NOT NULL ,
						IS_BILLING BOOLEAN NOT NULL ,
						ACTIVE BOOLEAN NOT NULL ,
						FIPS_COUNTRY VARCHAR(10) NULL,
						FIPS_STATE VARCHAR(10) NULL,
						ALT_PHONE_NUMBER VARCHAR(30) NULL,
						AVS_VALID BOOLEAN NOT NULL,
						SKIP_AVS_VALIDATION BOOLEAN NOT NULL,
						 PRIMARY KEY (ADDRESS_ID)
					);


CREATE TABLE CARD (
						CARD_ID VARCHAR(40) NOT NULL ,
						ACCOUNT_ID VARCHAR(40) NOT NULL ,
						FIRST_NAME VARCHAR(40) NOT NULL ,
						LAST_NAME VARCHAR(40) NULL ,
						CARD_NUMBER VARCHAR(80) NOT NULL ,
						SUBSCRIPTION_ID VARCHAR(40) NULL ,
						CARD_TYPE VARCHAR(40) NOT NULL ,
						EXPIRATION_MONTH VARCHAR(20) NULL ,
						EXPIRATION_YEAR VARCHAR(20) NULL ,
						CARD_NICK_NAME VARCHAR(120) NOT NULL ,
						ACTIVE BOOLEAN NOT NULL ,
						 PRIMARY KEY (CARD_ID)
					);
					

-- TABLE: READ_SIDE_OFFSETS

-- DROP TABLE READ_SIDE_OFFSETS;
CREATE TABLE READ_SIDE_OFFSETS(
    READ_SIDE_ID CHARACTER VARYING(255) collate pg_catalog."default" NOT NULL,
    TAG CHARACTER VARYING(255) collate pg_catalog."default" NOT NULL,
    SEQUENCE_OFFSET BIGINT,
    TIME_UUID_OFFSET CHARACTER(36) collate pg_catalog."default",
    CONSTRAINT READ_SIDE_OFFSETS_PK PRIMARY KEY (READ_SIDE_ID, TAG)
)

WITH (
    OIDS = FALSE
)

TABLESPACE PG_DEFAULT;

------------------------------------RBAC SCRIPTS ------------------------------------


-- RBAC DDLs----
CREATE TABLE IF NOT EXISTS business_user (
    id VARCHAR(36) NOT NULL ,
    account_id VARCHAR(36) NOT NULL ,
    first_name VARCHAR(64) NOT NULL ,
    middle_name VARCHAR(64) NULL ,
    last_name VARCHAR(64) NOT NULL ,
    email VARCHAR(36),
    phone_number VARCHAR(36) ,
    designation VARCHAR(36) NULL ,
    role_id VARCHAR(36) NULL ,
    created_at VARCHAR(36) NOT NULL ,
    modified_at VARCHAR(36) NOT NULL ,
    active boolean NOT NULL ,
    PRIMARY KEY (id));


CREATE TABLE IF NOT EXISTS access_right (
    id VARCHAR(36) NOT NULL ,
    access_right_id VARCHAR(36) NOT NULL ,
    name VARCHAR(64) NOT NULL ,
    description VARCHAR(264) NOT NULL,
    locale_code  VARCHAR(10),
    active BOOLEAN NOT NULL ,
    PRIMARY KEY (id));

CREATE TABLE IF NOT EXISTS permission (
    id VARCHAR(36) NOT NULL ,
    name VARCHAR(64) NOT NULL,
    locale_code  VARCHAR(10),
    permission_id VARCHAR(36) NOT NULL ,
    access_right_id VARCHAR(36) NOT NULL ,
    workspace_id VARCHAR(64) NOT NULL ,
    active BOOLEAN NOT NULL ,
    PRIMARY KEY (id));

CREATE TABLE IF NOT EXISTS role_group (
    id VARCHAR(36) NOT NULL ,
    role_group_id VARCHAR(36) NOT NULL ,
    name VARCHAR(64) NOT NULL ,
    description VARCHAR(264) NOT NULL,
    locale_code  VARCHAR(10),
    active BOOLEAN NOT NULL ,
    PRIMARY KEY (id));

CREATE TABLE IF NOT EXISTS role (
    id VARCHAR(36) NOT NULL ,
    role_id VARCHAR(36) NOT NULL ,
    role_group_id VARCHAR(36) ,
    name VARCHAR(64) NOT NULL,
    locale_code  VARCHAR(10),
    active boolean,
    PRIMARY KEY (id));

CREATE TABLE IF NOT EXISTS workspace (
    id VARCHAR(36) NOT NULL ,
    name VARCHAR(64) NOT NULL,
    locale_code  VARCHAR(10),
    workspace_id VARCHAR(36) NOT NULL ,
    module VARCHAR(64) NOT NULL ,
    permission_type VARCHAR(36) NOT NULL ,
    active BOOLEAN NOT NULL ,
    PRIMARY KEY (id));

CREATE TABLE IF NOT EXISTS locale (
    id VARCHAR(36) NOT NULL ,
    localeId VARCHAR(36) NOT NULL ,
    countryId VARCHAR(36) ,
    name VARCHAR(400)  ,
    codes VARCHAR(400)  ,
    descriptions VARCHAR(600) ,
    isValue boolean,
    PRIMARY KEY (id));

--RABC DDLs ---
