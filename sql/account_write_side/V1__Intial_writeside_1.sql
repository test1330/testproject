-- SEQUENCE: JOURNAL_ORDERING_SEQ
-- DROP SEQUENCE JOURNAL_ORDERING_SEQ;

CREATE SEQUENCE JOURNAL_ORDERING_SEQ
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

-- TABLE: JOURNAL

-- DROP TABLE JOURNAL;

CREATE TABLE JOURNAL(
    ORDERING BIGINT NOT NULL DEFAULT NEXTVAL('JOURNAL_ORDERING_SEQ'::REGCLASS),
    DELETED BOOLEAN NOT NULL DEFAULT FALSE,
    PERSISTENCE_ID CHARACTER VARYING(255) collate pg_catalog."default" NOT NULL,
    SEQUENCE_NUMBER BIGINT NOT NULL,
    MESSAGE BYTEA NOT NULL,
    TAGS CHARACTER VARYING(255) collate pg_catalog."default",
    CONSTRAINT JOURNAL_PK PRIMARY KEY (PERSISTENCE_ID, SEQUENCE_NUMBER)
)

WITH (
    OIDS = FALSE
)

TABLESPACE PG_DEFAULT;

-- INDEX: JOURNAL_ORDERING_IDX

-- DROP INDEX JOURNAL_ORDERING_IDX;

CREATE UNIQUE INDEX JOURNAL_ORDERING_IDX
    ON JOURNAL USING BTREE
    (ORDERING)
    TABLESPACE PG_DEFAULT;
 

-- TABLE: READ_SIDE_OFFSETS

-- DROP TABLE READ_SIDE_OFFSETS;

CREATE TABLE READ_SIDE_OFFSETS(
    READ_SIDE_ID CHARACTER VARYING(255) collate pg_catalog."default" NOT NULL,
    TAG CHARACTER VARYING(255) collate pg_catalog."default" NOT NULL,
    SEQUENCE_OFFSET BIGINT,
    TIME_UUID_OFFSET CHARACTER(36) collate pg_catalog."default",
    CONSTRAINT READ_SIDE_OFFSETS_PK PRIMARY KEY (READ_SIDE_ID, TAG)
)

WITH (
    OIDS = FALSE
)

TABLESPACE PG_DEFAULT;


-- TABLE: SNAPSHOT

-- DROP TABLE SNAPSHOT;

CREATE TABLE SNAPSHOT(
    PERSISTENCE_ID CHARACTER VARYING(255) collate pg_catalog."default" NOT NULL,
    SEQUENCE_NUMBER BIGINT NOT NULL,
    CREATED BIGINT NOT NULL,
    SNAPSHOT BYTEA NOT NULL,
    CONSTRAINT SNAPSHOT_PK PRIMARY KEY (PERSISTENCE_ID, SEQUENCE_NUMBER)
)

WITH (
    OIDS = FALSE
)

TABLESPACE PG_DEFAULT;
